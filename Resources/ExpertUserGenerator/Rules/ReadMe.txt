The 'Expert User Generator' allows the selection of the templates located in the plug-in templates folder and subfolders. 

The restrictions to the plug-in templates are:

* The template file name must end with "_Template.py". So a valid name would be "MyExample_Template.py". 
** If you don't name the file like this it will not be displayed after clicking the "Reload Table" button.

* The file name (without the .py extension) must be the same as the jython class name.
** For the example above the class name must be: MyExample_Template

* The template can call other Jython scripts located in the same folder or the SharedTemplates folder

The template can get a reference to the specs file using a plugin method:

{code}
from research.ch.cern.unicos.plugins.interfaces import APlugin #REQUIRED
...

# Get a reference to the plug-in
self.thePlugin = APlugin.getPluginInterface()	
# Get a reference to the specs file.
self.theUnicosProject = self.thePlugin.getUnicosProject()
{code}

To write files in the output folder use the new methods defined in the plugin:

{code}
# Write a text file in the output folder
textContent = "This is an example of text file."
self.thePlugin.writeFile("example.txt", textContent)

# Write an xml file in the output folder (and verify that the content is well formed)
xmlContent = "<root><text>This is an example of xml file.</text></root>"
self.thePlugin.writeXmlFile("example.xml", xmlContent)
{code}

For more information on other available jython functions see the 
UAB CPC Template API delivered with the UAB CPC Wizard release