# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from time import strftime
from java.lang import System


class Example_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        # Get a reference to the plug-in
        self.thePlugin = APlugin.getPluginInterface()
        # Get a reference to the specs file.
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.thePlugin.writeInUABLog("initialize Example Template.")
        # to call if you want use s7db_id function do not forget to import the package: from research.ch.cern.unicos.cpc.utilities.siemens import S7Functions
        # S7Functions.initialize()

    def check(self):
        self.thePlugin.writeInUABLog("check Example Template.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for Example Template.")
        self.dateAndTime = strftime("%Y-%m-%d %H:%M:%S")  # '2007-03-03 22:14:39'
        self.theApplicationName = self.thePlugin.getApplicationName()
        self.thePluginId = self.thePlugin.getId()
        self.thePluginVersion = self.thePlugin.getVersionId()
        self.theUserName = System.getProperty("user.name")

    def process(self, *params):
        self.thePlugin.writeInUABLog("Starting processing Example Template.")
        # Write a text file in the output folder
        textContent = "This is an example of text file."
        self.thePlugin.writeFile("example.txt", textContent)
        # Write an xml file in the output folder (and verify that the content is well formed)
        xmlContent = "<root><text>This is an example of xml file.</text></root>"
        self.thePlugin.writeXmlFile("example.xml", xmlContent)

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for Example Template.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for Example Template.")
