# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate     #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin            #REQUIRED
from time import strftime
from java.lang import System
import openpyxl # Version 1.2.3 used by FlexExtractor. Same one here

import ucpc_library.shared_grafcet_data
reload(ucpc_library.shared_grafcet_data)
import os
from research.ch.cern.unicos.utilities import AbsolutePathBuilder


class Schneider_Expert_Stepper_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   

   
   def initialize(self):
    # Get a reference to the plug-in
    self.thePlugin = APlugin.getPluginInterface()    
    # Get a reference to the specs file.
    self.theUnicosProject = self.thePlugin.getUnicosProject()
    self.thePlugin.writeInUABLog("Initialize Schneider Stepper Template.")

   def check(self):
    self.thePlugin.writeInUABLog("Check Schneider Stepper Template.")

   def begin(self):
    self.thePlugin.writeInUABLog("begin in Jython for Schneider Stepper  Template.")
    self.dateAndTime = strftime("%Y-%m-%d %H:%M:%S")    #'2007-03-03 22:14:39'
    self.theApplicationName = self.thePlugin.getApplicationName()
    self.thePluginId = self.thePlugin.getId()
    self.thePluginVersion = self.thePlugin.getVersionId()
    self.theUserName = System.getProperty("user.name")
        
   def process(self, *params):       
    self.thePlugin.writeInUABLog("Starting processing Schneider Stepper Template.")

    folderName = os.path.dirname(AbsolutePathBuilder.getApplicationPathParameter("GeneralData:InstancesConfigurationFileName")) + "\\StepperSpecs\\"
    if not os.path.exists(folderName):
        self.thePlugin.writeWarningInUABLog('Folder StepperSpecs will be generated in Specs folder')
        os.makedirs(folderName)
    if not True in [fileName.endswith(".xlsx") and not fileName.startswith("~\x24") and not fileName == "StepperSpecs_Example.xlsx" for fileName in os.listdir(folderName)]:
        self.thePlugin.writeWarningInUABLog('No valid file found in StepperSpecs directory. Nothing will be generated')
    for fileName in os.listdir(folderName):
        if fileName.endswith(".xlsx") and not fileName.startswith("~\x24") and not fileName == "StepperSpecs_Example.xlsx":
            SharedGrafcetData = ucpc_library.shared_grafcet_data.SharedGrafcetData(self.thePlugin, folderName + fileName)
            if not SharedGrafcetData.wrong_data:
                header = """# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: \xe9
from java.util import Vector
from java.util import ArrayList
import SchLogic_DefaultAlarms_Template
# reload(SchLogic_DefaultAlarms_Template)
%(imports)s

def %(logicName)sLogic(thePlugin, theUnicosProject, %(logicParams)s, theSectionText, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = SchLogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

%(templateCode)s

    theSectionText = '''
(*
Master:     \x24Master\x24
Name:     \x24Name\x24
Lparam1:    \x24Lparam1\x24
Lparam2:    \x24Lparam2\x24
Lparam3:    \x24Lparam3\x24
Lparam4:    \x24Lparam4\x24
Lparam5:    \x24Lparam5\x24
Lparam6:    \x24Lparam6\x24
Lparam7:    \x24Lparam7\x24
Lparam8:    \x24Lparam8\x24
Lparam9:    \x24Lparam9\x24
Lparam10:    \x24Lparam10\x24
*)

'''

"""
                footer = """    return theSectionText
"""
                for PCOName in SharedGrafcetData.get_PCO_list():
                    outFileName, [fileContent, variables] = SharedGrafcetData.get_graph_file(PCOName)

                    code_variables = SharedGrafcetData.get_code_variables(PCOName)

                    sl_content = """    theSectionText = '''$fileContent$'''

    thePlugin.writeVariable('''$variables$''')
"""
                    sl_file_content = ""
                    sl_file = SharedGrafcetData.get_logic_file(PCOName, "SL")
                    if sl_file:
                        sl_file_full = os.path.dirname(AbsolutePathBuilder.getApplicationPathParameter("GeneralData:InstancesConfigurationFileName")) + "\\StepperSpecs\\" + sl_file
                        if os.path.isfile(sl_file_full):
                            open_sl_file = open(sl_file_full)
                            sl_file_content = open_sl_file.read().replace("# Grafcet_logic", sl_content).replace("# Grafcet_template_variables", SharedGrafcetData.get_template_variables(PCOName))
                            open_sl_file.close()
                        else:
                            self.thePlugin.writeErrorInUABLog("Sequencer logic specified file \"$sl_file$\" does not exist")
                    else:
                        sl_file_content = header % {'imports': '', 'templateCode': '    $SharedGrafcetData.get_template_variables(PCOName)$', 'logicName': 'SL', 'logicParams': 'Name, Master'} + sl_content + footer
                    if sl_file_content:
                        self.thePlugin.writeFile("SchLogic_$PCOName$_SL.py", sl_file_content)
                        outputFolder = os.path.join(AbsolutePathBuilder.getTechnicalPathParameter("PluginsList:UnityLogicGenerator:Templates:TemplatesFolder"),
                                                    self.thePlugin.getXMLConfig().getTechnicalParameter("PluginsList:UnityLogicGenerator:Templates:UserTemplatesFolder").replace("/","\\"),
                                                    "AutomaticGeneratedGrafcet\\")
                        if not os.path.exists(outputFolder):
                            os.makedirs(outputFolder)
                        outputFile = open(os.path.join(outputFolder, "SchLogic_$PCOName$_SL.py"), 'w')
                        outputFile.write(sl_file_content.replace("\r",""))
                        outputFile.close()

                    tl_content = "    theSectionText += '''(* Automatic generated code from $fileName$ using the Schneider_Expert_Stepper_Template <begin> *)\n'''\n\n"
                    tl_vars = ""
                    tl_step_content = SharedGrafcetData.get_word_status_steps(PCOName)
                    tl_trans_content = SharedGrafcetData.get_word_status_transitions(PCOName)
                    tl_WS_pattern = SharedGrafcetData.get_WS_pattern(PCOName)
                    if code_variables:
                        tl_content += "    theSectionText += Decorator.decorateExpression('''$code_variables$''')\n\n"
                        tl_vars = SharedGrafcetData.get_code_variables_declaration(PCOName) + "\n"
                    tl_content += "    theSectionText += Decorator.decorateExpression('''$SharedGrafcetData.get_transition_conditions(PCOName)$''')\n\n"
                    if tl_step_content or tl_trans_content:
                        tl_content += """    theSectionText += '''
$tl_step_content$'''

    theSectionText += '''
$tl_trans_content$'''

"""
                    if tl_vars:
                        tl_content += "    thePlugin.writeVariable('''\n$tl_vars$\n\n''')\n\n"
                    if tl_WS_pattern != "(**)":
                        tl_content += """    theSectionText += '''
$tl_WS_pattern$'''

"""
                    tl_content += "    theSectionText += '''\n(* Automatic generated code from $fileName$ using the Schneider_Expert_Stepper_Template <end> *)'''\n\n"
                    tl_file_content = ""
                    tl_file = SharedGrafcetData.get_logic_file(PCOName, "TL")
                    if tl_file:
                        tl_file_full = os.path.dirname(AbsolutePathBuilder.getApplicationPathParameter("GeneralData:InstancesConfigurationFileName")) + "\\StepperSpecs\\" + tl_file
                        if os.path.isfile(tl_file_full):
                            open_tl_file = open(tl_file_full)
                            tl_file_content = open_tl_file.read().replace("# Grafcet_logic", tl_content).replace("# Grafcet_template_variables", SharedGrafcetData.get_template_variables(PCOName))
                            open_tl_file.close()
                        else:
                            self.thePlugin.writeErrorInUABLog("Transition logic specified file \"$tl_file$\" does not exist")
                    else:
                        tl_content = """    theSectionText = theSectionText + '''(* IO Error/Simu *)
\x24Name\x24_TL_IOErr :=0 ; (*To Complete*)
\x24Name\x24_TL_IOSim := 0;(*To Complete*)
'''

""" + tl_content
                        tl_file_content = header % {'imports': 'import ucpc_library.shared_decorator\nreload(ucpc_library.shared_decorator)\n', 'templateCode': '    Decorator = ucpc_library.shared_decorator.ExpressionDecorator()\n$SharedGrafcetData.get_template_variables(PCOName)$', 'logicName': 'TL', 'logicParams': 'Master, Name'} + tl_content + footer
                    if tl_file_content:
                        self.thePlugin.writeFile("SchLogic_$PCOName$_TL.py", tl_file_content)
                        outputFolder = os.path.join(AbsolutePathBuilder.getTechnicalPathParameter("PluginsList:UnityLogicGenerator:Templates:TemplatesFolder"),
                                                    self.thePlugin.getXMLConfig().getTechnicalParameter("PluginsList:UnityLogicGenerator:Templates:UserTemplatesFolder").replace("/","\\"),
                                                    "AutomaticGeneratedGrafcet\\")
                        if not os.path.exists(outputFolder):
                            os.makedirs(outputFolder)
                        outputFile = open(os.path.join(outputFolder, "SchLogic_$PCOName$_TL.py"), 'w')
                        outputFile.write(tl_file_content.replace("\r",""))
                        outputFile.close()

   def end(self):
    self.thePlugin.writeInUABLog("end in Jython for Schneider Stepper  Template.")


   def shutdown(self):
    self.thePlugin.writeInUABLog("shutdown in Jython for Schneider Stepper  Template.")
