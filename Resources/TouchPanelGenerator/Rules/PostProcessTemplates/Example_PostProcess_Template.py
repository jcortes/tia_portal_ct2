# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from time import strftime
from java.lang import System
import os
from research.ch.cern.unicos.utilities import AbsolutePathBuilder


class Example_PostProcess_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
     # Get a reference to the plug-in
        self.thePlugin = APlugin.getPluginInterface()
        # Get a reference to the specs file.
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.thePlugin.writeInUABLog("initialize Example_PostProcess_Template.")

    def check(self):
        self.thePlugin.writeInUABLog("check Example_PostProcess_Template.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for Example_PostProcess_Template.")
        self.dateAndTime = strftime("%Y-%m-%d %H:%M:%S")  # '2007-03-03 22:14:39'
        self.theApplicationName = self.thePlugin.getApplicationName()
        self.thePluginId = self.thePlugin.getId()
        self.thePluginVersion = self.thePlugin.getVersionId()
        self.theUserName = System.getProperty("user.name")

    def process(self, *params):
        self.thePlugin.writeInUABLog("Starting processing Example_PostProcess_Template.")

        #outputFilePath = AbsolutePathBuilder.getTechnicalPathParameter(self.thePlugin.getId() + ":OutputParameters:OutputFile")
        #self.thePlugin.writeDebugInUABLog(self.thePlugin.getId() + ":OutputParameters:OutputFile path = $outputFilePath$")

        config = self.thePlugin.getXMLConfig()
        paramsMap = config.getTechnicalParametersMap(self.thePlugin.getId() + ":OutputParameters")
        self.thePlugin.writeDebugInUABLog(self.thePlugin.getId() + ":OutputParameters map")
        for key in paramsMap.keySet():
            value = paramsMap.get(key)
            self.thePlugin.writeDebugInUABLog("$key$ = $value$")

        # Get the absolute path of the specs
        absolutePath = self.theUnicosProject.getSpecsPath()
        # Get the relative path of the specs
        filename = os.path.basename(absolutePath)
        self.thePlugin.writeDebugInUABLog("Processing spec file $filename$:")

        # Example of how to loop through all the spec sheets counting # of objects
        allDevices = self.theUnicosProject.getAllDeviceTypes()

        for deviceType in allDevices:
            instances = deviceType.getAllDeviceTypeInstances()
            self.thePlugin.writeDebugInUABLog("contains " + str(len(instances)) + " " + deviceType.getDeviceTypeName() + " objects")

        # Write a text file in the output folder
        textContent = "This is an example of text file."
        self.thePlugin.writeFile("example.txt", textContent)
        # Write an xml file in the output folder (and verify that the content is well formed)
        xmlContent = "<root><text>This is an example of xml file.</text></root>"
        self.thePlugin.writeXmlFile("example.xml", xmlContent)

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for Example_PostProcess_Template.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for Example_PostProcess_Template.")
