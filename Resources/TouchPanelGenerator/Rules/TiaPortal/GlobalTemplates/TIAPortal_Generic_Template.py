# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from research.ch.cern.unicos.core import CoreManager  # REQUIRED
from time import strftime
from java.lang import System

import TIAPortal_GenericFunctions
reload(TIAPortal_GenericFunctions)


class Generic_Template(IUnicosTemplate, TIAPortal_GenericFunctions.GenericFunctions):
    thePlugin = 0
    theUnicosProject = 0
    theDeviceType = "generic"

    def initialize(self):
        # Get a reference to the plug-in
        self.thePlugin = APlugin.getPluginInterface()
        # Get a reference to the specs
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.thePlugin.writeInUABLog("initialize in Jython for $self.theDeviceType$.")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for $self.theDeviceType$.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for $self.theDeviceType$.")

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for $self.theDeviceType$.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for $self.theDeviceType$.")

    # Override the process method of the super class
    def process(self, *params):
        self.thePlugin.writeInUABLog("Starting processing in Jython $self.theDeviceType$ template")

        theCurrentDeviceType = params[0]
        instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()

        if self.thePlugin.getPlcManufacturer().lower().strip() == "schneider":
            self.integer = "Int"
            self.real = "Float"
        else:
            self.integer = "Word"
            self.real = "Real"

        for instance in instancesVector:
            if self.isSpareInstance(instance):
                continue

            self.processInstance(instance, params)
