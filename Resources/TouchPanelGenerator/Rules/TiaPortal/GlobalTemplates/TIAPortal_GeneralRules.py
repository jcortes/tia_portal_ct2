# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.plugins.interfaces import APlugin
from research.ch.cern.unicos.utilities import AbsolutePathBuilder
from time import strftime
from java.lang import System
import os.path

import TIAPortal_GenericFunctions
reload(TIAPortal_GenericFunctions)
from TIAPortal_GenericFunctions import *


class ApplicationGeneral_Template(IUnicosTemplate, TIAPortal_GenericFunctions.GenericFunctions):
    thePlugin = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("Application Generation rules: initialize")

    def check(self):
        self.thePlugin.writeInUABLog("Application Generation rules: check")

    def begin(self):
        self.thePlugin.writeInUABLog("Application Generation rules: begin")
        self.dateAndTime = strftime("%Y-%m-%d %H:%M:%S")
        self.theUserName = System.getProperty("user.name")
        # Plugin Information
        self.thePluginId = self.thePlugin.getId()
        self.thePluginVersion = self.thePlugin.getVersionId()
        # Application information
        self.theUniqueId = self.thePlugin.getApplicationUniqueID()
        self.theApplicationVersion = self.thePlugin.getApplicationVersion()
        self.outputFolder = AbsolutePathBuilder.getTechnicalPathParameter(self.thePlugin.getId() + ":OutputParameters:OutputFolder")

    def process(self, *params):
        self.thePlugin.writeInUABLog("Application Generation rules: processApplicationData")

        # tags header
        self.printScriptTag({'name': "Name",  'path': "Path", 'connection': "Connection", 'plcTag': "PLC tag", 'dataType': "DataType", 'length': "Length", 'address': "Address",
                             'accessMethod': "Access Method", 'indirectAddressing': "Indirect addressing", 'indexTag': "Index tag", 'startValue': "Start value", 'idTag': "ID tag",
                             'coding': "Coding", 'displayName': "Display name [en-US]", 'comment': "Comment [en-US]", 'acquisitionMode': "Acquisition mode", 'acquisitionCycle': "Acquisition cycle",
                             'rangeMaximumType': "Range Maximum Type", 'rangeMaximum': "Range Maximum", 'rangeMinimumType': "Range Minimum Type", 'rangeMinimum': "Range Minimum",
                             'linearScaling': "Linear scaling", 'endValuePLC': "End value PLC", 'startValuePLC': "Start value PLC", 'endValueHMI': "End value HMI",
                             'startValueHMI': "Start value HMI", 'gmpRelevant': "Gmp relevant", 'confirmationType': "Confirmation Type", 'mandatoryCommenting': "Mandatory Commenting"})

        # put two general tags for the security at the beginning of the tags file
        self.writeScriptTag(None, 'userName', 'WString', {'name': 'userName', 'path': 'SecuritySettings'})
        self.writeScriptTag(None, 'userLogged', 'UInt', {'name': 'userLogged', 'path': 'SecuritySettings'})

        # script - smart tags header
        self.writeScriptInstance("'$self.thePluginId$ Version: $self.thePluginVersion$ - $self.theUserName$-$self.dateAndTime$")
        self.writeScriptInstance("'UniqueID: $self.theUniqueId$  FileVersion: $self.theApplicationVersion$")
        self.writeScriptInstance("'Target location: " + os.path.join(self.outputFolder))
        self.writeScriptInstance("'------------------------------------------")
        self.writeScriptInstance("'Generated Objects:")

        # alarm
        self.writeAlarmInstance("DiscreteAlarms", "ID,Name,Alarm text [en-US]\, Alarm text,FieldInfo [Alarm text],Class,Trigger tag,Trigger bit,Acknowledgement tag,Acknowledgement bit,PLC acknowledgement tag,PLC acknowledgement bit,Group,Report,Info text [en-US], Info text")

        # textLists
        self.writeTextlistInstance("TextList", "Name,ListRange,Comment [en-US]")
        self.writeTextlistInstance("TextListEntry", "Name,Parent,DefaultEntry,Value,Text [en-US],FieldInfos")

        # securitySettingsScript
        self.writeSecuritySettingsInstance("'$self.thePluginId$ Version: $self.thePluginVersion$ - $self.theUserName$-$self.dateAndTime$")
        self.writeSecuritySettingsInstance("'UniqueID: $self.theUniqueId$  FileVersion: $self.theApplicationVersion$")
        self.writeSecuritySettingsInstance("'Target location: " + os.path.join(self.outputFolder))
        self.writeSecuritySettingsInstance("'------------------------------------------")
        self.writeSecuritySettingsInstance("If  SmartTags(\"userName\") = \"\" Then")
        self.writeSecuritySettingsInstance("  SmartTags(\"userLogged\")=0")
        self.writeSecuritySettingsInstance("Else")
        self.writeSecuritySettingsInstance("  SmartTags(\"userLogged\")=1")
        self.writeSecuritySettingsInstance("End If")

    def end(self):
        self.thePlugin.writeInUABLog("Application Generation rules: end")

    def shutdown(self):
        self.thePlugin.writeInUABLog("Application Generation rules: shutdown")
