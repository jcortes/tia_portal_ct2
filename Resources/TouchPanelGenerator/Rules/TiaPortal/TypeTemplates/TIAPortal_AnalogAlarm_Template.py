# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for AnalogAlarm Objects.

import TIAPortal_Generic_Template
reload(TIAPortal_Generic_Template)


class AnalogAlarm_Template(TIAPortal_Generic_Template.Generic_Template):
    theDeviceType = "AnalogAlarm"

    def processInstance(self, instance, params):
        self.writeScriptTag(instance, 'visible', 'Bool')
        self.writeScriptTag(instance, 'objName', 'WString')
        self.writeScriptTag(instance, 'description', 'WString')
        self.writeScriptTag(instance, 'StsReg01', self.integer)
        self.writeScriptTag(instance, 'StsReg02', self.integer)
        self.writeScriptTag(instance, 'ManReg01', self.integer)

        self.writeSmartTagName(instance)
        self.writeSmartTagDescription(instance)

        self.writeAlarmInfo(instance)
