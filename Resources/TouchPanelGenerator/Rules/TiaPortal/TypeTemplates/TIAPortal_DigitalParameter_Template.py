# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for DigitalParameter Objects.

import TIAPortal_Generic_Template
reload(TIAPortal_Generic_Template)


class DigitalParameter_Template(TIAPortal_Generic_Template.Generic_Template):
    theDeviceType = "DigitalParameter"

    def processInstance(self, instance, params):
        self.writeScriptTag(instance, 'visible', 'Bool')
        self.writeScriptTag(instance, 'objName', 'WString')
        self.writeScriptTag(instance, 'setValue', 'Bool')
        self.writeScriptTag(instance, 'StsReg01', self.integer)
        self.writeScriptTag(instance, 'ManReg01', self.integer)

        self.writeSmartTagName(instance)
