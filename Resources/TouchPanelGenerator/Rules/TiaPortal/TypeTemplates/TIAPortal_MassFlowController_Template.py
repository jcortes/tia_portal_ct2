# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Analog Objects.

import TIAPortal_Generic_Template
reload(TIAPortal_Generic_Template)


class Analog_Template(TIAPortal_Generic_Template.Generic_Template):
    theDeviceType = "Analog"

    def processInstance(self, instance, params):
        self.writeScriptTag(instance, 'visible', 'Bool')
        self.writeScriptTag(instance, 'setParam', 'Bool')
        self.writeScriptTag(instance, 'objName', 'WString')
        self.writeScriptTag(instance, 'StsReg01', self.integer)
        self.writeScriptTag(instance, 'StsReg02', self.integer)
        self.writeScriptTag(instance, 'ManReg01', self.integer)
        self.writeScriptTag(instance, 'PosSt', self.real)
        self.writeScriptTag(instance, 'VolTSt', self.real)
        self.writeScriptTag(instance, 'AuVoTOfSt', self.real)
        self.writeScriptTag(instance, 'MaxFlowSt', self.real)

        self.writeSmartTagName(instance)
