# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for AnalogStatus Objects.

import TIAPortal_Generic_Template
reload(TIAPortal_Generic_Template)


class AnalogStatus_Template(TIAPortal_Generic_Template.Generic_Template):
    theDeviceType = "AnalogStatus"

    def processInstance(self, instance, params):
        self.writeScriptTag(instance, 'unit', 'WString')
        self.writeScriptTag(instance, 'PosSt', self.real)

        self.writeSmartTagUnit(instance)
