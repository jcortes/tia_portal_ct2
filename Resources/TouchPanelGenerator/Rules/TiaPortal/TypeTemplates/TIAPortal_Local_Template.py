# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Local Objects.

import TIAPortal_Generic_Template
reload(TIAPortal_Generic_Template)


class Local_Template(TIAPortal_Generic_Template.Generic_Template):
    theDeviceType = "Local"

    def processInstance(self, instance, params):
        self.writeScriptTag(instance, 'visible', 'Bool')
        self.writeScriptTag(instance, 'objName', 'WString')
        self.writeScriptTag(instance, 'StsReg01', self.integer)

        self.writeSmartTagName(instance)
