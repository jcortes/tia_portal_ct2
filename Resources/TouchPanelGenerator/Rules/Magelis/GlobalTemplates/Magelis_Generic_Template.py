# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from research.ch.cern.unicos.core import CoreManager  # REQUIRED
from time import strftime
from java.lang import System

import Magelis_GenericFunctions
reload(Magelis_GenericFunctions)
from Magelis_GenericFunctions import *


class Generic_Template(IUnicosTemplate, Magelis_GenericFunctions.GenericFunctions):
    thePlugin = 0
    theUnicosProject = 0
    theDeviceType = "generic"

    def initialize(self):
        # Get a reference to the plug-in
        self.thePlugin = APlugin.getPluginInterface()
        # Get a reference to the specs
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.thePlugin.writeInUABLog("initialize in Jython for $self.theDeviceType$.")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for $self.theDeviceType$.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for $self.theDeviceType$.")

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for $self.theDeviceType$.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for $self.theDeviceType$.")

    # Override the process method of the super class
    def process(self, *params):
        self.thePlugin.writeInUABLog("Starting processing in Jython $self.theDeviceType$ template")

        theCurrentDeviceType = params[0]
        instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()

        for instance in instancesVector:
            if self.isSpareInstance(instance):
                continue

            name = instance.getAttributeData("DeviceIdentification:Name")
            self.writeInstanceInfo(2, "<Folder name=\"$name$\">")
            self.processInstance(instance, params)
            self.writeInstanceInfo(2, "</Folder>")

    def getUnit(self, instance):
        return instance.getAttributeData("SCADADeviceGraphics:Unit")

    def getFormat(self, instance):
        return instance.getAttributeData("SCADADeviceGraphics:Format")
