# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for AnalogInputReal Objects.

import Magelis_AnalogInput_Template
reload(Magelis_AnalogInput_Template)


class AnalogInputReal_Template(Magelis_AnalogInput_Template.AnalogInput_Template):
    theDeviceType = "AnalogInputReal"
