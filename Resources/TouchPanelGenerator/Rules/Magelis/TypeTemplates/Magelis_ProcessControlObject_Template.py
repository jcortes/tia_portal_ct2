# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for ProcessControlObject Objects.

import Magelis_Generic_Template
reload(Magelis_Generic_Template)


class ProcessControlObject_Template(Magelis_Generic_Template.Generic_Template):
    theDeviceType = "ProcessControlObject"

    def processInstance(self, instance, params):
        self.writeVariable(instance, "ManReg01", "UINT")
        self.writeVariable(instance, "MOpMoR", "REAL")
        self.writeVariable(instance, "StsReg01", "UINT")
        self.writeVariable(instance, "StsReg02", "UINT")
        self.writeVariable(instance, "AuOpMoSt", "REAL")
        self.writeVariable(instance, "OpMoSt", "REAL")
        self.writeVariable(instance, "Name", "STRING")
        self.writeVariable(instance, "Description", "STRING")
        self.writeVariable(instance, "DisplayName", "STRING")

        self.writeInstanceInfo(2, '''<Variable name="OptionModeLabel">
                                     <ArrayType elementType="STRING">
                                         <NoOfBytes>15</NoOfBytes>
                                         <ArrayDimension size="9"/>
                                     </ArrayType>
                                     <Source>Internal</Source>''')
        for i in range(1, 9):
            optionModeLabel = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode " + str(i) + " Label").strip()
            self.writeInstanceInfo(2, '''<Variable name="[$i$]">
                                           <InitialValue>$optionModeLabel$</InitialValue>
                                           <Sharing>Read/Write</Sharing>
                                         </Variable>''')
        self.writeInstanceInfo(2, "</Variable>")

        self.writeInstanceInfo(2, '''<Variable name="OptionModeAllowance">
                                      <ArrayType elementType="STRING">
                                        <NoOfBytes>15</NoOfBytes>
                                        <ArrayDimension size="9"/>
                                      </ArrayType>
                                     <Source>Internal</Source>''')
        for i in range(1, 9):
            optionModeAllowance = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode " + str(i) + " Allowance").strip()
            self.writeInstanceInfo(2, '''<Variable name="[$i$]">
                                           <InitialValue>$optionModeAllowance$</InitialValue>
                                           <Sharing>Read/Write</Sharing>
                                         </Variable>''')
        self.writeInstanceInfo(2, "</Variable>")
