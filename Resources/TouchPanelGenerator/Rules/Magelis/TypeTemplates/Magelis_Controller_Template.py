# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Controller Objects.

import Magelis_Generic_Template
reload(Magelis_Generic_Template)


class Controller_Template(Magelis_Generic_Template.Generic_Template):
    theDeviceType = "Controller"

    def processInstance(self, instance, params):
        self.writeVariable(instance, "ManReg01", "UINT")
        self.writeVariable(instance, "ManReg02", "UINT")
        self.writeVariable(instance, "MPosR", "REAL")
        self.writeVariable(instance, "MSP", "REAL")
        self.writeVariable(instance, "StsReg01", "UINT")
        self.writeVariable(instance, "StsReg02", "UINT")
        self.writeVariable(instance, "ActSP", "REAL")
        self.writeVariable(instance, "ActTi", "REAL")
        self.writeVariable(instance, "ActKc", "REAL")
        self.writeVariable(instance, "ActTd", "REAL")
        self.writeVariable(instance, "ActTds", "REAL")
        self.writeVariable(instance, "MV", "REAL")
        self.writeVariable(instance, "Name", "STRING")
        self.writeVariable(instance, "Description", "STRING")
