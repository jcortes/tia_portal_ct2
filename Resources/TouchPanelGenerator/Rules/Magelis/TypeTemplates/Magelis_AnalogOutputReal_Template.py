# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for AnalogOutputReal Objects.

import Magelis_AnalogOutput_Template
reload(Magelis_AnalogOutput_Template)


class AnalogOutputReal_Template(Magelis_AnalogOutput_Template.AnalogOutput_Template):
    theDeviceType = "AnalogOutputReal"
