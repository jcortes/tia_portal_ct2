# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Local Objects.

import Magelis_Generic_Template
reload(Magelis_Generic_Template)


class Local_Template(Magelis_Generic_Template.Generic_Template):
    theDeviceType = "Local"

    def processInstance(self, instance, params):
        self.writeVariable(instance, "StsReg01", "UINT")
        self.writeVariable(instance, "Name", "STRING")
        self.writeVariable(instance, "Description", "STRING")
