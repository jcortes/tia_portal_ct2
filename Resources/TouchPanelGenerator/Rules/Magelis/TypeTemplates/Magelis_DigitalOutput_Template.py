# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for DigitalOutput Objects.

import Magelis_Generic_Template
reload(Magelis_Generic_Template)


class DigitalOutput_Template(Magelis_Generic_Template.Generic_Template):
    theDeviceType = "DigitalOutput"

    def processInstance(self, instance, params):
        self.writeVariable(instance, "ManReg01", "UINT")
        self.writeVariable(instance, "StsReg01", "UINT")
        self.writeVariable(instance, "Name", "STRING")
        self.writeVariable(instance, "Description", "STRING")
