# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$

import ucpc_library.shared_generic_functions
reload(ucpc_library.shared_generic_functions)

NO_VALUE = ""


class GenericFunctions(ucpc_library.shared_generic_functions.SharedGenericFunctions):

    def writeWarning(self, instance, text):
        if instance == None:
            self.thePlugin.writeWarningInUABLog(text)
        else:
            alias = instance.getAttributeData("DeviceIdentification:Name")
            self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $alias$. " + text)

    def writeError(self, instance, text):
        if instance == None:
            self.thePlugin.writeErrorInUABLog(text)
        else:
            alias = instance.getAttributeData("DeviceIdentification:Name")
            self.thePlugin.writeErrorInUABLog("$self.theDeviceType$ instance: $alias$. " + text)

    def isSpareInstance(self, instance):
        """ Return true for spare instance. """
        description = instance.getAttributeData("DeviceDocumentation:Description")
        return description.strip().lower() == "spare"

    def computeAddress(self, instance, pinName):
        """ Function calculates an address name based on instance name and pin name and calls thePlugin.computeAddress """
        fullName = instance.getAttributeData("DeviceIdentification:Name") + "_" + pinName
        return str(self.thePlugin.computeAddress(fullName))

    def writeScriptTag(self, instance, tag, dataType, props={}):
        """ Calculate tag properties and write tag to the output file.
        If tag starts with capital than tag's address will be calculated.
        Any property could be overwritten using props attribute"""
        if tag != "" and tag[0].isupper():
            address = self.computeAddress(instance, tag)
        else:
            address = NO_VALUE
        defaultValues = {
            'address': address,
            'dataType': dataType,
            'arrayCount': "1",
            'acquisitionMode': "2",
            'acquisitionCycle': "1 s",
            'upperLimit': NO_VALUE,
            'additionalUpperLimit': NO_VALUE,
            'additionalLowerLimit': NO_VALUE,
            'lowerLimit': NO_VALUE,
            'linearScaling': '0',
            'upperPLCscalingValue': '10',
            'lowerPLCscalingValue': '0',
            'upperHMIscalingValue': '100',
            'lowerHMIscalingValue': '0',
            'startValue': NO_VALUE,
            'updateID': '0',
            'comment': NO_VALUE
        }
        values = dict(defaultValues.items() + props.items())

        if 'name' not in values:
            if instance.getAttributeData("DeviceIdentification:Expert Name"):
                nameAlias = instance.getDeviceTypeName() + "/" + instance.getAttributeData("DeviceIdentification:Expert Name")
            else:
                nameAlias = instance.getDeviceTypeName() + "/" + instance.getAttributeData("DeviceIdentification:Name")
            values['name'] = nameAlias + "." + tag
        if 'length' not in values:
            values['length'] = self.getDataLength(instance, tag, values['dataType'])
        if 'connection' not in values:
            values['connection'] = "" if values['address'] == NO_VALUE else self.getPLCName()

        self.printScriptTag(values)

    def printScriptTag(self, values):
        """ Write down a tag following format. """
        format = ['name', 'connection', 'address', 'dataType', 'length', 'arrayCount', 'acquisitionMode', 'acquisitionCycle', 'upperLimit', 'additionalUpperLimit',
                  'additionalLowerLimit', 'lowerLimit', 'linearScaling', 'upperPLCscalingValue', 'lowerPLCscalingValue', 'upperHMIscalingValue', 'lowerHMIscalingValue', 'startValue', 'updateID', 'comment']

        output = []
        for key in format:
            if key in values:
                output.append(values[key])
            else:
                self.writeWarning(None, "No value defined for key: " + key)
                output.addend(NO_VALUE)

        self.writeTagInstance(3, "\t".join(output))

    def getDataLength(self, instance, name, dataType):
        """ Calculate data length for given data type. """
        stringTypes = {'userName': '10', 'unit': '10', 'name': '40', 'description': '100', 'displayName': '8'}
        if dataType == 'String':
            if name in stringTypes:
                return stringTypes[name]
        else:
            return NO_VALUE
        self.writeError(instance, "The data type is not supported: " + dataType + ' of ' + name)
        return NO_VALUE

    def writeSmartTagName(self, instance):
        """ Write down smart tag for name. """
        if instance.getAttributeData("DeviceIdentification:Expert Name"):
            name = instance.getAttributeData("DeviceIdentification:Expert Name")
        else:
            name = instance.getAttributeData("DeviceIdentification:Name")
        self.writeScriptInstance(3, "SmartTags(\"$instance.getDeviceTypeName()$\\$name$.name\") = \"$name$\"")

    def writeSmartTagDisplayName(self, instance):
        """ Write down smart tag for display name. """
        if instance.getAttributeData("DeviceIdentification:Expert Name"):
            name = instance.getAttributeData("DeviceIdentification:Expert Name")
        else:
            name = instance.getAttributeData("DeviceIdentification:Name")
        if instance.getAttributeData("SCADADeviceGraphics:Display Name"):
            displayName = instance.getAttributeData("SCADADeviceGraphics:Display Name")
        else:
            displayName = instance.getAttributeData("DeviceIdentification:Name")
        self.writeScriptInstance(3, "SmartTags(\"$instance.getDeviceTypeName()$\\$name$.displayName\") = \"$displayName$\"")

    def writeSmartTagUnit(self, instance):
        """ Write down smart tag for unit. """
        if instance.getAttributeData("DeviceIdentification:Expert Name"):
            name = instance.getAttributeData("DeviceIdentification:Expert Name")
        else:
            name = instance.getAttributeData("DeviceIdentification:Name")
        unit = instance.getAttributeData("SCADADeviceGraphics:Unit")
        unit = unit.replace(" ", "").replace("\"", "\"\"")
        self.writeScriptInstance(3, "SmartTags(\"$instance.getDeviceTypeName()$\\$name$.unit\") = \"$unit$\"")

    def writeSmartTagDescription(self, instance):
        """ Write down smart tag for description. """
        if instance.getAttributeData("DeviceIdentification:Expert Name"):
            name = instance.getAttributeData("DeviceIdentification:Expert Name")
        else:
            name = instance.getAttributeData("DeviceIdentification:Name")
        description = instance.getAttributeData("DeviceDocumentation:Description").replace("\"", "\"\"")
        self.writeScriptInstance(3, "SmartTags(\"$instance.getDeviceTypeName()$\\$name$.description\") = \"$description$\"")

    def writeAlarmInfo(self, instance):
        """ Retrieve and write down alarm info. """
        if instance.getAttributeData("DeviceIdentification:Expert Name"):
            name = instance.getAttributeData("DeviceIdentification:Expert Name")
        else:
            name = instance.getAttributeData("DeviceIdentification:Name")
        deviceNumber = int(instance.getInstanceNumber())
        deviceTypeName = instance.getDeviceTypeName()
        description = instance.getAttributeData("DeviceDocumentation:Description")
        if instance.getDeviceTypeName() == "AnalogAlarm":
            deviceNumber += int(self.thePlugin.getUnicosProject().getDeviceType("DigitalAlarm").getAllDeviceTypeInstances().size())
        deviceNumber = str(deviceNumber)
        self.writeAlarmInstance(3, "\"D\"\t\"$deviceNumber$\"\t\"Events\"\t\"$deviceTypeName$/$name$.stsReg01\"\t\"0\"\t\t\t\t\t\t\"0\"\t\"en-US=$description$\"\t\t\"en-US=\"")

    def writeTextListPattern(self, instance):
        """ Retrieve and write down WS pattern. """
        if instance.getAttributeData("DeviceIdentification:Expert Name"):
            name = instance.getAttributeData("DeviceIdentification:Expert Name")
        else:
            name = instance.getAttributeData("DeviceIdentification:Name")
        pattern = instance.getAttributeData("SCADADeviceGraphics:Pattern")
        widgetType = instance.getAttributeData("SCADADeviceGraphics:Widget Type")
        if "Bit" in widgetType:
            self.writeWarning(instance, "skip text list generation for WSBit type.")
            return
        if pattern in ["", "-"]:
            return
        self.writeTextlistInstance(3, "$name$\tR\t\t\t")
        for pair in pattern.split(","):
            key, value = pair.split("=")
            self.writeTextlistInstance(3, "$name$\t\t\t" + str(key) + "\ten-US=" + value + "")

    tag_filename = 0

    def writeTagInstance(self, section, data):
        if self.tag_filename == 0:
            self.tag_filename = self.thePlugin.getPluginParameter("OutputParameters:WinCCFlex:TagsFileName")
        self.thePlugin.writeInstanceInfo(self.tag_filename, section, data)

    script_filename = 0

    def writeScriptInstance(self, section, data):
        if self.script_filename == 0:
            self.script_filename = self.thePlugin.getPluginParameter("OutputParameters:WinCCFlex:ScriptFileName")
        self.thePlugin.writeInstanceInfo(self.script_filename, section, data)

    alarm_filename = 0

    def writeAlarmInstance(self, section, data):
        if self.alarm_filename == 0:
            self.alarm_filename = self.thePlugin.getPluginParameter("OutputParameters:WinCCFlex:AlarmFileName")
        self.thePlugin.writeInstanceInfo(self.alarm_filename, section, data)

    textlist_filename = 0

    def writeTextlistInstance(self, section, data):
        if self.textlist_filename == 0:
            self.textlist_filename = self.thePlugin.getPluginParameter("OutputParameters:WinCCFlex:TextListsFileName")
        self.thePlugin.writeInstanceInfo(self.textlist_filename, section, data)

    security_settings_filename = 0

    def writeSecuritySettingsInstance(self, section, data):
        if self.security_settings_filename == 0:
            self.security_settings_filename = self.thePlugin.getPluginParameter("OutputParameters:WinCCFlex:SecuritySettingsScriptFileName")
        self.thePlugin.writeInstanceInfo(self.security_settings_filename, section, data)
