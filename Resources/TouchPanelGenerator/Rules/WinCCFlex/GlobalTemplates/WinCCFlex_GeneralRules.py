# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.plugins.interfaces import APlugin
from research.ch.cern.unicos.utilities import AbsolutePathBuilder
from time import strftime
from java.lang import System
import os.path

import WinCCFlex_GenericFunctions
reload(WinCCFlex_GenericFunctions)
from WinCCFlex_GenericFunctions import *


class ApplicationGeneral_Template(IUnicosTemplate, WinCCFlex_GenericFunctions.GenericFunctions):
    thePlugin = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("Application Generation rules: initialize")

    def check(self):
        self.thePlugin.writeInUABLog("Application Generation rules: check")

    def begin(self):
        self.thePlugin.writeInUABLog("Application Generation rules: begin")
        self.dateAndTime = strftime("%Y-%m-%d %H:%M:%S")
        self.theUserName = System.getProperty("user.name")
        # Plugin Information
        self.thePluginId = self.thePlugin.getId()
        self.thePluginVersion = self.thePlugin.getVersionId()
        # Application information
        self.theUniqueId = self.thePlugin.getApplicationUniqueID()
        self.theApplicationVersion = self.thePlugin.getApplicationVersion()
        self.outputFolder = AbsolutePathBuilder.getTechnicalPathParameter(self.thePlugin.getId() + ":OutputParameters:OutputFolder")

    def process(self, *params):
        self.thePlugin.writeInUABLog("Application Generation rules: processApplicationData")

        # tags header
        self.writeTagInstance(1, "#$self.thePluginId$ Version: $self.thePluginVersion$ - $self.theUserName$-$self.dateAndTime$")
        self.writeTagInstance(1, "#UniqueID: $self.theUniqueId$  FileVersion: $self.theApplicationVersion$")
        self.writeTagInstance(1, "#Target location: " + os.path.join(self.outputFolder, self.thePlugin.getPluginParameter("OutputParameters:WinCCFlex:TagsFileName")))
        self.writeTagInstance(1, "#------------------------------------------")
        self.writeTagInstance(1, "#Column A: tag name (mandatory - without apostrophe)\n#Column B: connection \(if empty then internal\)\n#Column C: address (depends on the based connection)\n#Column D: data type (depends on the based connection)\n#Column E: length (necessary if data type is string)\n#Column F: array count\n#Column G: acquisition mode (1 = On Demand 2 = cyclic on use (Default) 3 = cyclic continuous)\n#Column H: acquisition cycle e.g. 1 s\n#Column I: Upper Limit (floating point number)\n#Column J: Additional Upper Limit (floating point number)\n#Column K: Additional Lower Limit (floating point number)\n#Column L: Lower Limit (floating point number)\n#Column M: Linear scaling (0 or false  1 or true)\n#Column N: Upper PLC scaling value (floating point number)\n#Column O: Lower PLC scaling value (floating point number)\n#Column P: Upper HMI scaling value (floating point number)\n#Column Q: Lower HMI scaling value (floating point number)\n#Column R: start value (type depends on the based data type)\n#Column S: UpdateID\n#Column T: Comment (max. 500 characters)")
        self.writeTagInstance(1, "#------------------------------------------")
        # put two general tags for the security at the beginning of the tags file
        self.writeTagInstance(1, "#Security Settings:")
        self.writeScriptTag(None, 'userName', 'String', {'name': 'SecuritySettings/userName'})
        self.writeScriptTag(None, 'userLogged', 'Int', {'name': 'SecuritySettings/userLogged'})
        self.writeTagInstance(1, "#Generated Objects:")

        # script - smart tags header
        self.writeScriptInstance(1, "'$self.thePluginId$ Version: $self.thePluginVersion$ - $self.theUserName$-$self.dateAndTime$")
        self.writeScriptInstance(1, "'UniqueID: $self.theUniqueId$  FileVersion: $self.theApplicationVersion$")
        self.writeScriptInstance(1, "'Target location: " + os.path.join(self.outputFolder, self.thePlugin.getPluginParameter("OutputParameters:WinCCFlex:ScriptFileName")))
        self.writeScriptInstance(1, "'------------------------------------------")
        self.writeScriptInstance(1, "'Generated Objects:")

        # alarm
        self.writeAlarmInstance(1, "//$self.thePluginId$ Version: $self.thePluginVersion$ - $self.theUserName$-$self.dateAndTime$")
        self.writeAlarmInstance(1, "//UniqueID: $self.theUniqueId$  FileVersion: $self.theApplicationVersion$")
        self.writeAlarmInstance(1, "//Target location: " + os.path.join(self.outputFolder, self.thePlugin.getPluginParameter("OutputParameters:WinCCFlex:AlarmFileName")))
        self.writeAlarmInstance(1, "//------------------------------------------")
        self.writeAlarmInstance(1, "//Alarm type\tAlarm number\tAlarm class\tTrigger tag\tTrigger bit number\tAcknowledgment HMI tag\tAcknowledgment HMI tag bit number\tAcknowledgment PLC tag\tAcknowledgment PLC tag bit number\tAlarm group\tReported\tText[en-US]\tField info[01]\tInfotext[en-US]")
        self.writeAlarmInstance(1, "//------------------------------------------")
        self.writeAlarmInstance(1, "//Generated Objects:")

        # textLists
        self.writeTextlistInstance(1, "//$self.thePluginId$ Version: $self.thePluginVersion$ - $self.theUserName$-$self.dateAndTime$")
        self.writeTextlistInstance(1, "//UniqueID: $self.theUniqueId$  FileVersion: $self.theApplicationVersion$")
        self.writeTextlistInstance(1, "//Target location: " + os.path.join(self.outputFolder, self.thePlugin.getPluginParameter("OutputParameters:WinCCFlex:TextListsFileName")))
        self.writeTextlistInstance(1, "//------------------------------------------")
        self.writeTextlistInstance(1, "//Name\tSelection\tComment\tValue\tEntry[en-US]")
        self.writeTextlistInstance(1, "//------------------------------------------")
        self.writeTextlistInstance(1, "//Generated Objects:")

        # securitySettingsScript
        self.writeSecuritySettingsInstance(1, "'$self.thePluginId$ Version: $self.thePluginVersion$ - $self.theUserName$-$self.dateAndTime$")
        self.writeSecuritySettingsInstance(1, "'UniqueID: $self.theUniqueId$  FileVersion: $self.theApplicationVersion$")
        self.writeSecuritySettingsInstance(1, "'Target location: " + os.path.join(self.outputFolder, self.thePlugin.getPluginParameter("OutputParameters:WinCCFlex:SecuritySettingsScriptFileName")))
        self.writeSecuritySettingsInstance(1, "'------------------------------------------")
        self.writeSecuritySettingsInstance(1, "If  SmartTags(\"SecuritySettings\\userName\") = \"\" Then")
        self.writeSecuritySettingsInstance(1, "  SmartTags(\"SecuritySettings\\userLogged\")=0")
        self.writeSecuritySettingsInstance(1, "Else")
        self.writeSecuritySettingsInstance(1, "  SmartTags(\"SecuritySettings\\userLogged\")=1")
        self.writeSecuritySettingsInstance(1, "End If")

    def end(self):
        self.thePlugin.writeInUABLog("Application Generation rules: end")

    def shutdown(self):
        self.thePlugin.writeInUABLog("Application Generation rules: shutdown")
