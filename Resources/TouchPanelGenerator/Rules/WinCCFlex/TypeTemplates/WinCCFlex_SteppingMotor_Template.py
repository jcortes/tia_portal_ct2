# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2016 all rights reserved
# $LastChangedRevision$
# Jython source file for SteppingMotor Objects.

import WinCCFlex_Generic_Template
reload(WinCCFlex_Generic_Template)


class SteppingMotor_Template(WinCCFlex_Generic_Template.Generic_Template):
    theDeviceType = "SteppingMotor"

    def processInstance(self, instance, params):
        self.thePlugin.writeWarningInUABLog("the object SteppingMotor has not been develop for the WinCC Flexible 2008 Platform.")
