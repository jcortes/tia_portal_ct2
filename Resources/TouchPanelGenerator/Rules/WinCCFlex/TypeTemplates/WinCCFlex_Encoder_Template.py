# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2016 all rights reserved
# $LastChangedRevision$
# Jython source file for Encoder Objects.

import WinCCFlex_Generic_Template
reload(WinCCFlex_Generic_Template)


class Encoder_Template(WinCCFlex_Generic_Template.Generic_Template):
    theDeviceType = "Encoder"

    def processInstance(self, instance, params):
        self.thePlugin.writeWarningInUABLog("the object Encoder has not been developed for the WinCC Flexible 2008 Platform.")
