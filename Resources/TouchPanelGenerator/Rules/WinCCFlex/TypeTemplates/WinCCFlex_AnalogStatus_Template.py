# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for AnalogStatus Objects.

import WinCCFlex_Generic_Template
reload(WinCCFlex_Generic_Template)


class AnalogStatus_Template(WinCCFlex_Generic_Template.Generic_Template):
    theDeviceType = "AnalogStatus"

    def processInstance(self, instance, params):
        self.writeScriptTag(instance, 'unit', 'String')
        self.writeScriptTag(instance, 'PosSt', self.real)

        self.writeSmartTagUnit(instance)
