# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Controller Objects.

import WinCCFlex_Generic_Template
reload(WinCCFlex_Generic_Template)


class Controller_Template(WinCCFlex_Generic_Template.Generic_Template):
    theDeviceType = "Controller"

    def processInstance(self, instance, params):
        self.writeScriptTag(instance, 'visible', 'Bool')
        self.writeScriptTag(instance, 'name', 'String')
        self.writeScriptTag(instance, 'setSetPoint', 'Bool')
        self.writeScriptTag(instance, 'StsReg01', self.integer)
        self.writeScriptTag(instance, 'StsReg02', self.integer)
        self.writeScriptTag(instance, 'ManReg01', self.integer)
        self.writeScriptTag(instance, 'ManReg02', self.integer)
        self.writeScriptTag(instance, 'MV', self.real)
        self.writeScriptTag(instance, 'ActSP', self.real)
        self.writeScriptTag(instance, 'ActTi', self.real)
        self.writeScriptTag(instance, 'ActKc', self.real)
        self.writeScriptTag(instance, 'ActTd', self.real)
        self.writeScriptTag(instance, 'ActTds', self.real)
        self.writeScriptTag(instance, 'MSP', self.real)

        self.writeSmartTagName(instance)
