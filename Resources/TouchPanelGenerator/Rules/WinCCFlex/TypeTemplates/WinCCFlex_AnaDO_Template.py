# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for AnaDO Objects.

import WinCCFlex_Generic_Template
reload(WinCCFlex_Generic_Template)


class AnaDO_Template(WinCCFlex_Generic_Template.Generic_Template):
    theDeviceType = "AnaDO"

    def processInstance(self, instance, params):
        self.writeScriptTag(instance, 'visible', 'Bool')
        self.writeScriptTag(instance, 'name', 'String')
        self.writeScriptTag(instance, 'setValue', 'Bool')
        self.writeScriptTag(instance, 'StsReg01', self.integer)
        self.writeScriptTag(instance, 'StsReg02', self.integer)
        self.writeScriptTag(instance, 'ManReg01', self.integer)
        self.writeScriptTag(instance, 'PosSt', self.real)
        self.writeScriptTag(instance, 'MPosR', self.real)

        self.writeSmartTagName(instance)
