# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for DigitalInput Objects.

import WinCCFlex_Generic_Template
reload(WinCCFlex_Generic_Template)


class DigitalInput_Template(WinCCFlex_Generic_Template.Generic_Template):
    theDeviceType = "DigitalInput"

    def processInstance(self, instance, params):
        self.writeScriptTag(instance, 'visible', 'Bool')
        self.writeScriptTag(instance, 'name', 'String')
        self.writeScriptTag(instance, 'StsReg01', self.integer)
        self.writeScriptTag(instance, 'ManReg01', self.integer)

        self.writeSmartTagName(instance)
