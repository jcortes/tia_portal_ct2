# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class AnalogAlarm_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        instance_amount = len(instance_list)

        params = {'object': 'AA',
                  'Communicated_blocks': '',
                  'DB_object': '',
                  'object_call': '',
                  'instance_amount': instance_amount,
                  'DB_object_ManRequest': '',
                  'FI': ''}

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")

            params['DB_object'] += '''DATA_BLOCK "$name$" "CPC_FB_AA"
BEGIN
$self.get_instance_assignment(instance, idx)$
END_DATA_BLOCK

'''

            params['object_call'] += '''    "CPC_FC_AA"(DB_AA := "$name$");''' + self.CRLF

            hh = self.thePlugin.formatNumberPLC(self.spec.get_attribute_value(
                instance, "FEDeviceManualRequests:HH Alarm", "0.0"))
            params['DB_object_ManRequest'] += "   AA_Requests[%s].HH := %s;" % (idx, hh) + self.CRLF
            h = self.thePlugin.formatNumberPLC(self.spec.get_attribute_value(
                instance, "FEDeviceManualRequests:H Warning", "0.0"))
            params['DB_object_ManRequest'] += "   AA_Requests[%s].H := %s;" % (idx, h) + self.CRLF
            l = self.thePlugin.formatNumberPLC(self.spec.get_attribute_value(
                instance, "FEDeviceManualRequests:L Warning", "0.0"))
            params['DB_object_ManRequest'] += "   AA_Requests[%s].L := %s;" % (idx, l) + self.CRLF
            ll = self.thePlugin.formatNumberPLC(self.spec.get_attribute_value(
                instance, "FEDeviceManualRequests:LL Alarm", "0.0"))
            params['DB_object_ManRequest'] += "   AA_Requests[%s].LL := %s;" % (idx, ll) + self.CRLF

        params['Communicated_blocks'] = (2 * self.CRLF).join(
            [self.process_template("TIAInst_ObjectManReq_Template.scl", params),
             self.process_template("TIAInst_ObjectEvent_Template.scl", params),
             self.process_template("TIAInst_ObjectBinStatus_Template.scl", params),
             self.process_template("TIAInst_ObjectAnaStatus_Template.scl", params)])

        self.writeDeviceInstances(self.process_template("TIAInst_Object_Template.scl", params))
        self.writeFCDeviceInstances(self.process_template("TIAInst_Object_FC_Template.scl", params))

    def get_threshold_bit(self, instance, param):
        value = instance.getAttributeData(param)
        if self.thePlugin.isString(value) or value.strip() == "":
            return '1'
        else:
            return '0'

    def get_parreg_value(self, instance):
        par_reg = ['0'] * 16
        par_reg[15] = self.get_threshold_bit(instance, "FEDeviceManualRequests:HH Alarm")
        par_reg[14] = self.get_threshold_bit(instance, "FEDeviceManualRequests:H Warning")
        par_reg[13] = self.get_threshold_bit(instance, "FEDeviceManualRequests:L Warning")
        par_reg[12] = self.get_threshold_bit(instance, "FEDeviceManualRequests:LL Alarm")
        par_reg[11] = self.spec.get_bit_from_attribute(instance, "FEDeviceAlarm:Auto Acknowledge",
                                                       on_value="true")
        return "".join(par_reg)

    @staticmethod
    def get_threshold_param(instance, param):
        value = instance.getAttributeData(param)
        if value.strip() == "":
            return 'FALSE'
        else:
            return 'TRUE'

    def get_instance_assignment(self, instance, idx):
        instance_assignment = '''    index := %(index)s;
    HHSt := %(HH)s;
    HSt := %(H)s;
    LSt := %(L)s;
    LLSt := %(LL)s;
    HH := %(HH)s;
    H := %(H)s;
    L := %(L)s;
    LL := %(LL)s;
    %(PAlDt)s
    PAA.ParReg := 2#%(PArReg)s;
    AuEHH := %(AuEHH)s;
    AuEH := %(AuEH)s;
    AuEL := %(AuEL)s;
    AuELL := %(AuELL)s;'''

        params = {
            'index': idx,
            'PArReg': self.get_parreg_value(instance),
            'AuEHH': self.get_threshold_param(instance, "FEDeviceManualRequests:HH Alarm"),
            'AuEH': self.get_threshold_param(instance, "FEDeviceManualRequests:H Warning"),
            'AuEL': self.get_threshold_param(instance, "FEDeviceManualRequests:L Warning"),
            'AuELL': self.get_threshold_param(instance, "FEDeviceManualRequests:LL Alarm"),
            'HH': self.spec.get_attribute_value(instance, "FEDeviceManualRequests:HH Alarm", "0.0"),
            'H': self.spec.get_attribute_value(instance, "FEDeviceManualRequests:H Warning", "0.0"),
            'L': self.spec.get_attribute_value(instance, "FEDeviceManualRequests:L Warning", "0.0"),
            'LL': self.spec.get_attribute_value(instance, "FEDeviceManualRequests:LL Alarm", "0.0")
        }

        delay = instance.getAttributeData("FEDeviceParameters:Alarm Delay (s)").strip()
        if delay == "":
            params['PAlDt'] = '''PAlDt := 0;'''
        elif self.thePlugin.isString(delay):
            params['PAlDt'] = '''// The Alarm Delay is defined in the logic'''
        else:
            params['PAlDt'] = '''PAlDt := %s;''' % (int(round(float(delay))))

        return instance_assignment % params
