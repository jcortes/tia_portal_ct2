# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template
from TIAInst_Analog_Template import Analog_Template
from TIAInst_AnaDO_Template import AnaDO_Template
from collections import OrderedDict
import math


class Controller_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        instance_amount = len(instance_list)

        params = {'object': 'PID',
                  'Communicated_blocks': '',
                  'DB_object': '',
                  'instance_amount': instance_amount,
                  'DB_object_ManRequest': '',
                  'FI': ''}

        params['DB_object'] += '''// INIT of PARAMETERS DONE in the DB creation
// DB scheduler to manage the PID sampling times

DATA_BLOCK "DB_SCHED"
{ S7_Optimized_Access := 'TRUE' }
   STRUCT 
      GLP_NBR : Int;   // greatest loop number
      ALP_NBR : Int;   // actual loop number
      LOOP_DAT : Array  [1 .. %(GLP_NBR)s] of STRUCT 
         MAN_CYC : Time;   // loop data: manual sample time
         MAN_DIS : Bool;   // loop data: manual loop disable
         MAN_CRST : Bool;  // loop data: manual complete restart
         ENABLE : Bool;   // loop data: enable loop
         COM_RST : Bool;   // loop data: complete restart
         ILP_COU : Int;   // loop data: internal loop counter
         CYCLE : Time;   // loop data: sample time
      END_STRUCT;
   END_STRUCT;


BEGIN
    GLP_NBR := %(GLP_NBR)s;
    ALP_NBR := 0;
%(LOOP_DAT)s
END_DATA_BLOCK

''' % {'GLP_NBR': self.thePlugin.getGroupMaxNb(), 'LOOP_DAT': self.get_db_sched(instance_list)}

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")

            params['DB_object'] += '''DATA_BLOCK "$name$" "CPC_FB_PID"
BEGIN
$self.get_instance_assignment(instance, idx)$
END_DATA_BLOCK

'''

        params['Communicated_blocks'] = (2 * self.CRLF).join(
            [self.process_template("TIAInst_ObjectManReq_Template.scl", params),
             self.process_template("TIAInst_ObjectEvent_Template.scl", params),
             self.process_template("TIAInst_ObjectBinStatus_Template.scl", params),
             self.process_template("TIAInst_ObjectAnaStatus_Template.scl", params)])

        # TODO move to the semantic check
        self.check_scaling_consistency(self.thePlugin, instance_list)
        self.writeDeviceInstances(self.process_template("TIAInst_LogicObject_Template.scl", params))

    def get_db_sched(self, instance_list):
        # TODO keep it here or move back to the plugin?
        max_group_amount = 10.0
        max_devices_per_group = int(math.floor(len(instance_list) / max_group_amount + 0.5)) + 1
        cycles = []
        for instance in instance_list:
            pid_cycles = instance.getAttributeData("FEDeviceParameters:Controller Parameters:PID Cycle (s)")
            if pid_cycles == "":
                self.thePlugin.writeErrorInUABLog("PID group is not assigned.")
            else:
                cycles.append(float(pid_cycles))
        cycles.sort()
        groups = OrderedDict()
        curr = cycles[0]
        prev = cycles[0]
        current_group_number = 1
        amount = 1
        for curr in cycles[1:]:
            amount += 1
            if amount > max_devices_per_group or curr != prev:
                groups[current_group_number] = [prev, amount]
                amount = 1
                current_group_number += 1
            prev = curr
        groups[current_group_number] = [curr, amount]

        result = ""
        for idx in groups:
            result += '''	#LOOP_DAT[%(group)s].MAN_CYC := T#%(tsamp)ss;   // Group: %(group)s; Components: %(max)s PIDs''' % \
                      {'group': idx, 'tsamp': groups[idx][0], 'max': max_devices_per_group} + self.CRLF
        return result

    @staticmethod
    def check_scaling_consistency(plugin, instance_list):
        scaling_method_previous = None
        for instance in instance_list:
            scaling_method_current = instance.getAttributeData(
                "FEDeviceParameters:Controller Parameters:Scaling Method")
            if scaling_method_previous and scaling_method_previous != scaling_method_current:
                plugin.writeWarningInUABLog(
                    "PID instances: All the Controller instances don't have the same Scaling Method")
                return
            scaling_method_previous = scaling_method_current

    def get_range(self, instance):
        max_range = "100.0"
        min_range = "0.0"
        measured_value = instance.getAttributeData("FEDeviceEnvironmentInputs:Measured Value")
        if measured_value != '':
            unicos_project = self.thePlugin.getUnicosProject()
            linked_devices = unicos_project.findMatchingInstances(
                "AnalogInput, AnalogInputReal, AnalogStatus", "'#DeviceIdentification:Name#'='%s'" % measured_value)
            if len(linked_devices) > 0:
                min_range = self.spec.get_attribute_value(linked_devices[0], "FEDeviceParameters:Range Min")
                max_range = self.spec.get_attribute_value(linked_devices[0], "FEDeviceParameters:Range Max")

        return min_range, max_range

    def get_output_range(self, instance):
        max_range = self.spec.get_attribute_value(instance, "FEDeviceParameters:Controller Parameters:Output Range Max")
        min_range = self.spec.get_attribute_value(instance, "FEDeviceParameters:Controller Parameters:Output Range Min")
        if max_range != "":  # range is explicitly defined in the spec
            return min_range, max_range

        controlled_object = instance.getAttributeData("FEDeviceOutputs:Controlled Objects").split(",")[0]
        if controlled_object == "":
            max_range = "100.0"
            min_range = "0.0"
            self.logger.write_warning(
                instance, "The absence of ControlledObjects forces the POutMaxRan to (%s) and the POutMinRan to (%s)." %
                          (max_range, min_range))
            return min_range, max_range

        unicos_project = self.thePlugin.getUnicosProject()
        linked_devices = unicos_project.findMatchingInstances(
            "Analog, AnalogDigital, AnaDO, Controller", "'#DeviceIdentification:Name#'='%s'" % controlled_object)
        if len(linked_devices) > 0:
            controlled_object = linked_devices[0]
            controlled_object_type = linked_devices[0].getDeviceTypeName()
            if controlled_object_type == "Analog":
                min_range, max_range = Analog_Template.get_range(self.thePlugin, controlled_object)
            elif controlled_object_type == "AnaDO":
                min_range, max_range = AnaDO_Template.get_range(self.thePlugin, controlled_object)
            elif controlled_object_type == "AnalogDigital":
                max_range = self.spec.get_attribute_value(controlled_object, "FEDeviceParameters:Range Max")
                min_range = self.spec.get_attribute_value(controlled_object, "FEDeviceParameters:Range Min")
            elif controlled_object_type == "Controller":
                min_range, max_range = self.get_range(controlled_object)
                if max_range == "100.0" and min_range == "0.0":
                    self.logger.write_warning(
                        instance, "The ControlledObject " + controlled_object
                                  + " cannot be a controlled object. POutMaxRan has been forced to (" + max_range
                                  + ") and POutMinRan has been forced to (" + min_range + ")")
        return min_range, max_range

    def get_instance_assignment(self, instance, idx):
        output = '''   index := %(index)s;
   AuSPSpd.InSpd := %(inspd)s;
   AuSPSpd.DeSpd := %(despd)s;
   PControl.PMaxRan := %(pmaxran)s;
   PControl.PMinRan := %(pminran)s;
   PControl.POutMaxRan := %(poutmaxran)s;
   PControl.POutMinRan := %(poutminran)s;
   PControl.MVFiltTime := T#%(mvfilttime)ss;
   PControl.PIDCycle := T#%(pidcycle)ss;
   PControl.ScaMethod := %(scamethod)s;
   PControl.RA := %(ra)s;
   DefKc := %(defkc)s;
   DefTi := %(defti)s;
   DefTd := %(deftd)s;
   DefTds := %(deftds)s;
   DefSP := %(defsp)s;
   DefSPH := %(defsph)s;
   DefSPL := %(defspl)s;
   DefOutH := %(defouth)s;
   DefOutL := %(defoutl)s;'''
        min_range, max_range = self.get_range(instance)
        min_output_range, max_output_range = self.get_output_range(instance)
        scaling_method_map = {
            "": '',
            "Input Scaling": '1',
            "Input/Output Scaling": '2',
            "No Scaling": '3'
        }
        params = {
            "name": instance.getAttributeData("DeviceIdentification:Name"),
            "index": idx,
            "pmaxran": max_range,
            "pminran": min_range,
            "poutmaxran": max_output_range,
            "poutminran": min_output_range,
            "mvfilttime": self.spec.get_attribute_value(
                instance, "FEDeviceParameters:Controller Parameters:MV Filter Time (s)"),
            "pidcycle": instance.getAttributeData("FEDeviceParameters:Controller Parameters:PID Cycle (s)"),
            "scamethod": scaling_method_map[instance.getAttributeData(
                "FEDeviceParameters:Controller Parameters:Scaling Method")],
            "ra": instance.getAttributeData("FEDeviceParameters:Controller Parameters:RA"),
            "inspd": self.spec.get_attribute_value(
                instance, "FEDeviceAutoRequests:Default Setpoint Speed:Increase Speed"),
            "despd": self.spec.get_attribute_value(
                instance, "FEDeviceAutoRequests:Default Setpoint Speed:Decrease Speed"),
            "defkc": self.spec.get_attribute_value(
                instance, "FEDeviceVariables:Default PID Parameters:Kc", "1.0"),
            "defti": self.spec.get_attribute_value(
                instance, "FEDeviceVariables:Default PID Parameters:Ti", '100.0'),
            "deftd": self.spec.get_attribute_value(
                instance, "FEDeviceVariables:Default PID Parameters:Td", '0.0'),
            "deftds": self.spec.get_attribute_value(
                instance, "FEDeviceVariables:Default PID Parameters:Tds", '0.0'),
            "defsp": self.spec.get_attribute_value(
                instance, "FEDeviceVariables:Default PID Parameters:Setpoint", '0.0'),
            "defsph": self.spec.get_attribute_value(
                instance, "FEDeviceVariables:Default PID Parameters:SP High Limit", max_range),
            "defspl": self.spec.get_attribute_value(
                instance, "FEDeviceVariables:Default PID Parameters:SP Low Limit", min_range),
            "defouth": self.spec.get_attribute_value(
                instance, "FEDeviceVariables:Default PID Parameters:Out High Limit", max_output_range),
            "defoutl": self.spec.get_attribute_value(
                instance, "FEDeviceVariables:Default PID Parameters:Out Low Limit", min_output_range)
        }
        return output % params
