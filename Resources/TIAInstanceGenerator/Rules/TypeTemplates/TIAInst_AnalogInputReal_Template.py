# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class AnalogInputReal_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        instance_amount = len(instance_list)

        params = {'object' : 'AIR',
                  'Communicated_blocks': '',
                  'DB_object' : '',
                  'object_call' : '',
                  'instance_amount': instance_amount,
                  'DB_object_ManRequest': '',
                  'FI': ''}

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")

            params['DB_object'] += '''DATA_BLOCK "$name$" "CPC_FB_AIR"
BEGIN
$self.get_instance_assignment(instance, idx)$
END_DATA_BLOCK

'''

            fe_type = self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0")
            interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip()
            interface_param2 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2").strip()
            address_interface_param1 = "NULL"
            address_interface_param2 = "NULL"
            if fe_type in ["101", "102", "103"]:
                address_interface_param1\
                    = '"$interface_param1.split(".")[0]$".$".".join(interface_param1.split(".")[1:])$'
                if fe_type in ["102", "103"]:
                    address_interface_param2\
                        = '"$interface_param2.split(".")[0]$".$".".join(interface_param2.split(".")[1:])$'
            elif fe_type in ["201", "202", "203", "204", "205", "206"]:
                address_interface_param1 = interface_param1.lower().replace("pid","").replace("id","")
                params['object_call'] += '''#Temp_InterfaceParam1 := $address_interface_param1$;
'''
                address_interface_param1 = '#Temp_InterfaceParam1'
                if fe_type == "206":
                    address_interface_param2 = interface_param2.lower().replace("pib","").replace("ib","")
                    params['object_call'] += '''#Temp_InterfaceParam2 := $address_interface_param2$;
'''
                    address_interface_param2 = '#Temp_InterfaceParam2'
            params['object_call'] += '''    "CPC_FC_AIR"(DB_AIR := "$name$",
            InterfaceParam1 := $address_interface_param1$,
            InterfaceParam2 := $address_interface_param2$);
'''

        params['Communicated_blocks'] = (2 * self.CRLF).join(
            [self.process_template("TIAInst_ObjectManReq_Template.scl", params),
             self.process_template("TIAInst_ObjectEvent_Template.scl", params),
             self.process_template("TIAInst_ObjectBinStatus_Template.scl", params),
             self.process_template("TIAInst_ObjectAnaStatus_Template.scl", params)])

        self.writeDeviceInstances(self.process_template("TIAInst_Object_Template.scl", params))
        self.writeFCDeviceInstances(self.process_template("TIAInst_Object_FC_Template.scl", params))

    def get_instance_assignment(self, instance, idx):
        optimized_db_all = '''   // AIR number <%(index)s>
   index := %(index)s;
   PMaxRan := %(PMaxRan)s;
   PMinRan := %(PMinRan)s;
   PMaxRaw := %(PMaxRaw)s;
   PMinRaw := %(PMinRaw)s;
   PDb := %(PDb)s;
   FOFEn := %(FOFEn)s;
   FEType := %(FEType)s;
   PIWDef := %(PIWDef)s;'''
        params = {
            'name': instance.getAttributeData('DeviceIdentification:Name'),
            'index': idx,
            'PMaxRan': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Max")),
            'PMinRan': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Min")),
            'PMaxRaw': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Raw Max")),
            'PMinRaw': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Raw Min")),
            'PDb': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Deadband (%)")),
            'FEType': self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0")
        }
        filter_time = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Filtering Time (s)"))
        if filter_time != "":
            params['FOFEn'] = "TRUE"
        else:
            params['FOFEn'] = "FALSE"
        interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
        if interface_param1.startswith('p'):
            params['PIWDef'] = "TRUE"
        else:
            params['PIWDef'] = "FALSE"
        return optimized_db_all % params
