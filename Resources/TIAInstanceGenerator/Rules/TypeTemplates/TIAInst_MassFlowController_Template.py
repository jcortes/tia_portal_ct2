# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.plugins.interfaces import APlugin


class MassFlowController_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython for MassFlowController.")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for MassFlowController.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for MassFlowController.")

    def process(self, *params):
        self.thePlugin.writeErrorInUABLog("the object MassFlowController has not been develop for the Siemens Platform. The instance file will be empty.")

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for MassFlowController.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for MassFlowController.")
