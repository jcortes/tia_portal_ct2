# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class AnalogParameter_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        instance_amount = len(instance_list)

        params = {'object': 'APAR',
                  'Communicated_blocks': '',
                  'DB_object': '',
                  'object_call': '',
                  'instance_amount': instance_amount,
                  'DB_object_ManRequest': '',
                  'FI': ''}

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")

            params['DB_object'] += '''DATA_BLOCK "$name$" "CPC_FB_APAR"
BEGIN
$self.get_instance_assignment(instance, idx)$
END_DATA_BLOCK

'''

            params['object_call'] += '''    "CPC_FC_APAR"(DB_APAR := "$name$");''' + self.CRLF

        params['Communicated_blocks'] = (2 * self.CRLF).join(
            [self.process_template("TIAInst_ObjectManReq_Template.scl", params),
             self.process_template("TIAInst_ObjectEvent_Template.scl", params),
             self.process_template("TIAInst_ObjectBinStatus_Template.scl", params),
             self.process_template("TIAInst_ObjectAnaStatus_Template.scl", params)])

        self.writeDeviceInstances(self.process_template("TIAInst_Object_Template.scl", params))
        self.writeFCDeviceInstances(self.process_template("TIAInst_Object_FC_Template.scl", params))

    def get_instance_assignment(self, instance, idx):
        optimized_db_all = '''   // APAR number <%(index)s>
    index := %(index)s;
    PosSt := %(default_value)s;'''
        params = {
            'index': idx,
            'default_value': self.thePlugin.formatNumberPLC(self.spec.get_attribute_value(
                instance, "FEDeviceParameters:Default Value", "0.0"))
        }

        default_value = self.thePlugin.formatNumberPLC(self.spec.get_attribute_value(
            instance, "FEDeviceParameters:Default Value", "0.0"))
        name = instance.getAttributeData("DeviceIdentification:Name")
        range_max = instance.getAttributeData("FEDeviceParameters:Range Max").strip()
        range_min = instance.getAttributeData("FEDeviceParameters:Range Min").strip()
        if float(default_value) < float(range_min) or float(default_value) > float(range_max):
            self.thePlugin.writeWarningInUABLog("Apar instance: " + name + ". The default value:(" + default_value
                                                + ") is outside its range [" + range_min + " - " + range_max + "].")
        if float(range_max) < float(range_min):
            self.thePlugin.writeWarningInUABLog("Apar instance: " + name + ". The value Max:(" + range_max +
                                                ") should be bigger than the Min:(" + range_min + ").")

        return optimized_db_all % params
