# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class ProcessControlObject_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        instance_amount = len(instance_list)

        params = {'object': 'PCO',
                  'Communicated_blocks': '',
                  'DB_object': '',
                  'instance_amount': instance_amount,
                  'DB_object_ManRequest': '',
                  'FI': ''}

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")

            params['DB_object'] += '''DATA_BLOCK "$name$" "CPC_FB_PCO"
BEGIN
$self.get_instance_assignment(instance, idx)$
END_DATA_BLOCK

'''

        params['Communicated_blocks'] = (2 * self.CRLF).join(
            [self.process_template("TIAInst_ObjectManReq_Template.scl", params),
             self.process_template("TIAInst_ObjectEvent_Template.scl", params),
             self.process_template("TIAInst_ObjectBinStatus_Template.scl", params),
             self.process_template("TIAInst_ObjectAnaStatus_Template.scl", params)])

        self.writeDeviceInstances(self.process_template("TIAInst_LogicObject_Template.scl", params))

    @staticmethod
    def get_allowance_mode(instance, spec_field):
        allowance_mode = instance.getAttributeData(spec_field).strip()
        if allowance_mode == "":
            allowance_mode = '00000000'
        allowance_mode = str(hex(int(allowance_mode, 2))).replace('0x', '')
        return allowance_mode

    def get_instance_assignment(self, instance, idx):
        instance_db = '''   index := %(index)s;
   // new: ParReg for PCO
   PPCO.PArReg := 2#%(PArReg)s;
   POpMoTa[0] := B#16#%(mode1)s;
   POpMoTa[1] := B#16#%(mode2)s;
   POpMoTa[2] := B#16#%(mode3)s;
   POpMoTa[3] := B#16#%(mode4)s;
   POpMoTa[4] := B#16#%(mode5)s;
   POpMoTa[5] := B#16#%(mode6)s;
   POpMoTa[6] := B#16#%(mode7)s;
   POpMoTa[7] := B#16#%(mode8)s;'''

        par_reg = ['0'] * 15
        par_reg[6] = self.spec.get_bit_from_attribute(
            instance, "FEDeviceParameters:ParReg:Manual Restart after Full Stop", off_value="false")
        par_reg[5] = self.spec.get_bit_from_attribute(
            instance, "FEDeviceParameters:ParReg:Manual Restart after Full Stop",
            on_value="true even if full stop still active")

        params = {
            'index': idx,
            'PArReg': "".join(par_reg),
            'mode1': self.get_allowance_mode(
                instance, "FEDeviceParameters:Option Mode Allowance Table:Option Mode 1 Allowance"),
            'mode2': self.get_allowance_mode(
                instance, "FEDeviceParameters:Option Mode Allowance Table:Option Mode 2 Allowance"),
            'mode3': self.get_allowance_mode(
                instance, "FEDeviceParameters:Option Mode Allowance Table:Option Mode 3 Allowance"),
            'mode4': self.get_allowance_mode(
                instance, "FEDeviceParameters:Option Mode Allowance Table:Option Mode 4 Allowance"),
            'mode5': self.get_allowance_mode(
                instance, "FEDeviceParameters:Option Mode Allowance Table:Option Mode 5 Allowance"),
            'mode6': self.get_allowance_mode(
                instance, "FEDeviceParameters:Option Mode Allowance Table:Option Mode 6 Allowance"),
            'mode7': self.get_allowance_mode(
                instance, "FEDeviceParameters:Option Mode Allowance Table:Option Mode 7 Allowance"),
            'mode8': self.get_allowance_mode(
                instance, "FEDeviceParameters:Option Mode Allowance Table:Option Mode 8 Allowance")
        }

        return instance_db % params
