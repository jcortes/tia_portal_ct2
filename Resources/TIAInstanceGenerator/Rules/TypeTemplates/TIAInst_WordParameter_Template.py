# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class WordParameter_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        instance_amount = len(instance_list)

        params = {'object': 'WPAR',
                  'Communicated_blocks': '',
                  'DB_object': '',
                  'object_call': '',
                  'instance_amount': instance_amount,
                  'DB_object_ManRequest': '',
                  'FI': ''}

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")

            params['DB_object'] += '''DATA_BLOCK "$name$" "CPC_FB_WPAR"
BEGIN
$self.get_instance_assignment(instance, idx)$
END_DATA_BLOCK

'''

            fe_type = self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0")
            interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip()
            address_interface_param1 = "NULL"
            if fe_type == "101":
                address_interface_param1\
                    = '"$interface_param1.split(".")[0]$".$".".join(interface_param1.split(".")[1:])$'
            elif fe_type in ["1", "100"]:
                address_interface_param1 = interface_param1.lower().replace("pqw", "").replace("qw", "")\
                    .replace("mw", "")
                params['object_call'] += '''#Temp_InterfaceParam1 := $address_interface_param1$;
'''
                address_interface_param1 = '#Temp_InterfaceParam1'
            params['object_call'] += '''    "CPC_FC_WPAR"(DB_WPAR := "$name$",
            InterfaceParam1 := $address_interface_param1$);
'''

        params['Communicated_blocks'] = (2 * self.CRLF).join(
            [self.process_template("TIAInst_ObjectManReq_Template.scl", params),
             self.process_template("TIAInst_ObjectEvent_Template.scl", params),
             self.process_template("TIAInst_ObjectBinStatus_Template.scl", params),
             self.process_template("TIAInst_ObjectAnaStatus_Template.scl", params)])

        self.writeDeviceInstances(self.process_template("TIAInst_Object_Template.scl", params))
        self.writeFCDeviceInstances(self.process_template("TIAInst_Object_FC_Template.scl", params))

    def get_instance_assignment(self, instance, idx):
        optimized_db_all = '''    index := %(index)s;
    PosSt := W#16#%(default_value)s;
    FEType := %(FEType)s;
    %(pqw_def)s'''
        params = {
            'index': idx,
            'FEType': self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0"),
            'default_value': [],
            'pqw_def': ''
        }
        default_value = instance.getAttributeData("FEDeviceParameters:Default Value")
        range_max = instance.getAttributeData("FEDeviceParameters:Range Max").strip()
        range_min = instance.getAttributeData("FEDeviceParameters:Range Min").strip()
        # TODO move to semantic check
        # check consistency
        if float(default_value) < float(range_min) or float(default_value) > float(range_max):
            self.logger.write_warning(
                instance, "The default value:(" + str(default_value) + ") is outside its range [" + str(range_min)
                          + " - " + str(range_max) + "].")
        if float(range_max) < float(range_min):
            self.logger.write_warning(
                instance, "The value Max:(" + str(range_max) + ") should be bigger than the Min:(" + str(range_min)
                          + ").")
        params['default_value'] = hex(int(default_value))[2:]
        interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
        fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
        if fe_type == '1':
            if interface_param1.startswith('p'):
                params['pqw_def'] = 'PQWDef := true;'
            else:
                params['pqw_def'] = 'PQWDef := false;'
        return optimized_db_all % params
