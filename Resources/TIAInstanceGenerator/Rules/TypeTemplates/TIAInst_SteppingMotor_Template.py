# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.plugins.interfaces import APlugin


class SteppingMotor_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython for SteppingMotor.")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for SteppingMotor.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for SteppingMotor.")

    def process(self, *params):
        self.thePlugin.writeErrorInUABLog("the object SteppingMotor has not been develop for TIA Portal iemens Platform. The instance file will be empty.")

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for SteppingMotor.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for SteppingMotor.")
