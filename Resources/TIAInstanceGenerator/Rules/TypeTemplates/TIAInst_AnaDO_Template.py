# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


# noinspection PyClassHasNoInit,PyClassHasNoInit
class AnaDO_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        instance_amount = len(instance_list)

        params = {'object': 'ANADO',
                  'Communicated_blocks': '',
                  'DB_object': '',
                  'object_call': '',
                  'instance_amount': instance_amount,
                  'DB_object_ManRequest': '',
                  'FI': ''}

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")

            params['DB_object'] += '''DATA_BLOCK "$name$" "CPC_FB_ANADO"
BEGIN
$self.get_instance_assignment(instance, idx)$
END_DATA_BLOCK

'''

            params['object_call'] += self.get_instance_call(instance, name, idx)

            range_min, range_max = self.get_range(self.thePlugin, instance)
            range_threshold = 0.1 * (float(range_max) - float(range_min))
            limit_off = self.spec.get_attribute_value(
                instance, "FEDeviceManualRequests:Parameter Limit Off/Closed", float(range_min) + range_threshold)
            limit_on = self.spec.get_attribute_value(
                instance, "FEDeviceManualRequests:Parameter Limit On/Open", float(range_max) - range_threshold)
            params['DB_object_ManRequest'] += '   AnaDO_Requests[%s].PliOn := %s;' % (idx, limit_on) + self.CRLF
            params['DB_object_ManRequest'] += '   AnaDO_Requests[%s].PliOff := %s;' % (idx, limit_off) + self.CRLF

        params['Communicated_blocks'] = (2 * self.CRLF).join(
            [self.process_template("TIAInst_ObjectManReq_Template.scl", params),
             self.process_template("TIAInst_ObjectEvent_Template.scl", params),
             self.process_template("TIAInst_ObjectBinStatus_Template.scl", params),
             self.process_template("TIAInst_ObjectAnaStatus_Template.scl", params)])

        self.writeDeviceInstances(self.process_template("TIAInst_Object_Template.scl", params))
        self.writeFCDeviceInstances(self.process_template("TIAInst_Object_FC_Template.scl", params))

    def get_instance_call(self, instance, name, idx):
        instance_fc = '''	// ----------------------------------------------
    // ---- AnaDO <%(idx)s>: %(name)s
    // ----------------------------------------------
    %(PWDt_link)s
    %(PWDb_link)s
    %(HFOn_link)s
    %(HFPos_link)s
    %(HLD_link)s
    %(HOnR_link)s
    %(HOffR_link)s
    // IOError
    %(io_error)s
    
    // IOSimu
    %(io_simu)s
    
    // Calls the Baseline function
    "CPC_FC_ANADO"(DB_ANADO := "%(name)s");
    %(OutOV_link)s
    %(OutOnOV_link)s
    
    //Reset AuAuMoR and AuAlAck
    "%(name)s".AuAuMoR := FALSE;
    "%(name)s".AuAlAck := FALSE;
    "%(name)s".AuRstart := FALSE;
    
'''
        params = {
            'name': name,
            'idx': idx,
            'PWDt_link': '',
            'PWDb_link': '',
            'HFOn_link': '',
            'HFPos_link': '',
            'HLD_link': '',
            'HOnR_link': '',
            'HOffR_link': '',
            'OutOV_link': '',
            'OutOnOV_link': '',
            'io_error': '',
            'io_simu': ''
        }

        the_unicos_project = self.thePlugin.getUnicosProject()

        warning_delay = instance.getAttributeData("FEDeviceParameters:Warning Time Delay (s)")
        if the_unicos_project.findInstanceByName("AnalogParameter,AnalogStatus,AnalogInput,AnalogInputReal",
                                                 warning_delay):
            params['PWDt_link'] = '''"%s".PAnalog.PWDt := DINT_TO_TIME(REAL_TO_DINT("%s".PosSt*1000.0));''' %\
                                  (name, warning_delay)

        deadband = instance.getAttributeData("FEDeviceParameters:Warning Deadband Value (Unit)")
        if the_unicos_project.findInstanceByName("AnalogParameter,AnalogStatus,AnalogInput,AnalogInputReal", deadband):
            params['PWDb_link'] = '''"%s".PAnalog.PWDb := "%s".PosSt;''' % (name, deadband)

        feedback_on = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback On")
        if the_unicos_project.findInstanceByName("DigitalInput", feedback_on):
            params['HFOn_link'] = '''"%s".HFOn := "%s".PosSt;''' % (name, feedback_on)

        feedback_analog = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Analog")
        if the_unicos_project.findInstanceByName("AnalogInput,AnalogInputReal, AnalogStatus, WordStatus",
                                                 feedback_analog):
            params['HFPos_link'] = '''"%s".HFPos := "%s".PosSt;''' % (name, feedback_analog)

        local_drive = instance.getAttributeData("FEDeviceEnvironmentInputs:Local Drive")
        if the_unicos_project.findInstanceByName("DigitalInput", local_drive):
            params['HLD_link'] = '''"%s".HLD := "%s".PosSt;''' % (name, local_drive)

        local_on = instance.getAttributeData("FEDeviceEnvironmentInputs:Local On")
        if the_unicos_project.findInstanceByName("DigitalInput", local_on):
            params['HOnR_link'] = '''"%s".HOnR := "%s".PosSt;''' % (name, local_on)

        local_off = instance.getAttributeData("FEDeviceEnvironmentInputs:Local Off")
        if the_unicos_project.findInstanceByName("DigitalInput", local_off):
            params['HOffR_link'] = '''"%s".HOffR := "%s".PosSt;''' % (name, local_off)

        analog_process_output = instance.getAttributeData("FEDeviceOutputs:Analog Process Output")
        if the_unicos_project.findInstanceByName("AnalogOutput,AnalogOutputReal", analog_process_output):
            params['OutOV_link'] = '''"%s".AuposR := "%s".OutOV;''' % (analog_process_output, name)

        digital_process_output = instance.getAttributeData("FEDeviceOutputs:Digital Process Output")
        if the_unicos_project.findInstanceByName("DigitalOutput", digital_process_output):
            params['OutOnOV_link'] = '''"%s".AuposR := "%s".OutOnOV;''' % (digital_process_output, name)

        linked_objects = [feedback_on, feedback_analog, local_drive, local_on, local_off, analog_process_output,
                          digital_process_output]
        params['io_error'], params['io_simu'] = self.get_io_error_and_simu(the_unicos_project, name, linked_objects)

        return instance_fc % params

    @staticmethod
    def get_range(plugin, instance):
        range_min = "0.0"
        range_max = "100.0"
        analog_process_output = instance.getAttributeData("FEDeviceOutputs:Analog Process Output")
        if analog_process_output != "":
            unicos_project = plugin.getUnicosProject()
            linked_devices = unicos_project.findMatchingInstances(
                "AnalogOutput, AnalogOutputReal", "'#DeviceIdentification:Name#'='%s'" % analog_process_output)
            if len(linked_devices) > 0:
                range_min = plugin.formatNumberPLC(linked_devices[0].getAttributeData("FEDeviceParameters:Range Min"))
                range_max = plugin.formatNumberPLC(linked_devices[0].getAttributeData("FEDeviceParameters:Range Max"))

        return range_min, range_max

    def get_parreg_value(self, instance):
        par_reg = ['0'] * 16
        par_reg[15] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Fail-Safe",
                                                       on_value="on/open")
        par_reg[14] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Feedback On")
        par_reg[12] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Feedback Analog")
        par_reg[11] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Local Drive")
        par_reg[10] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Hardware Analog Output")
        par_reg[7] = self.spec.get_bit_from_attribute(
            instance, "FEDeviceParameters:ParReg:Manual Restart after Full Stop", off_value="false")
        par_reg[6] = self.spec.get_bit_from_attribute(
            instance, "FEDeviceParameters:ParReg:Manual Restart after Full Stop",
            on_value="true even if full stop still active")
        return "".join(par_reg)

    def get_instance_assignment(self, instance, idx):
        instance_db = '''   index := %(index)s;
   PAnalog.ParReg := 2#%(PArReg)s;
   PAnalog.PMaxRan := %(PMaxRan)s;
   PAnalog.PMInSpd := %(PMinRan)s;
   PAnalog.PMStpInV := %(PMStpInV)s;
   PAnalog.PMStpDeV := %(PMStpDeV)s;
   PAnalog.PMinSpd := %(PMinSpd)s;
   PAnalog.PMDeSpd := %(PMDeSpd)s;
   PAnalog.PWDt := T#%(PWDt)ss;
   PAnalog.PWDb := %(PWDb)s;'''
        params = {
            'index': idx,
            'PArReg': self.get_parreg_value(instance)
        }

        range_min, range_max = self.get_range(self.thePlugin, instance)
        output_range = float(range_max) - float(range_min)
        params['PMaxRan'] = range_max
        params['PMinRan'] = range_min
        params['PWDt'] = self.spec.get_attribute_value(instance, "FEDeviceParameters:Warning Time Delay (s)", "5.0")
        # default increase/decrease speed makes if from 0% to 100% in 10 seconds
        params['PMinSpd'] = self.spec.get_attribute_value(
            instance, "FEDeviceParameters:Manual Increase Speed (Unit/s)", output_range / 10)
        params['PMDeSpd'] = self.spec.get_attribute_value(
            instance, "FEDeviceParameters:Manual Decrease Speed (Unit/s)", output_range / 10)
        # default step is 1% of the range
        params['PWDb'] = self.spec.get_attribute_value(
            instance, "FEDeviceParameters:Warning Deadband Value (Unit)", output_range / 100)
        params['PMStpInV'] = self.spec.get_attribute_value(
            instance, "FEDeviceParameters:Manual Increase Step (Unit)", output_range / 100)
        params['PMStpDeV'] = self.spec.get_attribute_value(
            instance, "FEDeviceParameters:Manual Decrease Step (Unit)", output_range / 100)

        return instance_db % params
