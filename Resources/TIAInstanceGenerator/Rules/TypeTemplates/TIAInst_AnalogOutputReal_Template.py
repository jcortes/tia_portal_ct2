# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class AnalogOutputReal_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        instance_amount = len(instance_list)

        params = {'object' : 'AOR',
                  'Communicated_blocks': '',
                  'DB_object' : '',
                  'object_call' : '',
                  'instance_amount': instance_amount,
                  'DB_object_ManRequest': '',
                  'FI': ''}

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")

            params['DB_object'] += '''DATA_BLOCK "$name$" "CPC_FB_AOR"
BEGIN
$self.get_instance_assignment(instance, idx)$
END_DATA_BLOCK

'''

            fe_type = self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0")
            interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip()
            interface_param2 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2").strip()
            address_interface_param1 = "NULL"
            address_interface_param2 = "NULL"
            if fe_type in ["101", "102", "103"]:
                address_interface_param1\
                    = '"$interface_param1.split(".")[0]$".$".".join(interface_param1.split(".")[1:])$'
                if fe_type in ["102", "103"]:
                    address_interface_param2\
                        = '"$interface_param2.split(".")[0]$".$".".join(interface_param2.split(".")[1:])$'
            elif fe_type in ["201", "205"]:
                address_interface_param1 = interface_param1.lower().replace("pqd","").replace("qd","")
                params['object_call'] += '''#Temp_InterfaceParam1 := $address_interface_param1$;
'''
                address_interface_param1 = '#Temp_InterfaceParam1'
            params['object_call'] += '''    "CPC_FC_AOR"(DB_AOR := "$name$",
            InterfaceParam1 := $address_interface_param1$,
            InterfaceParam2 := $address_interface_param2$);
'''

        params['Communicated_blocks'] = (2 * self.CRLF).join(
            [self.process_template("TIAInst_ObjectManReq_Template.scl", params),
             self.process_template("TIAInst_ObjectEvent_Template.scl", params),
             self.process_template("TIAInst_ObjectBinStatus_Template.scl", params),
             self.process_template("TIAInst_ObjectAnaStatus_Template.scl", params)])

        self.writeDeviceInstances(self.process_template("TIAInst_Object_Template.scl", params))
        self.writeFCDeviceInstances(self.process_template("TIAInst_Object_FC_Template.scl", params))

    def get_instance_assignment(self, instance, idx):
        instance_assignment = '''   FEType := %(FEType)s;
   index := %(index)s;
   PMaxRan := %(PMaxRan)s;
   PMinRan := %(PMinRan)s;
   PMaxRaw := %(PMaxRaw)s;
   PMinRaw := %(PMinRaw)s;
   PQWDef := %(PQWDef)s;'''
        params = {
            'index': idx,
            'PMaxRan': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Max")),
            'PMinRan': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Min")),
            'PMaxRaw': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Raw Max")),
            'PMinRaw': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Raw Min")),
            'FEType': self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0")
        }
        interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
        if interface_param1.startswith('p'):
            params['PQWDef'] = "TRUE"
        else:
            params['PQWDef'] = "FALSE"

        return instance_assignment % params
