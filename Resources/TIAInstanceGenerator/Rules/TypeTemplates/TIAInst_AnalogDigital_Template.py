# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class AnalogDigital_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        instance_amount = len(instance_list)

        params = {'object': 'ANADIG',
                  'Communicated_blocks': '',
                  'DB_object': '',
                  'object_call': '',
                  'instance_amount': instance_amount,
                  'DB_object_ManRequest': '',
                  'FI': ''}

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")

            params['DB_object'] += '''DATA_BLOCK "$name$" "CPC_FB_ANADIG"
BEGIN
$self.get_instance_assignment(instance, idx)$
END_DATA_BLOCK

'''

            params['object_call'] += self.get_instance_call(instance)

            range_max = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Max"))
            range_min = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Min"))
            range_threshold = 0.1 * (float(range_max) - float(range_min))
            limit_off = self.spec.get_attribute_value(
                instance, "FEDeviceManualRequests:Parameter Limit Off/Closed", float(range_min) + range_threshold)
            limit_on = self.spec.get_attribute_value(
                instance, "FEDeviceManualRequests:Parameter Limit On/Open", float(range_max) - range_threshold)
            params['DB_object_ManRequest'] += '   AnaDig_Requests[%s].PliOn := %s;' % (idx, limit_on) + self.CRLF
            params['DB_object_ManRequest'] += '   AnaDig_Requests[%s].PliOff := %s;' % (idx, limit_off) + self.CRLF

        params['Communicated_blocks'] = (2 * self.CRLF).join(
            [self.process_template("TIAInst_ObjectManReq_Template.scl", params),
             self.process_template("TIAInst_ObjectEvent_Template.scl", params),
             self.process_template("TIAInst_ObjectBinStatus_Template.scl", params),
             self.process_template("TIAInst_ObjectAnaStatus_Template.scl", params)])

        self.writeDeviceInstances(self.process_template("TIAInst_Object_Template.scl", params))
        self.writeFCDeviceInstances(self.process_template("TIAInst_Object_FC_Template.scl", params))

    def get_parreg_value(self, instance):
        par_reg = ['0'] * 16
        par_reg[15] = self.spec.get_bit_from_attribute(
            instance, "FEDeviceParameters:ParReg:Fail-Safe", off_value=["off/close"])
        par_reg[14] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Feedback On")
        par_reg[13] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Feedback Off")
        par_reg[12] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Feedback Analog")
        par_reg[11] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Local Drive")
        par_reg[10] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Hardware Analog Output")
        par_reg[9] = self.spec.get_bit_from_attribute(
            instance, "FEDeviceParameters:ParReg:PWM Mode", off_value=["classic", ""])
        par_reg[8] = self.spec.get_bit_from_attribute(
            instance, "FEDeviceParameters:ParReg:Outputs Maintained", off_value=["false", ""])
        par_reg[7] = self.spec.get_bit_from_attribute(
            instance, "FEDeviceParameters:ParReg:Manual Restart after Full Stop", off_value=["false"])
        par_reg[6] = self.spec.get_bit_from_attribute(
            instance, "FEDeviceParameters:ParReg:Manual Restart after Full Stop",
            on_value="true even if full stop still active")

        return "".join(par_reg)

    def get_instance_assignment(self, instance, idx):
        instance_db = '''    index := %(index)s;
    PAnalog.ParReg := 2#%(PArReg)s;
    PAnalog.PMaxRan := %(PMaxRan)s;
    PAnalog.PMinRan := %(PMinRan)s;
    PAnalog.PMStpInV := %(PMStpInV)s;
    PAnalog.PMStpDeV := %(PMStpDeV)s;
    PAnalog.PMInSpd := %(PMinSpd)s;
    PAnalog.PMDeSpd := %(PMDeSpd)s;
    PAnalog.PWDt := T#%(PWDt)ss;
    PAnalog.PWDb := %(PWDb)s;
    PPWM.PTPeriod := T#%(PTPeriod)ss;
    PPWM.PTMin := T#%(PTMin)ss;
    PPWM.PInMax := %(PInMax)s;'''
        range_max = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Max"))
        range_min = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Min"))
        output_range = float(range_max) - float(range_min)

        params = {
            'index': idx,
            'PArReg': self.get_parreg_value(instance),
            'PMaxRan': range_max,
            'PMinRan': range_min,
            'PMStpInV': self.spec.get_attribute_value(
                instance, "FEDeviceParameters:Manual Increase Step (Unit)", output_range / 100),
            'PMStpDeV': self.spec.get_attribute_value(
                instance, "FEDeviceParameters:Manual Decrease Step (Unit)", output_range / 100),
            'PMinSpd': self.spec.get_attribute_value(
                instance, "FEDeviceParameters:Manual Increase Speed (Unit/s)", output_range / 10),
            'PMDeSpd': self.spec.get_attribute_value(
                instance, "FEDeviceParameters:Manual Decrease Speed (Unit/s)", output_range / 10),
            'PWDt': self.spec.get_attribute_value(instance, "FEDeviceParameters:Warning Time Delay (s)", "5.0"),
            'PWDb': self.spec.get_attribute_value(
                instance, "FEDeviceParameters:Warning Deadband Value (Unit)", output_range / 100),
            'PTPeriod': self.spec.get_attribute_value(
                instance, "FEDeviceParameters:PWM Parameters:Time Period (s)", "1.0"),
            'PTMin': self.spec.get_attribute_value(
                instance, "FEDeviceParameters:PWM Parameters:Minimum Duration (s)", "0.0"),
            'PInMax': self.spec.get_attribute_value(
                instance, "FEDeviceParameters:PWM Parameters:Max Deviation", output_range)
        }

        return instance_db % params

    def get_instance_call(self, instance):
        instance_assignment = '''	// ----------------------------------------------
    // ---- Anadig <%(index)s>: %(name)s
    // ----------------------------------------------	
    %(PWDt_link)s
    %(PWDb_link)s
    %(HFOn_link)s
    %(HFOff_link)s
    %(HFPos_link)s
    %(HLD_link)s
    // IO Error
    %(io_error)s
    // IOSimu
    %(io_simu)s
    // Calls the Baseline function
    "CPC_FC_ANADIG"(DB_ANADIG := "%(name)s");
    
    %(DOutOnOV_link)s
    %(DOutOffOV_link)s
    
    //Reset AuAuMoR and AuAlAck
    "%(name)s".AuAuMoR := FALSE;
    "%(name)s".AuAlAck := FALSE;
    "%(name)s".AuRstart := FALSE;
        
'''
        name = instance.getAttributeData("DeviceIdentification:Name")
        idx = instance.getInstanceNumber()
        params = {
            'name': name,
            'index': idx,
            'PWDt_link': "",
            'PWDb_link': "",
            'HFOn_link': "",
            'HFOff_link': "",
            'HFPos_link': "",
            'HLD_link': "",
            'io_error': "",
            'io_simu': "",
            'DOutOnOV_link': "",
            'DOutOffOV_link': ""
        }

        the_unicos_project = self.thePlugin.getUnicosProject()

        warning_delay = instance.getAttributeData("FEDeviceParameters:Warning Time Delay (s)")
        if the_unicos_project.findInstanceByName(
                "AnalogParameter,AnalogStatus,AnalogInput,AnalogInputReal", warning_delay):
            params['PWDt_link'] = '''"%s".PAnalog.PWDt := DINT_TO_TIME(REAL_TO_DINT("%s".PosSt*1000.0));''' %\
                                  (name, warning_delay)

        deadband = instance.getAttributeData("FEDeviceParameters:Warning Deadband Value (Unit)")
        if the_unicos_project.findInstanceByName("AnalogParameter,AnalogStatus,AnalogInput,AnalogInputReal", deadband):
            params['PWDb_link'] = '''"%s".PAnalog.PWDb := "%s".PosSt;''' % (name, deadband)

        feedback_on = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback On")
        if the_unicos_project.findInstanceByName("DigitalInput", feedback_on):
            params['HFOn_link'] = '''"%s".HFOn := "%s".PosSt;''' % (name, feedback_on)

        feedback_off = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Off")
        if the_unicos_project.findInstanceByName("DigitalInput", feedback_off):
            params['HFOff_link'] = '''"%s".HFOff := "%s".PosSt;''' % (name, feedback_off)

        feedback_analog = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Analog")
        if the_unicos_project.findInstanceByName("AnalogInput,AnalogInputReal,AnalogStatus", feedback_analog):
            params['HFPos_link'] = '''"%s".HFPos := "%s".PosSt;''' % (name, feedback_analog)

        local_drive = instance.getAttributeData("FEDeviceEnvironmentInputs:Local Drive")
        if the_unicos_project.findInstanceByName("DigitalInput", local_drive):
            params['HLD_link'] = '''"%s".HLD := "%s".PosSt;''' % (name, local_drive)

        output_on = instance.getAttributeData("FEDeviceOutputs:Output On")
        if the_unicos_project.findInstanceByName("DigitalOutput", output_on):
            params['DOutOnOV_link'] = '''"%s".AuposR := "%s".DOutOnOV;''' % (output_on, name)

        output_off = instance.getAttributeData("FEDeviceOutputs:Output Off")
        if the_unicos_project.findInstanceByName("DigitalOutput", output_off):
            params['DOutOffOV_link'] = '''"%s".AuposR := "%s".DOutOffOV;''' % (output_off, name)

        linked_objects = [feedback_on, feedback_off, feedback_analog, local_drive, output_on, output_off]
        params['io_error'], params['io_simu'] =  self.get_io_error_and_simu(the_unicos_project, name, linked_objects)

        return instance_assignment % params
