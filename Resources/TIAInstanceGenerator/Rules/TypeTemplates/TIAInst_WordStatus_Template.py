# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class WordStatus_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        instance_amount = len(instance_list)

        params = {'object': 'WS',
                  'Communicated_blocks': '',
                  'DB_object': '',
                  'object_call': '',
                  'instance_amount': instance_amount,
                  'FI': ''}

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")

            params['DB_object'] += '''DATA_BLOCK "$name$" "CPC_FB_WS"
BEGIN
$self.get_instance_assignment(instance, idx)$
END_DATA_BLOCK

'''

            fe_type = self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0")
            interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip()
            address_interface_param1 = "NULL"
            if fe_type == "101":
                address_interface_param1 = '"$interface_param1.split(".")[0]$".$".".join(interface_param1.split(".")[1:])$'
            elif fe_type in ["1", "100"]:
                address_interface_param1 = interface_param1.lower().replace("piw","").replace("iw","").replace("pib","").replace("ib","").replace("mw","").replace("mb","")
                params['object_call'] += '''#Temp_InterfaceParam1 := $address_interface_param1$;
'''
                address_interface_param1 = '#Temp_InterfaceParam1'
            params['object_call'] += '''    "CPC_FC_WS"(DB_WS := "$name$",
            InterfaceParam1 := $address_interface_param1$);
'''

        params['Communicated_blocks'] = self.process_template("TIAInst_ObjectAnaStatus_Template.scl", params)

        self.writeDeviceInstances(self.process_template("TIAInst_Object_Template.scl", params))
        self.writeFCDeviceInstances(self.process_template("TIAInst_Object_FC_Template.scl", params))

    def get_instance_assignment(self, instance, idx):
        optimized_db_all = '''    index := %(index)s;
    FEType := %(FEType)s;
    %(piw_def)s
    %(read_byte)s'''
        params = {
            'FEType': self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0"),
            'index': idx,
            'piw_def': '',
            'read_byte': ''
        }
        interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
        fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
        if fe_type == '1':
            if interface_param1.startswith('pib') or interface_param1.startswith('ib'):
                params['read_byte'] = 'ReadByte := true;'
            else:
                params['read_byte'] = 'ReadByte := false;'
            if interface_param1.startswith('p'):
                params['piw_def'] = 'PIWDef := true;'
            else:
                params['piw_def'] = 'PIWDef := false;'
        return optimized_db_all % params
