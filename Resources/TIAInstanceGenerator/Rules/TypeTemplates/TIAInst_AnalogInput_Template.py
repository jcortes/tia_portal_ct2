# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class AnalogInput_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        instance_amount = len(instance_list)

        params = {'object' : 'AI',
                  'Communicated_blocks': '',
                  'DB_object' : '',
                  'object_call' : '',
                  'instance_amount': instance_amount,
                  'DB_object_ManRequest': '',
                  'FI': ''}

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")

            params['DB_object'] += '''DATA_BLOCK "$name$" "CPC_FB_AI"
BEGIN
$self.get_instance_assignment(instance, idx)$
$self.get_instance_io_config(instance)$
END_DATA_BLOCK

'''

            params['object_call'] += '''    "CPC_FC_AI"(DB_AI := "$name$");''' + self.CRLF

        params['Communicated_blocks'] = (2 * self.CRLF).join(
            [self.process_template("TIAInst_ObjectManReq_Template.scl", params),
             self.process_template("TIAInst_ObjectEvent_Template.scl", params),
             self.process_template("TIAInst_ObjectBinStatus_Template.scl", params),
             self.process_template("TIAInst_ObjectAnaStatus_Template.scl", params)])

        self.writeDeviceInstances(self.process_template("TIAInst_Object_Template.scl", params))
        self.writeFCDeviceInstances(self.process_template("TIAInst_Object_FC_Template.scl", params))

    def get_instance_assignment(self, instance, idx):
        optimized_db_all = '''   // AI number <%(index)s>
    index := %(index)s;
    Diagnostics := %(diagnostics)s;
    PMinRan := %(PMinRan)s;
    PMaxRan := %(PMaxRan)s;
    PMinRaw := %(PMinRaw)s;
    PMaxRaw := %(PMaxRaw)s;
    PDb := %(PDb)s;
    FOFEn := %(FOFEn)s;
    FEType := %(FEType)s;'''
        params = {
            'index': idx,
            'diagnostics': "TRUE" if self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel") else "FALSE",
            'PMaxRan': self.spec.get_attribute_value(instance, "FEDeviceParameters:Range Max"),
            'PMinRan': self.spec.get_attribute_value(instance, "FEDeviceParameters:Range Min"),
            'PMaxRaw': instance.getAttributeData("FEDeviceParameters:Raw Max"),
            'PMinRaw': instance.getAttributeData("FEDeviceParameters:Raw Min"),
            'PDb': self.spec.get_attribute_value(instance, "FEDeviceParameters:Deadband (%)"),
            'FEType': self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0")
        }
        filter_time = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Filtering Time (s)"))
        if filter_time != "":
            params['FOFEn'] = "TRUE"
        else:
            params['FOFEn'] = "FALSE"
        return optimized_db_all % params

    @staticmethod
    def get_instance_io_config(instance):
        result = []
        fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
        if fe_type == '1':
            interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
            if interface_param1.startswith('p'):
                piw_def = 'true'
                interface_param1 = interface_param1.replace('piw', '').replace('pib', '')
            else:
                piw_def = 'false'
                interface_param1 = interface_param1.replace('iw', '').replace('ib', '')
            result.append('''    perAddress := %s;''' % interface_param1)
            result.append('''    PiwDef := %s;''' % piw_def)
        return '\r\n'.join(result)
