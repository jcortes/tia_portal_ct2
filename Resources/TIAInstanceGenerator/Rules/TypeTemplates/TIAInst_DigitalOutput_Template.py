# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class DigitalOutput_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        instance_amount = len(instance_list)

        fast_interlock_device = list(self.thePlugin.getInstancesFI("DigitalOutput"))
        if len(fast_interlock_device):
            instance_amount_FI = len(fast_interlock_device)
            instance_amount -= instance_amount_FI
            params_FI = {'object' : 'DO',
                         'object_call': '',
                         'instance_amount': instance_amount_FI,
                         'DB_object_ManRequest': '',
                         'FI': '_FI'}

        params = {'object' : 'DO',
                  'Communicated_blocks': '',
                  'DB_object' : '',
                  'object_call' : '',
                  'instance_amount': instance_amount,
                  'DB_object_ManRequest': '',
                  'FI': ''}

        for instance in instance_list:
            name = instance.getAttributeData("DeviceIdentification:Name")

            params['DB_object'] += '''DATA_BLOCK "$name$" "CPC_FB_DO"
BEGIN
$self.get_instance_assignment(instance, instance_list, fast_interlock_device)$
END_DATA_BLOCK

'''

            fe_type = self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0")
            interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip()
            interface_param2 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2").strip()
            temp_interface_param1 = 0
            temp_interface_param2 = 0
            address_interface_param1 = "NULL"
            address_interface_param2 = "NULL"
            if fe_type in ["101", "102", "103"]:
                address_interface_param1\
                    = '"$interface_param1.split(".")[0]$".$".".join(interface_param1.split(".")[1:])$'
                if fe_type in ["102", "103"]:
                    address_interface_param2\
                        = '"$interface_param2.split(".")[0]$".$".".join(interface_param2.split(".")[1:])$'
            elif fe_type == "1":
                temp_interface_param1, temp_interface_param2 = interface_param1.lower().replace("q", "").split(".")
                address_interface_param1 = '#Temp_InterfaceParam1'
                address_interface_param2 = '#Temp_InterfaceParam2'
                params['object_call'] += '''#Temp_InterfaceParam1 := $temp_interface_param1$;
#Temp_InterfaceParam2 := $temp_interface_param2$;
'''
            if instance in fast_interlock_device:
                params['object_call'] += '''    // Delay interrupt OBs until EN_AIRT is called
    NbOfDelayedInterrupts := DIS_AIRT();
''' + self.get_instance_call(name, True, address_interface_param1, address_interface_param2)\
                                         + '''    // Reenable interrupt OBs
    NbOfQueuedInterrupts := EN_AIRT();
'''
                params_FI['object_call'] += '''#Temp_InterfaceParam1 := $temp_interface_param1$;
#Temp_InterfaceParam2 := $temp_interface_param2$;
''' + self.get_instance_call(name, True, address_interface_param1, address_interface_param2)
            else:
                params['object_call'] += self.get_instance_call(
                    name, False, address_interface_param1, address_interface_param2)

        params['Communicated_blocks'] = (2 * self.CRLF).join(
            [self.process_template("TIAInst_ObjectManReq_Template.scl", params),
             self.process_template("TIAInst_ObjectEvent_Template.scl", params),
             self.process_template("TIAInst_ObjectBinStatus_Template.scl", params)])

        if len(fast_interlock_device):
            params['Communicated_blocks'] += 2 * self.CRLF + (2 * self.CRLF).join(
                [self.process_template("TIAInst_ObjectManReq_Template.scl", params_FI),
                 self.process_template("TIAInst_ObjectEvent_Template.scl", params_FI),
                 self.process_template("TIAInst_ObjectBinStatus_Template.scl", params_FI)])

        self.writeDeviceInstances(self.process_template("TIAInst_Object_Template.scl", params))
        if len(fast_interlock_device):
            self.writeFCDeviceInstances(self.process_template("TIAInst_Object_FC_Template.scl", params) + 2 * self.CRLF
                                        + self.process_template("TIAInst_Object_FC_Template.scl", params_FI))
        else:
            self.writeFCDeviceInstances(self.process_template("TIAInst_Object_FC_Template.scl", params))

    def get_instance_assignment(self, instance, instance_list, instance_list_FI):
        instance_assignment = '''    FEType := %(FEType)s;
    Diagnostics := %(diagnostics)s;
    index := %(index)s;'''
        params = {
            'index': 0,
            'diagnostics': "TRUE" if self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel") else "FALSE",
            'FEType': self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0")
        }
        if instance in instance_list_FI:
            params['index'] = instance_list_FI.index(instance)
        else:
            original_list = list(instance_list)
            for instance_FI in instance_list_FI:
                original_list.remove(instance_FI)
            params['index'] = original_list.index(instance)
        params['index'] += 1
        return instance_assignment % params

    def get_instance_call(self, name, fast_interlock_device, address_param1, address_param2):
        FI = "_FI" if fast_interlock_device else ""
        return '''    "CPC_FC_DO$FI$"(DB_DO := "$name$",
            InterfaceParam1 := $address_param1$,
            InterfaceParam2 := $address_param2$);
'''
