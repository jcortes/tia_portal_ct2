# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class AnalogStatus_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        instance_amount = len(instance_list)

        params = {'object': 'AS',
                  'Communicated_blocks': '',
                  'DB_object': '',
                  'object_call': '',
                  'instance_amount': instance_amount,
                  'FI': ''}

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")

            params['DB_object'] += '''DATA_BLOCK "$name$" "CPC_FB_AS"
BEGIN
$self.get_instance_assignment(instance, idx)$
END_DATA_BLOCK

'''

            fe_type = self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0")
            interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip()
            address_interface_param1 = "NULL"
            if fe_type == "101":
                address_interface_param1 = '"$interface_param1.split(".")[0]$".$".".join(interface_param1.split(".")[1:])$'
            elif fe_type in ["1", "100"]:
                address_interface_param1 = interface_param1.lower().replace("pid","").replace("id","").replace("md","")
                params['object_call'] += '''#Temp_InterfaceParam1 := $address_interface_param1$;
'''
                address_interface_param1 = '#Temp_InterfaceParam1'
            params['object_call'] += '''    "CPC_FC_AS"(DB_AS := "$name$",
            InterfaceParam1 := $address_interface_param1$);
'''

        params['Communicated_blocks'] = self.process_template("TIAInst_ObjectAnaStatus_Template.scl", params)

        self.writeDeviceInstances(self.process_template("TIAInst_Object_Template.scl", params))
        self.writeFCDeviceInstances(self.process_template("TIAInst_Object_FC_Template.scl", params))

    def get_instance_assignment(self, instance, idx):
        optimized_db_all = '''    index := %(index)s;
    FOFEn := %(FOFEn)s;
    FEType := %(FEType)s;
    %(filter_time)s
    %(piwdef)s'''
        params = {
            'index': idx,
            'FEType': self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0"),
            'filter_time': '',
            'piwdef': ''
        }

        filter_time = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Filtering Time (s)"))
        if filter_time != "":
            params['FOFEn'] = "TRUE"
            params['filter_time'] = 'FilterTime := %s;' % filter_time
        else:
            params['FOFEn'] = "FALSE"
        fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
        interface_param1 = instance.getAttributeData('FEDeviceIOConfig:FEChannel:InterfaceParam1').strip().lower()
        if fe_type == '1':
            if interface_param1.startswith('pid'):
                params['piwdef'] = 'PIWDef := TRUE;'
            else:
                params['piwdef'] = 'PIWDef := FALSE;'

        return optimized_db_all % params
