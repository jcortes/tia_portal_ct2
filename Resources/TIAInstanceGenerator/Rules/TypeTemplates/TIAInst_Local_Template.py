# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template

class Local_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        instance_amount = len(instance_list)

        params = {'object': 'LOCAL',
                  'Communicated_blocks': '',
                  'DB_object': '',
                  'object_call': '',
                  'instance_amount': instance_amount,
                  'DB_object_ManRequest': '',
                  'FI': ''}

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")
            
            params['DB_object'] += '''DATA_BLOCK "$name$" "CPC_FB_LOCAL"
BEGIN
$self.get_instance_assignment(instance, idx)$
END_DATA_BLOCK

'''

            params['object_call'] += self.get_instance_call(instance, name, idx)

        params['Communicated_blocks'] = (2 * self.CRLF).join(
            [self.process_template("TIAInst_ObjectManReq_Template.scl", params),
             self.process_template("TIAInst_ObjectEvent_Template.scl", params),
             self.process_template("TIAInst_ObjectBinStatus_Template.scl", params)])

        self.writeDeviceInstances(self.process_template("TIAInst_Object_Template.scl", params))
        self.writeFCDeviceInstances(self.process_template("TIAInst_Object_FC_Template.scl", params))

    def get_instance_call(self, instance, name, idx):
        instance_fc = '''	// ----------------------------------------------
	// ---- Local <%(idx)s>: %(name)s
	// ----------------------------------------------
	%(HFOn_link)s
	%(HFOff_link)s

	// IOError
	%(io_error)s
	// IOSimu
	%(io_simu)s
	
	// Calls the Baseline function
	"CPC_FC_LOCAL"(DB_LOCAL := "%(name)s");
	
'''
        params = {
            "name": name,
            "idx": idx,
            "HFOn_link": "",
            "HFOff_link": "",
            "io_error": "",
            "io_simu": ""
        }

        the_unicos_project = self.thePlugin.getUnicosProject()

        feedback_on = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback On")
        if the_unicos_project.findInstanceByName("DigitalInput", feedback_on):
            params['HFOn_link'] = '''"%s".HFOn := "%s".PosSt;''' % (name, feedback_on)

        feedback_off = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Off")
        if the_unicos_project.findInstanceByName("DigitalInput", feedback_off):
            params['HFOff_link'] = '''"%s".HFOff := "%s".PosSt;''' % (name, feedback_off)

        linked_objects = [feedback_on, feedback_off]
        params['io_error'], params['io_simu'] =  self.get_io_error_and_simu(the_unicos_project, name, linked_objects,
                                                                            exclude_itself=True)

        return instance_fc % params

    def get_parreg_value(self, instance):
        par_reg = ['0'] * 16
        par_reg[14] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Feedback On")
        par_reg[13] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Feedback Off")
        par_reg[9] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Full/Empty Animation",
                                                      on_value='Full/Empty')
        par_reg[5] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Position Alarm",
                                                      on_value='true')
        return "".join(par_reg)

    def get_instance_assignment(self, instance, idx):
        instance_db = '''   index := %(index)s;
   PLocal.ParReg := 2#%(ParReg)s;'''
        params = {
                "index": idx,
                "ParReg": self.get_parreg_value(instance)
        }
        return instance_db % params
