# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class OnOff_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        instance_amount = len(instance_list)

        fast_interlock_device = list(self.thePlugin.getInstancesFI("OnOff"))
        if len(fast_interlock_device):
            instance_amount_FI = len(fast_interlock_device)
            instance_amount -= instance_amount_FI
            params_FI = {'object': 'OnOff',
                         'object_call': '',
                         'instance_amount': instance_amount_FI,
                         'DB_object_ManRequest': '',
                         'FI': '_FI'}

        params = {'object': 'ONOFF',
                  'Communicated_blocks': '',
                  'DB_object': '',
                  'object_call': '',
                  'instance_amount': instance_amount,
                  'DB_object_ManRequest': '',
                  'FI': ''}

        for instance in instance_list:
            name = instance.getAttributeData("DeviceIdentification:Name")

            params['DB_object'] += '''DATA_BLOCK "$name$" "CPC_FB_ONOFF"
BEGIN
$self.get_instance_assignment(instance, instance_list, fast_interlock_device)$
END_DATA_BLOCK

'''

            if instance in fast_interlock_device:
                params['object_call'] += self.get_instance_call(instance, instance_list, fast_interlock_device, False)
                params_FI['object_call'] += self.get_instance_call(instance, instance_list, fast_interlock_device, True)
            else:
                params['object_call'] += self.get_instance_call(instance, instance_list, fast_interlock_device, False)

        params['Communicated_blocks'] = (2 * self.CRLF).join(
            [self.process_template("TIAInst_ObjectManReq_Template.scl", params),
             self.process_template("TIAInst_ObjectEvent_Template.scl", params),
             self.process_template("TIAInst_ObjectBinStatus_Template.scl", params)])

        if len(fast_interlock_device):
            params['Communicated_blocks'] += 2 * self.CRLF + (2 * self.CRLF).join(
                [self.process_template("TIAInst_ObjectManReq_Template.scl", params_FI),
                 self.process_template("TIAInst_ObjectEvent_Template.scl", params_FI),
                 self.process_template("TIAInst_ObjectBinStatus_Template.scl", params_FI)])

        self.writeDeviceInstances(self.process_template("TIAInst_Object_Template.scl", params))
        if len(fast_interlock_device):
            self.writeFCDeviceInstances(self.process_template("TIAInst_Object_FC_Template.scl", params) + 2 * self.CRLF
                                        + self.process_template("TIAInst_Object_FC_Template.scl", params_FI))
        else:
            self.writeFCDeviceInstances(self.process_template("TIAInst_Object_FC_Template.scl", params))

    def get_instance_call(self, instance, instance_list, instance_list_FI, FC_FI_call):
        """ this function returns a piece of content of FC_ONOFF function related to given instance """
        instance_fc = '''	// ----------------------------------------------
	// ---- OnOff%(FI)s <%(index)s>: %(name)s
	// ----------------------------------------------
	
	%(PWDt_link)s
	%(HFOn_link)s
	%(HFOff_link)s
	%(HLD_link)s
	%(HOnR_link)s
	%(HOffR_link)s
	// IOError
	%(io_error)s
	// IOSimu
	%(io_simu)s
	// Calls the Baseline function
%(FI_start)s
	"CPC_FC_ONOFF%(FI)s"(DB_ONOFF := "%(name)s");
%(FI_end)s
	%(OutOnOV_link)s
	%(OutOffOV_link)s
	
	//Reset AuAuMoR and AuAlAck
	"%(name)s".AuAuMoR := FALSE;
	"%(name)s".AuAlAck := FALSE;
	"%(name)s".AuRstart := FALSE;'''
        name = instance.getAttributeData("DeviceIdentification:Name")
        params = {
            "name": name,
            "index": 0,
            "PWDt_link": "",
            "HFOn_link": "",
            "HFOff_link": "",
            "HLD_link": "",
            "HOnR_link": "",
            "HOffR_link": "",
            "OutOnOV_link": "",
            "OutOffOV_link": "",
            "io_error": "",
            "io_simu": "",
            "FI": "_FI" if instance in instance_list_FI else "",
            "FI_start": '''    // Delay interrupt OBs until EN_AIRT is called
    NbOfDelayedInterrupts := DIS_AIRT();''' if instance in instance_list_FI and not FC_FI_call else "",
            "FI_end": '''    // Reenable interrupt OBs
    NbOfQueuedInterrupts := EN_AIRT();''' if instance in instance_list_FI and not FC_FI_call else ""
        }
        if instance in instance_list_FI:
            params['index'] = instance_list_FI.index(instance)
        else:
            original_list = list(instance_list)
            for instance_FI in instance_list_FI:
                original_list.remove(instance_FI)
            params['index'] = original_list.index(instance)
        params['index'] += 1

        the_unicos_project = self.thePlugin.getUnicosProject()

        warning_delay = instance.getAttributeData("FEDeviceParameters:Warning Time Delay (s)")
        if the_unicos_project.findInstanceByName("AnalogParameter,AnalogStatus,AnalogInput,AnalogInputReal",
                                                 warning_delay):
            params['PWDt_link'] = '''"%s".POnOff.PWDt := DINT_TO_TIME(REAL_TO_DINT("%s".PosSt * 1000.0));''' % \
                                  (name, warning_delay)

        feedback_on = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback On")
        if the_unicos_project.findInstanceByName("DigitalInput", feedback_on):
            params['HFOn_link'] = '''"%s".HFOn := "%s".PosSt;''' % (name, feedback_on)

        feedback_off = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Off")
        if the_unicos_project.findInstanceByName("DigitalInput", feedback_off):
            params['HFOff_link'] = '''"%s".HFOff := "%s".PosSt;''' % (name, feedback_off)

        local_drive = instance.getAttributeData("FEDeviceEnvironmentInputs:Local Drive")
        if the_unicos_project.findInstanceByName("DigitalInput", local_drive):
            params['HLD_link'] = '''"%s".HLD := "%s".PosSt;''' % (name, local_drive)

        local_on = instance.getAttributeData("FEDeviceEnvironmentInputs:Local On")
        if the_unicos_project.findInstanceByName("DigitalInput", local_on):
            params['HOnR_link'] = '''"%s".HOnR := "%s".PosSt;''' % (name, local_on)

        local_off = instance.getAttributeData("FEDeviceEnvironmentInputs:Local Off")
        if the_unicos_project.findInstanceByName("DigitalInput", local_off):
            params['HOffR_link'] = '''"%s".HOffR := "%s".PosSt;''' % (name, local_off)

        process_output = instance.getAttributeData("FEDeviceOutputs:Process Output")
        if the_unicos_project.findInstanceByName("DigitalOutput", process_output):
            params['OutOnOV_link'] = '''"%s".AuposR := "%s".OutOnOV;''' % (process_output, name)

        process_output_off = instance.getAttributeData("FEDeviceOutputs:Process Output Off")
        if the_unicos_project.findInstanceByName("DigitalOutput", process_output_off):
            params['OutOffOV_link'] = '''"%s".AuposR := "%s".OutOffOV;''' % (process_output_off, name)

        linked_objects = [feedback_on, feedback_off, local_drive, local_on, local_off, process_output,
                          process_output_off]
        params['io_error'], params['io_simu'] = self.get_io_error_and_simu(the_unicos_project, name, linked_objects)

        return instance_fc % params

    def get_parreg_value(self, instance):
        """this function returns parreg for given instance"""
        par_reg = ['0'] * 15
        par_reg[14] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Fail-Safe",
                                                       on_value=["on/open", "2 do on"])
        par_reg[13] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Feedback On")
        par_reg[12] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Feedback Off")
        par_reg[11] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:Pulse Duration (s)",
                                                       off_value=["", "0.0", "0"])
        par_reg[10] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Local Drive")
        local_on = instance.getAttributeData("FEDeviceEnvironmentInputs:Local On").strip().lower()
        local_off = instance.getAttributeData("FEDeviceEnvironmentInputs:Local Off").strip().lower()
        if local_off == "" and local_on == "":
            par_reg[9] = '0'
        else:
            par_reg[9] = '1'
        par_reg[8] = self.spec.get_bit_from_attribute(
            instance, "FEDeviceParameters:ParReg:Full/Empty Animation", on_value="full/empty")
        par_reg[7] = self.spec.get_bit_from_attribute(instance, "FEDeviceOutputs:Process Output Off")
        par_reg[6] = self.spec.get_bit_from_attribute(
            instance, "FEDeviceParameters:ParReg:Manual Restart after Full Stop",
            on_value=['true only if full stop disappeared', 'true even if full stop still active'])
        par_reg[5] = self.spec.get_bit_from_attribute(
            instance, "FEDeviceParameters:ParReg:Manual Restart after Full Stop",
            on_value='true even if full stop still active')
        par_reg[4] = self.spec.get_bit_from_attribute(
            instance, "FEDeviceParameters:ParReg:Fail-Safe", on_value=["2 do on", "2 do off"])
        par_reg[3] = self.spec.get_bit_from_attribute(
            instance, "FEDeviceParameters:ParReg:Constant Time Pulse", on_value='true')
        return "".join(par_reg)

    def get_instance_assignment(self, instance, instance_list, instance_list_FI):
        """ this function returns DATA_BLOCK for given instance """
        instance_db = '''    index := %(index)s;
    POnOff.ParReg := 2#%(ParReg)s;
    POnOff.PPulseLe := T#%(PPulseLe)ss;
    POnOff.PWDt := T#%(PWDt)ss;'''
        params = {
            'index': 0,
            'ParReg': self.get_parreg_value(instance),
            'PPulseLe': self.thePlugin.formatNumberPLC(self.spec.get_attribute_value(
                instance, "FEDeviceParameters:Pulse Duration (s)", "0.0")),
            'PWDt': self.thePlugin.formatNumberPLC(self.spec.get_attribute_value(
                instance, "FEDeviceParameters:Warning Time Delay (s)", "5.0"))
        }
        if instance in instance_list_FI:
            params['index'] = instance_list_FI.index(instance)
        else:
            original_list = list(instance_list)
            for instance_FI in instance_list_FI:
                original_list.remove(instance_FI)
            params['index'] = original_list.index(instance)
        params['index'] += 1

        return instance_db % params
