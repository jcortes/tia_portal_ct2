# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class AnalogOutput_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        instance_amount = len(instance_list)

        params = {'object' : 'AO',
                  'Communicated_blocks': '',
                  'DB_object' : '',
                  'object_call': '',
                  'instance_amount': instance_amount,
                  'DB_object_ManRequest': '',
                  'FI': ''}

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")

            params['DB_object'] += '''DATA_BLOCK "$name$" "CPC_FB_AO"
BEGIN
$self.get_instance_assignment(instance, idx)$
$self.get_instance_io_config(instance)$
END_DATA_BLOCK

'''

            params['object_call'] += '''    "CPC_FC_AO"(DB_AO := "$name$");''' + self.CRLF

        params['Communicated_blocks'] = (2 * self.CRLF).join(
            [self.process_template("TIAInst_ObjectManReq_Template.scl", params),
             self.process_template("TIAInst_ObjectEvent_Template.scl", params),
             self.process_template("TIAInst_ObjectBinStatus_Template.scl", params),
             self.process_template("TIAInst_ObjectAnaStatus_Template.scl", params)])

        self.writeDeviceInstances(self.process_template("TIAInst_Object_Template.scl", params))
        self.writeFCDeviceInstances(self.process_template("TIAInst_Object_FC_Template.scl", params))

    def get_instance_assignment(self, instance, idx):
        instance_assignment = '''    index := %(index)s;
    Diagnostics := %(diagnostics)s;
    PMaxRan := %(PMaxRan)s;
    PMinRan := %(PMinRan)s;
    PMinRaw := %(PMinRaw)s;
    PMaxRaw := %(PMaxRaw)s;
    FEType := %(FEType)s;'''
        params = {
            'index': idx,
            'diagnostics': "TRUE" if self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel") else "FALSE",
            'PMaxRan': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Max")),
            'PMinRan': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Min")),
            'PMaxRaw': instance.getAttributeData("FEDeviceParameters:Raw Max"),
            'PMinRaw': instance.getAttributeData("FEDeviceParameters:Raw Min"),
            'FEType': self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0")
        }
        return instance_assignment % params

    @staticmethod
    def get_instance_io_config(instance):
        result = []
        fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
        if fe_type == '1':
            interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
            if interface_param1.startswith('p'):
                pqw_def = 'true'
                interface_param1 = interface_param1.replace('pqw', '').replace('pqb', '')
            else:
                pqw_def = 'false'
                interface_param1 = interface_param1.replace('qw', '').replace('qb', '')
            result.append('''	perAddress := %s;''' % interface_param1)
            result.append('''	PqwDef := %s;''' % pqw_def)
        return '\r\n'.join(result)
