# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class DigitalAlarm_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        instance_amount = len(instance_list)

        fast_interlock_device = list(self.thePlugin.getInstancesFI("DigitalAlarm"))
        if len(fast_interlock_device):
            instance_amount_FI = len(fast_interlock_device)
            instance_amount -= instance_amount_FI
            params_FI = {'object': 'DA',
                         'object_call': '',
                         'instance_amount': instance_amount_FI,
                         'DB_object_ManRequest': '',
                         'FI': '_FI'}

        params = {'object': 'DA',
                  'Communicated_blocks': '',
                  'DB_object': '',
                  'object_call': '',
                  'instance_amount': instance_amount,
                  'DB_object_ManRequest': '',
                  'FI': ''}

        for instance in instance_list:
            name = instance.getAttributeData("DeviceIdentification:Name")

            params['DB_object'] += '''DATA_BLOCK "$name$" "CPC_FB_DA"
BEGIN
$self.get_instance_assignment(instance, instance_list, fast_interlock_device)$
END_DATA_BLOCK

'''

            if instance in fast_interlock_device:
                params['object_call'] += '''    // Delay interrupt OBs until EN_AIRT is called
    NbOfDelayedInterrupts := DIS_AIRT();
''' + self.get_instance_call(name, True) + '''    // Reenable interrupt OBs
    NbOfQueuedInterrupts := EN_AIRT();
'''
                params_FI['object_call'] += self.get_instance_call(name, True)
            else:
                params['object_call'] += self.get_instance_call(name, False)

        params['Communicated_blocks'] = (2 * self.CRLF).join(
            [self.process_template("TIAInst_ObjectManReq_Template.scl", params),
             self.process_template("TIAInst_ObjectEvent_Template.scl", params),
             self.process_template("TIAInst_ObjectBinStatus_Template.scl", params)])

        if len(fast_interlock_device):
            params['Communicated_blocks'] += 2 * self.CRLF + (2 * self.CRLF).join(
                [self.process_template("TIAInst_ObjectManReq_Template.scl", params_FI),
                 self.process_template("TIAInst_ObjectEvent_Template.scl", params_FI),
                 self.process_template("TIAInst_ObjectBinStatus_Template.scl", params_FI)])

        self.writeDeviceInstances(self.process_template("TIAInst_Object_Template.scl", params))
        if len(fast_interlock_device):
            self.writeFCDeviceInstances(self.process_template("TIAInst_Object_FC_Template.scl", params) + 2 * self.CRLF
                                        + self.process_template("TIAInst_Object_FC_Template.scl", params_FI))
        else:
            self.writeFCDeviceInstances(self.process_template("TIAInst_Object_FC_Template.scl", params))

    def get_instance_assignment(self, instance, instance_list, instance_list_FI):
        instance_assignment = '''    index := %(index)s;
    PAuAckAl := %(PAuAckAl)s;
    %(PAlDt)s'''

        params = {'index': 0}
        if instance in instance_list_FI:
            params['index'] = instance_list_FI.index(instance)
        else:
            original_list = list(instance_list)
            for instance_FI in instance_list_FI:
                original_list.remove(instance_FI)
            params['index'] = original_list.index(instance)
        params['index'] += 1

        auto_acknowledgement = instance.getAttributeData('FEDeviceAlarm:Auto Acknowledge').lower().strip()
        params['PAuAckAl'] = 'TRUE' if auto_acknowledgement == 'true' else 'FALSE'

        delay = instance.getAttributeData("FEDeviceParameters:Alarm Delay (s)").strip()
        if delay == "":
            params['PAlDt'] = '''PAlDt := 0;'''
        elif self.thePlugin.isString(delay):
            params['PAlDt'] = '''// The Alarm Delay is defined in the logic'''
        else:
            params['PAlDt'] = '''PAlDt := %s;''' % (int(round(float(delay))))

        return instance_assignment % params

    def get_instance_call(self, name, fast_interlock_device):
        FI = "_FI" if fast_interlock_device else ""
        return '''    "CPC_FC_DA$FI$"(DB_DA := "$name$");
'''
