# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template
from research.ch.cern.unicos.utilities import DeviceTypeFactory


class Communication_Template(TIAInst_Generic_Template):

    def process(self, unicos_project, xml_config, generate_global_files, *_):
        """
        General Steps for the Communication file:
        1. DB_WinCCOA creation. This DB contains the TSPP parameters for WinCCOA
        2. DB_COMM creation. This DB contains all numbers of status DB
        3. DB_EventData creation. This DB contains pointers on evstsreg which have been changed
        4. FC_TSPP creation. This FUNCTION calls the TSPP manager function
        5. FC_Event creation. This FUNCTION generates an event for evstsreg01 and calls the TS_EVENT_MANAGER

        :param unicos_project:
        :param xml_config:
        :param generate_global_files: comes from "Global files scope" dropdown on Wizard.
                true = All types. false = Selected types.
        """
        xml_config = self.thePlugin.getXMLConfig() # TODO: remove this 
        tiaDeclarations = xml_config.getTiaPLCDeclarations()
        thePLCName = tiaDeclarations.get(0).getName()
        plcType = xml_config.getPLCParameter(thePLCName + ":PLCType")
        resource_package_version = self.thePlugin.getResourcesVersion()
        # remove the "-beta-..." if it exists
        pos = resource_package_version.find("-")
        if pos > 0:
            resource_package_version = resource_package_version[:pos]
        major_version, minor_version, small_version = resource_package_version.split(".")

        params = {
            'application_version': xml_config.getConfigInfoParameter("version"),
            'major_version': major_version,
            'minor_version': minor_version,
            'small_version': small_version,
            'db_comm_assignment': [],
            'db_comm_amount': 0,
            'EventBufferSize': xml_config.getPLCParameter(
                "SiemensSpecificParameters:GeneralConfiguration:EventBufferSize"),
            'events_assignments': [],
            'local_id': xml_config.getPLCParameter("SiemensSpecificParameters:PLCS7Connection:LocalId"),
            'FC_Event_FI': ''
        }

        device_type_definitions = DeviceTypeFactory.getInstance()
        types_to_process = self.get_types_to_process(self.thePlugin, unicos_project, xml_config, generate_global_files)
        FI_instance_list = self.thePlugin.getInstancesFI("DigitalAlarm")
        fast_interlock_present = len(FI_instance_list) != 0  # Fast interlock presence detected by any fast interlock
        # digital alarm
        if fast_interlock_present:
            params['FC_Event_FI'] = self.get_FI_event(params['EventBufferSize'], types_to_process, plcType)
        if xml_config.getPLCParameter("RecipeParameters:GenerateBuffers").strip().lower() == "true":
            params['db_comm_amount'] += 1
            params['db_comm_assignment'].append(self.get_recipe_comm_db(xml_config))
        for device_type, instance_amount in types_to_process.iteritems():
            device_type_name = device_type.getDeviceTypeName()
            representation_name = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", device_type_name)
            FI_instance_list = self.thePlugin.getInstancesFI(device_type_name)
            fast_interlock_device_present = not FI_instance_list.isEmpty()
            if fast_interlock_device_present:
                instance_amount = instance_amount - len(FI_instance_list)  # instance amount of the not fast interlock
                # objects

            size = self.thePlugin.getDeviceCommSize(device_type_name, "BIN")
            if size > 0:
                if fast_interlock_device_present:
                    params['db_comm_amount'] += 1
                    params['db_comm_assignment'].append(self.get_comm_db(
                        "DB_bin_Status_", representation_name, size, params['db_comm_amount'], len(FI_instance_list),
                        True))
                params['db_comm_amount'] += 1
                params['db_comm_assignment'].append(self.get_comm_db(
                    "DB_bin_Status_", representation_name, size, params['db_comm_amount'], instance_amount, False))
            size = self.thePlugin.getDeviceCommSize(device_type_name, "ANA")
            if size > 0:
                if fast_interlock_device_present:
                    params['db_comm_amount'] += 1
                    params['db_comm_assignment'].append(self.get_comm_db(
                        "DB_ana_Status_", representation_name, size, params['db_comm_amount'], len(FI_instance_list),
                        True))
                params['db_comm_amount'] += 1
                params['db_comm_assignment'].append(self.get_comm_db(
                    "DB_ana_Status_", representation_name, size, params['db_comm_amount'], instance_amount, False))

        for device_type in types_to_process:
            device_type_name = device_type.getDeviceTypeName()

            FI_instance_list = self.thePlugin.getInstancesFI(device_type_name)
            fast_interlock_device_present = not FI_instance_list.isEmpty()

            attributeFamilyList = device_type_definitions.getDeviceType(device_type_name).getAttributeFamily()
            representation_name = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", device_type_name)
            for attributeFamily in attributeFamilyList:
                attributeFamilyName = attributeFamily.getAttributeFamilyName()
                if attributeFamilyName == "FEDeviceOutputs":
                    attributes = [attr.getAttributeName() for attr in attributeFamily.getAttribute()]
                    if "StsReg02" in attributes:
                        addr_shift = 8
                    else:
                        addr_shift = 4
                    if "StsReg01" in attributes:
                        if fast_interlock_device_present:
                            params['events_assignments'].append(self.get_event_db(
                                "StsReg01", representation_name, addr_shift, plcType, False, True))  # Fast interlocks,
                            # normal processing
                        params['events_assignments'].append(self.get_event_db(
                            "StsReg01", representation_name, addr_shift, plcType, False, False))
                    if "StsReg02" in attributes:
                        if fast_interlock_device_present:
                            params['events_assignments'].append(self.get_event_db(
                                "StsReg02", representation_name, addr_shift, plcType, False, True))  # Fast interlocks,
                            # normal processing
                        params['events_assignments'].append(self.get_event_db(
                            "StsReg02", representation_name, addr_shift, plcType, False, False))

        output = self.process_template("TIAInst_Communication_Template.scl", params)
        self.thePlugin.writeInstanceInfo("COMMUNICATION.scl", output)

    def get_FI_event(self, EventBufferSize, types_to_process, plcType):
        params = {'EventBufferSize': EventBufferSize,
                  'events_assignments': [],
                  'events_overwrite': []}

        device_type_definitions = DeviceTypeFactory.getInstance()

        for device_type in types_to_process:
            device_type_name = device_type.getDeviceTypeName()
            FI_instance_list = self.thePlugin.getInstancesFI(device_type_name)
            fast_interlock_device_present = not FI_instance_list.isEmpty()

            if fast_interlock_device_present:
                attributeFamilyList = device_type_definitions.getDeviceType(device_type_name).getAttributeFamily()
                representation_name = self.thePlugin.getTargetDeviceInformationParam(
                    "RepresentationName", device_type_name)
                for attributeFamily in attributeFamilyList:
                    attributeFamilyName = attributeFamily.getAttributeFamilyName()
                    if attributeFamilyName == "FEDeviceOutputs":
                        attributes = [attr.getAttributeName() for attr in attributeFamily.getAttribute()]
                        if "StsReg02" in attributes:
                            addr_shift = 8
                        else:
                            addr_shift = 4
                        if "StsReg01" in attributes:
                            params['events_assignments'].append(self.get_event_db(
                                "StsReg01", representation_name, addr_shift, plcType, True, True))  # Fast interlocks,
                            # fast interlock processing
                            params['events_overwrite'].append(self.get_event_copy_FI("StsReg01", representation_name))
                        if "StsReg02" in attributes:
                            params['events_assignments'].append(self.get_event_db(
                                "StsReg02", representation_name, addr_shift, plcType, True, True))  # Fast interlocks,
                            # fast interlock processing
                            params['events_overwrite'].append(self.get_event_copy_FI("StsReg02", representation_name))
        return self.process_template("TIAInst_Communication_Template_FI.scl", params)

    def get_comm_db(self, name_prefix, representation_name, size, index, instance_amount, FI_treatment):
        comm_db = '''   (* Global %(type)s status DB of %(device_name)s *)
   DB_List[%(index)s].Status_DB := %(status_db)s;
   DB_List[%(index)s].Status_DB_old := %(status_db_old)s;
   DB_List[%(index)s].size := %(size)s;
   // nbDB:= %(index)s'''

        params = {
            'index': index,
            'device_name': representation_name,
            'size': size * instance_amount,
            'status_db': self.thePlugin.getAddress(name_prefix + representation_name),
            'status_db_old': self.thePlugin.getAddress(name_prefix + representation_name + "_old"),
            'type': "Binary" if "_bin_" in name_prefix else "Analog"
        }

        if FI_treatment:
            params['status_db'] = self.thePlugin.getAddress(name_prefix + representation_name + "_FI")
            params['status_db_old'] = self.thePlugin.getAddress(name_prefix + representation_name + "_FI_old")

        return comm_db % params

    def get_recipe_comm_db(self, xml_config):
        recipe_assignment_scl = '''   (* Recipes interface DB *)
   DB_List[1].status_DB := %(Status_DB)s;
   DB_List[1].status_DB_old := %(Status_DB_old)s;
   DB_List[1].size := %(size)s;
   // nbDB:= 1'''
        params = {
            'Status_DB': self.thePlugin.getAddress("DB_RECIPES_INTERFACE"),
            'Status_DB_old': self.thePlugin.getAddress("DB_RECIPES_INTERFACE_old"),
            'size': int(xml_config.getPLCParameter("RecipeParameters:HeaderBufferSize")) * 4 + 2
        }

        return recipe_assignment_scl % params

    @staticmethod
    def get_event_db(stsreg_name, device_name, addr_shift, platform="S7-300", FI_DB_event = False, FI_block = False):
        full_device_name = device_name + "_FI" if FI_block else device_name

        event_db = '''    (* Test if %(event_name)s of %(device_name)s have changed *)
    FOR #i := 1 TO "DB_Event_%(full_device_name)s".Nb_%(device_name)s DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD(%(event_pin)s) <> DWORD_TO_WORD(ROR(IN := %(event_pin)s, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "%(DB_name)s".List_Event[#j].S7_ID := B#16#10;
        "%(DB_name)s".List_Event[#j].DataType := 6; // DWORD
        "%(DB_name)s".List_Event[#j].NbOfData := 1;
        "%(DB_name)s".List_Event[#j].DBNumber := %(convertion_function)s"DB_Event_%(full_device_name)s"));
        "%(DB_name)s".List_Event[#j].Address := SHL(IN := INT_TO_DWORD(%(addr_position)s), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    '''
        event_name = 'ev' + stsreg_name
        event_pin = '''"DB_Event_%(full_device_name)s".%(device_name)s_evstsreg[#i].%(event_name)s''' % \
                    {'full_device_name': full_device_name, 'device_name': device_name,  'event_name': event_name}
        if stsreg_name == "StsReg01":
            addr_position = '''(#i - 1) * %s + #k''' % addr_shift
        else:
            addr_position = '''(#i - 1) * %s + #k + 4''' % addr_shift
        params = {
            'device_name': device_name,
            'event_name': event_name,
            'event_pin': event_pin,
            'addr_position': addr_position,
            'convertion_function': '''UINT_TO_INT(DB_ANY_TO_UINT(''' if "1500" in platform else
            '''WORD_TO_INT(BLOCK_DB_TO_WORD(''',
            'full_device_name': full_device_name,
            'DB_name': "DB_FIEvent" if FI_DB_event else "DB_EventData"
        }
        if stsreg_name == "StsReg02":
            # TODO can it be removed?
            first_obj = '''    #First_obj := "DB_Event_%s".Nb_%s;\r\n''' % (full_device_name, device_name)
            event_db = first_obj + event_db
        return event_db % params

    def get_event_copy_FI(self, stsreg_name, device_name):
        event_db = '''(*Copy actual to old of %(event_name)s of %(device_name)s*)
FOR i:= 1 TO DB_event_%(device_name)s_FI.Nb_%(device_name)s DO
    //if new_stsreg <> old_stsreg
    IF DWORD_TO_WORD(%(event_pin)s) <> DWORD_TO_WORD(ROR(IN:=%(event_pin)s, N:= 16)) THEN
        tempsts := DB_bin_status_%(device_name)s_FI.StsReg01[#i].%(stsreg_name)s;
        %(event_pin)s := DB_bin_status_%(device_name)s_FI.StsReg01[#i].%(stsreg_name)s OR ROR(IN:=tempsts, N:=16);
    END_IF;
END_FOR;
'''
        event_name = 'ev' + stsreg_name.lower()
        event_pin = "DB_event_%(device_name)s_FI.%(device_name)s_evstsreg[i].%(event_name)s" % \
                    {'device_name': device_name,  'event_name': event_name}
        params = {
            'device_name': device_name,
            'event_name': event_name,
            'event_pin': event_pin,
            'stsreg_name': stsreg_name
        }

        return event_db % params
