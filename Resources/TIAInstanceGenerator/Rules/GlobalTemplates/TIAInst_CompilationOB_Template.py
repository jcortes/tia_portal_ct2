# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class CompilationOB_Template(TIAInst_Generic_Template):

    def process(self, unicos_project, xml_config, generate_global_files, *_):
        """
        General Steps for the Compilation file:
        1. Starting OB (100,102)
        2. Diagnostic OB (82)
        3. Errors and Hot restart: 81, 83, 84, 86, 101 (only S7-400)
        4. OB1(main cycle)
        5. Call the Recipes mechanism and provide the right values form WinCCOA
        6. OB32 (UNICOS live counter)
        7. Cyclic Interrupt OB (sampling PIDs)
        8. All other errors treatment are included in the baseline

        :param unicos_project:
        :param xml_config:
        :param generate_global_files: comes from "Global files scope" dropdown on Wizard.
                true = All types. false = Selected types.
        """
        params = {
            'DIAGNOSTICS': '',
            'OB100': "",
            'OB82': "",
            'OB1_1': "", 'OB1_2': "", 'OB1_3': "", 'OB1_4': "",
            'DB_RECIPES': "",
            'OB1_COUNTER': "", 'OB32': "",
            'OB86': "",
            'Cyclic_Interrupt_OB': "",
            'FI': ""
        }
        xml_config = self.thePlugin.getXMLConfig() # TODO: remove this 
        plc_declaration = xml_config.getPLCDeclarations()[0]
        plc_type = str(plc_declaration.getPLCType().getValue())

        types_to_process = self.get_types_to_process(self.thePlugin, unicos_project, xml_config, generate_global_files)
        type_names_to_process = [device_type.getDeviceTypeName() for device_type in types_to_process]

        diagnostic = self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel")
        params['OB100'] = '''(*RESTART (WARM RESTART)*********************************************)
''' + self.get_OB10x(100, 'Controller' in type_names_to_process, diagnostic)

        FIdeviceVector = self.thePlugin.getInstancesFI("DigitalAlarm")
        fast_interlocks_present = not FIdeviceVector.isEmpty() # Fast interlock presence detected by any fast interlock digital alarm
        if fast_interlocks_present:
            params['FI'] = self.get_FI_block(unicos_project)
            
        # order of devices is important and hard-coded in this file
        if "DigitalInput" in type_names_to_process:
            params['OB1_1'] += '''	"FC_DI"();   // Digital inputs\n'''
        if "AnalogInput" in type_names_to_process:
            params['OB1_1'] += '''	"FC_AI"();   // Analog inputs\n'''
        if "AnalogInputReal" in type_names_to_process:
            params['OB1_1'] += '''	"FC_AIR"();   // Analog inputs Real\n'''
        if "Encoder" in type_names_to_process:
            params['OB1_1'] += '''	"FC_ENC"();   // Encoders\n'''
        if "Controller" in type_names_to_process:
            params['OB1_2'] += '''	(* Controller functions called in FC_PCO_LOGIC(). Activate only if no PID Logic declared *)\n'''
            params['OB1_2'] += '''	// "FC_CONTROLLER"(group := 0); All Controllers running (option=0)\n'''
        if "ProcessControlObject" in type_names_to_process:
            params['OB1_2'] += '''	(*Process Logic Control *)\n'''
            params['OB1_2'] += '''	"FC_PCO_LOGIC"();   // Process Control object logic\n'''

        for device_type, instance_amount in types_to_process.iteritems():
            device_type_name = device_type.getDeviceTypeName()
            family_name = device_type.getObjectType()
            representation_name = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", device_type_name)
            if family_name == "InterfaceObjectFamily":
                params['OB1_1'] += '''	"FC_%s"();   // %s objects\n''' % (representation_name, device_type_name)
            elif family_name == "FieldObjectFamily":
                params['OB1_3'] += '''	"FC_%(repr)s"();   // %(type)s objects\n''' % {'repr': representation_name, 'type': device_type_name}

        if "DigitalAlarm" in type_names_to_process:
            params['OB1_3'] += '''	"FC_DA"();   // DIGITAL ALARM objects\n'''
        if "AnalogAlarm" in type_names_to_process:
            params['OB1_3'] += '''	"FC_AA"();   // ANALOG ALARM objects\n'''
        if "DigitalOutput" in type_names_to_process:
            params['OB1_4'] += '''	"FC_DO"();   // Digital Outputs\n'''
        if "AnalogOutput" in type_names_to_process:
            params['OB1_4'] += '''	"FC_AO"();   // Analog Outputs\n'''
        if "AnalogOutputReal" in type_names_to_process:
            params['OB1_4'] += '''	"FC_AOR"();   // Analog Outputs REAL\n'''

        params['DB_RECIPES'] = self.get_recipe_db(xml_config)

        if plc_type.startswith("S7-300") or plc_type.startswith("s7-1500"):
            params['OB1_COUNTER'] = '''	// 1 second counter to simulate OB32 in a S7-300
	"CPC_GLOBAL_VARS".UNICOS_Counter1 := "CPC_GLOBAL_VARS".UNICOS_Counter1 + #OB1_PREV_CYCLE;
	IF "CPC_GLOBAL_VARS".UNICOS_Counter1 > 1000 THEN
	  "CPC_GLOBAL_VARS".UNICOS_Counter1 := "CPC_GLOBAL_VARS".UNICOS_Counter1 - 1000;
	  "CPC_GLOBAL_VARS".UNICOS_LiveCounter := "CPC_GLOBAL_VARS".UNICOS_LiveCounter + 1;
	END_IF;'''
        else:
            params['OB32'] = '''
// UNICOS Live Counter 
ORGANIZATION_BLOCK "Cyclic Interrupt_1"
{ S7_Optimized_Access := 'TRUE' }
   VAR_INPUT 
      Initial_Call : Bool;
      Event_Count : Int;
   END_VAR


BEGIN
	"CPC_GLOBAL_VARS".UNICOS_LiveCounter := "CPC_GLOBAL_VARS".UNICOS_LiveCounter + 1;
END_ORGANIZATION_BLOCK
'''

        if diagnostic == 'true':
            params['DIAGNOSTICS'] = self.get_diagnostics_blocks()
            params['OB82'] = '''	"CPC_IOERROR_ADDRESS"(Mode := "CPC_GLOBAL_VARS".Channel_error_call);'''
            params['OB86'] = '''	"CPC_IOERROR_ADDRESS"(Mode := "CPC_GLOBAL_VARS".Station_error_call);'''

        if 'Controller' not in type_names_to_process:
            params['Cyclic_Interrupt_OB'] = '''// Scheduling: No controller defintion'''
        else:
            params['Cyclic_Interrupt_OB'] = '''// PID sampling time management
ORGANIZATION_BLOCK "Cyclic Interrupt_2"
{ S7_Optimized_Access := 'TRUE' }
   VAR_TEMP 
      I : Int;
   END_VAR


BEGIN

"CPC_SCHED"(PID_EXEC_CYCLE := "CPC_GLOBAL_VARS".PID_EXEC_CYCLE);
	
	// Reset bit activated DB_SCHED
	FOR #I := 1 TO %s DO
	  IF "DB_SCHED".LOOP_DAT[#I].ENABLE THEN
	    "FC_CONTROLLER"(group := #I);
	    "DB_SCHED".LOOP_DAT[#I].ENABLE := false;
	  END_IF;
	END_FOR;
END_ORGANIZATION_BLOCK''' % self.thePlugin.getGroupMaxNb()

        output = self.process_template("TIAInst_CompilationOB_Template.scl", params)
        self.thePlugin.writeInstanceInfo("4_Compilation_OB.scl", output)

    def get_FI_block(self, unicos_project):
        fc_block = '''(*%(OB_Name)s**************************************************************************)
ORGANIZATION_BLOCK "%(OB_Name)s" 
TITLE = '%(Type_interrupt)s Interrupt'
{ S7_Optimized_Access := 'TRUE' }
//
// %(Call_Type)s
//
AUTHOR: 'ICE/PLC'
FAMILY: 'UNICOS'
VAR_TEMP
    (*UNUSED VARIABLE*)
    UNUSED_VARIABLE : ARRAY [0..20] OF BYTE;
%(CI_Vars)s
END_VAR
BEGIN
    (*Getting inputs*)
%(FI_1)s
%(FI_2)s
%(FI_3)s
    (*UNICOS objects calculating*)
%(FI_4)s
    (*Setting Outputs*)
%(FI_5)s
     
    //Calling TS_Event_Manager for TSPP communication with the SCADA
    FC_Event_FI();
%(CI_end)s    
END_ORGANIZATION_BLOCK
'''

        params = {
            'Type_interrupt': "",
            'OB_Name': "",
            'Call_Type': "",
            'FI_1': "",
            'FI_2': "",
            'FI_3': "",
            'FI_4': "",
            'FI_5': "",
            'CI_Vars': "",
            'CI_end': ""
        }

        DI_FI_instance_list = self.thePlugin.getInstancesFI("DigitalInput")
        fast_interlocks_DI_present = len(DI_FI_instance_list) != 0
        ONOFF_FI_instance_list = self.thePlugin.getInstancesFI("OnOff")
        fast_interlocks_OnOff_present = len(ONOFF_FI_instance_list) != 0
        DA_FI_instance_list = self.thePlugin.getInstancesFI("DigitalAlarm")
        fast_interlocks_DA_present = len(DA_FI_instance_list) != 0
        DO_FI_instance_list = self.thePlugin.getInstancesFI("DigitalOutput")
        fast_interlocks_DO_present = len(DO_FI_instance_list) != 0

        if fast_interlocks_OnOff_present:
            params['FI_3'] += '''    (*Process Logic Control*)
    FC_FI_LOGIC();  // Process Control object logic\n'''
            params['FI_4'] += '''    FC_ONOFF_FI();    // OnOff objects\n'''
        if fast_interlocks_DA_present:
            params['FI_2'] += '''    FC_FI_LOGIC();
    FC_DA_FI();   // DIGITAL ALARM objects\n'''
        if fast_interlocks_DO_present:
            params['FI_5'] += '''    FC_DO_FI();    //Digital Outputs\n'''

        interrupt_type = DA_FI_instance_list.iterator().next().getAttributeData("LogicDeviceDefinitions:Fast Interlock Type")
        if interrupt_type == "Hardware Interrupt":
            params["OB_Name"] = "Hardware Interrupt"
            params["Type_interrupt"] = "Hardware"
            params["Call_Type"] = "Called after a hardware interrupt trigger"
            if fast_interlocks_DI_present:
                params['FI_1'] += '''    FC_DI_FI();    // Digital inputs\n'''
        elif interrupt_type == "Cyclic Interrupt":
            params["OB_Name"] = "Cyclic Interrupt_4"
            params["Type_interrupt"] = "Cyclic"
            params["Call_Type"] = "Called every user-defined cyclic time"
            params["CI_Vars"] = '''    Dis_error : INT;
    En_error : INT;'''
            params["CI_end"] = '''    // Reenable interrupt OB
    En_error := EN_IRT(MODE := B#16#02, OB_NR :=  "OB_Cyclic Interrupt_4");
END_IF;'''
            if fast_interlocks_DI_present:
                params['FI_1'] += '''// Check digital input change
IF CI_DI_Check() THEN
    // Disable interrupt OB until EN_IRT is called
    Dis_error := DIS_IRT(MODE := B#16#02, OB_NR := "OB_Cyclic Interrupt_4");
    
    //Calling TS_Event_Manager for TSPP communication with the SCADA
    FC_Event_FI();
'''
                fc_block = self.get_DI_check_FC(DI_FI_instance_list) + "\n\n" + fc_block

        return fc_block % params

    def get_DI_check_FC(self, DI_FI_instance_list):
        return '''FUNCTION "CI_DI_Check" : Bool
{ S7_Optimized_Access := 'TRUE' }
//
// Check DI_FI changes and call TSPP if needed
VAR_TEMP
    TempEvStsReg : Array [1..$len(DI_FI_instance_list)$] of DI_event;
    index : Int;
END_VAR

BEGIN
    #CI_DI_Check := FALSE;
    // Save old DI events
    FOR #index := 1 TO $len(DI_FI_instance_list)$ DO
        #TempEvStsReg[#index].evStsReg01 := "DB_Event_DI_FI".DI_evstsreg[#index].evStsReg01;
    END_FOR;
    "FC_DI_FI"();    // Digital inputs
    FOR #index := 1 TO $len(DI_FI_instance_list)$ DO
        IF (DWORD_TO_WORD(#TempEvStsReg[#index].evStsReg01) <> DWORD_TO_WORD(ROR(IN := #TempEvStsReg[#index].evStsReg01, N := 16))) OR
          (DWORD_TO_WORD("DB_Event_DI_FI".DI_evstsreg[#index].evStsReg01) <> DWORD_TO_WORD(ROR(IN := "DB_Event_DI_FI".DI_evstsreg[#index].evStsReg01, N := 16))) THEN
            "DB_Event_DI_FI".DI_evstsreg[#index].evStsReg01 := (#TempEvStsReg[#index].evStsReg01 AND DW#16#FFFF0000) OR ("DB_Event_DI_FI".DI_evstsreg[#index].evStsReg01 AND DW#16#0000FFFF); 
            #CI_DI_Check := TRUE;
        END_IF; 
    END_FOR;
  
END_FUNCTION'''

    def get_diagnostics_blocks(self):
        params = {'objects_call': ''}
        device_types = ["DigitalInput", "DigitalOutput", "AnalogInput", "AnalogOutput"]
        for device_type in device_types:
            for instance in self.thePlugin.getUnicosProject().getDeviceType(device_type).getAllDeviceTypeInstances():
                if instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type").strip() == "1":
                    if device_type.startswith("Digital"):
                        byte_addr, bit_addr = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower().replace("i","").replace("q","").split(".")
                        params['objects_call'] += """"CPC_IOERROR_%(device_short)s"(Error := #Error,
              InterfaceParam1 := %(byte_addr)s,
              InterfaceParam2 := %(bit_addr)s,
              DB_%(device_short)s := "%(name)s"); // ADDRESS %(byte_addr)s.%(bit_addr)s
""" % {"device_short": self.thePlugin.getTargetDeviceInformationParam("RepresentationName", device_type), "byte_addr": byte_addr, "bit_addr": bit_addr, "name": instance.getAttributeData("DeviceIdentification:Name")}
                    else:
                        params['objects_call'] += """"CPC_IOERROR_%(device_short)s"(Error := #Error,
              InterfaceParam1 := %(byte_addr)s,
              DB_%(device_short)s := "%(name)s"); // ADDRESS %(byte_addr)s
""" % {"device_short": self.thePlugin.getTargetDeviceInformationParam("RepresentationName", device_type), "byte_addr": instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower().replace("piw","").replace("pqw","").replace("iw","").replace("qw",""), "name": instance.getAttributeData("DeviceIdentification:Name")}
        return self.process_template("TIAInst_IOError_Template.scl", params)

    def get_OB10x(self, OB_number, controller_exists_in_generation, diagnostic):
        OB10x_block = '''ORGANIZATION_BLOCK OB%(OB_number)s 
TITLE = '%(OB_type)s restart'
{ S7_Optimized_Access := 'TRUE' }
AUTHOR: 'UNICOS'
FAMILY: 'EXEC'
NAME: 'OB%(OB_number)s'
// 
// Called during warm restart
VAR_INPUT
    LostRetentive             : BOOL;
    LostRTC                   : BOOL;
END_VAR
VAR_TEMP
    Phase                     : UDINT;
    Status                    : WORD;
    QRY_CINT_ERROR            : INT;
    SET_CINT_ERROR            : INT;
    QRY_CINT_CYCLE            : UDINT;
END_VAR

BEGIN
//Time Smoothing (alarms)
    "CPC_GLOBAL_VARS".UNICOS_TimeSmooth := 10;

//Init of Live counter marker form Live counter datablock    
    "CPC_GLOBAL_VARS".UNICOS_LiveCounter := "DB_WINCCOA".UNICOS_LiveCounter;
    //Set cyclic time interval of LiveCounter cyclic OB to 1 second
    #SET_CINT_ERROR := SET_CINT(OB_NR := "OB_Cyclic Interrupt_1", CYCLE := 1000000, PHASE := 0);
    
    %(FC_CONTROLLER)s

    %(OB10x_DIAGNOSTIC)s

END_ORGANIZATION_BLOCK'''

        params = {
            'OB_number' : OB_number,
            'OB_type' : '',
            'OB10x_DIAGNOSTIC' : '',
            'FC_CONTROLLER' : ''
        }

        if OB_number == 100:
            params['OB_type'] = 'Warm'
        if OB_number == 101:
            params['OB_type'] = 'Hot'
        if OB_number == 102:
            params['OB_type'] = 'Cold'

        if controller_exists_in_generation:
            params['FC_CONTROLLER'] = '''FC_CONTROLLER(group:=0);// All Controllers running (option=0)\n'''
            min_cycle_time = None
            for controller in self.thePlugin.getUnicosProject().getDeviceType("Controller").getAllDeviceTypeInstances():
                current_cycle_time = controller.getAttributeData("FEDeviceParameters:Controller Parameters:PID Cycle (s)")
                if not min_cycle_time or current_cycle_time < min_cycle_time:
                    min_cycle_time = current_cycle_time
            params['FC_CONTROLLER'] += '''//Setting interrupt time interval for PID scheduler interrupt OB
    #SET_CINT_ERROR := SET_CINT(OB_NR := "OB_Cyclic Interrupt_2", CYCLE := REAL_TO_UDINT($min_cycle_time$ * 1000000), PHASE := 0);
    
//Querying cyclic interrupt settings from interrupt OB used for PID scheduler
    #QRY_CINT_ERROR := QRY_CINT(OB_NR := "OB_Cyclic Interrupt_2", CYCLE => #QRY_CINT_CYCLE, PHASE => #Phase, STATUS => #Status);
    "CPC_GLOBAL_VARS".PID_EXEC_CYCLE := UDINT_TO_TIME(#QRY_CINT_CYCLE / 1000);
'''
        if diagnostic == 'true':
            params['OB10x_DIAGNOSTIC'] = '''"CPC_IOERROR_ADDRESS"(Mode := "CPC_GLOBAL_VARS".Locate);'''

        return OB10x_block % params

    def get_recipe_db(self, xml_config):
    
        generate_buffers = xml_config.getPLCParameter("RecipeParameters:GenerateBuffers").strip().lower()
        if generate_buffers != "true":
            return ""
            
        recipe_db = '''
	// Calling the Recipes Mechanism and passing the information from WinCCOA (DB_RECIPES_INTERFACE) to the CPC_DB_RECIPES
	"CPC_DB_RECIPES"(
	                 Header := "DB_RECIPES_INTERFACE".RecipeHeader,   // Header from DB_RECIPES_INTERFACE to CPC_DB_RECIPES
	                 DBnum := %(DBnum)s,   // DB number of Recipe buffers
	                 HeaderBase := %(HeaderBase)s,   // Header buffer base address
	                 StatusBase := %(StatusBase)s,   // Recipe Status buffer base address
	                 ManRegAddrBase := %(ManRegAddrBase)s,   // Manual Requests addresses buffer base address
	                 ManRegValBase := %(ManRegValBase)s,   // Manual Reuqests values buffer base address
	                 ReqAddrBase := %(ReqAddrBase)s,   // Request Address buffer base address
	                 ReqValBase := %(ReqValBase)s,   // Request Values buffer base address
	                 BuffersBase := %(ManRegAddrBase)s,   // Buffers base address
	                 BuffersEnd := %(BuffersEnd)s,   // Buffers last address
	                 Timeout := T#%(Timeout)ss   // Recipe transfer timeout
	);
	
	"DB_RECIPES_INTERFACE".RecipeStatus := "CPC_DB_RECIPES".Status;   // Status from CPC_DB_RECIPES to DB_RECIPES_INTERFACE'''
        
        header_buffer_size = int(xml_config.getPLCParameter("RecipeParameters:HeaderBufferSize").strip())
        buffer_size_calculated = self.get_recipe_buffer_size(xml_config)
        params = {
            'DBnum': self.thePlugin.getAddress("DB_RECIPES_INTERFACE"),
            'HeaderBase': '2',
            'StatusBase': header_buffer_size * 2 + 2,
            'ManRegAddrBase': header_buffer_size * 4 + 2,
            'ManRegValBase': header_buffer_size * 4 + buffer_size_calculated * 2 + 2,
            'ReqAddrBase': header_buffer_size * 4 + buffer_size_calculated * 4 + 2,
            'ReqValBase': header_buffer_size * 4 + buffer_size_calculated * 6 + 2,
            'BuffersEnd': header_buffer_size * 4 + buffer_size_calculated * 10 + 2,
            'Timeout': xml_config.getPLCParameter("RecipeParameters:ActivationTimeout")
        }

        return recipe_db % params
