# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class CompilationInstances_Template(TIAInst_Generic_Template):

    def process(self, unicos_project, xml_config, generate_global_files, *_):
        """
        General Steps for the nstances compilation file:
        1. To compile the instances files the next order is required:
            - IOObjects
            - DPARObject (the others interface objects uses the DPAR object, so it must be compiled before them)
            - InterfaceObjects
            - FieldObject
            - ControlObject
            - COMMUNICATION
        Note: to follow this order, a set of vectors must be created to preprocess all the DeviceTypes and call the files in the right order

        :param unicos_project:
        :param xml_config:
        :param generate_global_files: comes from "Global files scope" dropdown on Wizard.
                true = All types. false = Selected types.
        """
        instances_scl = '''%(Recipes)s
"CPC_TSPP_Unicos.SCL"
%(IOObjectFamily)s
%(DPAR_objects)s
%(InterfaceObjectFamily)s
%(FieldObjectFamily)s
%(ControlObjectFamily)s
"COMMUNICATION.SCL"'''
        instances_part2_scl = '''%(IOObjectFamily)s
%(DPAR_objects)s
%(InterfaceObjectFamily)s
%(FieldObjectFamily)s
%(ControlObjectFamily)s
"4_Compilation_OB.SCL"'''
        params = {
            'IOObjectFamily': [],
            'DPAR_objects': [],
            'InterfaceObjectFamily': [],
            'FieldObjectFamily': [],
            'ControlObjectFamily': [],
            'Recipes': []
        }
        params_part2 = {
            'IOObjectFamily': [],
            'DPAR_objects': [],
            'InterfaceObjectFamily': [],
            'FieldObjectFamily': [],
            'ControlObjectFamily': [],
        }
        
        xml_config = self.thePlugin.getXMLConfig() # TODO: remove this 
        if xml_config.getPLCParameter("RecipeParameters:GenerateBuffers").strip().lower() == "true":
            params['Recipes'].append('''"Recipes.SCL"''')
        
        self.thePlugin.writeInUABLog("processInstances in Jython for Compilation.")
        types_to_process = self.get_types_to_process(self.thePlugin, unicos_project, xml_config, generate_global_files)

        for device_type in types_to_process:
            device_type_name = device_type.getDeviceTypeName()
            device_type_family = device_type.getObjectType()
            representation_name = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", device_type_name)
            if representation_name == "DPAR":
                params['DPAR_objects'].append('''"%(device)s.SCL"
"CPC_FC_%(device)s.SCL"''' % {'device': representation_name})
                params_part2['DPAR_objects'].append('''"FC_%s.SCL"''' % representation_name)
            elif device_type_family in params:
                params[device_type_family].append('''"%(device)s.SCL"
"CPC_FC_%(device)s.SCL"''' % {'device': representation_name})
                params_part2[device_type_family].append('''"FC_%s.SCL"''' % representation_name)
                if self.thePlugin.getInstancesFI(device_type_name):
                    params[device_type_family].append('''"CPC_FC_%(device)s_FI.SCL"''' % {'device': representation_name})
        for key in params:
            if key in params_part2:
                params_part2[key] = "\r\n".join(params_part2[key])
            params[key] = "\r\n".join(params[key])
        self.thePlugin.writeInstanceInfo("2_Compilation_instance.INP", instances_scl % params)
        self.thePlugin.writeInstanceInfo("4_Compilation_instance_part2.INP", instances_part2_scl % params_part2)
