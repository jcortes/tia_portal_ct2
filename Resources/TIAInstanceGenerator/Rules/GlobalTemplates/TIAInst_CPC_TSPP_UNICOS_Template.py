# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for CPC_TSPP_UNICOS Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from time import strftime

from TIAInst_Generic_Template import TIAInst_Generic_Template

from research.ch.cern.unicos.utilities import AbsolutePathBuilder
from java.io import File


class CPC_TSPP_UNICOS_Template(TIAInst_Generic_Template):

    def process(self, unicos_project, xml_config, generate_global_files, *_):
        
        xml_config = self.thePlugin.getXMLConfig() # TODO: remove this 
        tiaDeclarations = xml_config.getTiaPLCDeclarations()
        thePLCName = tiaDeclarations.get(0).getName()
        plcType = xml_config.getPLCParameter(thePLCName + ":PLCType")
        fast_interlock_DA_present = not self.thePlugin.getInstancesFI("DigitalAlarm").isEmpty()

        nbStatusTables = self.thePlugin.getSumNbStatusTable()
        if nbStatusTables > 150:
            maxTableInOneSend = 150
        else:
            maxTableInOneSend = nbStatusTables

        # constants to be set by user
        MaxNbOfTSEvent = 100           # Nb of event wich will trig the send to WinCCOA
        SpareEventNumber = 50            # spare places to be able to continue to ../.. record event when the send function is buzy
        Word_in_one_TSEvent = 2             # Number of data word in one Event
        MaxStatusTable = nbStatusTables
        # MaxStatusTable     = 100           # Max number of status tables (the length of one table is 200 bytes without the header)
        MaxTableInOneSend = maxTableInOneSend           # (MAX value S7-400:250  S7-300:100) Max nb of status tables in one TSPP message ; Must lower or equal to MaxStatusTable
        # MaxTableInOneSend  = 100          # (MAX value S7-400:250  S7-300:100) Max nb of status tables in one TSPP message ; Must lower or equal to MaxStatusTable
        ListEventSize = xml_config.getPLCParameter("SiemensSpecificParameters:GeneralConfiguration:EventBufferSize")
        # ListEventSize      = 1000          # Size of the Event List

        # calculated values
        ExtendedNbOfEvent = MaxNbOfTSEvent + SpareEventNumber         # MaxNbOfTSEvent + SpareEventNumber; // Full size of event buffer
        TSEventHeaderSize = 6                                         # TimeStamp + DB number + Address
        TSEventWordSize = Word_in_one_TSEvent + TSEventHeaderSize   # Word_in_one_TSEvent + TSEventHeaderSize;
        TSEventByteSize = TSEventWordSize * 2
        EventByteDataSize = Word_in_one_TSEvent * 2
        MaxStatusReqNb = MaxStatusTable
        StatusReqListSize = MaxStatusReqNb + 1
        StatusWordSize = 100                                       # Size of status table
        StatusByteSize = StatusWordSize * 2
        MaxReqNumber = 3
        ReqListSize = MaxReqNumber + 1
        MaxBufferSize_Event = 6 + (TSEventByteSize * ExtendedNbOfEvent)
        MaxBufferSize_Status = 6 + ((StatusByteSize + 12) * MaxTableInOneSend)

        MaxBufferSize = int(MaxBufferSize_Status + (MaxBufferSize_Event - MaxBufferSize_Status) * (1.0 / (1.0 + MaxBufferSize_Status / (MaxBufferSize_Event + 1))))
        EventExtension = (MaxBufferSize_Event - MaxBufferSize_Status) * (1 // (1 + (MaxBufferSize_Status // MaxBufferSize_Event)))

        # verification of values
        if (("S7-300".lower() == plcType.lower() and MaxTableInOneSend > 100) or (MaxTableInOneSend > 250)):
            self.thePlugin.writeErrorInUABLog("MaxTableInOneSend = " + MaxTableInOneSend + " is too large number for plc " + plcType)
        if (MaxTableInOneSend > MaxStatusTable):
            self.thePlugin.writeErrorInUABLog("MaxTableInOneSend value (" + MaxTableInOneSend + ") must be lower than MaxStatusTable value (" + MaxStatusTable + ")")

        
        params = {
            'ReqListSize'           : ReqListSize,
            'StatusReqListSize'     : StatusReqListSize,
            'Word_in_one_TSEvent'   : Word_in_one_TSEvent,
            'MaxNbOfTSEvent'        : MaxNbOfTSEvent,
            'SpareEventNumber'      : SpareEventNumber,
            'MaxStatusTable'        : MaxStatusTable,
            'MaxTableInOneSend'     : MaxTableInOneSend,
            'ListEventSize'         : ListEventSize,
            'ExtendedNbOfEvent'     : ExtendedNbOfEvent,
            'TSEventHeaderSize'     : TSEventHeaderSize,
            'TSEventWordSize'       : TSEventWordSize,
            'TSEventByteSize'       : TSEventByteSize,
            'EventByteDataSize'     : EventByteDataSize,
            'MaxStatusReqNb'        : MaxStatusReqNb,
            'StatusWordSize'        : StatusWordSize,
            'StatusByteSize'        : StatusByteSize,
            'MaxReqNumber'          : MaxReqNumber,
            'MaxBufferSize_Event'   : MaxBufferSize_Event,
            'MaxBufferSize_Status'  : MaxBufferSize_Status,
            'MaxBufferSize'         : MaxBufferSize,
            'EventExtension'        : EventExtension,
            'DB_pointer_type'       : "",
            'NumberOfTableDB'       : "",
            'TablesInDB'            : "",
            'LastTableSize'         : "",
            'ActualTableList'       : "",
            'OldTableList'          : "",
            'MoveActualToOld'       : "",
            'EventListElement'      : "",
            'TransferTimestamp'     : "",
            'TS_EventBuffer'        : "",
            'MoveActualToLocal'     : "",
            'MoveOldToLocal'        : "",
            'IPReceived'            : "",
            'TempCounter'           : "",
            'UpdateWatchdog'        : "",
            'EventBufferToBsend'    : "",
            'ActualToBsend'         : "",
            'FI_vars'               : "",
            'FI_start'              : "",
            'FI_end'                : "",
            'FI_TSPP'               : ""
        }
        
        if "S7-1500".lower() == plcType.lower():
            params['DB_pointer_type'] = '''DB_ANY'''
            params['NumberOfTableDB'] = '''PEEK_WORD(area := 16#84, dbNumber := #ListOfStatusTable, byteOffset := 0)'''
            params['TablesInDB']      = '''PEEK_WORD(area := 16#84, dbNumber := #ListOfStatusTable, byteOffset := (6 + ((#Index - 1) * 6)))'''
            params['LastTableSize']   = '''PEEK_WORD(area := 16#84, dbNumber := #ListOfStatusTable, byteOffset := (6 + ((#Index - 1) * 6)))'''
            params['ActualTableList'] = '''PEEK_WORD(area := 16#84, dbNumber := #ListOfStatusTable, byteOffset := (2 + ((#Index - 1) * 6)))'''
            params['OldTableList']    = '''PEEK_WORD(area := 16#84, dbNumber := #ListOfStatusTable, byteOffset := (4 + ((#Index - 1) * 6)))'''
            params['MoveActualToOld'] = '''	      POKE_BLK(
	               area_src := 16#84,
	               dbNumber_src := #CreateActual.DBNumber,
	               byteOffset_src := SHR(IN:=DWORD_TO_DINT(#CreateActual.Address),N:=3),
	               area_dest := 16#84,
	               dbNumber_dest := #CreateOld.DBNumber,
	               byteOffset_dest := SHR(IN:=DWORD_TO_DINT(#CreateOld.Address),N:=3),
	               count := #CreateActual.NbOfData
	      );
	      
	      #Result := 0; // !!'''
            params['EventListElement'] = '''	      #EventListElement.IDAndType := PEEK_WORD(area := 16#84, dbNumber := #EventListDB, byteOffset := #EventListAdr);
	      #EventListElement.DataAndDBNb := PEEK_DWORD(area := 16#84, dbNumber := #EventListDB, byteOffset := #EventListAdr + 2);
	      #EventListElement.Address := PEEK_DWORD(area := 16#84, dbNumber := #EventListDB, byteOffset := #EventListAdr + 6);'''
            params['TransferTimestamp'] = '''	          #TS_EventBuffer.Data[#CurrentEvent].TimeStamp := LDT_TO_DT(LWORD_TO_LDT(PEEK_LWORD(area := 16#84, dbNumber := #IN_Event.DBNumber, byteOffset := DWORD_TO_DINT(#IN_Event.Address))));
	          #Result := 0; // !!'''
            params['TS_EventBuffer'] = '''	        FOR #Index4 := 1 TO #Word_in_one_TSEvent DO
	          #TS_EventBuffer.Data[#CurrentEvent].Data[#Index4] := PEEK_WORD(area := 16#84,
	                                                                         dbNumber := #Event.DBNumber,
	                                                                         byteOffset := DWORD_TO_DINT(#TempAdr) + 2 * (#Index4 - 1));
	        END_FOR;
	        #Result := 0; // !!'''
            params['MoveActualToLocal'] = '''	      FOR #Index4 := 1 TO #CreateActual.NbOfData / 2 DO
	        #LocalActual[#Index4] := PEEK_WORD(area := 16#84,
	                                           dbNumber := #CreateActual.DBNumber,
	                                           byteOffset := DWORD_TO_DINT(SHR(IN:=#CreateActual.Address,N:=3)) + 2 * (#Index4 - 1));
	      END_FOR;
	      #Result := 0; // !!'''
            params['MoveOldToLocal'] = '''	      FOR #Index4 := 1 TO #CreateOld.NbOfData / 2 DO
	        #LocalOld[#Index4] := PEEK_WORD(area := 16#84,
	                                        dbNumber := #CreateOld.DBNumber,
	                                        byteOffset := DWORD_TO_DINT(SHR(IN:=#CreateOld.Address,N:=3)) + 2 * (#Index4 - 1));
	      END_FOR;
	      #Result := 0; // !!'''
            params['IPReceived'] = '''	  IF DWORD_TO_DINT(PEEK_DWORD(area := 16#84, dbNumber := WORD_TO_INT(#IN_WatchDog.DBNumber), byteOffset := 0)) <> -1 THEN // new IP received
	    POKE(area := 16#84,
	               dbNumber := WORD_TO_INT(#IN_WatchDog.DBNumber), 
	               byteOffset := 0, 
	               value := DINT_TO_DWORD(-1));'''
            params['TempCounter'] = '''#TempCounter := WORD_TO_INT(PEEK_WORD(area := 16#84, dbNumber := WORD_TO_INT(#IN_WatchDog.DBNumber), byteOffset := #DWNumber));'''
            params['UpdateWatchdog'] = '''POKE(area := 16#84,
	         dbNumber := WORD_TO_INT(#IN_WatchDog.DBNumber),
	         byteOffset := #DWNumber,
	         value := INT_TO_WORD(#TempCounter));'''
            params['EventBufferToBsend'] = '''	        #Result := BLKMOV(SRCBLK := #Src,
	                          DSTBLK => #BSendBuffer   // Struct
	        );'''
            params['ActualToBsend'] = '''	            FOR #Index4 := 1 TO #CreateActual.NbOfData / 2 DO
	              #BSendBuffer.TableList[#TSPPTableIndex].Table[#Index4] := PEEK_WORD(area := 16#84,
	                                                                                  dbNumber := #CreateActual.DBNumber,
	                                                                                  byteOffset := DWORD_TO_DINT(#TempAdr) + 2 * (#Index4 - 1));
	            END_FOR;
	            POKE_BLK(
	                     area_src := 16#84,
	                     dbNumber_src := #CreateActual.DBNumber,
	                     byteOffset_src := DWORD_TO_DINT(#TempAdr),
	                     area_dest := 16#84,
	                     dbNumber_dest := #CreateOld.DBNumber,
	                     byteOffset_dest := DWORD_TO_DINT(SHR(IN:=#CreateOld.Address,N:=3)),
	                     count := #CreateActual.NbOfData
	            );'''
        else:
            params['DB_pointer_type'] = "Block_DB"
            params['NumberOfTableDB'] = "#ListOfStatusTable.DW(0)"
            params['TablesInDB']      = "#ListOfStatusTable.DW(6 + ((#Index - 1) * 6))"
            params['LastTableSize']   = "#ListOfStatusTable.DW(6 + ((#Index - 1) * 6))"
            params['ActualTableList'] = "#ListOfStatusTable.DW(2 + ((#Index - 1) * 6))"
            params['OldTableList']    = "#ListOfStatusTable.DW(4 + ((#Index - 1) * 6))"
            params['MoveActualToOld'] = '''	      #Result := BLKMOV(
	                        SRCBLK :=  #ActualTable,
	                        DSTBLK => #OldTable
	      );'''
            params['EventListElement'] = '''	      #EventListElement.IDAndType := #EventListDB.DW(#EventListAdr);
	      #EventListElement.DataAndDBNb := #EventListDB.DD(#EventListAdr + 2);
	      #EventListElement.Address := #EventListDB.DD(#EventListAdr + 6);'''
            params['TransferTimestamp'] = '''	          #Result := BLKMOV(SRCBLK := #EventData,
	                            DSTBLK => #TS_EventBuffer.Data[#CurrentEvent].TimeStamp
	          ); // Transfer associated timestamp'''
            params['TS_EventBuffer'] = '''	        #Result := BLKMOV(SRCBLK := #EventPtr, // IN: Any
	                          DSTBLK => #TS_EventBuffer.Data[#CurrentEvent].Data // OUT: Any
	        ); // INT'''
            params['MoveActualToLocal'] = '''	      #Result := BLKMOV(SRCBLK :=  #ActualTable,   // IN: Any
	                        DSTBLK => #LocalActual   // OUT: Any
	      );                         // INT'''
            params['MoveOldToLocal'] = '''	      #Result := BLKMOV(SRCBLK :=  #OldTable,   // IN: Any
	                        DSTBLK => #LocalOld   // OUT: Any
	      );   // INT'''
            params['IPReceived'] = '''	  IF DWORD_TO_DINT(WORD_TO_BLOCK_DB(#IN_WatchDog.DBNumber).DD(0)) <> -1 THEN   // new IP received; corrected parentheses
	    WORD_TO_BLOCK_DB(#IN_WatchDog.DBNumber).DD(0) := DINT_TO_DWORD(-1);   // corrected parentheses'''
            params['TempCounter'] = '''#TempCounter := WORD_TO_INT(WORD_TO_BLOCK_DB(#IN_WatchDog.DBNumber).DW(#DWNumber));'''
            params['UpdateWatchdog'] = '''WORD_TO_BLOCK_DB(#IN_WatchDog.DBNumber).DW(#DWNumber):= INT_TO_WORD(#TempCounter);'''
            params['EventBufferToBsend'] = '''	        #ATSrc.NbOfData := #EventBufferSize;
	        #Result := BLKMOV(SRCBLK :=  #Src,   // IN: Any
	                          DSTBLK => #BSendBuffer   // OUT: Any
	        );   // INT '''
            params['ActualToBsend'] = '''                          #Result :=  BLKMOV(SRCBLK :=  #ActualTable,// Copy Actual table in BSend buffer
                                             DSTBLK => #BSendBuffer.TableList[#TSPPTableIndex].Table // OUT: Any
                                            ); // INT
                          
                          #Result :=  BLKMOV(SRCBLK :=  #ActualTable,// Copy actual table in old table
                                             DSTBLK => #OldTable // OUT: Any
                                            ); // INT'''

        if fast_interlock_DA_present:
            params['FI_vars'] = """  NbOfDelayedInterrupts : INT;
  NbOfQueuedInterrupts  : INT;"""
            params['FI_start'] = """  // Delay interrupt OBs until EN_AIRT is called
  NbOfDelayedInterrupts := DIS_AIRT();"""
            params['FI_end'] = """  // Reenable interrupt OBs
  NbOfQueuedInterrupts := EN_AIRT();"""
            params['FI_TSPP'] = """FUNCTION_BLOCK TSPP_FI
TITLE = 'TSPP_FI'
{ S7_Optimized_Access := 'FALSE' }
AUTHOR  : 'UNICOS'
FAMILY  : 'COMMS'
NAME    : 'COMM'
VERSION : '4.3'  //optimization of the redundant implementation

VAR_INPUT
  // Input Variables
      NewEvent : Bool;   // New event indication
      MultipleEvent : Bool;   // EventData point to a list of events
      EventListDB : DB_ANY;
      EventTSIncluded : Bool;   // Event(s) with(out)time stamp indication
      EventData : Any;   // Pointer to event
      IN_Event AT EventData : Struct   // NOT USED
         S7_ID : Byte;
         DataType : Byte;
         NbOfData : Int;
         DBNumber : Int;
         Address : DWord;
      END_STRUCT;
END_VAR

VAR_OUTPUT
      Error : Bool;   // Function error indicator
      Error_code : Word;   // Function error code
      Special_code : Word;   // Error code of internaly used functions
END_VAR

VAR_IN_OUT
      CurrentEvent : Int;
      ExtendedBufFull : Bool;
      TS_EventBuffer : Struct
         TSPP_ID1 : Char := 'T';
         TSPP_ID2 : Char := 'S';
         TSPP_ID3 : Char := 'P';
         Nb_of_TSWord : Byte := 2;   // = Word_in_one_TSEvent
         TS_Data_Length : Int;
         Data : Array[1..#ExtendedNbOfEvent] of Struct
            TimeStamp : Date_And_Time;
            DBNumber : Int;
            Address : Int;
            Data : Array[1..#Word_in_one_TSEvent] of Word;
         END_STRUCT;
      END_STRUCT;
      EventBufferSize : Int;
      BufferFullSendReq : Bool;
END_VAR

VAR
      ID_NewEvent : Int;
      ID_EventSent : Int;
END_VAR

VAR_TEMP
      Result : Int;
      NumberOfEvents : Int;
      EventListAdr : Int;
      EventListElemPtr : Any;
      EventListElement AT EventListElemPtr : Struct
         IDAndType : Word;
         DataAndDBNb : DWord;
         Address : DWord;
      END_STRUCT;   
      EventPtr : Any;
      Event AT EventPtr : Struct
         S7_ID : Byte;
         DataType : Byte;
         NbOfData : Int;
         DBNumber : Int;
         Address : DWord;
      END_STRUCT;
      CurrentInEvent : Int;
      InEventByteDataSize : Int;
      TempAdr : DWord;
      Er_Code : Int;
      Index4 : Int;
END_VAR

VAR CONSTANT 
      (* ############################################################################
      These parameters MUST be adjusted to fullfill the user requirements 
      You can change them in the jython template in \Resources\TIAInstanceGenerator\Rules\GlobalTemplates\TIAInst_CPC_TSPP_UNICOS_Template.py 
      Do not edit SCL file directly *)
      MaxNbOfTSEvent : Int := 100;   // Nb of event wich will trig the send to WinCCOA
      SpareEventNumber : Int := 50;   // spare places to be able to continue to ../.. record event when the send function is buzy
      Word_in_one_TSEvent : Int := 2;   // Number of data word in one Event
      MaxStatusTable : Int := 7;   // Max number of status tables (the length of one table is 200 bytes without the header)
      MaxTableInOneSend : Int := 7;   // (MAX value S7-400:250  S7-300:100); Max nb of status tables in one TSPP message; Must lower or equal to MaxStatusTable
      ListEventSize : Int := 1000;   // Size of the Event List
      (* ########################################################################### *)
      ExtendedNbOfEvent : Int := 150;   // Full size of event buffer
      TSEventHeaderSize : Int := 6;   // TimeStamp + DB number + Address
      TSEventWordSize : Int := 8;
      TSEventByteSize : Int := 16;
      EventByteDataSize : Int := 4;
      MaxStatusReqNb : Int := 7;
      StatusReqListSize : Int := 8;
      StatusWordSize : Int := 100;   // Size of status table
      StatusByteSize : Int := 200;
      MaxReqNumber : Int := 3;
      ReqListSize : Int := 4;
      MaxBufferSize_Event : Int := 2406;
      MaxBufferSize_Status : Int := 1490;
      MaxBufferSize : Int := 2406;   // VARIABLE NOT USED
      EventExtension : Int := 916;
      EventBufferFull : Word := W#16#F001;   // The number of event is exceeded
      WrongDBNumber : Word := W#16#F002;   // The DB number is 0
      WrongEventSize : Word := W#16#F003;   // In Event size <> Event size in buffer
      MainQueueFull : Word := W#16#F004;   // Number of req in the global queue exceeded
      NbTableExceeded : Word := W#16#F005;   // Number of tables exceeded
      NoAccessToStatus : Word := W#16#F007;   // Error during access to one or more status table
      EventAccessError : Word := W#16#F008;   // Error during access of the  event (in application part)
      TimeStampError : Word := W#16#F009;   // READ_CLK error; NOT USED
      SendTimeOutError : Word := W#16#F00A;   // No reaction from network after sendind TSPP frame
      Transmission_Error : Word := W#16#F00B;   // The BSEND error code is added to this value
   END_VAR

    // Events treatment
    IF #NewEvent THEN
      IF #MultipleEvent THEN
        //#EventListAdr := 0;
        //#NumberOfEvents := WORD_TO_INT(#EventListDB.DW(#EventListAdr));   // corrected parentheses
        #NumberOfEvents := #ID_NewEvent - #ID_EventSent;
        IF #NumberOfEvents < 0 THEN
          #NumberOfEvents := #NumberOfEvents + #ListEventSize;
        END_IF;
        #EventListAdr := 2 + #ID_EventSent * 10;
        #EventListElement.IDAndType := PEEK_WORD(area := 16#84, dbNumber := #EventListDB, byteOffset := #EventListAdr);
        #EventListElement.DataAndDBNb := PEEK_DWORD(area := 16#84, dbNumber := #EventListDB, byteOffset := #EventListAdr + 2);
        #EventListElement.Address := PEEK_DWORD(area := 16#84, dbNumber := #EventListDB, byteOffset := #EventListAdr + 6);
        #EventPtr := #EventListElemPtr;
      ELSE
        #NumberOfEvents := 0;
        #EventPtr := #EventData;
      END_IF;
      #CurrentInEvent := 1;
      REPEAT
        CASE CHAR_TO_INT(BYTE_TO_CHAR(#Event.DataType)) OF
          2:
            #InEventByteDataSize := #Event.NbOfData;
          4..5:
            #InEventByteDataSize := #Event.NbOfData * 2;
          6..8:
            #InEventByteDataSize := #Event.NbOfData * 4;
          ELSE:
            ;
        END_CASE;
        IF #EventTSIncluded THEN
          #InEventByteDataSize := #InEventByteDataSize - 8;   //data size without time stamp   
        END_IF;
        IF #InEventByteDataSize <> #EventByteDataSize THEN
          #Error := TRUE;
          #Error_code := #WrongEventSize;
          #Special_code := 0;
        ELSIF #Event.DBNumber = 0 THEN
          #Error := TRUE;
          #Error_code := #WrongDBNumber;
          #Special_code := 0;
        ELSIF #CurrentEvent >= #ExtendedNbOfEvent THEN
          #Error := TRUE;
          #Error_code := #EventBufferFull;
          #Special_code := 0;
          #ExtendedBufFull := TRUE;
        ELSE   // Record new event
          #CurrentEvent := #CurrentEvent + 1;
          #TS_EventBuffer.Data[#CurrentEvent].DBNumber := #Event.DBNumber;
          #TempAdr := SHR(IN := #Event.Address, N := 3);
          IF #EventTSIncluded THEN
            #TS_EventBuffer.Data[#CurrentEvent].Address := DWORD_TO_INT(#TempAdr) + 8;// Address of data  
            #Event.DataType := 2;
            #Event.NbOfData := 8;
            #TS_EventBuffer.Data[#CurrentEvent].TimeStamp := LDT_TO_DT(LWORD_TO_LDT(PEEK_LWORD(area := 16#84, dbNumber := #IN_Event.DBNumber, byteOffset := DWORD_TO_DINT(#IN_Event.Address))));
            #Result := 0; // !!
            IF #Result <> 0 THEN
              #Error := TRUE;
              #Error_code := #EventAccessError;
              #Special_code := INT_TO_WORD(#Result);
            END_IF;
            #Event.NbOfData := #InEventByteDataSize;   // Size of data only
            #Event.Address := DINT_TO_DWORD(DWORD_TO_DINT(#Event.Address) + 64);   //8 bytes more
          ELSE
            #TS_EventBuffer.Data[#CurrentEvent].Address := DWORD_TO_INT(#TempAdr);
            #Er_Code := RD_SYS_T(#TS_EventBuffer.Data[#CurrentEvent].TimeStamp);   // removed CDT:=  // changed from READ_CLK
          END_IF;
          FOR #Index4 := 1 TO #Word_in_one_TSEvent DO
            #TS_EventBuffer.Data[#CurrentEvent].Data[#Index4] := PEEK_WORD(area := 16#84,
                                                                           dbNumber := #Event.DBNumber,
                                                                           byteOffset := DWORD_TO_DINT(#TempAdr) + 2 * (#Index4 - 1));
          END_FOR;
          #Result := 0; // !!
          IF #Result = 0 THEN
            #TS_EventBuffer.TS_Data_Length := #TS_EventBuffer.TS_Data_Length + #TSEventWordSize;
            #EventBufferSize := #EventBufferSize + #TSEventByteSize;
          ELSE
            #CurrentEvent := #CurrentEvent - 1;
            #Error := TRUE;
            #Error_code := #EventAccessError;
            #Special_code := INT_TO_WORD(#Result);
          END_IF;
          IF #CurrentEvent > #MaxNbOfTSEvent THEN
            #BufferFullSendReq := TRUE;
          END_IF;
        END_IF;
        IF #MultipleEvent THEN
          #ID_EventSent := #ID_EventSent + 1;   // changed tnr
          
          IF #ID_EventSent > #ListEventSize - 1 THEN
            #ID_EventSent := 0;
            #EventListAdr := 2 + #ID_EventSent * 10;
          ELSE
            #EventListAdr := #EventListAdr + 10;
          END_IF;
        #EventListElement.IDAndType := PEEK_WORD(area := 16#84, dbNumber := #EventListDB, byteOffset := #EventListAdr);
        #EventListElement.DataAndDBNb := PEEK_DWORD(area := 16#84, dbNumber := #EventListDB, byteOffset := #EventListAdr + 2);
        #EventListElement.Address := PEEK_DWORD(area := 16#84, dbNumber := #EventListDB, byteOffset := #EventListAdr + 6);
          #EventPtr := #EventListElemPtr;
          #CurrentInEvent := #CurrentInEvent + 1;
        END_IF;
      UNTIL #CurrentInEvent > #NumberOfEvents OR #Error
      END_REPEAT;
      // DB_EventData.ID_NewEvent := DB_EventData.ID_NewEvent + #NumberOfEvents;//changed tnr
    END_IF;                                     
    // END manage event
END_FUNCTION_BLOCK


DATA_BLOCK "TSPP_FI_DB" "TSPP_FI"
BEGIN
END_DATA_BLOCK"""
                                          
        output = self.process_template("TIAInst_CPC_TSPP_UNICOS_Template.scl", params)
        self.thePlugin.writeInstanceInfo("CPC_TSPP_UNICOS.scl", output)
