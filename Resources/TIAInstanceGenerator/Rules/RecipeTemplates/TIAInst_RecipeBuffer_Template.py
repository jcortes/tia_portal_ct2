# -*- coding : utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class RecipeBuffer_Template(TIAInst_Generic_Template) :

    def process(self, *params) :
        self.thePlugin.writeInUABLog("processInstances in Jython for Recipes.")
        xml_config = params[1]

        recipe_info = '''// DB_RECIPES_INTERFACE
DATA_BLOCK "DB_RECIPES_INTERFACE"
TITLE = 'DB_RECIPES_INTERFACE'
{ S7_Optimized_Access := 'FALSE' }
AUTHOR : 'ICE/SIC'
FAMILY : UNICOS
NAME : Recipes
   STRUCT 
      ActivationTimeout : Int;
      RecipeHeader : Struct   // Recipe Header
         RecipeIDLow : Int;
         RecipeIDHigh : Int;
         NbManReg : Int;
         NbofDPAR : Int;
         DBofDPAR : Int;
         NbofWPAR : Int;
         DBofWPAR : Int;
         NbofAPAR : Int;
         DBofAPAR : Int;
         NbofAA : Int;
         DBofAA : Int;
         NbofPID : Int;
         DBofPID : Int;
         SentValue : Int;
         CRC : Int;
         Void01 : Int;
         Void02 : Int;
         Void03 : Int;
         Void04 : Int;
         Void05 : Int;
      END_STRUCT;
      RecipeStatus : Struct   // Recipe Status
         RecipeIDLow : Int;
         RecipeIDHigh : Int;
         NbManReg : Int;
         NbofDPAR : Int;
         DBofDPAR : Int;
         NbofWPAR : Int;
         DBofWPAR : Int;
         NbofAPAR : Int;
         DBofAPAR : Int;
         NbofAA : Int;
         DBofAA : Int;
         NbofPID : Int;
         DBofPID : Int;
         RecipeStatus : Int;
         CRCStatus : Int;
         ErrorDetail : Int;
         Void01 : Int;
         Void02 : Int;
         Void03 : Int;
         Void04 : Int;
      END_STRUCT;
      ManRegAddr : Array[1..%(buffer_size)s] of Int;   // ManReg01 Addresses
      ManRegVal : Array[1..%(buffer_size)s] of Int;   // ManReg01 Values
      RequestAddr : Array[1..%(buffer_size)s] of Int;   // Request Addresses
      RequestVal : Array[1..%(buffer_size)s] of Real;   // Request Values
   END_STRUCT;


BEGIN
   ActivationTimeout := %(ActivationTimeout)s;

END_DATA_BLOCK



// DB_RECIPES_INTERFACE_old
DATA_BLOCK DB_RECIPES_INTERFACE_old
TITLE = 'DB_RECIPES_INTERFACE_old'
{ S7_Optimized_Access := 'FALSE' }
AUTHOR : 'ICE/SIC'
FAMILY : UNICOS
NAME : Recipes
   STRUCT 
      ActivationTimeout : Int;
      RecipeHeader : Struct   // Recipe Header
         RecipeIDLow : Int;
         RecipeIDHigh : Int;
         NbManReg : Int;
         NbofDPAR : Int;
         DBofDPAR : Int;
         NbofWPAR : Int;
         DBofWPAR : Int;
         NbofAPAR : Int;
         DBofAPAR : Int;
         NbofAA : Int;
         DBofAA : Int;
         NbofPID : Int;
         DBofPID : Int;
         SentValue : Int;
         CRC : Int;
         Void01 : Int;
         Void02 : Int;
         Void03 : Int;
         Void04 : Int;
         Void05 : Int;
      END_STRUCT;
      RecipeStatus : Struct   // Recipe Status
         RecipeIDLow : Int;
         RecipeIDHigh : Int;
         NbManReg : Int;
         NbofDPAR : Int;
         DBofDPAR : Int;
         NbofWPAR : Int;
         DBofWPAR : Int;
         NbofAPAR : Int;
         DBofAPAR : Int;
         NbofAA : Int;
         DBofAA : Int;
         NbofPID : Int;
         DBofPID : Int;
         RecipeStatus : Int;
         CRCStatus : Int;
         ErrorDetail : Int;
         Void01 : Int;
         Void02 : Int;
         Void03 : Int;
         Void04 : Int;
      END_STRUCT;
   END_STRUCT;


BEGIN
   ActivationTimeout := %(ActivationTimeout)s;

END_DATA_BLOCK
'''
        # General Steps for the Recipes file :
        # 1. Create the DB_RECIPES with the required buffers

        xml_config = self.thePlugin.getXMLConfig() # TODO : remove this 
        
        params = {
            'ActivationTimeout' : xml_config.getPLCParameter("RecipeParameters:ActivationTimeout"),
            'buffer_size' : self.get_recipe_buffer_size(xml_config)
        }
        self.thePlugin.writeInstanceInfo("Recipes.SCL", recipe_info % params)
