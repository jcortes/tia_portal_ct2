# -*- coding: utf-8 -*-
from java.util import Vector
from java.util import ArrayList
from research.ch.cern.unicos.utilities import SemanticVerifier
import TIALogic_DefaultAlarms_Template
reload(TIALogic_DefaultAlarms_Template)

import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)

def GLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    theSemanticVerifier = SemanticVerifier.getUtilityInterface()
    Lparam1,Lparam2,Lparam3,Lparam4,Lparam5,Lparam6,Lparam7,Lparam8,Lparam9,Lparam10=TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)
    Name = name
    
    theUnicosProject = thePlugin.getUnicosProject()
    
    Decorator = ucpc_library.shared_decorator.ExpressionDecorator()
    thePlugin.writeTIALogic('''
DATA_BLOCK DB_GLOBAL

//
// Block contaning info for power cut
//
    STRUCT
        RealPowerCut         :BOOL;
        RealPowerCut_old    :BOOL;
        RealPowerCut_temp     :BOOL;
        Pulse_Ack        :BOOL;
        T_powerCutOn    :DINT;
        T_powerCutOff    :DINT;
        T_PulseAck    :DINT;
        LostPowerMem : BOOL;
        PrTension: BOOL;
        old_PrTension: BOOL;
        
        FDEU_893_P5101ACrst:BOOL;
        F_FDEU_893_P5101SI:BOOL;
        F_FDEU_893_P5101SIo:BOOL;
        FDEU_893_P5201ACrst:BOOL;
        F_FDEU_893_P5201SI:BOOL;
        F_FDEU_893_P5201SIo:BOOL;
        FDEU_893_P1501ACrst:BOOL;
        F_FDEU_893_P1501SI:BOOL;
        F_FDEU_893_P1501SIo:BOOL;
        FDEU_893_P1101ACrst:BOOL;	
        F_FDEU_893_P1101SI:	BOOL;	
        F_FDEU_893_P1101SIo:BOOL;	      
        FDEU_893_P2401ACrst:BOOL;	
        F_FDEU_893_P2401SI:	BOOL;	
        F_FDEU_893_P2401SIo:BOOL;	
        FDEU_893_P2501ACrst:BOOL;	
        F_FDEU_893_P2501SI:	BOOL;	
        F_FDEU_893_P2501SIo:BOOL;	
        FDEU_893_P2701ACrst:BOOL;	
        F_FDEU_893_P2701SI:	BOOL;	
        F_FDEU_893_P2701SIo:BOOL;	     
        
        GAC_IX_Minute_Pass:BOOL;
        GAC_IX_P1001_Req:BOOL;
        GAC_IX_P5101_Req : BOOL;
        
        MMF_UF_Minute_Pass:BOOL;
        MMF_UF_P5101_Req:BOOL;
        
        FDEU_893_FQ1101_old:BOOL;
        FDEU_893_FQ5101_old:BOOL;
        FDEU_893_FQ2601_old:BOOL;

        CCD_RO_ROCCD_PFDCon: BOOL;          //TO BE CHECKED!!!!!!
        CCD_RO_ROCCD_SDFCon: BOOL;          //TO BE CHECKED!!!!!!
        FDEU_893_RO_QUICK_old: BOOL;
        CCD_RO_STANDBY_X_old:BOOL;
        CCD_RO_LastStep: WORD;
        FDEU_893_FQ2401_old:BOOL;
        FDEU_893_FQ2701_old:BOOL;
        
        FDEU_893_DP7001_old_RE:BOOL;
        FDEU_893_DP7001_Et:DINT;
        FDEU_893_DP7001_Next:DINT;
        FDEU_893_DP7001_Period:DINT;
        FDEU_893_DP7001_old_FE:BOOL;
        
        FDEU_893_DP7101_old_RE:BOOL;
        FDEU_893_DP7101_Et:DINT;
        FDEU_893_DP7101_Next:DINT;
        FDEU_893_DP7101_Period:DINT;
        FDEU_893_DP7101_old_FE:BOOL;
        
        FDEU_893_DP7201_old_RE:BOOL;
        FDEU_893_DP7201_Et:DINT;
        FDEU_893_DP7201_Next:DINT;
        FDEU_893_DP7201_Period:DINT;
        FDEU_893_DP7201_old_FE:BOOL;
        
        FDEU_893_DP7301_old_RE:BOOL;
        FDEU_893_DP7301_Et:DINT;
        FDEU_893_DP7301_Next:DINT;
        FDEU_893_DP7301_Period:DINT;
        FDEU_893_DP7301_old_FE:BOOL;
        
        FDEU_893_DP7401_old_RE:BOOL;
        FDEU_893_DP7401_Et:DINT;
        FDEU_893_DP7401_Next:DINT;
        FDEU_893_DP7401_Period:DINT;
        FDEU_893_DP7401_old_FE:BOOL;
        
        
        S3_03_2_old:BOOL;
        S3_05_4_old:BOOL;
    END_STRUCT
BEGIN
END_DATA_BLOCK
''')

# Step 1. Create the FUNCTION called DeviceName_GL.
    thePlugin.writeTIALogic('''
FUNCTION $name$_GL : VOID
TITLE = '$name$_GL'
//
// Global Logic of $name$
(*
 Lparam1:    $Lparam1$      // FB name of DB_GLOBAL
 Lparam2:    $Lparam2$      // DB name of DB_GLOBAL
 Lparam3:    $Lparam3$      // Global DB Name
 Lparam4:    $Lparam4$     //Liste des DI pour detecter coupure de courant separes par des virgules (DI = true = presence tension)
 Lparam5:    $Lparam5$     //liste des actionneurs actifs (Motors) separes par des virgules
 Lparam6:    $Lparam6$    //APAR: Temps de memorisation de la perte de courant sur les DI (sec)
 Lparam7:    $Lparam7$    //APAR: Temps apres lequel on aquitte tout et on redemarre (sec)
 Lparam8:    $Lparam8$
 Lparam9:    $Lparam9$
 Lparam10:   $Lparam10$
*)
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_GL'
FAMILY: 'GL'
''')

    #children to ack/allow restart after power cut
    theChildren= theRawInstances.findMatchingInstances("OnOff,Analog,AnaDO,AnalogDigital,ProcessControlObject", "'#LogicDeviceDefinitions:Master#'!=''")

    # Third static variables
    thePlugin.writeTIALogic('''
VAR_TEMP
    old_status : DWORD;
    C_M             :REAL;      // Current Month
    C_D             :REAL;      // Current Day
    C_H             :REAL;      // Current Hour
    C_Min           :REAL;      // Current Minute
    C_S             :REAL;      // Current Second
END_VAR
BEGIN
''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')
#(* SELECTING SETPOINT DEPENDING ON REQUEST. CHAPTER 3.3.2)
    thePlugin.writeTIALogic(Decorator.decorateExpression('''
(* SELECTING SETPOINT FOR FC5101 DEPENDING ON REQUEST*)
IF DB_GLOBAL.GAC_IX_P5101_Req THEN
    FDEU_893_FC5101Sp.HFPos := FDEU_893_GXFC5101Sp;
    FDEU_893_FC5101SpSel.AuPosR := 0;
END_IF;
IF DB_GLOBAL.MMF_UF_P5101_Req THEN
    FDEU_893_FC5101Sp.HFPos := FDEU_893_MUFC5101Sp;
    FDEU_893_FC5101SpSel.AuPosR := 1;
END_IF;
'''))

#(* COMPUTED VARIABLES. CHAPTER 3.6 *)
    thePlugin.writeTIALogic(Decorator.decorateExpression('''
(* COMPUTED VARIABLES*)
GET_CURRENT_TIME(Month=>C_M, DayOfMonth=>C_D, Hour=>C_H, Minute=>C_M, Second=>C_S);
IF (C_M = 1.0 AND C_D = 1.0 AND C_H = 0.0 AND C_M = 1.0) THEN
    FDEU_893_P1101TreatVol.AuPosR := 0.0;
    FDEU_893_P5101TreatVol.AuPosR := 0.0;
    FDEU_893_CT2TreatedVol.AuPosR := 0.0;
    FDEU_893_CT2ExtractVol.AuPosR := 0.0;
END_IF;
 

IF R_EDGE(new := FDEU_893_FQ1101,
    old := DB_GLOBAL.FDEU_893_FQ1101_old) THEN
    FDEU_893_P1101TreatVol.AuPosR := FDEU_893_P1101TreatVol + FDEU_893_FQ1101_Val;
END_IF;


IF R_EDGE(new := FDEU_893_FQ5101,
    old := DB_GLOBAL.FDEU_893_FQ5101_old) THEN
    FDEU_893_P5101TreatVol.AuPosR := FDEU_893_P5101TreatVol + FDEU_893_FQ5101_Val;
END_IF; 

IF R_EDGE(new := FDEU_893_FQ2601,
    old := DB_GLOBAL.FDEU_893_FQ2601_old) THEN
    FDEU_893_CT2TreatedVol.AuPosR := FDEU_893_CT2TreatedVol + FDEU_893_FQ2601_Val;
END_IF; 

FDEU_893_CT2ExtractVol.AuPosR := FDEU_893_P1101TreatVol + FDEU_893_P5101TreatVol;


//FOA1101 cartridge filter pressure 
FDEU_893_DP1101.HFPos := FDEU_893_PT1101 - FDEU_893_PT1102;
FDEU_893_DP1101.IOError := FDEU_893_PT1101.IOErrorW OR FDEU_893_PT1102.IOErrorW;
//MMF pressure drop
FDEU_893_DP1201.HFPos := FDEU_893_PT1102 - FDEU_893_PT1201; 
FDEU_893_DP1201.IOError := FDEU_893_PT1102.IOErrorW OR FDEU_893_PT1201.IOErrorW;
//UF PRESSURE DROP
FDEU_893_DP1301.HFPos := FDEU_893_PT1201 - FDEU_893_PT1301; 
FDEU_893_DP1301.IOError := FDEU_893_PT1201.IOErrorW OR FDEU_893_PT1201.IOErrorW;
//GAC PRESSURE DROP
FDEU_893_DP1901.HFPos := FDEU_893_PT1501 -  FDEU_893_PT1901; 
FDEU_893_DP1901.IOError := FDEU_893_PT1501.IOErrorW OR FDEU_893_PT1901.IOErrorW;
//	FOA2401 cartridge filter pressure 
FDEU_893_DP2401.HFPos := FDEU_893_PT2401 - FDEU_893_PT2402; 
FDEU_893_DP2401.IOError := FDEU_893_PT2401.IOErrorW OR FDEU_893_PT2402.IOErrorW;
//CCD-RO membrane pressure drop (CONCENTRATE)
FDEU_893_DP2701.HFPos := FDEU_893_PT2501 - FDEU_893_PT2701;
FDEU_893_DP2701.IOError := FDEU_893_PT2501.IOErrorW OR FDEU_893_PT2701.IOErrorW;
//CCD-RO membrane pressure drop (PERMEATE)
FDEU_893_DP2601.HFPos := FDEU_893_PT2601 - FDEU_893_PT2501;
FDEU_893_DP2601.IOError := FDEU_893_PT2601.IOErrorW OR FDEU_893_PT2501.IOErrorW;
//FOA5101 cartridge filter pressure
FDEU_893_DP5101.HFPos := FDEU_893_PT5101 - FDEU_893_PT5102; 
FDEU_893_DP5101.IOError := FDEU_893_PT5102.IOErrorW OR FDEU_893_PT5101.IOErrorW;


IF FDEU_893_FSL5201 THEN
    FDEU_893_PHT5201Filtered.HFPos := FDEU_893_PHT5201;
    FDEU_893_TT5201Filtered.HFPos := FDEU_893_TT5201;
    FDEU_893_C2T5201Filtered.HFPos := FDEU_893_CL2T5201;
    FDEU_893_C2T5201Filtered.IOError := FDEU_893_PHT5201.IOErrorW;
    FDEU_893_TT5201Filtered.IOError := FDEU_893_TT5201.IOErrorW;
    FDEU_893_PHT5201Filtered.IOError := FDEU_893_CL2T5201.IOErrorW;
ELSE
    FDEU_893_PHT5201Filtered.HFPos := 0.0;
    FDEU_893_TT5201Filtered.HFPos := 0.0;
    FDEU_893_C2T5201Filtered.HFPos := 0.0;
    FDEU_893_C2T5201Filtered.IOError := FALSE;
    FDEU_893_TT5201Filtered.IOError := FALSE;
    FDEU_893_PHT5201Filtered.IOError := FALSE;
END_IF;

IF FDEU_893_FSL1901 THEN
    FDEU_893_ORPT1901Filterd.HFPos := FDEU_893_PHT5201;
    FDEU_893_ORPT1901Filterd.IOError := FDEU_893_PHT5201.IOErrorW; 
ELSE
    FDEU_893_ORPT1901Filterd.HFPos := 0.0;
    FDEU_893_ORPT1901Filterd.IOError := FALSE;
END_IF;

'''))



#POWER CUT MANAGEMENT GL PART (1st part in IL)
    thePlugin.writeTIALogic('''
//On falling edge of power cut, pulse 1sec the ack and allow restart
    IF DB_GLOBAL.RealPowerCut_old AND NOT DB_GLOBAL.RealPowerCut THEN
        DB_GLOBAL.Pulse_Ack := TRUE;
    END_IF;
    
    IF NOT DB_GLOBAL.Pulse_Ack OR DB_GLOBAL.T_PulseAck > CPC_GLOBAL_VARS.UNICOS_LiveCounter THEN
        DB_GLOBAL.T_PulseAck := CPC_GLOBAL_VARS.UNICOS_LiveCounter;
    END_IF;
    
    IF CPC_GLOBAL_VARS.UNICOS_LiveCounter - DB_GLOBAL.T_PulseAck > 1.0 THEN
        DB_GLOBAL.Pulse_Ack := FALSE;
    END_IF;
    
IF DB_GLOBAL.RealPowerCut_temp AND NOT DB_GLOBAL.Pulse_Ack THEN
    $Name$.AuAlAck := FALSE;
    $Name$.AuRStart := FALSE;''')
    for inst in theChildren:
        ChildName = inst.getAttributeData ("DeviceIdentification:Name")
        thePlugin.writeTIALogic('''
    $ChildName$.AuAlAck := FALSE;
    $ChildName$.AuRStart := FALSE;''')
    
    thePlugin.writeTIALogic('''
    DB_GLOBAL.RealPowerCut_temp:=FALSE;
END_IF;

(*ack + allow restart all PCO + field objects*)
IF DB_GLOBAL.Pulse_Ack THEN
    $Name$.AuAlAck := TRUE;
    $Name$.AuRStart := TRUE;''')
    for inst in theChildren:
        ChildName = inst.getAttributeData ("DeviceIdentification:Name")
        thePlugin.writeTIALogic('''
    $ChildName$.AuAlAck := TRUE;
    $ChildName$.AuRStart := TRUE;''')

    thePlugin.writeTIALogic('''
    DB_GLOBAL.RealPowerCut_temp:=TRUE;
END_IF;

  DB_GLOBAL.RealPowerCut_old:= DB_GLOBAL.RealPowerCut;
''')


    #Recover Copy and CCC alarms
    #In DA:
    # Input = Name of original Alarm for alarm copies
    # Parameter1 = 'copy' or 'CCC'
    # Parameter2 = empty for CCC alarms / Name of the CCC alarms where the alarm copies should be linked.
    # Parameter3 = optional for CCC alarms: Name of the DIC to force the CCC alarm
    # Parameter4 = optional for copies: when input is a field/PCO, links all interlock signals: "FS" or "TS" or "AL" or "FSTS" or "ALL"

    theDigitalAlarmsCpy = theRawInstances.findMatchingInstances("DigitalAlarm", "'#LogicDeviceDefinitions:CustomLogicParameters:Parameter1#'='copy'")
    theDigitalAlarmsCCC = theRawInstances.findMatchingInstances("DigitalAlarm", "'#LogicDeviceDefinitions:CustomLogicParameters:Parameter1#'='CCC'")

    thePlugin.writeTIALogic('''
(*Copy of alarms linked to CCC alarms*)
''')

    for inst in theDigitalAlarmsCpy:
        DAName = inst.getAttributeData ("DeviceIdentification:Name")
        DAI = inst.getAttributeData ("FEDeviceEnvironmentInputs:Input")
        Delay = inst.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
        DA_Param2 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter2") # CCC alarm to link to
        DA_Param4 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter4").lower().strip()

        # Input conditions
        if DAI == "":
            thePlugin.writeTIALogic('''$DAName$.I := 0; //to Complete
    $DAName$.AuAlAck := 0;//to Complete
    $DAName$.IOError := 0;
    $DAName$.IOSimu := 0;
''')
        else:
            #Check if the input is an alarm or a field object
            TheInput = theRawInstances.findMatchingInstances("DigitalAlarm,AnalogAlarm", "'#DeviceIdentification:Name#'='$DAI$'")
            thePlugin.writeDebugInUABLog(DAI)
            for instance in TheInput:
                thePlugin.writeDebugInUABLog(str(instance.getDeviceTypeName()))
            DA_ack = ""
            if TheInput.size() > 0:
                if DA_Param4 in ["","i"]:
                    CopyInput = "$DAI$.ISt"
                else:
                    device_type = []
                    for instance in TheInput:
                        device_type.append(instance.getDeviceTypeName())
                        thePlugin.writeDebugInUABLog(instance.getDeviceTypeName())
                    if "DigitalAlarm" in device_type:
                        thePlugin.writeErrorInUABLog("Alarm copy: "+DAName+ ". The value in Parameter4: "+DA_Param4+" is invalid for DigitalAlarm, please delete.")
                    elif "AnalogAlarm" in device_type:
                        # allow sending individual thresholds to CCC alarms, did not do fancy processing, only allow certain combinations from:
                        # i, w, hh, h, l, ll 
                        # i.e. hh,h is allows, but not just h, because in that case it would be inconsistent (imho)
                        if DA_Param4 in ["","ll,hh","hh,ll"]:
                            CopyInput = "$DAI$.ISt"
                        elif DA_Param4 in ["i,w","w,i","hh,h,l,ll","ll,l,h,hh"]:
                            CopyInput = "$DAI$.ISt OR $DAI$.WSt"
                        elif DA_Param4 in ["hh,h","h,hh"]:
                            CopyInput = "$DAI$.HHAlSt OR $DAI$.HWSt"
                        elif DA_Param4 in ["ll,l","l,ll"]:
                            CopyInput = "$DAI$.LLAlSt OR $DAI$.LWSt"
                        else:
                            thePlugin.writeErrorInUABLog("Alarm copy: "+DAName+ ". The value in Parameter4: "+DA_Param4+" is invalid for AnalogAlarm, please correct and re-run."+'''
Available options are empty (default) to send ISt, i.e. HH/LL, "I,W" (or "HH,H,L,LL") - send all thresholds, "HH,H", "LL,L". Other combinations are invalid. ''')
                        
                
                #Check if the process alarm is auto ack or not
                for inst in TheInput:
                    DA_ack = inst.getAttributeData("FEDeviceAlarm:Auto Acknowledge").strip().lower()
            elif DA_Param4 == "fs":
                CopyInput = "NOT $DAI$.EnRStartSt "
            elif DA_Param4 == "ts":
                CopyInput = "$DAI$.TStopISt"
            elif DA_Param4 == "al":
                CopyInput = "$DAI$.ALSt"
            elif DA_Param4 == "fsts":
                CopyInput = "NOT $DAI$.EnRStartSt OR $DAI$.TStopISt"
            elif DA_Param4 == "fstsal":
                CopyInput = "NOT $DAI$.EnRStartSt OR $DAI$.TStopISt OR $DAI$.ALSt"
            elif DA_Param4 == "all":
                CopyInput = "NOT $DAI$.EnRStartSt OR $DAI$.TStopISt OR $DAI$.StartISt OR $DAI$.ALSt"
            else:
                CopyInput = "NOT $DAI$.EnRStartSt OR $DAI$.TStopISt"

            if DA_ack <> "true":
                thePlugin.writeTIALogic('''    $DAName$.I := $CopyInput$ ;
    $DAName$.AuAlAck := NOT '''+DAI+'''.AlUnAck;
    $DAName$.IOError := '''+DAI+'''.IOErrorW;
    $DAName$.IOSimu := '''+DAI+'''.IOSimuW ;
''')
            else:
                thePlugin.writeTIALogic('''    $DAName$.I := $CopyInput$ ;
    $DAName$.AuAlAck := NOT '''+DAI+'''.ISt;
    $DAName$.IOError := '''+DAI+'''.IOErrorW;
    $DAName$.IOSimu := '''+DAI+'''.IOSimuW ;
''')


        # Delay Alarm conditions
        if Delay.strip() == "":
            text = 'do nothing'
        elif thePlugin.isString(Delay) and Delay.strip().lower() <> "logic":
            thePlugin.writeTIALogic('''    $db_alarm$$DAName$.PAlDt := REAL_TO_INT('''+Delay+'''.PosSt);
''')
        elif Delay.strip().lower() == "logic":
            thePlugin.writeTIALogic('''    $db_alarm$$DAName$.PAlDt := 0; // To complete
''')
        else:
            text = 'do nothing'


        # Check if CCC alarm exists, if not write Error
        if theSemanticVerifier.doesObjectExist(DA_Param2, theRawInstances) is not True:
            thePlugin.writeErrorInUABLog("Alarm copy: "+DAName+ ". The linked CCC alarm : "+DA_Param2+" in LogicParam2 doesn't exist")


    #CCC Alarms
    thePlugin.writeTIALogic('''
(*CCC alarms*)
''')

    for inst in theDigitalAlarmsCCC:
        DAName = inst.getAttributeData ("DeviceIdentification:Name")
        DADesc = inst.getAttributeData ("DeviceDocumentation:Description")
        Delay = inst.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
        DAI = inst.getAttributeData ("FEDeviceEnvironmentInputs:Input")
        Param3 = inst.getAttributeData ("LogicDeviceDefinitions:CustomLogicParameters:Parameter3") # DI to force CCC alarm
        if Param3 <> "":
            if theSemanticVerifier.doesObjectExist(Param3, theRawInstances) is not True:
                thePlugin.writeErrorInUABLog("Alarm CCC: "+DAName+ ". The DI to force CCC : "+Param3+" in LogicParam3 doesn't exist")
            DIC_SET = "$Param3$.PosSt OR "
            DIC_RESET = "NOT $Param3$.PosSt AND "
        else:
            DIC_SET = ""
            DIC_RESET = ""

        # CCC alarm Input conditions
        if DAI == "":
            thePlugin.writeTIALogic('''

(*CCC alarm: $DADesc$*)
IF ''' )
            for inst in theDigitalAlarmsCpy:
                DACpyName = inst.getAttributeData ("DeviceIdentification:Name")
                DACpyParam2 = inst.getAttributeData ("LogicDeviceDefinitions:CustomLogicParameters:Parameter2")
                if DACpyParam2 == DAName:
                    thePlugin.writeTIALogic('''
    $DACpyName$.ISt OR ''')

            thePlugin.writeTIALogic('''$DIC_SET$ 0 THEN
        $DAName$.I := TRUE;    //SET CCC ALARM
ELSIF ''')
            for inst in theDigitalAlarmsCpy:
                DACpyName = inst.getAttributeData ("DeviceIdentification:Name")
                DACpyParam2 = inst.getAttributeData ("LogicDeviceDefinitions:CustomLogicParameters:Parameter2")
                if DACpyParam2 == DAName:
                    thePlugin.writeTIALogic('''
    NOT $DACpyName$.AlUnAck AND ''')
            thePlugin.writeTIALogic('''$DIC_RESET$ 1 THEN
        $DAName$.I := FALSE;    //RESET CCC ALARM
END_IF;''')


        #Ack CCC alarm when all copies acknowledged
            thePlugin.writeTIALogic('''

    $DAName$.AuAlAck := ''' )
            for inst in theDigitalAlarmsCpy:
                DACpyName = inst.getAttributeData ("DeviceIdentification:Name")
                DACpyParam2 = inst.getAttributeData ("LogicDeviceDefinitions:CustomLogicParameters:Parameter2")
                if DACpyParam2 == DAName:
                    thePlugin.writeTIALogic('''
    NOT $DACpyName$.AlUnAck AND ''')
            thePlugin.writeTIALogic('''1; ''')
        else:
            thePlugin.writeTIALogic('''
    $DAName$.I := $DAI$.ISt;''' )

        # Delay Alarm conditions
        if Delay.strip() == "":
            text = 'do nothing'
        elif thePlugin.isString(Delay) and Delay.strip().lower() <> "logic":
            thePlugin.writeTIALogic('''    $DAName$.PAlDt := REAL_TO_INT('''+Delay+'''.PosSt);
''')
        elif Delay.strip().lower() == "logic":
            thePlugin.writeTIALogic('''    $DAName$.PAlDt := 0; // To complete
''')
        else:
            text = 'do nothing'



    # Step 1.4: IOError IOSimu
    thePlugin.writeTIALogic('''

// Errors and Simulated data
DB_ERROR_SIMU.$name$_GL_E := 0; // To complete
DB_ERROR_SIMU.$name$_GL_S := 0; // To complete

// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
