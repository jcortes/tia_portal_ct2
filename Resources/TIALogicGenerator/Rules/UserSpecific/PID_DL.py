# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from java.util import Vector
from java.util import ArrayList
from research.ch.cern.unicos.utilities import AbsolutePathBuilder
import os

import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)
import TIALogic_DefaultAlarms_Template
reload(TIALogic_DefaultAlarms_Template)
import ucpc_library.shared_grafcet_parsing
reload(ucpc_library.shared_grafcet_parsing)

import ProcessFeatures
reload(ProcessFeatures)


def ControllerLogic(thePlugin, theRawInstances, master, name, LparamVector, MasterControllerName, IncreaseSpeed, DecreaseSpeed, MeasuredValue):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    Def_SP = thePlugin.formatNumberPLC(theRawInstances.DlookupString("FEDeviceVariables:Default PID Parameters:Setpoint", "Controller", "'#DeviceIdentification:Name#' = '$name$'"))

    # Init external classes -----------------------------------------------------------
    GrafcetParser = ucpc_library.shared_grafcet_parsing.SharedGrafcetParsing()
    Decorator = ucpc_library.shared_decorator.ExpressionDecorator()
    # ---------------------------------------------------------------------------------

    # Load parameters -----------------------------------------------------------------
    DB_DB_GLOBAL = Lparam1                                                                    # name of the grafcet DB
    listOfSteps = Lparam10.split(',')                           # list of steps of DB_DB_GLOBAL (empty if DB_DB_GLOBAL is "")

    RegulationON = Decorator.decorateExpression(Lparam2, DB_DB_GLOBAL, listOfSteps)           # condition for regulation
    Positioning = Lparam3                                                                   # list of condition:position pairs for positioning
    PositioningON = Decorator.decorateExpression(Lparam4, DB_DB_GLOBAL, listOfSteps)          # global condition for positioning

    Features = Lparam8                                                                      # list of optional features
    # ---------------------------------------------------------------------------------

    # List of supported features with number of parameters ----------------------------
    SupportedFeatures = {
        'dbglobal'			:	[1, "string"],                                                          # takes: db name; specifies name of the global DB
        'setpoint'			:	[1, "string"],                                                          # takes: SetPoint (APAR/AS/AIR or logic);
        'minpos'			:	[1, "AnalogParameter|AnalogStatus|AnalogInputReal"],                    # takes: MinimumValue (APAR/AS/AIR); minimal output value (only in regulation)
        'maxpos'			:	[1, "AnalogParameter|AnalogStatus|AnalogInputReal"],                    # takes: MaximumValue (APAR/AS/AIR); maximal output value
        'errordeadband'		:	[1, "AnalogParameter"],                         		                # takes: APAR with deadband; Deadband for the measured value
        'defaultposition'   :   [1, "string"],                                                          # takes: position, any format; The default position of device (0.0 if not specified)
        'freezeonioerror'	:	[0],                                          		                    # takes: nothing; Freezes regulation in case of measured value IO error
        'freezeoncondition'	:	[1, "string"],                              		                    # takes: condition for freezing the controller
        'freezeposition'	:	[1, "string"],                              		                    # takes: position to freeze the controller: default = PID.OutOV
        'reverseaction'		:	[2, "AnalogParameter|AnalogStatus|AnalogInputReal|AnalogInput", "AnalogParameter|AnalogStatus|AnalogInputReal|AnalogInput"],  # takes: two values to compare (AI, AIR, AS); condition: (arg1 > arg2) reverse action
        'spfiltering'		:	[2, "string", "AnalogInputReal"],                  	                    # takes: filter db name, AIR for filtered setpoint; filters given setpoint over 3 days
        'spl'				:	[1, "AnalogParameter|AnalogStatus|AnalogInputReal"],                    # takes: SetPointLow (APAR/AS/AIR);
        'sph'				:	[1, "AnalogParameter|AnalogStatus|AnalogInputReal"],                    # takes: SetPointHigh (APAR/AS/AIR);
        'kc'                :   [1, "AnalogParameter|AnalogStatus|AnalogInputReal"],                    # takes: Kc (APAR/AS/AIR);
        'ti'                :   [1, "AnalogParameter|AnalogStatus|AnalogInputReal"],                    # takes: Ti (APAR/AS/AIR);
        'mvfilttime'        :   [1, "AnalogParameter"],                                                 # takes: MvFiltTime (APAR), must be in sec;
        'outovcopy'         :   [0],                                                                    # takes: nothing, copy of OutOV of controller to AS;
        'errorcalc'         :   [0],                                                                    # takes: nothing, copy of difference between SP and MV of controller to AIR;
        'audespd'           :   [2,"string", "string"],                                                 # takes: (1) logic condition to set specified AuDeSpd, and (2) new AuDeSpd parameter or logic, etc 
        'auinspd'           :   [2,"string", "string"]                                                  # takes: (1) logic condition to set specified AuInSpd, and (2) new AuInSpd parameter or logic, etc
    }
    # ---------------------------------------------------------------------------------

    # Process features ----------------------------------------------------------------
    RequestedFeatures = ProcessFeatures.parseFeatures(Features)                     # parse features
    ProcessFeatures.checkFeatures(name, RequestedFeatures, SupportedFeatures, thePlugin, theRawInstances)  # check corectness of parameters numbers

    (ListOfConditions, ListOfPositions) = ProcessFeatures.getConditionsAndPositions(Positioning, DB_DB_GLOBAL, listOfSteps)    # get conditions and positions for positioning
    # ---------------------------------------------------------------------------------

    # Step 1.0. Header of the file
    thePlugin.writeTIALogic('''
FUNCTION $name$_DL : VOID
TITLE = '$name$_DL'
//
// Dependent Logic of $name$
//
//
// Master: 	$master$
// Name: 	$name$
(*
 Lparam1:	$Lparam1$   // Name of the DB of the DB_GLOBAL
 Lparam2:	$Lparam2$   // Condition for regulation ON
 Lparam3:	$Lparam3$	// List of conditions and positions for positioning
 Lparam4:	$Lparam4$	
 Lparam5:	$Lparam5$   
 Lparam6:	$Lparam6$   
 Lparam7:	$Lparam7$   
 Lparam8:	$Lparam8$   // List of optional features
 Lparam9:	$Lparam9$
 Lparam10:  $Lparam10$
*)
AUTHOR: 'UNICOS'
NAME: 'Logic_DL'
FAMILY: 'PID'
''')

    # Step 1.1. Create all the variable that we're using on this Function
    thePlugin.writeTIALogic('''	
VAR_TEMP
   old_status1  :DWORD; // TO BE DELETED
   old_status2  :DWORD; // TO BE DELETED
   par_$name$   :BOOL;
   
   IN 	        :REAL; //Scaling input
   OUT 	        :REAL; //Scaling Output, connected to AuPosR
   In_Min	    :REAL;
   In_Max 	    :REAL;
   Out_Min      :REAL;
   Out_Max      :REAL;	
   SetPoint     :REAL;
	
END_VAR
BEGIN
''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')
    VariablesToCreate = ""

    #=========================== Global DB ==========================================================================
    if "dbglobal" in RequestedFeatures:   # argument with name of global DB, has to be a single string, no spaces
        if ' ' in RequestedFeatures['dbglobal'][0]: 
            thePlugin.writeErrorInUABLog("PID : Param8 feature (dbglobal="+RequestedFeatures['dbglobal'][0]+''') is invalid. 
The dbglobal variable must be a valid siemens variable name, i.e. a string with no spaces.''')
        DB_Global = RequestedFeatures['dbglobal'][0] + "."
    else:
        DB_Global = ""

    if "setpoint" in RequestedFeatures:                               # name of the SetPoint for PID, always AnalogParamter or AnalogStatus
        thePlugin.writeErrorInUABLog("PID $name$: Setpoint feature not needed anymore, please use FEDeviceVariables:Default PID Parameters:Setpoint field ")

    # Step 1.2: Setting the "Auto Parameter Restore" (AuPRest). Set the RE_RunOSt or the FE_RunOSt of his master into the AuPRest of the current control Object
    if master.lower() <> "no_master":
        thePlugin.writeTIALogic('''
(*Parameters**********)	
par_$name$:= $master$.RE_RunOSt OR $master$.FE_RunOSt;
(*$name$.AuPRest := par_$name$;*)
''')

    SetPointInit = ""   # not used
    # solve the problem with SePoint initialisation

    if master.lower() <> "no_master" and MasterControllerName <> "":
        SetPoint = "$MasterControllerName$.OutOV"
        SetPointInit = "22.0"

    elif master.lower() <> "no_master":
        SetPoint = Decorator.decorateExpression(Def_SP, DB_DB_GLOBAL, listOfSteps)
        if "spfiltering" in RequestedFeatures:
            SPid = thePlugin.s7db_id(SetPoint, "AnalogParameter, AnalogStatus, AnalogInputReal, AnalogInput")
            if SPid != "":      # when the SetPoint is object (APAR/AS/AIR)
                if SPid.lower() == "":          #
                    SetPointInit = SPid + SetPoint + ".AuPosR"
                elif SPid.lower() == "":
                    SetPointInit = SPid + SetPoint + ".PosSt"
                elif SPid.lower() == "":
                    SetPointInit = SPid + SetPoint + ".HFPos"
                else:
                    thePlugin.writeWarningInUABLog("PID $name$: SetPointInit for CPC_GLOBAL_VARS.First_Cycle filtering will be set to 0.0 because $SetPoint$ is not APAR/AS/AIR/AI")
                    SetPointInit = "0.0"

            else:
                thePlugin.writeWarningInUABLog("PID $name$: SetPointInit for CPC_GLOBAL_VARS.First_Cycle filtering will be set to 0.0 because cannot find object for $SetPoint$")
                SetPointInit = "0.0"
    else:
        SetPoint = "0.0"
        SetPointInit = "0.0"

    # === Filtering management ======================================================================================================================
    if "spfiltering" in RequestedFeatures:
        FilterName = RequestedFeatures['spfiltering'][0]
        FilteredSP = RequestedFeatures['spfiltering'][1]
        # TM_LAG can be parametrized

        FSPid = thePlugin.s7db_id(FilteredSP, "AnalogInput, AnalogInputReal, AnalogStatus")

        thePlugin.writeTIALogic('''
//FILTERING Setpoint over 3days
IF CPC_GLOBAL_VARS.First_Cycle THEN
     $FilterName$.TRACK:= TRUE;
     $FilterName$.INV := $SetPointInit$; 
ELSE
     $FilterName$.TRACK:= FALSE;
     $FilterName$.INV := $SetPoint$; 
END_IF;

$FilterName$.TM_LAG := T#3d;
$FilterName$.CYCLE  := T#1s;

LAG1ST.$FilterName$();

// this needs correction
$FSPid$$FilteredSP$.HFPos := $FilterName$.OUTV;

SetPoint := $FSPid$$FilteredSP$.PosSt;
''')
    else:
        thePlugin.writeTIALogic('''
//NO FILTERING
SetPoint := $SetPoint$;
        ''')

    # Step 1.3: Setpoint management:
    # Step 1.3.1: Set the AuESP (The control logic enables of the auto set-Point) to 1 if the RE_RunOSt or FE_RunOSt or RE_AuDeSt of his master are active
    # Step 1.3.2: Set the AuRegR (The control logic requests the regulation working state) to 1 if the RunOSt of his master is active
    # Step 1.3.3: Set the AuPosR (Control logic Position value request) to 1 if the RunOSt of his master is active
    # Step 1.3.4: Setpoint speed change. Set the InSpd (The control logic set Increase Speed of Setpoint (SP unit/s)) and the DeSpd (The control logic set Decrease Speed of Setpoint (SP unit/s))

    # cascade with master

    # Make cascade if master controller exists
    if (MasterControllerName <> ""):
        SetPoint = "$name$.AuSPR := $MasterControllerName$.OutOV;"
        AuESP = "$name$.AuESP := TRUE;"
    elif not thePlugin.isString(Def_SP):
        SetPoint = "$name$.AuSPR := $name$.DefSP;  (*To Complete*)"
        if "$master$" <> "NO_Master":
            AuESP = "$name$.AuESP := FALSE; (*To Complete*)"
        else:
            AuESP = "$name$.AuESP := FALSE;  (*To Complete*)"
    else:
        AuESP = ""
        SetPoint = ""

    if master.lower() <> "no_master" and MasterControllerName <> "":
        thePlugin.writeTIALogic('''
// Setpoint management 
// AuESP and AuSPR
$AuESP$
$SetPoint$
''')

    # No cascade with master
    elif master.lower() <> "no_master":
        thePlugin.writeTIALogic('''
// Setpoint management 
// AuESP and AuSPR
$AuESP$
$SetPoint$
''')
    # No master
    else:
        thePlugin.writeTIALogic('''
// Setpoint management 
$AuESP$
$SetPoint$
''')

    # === Maximum Position ==========================================================================================================================
    # maximum position that can be ordered, always AnalogParameter
    if "maxpos" in RequestedFeatures:
        thePlugin.writeErrorInUABLog("PID $name$: MaxPos feature not needed anymore, please use FEDeviceVariables:Default PID Parameters:Out High Limit field ")

    # === Minimum Position ==========================================================================================================================
    # minimum position that can be ordered, always AnalogParameter
    # taken into account only in Regulation
    if "minpos" in RequestedFeatures:
        thePlugin.writeErrorInUABLog("PID $name$: MinPos feature not needed anymore, please use FEDeviceVariables:Default PID Parameters:Out Low Limit field ")

    # === SetPointLow ==========================================================================================================================
    if "spl" in RequestedFeatures:
        thePlugin.writeErrorInUABLog("PID $name$: SPL feature not needed anymore, please use FEDeviceVariables:Default PID Parameters:SP Low Limit field ")

    # === SetPointHigh ==========================================================================================================================
    if "sph" in RequestedFeatures:
        thePlugin.writeErrorInUABLog("PID $name$: SPH feature not needed anymore, please use FEDeviceVariables:Default PID Parameters:SP High Limit field ")

    # === Kc ==========================================================================================================================
    if "kc" in RequestedFeatures:
        thePlugin.writeErrorInUABLog("PID $name$: Kc feature not needed anymore, please use FEDeviceVariables:Default PID Parameters:Kc field ")

    # === Ti ==========================================================================================================================
    if "ti" in RequestedFeatures:
        thePlugin.writeErrorInUABLog("PID $name$: Ti feature not needed anymore, please use FEDeviceVariables:Default PID Parameters:Ti field ")
        
    # === outovcopy ==========================================================================================================================
    if "outovcopy" in RequestedFeatures:
        thePlugin.writeTIALogic('''
// Copy of controller output
$name$Out.AuPosR := $name$.OutOV;
''')            

    # === MVFiltTime ==========================================================================================================================
    if "mvfilttime" in RequestedFeatures:
        MVFiltTime = Decorator.decorateExpression(RequestedFeatures['mvfilttime'][0], DB_DB_GLOBAL, listOfSteps)
        thePlugin.writeTIALogic('''
$name$.PControl.MVFiltTime:= DINT_TO_TIME( REAL_TO_DINT ($MVFiltTime$) );
''')

    # === Reverse Action of the controller ==========================================================================================================
    # for ReverseAction feature two values must be provided: Input1 and Input2 in format: Input1,Input2
    # condition for ReverseAction is then: (Input1 > Input2)
    if "reverseaction" in RequestedFeatures:

        ReverseInput1 = Decorator.decorateExpression(RequestedFeatures['reverseaction'][0], DB_DB_GLOBAL, listOfSteps)
        ReverseInput2 = Decorator.decorateExpression(RequestedFeatures['reverseaction'][1], DB_DB_GLOBAL, listOfSteps)
        ReverseActionCondition = ReverseInput1 + " > " + ReverseInput2

        thePlugin.writeTIALogic('''
// Reverse Action
IF ($ReverseActionCondition$) THEN
    $name$.PControl.RA:= TRUE;
ELSE
    $name$.PControl.RA:= FALSE;
END_IF;
''')

    if MeasuredValue <> "":
        MV = MeasuredValue
        thePlugin.writeTIALogic('''

$name$_ITON.TON(IN := NOT $MeasuredValue$.IOErrorW, PT := T#5s);

// AuRegR and AuOutPR
IF ($RegulationON$) ''')

    # add logic to calculate IOError over the time
    if "freezeonioerror" in RequestedFeatures:  # if IOerror switch to Positioning;
        thePlugin.writeTIALogic('''
    AND $name$_ITON.Q ''')
    if "freezeoncondition" in RequestedFeatures:  # if IOerror switch to Positioning;
        thePlugin.writeTIALogic('''
    AND NOT (''' + Decorator.decorateExpression(RequestedFeatures['freezeoncondition'][0], DB_DB_GLOBAL, listOfSteps) + ''')''')
    thePlugin.writeTIALogic(''' THEN 
	$name$.AuRegR := 1; // SET
ELSE
  $name$.AuRegR := 0;  // RESET
END_If;
$name$.AuOutPR := NOT $name$.AuRegR;

// Positioning
''')

    #=========================== Default Position ===================================================================
    if 'defaultposition' in RequestedFeatures:
        DefaultPosition = Decorator.decorateExpression(RequestedFeatures['defaultposition'][0], DB_DB_GLOBAL, listOfSteps)
    else:
        DefaultPosition = "0.0"

    #=========================== Default Position ===================================================================
    if 'freezeposition' in RequestedFeatures:
        FreezePosition = Decorator.decorateExpression(RequestedFeatures['freezeposition'][0], DB_DB_GLOBAL, listOfSteps)
    else:
        FreezePosition = str(name) + ".OutOV"

    # === Logic keeping the output value in case of input IOerror (only when regulation SHOULD be on) ===============================================
    # no additional changes needed

    thePlugin.writeTIALogic('''
// dummy condition, not used
IF FALSE THEN
    $name$.AuPosR := 0.0;
''')
    for (condition, position) in zip(ListOfConditions, ListOfPositions):       # go through all the pairs steps:position
        thePlugin.writeTIALogic('''// positioning
ELSIF ($condition$ $PositioningON$) THEN
    $name$.AuPosR := $position$;
''')

    # freezing the regulation on IOerror or condition
    if "freezeonioerror" in RequestedFeatures and "freezeoncondition" in RequestedFeatures:
        VariablesToCreate = VariablesToCreate + '''
        $name$_ITON:\TON;\t// add timer in static resources for $name$ controller freezeonioerror management'''
        FreezeRegulation = '''($RegulationON$)
        AND (NOT $name$_ITON.Q
        OR (''' + Decorator.decorateExpression(RequestedFeatures['freezeoncondition'][0], DB_DB_GLOBAL, listOfSteps) + ''') )'''
    elif "freezeonioerror" in RequestedFeatures:
        FreezeRegulation = '''($RegulationON$)
        AND NOT $name$_ITON.Q'''
    elif "freezeoncondition" in RequestedFeatures:
        FreezeRegulation = '''($RegulationON$)
        AND (''' + Decorator.decorateExpression(RequestedFeatures['freezeoncondition'][0], DB_DB_GLOBAL, listOfSteps) + ''')'''
    else:
        FreezeRegulation = '''FALSE'''

    thePlugin.writeTIALogic('''
// AuPosR In case of IOerror (FreezeRegulation) in Regulation
ELSIF $FreezeRegulation$ THEN  // IF should be regulating, but there is IOError
	$name$.AuPosR := $FreezePosition$ ;
''')
    thePlugin.writeTIALogic('''ELSE
    $name$.AuPosR := $DefaultPosition$;
END_IF;
''')
#=========================== AuDeSpd & AuInSpd ===================================================================
    if 'audespd' in RequestedFeatures:
        ConditionDeSpd = Decorator.decorateExpression(RequestedFeatures['audespd'][0], DB_DB_GLOBAL, listOfSteps)
        DeSpd = Decorator.decorateExpression(RequestedFeatures['audespd'][1], DB_DB_GLOBAL, listOfSteps)
        thePlugin.writeTIALogic('''
// Setpoint speed decrease rate (Unit/s)
IF ($ConditionDeSpd$) THEN
	$name$.AuSPSpd.DeSpd := $DeSpd$;
ELSE
	$name$.AuSPSpd.DeSpd := $DecreaseSpeed$;
END_IF;''')
    else:
        thePlugin.writeTIALogic('''
// Setpoint speed decrease rate (Unit/s)
$name$.AuSPSpd.DeSpd := $DecreaseSpeed$;''')

    if 'auinspd' in RequestedFeatures:
        ConditionInSpd = Decorator.decorateExpression(RequestedFeatures['auinspd'][0], DB_DB_GLOBAL, listOfSteps)
        InSpd = Decorator.decorateExpression(RequestedFeatures['auinspd'][1], DB_DB_GLOBAL, listOfSteps)
        thePlugin.writeTIALogic('''
// Setpoint speed increase rate (Unit/s)
IF ($ConditionInSpd$) THEN
	$name$.AuSPSpd.InSpd := $InSpd$;
ELSE
	$name$.AuSPSpd.InSpd := $IncreaseSpeed$;
END_IF;''')
    else:
        thePlugin.writeTIALogic('''
// Setpoint speed increase rate (Unit/s)
$name$.AuSPSpd.InSpd := $IncreaseSpeed$;''')

    # === errorcalc ==========================================================================================================================
    if MeasuredValue <> "" and "errorcalc" in RequestedFeatures:
        thePlugin.writeTIALogic('''
(* Copy of controller error *)
$name$Err.HFPos := $MV$.PosSt - $name$.ActSP;
$name$Err.IOError := $MV$.IOErrorW ;
$name$Err.IOSimu := $MV$.IOSimuW;
''')

    #================================================================================================================

    # Step 1.4: Driving the controller. HMV (Hardware Measured value. Feedback Measured Value of the controlled process.)
    thePlugin.writeTIALogic('''
(*Driving***********)
''')

    if MeasuredValue <> "" and "errordeadband" in RequestedFeatures:
        MV = Decorator.decorateExpression(MeasuredValue, DB_DB_GLOBAL, listOfSteps)
        ED = Decorator.decorateExpression(RequestedFeatures['errordeadband'][0], DB_DB_GLOBAL, listOfSteps)

        thePlugin.writeTIALogic('''
// Deadband calculation
IF ABS($MV$ - $name$.ActSP) < $ED$ THEN
     $name$.HMV :=$name$.ActSP;
ELSIF $MV$ > $name$.ActSP + $ED$ THEN
    $name$.HMV := $MV$ - $ED$ ;
ELSIF $MV$ < $name$.ActSP - $ED$ THEN
    $name$.HMV := $MV$ + $ED$;
ELSE
     $name$.HMV := $MV$;    
END_IF;
''')
    elif MeasuredValue <> "":
        MV = Decorator.decorateExpression(MeasuredValue, DB_DB_GLOBAL, listOfSteps)
        thePlugin.writeTIALogic('''$name$.HMV := $MV$;
''')
    else:
        thePlugin.writeTIALogic('''$name$.HMV := 0.0 ; // to complete
''')

    thePlugin.writeTIALogic('''
DB_ERROR_SIMU.$name$_DL_E := 0 ; // To complete
DB_ERROR_SIMU.$name$_DL_S := 0 ; // To complete
''')
    if VariablesToCreate != "":
        thePlugin.writeWarningInUABLog("You need to add following variables: " + VariablesToCreate)

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
