# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from java.util import Vector
from java.util import ArrayList
from research.ch.cern.unicos.utilities import AbsolutePathBuilder
import os

import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)
    
import TIALogic_DefaultAlarms_Template
reload(TIALogic_DefaultAlarms_Template)

import ucpc_library.shared_grafcet_parsing
reload(ucpc_library.shared_grafcet_parsing)

import ProcessFeatures
reload(ProcessFeatures)

def OnOffLogic(thePlugin, theRawInstances, master, name, LparamVector):
    Lparam1,Lparam2,Lparam3,Lparam4,Lparam5,Lparam6,Lparam7,Lparam8,Lparam9,Lparam10=TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    # Init external classes -----------------------------------------------------------
    GrafcetParser   = ucpc_library.shared_grafcet_parsing.SharedGrafcetParsing()        
    Decorator       = ucpc_library.shared_decorator.ExpressionDecorator()               
    
    # Load parameters -----------------------------------------------------------------
    DB_DB_GLOBAL      = Lparam1                                                           # Name of the DB
    listOfSteps     = Lparam10.split(',')                      # list of steps of DB_DB_GLOBAL (empty if DB_DB_GLOBAL is "")
    
    ConditionON      = Decorator.decorateExpression(Lparam2, DB_DB_GLOBAL, listOfSteps)   # condition for AuOnR
    ConditionOFF     = Decorator.decorateExpression(Lparam3, DB_DB_GLOBAL, listOfSteps)   # condition for AuOffR
    
    Features    = Lparam8   # List of optional features
    
    # List of supported features with number of parameters ----------------------------
    SupportedFeatures = {
        'dbglobal'      :[1,"string"],                          # takes: db name; specifies name of the global DB
        'anticourtcycle':[4,"string","OnOff", "AnalogParameter", "AnalogStatus"],    # takes: TON timer name, OnOff to reset, APAR for rest time, AS to keep remaining time
        'worktime'      :[4, "string", "OnOff", "AnalogStatus", "AnalogParameter"],  # takes: RTM timer name, OnOff to Reset, AS to keep working time, APAR with offset
        'runtime'       :[4, "string", "OnOff", "AnalogStatus", "AnalogParameter"],  # takes: RTM timer name, OnOff to Reset, AS to keep run time, APAR with offset
        'auaumoronfault':[-1],                                  # takes: list of names of redundant devices
        'auihfomo'      :[1,"string"],                          # takes: logic to set AuIhFoMo input
        'auihmmo'       :[1,"string"],                          # takes: logic to set AuIhMMo input
        'auaumor'       :[0],                                   # takes: nothing; sets AuAuMoR  := AuOnR
        'lastruntime'   :[2,"string","AnalogStatus"],           # takes: RTM timer name, AS to keep the value
        'auackstartinterlock':[0],                              # takes: nothing; sets AuAlAck on F_EDGE of StartISt, if no FS or TS
        'startcount'    :[2, "OnOff", "AnalogStatus"],          # takes: OnOff to reset, AS to keep number of starts; Counts starts of the device
        'startcountwoff':[4,"OnOff", "AnalogStatus"],           # takes: OnOff to reset, AS to keep number of starts; Counts starts of the device
        'aualackmaster' :[1, "string"]                          # takes: the name of 'master' device
    }

    # Process features ----------------------------------------------------------------
    RequestedFeatures = ProcessFeatures.parseFeatures(Features)                     # parse features
    ProcessFeatures.checkFeatures(name, RequestedFeatures, SupportedFeatures, thePlugin, theRawInstances)  # check corectness of parameters numbers
    
    # Handle AuOnR ans AuOffR ---------------------------------------------------------
    if not ConditionON and not ConditionOFF:
        thePlugin.writeWarningInUABLog("OnOff $name$: No condition for AuOnR nor for AuOffR provided! Assumed: AuOnR := FALSE, AuOffR := TRUE")
        AuOnR = "FALSE"
        AuOffR = "TRUE"
    else:
        if not ConditionOFF:        # if off condition not provided, make it complementary to On condition
            AuOffR = '''NOT $name$.AuOnR'''
        else:
            AuOffR = '''$ConditionOFF$'''
    
        if not ConditionON:         # if on conditions not provided, make it complementary to off condition
            AuOnR = '''NOT ($ConditionOFF$)'''
        else:
            AuOnR = '''$ConditionON$'''
    
    # Step 1.0. Header of the file
    thePlugin.writeTIALogic('''//
FUNCTION $name$_DL : VOID
TITLE = '$name$_DL'
//
// Dependent Logic of $name$
//
// Master: 	$master$
// Name: 	$name$
(*
 Lparam1:	$Lparam1$	// Name of the DB of the DB_GLOBAL
 Lparam2:	$Lparam2$   // Condition for AuOnR
 Lparam3:	$Lparam3$   // Condition for AuOffR
 Lparam4:	$Lparam4$	
 Lparam5:	$Lparam5$   
 Lparam6:	$Lparam6$
 Lparam7:	$Lparam7$   
 Lparam8:	$Lparam8$   // List of optional features
 Lparam9:	$Lparam9$
 Lparam10:  $Lparam10$
*)
AUTHOR: 'ICE/PLC'
NAME: 'Logic_DL'
FAMILY: 'ONOFF'
VAR_TEMP
   old_status : DWORD;
   mode_RTM_WT: INT;
   mode_RTM_LR: INT;''')

    if "runtime" in RequestedFeatures:
        thePlugin.writeTIALogic('''
   mode_RTM_RT: INT;''')

    thePlugin.writeTIALogic('''
END_VAR
BEGIN
''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')
    
    VariablesToCreate = ""
    
    thePlugin.writeTIALogic('''	
(*Position Management*)
$name$.AuOnR := $AuOnR$;
$name$.AuOffR:= $AuOffR$;
''')

    # Feature: AuAuMoR ----------------------------------------------------------------
    if "auaumor" in RequestedFeatures:
        thePlugin.writeTIALogic('''
$name$.AuAuMoR  := $name$.AuOnR;''')
    
    # Feature: AuIhFoMo ---------------------------------------------------------------
    if "auihfomo" in RequestedFeatures:
        AuIhFoMo = Decorator.decorateExpression(RequestedFeatures['auihfomo'][0], DB_DB_GLOBAL, listOfSteps)
        thePlugin.writeTIALogic('''
$name$.AuIhFoMo := $AuIhFoMo$;''')

    # Feature: AuIhMMo ----------------------------------------------------------------
    if "auihmmo" in RequestedFeatures:
        AuIhMMo = Decorator.decorateExpression(RequestedFeatures['auihmmo'][0], DB_DB_GLOBAL, listOfSteps)
        thePlugin.writeTIALogic('''
$name$.AuIhMMo := $AuIhMMo$;''')

    # Feature: Global DB --------------------------------------------------------------
    if "dbglobal" in RequestedFeatures:   # argument with name of global DB, has to be a single string, no spaces
        if ' ' in RequestedFeatures['dbglobal'][0]: 
            thePlugin.writeErrorInUABLog("PID : Param8 feature (dbglobal="+RequestedFeatures['dbglobal'][0]+''') is invalid. 
The dbglobal variable must be a valid siemens variable name, i.e. a string with no spaces.''')
        DB_Global = RequestedFeatures['dbglobal'][0] + "."
    else:
        DB_Global = ""

    # Feature: Anticourt cycle --------------------------------------------------------
    if "anticourtcycle" in RequestedFeatures:
        VariablesToCreate = VariablesToCreate + '''
        $name$ACrst:\tBOOL;\t// ACC reset
        F_$name$SI:\tBOOL;\t// SI falling edge
        F_$name$SIo:\tBOOL;\t// SI falling edge old value
        '''
        ACC_TON     = RequestedFeatures['anticourtcycle'][0].strip()    # timer name
        ACC_Reset   = RequestedFeatures['anticourtcycle'][1].strip()    # OnOff to reset ACC
        ACC_APRest  = RequestedFeatures['anticourtcycle'][2].strip()    # APAR for ACC duration
        ACC_ASRest  = RequestedFeatures['anticourtcycle'][3].strip()    # AS for time left
        
        thePlugin.writeTIALogic('''
(* ---------------------------------------  $name$ Restart CountDown ----------------------------------   *)

(*Timer Time assignment*)
$ACC_TON$.TON (IN := NOT  $name$.OutOnOV AND NOT $DB_Global$$name$ACrst,
        PT := DINT_TO_TIME ( REAL_TO_DINT (1000.0 * $ACC_APRest$.PosSt)));	

(*Timer Input edge assignment*)
(*When Pump off Timer starts*)
(*When Reset Command On Timer Resets*)

IF $ACC_Reset$.OnSt THEN
    $DB_Global$$name$ACrst := TRUE;
END_IF;
IF $name$.OutOnOV THEN
    $DB_Global$$name$ACrst := FALSE;
END_IF;

(*Count Down evaluation*)

IF NOT $ACC_TON$.IN OR $ACC_TON$.Q THEN
    $ACC_ASRest$.AuPosR := 0.0;
ELSE
    $ACC_ASRest$.AuPosR := $ACC_APRest$.PosSt - ((DINT_TO_REAL (TIME_TO_DINT ($ACC_TON$.ET))) / 1000.0);
END_IF;

// Falling edge of StartI alarm for anti-court cycle $name$
$DB_Global$F_$name$SI := F_EDGE (new := $name$.StartISt, old := $DB_Global$F_$name$SIo);

IF $DB_Global$F_$name$SI AND $name$.EnRstartSt AND NOT $name$.TStopISt THEN
    $name$.AuAlAck := TRUE;
END_IF;
''')

    # Feature: Auto Acknowledge StartInterlock ----------------------------------------
    #sets AuAlAck on F_EDGE of StartISt, if no FS or TS 
    if "auackstartinterlock" in RequestedFeatures:
        if "anticourtcycle" not in RequestedFeatures:
            VariablesToCreate = VariablesToCreate + '''
        F_$name$SI:\tBOOL;\t// SI falling edge
        F_$name$SIo:\tBOOL;\t// SI falling edge old value
        '''
            thePlugin.writeTIALogic('''
// AuAlAck on falling edge of StartI alarm for $name$
$DB_Global$F_$name$SI := F_EDGE (new := $name$.StartISt, old := F_$DB_Global$$name$SIo);

IF $DB_Global$F_$name$SI AND $name$.EnRstartSt AND NOT $name$.TStopISt THEN
    $name$.AuAlAck := TRUE;
END_IF;
    ''')

    # Feature: WorkTime caluclation ---------------------------------------------------
    if "worktime" in RequestedFeatures:
        WT_RTM      = RequestedFeatures['worktime'][0].strip()      # RTM timer measuring WorkTime
        WT_Reset    = RequestedFeatures['worktime'][1].strip()      # OnOff to reset timer
        WT_Time     = RequestedFeatures['worktime'][2].strip()      # AS keeping WorkTime
        WT_Offset   = RequestedFeatures['worktime'][3].strip()      # Offset time for WorkTime
        
        thePlugin.writeTIALogic('''
(*Counter of time*)
//Working TIME OF the OnOff                
IF $WT_Reset$.OnSt THEN
    mode_RTM_WT:=2; //Reset
ELSIF $name$.OnSt THEN
    mode_RTM_WT:=1; //COUNTING
ELSE
    mode_RTM_WT:=0; //STOP
END_IF;

$WT_RTM$(mode:= mode_RTM_WT 	//IN
                    ,NewValue:=0 	//IN
                    );

$WT_Time$.AuPosR := DINT_TO_REAL($WT_RTM$.CurrentValue)/3600.0 + $WT_Offset$.PosSt;
''')

    if "runtime" in RequestedFeatures:
        RT_RTM      = RequestedFeatures['runtime'][0].strip()      # RTM timer measuring runtime
        RT_Reset    = RequestedFeatures['runtime'][1].strip()      # OnOff to reset timer
        RT_Time     = RequestedFeatures['runtime'][2].strip()      # AS keeping runtime
        RT_Offset   = RequestedFeatures['runtime'][3].strip()      # Offset time for runtime
        
        thePlugin.writeTIALogic('''
(*Counter of time*)
//RUNTIME OF the OnOff                
IF $RT_Reset$.OnSt OR NOT $name$.OnSt THEN
    mode_RTM_RT:=2; //Reset
ELSIF $name$.OnSt THEN
    mode_RTM_RT:=1; //COUNTING
ELSE
    mode_RTM_RT:=0; //STOP
END_IF;

$RT_RTM$(mode:= mode_RTM_RT 	//IN
                    ,NewValue:=0 	//IN
                    );

$RT_Time$.AuPosR := DINT_TO_REAL($RT_RTM$.CurrentValue)/3600.0 + $RT_Offset$.PosSt;
''')

    # Feature: Last Run Time caluclation ----------------------------------------------
    if "lastruntime" in RequestedFeatures:
        LR_RTM = RequestedFeatures["lastruntime"][0].strip()     # RTM timer
        LR_AS  = RequestedFeatures["lastruntime"][1].strip()     # analog status with runTime
        thePlugin.writeTIALogic('''
(*Counter of time*)
IF $name$.OnSt THEN
    mode_RTM_LR:=1; //COUNTING
ELSE
    mode_RTM_LR:=2; //STOP
END_IF;

$LR_RTM$(mode:= mode_RTM_LR 	//IN
                    ,NewValue:=0 	//IN
                    );

$LR_AS$.AuPosR := DINT_TO_REAL($LR_RTM$.CurrentValue)/60.0;
''')

    #=========================== Switch with the redundant OnOff ====================================================
    # used only when working in pairs
    if "auaumoronfault" in RequestedFeatures:
        SwitchCondition = " OR \n".join([device.strip() + ".E_FuStopI" for device in RequestedFeatures["auaumoronfault"]])
        thePlugin.writeTIALogic('''
// If RE of FullStop on other OnOff, set AuAuMoR
IF $SwitchCondition$ THEN
    $name$.AuAuMoR := TRUE;
END_IF;
	''')
    
    #=========================== Starts counter with offset ========================================================
    if 'startcountwoff' in RequestedFeatures:
        SC_Reset    = RequestedFeatures['startcountwoff'][0]    # OnOff to reset counter
        SC_Int      = RequestedFeatures['startcountwoff'][1]    # AS to keep number of starts
        SC_Tot      = RequestedFeatures['startcountwoff'][2]    # AS to keep number of starts + offset
        SC_Ofs      = RequestedFeatures['startcountwoff'][3]    # APAR with offset
        VariablesToCreate = VariablesToCreate + '''
        $name$prvSt:\tBOOL;\t// previous state
        '''
        
        thePlugin.writeTIALogic('''
(*Counter of starts*)
IF $SC_Reset$.OnSt THEN			// Reset
	$SC_Int$.AuPosR := 0;
ELSIF $name$.OnSt AND $DB_Global$$name$prvSt = 0 THEN		                    // If the ANADO is on and previously was off
   $SC_Int$.AuPosR := $SC_Int$.PosSt + 1.0;	// count it
END_IF;

$SC_Tot$.AuPosR  := $SC_Int$.PosSt + $SC_Ofs$.PosSt;

$DB_Global$$name$prvSt := $name$.OnSt;
''')

    #=========================== AuAlAck/AuRStart ====================================================================
    if 'aualackmaster' in RequestedFeatures:
        Actuator    = RequestedFeatures['aualackmaster'][0]    # 'master' actuator
        thePlugin.writeTIALogic('''
//In case AlAck in 'master' actuator, we do AuAlAck/AuRStart on the device
IF $Actuator$.E_MAlAckR OR $Actuator$.AuAlAck THEN
    $name$.AuAlAck := TRUE;
    $name$.AuRStart := TRUE;
END_IF;
''')

    #=========================== Starts counter ====================================================================
    if 'startcount' in RequestedFeatures:
        SC_Reset    = RequestedFeatures['startcount'][0]    # OnOff to reset counter
        SC_Stat     = RequestedFeatures['startcount'][1]    # AS to keep number of starts
        VariablesToCreate = VariablesToCreate + '''
        $name$prvSt:\tBOOL;\t// previous state
        '''
        
        thePlugin.writeTIALogic('''
(*Counter of starts*)
IF $SC_Reset$.OnSt THEN			// Reset
	$SC_Stat$.AuPosR := 0;
ELSIF $name$.OnSt AND $DB_Global$$name$prvSt = 0 THEN		                    // If the ANADO is on and previously was off
   $SC_Stat$.AuPosR := $SC_Stat$.PosSt + 1.0;	// count it
END_IF;

$DB_Global$$name$prvSt := $name$.OnSt;
''')

    if VariablesToCreate != "":
        thePlugin.writeWarningInUABLog("You need to add following variables: " + VariablesToCreate)

        
    # Step 1.4: Fill the IOError and IOSimu from the Inputs linked by the user logic (if procceds)
    thePlugin.writeTIALogic('''
(*IoSimu and IoError*****************)
// The user must connect the IOError and IOSimu from the linked devices ("IN" variable) if proceeds
DB_ERROR_SIMU.$name$_DL_E := 0; // To complete

DB_ERROR_SIMU.$name$_DL_S :=  0; // To complete
''')

    # Not configured alarms
    #Gets all the Digital Alarms that are child of the 'master' object
    theDigitalAlarms, theDigitalAlarmsMultiple, allTheDigitalAlarms, DAListPosition = TIALogic_DefaultAlarms_Template.getDigitalAlarms(theRawInstances, name)
    #Gets all the Analog Alarms that are child of the 'master' object
    theAnalogAlarms, theAnalogAlarmsMultiple, allTheAnalogAlarms, AAListPosition = TIALogic_DefaultAlarms_Template.getAnalogAlarms(theRawInstances, name)

    #-------------------------------------------------------------------------------------------------------------
    # The "writeNotConfiguredDAParameters" function writes default values for DA parameters: 
    #
    # If Input (in spec) is empty, writes IOError, IOSimu and I
    # Else if Input is not empty, IOError, IOSimu and I will be written by main template function
    # If Delay (in spec) is "logic", writes PAlDt
    # Else if Delay is not "logic", PAlDt will be written by main template function
    #
    # NOTE: the user can choose to remove this function call from the user template, in which case he will be 
    # responsible for writing the code for the appropriate pins himself
    #
    #-------------------------------------------------------------------------------------------------------------
    TIALogic_DefaultAlarms_Template.writeNotConfiguredDAParameters(thePlugin, theDigitalAlarms, theDigitalAlarmsMultiple, DAListPosition)

    #-------------------------------------------------------------------------------------------------------------
    # The "writeNotConfiguredAAParameters" function writes default values for AA parameters: 
    #
    # If HH Alarm (in spec) is a number, writes AuEHH. In this case HH will be written by main template function
    # If HH Alarm is "logic", writes AuEHH and HH
    # If HH Alarm is a reference to an object, writes AuEHH. In this case HH will be written by main template function
    # NOTE: the same applies for H Warning, L Warning, and LL Alarm
    # If Input (in spec) is empty, writes IOError, IOSimu and I
    # If Delay (in spec) is "logic", writes PAlDt
    #
    # NOTE: the user can choose to remove this function call from the user template, in which case he will be 
    # responsible for writing the code for the appropriate pins himself
    #
    #-------------------------------------------------------------------------------------------------------------
    TIALogic_DefaultAlarms_Template.writeNotConfiguredAAParameters(thePlugin, theAnalogAlarms, theAnalogAlarmsMultiple, AAListPosition)

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
