# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)


def GLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)
    
    theUnicosProject = thePlugin.getUnicosProject()        
    Decorator = ucpc_library.shared_decorator.ExpressionDecorator()    

# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeTIALogic('''
FUNCTION $name$_GL : VOID
TITLE = '$name$_GL'
//
// Global Logic of $name$
//
(*
 Lparam1: $Lparam1$
 Lparam2: $Lparam2$
 Lparam3: $Lparam3$
 Lparam4: $Lparam4$
 Lparam5: $Lparam5$
 Lparam6: $Lparam6$
 Lparam7: $Lparam7$
 Lparam8: $Lparam8$
 Lparam9: $Lparam9$
 Lparam10:  $Lparam10$
*)
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_GL'
FAMILY: 'GL'
BEGIN
''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    # Step 1.4: IOError IOSimu
    thePlugin.writeTIALogic('''

// Errors and Simulated data
DB_ERROR_SIMU.$name$_GL_E := 0; // To complete
DB_ERROR_SIMU.$name$_GL_S := 0; // To complete
''')

    thePlugin.writeTIALogic(Decorator.decorateExpression('''
    
    
    //RONormalStartCondition
    FDEU_893_CCD_RO_NorStart.HFPos := (FDEU_893_LT2301 > FDEU_893_LT2301_CCD_ON AND 
                                       FDEU_893_CT1001 > FDEU_893_CT1001Start) 
                                    AND NOT FDEU_893_CCD_PFDRUN.OnSt AND NOT FDEU_893_CCD_RO_SDF.OnSt AND NOT FDEU_893_CCD_RO_Rin_Au;
    //ROCPFDStartCondition
    FDEU_893_CCD_RO_PFDCon.HFPos := (FDEU_893_LT2301 > FDEU_893_LT2301_CCD_OFF) AND FDEU_893_CCD_PFDRUN.OnSt AND NOT FDEU_893_CCD_RO_Rin_Au AND NOT FDEU_893_CCD_RO_SDF.OnSt;
    
    //RONormalStopCondition
    FDEU_893_CCD_RO_NorStop.HFPos := (FDEU_893_LT2301 < FDEU_893_LT2301_CCD_OFF ) 
                                        OR (FDEU_893_CT1001 < FDEU_893_CT1001Stop);
    
    //ROEmergencyStopCondition
    FDEU_893_CCD_RO_EmerStop.HFPos := FDEU_893_LT2301_AL1.LWSt;
    
    TON_ROCCD_VR2501.TON(IN :=  FDEU_893_VR2501 > FDEU_893_VR2501SP, PT := MAX (IN1:=DINT_TO_TIME( REAL_TO_DINT ( FDEU_893_VR2501Dt * 1000.0)), IN2:=T#0.001s));
    TON_ROCCD_PT2501.TON(IN :=  FDEU_893_PT2501 > FDEU_893_CCD_PT2501SP, PT := MAX (IN1:=DINT_TO_TIME( REAL_TO_DINT ( FDEU_893_CCD_PT2501Dt *1000.0)), IN2:=T#0.001s));
    TON_ROCCD_CT2701.TON(IN :=  FDEU_893_CT2701 > FDEU_893_CCD_CT2701SP, PT := MAX (IN1:=DINT_TO_TIME( REAL_TO_DINT ( FDEU_893_CT2701Dt *1000.0)), IN2:=T#0.001s));
    
    //ROCCDEndCondition1
    FDEU_893_CCD_RO_END1.HFPos := NOT FDEU_893_CCD_RO_NorStop 
    AND (TON_ROCCD_VR2501.Q OR TON_ROCCD_PT2501.Q OR TON_ROCCD_CT2701.Q);
    
    //ROCCDEndCondition2
    FDEU_893_CCD_RO_END2.HFPos := (FDEU_893_CCD_RO_NorStop AND NOT FDEU_893_CCD_RO_RSM_Sel) 
                                           OR ((FDEU_893_CCD_RO_NorStop AND FDEU_893_CCD_RO_RSM_Sel) 
                                           AND (TON_ROCCD_VR2501.Q OR TON_ROCCD_PT2501.Q OR TON_ROCCD_CT2701.Q)) 
                                            OR FDEU_893_CCD_RO_SDF.OnSt OR (FDEU_893_LT2301 < FDEU_893_LT2301_CCD_OFF );
    
    //Quick Start Enabled. Count down for timer after pushing RO_QUICK button. Set bit FDEU_893_CCD_RO_QS while active
    TP_CCD_RO_QS.tp( IN := FDEU_893_RO_QUICK.OnSt, PT := DINT_TO_TIME( REAL_TO_DINT (FDEU_893_RO_QuickDt * 1000.0 )));
    IF NOT TP_CCD_RO_QS.Q THEN
        FDEU_893_CCD_RO_QSCD.AuPosR := 0.0;
        FDEU_893_CCD_RO_QS.HFPos := FALSE;
    ELSE
        FDEU_893_CCD_RO_QSCD.AuPosR := FDEU_893_RO_QuickDt.PosSt - ((DINT_TO_REAL (TIME_TO_DINT (TP_CCD_RO_QS.ET))) / 1000.0);
        FDEU_893_CCD_RO_QS.HFPos := TRUE;
    END_IF;
    
    // Interval Rinse
    // TimeToRORinse   Real    minutes Operation time remaining before triggering an RO Interval Rinse
    // Decremented in RO Standby state 
    IF (CPC_GLOBAL_VARS.UNICOS_LiveCounter MOD 60) = 0 AND NOT DB_GLOBAL.MMF_UF_Minute_Pass THEN
        IF GRAPH_CCD_RO.STANDBY.X THEN
            IF FDEU_893_CCD_RICD > 0.0 THEN
                FDEU_893_CCD_RICD.AuPosR := FDEU_893_CCD_RICD - 1.0;
            END_IF;
        END_IF;
    END_IF;
    
    // Set to ROMaxOperation each time that the system is put in Standby State (S3-00).
    IF R_EDGE (new := GRAPH_CCD_RO.STANDBY.X, old := DB_GLOBAL.CCD_RO_STANDBY_X_old) THEN
        FDEU_893_CCD_RICD.AuPosR := FDEU_893_CCD_RI_Max;
    END_IF;
    
    // RORinseAuto Bool    N/A Automatic activation of an RO Interval rinse cycle controlled by time:
    // RORinseAuto = TRUE if TimeToRORinse = 0
    FDEU_893_CCD_RO_Rin_Au.HFPos := FDEU_893_CCD_RICD.AuPosR < 1.0;
    
    
    
    (*      DOSING PUMPS CALCULATIONS *)
    
    FDEU_893_AS7201_MASS.HfPos := FDEU_893_FT2401 * FDEU_893_AS7201_Dos;
    IF (FDEU_893_AS7201_Den * FDEU_893_AS7201_Con) > 0.000000001 THEN
        FDEU_893_AS7201_VOL.HfPos := FDEU_893_AS7201_MASS / (FDEU_893_AS7201_Den * FDEU_893_AS7201_Con / 100.0) * 1000.0;
    END_IF;
    FDEU_893_DP7201_VOLPUL.HfPos := FDEU_893_AS7201_Min +(FDEU_893_AS7201_Max - FDEU_893_AS7201_Min) * FDEU_893_AS7201_Per /100.0;
    IF FDEU_893_DP7201_VOLPUL > 0.000000001 THEN
        FDEU_893_DP7201_FREQ.HfPos := FDEU_893_AS7201_VOL /( 3600.0 * FDEU_893_DP7201_VOLPUL);
    END_IF;
    
    IF GRAPH_CCD_RO.S3_01_3.X THEN   // For the starting of the pump, we force a frequency of 0.5 Hz. Otherwise we take the output of the regulator. 
        FDEU_893_DP7301_FREQ.HfPos := 0.5;
    ELSE
        FDEU_893_DP7301_FREQ.HfPos := FDEU_893_DC7301.OutOV;
    END_IF;
    
    (* COUNTERS CALCULATIONS*)
    
    IF R_EDGE (new := FDEU_893_FQ2401, old := DB_GLOBAL.FDEU_893_FQ2401_old) THEN
        IF (GRAPH_CCD_RO.S3_03_1.X OR GRAPH_CCD_RO.S3_03_2.X) THEN
            FDEU_893_FQT2401_CCD.HfPos := FDEU_893_FQT2401_CCD + FDEU_893_FQ2401_Val;
        END_IF;
        IF GRAPH_CCD_RO.S3_04_1.X OR GRAPH_CCD_RO.S3_04_2.X THEN
            FDEU_893_FQT2402_PFD.HfPos := FDEU_893_FQT2402_PFD + FDEU_893_FQ2401_Val;
        END_IF;
        IF GRAPH_CCD_RO.S3_01_2.X OR GRAPH_CCD_RO.S3_01_3.X OR GRAPH_CCD_RO.S3_01_4.X OR GRAPH_CCD_RO.S3_02.X THEN
            FDEU_893_FQT2403_PUR.HfPos := FDEU_893_FQT2403_PUR + FDEU_893_FQ2401_Val;
        END_IF;
    END_IF;
    IF F_EDGE(new := GRAPH_CCD_RO.S3_03_2.X, old := DB_GLOBAL.S3_03_2_old) THEN
        FDEU_893_FQT2401_CCD.HfPos := 0.0;
        FDEU_893_FQT2402_PFD.HfPos := 0.0;
        FDEU_893_FQT2403_PUR.HfPos := 0.0;
        FDEU_893_FQT2702_PFD.HfPos := 0.0;
        FDEU_893_FQT2703_PUR.HfPos := 0.0;
    END_IF;
    
    IF F_EDGE(new := GRAPH_CCD_RO.S3_05_4.X, old := DB_GLOBAL.S3_05_4_old) THEN
        FDEU_893_FQT2704_SDF.HfPos := 0.0;
    END_IF;
    
    IF R_EDGE (new := FDEU_893_FQ2701, old := DB_GLOBAL.FDEU_893_FQ2701_old) THEN
        IF GRAPH_CCD_RO.S3_04_1.X OR GRAPH_CCD_RO.S3_04_2.X THEN
            FDEU_893_FQT2702_PFD.HfPos := FDEU_893_FQT2702_PFD + FDEU_893_FQ2701_Val;
        END_IF;
        IF GRAPH_CCD_RO.S3_01_2.X OR GRAPH_CCD_RO.S3_01_3.X OR GRAPH_CCD_RO.S3_01_4.X OR GRAPH_CCD_RO.S3_02.X THEN
            FDEU_893_FQT2703_PUR.HfPos := FDEU_893_FQT2703_PUR + FDEU_893_FQ2701_Val;
        END_IF;
        IF GRAPH_CCD_RO.S3_05_2.X OR GRAPH_CCD_RO.S3_05_3.X OR GRAPH_CCD_RO.S3_05_4.X THEN
            FDEU_893_FQT2704_SDF.HfPos := FDEU_893_FQT2704_SDF + FDEU_893_FQ2701_Val;
        END_IF;
    END_IF;
        
    FDEU_893_FQT2400.HfPos := FDEU_893_FQT2401_CCD + FDEU_893_FQT2402_PFD + FDEU_893_FQT2403_PUR;
    FDEU_893_FQT2700.HfPos := FDEU_893_FQT2702_PFD + FDEU_893_FQT2703_PUR;
    
    IF FDEU_893_FQT2400 > 0.000000001 THEN
        FDEU_893_VR2501.HfPos := 100.0 * ( FDEU_893_FQT2400 - FDEU_893_FQT2700) / FDEU_893_FQT2400;
    END_IF;
    
    
    
    (*SET POINT CALCULATIONS *)
    IF GRAPH_CCD_RO.S3_03_1.X OR GRAPH_CCD_RO.S3_03_2.X THEN
        FDEU_893_FC2403Sp.HFPos := FDEU_893_FC2403_CCDSp;
    ELSIF GRAPH_CCD_RO.S3_04_2.X OR GRAPH_CCD_RO.S3_07_5.X OR GRAPH_CCD_RO.S3_07_6.X THEN
        FDEU_893_FC2403Sp.HFPos := FDEU_893_FC2403_PFDSp;
    ELSE
        FDEU_893_FC2403Sp.HFPos := FDEU_893_FC2403_SDFSp;
    END_IF;

    // PC2401 - RO Feed pump
    IF GRAPH_CCD_RO.S3_03_1.X OR GRAPH_CCD_RO.S3_03_2.X THEN
        FDEU_893_PC2401Sp.HFPos := FDEU_893_CCD_PC2401Sp;
    ELSE
        FDEU_893_PC2401Sp.HFPos := FDEU_893_CCD_PC2401SpRO;
    END_IF;
'''))


    thePlugin.writeTIALogic('''
    
    
    
    
(**********************************************
*******                                 *******
******* LIST OF TIMERS FOR THE DB_GLOBAL  *******
*******                                 *******    
***********************************************)
''')
    listOfTimers = ['CCD_RO_S3_01_2','CCD_RO_S3_01_3','CCD_RO_S3_01_4','CCD_RO_S3_03_1','CCD_RO_S3_04_1','CCD_RO_S3_05_2','CCD_RO_S3_05_3','CCD_RO_S3_05_5','CCD_RO_S3_05_6','CCD_RO_S3_06_2','CCD_RO_S3_06_3','CCD_RO_S3_06_4','CCD_RO_S3_06_5','CCD_RO_S3_06_6','CCD_RO_S3_07_2','CCD_RO_S3_07_3','CCD_RO_S3_07_4','CCD_RO_S3_07_6','CCD_RO_S3_07_7','CCD_RO_S3_07_8','CCD_RO_S3_08_1','CCD_RO_S3_08_2']
    listOfSteps = ['S3_01_2','S3_01_3','S3_01_4','S3_03_1','S3_04_1','S3_05_2','S3_05_3','S3_05_5','S3_05_6','S3_06_2','S3_06_3','S3_06_4','S3_06_5','S3_06_6','S3_07_2','S3_07_3','S3_07_4','S3_07_6','S3_07_7','S3_07_8','S3_08_1','S3_08_2']
    
    i=0
    for step in listOfTimers:
        timer = "TON_" + step
        AS = step + "CD"
        APAR = step + "Dt"
        
        instance = theUnicosProject.findInstanceByName(APAR)
        unit = "s" 
        if instance is None:
            thePlugin.writeErrorInUABLog("The Analog Parameter " + APAR + "does not exist in the Spec File")
        else:
            unit = instance.getAttributeData("SCADADeviceGraphics:Unit")
        #The counting at the PLC level is handled in seconds. We retrieve the unit specified at the spec to automatically make the conversion. 
        if unit == "h":
            changeTimeUnits = "3600.0"
        elif unit == "min":
            changeTimeUnits = "60.0"
        elif unit == "s":
            changeTimeUnits = "1.0"
        else:
            thePlugin.writeErrorInUABLog('Units of Analog Parameters representing time delays must be "h" for hours, "min" for minuts or "s" for seconds')
       
        thePlugin.writeTIALogic('''

$timer$.TON( IN := ((GRAPH_CCD_RO.$listOfSteps[i]$.X)),
                       PT := MAX (IN1:=DINT_TO_TIME( REAL_TO_DINT ($APAR$.PosSt * 1000.0 * $changeTimeUnits$)), IN2:=T#0.001s));
IF NOT $timer$.IN OR $timer$.Q THEN
    $AS$.AuPosR := 0.0;
ELSE
    $AS$.AuPosR := $APAR$.PosSt - ((DINT_TO_REAL (TIME_TO_DINT ($timer$.ET))) / 1000.0 / $changeTimeUnits$);
END_IF;''')
        i=i+1


    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
