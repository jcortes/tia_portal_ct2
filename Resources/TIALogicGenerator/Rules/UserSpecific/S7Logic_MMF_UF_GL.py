# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)


def GLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)
    
    theUnicosProject = thePlugin.getUnicosProject()
    Decorator = ucpc_library.shared_decorator.ExpressionDecorator()    

# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeTIALogic('''
FUNCTION $name$_GL : VOID
TITLE = '$name$_GL'
//
// Global Logic of $name$
//
(*
 Lparam1: $Lparam1$
 Lparam2: $Lparam2$
 Lparam3: $Lparam3$
 Lparam4: $Lparam4$
 Lparam5: $Lparam5$
 Lparam6: $Lparam6$
 Lparam7: $Lparam7$
 Lparam8: $Lparam8$
 Lparam9: $Lparam9$
 Lparam10:  $Lparam10$
*)
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_GL'
FAMILY: 'GL'
BEGIN
''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    # Step 1.4: IOError IOSimu
    thePlugin.writeTIALogic('''

// Errors and Simulated data
DB_ERROR_SIMU.$name$_GL_E := 0; // To complete
DB_ERROR_SIMU.$name$_GL_S := 0; // To complete
''')

    thePlugin.writeTIALogic(Decorator.decorateExpression('''
    FDEU_893_MMF_UF_StartCon.HFPos := FDEU_893_LT1401 < FDEU_893_LT1401_MMF_ON;
    FDEU_893_MMF_UF_StopCon.HFPos := (FDEU_893_LT1401 > FDEU_893_LT1401_MMF_OFF)
                                    OR FDEU_893_FT1101_AL3 OR FDEU_893_FT5101_AL3 OR FDEU_893_FT5201_AL3;
    
    TON_MMF_UF_DP1201.TON(  IN := FDEU_893_DP1201 > FDEU_893_DP1201_Thres_H , 
                            PT:= MAX (IN1:=DINT_TO_TIME( REAL_TO_DINT (FDEU_893_MMF_BWDt  * 1000.0)), IN2:=T#0.001s));
    
    TON_MMF_UF_DP1301.TON(  IN := FDEU_893_DP1301 > FDEU_893_DP1301_Thres_H , 
                            PT:= MAX (IN1:=DINT_TO_TIME( REAL_TO_DINT (FDEU_893_UF_BWDt  * 1000.0)), IN2:=T#0.001s));
    
    IF (CPC_GLOBAL_VARS.UNICOS_LiveCounter MOD 60) = 0 AND NOT DB_GLOBAL.MMF_UF_Minute_Pass THEN
        IF GRAPH_MMF_UF_FIL.S1_02_RUN.X THEN
            IF FDEU_893_MMF_OPBWCD > 0.0 THEN
                FDEU_893_MMF_OPBWCD.AuPosR := FDEU_893_MMF_OPBWCD - 1.0;
            END_IF;
            IF FDEU_893_UF_OPCD> 0.0 THEN
                FDEU_893_UF_OPCD.AuPosR := FDEU_893_UF_OPCD - 1.0;
            END_IF;
            IF FDEU_893_UF_OXOPCD> 0.0 THEN
                FDEU_893_UF_OXOPCD.AuPosR := FDEU_893_UF_OXOPCD - 1.0;
            END_IF;
            IF FDEU_893_UF_ACOPCD> 0.0 THEN
                FDEU_893_UF_ACOPCD.AuPosR := FDEU_893_UF_ACOPCD - 1.0;
            END_IF;
        END_IF;
        IF FDEU_893_MMF_BWCD > 0.0 THEN
            FDEU_893_MMF_BWCD.AuPosR := FDEU_893_MMF_BWCD - 1.0;
        END_IF;
        IF FDEU_893_UF_BWCD > 0.0 THEN
            FDEU_893_UF_BWCD.AuPosR : = FDEU_893_UF_BWCD - 1.0;
        END_IF;
    END_IF; 
    DB_GLOBAL.MMF_UF_Minute_Pass := CPC_GLOBAL_VARS.UNICOS_LiveCounter MOD 60 = 0;
    IF GRAPH_MMF_BW.B1_09_3B.X THEN
        FDEU_893_MMF_OPBWCD.AuPosR := FDEU_893_MMF_BW_Max;
        FDEU_893_MMF_BWCD.AuPosR := FDEU_893_MMF_BW_Min;
    END_IF;
    IF GRAPH_UF_BW.B2_15_3.X THEN
        FDEU_893_UF_OPCD.AuPosR := FDEU_893_UF_BW_Max;
        FDEU_893_UF_BWCD.AuPosR := FDEU_893_UF_BW_Min;
    END_IF;

    FDEU_893_MMF_BW_Con.HFPos := TON_MMF_UF_DP1201.Q OR FDEU_893_MMF_BWCD.AuPosR = 0 OR FDEU_893_MMF_OPBWCD.AuPosR = 0;
    FDEU_893_UF_BW_Con.HFPos :=  TON_MMF_UF_DP1301.Q OR FDEU_893_UF_OPCD.AuPosR = 0 OR FDEU_893_UF_BWCD.AuPosR = 0;
    FDEU_893_UF_OXBW_Con.HFPos := FDEU_893_UF_OXOPCD.AuPosR = 0;
    FDEU_893_UF_ACBW_Con.HFPos := FDEU_893_UF_ACOPCD.AuPosR = 0;
    FDEU_893_MMF_UF_BW_En.HFPos := NOT (GRAPH_MMF_UF.MMF_BW.X OR GRAPH_MMF_UF.UF_BW.X OR GRAPH_MMF_UF.UF_OX_BW.X OR GRAPH_MMF_UF.UF_AC_BW.X) AND FDEU_893_P5101.EnRstartSt;
    FDEU_893_MMF_BW_En.HFPos := FDEU_893_MMF_UF_BW_En AND GRAPH_UF_BW.INIT.X AND GRAPH_UF_ACBW.INIT.X AND GRAPH_UF_OXBW.INIT.X;
    FDEU_893_UF_BW_En.HFPos := FDEU_893_MMF_UF_BW_En AND GRAPH_MMF_BW.INIT.X AND GRAPH_UF_ACBW.INIT.X AND GRAPH_UF_OXBW.INIT.X OR GRAPH_UF_ACBW.SYNC.X OR GRAPH_UF_ACBW.SYNC.X;
    FDEU_893_UF_OXBW_En.HFPos := FDEU_893_MMF_UF_BW_En AND GRAPH_MMF_BW.INIT.X AND GRAPH_UF_BW.INIT.X AND GRAPH_UF_ACBW.INIT.X;
    FDEU_893_UF_ACBW_En.HFPos := FDEU_893_MMF_UF_BW_En AND GRAPH_MMF_BW.INIT.X AND GRAPH_UF_BW.INIT.X AND GRAPH_UF_OXBW.INIT.X;
    
    IF GRAPH_MMF_UF_FIL.S1_01_5.X OR GRAPH_MMF_UF_FIL.S1_02_RUN.X OR GRAPH_MMF_UF_FIL.S1_03_1.X THEN
        FDEU_893_FC1101Sp.HFPos := FDEU_893_FC1101_FilSp;
    ELSIF GRAPH_MMF_BW.B1_01_2A.X OR GRAPH_MMF_BW.B1_01_3A.X OR GRAPH_MMF_BW.B1_02A.X OR GRAPH_MMF_BW.B1_03_1A.X OR GRAPH_MMF_BW.B1_03_2A.X OR GRAPH_MMF_BW.B1_04_1A.X OR GRAPH_MMF_BW.B1_04_2A.X OR GRAPH_MMF_BW.B1_05A.X OR GRAPH_MMF_BW.B1_01_2B.X OR GRAPH_MMF_BW.B1_01_3B.X OR GRAPH_MMF_BW.B1_02B.X OR GRAPH_MMF_BW.B1_03_1B.X OR GRAPH_MMF_BW.B1_03_2B.X OR GRAPH_MMF_BW.B1_04_1B.X OR GRAPH_MMF_BW.B1_04_2B.X OR GRAPH_MMF_BW.B1_05B.X THEN
        FDEU_893_FC1101Sp.HFPos := FDEU_893_FC1101_BWSp;
    ELSIF GRAPH_MMF_BW.B1_08A.X OR GRAPH_MMF_BW.B1_08B.X THEN
        FDEU_893_FC1101Sp.HFPos := FDEU_893_FC1101_RinSp;
    ELSIF GRAPH_UF_BW.B2_14.X THEN
        FDEU_893_FC1101Sp.HFPos := FDEU_893_FC1101_FWFSp;
    END_IF;
    
    IF GRAPH_UF_BW.B2_08.X OR GRAPH_UF_BW.B2_11.X THEN
        FDEU_893_FC5201Sp.HFPos := FDEU_893_FC5201_BWSp;
    ELSIF GRAPH_UF_OXBW.B3_07_3.X OR GRAPH_UF_OXBW.B3_08.X OR GRAPH_UF_OXBW.B3_09_1.X OR GRAPH_UF_OXBW.B3_10_3.X OR GRAPH_UF_OXBW.B3_11.X OR GRAPH_UF_OXBW.B3_12_1.X OR GRAPH_UF_ACBW.B4_07_3.X OR GRAPH_UF_ACBW.B4_08.X OR GRAPH_UF_ACBW.B4_09_1.X OR GRAPH_UF_ACBW.B4_10_3.X OR GRAPH_UF_ACBW.B4_11.X OR GRAPH_UF_ACBW.B4_12_1.X THEN
        FDEU_893_FC5201Sp.HFPos := FDEU_893_FC5201_BWSp;
    END_IF;
    
    (*      DOSING PUMPS CALCULATIONS *)
    
    FDEU_893_NAOCL7001_MASS.HFPos := FDEU_893_FT5201 * FDEU_893_NAOCL7001_Dos;
    IF (FDEU_893_NAOCL7001_Den * FDEU_893_NAOCL7001_Con) > 0.0000000001 THEN
        FDEU_893_NAOCL7001_VOL.HFPos := FDEU_893_NAOCL7001_MASS / (FDEU_893_NAOCL7001_Con / 100.0 * FDEU_893_NAOCL7001_Den) *1000.0;
    END_IF;

    FDEU_893_DP7001_VOLPUL.HFPos := FDEU_893_NAOCL7001_Min + ( FDEU_893_NAOCL7001_Max - FDEU_893_NAOCL7001_Min ) * FDEU_893_NAOCL7001_Per /100.0;
    IF FDEU_893_DP7001_VOLPUL > 0.0000000001 THEN 
        FDEU_893_DP7001_FREQ.HFPos := FDEU_893_NAOCL7001_VOL /(3600 * FDEU_893_DP7001_VOLPUL);
    END_IF;

    FDEU_893_HCL7101_MASS.HFPos := FDEU_893_FT5201 * FDEU_893_HCL7101_Dos;
    IF (FDEU_893_HCL7101_Den * FDEU_893_HCL7101_Con) > 0.0000000001 THEN
        FDEU_893_HCL7101_VOL.HFPos := FDEU_893_HCL7101_MASS / (FDEU_893_HCL7101_Con / 100.0 * FDEU_893_HCL7101_Den) *1000.0;
    END_IF;

    FDEU_893_DP7101_VOLPUL.HFPos := FDEU_893_HCL7101_Min + ( FDEU_893_HCL7101_Max - FDEU_893_HCL7101_Min ) * FDEU_893_HCL7101_Per /100.0;
    IF FDEU_893_DP7101_VOLPUL > 0.0000000001 THEN 
        FDEU_893_DP7101_FREQ.HFPos := FDEU_893_HCL7101_VOL /(3600 * FDEU_893_DP7101_VOLPUL);
    END_IF;
    
    DB_GLOBAL.MMF_UF_P5101_Req := GRAPH_MMF_BW.B1_04_2A.X OR GRAPH_MMF_BW.B1_04_2B.X;
    
    (* Requirement to stop feed pump (P1101) and close UF valves during BW if LT1401 > MMF_OFF until LT1401 < MMF_ON 
       Use hysteresis block output as input to alarm FDEU_893_LT1401_MMFBW_AL *)
    MMF_UF_LT1401_HYST(IN:=$master$_LT1401,
                                H:=$master$_LT1401_MMF_OFF,
                                L:=$master$_LT1401_MMF_ON);
    '''))
    
    
    thePlugin.writeTIALogic('''
    
    
    
    
(**********************************************
*******                                 *******
******* LIST OF TIMERS FOR THE DB_GLOBAL  *******
*******                                 *******    
***********************************************)
''')    
    listOfTimers = [('MMF_UF_FIL_S1_01_4','MMF_UF_FIL_S1_01_5','MMF_UF_FIL_S1_03_1','MMF_UF_FIL_S1_03_2'),('MMF_BW_B1_01_3A','MMF_BW_B1_02A_AIR','MMF_BW_B1_03_1A','MMF_BW_B1_04_2A','MMF_BW_B1_05A_BW','MMF_BW_B1_06_1A','MMF_BW_B1_07_2A','MMF_BW_B1_08A_BWR','MMF_BW_B1_09_1A','MMF_BW_B1_01_3B','MMF_BW_B1_02B_AIR','MMF_BW_B1_03_1B','MMF_BW_B1_04_2B','MMF_BW_B1_05B_BW','MMF_BW_B1_06_1B','MMF_BW_B1_07_2B','MMF_BW_B1_08B_BWR','MMF_BW_B1_09_1B'),('UF_BW_B2_01_2','UF_BW_B2_02_AIR','UF_BW_B2_03_1','UF_BW_B2_05_PUR','UF_BW_B2_07_2','UF_BW_B2_08_TOP','UF_BW_B2_09_1','UF_BW_B2_10_2','UF_BW_B2_11_BOT','UF_BW_B2_12_1','UF_BW_B2_13_3','UF_BW_B2_14_FWD','UF_BW_B2_15_1'),('UF_OXBW_B3_01_2','UF_OXBW_B3_02_AIR','UF_OXBW_B3_03_1','UF_OXBW_B3_05_PUR','UF_OXBW_B3_07_2','UF_OXBW_B3_07_3','UF_OXBW_B3_08_TOP','UF_OXBW_B3_09_1','UF_OXBW_B3_09_2','UF_OXBW_B3_10_2','UF_OXBW_B3_10_3','UF_OXBW_B3_11_BOT','UF_OXBW_B3_12_1','UF_OXBW_B3_12_2','UF_OXBW_B3_13'),('UF_ACBW_B4_01_2','UF_ACBW_B4_02_AIR','UF_ACBW_B4_03_1','UF_ACBW_B4_05_PUR','UF_ACBW_B4_07_2','UF_ACBW_B4_07_3','UF_ACBW_B4_08_TOP','UF_ACBW_B4_09_1','UF_ACBW_B4_09_2','UF_ACBW_B4_10_2','UF_ACBW_B4_10_3','UF_ACBW_B4_11_BOT','UF_ACBW_B4_12_1','UF_ACBW_B4_12_2','UF_ACBW_B4_13')]
    
    listOfSteps = [('S1_01_4','S1_01_5','S1_03_1','S1_03_2'),('B1_01_3A','B1_02A','B1_03_1A','B1_04_2A','B1_05A','B1_06_1A','B1_07_2A','B1_08A','B1_09_1A','B1_01_3B','B1_02B','B1_03_1B','B1_04_2B','B1_05B','B1_06_1B','B1_07_2B','B1_08B','B1_09_1B'),('B2_01_2','B2_02','B2_03_1','B2_05','B2_07_2','B2_08','B2_09_1','B2_10_2','B2_11','B2_12_1','B2_13_3','B2_14','B2_15_1'),('B3_01_2','B3_02','B3_03_1','B3_05','B3_07_2','B3_07_3','B3_08','B3_09_1','B3_09_2','B3_10_2','B3_10_3','B3_11','B3_12_1','B3_12_2','B3_13'),('B4_01_2','B4_02','B4_03_1','B4_05','B4_07_2','B4_07_3','B4_08','B4_09_1','B4_09_2','B4_10_2','B4_10_3','B4_11','B4_12_1','B4_12_2','B4_13')]
    
    listOfGraphs = ['GRAPH_MMF_UF_FIL','GRAPH_MMF_BW','GRAPH_UF_BW','GRAPH_UF_OXBW','GRAPH_UF_ACBW']

    i=0
    for list in listOfTimers:
        j=0
        for phase in list:
            timer = "TON_" + phase
            AS = phase + "CD"
            APAR = phase + "Dt"
            
            instance = theUnicosProject.findInstanceByName(APAR)
            unit = "s"
            if instance is None:
                thePlugin.writeErrorInUABLog("The Analog Parameter " + APAR + "does not exist in the Spec File")
            else:
                unit = instance.getAttributeData("SCADADeviceGraphics:Unit")
            
            #The counting at the PLC level is handled in seconds. We retrieve the unit specified at the spec to automatically make the conversion. 
            if unit == "h":
                changeTimeUnits = "3600.0"
            elif unit == "min":
                changeTimeUnits = "60.0"
            elif unit == "s":
                changeTimeUnits = "1.0"
            else:
                thePlugin.writeErrorInUABLog('Units of Analog Parameters representing time delays must be "h" for hours, "min" for minuts or "s" for seconds')
            thePlugin.writeTIALogic('''

$timer$.TON( IN := (($listOfGraphs[i]$.$listOfSteps[i][j]$.X)),
                       PT := MAX (IN1:=DINT_TO_TIME( REAL_TO_DINT ($APAR$.PosSt * 1000.0 * $changeTimeUnits$)), IN2:=T#0.001s));
IF NOT $timer$.IN OR $timer$.Q THEN
    $AS$.AuPosR := 0.0;
ELSE
    $AS$.AuPosR := $APAR$.PosSt - ((DINT_TO_REAL (TIME_TO_DINT ($timer$.ET))) / 1000.0 / $changeTimeUnits$);
END_IF;''')       
            j=j+1
        i=i+1

    thePlugin.writeTIALogic(Decorator.decorateExpression('''
    
    IF TON_UF_OXBW_B3_13.Q THEN 
        FDEU_893_UF_OXOPCD.AuPosR := FDEU_893_UF_OXBW_Max;
    END_IF;
    IF TON_UF_ACBW_B4_13.Q THEN 
        FDEU_893_UF_ACOPCD.AuPosR := FDEU_893_UF_ACBW_Max;
    END_IF;
    
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
'''))
