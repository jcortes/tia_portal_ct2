# -*- coding: utf-8 -*-
from java.util import Vector
from java.util import ArrayList
from research.ch.cern.unicos.utilities import SemanticVerifier
import TIALogic_DefaultAlarms_Template
reload(TIALogic_DefaultAlarms_Template)

def GLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    theSemanticVerifier = SemanticVerifier.getUtilityInterface()
    Lparam1,Lparam2,Lparam3,Lparam4,Lparam5,Lparam6,Lparam7,Lparam8,Lparam9,Lparam10=TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)
    Name = name
    thePlugin.writeTIALogic('''
DATA_BLOCK DB_GLOBAL

//
// Block contaning info for power cut
//
    STRUCT
        RealPowerCut         :BOOL;
        RealPowerCut_old    :BOOL;
        RealPowerCut_temp     :BOOL;
        Pulse_Ack        :BOOL;
        T_powerCutOn    :DINT;
        T_powerCutOff    :DINT;
        T_PulseAck    :DINT;
        LostPowerMem : BOOL;
        PrTension: BOOL;
        old_PrTension: BOOL;

    END_STRUCT
BEGIN
END_DATA_BLOCK
''')

# Step 1. Create the FUNCTION called DeviceName_GL.
    thePlugin.writeTIALogic('''
FUNCTION $name$_GL : VOID
TITLE = '$name$_GL'
//
// Global Logic of $name$
(*
 Lparam1:    $Lparam1$
 Lparam2:    $Lparam2$    
 Lparam3:    $Lparam3$    
 Lparam4:    $Lparam4$     //Liste des DI pour detecter coupure de courant separes par des virgules (DI = true = presence tension)
 Lparam5:    $Lparam5$     //liste des actionneurs actifs (Motors) separes par des virgules
 Lparam6:    $Lparam6$    //APAR: Temps de memorisation de la perte de courant sur les DI (sec)
 Lparam7:    $Lparam7$    //APAR: Temps apres lequel on aquitte tout et on redemarre (sec)
 Lparam8:    $Lparam8$
 Lparam9:    $Lparam9$
 Lparam10:   $Lparam10$
*)
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_GL'
FAMILY: 'GL'
''')

    #children to ack/allow restart after power cut
    theChildren= theRawInstances.findMatchingInstances("OnOff,Analog,AnaDO,AnalogDigital,ProcessControlObject", "'#LogicDeviceDefinitions:Master#'!=''")
    
    # Third static variables
    thePlugin.writeTIALogic('''
VAR_TEMP
    old_status : DWORD;
END_VAR
BEGIN
''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')    


#POWER CUT MANAGEMENT GL PART (1st part in IL)
    thePlugin.writeTIALogic('''
//On falling edge of power cut, pulse 1sec the ack and allow restart
    IF DB_GLOBAL.RealPowerCut_old AND NOT DB_GLOBAL.RealPowerCut THEN
        DB_GLOBAL.Pulse_Ack := TRUE;
    END_IF;
    
    IF NOT DB_GLOBAL.Pulse_Ack OR DB_GLOBAL.T_PulseAck > CPC_GLOBAL_VARS.UNICOS_LiveCounter THEN
        DB_GLOBAL.T_PulseAck := CPC_GLOBAL_VARS.UNICOS_LiveCounter;
    END_IF;
    
    IF CPC_GLOBAL_VARS.UNICOS_LiveCounter - DB_GLOBAL.T_PulseAck > 1.0 THEN
        DB_GLOBAL.Pulse_Ack := FALSE;
    END_IF;
    
IF DB_GLOBAL.RealPowerCut_temp AND NOT DB_GLOBAL.Pulse_Ack THEN
    $Name$.AuAlAck := FALSE;
    $Name$.AuRStart := FALSE;''')
    for inst in theChildren:
        ChildName = inst.getAttributeData ("DeviceIdentification:Name")
        thePlugin.writeTIALogic('''
    $ChildName$.AuAlAck := FALSE;
    $ChildName$.AuRStart := FALSE;''')
    
    thePlugin.writeTIALogic('''
    DB_GLOBAL.RealPowerCut_temp:=FALSE;
END_IF;

(*ack + allow restart all PCO + field objects*)
IF DB_GLOBAL.Pulse_Ack THEN
    $Name$.AuAlAck := TRUE;
    $Name$.AuRStart := TRUE;''')
    for inst in theChildren:
        ChildName = inst.getAttributeData ("DeviceIdentification:Name")
        thePlugin.writeTIALogic('''
    $ChildName$.AuAlAck := TRUE;
    $ChildName$.AuRStart := TRUE;''')

    thePlugin.writeTIALogic('''
    DB_GLOBAL.RealPowerCut_temp:=TRUE;
END_IF;

  DB_GLOBAL.RealPowerCut_old:= DB_GLOBAL.RealPowerCut;
''')


    #Recover Copy and CCC alarms
    #In DA:
    # Input = Name of original Alarm for alarm copies
    # Parameter1 = 'copy' or 'CCC'
    # Parameter2 = empty for CCC alarms / Name of the CCC alarms where the alarm copies should be linked.
    # Parameter3 = optional for CCC alarms: Name of the DIC to force the CCC alarm
    # Parameter4 = optional for copies: when input is a field/PCO, links all interlock signals: "FS" or "TS" or "AL" or "FSTS" or "ALL"
    
    theDigitalAlarmsCpy = theRawInstances.findMatchingInstances("DigitalAlarm", "'#LogicDeviceDefinitions:CustomLogicParameters:Parameter1#'='copy'")
    theDigitalAlarmsCCC = theRawInstances.findMatchingInstances("DigitalAlarm", "'#LogicDeviceDefinitions:CustomLogicParameters:Parameter1#'='CCC'")
    
    thePlugin.writeTIALogic('''
(*Copy of alarms linked to CCC alarms*)
''')

    for inst in theDigitalAlarmsCpy:
        DAName = inst.getAttributeData ("DeviceIdentification:Name")
        DAI = inst.getAttributeData ("FEDeviceEnvironmentInputs:Input")
        Delay = inst.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
        DA_Param2 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter2") # CCC alarm to link to
        DA_Param4 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter4").lower().strip()
        db_alarm = thePlugin.s7db_id(DAName)

        # Input conditions
        if DAI == "":
            thePlugin.writeTIALogic('''$db_alarm$$DAName$.I := 0; //to Complete
    $db_alarm$$DAName$.AuAlAck := 0;//to Complete
    $db_alarm$$DAName$.IOError := 0; 
    $db_alarm$$DAName$.IOSimu := 0;
''')
        else:
            #Check if the input is an alarm or a field object
            TheInput = theRawInstances.findMatchingInstances("DigitalAlarm,AnalogAlarm", "'#DeviceIdentification:Name#'='$DAI$'")
            DA_ack = ""
            s7db_id_result=thePlugin.s7db_id(DAI, "DigitalAlarm, AnalogAlarm")
            if TheInput.size() > 0:
                if DA_Param4 in ["","i"]:
                    CopyInput = "$s7db_id_result$$DAI$.ISt"
                else:
                    if "DA" in s7db_id_result:
                        thePlugin.writeErrorInUABLog("Alarm copy: "+DAName+ ". The value in Parameter4: "+DA_Param4+" is invalid for DigitalAlarm, please delete.")
                    elif "AA" in s7db_id_result:
                        # allow sending individual thresholds to CCC alarms, did not do fancy processing, only allow certain combinations from:
                        # i, w, hh, h, l, ll 
                        # i.e. hh,h is allows, but not just h, because in that case it would be inconsistent (imho)
                        if DA_Param4 in ["","ll,hh","hh,ll"]:
                            CopyInput = "$s7db_id_result$$DAI$.ISt"
                        elif DA_Param4 in ["i,w","w,i","hh,h,l,ll","ll,l,h,hh"]:
                            CopyInput = "$s7db_id_result$$DAI$.ISt OR $s7db_id_result$$DAI$.WSt"
                        elif DA_Param4 in ["hh,h","h,hh"]:
                            CopyInput = "$s7db_id_result$$DAI$.HHAlSt OR $s7db_id_result$$DAI$.HWSt"
                        elif DA_Param4 in ["ll,l","l,ll"]:
                            CopyInput = "$s7db_id_result$$DAI$.LLAlSt OR $s7db_id_result$$DAI$.LWSt"
                        else:
                            thePlugin.writeErrorInUABLog("Alarm copy: "+DAName+ ". The value in Parameter4: "+DA_Param4+" is invalid for AnalogAlarm, please correct and re-run."+'''
Available options are empty (default) to send ISt, i.e. HH/LL, "I,W" (or "HH,H,L,LL") - send all thresholds, "HH,H", "LL,L". Other combinations are invalid. ''')
                        
                
                #Check if the process alarm is auto ack or not
                for inst in TheInput:
                    DA_ack = inst.getAttributeData("FEDeviceAlarm:Auto Acknowledge").strip().lower()
            elif DA_Param4 == "fs":
                CopyInput = "NOT $DAI$.EnRStartSt "
            elif DA_Param4 == "ts":
                CopyInput = "$DAI$.TStopISt"
            elif DA_Param4 == "al":
                CopyInput = "$DAI$.ALSt"
            elif DA_Param4 == "fsts":
                CopyInput = "NOT $DAI$.EnRStartSt OR $DAI$.TStopISt"
            elif DA_Param4 == "fstsal":
                CopyInput = "NOT $DAI$.EnRStartSt OR $DAI$.TStopISt OR $DAI$.ALSt"
            elif DA_Param4 == "all":
                CopyInput = "NOT $DAI$.EnRStartSt OR $DAI$.TStopISt OR $DAI$.StartISt OR $DAI$.ALSt"
            else:
                CopyInput = "NOT $DAI$.EnRStartSt OR $DAI$.TStopISt"

            if DA_ack <> "true":
                thePlugin.writeTIALogic('''    $db_alarm$$DAName$.I := $CopyInput$ ;
    $db_alarm$$DAName$.AuAlAck := NOT '''+s7db_id_result+DAI+'''.AlUnAck;
    $db_alarm$$DAName$.IOError := '''+s7db_id_result+DAI+'''.IOErrorW;
    $db_alarm$$DAName$.IOSimu := '''+s7db_id_result+DAI+'''.IOSimuW ;
''')   
            else:
                thePlugin.writeTIALogic('''    $db_alarm$$DAName$.I := $CopyInput$ ;
    $db_alarm$$DAName$.AuAlAck := NOT '''+s7db_id_result+DAI+'''.ISt;
    $db_alarm$$DAName$.IOError := '''+s7db_id_result+DAI+'''.IOErrorW;
    $db_alarm$$DAName$.IOSimu := '''+s7db_id_result+DAI+'''.IOSimuW ;
''')   
            
            
        # Delay Alarm conditions
        if Delay.strip() == "":
            text = 'do nothing'
        elif thePlugin.isString(Delay) and Delay.strip().lower() <> "logic":
            s7db_id_result=thePlugin.s7db_id(Delay, "AnalogParameter, AnalogStatus")
            thePlugin.writeTIALogic('''    $db_alarm$$DAName$.PAlDt := REAL_TO_INT('''+s7db_id_result+Delay+'''.PosSt);
''')
        elif Delay.strip().lower() == "logic":
            thePlugin.writeTIALogic('''    $db_alarm$$DAName$.PAlDt := 0; // To complete 
''')
        else:
            text = 'do nothing'
            

        # Check if CCC alarm exists, if not write Error 
        if theSemanticVerifier.doesObjectExist(DA_Param2, theRawInstances) is not True:
            thePlugin.writeErrorInUABLog("Alarm copy: "+DAName+ ". The linked CCC alarm : "+DA_Param2+" in LogicParam2 doesn't exist")


    #CCC Alarms
    thePlugin.writeTIALogic('''
(*CCC alarms*)
''')

    for inst in theDigitalAlarmsCCC:
        DAName = inst.getAttributeData ("DeviceIdentification:Name")
        DADesc = inst.getAttributeData ("DeviceDocumentation:Description")
        Delay = inst.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
        DAI = inst.getAttributeData ("FEDeviceEnvironmentInputs:Input")
        db_CCCalarmDAI = thePlugin.s7db_id(DAI)
        Param3 = inst.getAttributeData ("LogicDeviceDefinitions:CustomLogicParameters:Parameter3") # DI to force CCC alarm
        db_CCCalarm = thePlugin.s7db_id(DAName)
        if Param3 <> "":
            if theSemanticVerifier.doesObjectExist(Param3, theRawInstances) is not True:
                thePlugin.writeErrorInUABLog("Alarm CCC: "+DAName+ ". The DI to force CCC : "+Param3+" in LogicParam3 doesn't exist")
            DIC_SET = "$Param3$.PosSt OR "
            DIC_RESET = "NOT $Param3$.PosSt AND "
        else:
            DIC_SET = ""
            DIC_RESET = ""
        
        # CCC alarm Input conditions
        if DAI == "":
            thePlugin.writeTIALogic('''
            
(*CCC alarm: $DADesc$*)
IF ''' )
            for inst in theDigitalAlarmsCpy:
                DACpyName = inst.getAttributeData ("DeviceIdentification:Name")
                DACpyParam2 = inst.getAttributeData ("LogicDeviceDefinitions:CustomLogicParameters:Parameter2")
                db_alarmCpy = thePlugin.s7db_id(DACpyName)
                if DACpyParam2 == DAName:
                    thePlugin.writeTIALogic('''
    $db_alarmCpy$$DACpyName$.ISt OR ''')

            thePlugin.writeTIALogic('''$DIC_SET$ 0 THEN
        $db_CCCalarm$$DAName$.I := TRUE;    //SET CCC ALARM
ELSIF ''')
            for inst in theDigitalAlarmsCpy:
                DACpyName = inst.getAttributeData ("DeviceIdentification:Name")
                DACpyParam2 = inst.getAttributeData ("LogicDeviceDefinitions:CustomLogicParameters:Parameter2")
                db_alarmCpy = thePlugin.s7db_id(DACpyName)
                if DACpyParam2 == DAName:
                    thePlugin.writeTIALogic('''
    NOT $db_alarmCpy$$DACpyName$.AlUnAck AND ''')
            thePlugin.writeTIALogic('''$DIC_RESET$ 1 THEN
        $db_CCCalarm$$DAName$.I := FALSE;    //RESET CCC ALARM
END_IF;''')
        
    
        #Ack CCC alarm when all copies acknowledged
            thePlugin.writeTIALogic('''
            
    $db_CCCalarm$$DAName$.AuAlAck := ''' )
            for inst in theDigitalAlarmsCpy:
                DACpyName = inst.getAttributeData ("DeviceIdentification:Name")
                DACpyParam2 = inst.getAttributeData ("LogicDeviceDefinitions:CustomLogicParameters:Parameter2")
                db_alarmCpy = thePlugin.s7db_id(DACpyName)
                if DACpyParam2 == DAName:
                    thePlugin.writeTIALogic('''
    NOT $db_alarmCpy$$DACpyName$.AlUnAck AND ''')
            thePlugin.writeTIALogic('''1; ''')
        else:
            thePlugin.writeTIALogic('''
    $db_CCCalarm$$DAName$.I := $db_CCCalarmDAI$$DAI$.ISt;''' )
        
        # Delay Alarm conditions
        if Delay.strip() == "":
            text = 'do nothing'
        elif thePlugin.isString(Delay) and Delay.strip().lower() <> "logic":
            s7db_id_result=thePlugin.s7db_id(Delay, "AnalogParameter, AnalogStatus")
            thePlugin.writeTIALogic('''    $db_CCCalarm$$DAName$.PAlDt := REAL_TO_INT('''+s7db_id_result+Delay+'''.PosSt);
''')
        elif Delay.strip().lower() == "logic":
            thePlugin.writeTIALogic('''    $db_CCCalarm$$DAName$.PAlDt := 0; // To complete 
''')
        else:
            text = 'do nothing'



    # Step 1.4: IOError IOSimu
    thePlugin.writeTIALogic('''

// Errors and Simulated data
DB_ERROR_SIMU.$name$_GL_E := 0; // To complete
DB_ERROR_SIMU.$name$_GL_S := 0; // To complete

// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')    