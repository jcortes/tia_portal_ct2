# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)


def GLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)
    
    theUnicosProject = thePlugin.getUnicosProject()        
    Decorator = ucpc_library.shared_decorator.ExpressionDecorator()
    
# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeTIALogic('''
FUNCTION $name$_GL : VOID
TITLE = '$name$_GL'
//
// Global Logic of $name$
//
(*
 Lparam1: $Lparam1$
 Lparam2: $Lparam2$
 Lparam3: $Lparam3$
 Lparam4: $Lparam4$
 Lparam5: $Lparam5$
 Lparam6: $Lparam6$
 Lparam7: $Lparam7$
 Lparam8: $Lparam8$
 Lparam9: $Lparam9$
 Lparam10:  $Lparam10$
*)
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_GL'
FAMILY: 'GL'
BEGIN
''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    thePlugin.writeTIALogic(Decorator.decorateExpression('''
    
(* COMPUTED VARIABLES. CHAPTER 5.7 *)

(* Treatment of DHT2301 sensor 
PCO location    Transmitted value   Precision   Description
GAC_IX  5 mA    5%  Measurement is in Pause mode (Stand-by)
    8 mA    5%  Good measurement for opeartion
    11 mA   5%  Bad measurement for operation
    14 mA   5%  Shortage of water to measurement device
    17 mA   5%  Shortage of reagent (chemical used in Testomat for analysis) - Almost empty
    20 mA   5%  Shortage of reagent (chemical used in Testomat for analysis) - Fully empty
*)

(* WORD STATUS FOR STATUS *)
IF NOT FDEU_893_DHT2301.IOErrorW THEN
    
    // PAUSE
    IF ((FDEU_893_DHT2301_PAUSE - FDEU_893_DHT2301_HYS) < FDEU_893_DHT2301) AND (FDEU_893_DHT2301 < (FDEU_893_DHT2301_PAUSE + FDEU_893_DHT2301_HYS)) THEN
        FDEU_893_DHT2301St.AuPosR := INT_TO_WORD(1);

    // GOOD
    ELSIF ((FDEU_893_DHT2301_GOOD - FDEU_893_DHT2301_HYS) < FDEU_893_DHT2301) AND (FDEU_893_DHT2301 < (FDEU_893_DHT2301_GOOD + FDEU_893_DHT2301_HYS)) THEN
        FDEU_893_DHT2301St.AuPosR := INT_TO_WORD(2);

    // BAD
    ELSIF ((FDEU_893_DHT2301_BAD - FDEU_893_DHT2301_HYS) < FDEU_893_DHT2301) AND (FDEU_893_DHT2301 < (FDEU_893_DHT2301_BAD + FDEU_893_DHT2301_HYS)) THEN
        FDEU_893_DHT2301St.AuPosR := INT_TO_WORD(3);

    // SHORT
    ELSIF ((FDEU_893_DHT2301_SHORT - FDEU_893_DHT2301_HYS) < FDEU_893_DHT2301) AND (FDEU_893_DHT2301 < (FDEU_893_DHT2301_SHORT + FDEU_893_DHT2301_HYS)) THEN
        FDEU_893_DHT2301St.AuPosR := INT_TO_WORD(4);

    // ALMOST
    ELSIF ((FDEU_893_DHT2301_ALMOST - FDEU_893_DHT2301_HYS) < FDEU_893_DHT2301) AND (FDEU_893_DHT2301 < (FDEU_893_DHT2301_ALMOST + FDEU_893_DHT2301_HYS)) THEN
        FDEU_893_DHT2301St.AuPosR := INT_TO_WORD(5);

    // EMPTY
    ELSIF ((FDEU_893_DHT2301_EMPTY - FDEU_893_DHT2301_HYS) < FDEU_893_DHT2301) AND (FDEU_893_DHT2301 < (FDEU_893_DHT2301_EMPTY + FDEU_893_DHT2301_HYS)) THEN
        FDEU_893_DHT2301St.AuPosR := INT_TO_WORD(6);

    // UNDEFINED
    ELSE
        FDEU_893_DHT2301St.AuPosR := INT_TO_WORD(0);

    END_IF;

// IOERROR
ELSE
    FDEU_893_DHT2301St.AuPosR := INT_TO_WORD(7);

END_IF;

FDEU_893_GAC_IX_StartCon.HfPos := (FDEU_893_LT1401 > FDEU_893_LT1401_GAC_ON AND FDEU_893_LT2301 < FDEU_893_LT2301_GAC_OFF ) OR
                                 (FDEU_893_LT1401 > FDEU_893_LT1401_GAC_ON AND (FDEU_893_LT2301 < FDEU_893_LT2301_GAC_ON OR 
                                 NOT FDEU_893_SFT2001_NOREGEN)); // NOTE: 0=regeneration
                                 
FDEU_893GAC_IX_StopCon.HfPos := (FDEU_893_LT1401 < FDEU_893_LT1401_GAC_OFF) OR FDEU_893_FT1501_AL3 OR 
                                 ( ( FDEU_893_LT2301 > FDEU_893_LT2301_GAC_OFF) AND FDEU_893_SFT2001_NOREGEN ); // 1=NO regeneration

TON_GAC_IX_DP1901.TON ( IN:= (FDEU_893_DP1901 > FDEU_893_DP1901_Thres_H),
                        PT := MAX (IN1:=DINT_TO_TIME( REAL_TO_DINT (FDEU_893_DP1901_HDt)), IN2:=T#0.001s));

IF (CPC_GLOBAL_VARS.UNICOS_LiveCounter MOD 60) = 0 AND NOT DB_GLOBAL.GAC_IX_Minute_Pass THEN
    IF GRAPH_GAC_IX.S2_02.X THEN
        IF FDEU_893_GX_BW_OPCD > 1.0 THEN
            FDEU_893_GX_BW_OPCD.AuPosR := FDEU_893_GX_BW_OPCD - 1.0;
        END_IF;
    END_IF;
    IF FDEU_893_GX_BWCD > 0 THEN
        FDEU_893_GX_BWCD.AuPosR := FDEU_893_GX_BWCD - 1.0;
    END_IF;
END_IF;
DB_GLOBAL.GAC_IX_Minute_Pass := CPC_GLOBAL_VARS.UNICOS_LiveCounter MOD 60 = 0;

(* Re-initialize the counters *)
IF GRAPH_GAC_IX.B6_06_3.X THEN
    FDEU_893_GX_BW_OPCD.AuPosR := FDEU_893_GX_BW_Max;
    FDEU_893_GX_BWCD.AuPosR := FDEU_893_GX_BW_Min;
END_IF;    

FDEU_893_GAC_IX_BWCon.HfPos := FDEU_893_SFT2001_NOREGEN AND (TON_GAC_IX_DP1901.Q OR FDEU_893_GX_BWCD.AuPosR = 0 OR FDEU_893_GX_BW_OPCD.AuPosR = 0);

FDEU_893_GAC_IX_BWEn.HfPos := (NOT (GRAPH_MMF_UF.MMF_BW.X OR GRAPH_MMF_UF.UF_BW.X OR GRAPH_MMF_UF.UF_OX_BW.X OR GRAPH_MMF_UF.UF_AC_BW.X)) AND FDEU_893_SFT2001_NOREGEN AND FDEU_893_P5101.EnRstartSt;

    (*      DOSING PUMPS CALCULATIONS *)
    
    FDEU_893_SBS7401_MASS.HFPos := FDEU_893_FT1501 * FDEU_893_SBS7401_Dos;
    IF (FDEU_893_SBS7401_Den * FDEU_893_SBS7401_Con) > 0.0000000001 THEN
        FDEU_893_SBS7401_VOL.HFPos := FDEU_893_SBS7401_MASS / (FDEU_893_SBS7401_Con / 100.0 * FDEU_893_SBS7401_Den) * 1000.0;
    END_IF;

    FDEU_893_DP7401_VOLPUL.HFPos := FDEU_893_SBS7401_Min + ( FDEU_893_SBS7401_Max - FDEU_893_SBS7401_Min ) * FDEU_893_SBS7401_Per /100.0;
    IF FDEU_893_DP7401_VOLPUL > 0.0000000001 THEN 
        FDEU_893_DP7401_FREQ.HFPos := FDEU_893_SBS7401_VOL /(3600 * FDEU_893_DP7401_VOLPUL);
    END_IF;
    
DB_GLOBAL.GAC_IX_P5101_Req := GRAPH_GAC_IX.B5_01_3.X OR GRAPH_GAC_IX.B5_02.X OR GRAPH_GAC_IX.B6_01_3.X OR GRAPH_GAC_IX.B6_02.X;
'''))

    thePlugin.writeTIALogic('''
    
    
    
    
(**********************************************
*******                                 *******
******* LIST OF TIMERS FOR THE DB_GLOBAL  *******
*******                                 *******    
***********************************************)
''')
    listOfTimers = ['GAC_IX_S2_01_2','GAC_IX_S2_03_1','GAC_IX_B5_01_3','GAC_IX_B5_02','GAC_IX_B5_03_1','GAC_IX_B5_04_2','GAC_IX_B5_05','GAC_IX_B5_06_1','GAC_IX_B6_01_3','GAC_IX_B6_02','GAC_IX_B6_03_1','GAC_IX_B6_04_2','GAC_IX_B6_05','GAC_IX_B6_06_1']
    listOfSteps = ['S2_01_2','S2_03_1','B5_01_3','B5_02','B5_03_1','B5_04_2','B5_05','B5_06_1','B6_01_3','B6_02','B6_03_1','B6_04_2','B6_05','B6_06_1']
    i=0
    for step in listOfTimers:
        timer = "TON_" + step
        AS = step + "CD"
        APAR = step + "Dt"
        
        instance = theUnicosProject.findInstanceByName(APAR)
        unit = "s" 
        if instance is None:
            thePlugin.writeErrorInUABLog("The Analog Parameter " + APAR + "does not exist in the Spec File")
        else:
            unit = instance.getAttributeData("SCADADeviceGraphics:Unit")
        #The counting at the PLC level is handled in seconds. We retrieve the unit specified at the spec to automatically make the conversion. 
        if unit == "h":
            changeTimeUnits = "3600.0"
        elif unit == "min":
            changeTimeUnits = "60.0"
        elif unit == "s":
            changeTimeUnits = "1.0"
        else:
            thePlugin.writeErrorInUABLog('Units of Analog Parameters representing time delays must be "h" for hours, "min" for minuts or "s" for seconds')
       
        thePlugin.writeTIALogic(Decorator.decorateExpression('''
$timer$.TON( IN := ((GRAPH_GAC_IX.$listOfSteps[i]$.X)),
                       PT := MAX (IN1:=DINT_TO_TIME( REAL_TO_DINT ($APAR$ * 1000.0 * $changeTimeUnits$)), IN2:=T#0.001s));
IF NOT $timer$.IN OR $timer$.Q THEN
    $AS$.AuPosR := 0.0;
ELSE
    $AS$.AuPosR := $APAR$ - ((DINT_TO_REAL (TIME_TO_DINT ($timer$.ET))) / 1000.0 / $changeTimeUnits$);
END_IF;
'''))
        i=i+1






    # Step 1.4: IOError IOSimu
    thePlugin.writeTIALogic('''

// Errors and Simulated data
DB_ERROR_SIMU.$name$_GL_E := 0; // To complete
DB_ERROR_SIMU.$name$_GL_S := 0; // To complete
''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
