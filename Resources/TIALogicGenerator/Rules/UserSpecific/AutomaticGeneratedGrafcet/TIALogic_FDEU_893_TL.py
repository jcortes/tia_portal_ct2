# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)


def TLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    Decorator = ucpc_library.shared_decorator.ExpressionDecorator()
    # Automatically generated variables
    

# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeTIALogic('''
FUNCTION $name$_TL : VOID
TITLE = '$name$_TL'
//
// Transition Logic of $name$
//
// Lparam1:    $Lparam1$
// Lparam2:    $Lparam2$
// Lparam3:    $Lparam3$
// Lparam4:    $Lparam4$
// Lparam5:    $Lparam5$
// Lparam6:    $Lparam6$
// Lparam7:    $Lparam7$
// Lparam8:    $Lparam8$
// Lparam9:    $Lparam9$
// Lparam10:    $Lparam10$
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_TL'
FAMILY: 'TL'
VAR_TEMP
    pco_transitions0 : WORD;


END_VAR
BEGIN

''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    thePlugin.writeTIALogic('''

DB_ERROR_SIMU.$name$_TL_E := 0 ; // To complete
DB_ERROR_SIMU.$name$_TL_S := 0 ; // To complete

''')

    thePlugin.writeTIALogic('''(* Automatic generated code from DB_GLOBALSpecs.xlsx using the TIA_Expert_DB_GLOBAL_Template <begin> *)
''')

    thePlugin.writeTIALogic('''(* VARIABLES COMPUTATION *)



''')

    thePlugin.writeTIALogic(Decorator.decorateExpression('''(* TRANSITIONS COMPUTATION *)

(* 1 TO 2 *) GRAPH_FDEU_893.SHUTDOWN_STOP := NOT FDEU_893.FuStopISt AND FDEU_893.EnRStartSt;
(* 2 TO 1 *) GRAPH_FDEU_893.STOP_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 2 TO 3 *) GRAPH_FDEU_893.STOP_RUN := $name$.RunOSt;
(* 3 TO 1 *) GRAPH_FDEU_893.RUN_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 3 TO 2 *) GRAPH_FDEU_893.RUN_STOP := $name$.TStopISt OR NOT $name$.RunOSt;
'''))

    thePlugin.writeTIALogic('''
(* WORD STATUS FOR STEPS *)
FDEU_893_St.AuPosR := INT_TO_WORD(GRAPH_FDEU_893.S_NO);
''')

    thePlugin.writeTIALogic('''
(* WORD STATUS FOR TRANSITIONS *)
pco_transitions0.%X0  := GRAPH_FDEU_893.SHUTDOWN_STOP;
pco_transitions0.%X1  := GRAPH_FDEU_893.STOP_SHUTDOWN;
pco_transitions0.%X2  := GRAPH_FDEU_893.STOP_RUN;
pco_transitions0.%X3  := GRAPH_FDEU_893.RUN_SHUTDOWN;
pco_transitions0.%X4  := GRAPH_FDEU_893.RUN_STOP;
pco_transitions0.%X5  := false;
pco_transitions0.%X6  := false;
pco_transitions0.%X7  := false;
pco_transitions0.%X8  := false;
pco_transitions0.%X9  := false;
pco_transitions0.%X10 := false;
pco_transitions0.%X11 := false;
pco_transitions0.%X12 := false;
pco_transitions0.%X13 := false;
pco_transitions0.%X14 := false;
pco_transitions0.%X15 := false;


FDEU_893_Tr0.AuPosR := pco_transitions0;

''')

    thePlugin.writeTIALogic('''
(* Automatic generated code from DB_GLOBALSpecs.xlsx using the TIA_Expert_DB_GLOBAL_Template <end> *)''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
