# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)


def TLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    Decorator = ucpc_library.shared_decorator.ExpressionDecorator()
    # Automatically generated variables
    

# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeTIALogic('''
FUNCTION $name$_TL : VOID
TITLE = '$name$_TL'
//
// Transition Logic of $name$
//
// Lparam1:    $Lparam1$
// Lparam2:    $Lparam2$
// Lparam3:    $Lparam3$
// Lparam4:    $Lparam4$
// Lparam5:    $Lparam5$
// Lparam6:    $Lparam6$
// Lparam7:    $Lparam7$
// Lparam8:    $Lparam8$
// Lparam9:    $Lparam9$
// Lparam10:    $Lparam10$
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_TL'
FAMILY: 'TL'
VAR_TEMP
    pco_transitions0 : WORD;
    pco_transitions1 : WORD;
    pco_transitions2 : WORD;
    pco_transitions3 : WORD;


END_VAR
BEGIN

''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    thePlugin.writeTIALogic('''

DB_ERROR_SIMU.$name$_TL_E := 0 ; // To complete
DB_ERROR_SIMU.$name$_TL_S := 0 ; // To complete

''')

    thePlugin.writeTIALogic('''(* Automatic generated code from DB_GLOBALSpecs.xlsx using the TIA_Expert_DB_GLOBAL_Template <begin> *)
''')

    thePlugin.writeTIALogic('''(* VARIABLES COMPUTATION *)



''')

    thePlugin.writeTIALogic(Decorator.decorateExpression('''(* TRANSITIONS COMPUTATION *)

(* 1 TO 2 *) GRAPH_UF_BW.INIT_B2_01_1 := GRAPH_MMF_UF.UF_BW.X;
(* 2 TO 1 *) GRAPH_UF_BW.B2_01_1_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 2 TO 3 *) GRAPH_UF_BW.B2_01_1_B2_01_2 := FDEU_893_AQE1305.OnSt AND FDEU_893_AQE5301.OnSt;
(* 3 TO 1 *) GRAPH_UF_BW.B2_01_2_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 3 TO 4 *) GRAPH_UF_BW.B2_01_2_B2_02 := FDEU_893_MFV5301.OnSt AND TON_UF_BW_B2_01_2.Q;
(* 4 TO 1 *) GRAPH_UF_BW.B2_02_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 4 TO 5 *) GRAPH_UF_BW.B2_02_B2_03_1 := TON_UF_BW_B2_02_AIR.Q;
(* 5 TO 1 *) GRAPH_UF_BW.B2_03_1_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 5 TO 6 *) GRAPH_UF_BW.B2_03_1_B2_03_2 := NOT FDEU_893_MFV5301.OnSt AND TON_UF_BW_B2_03_1.Q;
(* 6 TO 1 *) GRAPH_UF_BW.B2_03_2_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 6 TO 7 *) GRAPH_UF_BW.B2_03_2_B2_04 := FDEU_893_AQE5301.OffSt;
(* 7 TO 1 *) GRAPH_UF_BW.B2_04_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 7 TO 8 *) GRAPH_UF_BW.B2_04_B2_05 := FDEU_893_AQE1304.OnSt ;
(* 8 TO 1 *) GRAPH_UF_BW.B2_05_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 8 TO 9 *) GRAPH_UF_BW.B2_05_B2_06 := TON_UF_BW_B2_05_PUR.Q;
(* 9 TO 1 *) GRAPH_UF_BW.B2_06_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 9 TO 10 *) GRAPH_UF_BW.B2_06_B2_07_1 := FDEU_893_AQE1304.OffSt ;
(* 10 TO 1 *) GRAPH_UF_BW.B2_07_1_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 10 TO 11 *) GRAPH_UF_BW.B2_07_1_B2_07_2 := FDEU_893_AQE1303.OnSt ;
(* 11 TO 1 *) GRAPH_UF_BW.B2_07_2_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 11 TO 12 *) GRAPH_UF_BW.B2_07_2_B2_08 := FDEU_893_P5201.OnSt AND TON_UF_BW_B2_07_2.Q;
(* 12 TO 1 *) GRAPH_UF_BW.B2_08_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 12 TO 13 *) GRAPH_UF_BW.B2_08_B2_09_1 := TON_UF_BW_B2_08_TOP.Q;
(* 13 TO 1 *) GRAPH_UF_BW.B2_09_1_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 13 TO 14 *) GRAPH_UF_BW.B2_09_1_B2_09_2 := NOT FDEU_893_P5201.OnSt AND TON_UF_BW_B2_09_1.Q;
(* 14 TO 1 *) GRAPH_UF_BW.B2_09_2_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 14 TO 15 *) GRAPH_UF_BW.B2_09_2_B2_10_1 := FDEU_893_AQE1305.OffSt;
(* 15 TO 1 *) GRAPH_UF_BW.B2_10_1_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 15 TO 16 *) GRAPH_UF_BW.B2_10_1_B2_10_2 := FDEU_893_AQE1304.OnSt ;
(* 16 TO 1 *) GRAPH_UF_BW.B2_10_2_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 16 TO 17 *) GRAPH_UF_BW.B2_10_2_B2_11 := FDEU_893_P5201.OnSt AND TON_UF_BW_B2_10_2.Q;
(* 17 TO 1 *) GRAPH_UF_BW.B2_11_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 17 TO 18 *) GRAPH_UF_BW.B2_11_B2_12_1 := TON_UF_BW_B2_11_BOT.Q;
(* 18 TO 1 *) GRAPH_UF_BW.B2_12_1_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 18 TO 19 *) GRAPH_UF_BW.B2_12_1_B2_12_2 := NOT FDEU_893_P5201.OnSt AND TON_UF_BW_B2_12_1.Q;
(* 19 TO 1 *) GRAPH_UF_BW.B2_12_2_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 19 TO 20 *) GRAPH_UF_BW.B2_12_2_B2_13_1 := FDEU_893_AQE1303.OffSt AND FDEU_893_AQE1304.OffSt;
(* 20 TO 1 *) GRAPH_UF_BW.B2_13_1_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 20 TO 21 *) GRAPH_UF_BW.B2_13_1_B2_13_2 := FDEU_893_AQE1001.OnSt AND FDEU_893_AQE1201.OnSt AND FDEU_893_AQE1211.OnSt AND FDEU_893_AQE1202.OnSt AND FDEU_893_AQE1212.OnSt;
(* 21 TO 1 *) GRAPH_UF_BW.B2_13_2_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 21 TO 22 *) GRAPH_UF_BW.B2_13_2_B2_13_3 := FDEU_893_AQE1301.OnSt AND FDEU_893_AQE1305.OnSt ;
(* 22 TO 1 *) GRAPH_UF_BW.B2_13_3_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 22 TO 23 *) GRAPH_UF_BW.B2_13_3_B2_14 := FDEU_893_P1101.OnSt AND TON_UF_BW_B2_13_3.Q;
(* 23 TO 1 *) GRAPH_UF_BW.B2_14_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 23 TO 24 *) GRAPH_UF_BW.B2_14_B2_15_1 := TON_UF_BW_B2_14_FWD.Q;
(* 24 TO 1 *) GRAPH_UF_BW.B2_15_1_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 24 TO 25 *) GRAPH_UF_BW.B2_15_1_B2_15_2 := NOT FDEU_893_P1101.OnSt AND TON_UF_BW_B2_15_1.Q;
(* 25 TO 1 *) GRAPH_UF_BW.B2_15_2_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 25 TO 26 *) GRAPH_UF_BW.B2_15_2_B2_15_3 := FDEU_893_AQE1301.OffSt AND FDEU_893_AQE1305.OffSt ;
(* 26 TO 1 *) GRAPH_UF_BW.B2_15_3_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
(* 26 TO 27 *) GRAPH_UF_BW.B2_15_3_SYNC := (FDEU_893_AQE1001.OffSt OR FDEU_893_AQE1001.AuOnRSt) AND FDEU_893_AQE1201.OffSt AND FDEU_893_AQE1211.OffSt AND FDEU_893_AQE1202.OffSt AND FDEU_893_AQE1212.OffSt;
(* 27 TO 1 *) GRAPH_UF_BW.SYNC_INIT := NOT GRAPH_MMF_UF.UF_BW.X;
'''))

    thePlugin.writeTIALogic('''
(* WORD STATUS FOR STEPS *)
FDEU_893_UF_BW_St.AuPosR := INT_TO_WORD(GRAPH_UF_BW.S_NO);
''')

    thePlugin.writeTIALogic('''
(* WORD STATUS FOR TRANSITIONS *)
pco_transitions0.%X0  := GRAPH_UF_BW.INIT_B2_01_1;
pco_transitions0.%X1  := GRAPH_UF_BW.B2_01_1_INIT;
pco_transitions0.%X2  := GRAPH_UF_BW.B2_01_1_B2_01_2;
pco_transitions0.%X3  := GRAPH_UF_BW.B2_01_2_INIT;
pco_transitions0.%X4  := GRAPH_UF_BW.B2_01_2_B2_02;
pco_transitions0.%X5  := GRAPH_UF_BW.B2_02_INIT;
pco_transitions0.%X6  := GRAPH_UF_BW.B2_02_B2_03_1;
pco_transitions0.%X7  := GRAPH_UF_BW.B2_03_1_INIT;
pco_transitions0.%X8  := GRAPH_UF_BW.B2_03_1_B2_03_2;
pco_transitions0.%X9  := GRAPH_UF_BW.B2_03_2_INIT;
pco_transitions0.%X10 := GRAPH_UF_BW.B2_03_2_B2_04;
pco_transitions0.%X11 := GRAPH_UF_BW.B2_04_INIT;
pco_transitions0.%X12 := GRAPH_UF_BW.B2_04_B2_05;
pco_transitions0.%X13 := GRAPH_UF_BW.B2_05_INIT;
pco_transitions0.%X14 := GRAPH_UF_BW.B2_05_B2_06;
pco_transitions0.%X15 := GRAPH_UF_BW.B2_06_INIT;
pco_transitions1.%X0  := GRAPH_UF_BW.B2_06_B2_07_1;
pco_transitions1.%X1  := GRAPH_UF_BW.B2_07_1_INIT;
pco_transitions1.%X2  := GRAPH_UF_BW.B2_07_1_B2_07_2;
pco_transitions1.%X3  := GRAPH_UF_BW.B2_07_2_INIT;
pco_transitions1.%X4  := GRAPH_UF_BW.B2_07_2_B2_08;
pco_transitions1.%X5  := GRAPH_UF_BW.B2_08_INIT;
pco_transitions1.%X6  := GRAPH_UF_BW.B2_08_B2_09_1;
pco_transitions1.%X7  := GRAPH_UF_BW.B2_09_1_INIT;
pco_transitions1.%X8  := GRAPH_UF_BW.B2_09_1_B2_09_2;
pco_transitions1.%X9  := GRAPH_UF_BW.B2_09_2_INIT;
pco_transitions1.%X10 := GRAPH_UF_BW.B2_09_2_B2_10_1;
pco_transitions1.%X11 := GRAPH_UF_BW.B2_10_1_INIT;
pco_transitions1.%X12 := GRAPH_UF_BW.B2_10_1_B2_10_2;
pco_transitions1.%X13 := GRAPH_UF_BW.B2_10_2_INIT;
pco_transitions1.%X14 := GRAPH_UF_BW.B2_10_2_B2_11;
pco_transitions1.%X15 := GRAPH_UF_BW.B2_11_INIT;
pco_transitions2.%X0  := GRAPH_UF_BW.B2_11_B2_12_1;
pco_transitions2.%X1  := GRAPH_UF_BW.B2_12_1_INIT;
pco_transitions2.%X2  := GRAPH_UF_BW.B2_12_1_B2_12_2;
pco_transitions2.%X3  := GRAPH_UF_BW.B2_12_2_INIT;
pco_transitions2.%X4  := GRAPH_UF_BW.B2_12_2_B2_13_1;
pco_transitions2.%X5  := GRAPH_UF_BW.B2_13_1_INIT;
pco_transitions2.%X6  := GRAPH_UF_BW.B2_13_1_B2_13_2;
pco_transitions2.%X7  := GRAPH_UF_BW.B2_13_2_INIT;
pco_transitions2.%X8  := GRAPH_UF_BW.B2_13_2_B2_13_3;
pco_transitions2.%X9  := GRAPH_UF_BW.B2_13_3_INIT;
pco_transitions2.%X10 := GRAPH_UF_BW.B2_13_3_B2_14;
pco_transitions2.%X11 := GRAPH_UF_BW.B2_14_INIT;
pco_transitions2.%X12 := GRAPH_UF_BW.B2_14_B2_15_1;
pco_transitions2.%X13 := GRAPH_UF_BW.B2_15_1_INIT;
pco_transitions2.%X14 := GRAPH_UF_BW.B2_15_1_B2_15_2;
pco_transitions2.%X15 := GRAPH_UF_BW.B2_15_2_INIT;
pco_transitions3.%X0  := GRAPH_UF_BW.B2_15_2_B2_15_3;
pco_transitions3.%X1  := GRAPH_UF_BW.B2_15_3_INIT;
pco_transitions3.%X2  := GRAPH_UF_BW.B2_15_3_SYNC;
pco_transitions3.%X3  := GRAPH_UF_BW.SYNC_INIT;
pco_transitions3.%X4  := false;
pco_transitions3.%X5  := false;
pco_transitions3.%X6  := false;
pco_transitions3.%X7  := false;
pco_transitions3.%X8  := false;
pco_transitions3.%X9  := false;
pco_transitions3.%X10 := false;
pco_transitions3.%X11 := false;
pco_transitions3.%X12 := false;
pco_transitions3.%X13 := false;
pco_transitions3.%X14 := false;
pco_transitions3.%X15 := false;


FDEU_893_UF_BW_Tr0.AuPosR := pco_transitions0;
FDEU_893_UF_BW_Tr1.AuPosR := pco_transitions1;
FDEU_893_UF_BW_Tr2.AuPosR := pco_transitions2;
FDEU_893_UF_BW_Tr3.AuPosR := pco_transitions3;

''')

    thePlugin.writeTIALogic('''
(*1=1- INIT,2=2- B2_01_1 - Open / Wait Air Scour Valves,3=3- B2_01_2 - Start Air Scour Fan,4=4- B2_02 (Air Scour UF BW) - Operation Air Scour,5=5- B2_03_1 - Stop Air Scour Fan,6=6- B2_03_2 - Close / Wait Air Scour Valves,7=7- B2_04 - Open / Wait Purge Valves,8=8- B2_05 (Purge UF BW) - Operation Purge,9=9- B2_06 - Close / Wait Purge Valves,10=10- B2_07_1 - Open / Wait Top Backwash Valves,11=11- B2_07_2 - Start Backwash Pump,12=12- B2_08 (TOP UF BW) - Operation Top Backwash ,13=13- B2_09_1 - Stop Backwash Pump,14=14- B2_09_2 - Close / Wait Top Backwash Valves,15=15- B2_10_1 - Open / Wait Bottom Backwash Valves,16=16- B2_10_2 - Start Backwash Pump,17=17- B2_11 (BOT UF BW) - Operation Bottom Backwash,18=18- B2_12_1 - Stop Backwash Pump,19=19- B2_12_2 - Close / Wait Bottom Backwash Valves,20=20- B2_13_1 - Open / Wait Inlet Valve,21=21- B2_13_2 - Open / Wait Forward Flush Valves,22=22- B2_13_3 - Start Feed Pump,23=23- B2_14 (FWD Flush) - Operation Forward Flush,24=24- B2_15_1 - Stop Feed Pump,25=25- B2_15_2 - Close / Wait Forward Flush Valves,26=26- B2_15_3 - Close / Wait Inlet Valve,27=27- SYNC*)''')

    thePlugin.writeTIALogic('''
(* Automatic generated code from DB_GLOBALSpecs.xlsx using the TIA_Expert_DB_GLOBAL_Template <end> *)''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
