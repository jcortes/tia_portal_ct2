# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)


def TLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    Decorator = ucpc_library.shared_decorator.ExpressionDecorator()
    # Automatically generated variables
    

# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeTIALogic('''
FUNCTION $name$_TL : VOID
TITLE = '$name$_TL'
//
// Transition Logic of $name$
//
// Lparam1:    $Lparam1$
// Lparam2:    $Lparam2$
// Lparam3:    $Lparam3$
// Lparam4:    $Lparam4$
// Lparam5:    $Lparam5$
// Lparam6:    $Lparam6$
// Lparam7:    $Lparam7$
// Lparam8:    $Lparam8$
// Lparam9:    $Lparam9$
// Lparam10:    $Lparam10$
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_TL'
FAMILY: 'TL'
VAR_TEMP
    pco_transitions0 : WORD;
    pco_transitions1 : WORD;
    pco_transitions2 : WORD;
    pco_transitions3 : WORD;
    pco_transitions4 : WORD;
    pco_transitions5 : WORD;
    pco_transitions6 : WORD;
    pco_transitions7 : WORD;


END_VAR
BEGIN

''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    thePlugin.writeTIALogic('''

DB_ERROR_SIMU.$name$_TL_E := 0 ; // To complete
DB_ERROR_SIMU.$name$_TL_S := 0 ; // To complete

''')

    thePlugin.writeTIALogic('''(* Automatic generated code from DB_GLOBALSpecs.xlsx using the TIA_Expert_DB_GLOBAL_Template <begin> *)
''')

    thePlugin.writeTIALogic('''(* VARIABLES COMPUTATION *)



''')

    thePlugin.writeTIALogic(Decorator.decorateExpression('''(* TRANSITIONS COMPUTATION *)

(* 1 TO 2 *) GRAPH_CCD_RO.SHUTDOWN_STOP := NOT $name$.FuStopISt AND $name$.EnRStartSt;
(* 2 TO 1 *) GRAPH_CCD_RO.STOP_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 2 TO 3 *) GRAPH_CCD_RO.STOP_STANDBY := $name$.RunOSt;
(* 3 TO 1 *) GRAPH_CCD_RO.STANDBY_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 3 TO 2 *) GRAPH_CCD_RO.STANDBY_STOP := $name$.TStopISt;
(* 3 TO 4 *) GRAPH_CCD_RO.STANDBY_S3_01_1 := FDEU_893_CCD_RO_NorStart OR (FDEU_893_CCD_RO_RUN.OnSt AND NOT FDEU_893_CCD_RO_NorStop AND NOT FDEU_893_CCD_RO_EmerStop) AND NOT FDEU_893_LSLL7101_AL1 AND NOT FDEU_893_LSLL7201_AL1;
(* 3 TO 19 *) GRAPH_CCD_RO.STANDBY_S3_06_1 := (FDEU_893_CCD_RO_Rin_Au OR FDEU_893_CCD_RO_SDF.OnSt) AND NOT FDEU_893_CCD_RO_NorStop AND NOT FDEU_893_CCD_RO_EmerStop;
(* 3 TO 26 *) GRAPH_CCD_RO.STANDBY_S3_07_1 := (FDEU_893_CCD_RO_PFDCon OR FDEU_893_CCD_PFDRUN.OnSt) AND NOT FDEU_893_CCD_RO_NorStop AND NOT FDEU_893_CCD_RO_EmerStop;
(* 3 TO 37 *) GRAPH_CCD_RO.STANDBY_RO_CIP := FDEU_893_CCD_RO_CIP.OnSt AND NOT (FDEU_893_LT6001.PosSt > FDEU_893_LT6001_L.PosSt);
(* 4 TO 1 *) GRAPH_CCD_RO.S3_01_1_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 4 TO 2 *) GRAPH_CCD_RO.S3_01_1_STOP := $name$.TStopISt;
(* 4 TO 5 *) GRAPH_CCD_RO.S3_01_1_S3_01_2 := FDEU_893_AQE2701.OnSt AND FDEU_893_AQE2703.OnSt;
(* 5 TO 1 *) GRAPH_CCD_RO.S3_01_2_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 5 TO 2 *) GRAPH_CCD_RO.S3_01_2_STOP := $name$.TStopISt;
(* 5 TO 6 *) GRAPH_CCD_RO.S3_01_2_S3_01_3 := FDEU_893_P2401.OnSt  AND TON_CCD_RO_S3_01_2.Q;
(* 6 TO 1 *) GRAPH_CCD_RO.S3_01_3_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 6 TO 2 *) GRAPH_CCD_RO.S3_01_3_STOP := $name$.TStopISt;
(* 6 TO 7 *) GRAPH_CCD_RO.S3_01_3_S3_01_4 := (FDEU_893_DP7201.OnSt OR NOT FDEU_893_DP7201.EnRstartSt) AND (FDEU_893_DP7301.OnSt OR NOT FDEU_893_DP7301.EnRstartSt) AND TON_CCD_RO_S3_01_3.Q;
(* 7 TO 1 *) GRAPH_CCD_RO.S3_01_4_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 7 TO 2 *) GRAPH_CCD_RO.S3_01_4_STOP := $name$.TStopISt;
(* 7 TO 8 *) GRAPH_CCD_RO.S3_01_4_S3_02 := FDEU_893_P2501.OnSt AND TON_CCD_RO_S3_01_4.Q;
(* 8 TO 1 *) GRAPH_CCD_RO.S3_02_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 8 TO 2 *) GRAPH_CCD_RO.S3_02_STOP := $name$.TStopISt;
(* 8 TO 9 *) GRAPH_CCD_RO.S3_02_S3_03_1 := FDEU_893_FQT2703_PUR > FDEU_893_FQT2703_PURSP;
(* 9 TO 1 *) GRAPH_CCD_RO.S3_03_1_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 9 TO 2 *) GRAPH_CCD_RO.S3_03_1_STOP := $name$.TStopISt;
(* 9 TO 10 *) GRAPH_CCD_RO.S3_03_1_S3_03_2 := FDEU_893_AQE2701.OffSt AND FDEU_893_AQE2703.OffSt AND FDEU_893_P2701.OnSt AND TON_CCD_RO_S3_03_1.Q;
(* 10 TO 1 *) GRAPH_CCD_RO.S3_03_2_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 10 TO 2 *) GRAPH_CCD_RO.S3_03_2_STOP := $name$.TStopISt;
(* 10 TO 11 *) GRAPH_CCD_RO.S3_03_2_S3_04_1 := NOT FDEU_893_CCD_RO_EmerStop AND FDEU_893_CCD_RO_END1;
(* 10 TO 13 *) GRAPH_CCD_RO.S3_03_2_S3_05_2 := (NOT FDEU_893_CCD_RO_EmerStop AND FDEU_893_CCD_RO_END2) OR FDEU_893_LSLL7201_AL1 OR FDEU_893_LSLL7101_AL1;
(* 11 TO 1 *) GRAPH_CCD_RO.S3_04_1_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 11 TO 2 *) GRAPH_CCD_RO.S3_04_1_STOP := $name$.TStopISt;
(* 11 TO 12 *) GRAPH_CCD_RO.S3_04_1_S3_04_2 := FDEU_893_AQE2701.OnSt AND NOT FDEU_893_P2701.OnSt AND TON_CCD_RO_S3_04_1.Q;
(* 12 TO 1 *) GRAPH_CCD_RO.S3_04_2_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 12 TO 2 *) GRAPH_CCD_RO.S3_04_2_STOP := $name$.TStopISt;
(* 12 TO 9 *) GRAPH_CCD_RO.S3_04_2_S3_03_1 := NOT FDEU_893_CCD_RO_NorStop AND NOT FDEU_893_CCD_RO_EmerStop AND NOT FDEU_893_CCD_RO_SDF.OnSt AND (FDEU_893_FQT2702_PFD > FDEU_893_FQT2702_PFDSP);
(* 12 TO 14 *) GRAPH_CCD_RO.S3_04_2_S3_05_3 := FDEU_893_CCD_RO_NorStop OR FDEU_893_CCD_RO_SDF.OnSt ;
(* 13 TO 1 *) GRAPH_CCD_RO.S3_05_2_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 13 TO 2 *) GRAPH_CCD_RO.S3_05_2_STOP := $name$.TStopISt;
(* 13 TO 14 *) GRAPH_CCD_RO.S3_05_2_S3_05_3 := FDEU_893_AQE2701.OnSt AND NOT FDEU_893_P2701.OnSt AND TON_CCD_RO_S3_05_2.Q;
(* 14 TO 1 *) GRAPH_CCD_RO.S3_05_3_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 14 TO 2 *) GRAPH_CCD_RO.S3_05_3_STOP := $name$.TStopISt;
(* 14 TO 15 *) GRAPH_CCD_RO.S3_05_3_S3_05_4 := FDEU_893_dp7201.OffSt AND FDEU_893_dp7301.OffSt AND TON_CCD_RO_S3_05_3.Q;
(* 15 TO 1 *) GRAPH_CCD_RO.S3_05_4_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 15 TO 2 *) GRAPH_CCD_RO.S3_05_4_STOP := $name$.TStopISt;
(* 15 TO 16 *) GRAPH_CCD_RO.S3_05_4_S3_05_5 := (FDEU_893_FQT2704_SDF  > FDEU_893_FQT2704_SDFSP) AND (FDEU_893_CCD_RO_NorStop OR NOT FDEU_893_CCD_RO_QS );
(* 15 TO 35 *) GRAPH_CCD_RO.S3_05_4_S3_08_1 := NOT FDEU_893_CCD_RO_NorStop AND FDEU_893_CCD_RO_QS AND (FDEU_893_FQT2704_SDF  > FDEU_893_FQT2704_SDFSP);
(* 16 TO 1 *) GRAPH_CCD_RO.S3_05_5_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 16 TO 2 *) GRAPH_CCD_RO.S3_05_5_STOP := $name$.TStopISt;
(* 16 TO 17 *) GRAPH_CCD_RO.S3_05_5_S3_05_6 := NOT FDEU_893_P2501.OnSt AND TON_CCD_RO_S3_05_5.Q;
(* 17 TO 1 *) GRAPH_CCD_RO.S3_05_6_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 17 TO 2 *) GRAPH_CCD_RO.S3_05_6_STOP := $name$.TStopISt;
(* 17 TO 18 *) GRAPH_CCD_RO.S3_05_6_S3_05_7 := NOT FDEU_893_P2401.OnSt AND TON_CCD_RO_S3_05_6.Q;
(* 18 TO 1 *) GRAPH_CCD_RO.S3_05_7_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 18 TO 2 *) GRAPH_CCD_RO.S3_05_7_STOP := $name$.TStopISt;
(* 18 TO 3 *) GRAPH_CCD_RO.S3_05_7_STANDBY := FDEU_893_AQE2701.OffSt;
(* 19 TO 1 *) GRAPH_CCD_RO.S3_06_1_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 19 TO 2 *) GRAPH_CCD_RO.S3_06_1_STOP := $name$.TStopISt;
(* 19 TO 20 *) GRAPH_CCD_RO.S3_06_1_S3_06_2 := FDEU_893_AQE2701.OnSt;
(* 20 TO 1 *) GRAPH_CCD_RO.S3_06_2_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 20 TO 2 *) GRAPH_CCD_RO.S3_06_2_STOP := $name$.TStopISt;
(* 20 TO 21 *) GRAPH_CCD_RO.S3_06_2_S3_06_3 := FDEU_893_P2401.OnSt AND TON_CCD_RO_S3_06_2.Q;
(* 21 TO 1 *) GRAPH_CCD_RO.S3_06_3_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 21 TO 2 *) GRAPH_CCD_RO.S3_06_3_STOP := $name$.TStopISt;
(* 21 TO 22 *) GRAPH_CCD_RO.S3_06_3_S3_06_4 := FDEU_893_P2501.OnSt AND TON_CCD_RO_S3_06_3.Q;
(* 22 TO 1 *) GRAPH_CCD_RO.S3_06_4_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 22 TO 2 *) GRAPH_CCD_RO.S3_06_4_STOP := $name$.TStopISt;
(* 22 TO 23 *) GRAPH_CCD_RO.S3_06_4_S3_06_5 := TON_CCD_RO_S3_06_4.Q;
(* 23 TO 1 *) GRAPH_CCD_RO.S3_06_5_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 23 TO 2 *) GRAPH_CCD_RO.S3_06_5_STOP := $name$.TStopISt;
(* 23 TO 24 *) GRAPH_CCD_RO.S3_06_5_S3_06_6 := NOT FDEU_893_P2501.OnSt AND TON_CCD_RO_S3_06_5.Q;
(* 24 TO 1 *) GRAPH_CCD_RO.S3_06_6_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 24 TO 2 *) GRAPH_CCD_RO.S3_06_6_STOP := $name$.TStopISt;
(* 24 TO 25 *) GRAPH_CCD_RO.S3_06_6_S3_06_7 := NOT FDEU_893_P2401.OnSt AND TON_CCD_RO_S3_06_6.Q;
(* 25 TO 1 *) GRAPH_CCD_RO.S3_06_7_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 25 TO 2 *) GRAPH_CCD_RO.S3_06_7_STOP := $name$.TStopISt;
(* 25 TO 3 *) GRAPH_CCD_RO.S3_06_7_STANDBY := FDEU_893_AQE2701.OffSt;
(* 26 TO 1 *) GRAPH_CCD_RO.S3_07_1_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 26 TO 2 *) GRAPH_CCD_RO.S3_07_1_STOP := $name$.TStopISt;
(* 26 TO 27 *) GRAPH_CCD_RO.S3_07_1_S3_07_2 := FDEU_893_AQE2701.OnSt;
(* 27 TO 1 *) GRAPH_CCD_RO.S3_07_2_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 27 TO 2 *) GRAPH_CCD_RO.S3_07_2_STOP := $name$.TStopISt;
(* 27 TO 28 *) GRAPH_CCD_RO.S3_07_2_S3_07_3 := FDEU_893_P2401.OnSt AND TON_CCD_RO_S3_07_2.Q;
(* 28 TO 1 *) GRAPH_CCD_RO.S3_07_3_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 28 TO 2 *) GRAPH_CCD_RO.S3_07_3_STOP := $name$.TStopISt;
(* 28 TO 29 *) GRAPH_CCD_RO.S3_07_3_S3_07_4 := (FDEU_893_DP7201.OnSt OR NOT FDEU_893_DP7201.EnRstartSt) AND (FDEU_893_DP7301.OnSt OR NOT FDEU_893_DP7301.EnRstartSt) AND TON_CCD_RO_S3_07_3.Q;
(* 29 TO 1 *) GRAPH_CCD_RO.S3_07_4_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 29 TO 2 *) GRAPH_CCD_RO.S3_07_4_STOP := $name$.TStopISt;
(* 29 TO 30 *) GRAPH_CCD_RO.S3_07_4_S3_07_5 := FDEU_893_P2501.OnSt AND TON_CCD_RO_S3_07_4.Q;
(* 30 TO 1 *) GRAPH_CCD_RO.S3_07_5_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 30 TO 2 *) GRAPH_CCD_RO.S3_07_5_STOP := $name$.TStopISt;
(* 30 TO 31 *) GRAPH_CCD_RO.S3_07_5_S3_07_6 := FDEU_893_CCD_RO_NorStop OR FDEU_893_CCD_RO_SBY.OnSt;
(* 31 TO 1 *) GRAPH_CCD_RO.S3_07_6_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 31 TO 2 *) GRAPH_CCD_RO.S3_07_6_STOP := $name$.TStopISt;
(* 31 TO 32 *) GRAPH_CCD_RO.S3_07_6_S3_07_7 := TON_CCD_RO_S3_07_6.Q;
(* 32 TO 1 *) GRAPH_CCD_RO.S3_07_7_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 32 TO 2 *) GRAPH_CCD_RO.S3_07_7_STOP := $name$.TStopISt;
(* 32 TO 33 *) GRAPH_CCD_RO.S3_07_7_S3_07_8 := NOT FDEU_893_P2501.OnSt AND FDEU_893_dp7201.OffSt AND FDEU_893_DP7301.OffSt AND TON_CCD_RO_S3_07_7.Q;
(* 33 TO 1 *) GRAPH_CCD_RO.S3_07_8_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 33 TO 2 *) GRAPH_CCD_RO.S3_07_8_STOP := $name$.TStopISt;
(* 33 TO 34 *) GRAPH_CCD_RO.S3_07_8_S3_07_9 := NOT FDEU_893_P2401.OnSt AND TON_CCD_RO_S3_07_8.Q;
(* 34 TO 1 *) GRAPH_CCD_RO.S3_07_9_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 34 TO 2 *) GRAPH_CCD_RO.S3_07_9_STOP := $name$.TStopISt;
(* 34 TO 3 *) GRAPH_CCD_RO.S3_07_9_STANDBY := FDEU_893_AQE2701.OffSt;
(* 35 TO 1 *) GRAPH_CCD_RO.S3_08_1_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 35 TO 2 *) GRAPH_CCD_RO.S3_08_1_STOP := $name$.TStopISt;
(* 35 TO 36 *) GRAPH_CCD_RO.S3_08_1_S3_08_2 := (FDEU_893_DP7201.OnSt OR NOT FDEU_893_DP7201.EnRstartSt) AND (FDEU_893_DP7301.OnSt OR NOT FDEU_893_DP7301.EnRstartSt) AND TON_CCD_RO_S3_08_1.Q;
(* 36 TO 1 *) GRAPH_CCD_RO.S3_08_2_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 36 TO 2 *) GRAPH_CCD_RO.S3_08_2_STOP := $name$.TStopISt;
(* 36 TO 10 *) GRAPH_CCD_RO.S3_08_2_S3_03_2 := FDEU_893_P2701.OnSt AND FDEU_893_ZSL2701 AND TON_CCD_RO_S3_08_2.Q;
(* 37 TO 1 *) GRAPH_CCD_RO.RO_CIP_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 37 TO 2 *) GRAPH_CCD_RO.RO_CIP_STOP := $name$.TStopISt;
(* 37 TO 3 *) GRAPH_CCD_RO.RO_CIP_STANDBY := FDEU_893_CCD_RO_NorStop OR FDEU_893_CCD_RO_SBY.OnSt OR FDEU_893_CCD_RO_EmerStop;
'''))

    thePlugin.writeTIALogic('''
(* WORD STATUS FOR STEPS *)
FDEU_893_CCD_RO_St.AuPosR := INT_TO_WORD(GRAPH_CCD_RO.S_NO);
''')

    thePlugin.writeTIALogic('''
(* WORD STATUS FOR TRANSITIONS *)
pco_transitions0.%X0  := GRAPH_CCD_RO.SHUTDOWN_STOP;
pco_transitions0.%X1  := GRAPH_CCD_RO.STOP_SHUTDOWN;
pco_transitions0.%X2  := GRAPH_CCD_RO.STOP_STANDBY;
pco_transitions0.%X3  := GRAPH_CCD_RO.STANDBY_SHUTDOWN;
pco_transitions0.%X4  := GRAPH_CCD_RO.STANDBY_STOP;
pco_transitions0.%X5  := GRAPH_CCD_RO.STANDBY_S3_01_1;
pco_transitions0.%X6  := GRAPH_CCD_RO.STANDBY_S3_06_1;
pco_transitions0.%X7  := GRAPH_CCD_RO.STANDBY_S3_07_1;
pco_transitions0.%X8  := GRAPH_CCD_RO.STANDBY_RO_CIP;
pco_transitions0.%X9  := GRAPH_CCD_RO.S3_01_1_SHUTDOWN;
pco_transitions0.%X10 := GRAPH_CCD_RO.S3_01_1_STOP;
pco_transitions0.%X11 := GRAPH_CCD_RO.S3_01_1_S3_01_2;
pco_transitions0.%X12 := GRAPH_CCD_RO.S3_01_2_SHUTDOWN;
pco_transitions0.%X13 := GRAPH_CCD_RO.S3_01_2_STOP;
pco_transitions0.%X14 := GRAPH_CCD_RO.S3_01_2_S3_01_3;
pco_transitions0.%X15 := GRAPH_CCD_RO.S3_01_3_SHUTDOWN;
pco_transitions1.%X0  := GRAPH_CCD_RO.S3_01_3_STOP;
pco_transitions1.%X1  := GRAPH_CCD_RO.S3_01_3_S3_01_4;
pco_transitions1.%X2  := GRAPH_CCD_RO.S3_01_4_SHUTDOWN;
pco_transitions1.%X3  := GRAPH_CCD_RO.S3_01_4_STOP;
pco_transitions1.%X4  := GRAPH_CCD_RO.S3_01_4_S3_02;
pco_transitions1.%X5  := GRAPH_CCD_RO.S3_02_SHUTDOWN;
pco_transitions1.%X6  := GRAPH_CCD_RO.S3_02_STOP;
pco_transitions1.%X7  := GRAPH_CCD_RO.S3_02_S3_03_1;
pco_transitions1.%X8  := GRAPH_CCD_RO.S3_03_1_SHUTDOWN;
pco_transitions1.%X9  := GRAPH_CCD_RO.S3_03_1_STOP;
pco_transitions1.%X10 := GRAPH_CCD_RO.S3_03_1_S3_03_2;
pco_transitions1.%X11 := GRAPH_CCD_RO.S3_03_2_SHUTDOWN;
pco_transitions1.%X12 := GRAPH_CCD_RO.S3_03_2_STOP;
pco_transitions1.%X13 := GRAPH_CCD_RO.S3_03_2_S3_04_1;
pco_transitions1.%X14 := GRAPH_CCD_RO.S3_03_2_S3_05_2;
pco_transitions1.%X15 := GRAPH_CCD_RO.S3_04_1_SHUTDOWN;
pco_transitions2.%X0  := GRAPH_CCD_RO.S3_04_1_STOP;
pco_transitions2.%X1  := GRAPH_CCD_RO.S3_04_1_S3_04_2;
pco_transitions2.%X2  := GRAPH_CCD_RO.S3_04_2_SHUTDOWN;
pco_transitions2.%X3  := GRAPH_CCD_RO.S3_04_2_STOP;
pco_transitions2.%X4  := GRAPH_CCD_RO.S3_04_2_S3_03_1;
pco_transitions2.%X5  := GRAPH_CCD_RO.S3_04_2_S3_05_3;
pco_transitions2.%X6  := GRAPH_CCD_RO.S3_05_2_SHUTDOWN;
pco_transitions2.%X7  := GRAPH_CCD_RO.S3_05_2_STOP;
pco_transitions2.%X8  := GRAPH_CCD_RO.S3_05_2_S3_05_3;
pco_transitions2.%X9  := GRAPH_CCD_RO.S3_05_3_SHUTDOWN;
pco_transitions2.%X10 := GRAPH_CCD_RO.S3_05_3_STOP;
pco_transitions2.%X11 := GRAPH_CCD_RO.S3_05_3_S3_05_4;
pco_transitions2.%X12 := GRAPH_CCD_RO.S3_05_4_SHUTDOWN;
pco_transitions2.%X13 := GRAPH_CCD_RO.S3_05_4_STOP;
pco_transitions2.%X14 := GRAPH_CCD_RO.S3_05_4_S3_05_5;
pco_transitions2.%X15 := GRAPH_CCD_RO.S3_05_4_S3_08_1;
pco_transitions3.%X0  := GRAPH_CCD_RO.S3_05_5_SHUTDOWN;
pco_transitions3.%X1  := GRAPH_CCD_RO.S3_05_5_STOP;
pco_transitions3.%X2  := GRAPH_CCD_RO.S3_05_5_S3_05_6;
pco_transitions3.%X3  := GRAPH_CCD_RO.S3_05_6_SHUTDOWN;
pco_transitions3.%X4  := GRAPH_CCD_RO.S3_05_6_STOP;
pco_transitions3.%X5  := GRAPH_CCD_RO.S3_05_6_S3_05_7;
pco_transitions3.%X6  := GRAPH_CCD_RO.S3_05_7_SHUTDOWN;
pco_transitions3.%X7  := GRAPH_CCD_RO.S3_05_7_STOP;
pco_transitions3.%X8  := GRAPH_CCD_RO.S3_05_7_STANDBY;
pco_transitions3.%X9  := GRAPH_CCD_RO.S3_06_1_SHUTDOWN;
pco_transitions3.%X10 := GRAPH_CCD_RO.S3_06_1_STOP;
pco_transitions3.%X11 := GRAPH_CCD_RO.S3_06_1_S3_06_2;
pco_transitions3.%X12 := GRAPH_CCD_RO.S3_06_2_SHUTDOWN;
pco_transitions3.%X13 := GRAPH_CCD_RO.S3_06_2_STOP;
pco_transitions3.%X14 := GRAPH_CCD_RO.S3_06_2_S3_06_3;
pco_transitions3.%X15 := GRAPH_CCD_RO.S3_06_3_SHUTDOWN;
pco_transitions4.%X0  := GRAPH_CCD_RO.S3_06_3_STOP;
pco_transitions4.%X1  := GRAPH_CCD_RO.S3_06_3_S3_06_4;
pco_transitions4.%X2  := GRAPH_CCD_RO.S3_06_4_SHUTDOWN;
pco_transitions4.%X3  := GRAPH_CCD_RO.S3_06_4_STOP;
pco_transitions4.%X4  := GRAPH_CCD_RO.S3_06_4_S3_06_5;
pco_transitions4.%X5  := GRAPH_CCD_RO.S3_06_5_SHUTDOWN;
pco_transitions4.%X6  := GRAPH_CCD_RO.S3_06_5_STOP;
pco_transitions4.%X7  := GRAPH_CCD_RO.S3_06_5_S3_06_6;
pco_transitions4.%X8  := GRAPH_CCD_RO.S3_06_6_SHUTDOWN;
pco_transitions4.%X9  := GRAPH_CCD_RO.S3_06_6_STOP;
pco_transitions4.%X10 := GRAPH_CCD_RO.S3_06_6_S3_06_7;
pco_transitions4.%X11 := GRAPH_CCD_RO.S3_06_7_SHUTDOWN;
pco_transitions4.%X12 := GRAPH_CCD_RO.S3_06_7_STOP;
pco_transitions4.%X13 := GRAPH_CCD_RO.S3_06_7_STANDBY;
pco_transitions4.%X14 := GRAPH_CCD_RO.S3_07_1_SHUTDOWN;
pco_transitions4.%X15 := GRAPH_CCD_RO.S3_07_1_STOP;
pco_transitions5.%X0  := GRAPH_CCD_RO.S3_07_1_S3_07_2;
pco_transitions5.%X1  := GRAPH_CCD_RO.S3_07_2_SHUTDOWN;
pco_transitions5.%X2  := GRAPH_CCD_RO.S3_07_2_STOP;
pco_transitions5.%X3  := GRAPH_CCD_RO.S3_07_2_S3_07_3;
pco_transitions5.%X4  := GRAPH_CCD_RO.S3_07_3_SHUTDOWN;
pco_transitions5.%X5  := GRAPH_CCD_RO.S3_07_3_STOP;
pco_transitions5.%X6  := GRAPH_CCD_RO.S3_07_3_S3_07_4;
pco_transitions5.%X7  := GRAPH_CCD_RO.S3_07_4_SHUTDOWN;
pco_transitions5.%X8  := GRAPH_CCD_RO.S3_07_4_STOP;
pco_transitions5.%X9  := GRAPH_CCD_RO.S3_07_4_S3_07_5;
pco_transitions5.%X10 := GRAPH_CCD_RO.S3_07_5_SHUTDOWN;
pco_transitions5.%X11 := GRAPH_CCD_RO.S3_07_5_STOP;
pco_transitions5.%X12 := GRAPH_CCD_RO.S3_07_5_S3_07_6;
pco_transitions5.%X13 := GRAPH_CCD_RO.S3_07_6_SHUTDOWN;
pco_transitions5.%X14 := GRAPH_CCD_RO.S3_07_6_STOP;
pco_transitions5.%X15 := GRAPH_CCD_RO.S3_07_6_S3_07_7;
pco_transitions6.%X0  := GRAPH_CCD_RO.S3_07_7_SHUTDOWN;
pco_transitions6.%X1  := GRAPH_CCD_RO.S3_07_7_STOP;
pco_transitions6.%X2  := GRAPH_CCD_RO.S3_07_7_S3_07_8;
pco_transitions6.%X3  := GRAPH_CCD_RO.S3_07_8_SHUTDOWN;
pco_transitions6.%X4  := GRAPH_CCD_RO.S3_07_8_STOP;
pco_transitions6.%X5  := GRAPH_CCD_RO.S3_07_8_S3_07_9;
pco_transitions6.%X6  := GRAPH_CCD_RO.S3_07_9_SHUTDOWN;
pco_transitions6.%X7  := GRAPH_CCD_RO.S3_07_9_STOP;
pco_transitions6.%X8  := GRAPH_CCD_RO.S3_07_9_STANDBY;
pco_transitions6.%X9  := GRAPH_CCD_RO.S3_08_1_SHUTDOWN;
pco_transitions6.%X10 := GRAPH_CCD_RO.S3_08_1_STOP;
pco_transitions6.%X11 := GRAPH_CCD_RO.S3_08_1_S3_08_2;
pco_transitions6.%X12 := GRAPH_CCD_RO.S3_08_2_SHUTDOWN;
pco_transitions6.%X13 := GRAPH_CCD_RO.S3_08_2_STOP;
pco_transitions6.%X14 := GRAPH_CCD_RO.S3_08_2_S3_03_2;
pco_transitions6.%X15 := GRAPH_CCD_RO.RO_CIP_SHUTDOWN;
pco_transitions7.%X0  := GRAPH_CCD_RO.RO_CIP_STOP;
pco_transitions7.%X1  := GRAPH_CCD_RO.RO_CIP_STANDBY;
pco_transitions7.%X2  := false;
pco_transitions7.%X3  := false;
pco_transitions7.%X4  := false;
pco_transitions7.%X5  := false;
pco_transitions7.%X6  := false;
pco_transitions7.%X7  := false;
pco_transitions7.%X8  := false;
pco_transitions7.%X9  := false;
pco_transitions7.%X10 := false;
pco_transitions7.%X11 := false;
pco_transitions7.%X12 := false;
pco_transitions7.%X13 := false;
pco_transitions7.%X14 := false;
pco_transitions7.%X15 := false;


FDEU_893_CCD_RO_Tr0.AuPosR := pco_transitions0;
FDEU_893_CCD_RO_Tr1.AuPosR := pco_transitions1;
FDEU_893_CCD_RO_Tr2.AuPosR := pco_transitions2;
FDEU_893_CCD_RO_Tr3.AuPosR := pco_transitions3;
FDEU_893_CCD_RO_Tr4.AuPosR := pco_transitions4;
FDEU_893_CCD_RO_Tr5.AuPosR := pco_transitions5;
FDEU_893_CCD_RO_Tr6.AuPosR := pco_transitions6;
FDEU_893_CCD_RO_Tr7.AuPosR := pco_transitions7;

''')

    thePlugin.writeTIALogic('''
(*1=1- SHUTDOWN,2=2- STOP,3=3- STANDBY,4=4- S3_01_1 - Open / Wait Brine Discharge Valve,5=5- S3_01_2 - Start RO Feed Pump,6=6- S3_01_3 - Start RO Dosing Pump[s],7=7- S3_01_4 - Start RO HP Pump,8=8- S3_02 (PUR) - Operation Power Up Rinse [PUR],9=9- S3_03_1 - Start RO Recycle Pump,10=10- S3_03_2 (CCD) - Operation Closed Circuit Desalination [CCD],11=11- S3_04_1 - stop RO Recycle Pump,12=12- S3_04_2 (PFD) - Operation Plug Flow Desalination [PFD],13=13- S3_05_2 - Open / Wait Brine Discharge Valve. Stop RO Recyle Pump,14=14- S3_05_3 - Stop RO Dosing Pump[s],15=15- S3_05_4 (SDF) - Operation Shutdown Flush [SDF],16=16- S3_05_5 - Stop RO HP Pump,17=17- S3_05_6 - Stop RO Feed Pump,18=18- S3_05_7 - Close / Wait Brine Discharge Valve,19=19- S3_06_1 - Open / Wait Brine Discharge Valve,20=20- S3_06_2 - Start RO Feed Pump,21=21- S3_06_3 - Start RO HP Pump,22=22- S3_06_4 (Rinse) - Operation Rinse,23=23- S3_06_5 - Stop RO HP Pump,24=24- S3_06_6 - Stop RO Feed Pump,25=25- S3_06_7 - Close / Wait Brine Discharge Valve,26=26- S3_07_1 - Open / Wait Brine Discharge Valve,27=27- S3_07_2 - Start RO Feed Pump,28=28- S3_07_3 - Start RO Dosing Pump[s],29=29- S3_07_4 - Start RO HP Pump,30=30- S3_07_5 (CPFD) - Operation Continuous Plug Flow Desalination [CPFD],31=31- S3_07_6 - Stop RO HP Pump,32=32- S3_07_7 - Stop RO Dosing Pump[s],33=33- S3_07_8 - Stop RO Feed Pump,34=34- S3_07_9 - Close / Wait Brine Discharge Valve,35=35- S3_08_1 - Start RO Dosing Pump,36=36- S3_08_2 - Start RO Recycle Pump and Close / Wait Brine Discharge Valve,37=37-RO_CIP*)''')

    thePlugin.writeTIALogic('''
(* Automatic generated code from DB_GLOBALSpecs.xlsx using the TIA_Expert_DB_GLOBAL_Template <end> *)''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
