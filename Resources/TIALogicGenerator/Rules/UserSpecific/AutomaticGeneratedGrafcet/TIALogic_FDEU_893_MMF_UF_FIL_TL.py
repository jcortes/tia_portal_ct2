# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)


def TLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    Decorator = ucpc_library.shared_decorator.ExpressionDecorator()
    # Automatically generated variables
    

# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeTIALogic('''
FUNCTION $name$_TL : VOID
TITLE = '$name$_TL'
//
// Transition Logic of $name$
//
// Lparam1:    $Lparam1$
// Lparam2:    $Lparam2$
// Lparam3:    $Lparam3$
// Lparam4:    $Lparam4$
// Lparam5:    $Lparam5$
// Lparam6:    $Lparam6$
// Lparam7:    $Lparam7$
// Lparam8:    $Lparam8$
// Lparam9:    $Lparam9$
// Lparam10:    $Lparam10$
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_TL'
FAMILY: 'TL'
VAR_TEMP
    pco_transitions0 : WORD;
    pco_transitions1 : WORD;


END_VAR
BEGIN

''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    thePlugin.writeTIALogic('''

DB_ERROR_SIMU.$name$_TL_E := 0 ; // To complete
DB_ERROR_SIMU.$name$_TL_S := 0 ; // To complete

''')

    thePlugin.writeTIALogic('''(* Automatic generated code from DB_GLOBALSpecs.xlsx using the TIA_Expert_DB_GLOBAL_Template <begin> *)
''')

    thePlugin.writeTIALogic('''(* VARIABLES COMPUTATION *)



''')

    thePlugin.writeTIALogic(Decorator.decorateExpression('''(* TRANSITIONS COMPUTATION *)

(* 1 TO 2 *) GRAPH_MMF_UF_FIL.INIT_S1_01_1 := GRAPH_MMF_UF.FILTRATION.X;
(* 2 TO 1 *) GRAPH_MMF_UF_FIL.S1_01_1_INIT := NOT GRAPH_MMF_UF.FILTRATION.X;
(* 2 TO 3 *) GRAPH_MMF_UF_FIL.S1_01_1_S1_01_2 := FDEU_893_AQE1001.OnSt;
(* 3 TO 1 *) GRAPH_MMF_UF_FIL.S1_01_2_INIT := NOT GRAPH_MMF_UF.FILTRATION.X;
(* 3 TO 4 *) GRAPH_MMF_UF_FIL.S1_01_2_S1_01_3 := FDEU_893_AQE1201.OnSt AND FDEU_893_AQE1211.OnSt AND FDEU_893_AQE1202.OnSt AND FDEU_893_AQE1212.OnSt;
(* 4 TO 1 *) GRAPH_MMF_UF_FIL.S1_01_3_INIT := NOT GRAPH_MMF_UF.FILTRATION.X;
(* 4 TO 5 *) GRAPH_MMF_UF_FIL.S1_01_3_S1_01_4 := FDEU_893_AQE1301.OnSt AND FDEU_893_AQE1302.OnSt;
(* 5 TO 1 *) GRAPH_MMF_UF_FIL.S1_01_4_INIT := NOT GRAPH_MMF_UF.FILTRATION.X;
(* 5 TO 6 *) GRAPH_MMF_UF_FIL.S1_01_4_S1_01_5 := FDEU_893_P1101.OnSt AND TON_MMF_UF_FIL_S1_01_4.Q;
(* 6 TO 1 *) GRAPH_MMF_UF_FIL.S1_01_5_INIT := NOT GRAPH_MMF_UF.FILTRATION.X;
(* 6 TO 7 *) GRAPH_MMF_UF_FIL.S1_01_5_S1_02_RUN := TON_MMF_UF_FIL_S1_01_5.Q;
(* 7 TO 1 *) GRAPH_MMF_UF_FIL.S1_02_RUN_INIT := NOT GRAPH_MMF_UF.FILTRATION.X;
(* 7 TO 8 *) GRAPH_MMF_UF_FIL.S1_02_RUN_S1_03_1 := ((FDEU_893_MMF_BW_Con OR FDEU_893_MMF_BW_RUN) AND FDEU_893_MMF_BW_En) OR  
 ((FDEU_893_UF_BW_Con OR FDEU_893_UF_BW_RUN) AND FDEU_893_UF_BW_En) OR
 ((FDEU_893_UF_OXBW_Con OR FDEU_893_UF_OX_BW) AND FDEU_893_UF_OXBW_En) OR
 ((FDEU_893_UF_ACBW_Con OR FDEU_893_UF_AC_BW)  AND FDEU_893_UF_ACBW_En) OR FDEU_893_MMF_UF_SBY OR FDEU_893_MMF_UF_StopCon;
(* 8 TO 1 *) GRAPH_MMF_UF_FIL.S1_03_1_INIT := NOT GRAPH_MMF_UF.FILTRATION.X;
(* 8 TO 9 *) GRAPH_MMF_UF_FIL.S1_03_1_S1_03_2 := TON_MMF_UF_FIL_S1_03_1.Q;
(* 9 TO 1 *) GRAPH_MMF_UF_FIL.S1_03_2_INIT := NOT GRAPH_MMF_UF.FILTRATION.X;
(* 9 TO 10 *) GRAPH_MMF_UF_FIL.S1_03_2_S1_03_3 := NOT FDEU_893_P1101.OnSt AND TON_MMF_UF_FIL_S1_03_2.Q;
(* 10 TO 1 *) GRAPH_MMF_UF_FIL.S1_03_3_INIT := NOT GRAPH_MMF_UF.FILTRATION.X;
(* 10 TO 11 *) GRAPH_MMF_UF_FIL.S1_03_3_S1_03_4 := FDEU_893_AQE1301.OffSt AND FDEU_893_AQE1302.OffSt;
(* 11 TO 1 *) GRAPH_MMF_UF_FIL.S1_03_4_INIT := NOT GRAPH_MMF_UF.FILTRATION.X;
(* 11 TO 12 *) GRAPH_MMF_UF_FIL.S1_03_4_S1_03_5 := FDEU_893_AQE1201.OffSt AND FDEU_893_AQE1211.OffSt AND FDEU_893_AQE1202.OffSt AND FDEU_893_AQE1212.OffSt;
(* 12 TO 1 *) GRAPH_MMF_UF_FIL.S1_03_5_INIT := NOT GRAPH_MMF_UF.FILTRATION.X;
(* 12 TO 13 *) GRAPH_MMF_UF_FIL.S1_03_5_SYNC := FDEU_893_AQE1001.OffSt OR FDEU_893_AQE1001.AuOnRSt;
(* 13 TO 1 *) GRAPH_MMF_UF_FIL.SYNC_INIT := NOT GRAPH_MMF_UF.FILTRATION.X;
'''))

    thePlugin.writeTIALogic('''
(* WORD STATUS FOR STEPS *)
FDEU_893_MMF_UF_FIL_St.AuPosR := INT_TO_WORD(GRAPH_MMF_UF_FIL.S_NO);
''')

    thePlugin.writeTIALogic('''
(* WORD STATUS FOR TRANSITIONS *)
pco_transitions0.%X0  := GRAPH_MMF_UF_FIL.INIT_S1_01_1;
pco_transitions0.%X1  := GRAPH_MMF_UF_FIL.S1_01_1_INIT;
pco_transitions0.%X2  := GRAPH_MMF_UF_FIL.S1_01_1_S1_01_2;
pco_transitions0.%X3  := GRAPH_MMF_UF_FIL.S1_01_2_INIT;
pco_transitions0.%X4  := GRAPH_MMF_UF_FIL.S1_01_2_S1_01_3;
pco_transitions0.%X5  := GRAPH_MMF_UF_FIL.S1_01_3_INIT;
pco_transitions0.%X6  := GRAPH_MMF_UF_FIL.S1_01_3_S1_01_4;
pco_transitions0.%X7  := GRAPH_MMF_UF_FIL.S1_01_4_INIT;
pco_transitions0.%X8  := GRAPH_MMF_UF_FIL.S1_01_4_S1_01_5;
pco_transitions0.%X9  := GRAPH_MMF_UF_FIL.S1_01_5_INIT;
pco_transitions0.%X10 := GRAPH_MMF_UF_FIL.S1_01_5_S1_02_RUN;
pco_transitions0.%X11 := GRAPH_MMF_UF_FIL.S1_02_RUN_INIT;
pco_transitions0.%X12 := GRAPH_MMF_UF_FIL.S1_02_RUN_S1_03_1;
pco_transitions0.%X13 := GRAPH_MMF_UF_FIL.S1_03_1_INIT;
pco_transitions0.%X14 := GRAPH_MMF_UF_FIL.S1_03_1_S1_03_2;
pco_transitions0.%X15 := GRAPH_MMF_UF_FIL.S1_03_2_INIT;
pco_transitions1.%X0  := GRAPH_MMF_UF_FIL.S1_03_2_S1_03_3;
pco_transitions1.%X1  := GRAPH_MMF_UF_FIL.S1_03_3_INIT;
pco_transitions1.%X2  := GRAPH_MMF_UF_FIL.S1_03_3_S1_03_4;
pco_transitions1.%X3  := GRAPH_MMF_UF_FIL.S1_03_4_INIT;
pco_transitions1.%X4  := GRAPH_MMF_UF_FIL.S1_03_4_S1_03_5;
pco_transitions1.%X5  := GRAPH_MMF_UF_FIL.S1_03_5_INIT;
pco_transitions1.%X6  := GRAPH_MMF_UF_FIL.S1_03_5_SYNC;
pco_transitions1.%X7  := GRAPH_MMF_UF_FIL.SYNC_INIT;
pco_transitions1.%X8  := false;
pco_transitions1.%X9  := false;
pco_transitions1.%X10 := false;
pco_transitions1.%X11 := false;
pco_transitions1.%X12 := false;
pco_transitions1.%X13 := false;
pco_transitions1.%X14 := false;
pco_transitions1.%X15 := false;


FDEU_893_MMF_UF_FIL_Tr0.AuPosR := pco_transitions0;
FDEU_893_MMF_UF_FIL_Tr1.AuPosR := pco_transitions1;

''')

    thePlugin.writeTIALogic('''
(*1=1-INIT,2=2- S1_01_1 - Open / Wait Inlet Valve,3=3- S1_01_2 - Open / Wait MMF Service Valves,4=4- S1_01_3 - Open / Wait UF Service Valves,5=5- S1_01_4 - Start Feed Pump,6=6- S1_01_5 - Start Dosing Pump,7=7- S1_02_RUN - Operation Filtration,8=8- S1_03_1 - Stop Dosing Pump,9=9- S1_03_2 - Stop Feed Pump,10=10- S1_03_3 - Close / Wait UF Service Valves,11=11- S1_03_4 - Close / Wait MMF Service Valves,12=12- S1_03_5 - Close / Wait Inlet Valve,13=13- SYNC*)''')

    thePlugin.writeTIALogic('''
(* Automatic generated code from DB_GLOBALSpecs.xlsx using the TIA_Expert_DB_GLOBAL_Template <end> *)''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
