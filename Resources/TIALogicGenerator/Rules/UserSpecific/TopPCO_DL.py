# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
reload(TIALogic_DefaultAlarms_Template)

import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)

import ucpc_library.shared_grafcet_parsing
reload(ucpc_library.shared_grafcet_parsing)

import ProcessFeatures
reload(ProcessFeatures)

def ProcessControlObjectLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1,Lparam2,Lparam3,Lparam4,Lparam5,Lparam6,Lparam7,Lparam8,Lparam9,Lparam10=TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    # Init external classes -----------------------------------------------------------
    GrafcetParser   = ucpc_library.shared_grafcet_parsing.SharedGrafcetParsing()        
    Decorator       = ucpc_library.shared_decorator.ExpressionDecorator()

    # Load parameters -----------------------------------------------------------------
    DB_DB_GLOBAL      = Lparam2                                                           # Name of the DB
    listOfSteps     = Lparam10.split(',')                   # list of steps of DB_DB_GLOBAL (empty if DB_DB_GLOBAL is "")
    
    
    Features    = Lparam8   # List of optional features
    
    # List of supported features with number of parameters ----------------------------
    SupportedFeatures = {
        'auihfomo':[1,"string"],           # takes: logic to set AuIhFoMo input
        'auihmmo':[1,"string"],            # takes: logic to set AuIhMMo input
        'fon':[1,"string"],                # does nothing in this file - see PCO_CL
        'foff':[1,"string"],               # does nothing in this file - see PCO_CL
        'cstopfin':[1,"string"],            # does nothing in this file - see PCO_CL
        'auopmor':[1,"string"]            # does nothing in this file - see PCO_CL
    }
    
    # Process features ----------------------------------------------------------------
    RequestedFeatures = ProcessFeatures.parseFeatures(Features)                     # parse features
    ProcessFeatures.checkFeatures(name, RequestedFeatures, SupportedFeatures, thePlugin, theRawInstances)  # check corectness of parameters numbers
    

    thePlugin.writeTIALogic('''
FUNCTION $name$_DL : VOID
TITLE = '$name$_DL'
//
// Dependent Logic of $name$
// Master: $master$
(*
 Lparam1:   $Lparam1$
 Lparam2:   $Lparam2$   // Name of the DB of the DB_GLOBAL 
 Lparam3:   $Lparam3$
 Lparam4:   $Lparam4$
 Lparam5:   $Lparam5$
 Lparam6:   $Lparam6$
 Lparam7:   $Lparam7$
 Lparam8:   $Lparam8$   // List of optional features
 Lparam9:   $Lparam9$
 Lparam10:  $Lparam10$
*)
//
AUTHOR: 'UNICOS'
NAME: 'Logic_DL'
FAMILY: 'PCO'
BEGIN
''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')
    # Step 1.1: Position Request calculation. The user should complet this part according the logic process. By default we put that to "0".
    thePlugin.writeTIALogic('''
(*Position initialization*)
IF $name$.RunOSt THEN
    $name$.AuOnR:= TRUE;
    $name$.AuOffR:= FALSE;
END_IF;

IF NOT $name$.RunOSt AND NOT $name$.TStopISt THEN
    $name$.AuOnR:= FALSE;
    $name$.AuOffR:= TRUE;
END_IF;

$name$.AuCStopR:= 0;
''')

    # Feature: AuIhFoMo ---------------------------------------------------------------
    if "auihfomo" in RequestedFeatures:
        AuIhFoMo = Decorator.decorateExpression(RequestedFeatures['auihfomo'][0], DB_DB_GLOBAL, listOfSteps)
        thePlugin.writeTIALogic('''
$name$.AuIhFoMo := $AuIhFoMo$;''')

    # Feature: AuIhMMo ----------------------------------------------------------------
    if "auihmmo" in RequestedFeatures:
        AuIhMMo = Decorator.decorateExpression(RequestedFeatures['auihmmo'][0], DB_DB_GLOBAL, listOfSteps)
        thePlugin.writeTIALogic('''
$name$.AuIhMMo := $AuIhMMo$;''')

    if master == name or master.lower() == "no_master":
        test = "do nothing"
    else:
        thePlugin.writeTIALogic('''
$name$.AuOpMoR := 1.0; //  To complete
''')

    # Step 1.2: IoSimu and IoError: both for DA and AA. We pass the IOError and the IOSimu from the Input associated to the DB_ERROR_SIMU created
    # IoError
    thePlugin.writeTIALogic('''

(*IoSimu and IoError*****************)
DB_ERROR_SIMU.$name$_DL_E := 0; // To complete
DB_ERROR_SIMU.$name$_DL_S := 0; // To complete
''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
