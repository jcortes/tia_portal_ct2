# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
reload(TIALogic_DefaultAlarms_Template)

import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)

import ucpc_library.shared_grafcet_parsing
reload(ucpc_library.shared_grafcet_parsing)

import ProcessFeatures
reload(ProcessFeatures)

def AnalogLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1,Lparam2,Lparam3,Lparam4,Lparam5,Lparam6,Lparam7,Lparam8,Lparam9,Lparam10=TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)
    
    # Init external classes -----------------------------------------------------------
    GrafcetParser   = ucpc_library.shared_grafcet_parsing.SharedGrafcetParsing()        
    Decorator       = ucpc_library.shared_decorator.ExpressionDecorator()

    # Load parameters -----------------------------------------------------------------
    DB_DB_GLOBAL      = Lparam1                                                           # Name of the DB
    listOfSteps     = Lparam10.split(',')                     # list of steps of DB_DB_GLOBAL (empty if DB_DB_GLOBAL is "")
    
    Positioning     = Lparam3                                                           # list of condition:position pairs for positioning
    PositioningON   = Decorator.decorateExpression(Lparam4, DB_DB_GLOBAL, listOfSteps)    # additional condition for positioning    
    
    Features        = Lparam8                                                           # list of optional features
    
    # List of supported features with number of parameters ----------------------------
    SupportedFeatures = {
        'inverted'          : [0],                      # takes: nothing; inverts the scaling of PID ordered value
        'ignorecontroller'  : [0],                      # takes: nothing; value commanded by PID will be ignored in steps specified in Lparam4
        'defaultposition'   : [1, "string"],            # takes: position, any format; The default position of device (0.0 if not specified)
        'max'               : [0],                      # takes: nothing; if analog is controlled by multiple PIDs it will take greater OV
        'splitrange'        : [2, "string", "string"],  # takes: the beginning and the end of the range
        'auihfomo'          : [1, "string"],            # takes: logic to set AuIhFoMo input
        'auihmmo'           : [1, "string"],            # takes: logic to set AuIhMMo input
        'rangemin'          : [1, "string"],            # takes: value or object to override RangeMin, to modify split range for example.
        'rangemax'          : [1, "string"],            # takes: value or object to override RangeMax, to modify split range for example.
        'audespd'           :   [2,"string", "string"], # takes: (1) logic condition to set specified AuDeSpd, and (2) new AuDeSpd parameter or logic, etc 
        'auinspd'           :   [2,"string", "string"], # takes: (1) logic condition to set specified AuInSpd, and (2) new AuInSpd parameter or logic, etc
        'aualackmaster'     :   [1, "string"]           # takes: the name of 'master' device
    }
    
    # Process features ----------------------------------------------------------------
    RequestedFeatures = ProcessFeatures.parseFeatures(Features)                     # parse features
    ProcessFeatures.checkFeatures(name, RequestedFeatures, SupportedFeatures, thePlugin, theRawInstances)  # check corectness of parameters numbers
    
    (ListOfConditions, ListOfPositions) = ProcessFeatures.getConditionsAndPositions(Positioning, DB_DB_GLOBAL, listOfSteps)    # get conditions and positions for positioning
    
    # Step 1.0. Header of the file
    thePlugin.writeTIALogic('''//
FUNCTION $name$_DL : VOID
TITLE = '$name$_DL'
//
// Dependent Logic of $name$
//
// Master: 	$master$
// Name: 	$name$
(*
 Lparam1:	$Lparam1$   // Name of the DB of the DB_GLOBAL 
 Lparam2:	$Lparam2$   
 Lparam3:	$Lparam3$   // List of condition:position pairs for positioning
 Lparam4:	$Lparam4$   // Additional condition for positioning    
 Lparam5:	$Lparam5$
 Lparam6:	$Lparam6$ 
 Lparam7:	$Lparam7$
 Lparam8:	$Lparam8$   // List of optional features
 Lparam9:	$Lparam9$
 Lparam10:  $Lparam10$
*)
AUTHOR: 'UNICOS'
NAME: 'Logic_DL'
FAMILY: 'Analog'
''')
    
    # Step 1.1: Create all the variable that we're using on this Function
    thePlugin.writeTIALogic('''
VAR_TEMP
   old_status   :DWORD;
   IN 	        :REAL; //Scaling input
   OUT 	        :REAL; //Scaling Output, connected to AuPosR
   In_Min	    :REAL;
   In_Max 	    :REAL;
   Out_Min      :REAL;
   Out_Max      :REAL;
END_VAR
BEGIN
''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    #=========================== Default Position ===================================================================
    if 'defaultposition' in RequestedFeatures:
        DefaultPosition = Decorator.decorateExpression(RequestedFeatures['defaultposition'][0], DB_DB_GLOBAL, listOfSteps)
    else:
        DefaultPosition = "0.0"
    #=========================== Default Position END ===============================================================

    # Step 1.2: Scaling and obtaining the Output to drive the ANALOG device
    # Gets all the controller objects that are child of the analog object with some properties
    AllControllers = theRawInstances.findMatchingInstances("Controller","'#FEDeviceOutputs:Controlled Objects#' != ''")

    # go through all controllers and pick only the ones which control this Analog
    theControllerObjects = [Controller.getAttributeData("DeviceIdentification:Name").replace(",", " ") for Controller in AllControllers if name in Controller.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ").split()]

    theNumberOfControllers  = len(theControllerObjects)    
    theNumberOfControlledObjects = 0
    ScalingMethod = ""
    
    # get scaling method of the first PID of this Analog
    if theNumberOfControllers > 0:
        FirstController = theRawInstances.findMatchingInstances("Controller","'#DeviceIdentification:Name#'='$theControllerObjects[0]$'")[0]
        theNumberOfControlledObjects = len(FirstController.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ").split())
        ScalingMethod = FirstController.getAttributeData("FEDeviceParameters:Controller Parameters:Scaling Method")

    if "splitrange" in RequestedFeatures:   # split range
        In_Min = Decorator.decorateExpression(RequestedFeatures['splitrange'][0], DB_DB_GLOBAL, listOfSteps)
        In_Max = Decorator.decorateExpression(RequestedFeatures['splitrange'][1], DB_DB_GLOBAL, listOfSteps)
    else:                                   # default range
        In_Min = "0.0"
        In_Max = "100.0"
    
    # this replaces min and max of range (default 0-100 or split range)
    # and changes the function from increasing to decresing
    if "inverted" in RequestedFeatures:
        In_Min, In_Max = In_Max, In_Min     # swap the range boundaries
    
    # Gets the RangeMax and RangeMin for the current Analog
    thisAnalogInstance = theRawInstances.findMatchingInstances ("Analog","'#DeviceIdentification:Name#'='$name$'")[0]
    ProcessOutput = thisAnalogInstance.getAttributeData("FEDeviceOutputs:Process Output")
        
    inst_ProcessOutput_Vec = theRawInstances.findMatchingInstances("AnalogOutput, AnalogOutputReal","'#DeviceIdentification:Name#'='$ProcessOutput$'")
    RangeMin = "0.0"
    RangeMax = "100.0"
    if (ProcessOutput <> ""):
        RangeMin = thePlugin.formatNumberPLC(inst_ProcessOutput_Vec[0].getAttributeData("FEDeviceParameters:Range Min"))
        RangeMax = thePlugin.formatNumberPLC(inst_ProcessOutput_Vec[0].getAttributeData("FEDeviceParameters:Range Max"))
    else:
        RangeMin = "0.0"
        RangeMax = "100.0"

    # === RangeMin ==========================================================================================================================
    # allow user to overwrite RangeMin, i.e. for split range, if he wants
    if "rangemin" in RequestedFeatures:   # split range
        RangeMin = Decorator.decorateExpression(RequestedFeatures['rangemin'][0], DB_DB_GLOBAL, listOfSteps)

    # === RangeMax ==========================================================================================================================
    # allow user to overwrite RangeMin, i.e. for split range, if he wants
    if "rangemax" in RequestedFeatures:   # split range
        RangeMax = Decorator.decorateExpression(RequestedFeatures['rangemax'][0], DB_DB_GLOBAL, listOfSteps)


    # there are no controllers driving this Analog (means No Scaling), the position is calculated according to steps/positions
    if theNumberOfControllers == 0:
        # is there a need to have these comments?
        if master.lower() <> "no_master": thePlugin.writeTIALogic('''
// There are no controllers driving the Analog Object $name$ and this Analog has master: $master$
''')
        else: thePlugin.writeTIALogic('''
// There are no controllers driving the Analog Object $name$ and this Analog has no master
''')
        # generate logic for conditioned positioning
        for i, (condition, position) in enumerate(zip(ListOfConditions, ListOfPositions)):
            if i>0: thePlugin.writeTIALogic('''ELS''')  # if it's first entry, skip "ELS"
            thePlugin.writeTIALogic('''IF (($condition$) $PositioningON$) THEN
    $name$.AuPosR := $position$;
''')
        if ListOfConditions:
            thePlugin.writeTIALogic('''ELSE
    $name$.AuPosR := $DefaultPosition$;
END_IF;''')
        else:
            thePlugin.writeTIALogic('''$name$.AuPosR := $DefaultPosition$;''')

    # If there is only 1 controller driving this Analog, analyze the theNumberOfControllers, the theNumberOfControlledObjects and the ScalingMethod
    elif theNumberOfControllers == 1:
        thePlugin.writeTIALogic('''
$theControllerObjects[0]$.AuActR:= TRUE;
        ''')
        if theNumberOfControlledObjects == 1:
            thePlugin.writeTIALogic('''
// The analog object "$name$" is controlled by 1 regulator ($theControllerObjects[0]$) and the user selected "$ScalingMethod$"
            ''')
            # Input Scaling
            if ScalingMethod == "Input Scaling":
                thePlugin.writeTIALogic('''
IN := $theControllerObjects[0]$.OutOV;
(*Scaling Parameters**************) 
//Values to change if SPlit Range (No point in SplitRange for one object!)
In_Min	:= $In_Min$;
In_Max 	:= $In_Max$; 
Out_Min := $RangeMin$;
Out_Max := $RangeMax$;
OUT := (IN-In_Min) * (Out_Max-Out_Min)/(In_Max-In_Min) + Out_Min;  //Scaling
IF OUT > Out_Max Then 
	OUT := Out_Max; //High Limitation
END_IF;	
IF OUT < Out_Min Then 
	OUT := Out_Min; //Low Limitation
END_IF;
''')
            else:   # if ScalingMethod != "InputScaling"
                thePlugin.writeTIALogic('''
OUT := $theControllerObjects[0]$.OutOV;
''')
        else:   # if theNumberOfControlledObjects != 1:
            thePlugin.writeTIALogic('''
// The analog object "$name$" is controlled by 1 regulator ($theControllerObjects[0]$) and there are several controlled objects (as the $name$)
IN := $theControllerObjects[0]$.OutOV;
(*Scaling Parameters**************) 
//Values to change if Split Range (set automatically according to SplitRange feature)
In_Min	:= $In_Min$;
In_Max 	:= $In_Max$;
Out_Min := $RangeMin$;
Out_Max := $RangeMax$;
OUT := (IN-In_Min) * (Out_Max-Out_Min)/(In_Max-In_Min) + Out_Min;  //Scaling
IF OUT > Out_Max Then 
	OUT := Out_Max; //High Limitation
END_IF;	
IF OUT < Out_Min Then 
	OUT := Out_Min; //Low Limitation
END_IF;	
''')

    # If there are several controllers driving this Analog, there is not Scaling and the user has to fix the conditions
    else:   # theNumberOfControllers > 1
        thePlugin.writeTIALogic('''
// The Analog object "$name$" is controlled by '''+ str(theNumberOfControllers) +''' regulators:
''')
        for controllerName in theControllerObjects:
            thePlugin.writeTIALogic('''// $controllerName$
''')
        if "max" in RequestedFeatures:
        # this will be adapted to work with any number of controllers
            thePlugin.writeTIALogic('''// We take the biggest controller output
IF $theControllerObjects[0]$.OutOV > $theControllerObjects[1]$.OutOV THEN
    OUT := $theControllerObjects[0]$.OutOV;
	$theControllerObjects[0]$.AuActR:= TRUE; 
	$theControllerObjects[1]$.AuActR:= FALSE; 
ELSE
    OUT := $theControllerObjects[1]$.OutOV;
	$theControllerObjects[1]$.AuActR:= TRUE; 
	$theControllerObjects[0]$.AuActR:= FALSE; 
END_IF;

''')
        else:   # to be checked/completed
            thePlugin.writeTIALogic('''
// The user must select the condition for the AuActR
OUT := $theControllerObjects[0]$.OutOV;
$theControllerObjects[0]$.AuActR:= TRUE;
$theControllerObjects[1]$.AuActR:= FALSE;
        ''')
            

    # assigning output to the Analog
    if theNumberOfControllers > 0: # this section is related ONLY to controlled Analogs
        #if "ignorecontroller" not in RequestedFeatures or not ListOfConditions: # if there is no case where controller is ignored or no steps are specified
        if not ListOfConditions: # if there is no case where controller is ignored or no steps are specified
            thePlugin.writeTIALogic('''
$name$.AuPosR:= OUT;
''')
        else:   # if there ARE steps where position is required to be different than ordered by controller
            for i, (condition, position) in enumerate(zip(ListOfConditions, ListOfPositions)):
                if i>0: thePlugin.writeTIALogic('''ELS''')
                thePlugin.writeTIALogic('''IF (($condition$) $PositioningON$) THEN
    $name$.AuPosR := $position$;
''')
            thePlugin.writeTIALogic('''
ELSE
    $name$.AuPosR := OUT;
END_IF;
''')

    #=========================== AuIhFoMo ===================================================================
    if 'auihfomo' in RequestedFeatures:
        AuIhFoMo = Decorator.decorateExpression(RequestedFeatures['auihfomo'][0], DB_DB_GLOBAL, listOfSteps)
        thePlugin.writeTIALogic('''
$name$.AuIhFoMo := $AuIhFoMo$;''')

    #=========================== AuIhMMo ===================================================================
    if 'auihmmo' in RequestedFeatures:
        AuIhMMo = Decorator.decorateExpression(RequestedFeatures['auihmmo'][0], DB_DB_GLOBAL, listOfSteps)
        thePlugin.writeTIALogic('''
$name$.AuIhMMo := $AuIhMMo$;''')

    #=========================== AuDeSpd & AuInSpd ===================================================================
    # Step 1.3: Ramp Parameters
    
    ManualDecreaseSpeed = thisAnalogInstance.getAttributeData("FEDeviceParameters:Manual Decrease Speed (Unit/s)")
    ManualIncreaseSpeed = thisAnalogInstance.getAttributeData("FEDeviceParameters:Manual Increase Speed (Unit/s)")
        
    if ManualDecreaseSpeed.strip() == "":
        ManualDecreaseSpeed = '10'
    if ManualIncreaseSpeed.strip() == "":
        ManualIncreaseSpeed = '10'
        
    if 'audespd' in RequestedFeatures:
        ConditionDeSpd = Decorator.decorateExpression(RequestedFeatures['audespd'][0], DB_DB_GLOBAL, listOfSteps)
        DeSpd = Decorator.decorateExpression(RequestedFeatures['audespd'][1], DB_DB_GLOBAL, listOfSteps)
        thePlugin.writeTIALogic('''
		
// Ramp Parameters to fill by the user. By default the "Manual Decrease Speed" and "Manual Increase Speed" are taken
IF ($ConditionDeSpd$) THEN
	$name$.AuDeSpd := $DeSpd$;
ELSE
	$name$.AuDeSpd := $ManualDecreaseSpeed$;
END_IF;''')
    else:
        thePlugin.writeTIALogic('''
// Ramp Parameters to fill by the user. By default the "Manual Decrease Speed" and "Manual Increase Speed" are taken
$name$.AuDeSpd := $ManualDecreaseSpeed$; // To complete''')

    if 'auinspd' in RequestedFeatures:
        ConditionInSpd = Decorator.decorateExpression(RequestedFeatures['auinspd'][0], DB_DB_GLOBAL, listOfSteps)
        InSpd = Decorator.decorateExpression(RequestedFeatures['auinspd'][1], DB_DB_GLOBAL, listOfSteps)
        thePlugin.writeTIALogic('''
IF ($ConditionInSpd$) THEN
	$name$.AuInSpd := $InSpd$;
ELSE
	$name$.AuInSpd := $ManualIncreaseSpeed$;
END_IF;''')
    else:
        thePlugin.writeTIALogic('''
$name$.AuInSpd := $ManualIncreaseSpeed$; // To complete
''')

    #=========================== AuAlAck/AuRStart ====================================================================
    if 'aualackmaster' in RequestedFeatures:
        Actuator    = RequestedFeatures['aualackmaster'][0]    # 'master' actuator
        thePlugin.writeTIALogic('''
//In case AlAck in 'master' actuator, we do AuAlAck/AuRStart on the device
IF $Actuator$.E_MAlAckR OR $Actuator$.AuAlAck THEN
    $name$.AuAlAck := TRUE;
    $name$.AuRStart := TRUE;
END_IF;
''')

    # Step 1.4: Fill the IOError and IOSimu from the Inputs linked by the user logic (if procceds)
    thePlugin.writeTIALogic('''

(*IoSimu and IoError*****************)
// The user must connect the IOError and IOSimu from the linked devices ("IN" variable) if proceeds
DB_ERROR_SIMU.$name$_DL_E := 0; // To complete

DB_ERROR_SIMU.$name$_DL_S :=  0; // To complete
''')

    # Not configured alarms
    #Gets all the Digital Alarms that are child of the 'master' object
    theDigitalAlarms, theDigitalAlarmsMultiple, allTheDigitalAlarms, DAListPosition = TIALogic_DefaultAlarms_Template.getDigitalAlarms(theRawInstances, name)
    #Gets all the Analog Alarms that are child of the 'master' object
    theAnalogAlarms, theAnalogAlarmsMultiple, allTheAnalogAlarms, AAListPosition = TIALogic_DefaultAlarms_Template.getAnalogAlarms(theRawInstances, name)

    #-------------------------------------------------------------------------------------------------------------
    # The "writeNotConfiguredDAParameters" function writes default values for DA parameters: 
    #
    # If Input (in spec) is empty, writes IOError, IOSimu and I
    # Else if Input is not empty, IOError, IOSimu and I will be written by main template function
    # If Delay (in spec) is "logic", writes PAlDt
    # Else if Delay is not "logic", PAlDt will be written by main template function
    #
    # NOTE: the user can choose to remove this function call from the user template, in which case he will be 
    # responsible for writing the code for the appropriate pins himself
    #
    #-------------------------------------------------------------------------------------------------------------
    TIALogic_DefaultAlarms_Template.writeNotConfiguredDAParameters(thePlugin, theDigitalAlarms, theDigitalAlarmsMultiple, DAListPosition)

    #-------------------------------------------------------------------------------------------------------------
    # The "writeNotConfiguredAAParameters" function writes default values for AA parameters: 
    #
    # If HH Alarm (in spec) is a number, writes AuEHH. In this case HH will be written by main template function
    # If HH Alarm is "logic", writes AuEHH and HH
    # If HH Alarm is a reference to an object, writes AuEHH. In this case HH will be written by main template function
    # NOTE: the same applies for H Warning, L Warning, and LL Alarm
    # If Input (in spec) is empty, writes IOError, IOSimu and I
    # If Delay (in spec) is "logic", writes PAlDt
    #
    # NOTE: the user can choose to remove this function call from the user template, in which case he will be 
    # responsible for writing the code for the appropriate pins himself
    #
    #-------------------------------------------------------------------------------------------------------------
    TIALogic_DefaultAlarms_Template.writeNotConfiguredAAParameters(thePlugin, theAnalogAlarms, theAnalogAlarmsMultiple, AAListPosition)

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
