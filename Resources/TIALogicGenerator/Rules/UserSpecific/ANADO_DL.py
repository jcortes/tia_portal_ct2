# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from java.util import Vector
from java.util import ArrayList
from research.ch.cern.unicos.utilities import AbsolutePathBuilder
import os

import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)
   
import TIALogic_DefaultAlarms_Template
reload(TIALogic_DefaultAlarms_Template)

import ucpc_library.shared_grafcet_parsing
reload(ucpc_library.shared_grafcet_parsing)

import ProcessFeatures
reload(ProcessFeatures)

def AnaDOLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1,Lparam2,Lparam3,Lparam4,Lparam5,Lparam6,Lparam7,Lparam8,Lparam9,Lparam10=TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    # Init external classes -----------------------------------------------------------
    GrafcetParser   = ucpc_library.shared_grafcet_parsing.SharedGrafcetParsing()        
    Decorator       = ucpc_library.shared_decorator.ExpressionDecorator()               
    
    # Load parameters -----------------------------------------------------------------
    DB_DB_GLOBAL      = Lparam1                                                           # name of the DB
    listOfSteps     = Lparam10.split(',')                     # list of steps of DB_DB_GLOBAL (empty if DB_DB_GLOBAL is "")
    
    ConditionON     = Decorator.decorateExpression(Lparam2, DB_DB_GLOBAL, listOfSteps)    # condition for AuOnR
    Positioning     = Lparam3                                                           # list of condition:position pairs for positioning        
    PositioningON   = Decorator.decorateExpression(Lparam4, DB_DB_GLOBAL, listOfSteps)    # additional condition for positioning
    
    Features        = Lparam8                                                           # List of optional features
    
    # List of supported features with number of parameters ----------------------------
    SupportedFeatures = {
        'dbglobal'          :   [1, "string"],                                                      # takes: db name; specifies name of the global DB
        'inverted'          :   [0],                                                                # takes: nothing; inverts the scaling of PID ordered value
        'ignorecontroller'  :   [0],                                                                # takes: nothing; value commanded by PID will be ignored in steps specified in Lparam3
        'anticourtcycle'    :   [4, "string", "OnOff", "AnalogParameter", "AnalogStatus"],          # takes: TON timer name, OnOff to reset, APAR for rest time, AS to keep remaining time
        'worktime'          :   [4, "string", "OnOff", "AnalogStatus", "AnalogParameter"],          # takes: RTM timer name, OnOff to Reset, AS to keep working time, APAR with offset
        'startcount'        :   [2, "OnOff", "AnalogStatus"],                                       # takes: OnOff to reset, AS to keep number of starts; Counts starts of the device
        'startcountwoff'    :   [4, "OnOff", "AnalogStatus", "AnalogStatus", "AnalogParameter"],    # takes: OnOff to reset, AS to keep number of starts; Counts starts of the device
        'auaumoronfault'    :   [-1],                                                               # takes: list of names of redundant devices
        'defaultposition'   :   [1,"string"],                                                       # takes: position, any format; The default position of device (0.0 if not specified)
        'splitrange'        :   [2,"string", "string"],                                             # takes: the beginning and the end of the range
        'auihfomo'          :   [1, "string"],                                                      # takes: logic to set AuIhFoMo input
        'auihmmo'           :   [1, "string"],                                                      # takes: logic to set AuIhMMo input
        'auackstartinterlock':  [0],                                                                # takes: nothing; sets AuAlAck on F_EDGE of StartISt, if no FS or TS
        'rangemin'          :   [1, "string"],                                                      # takes: value or object to override RangeMin, to modify split range for example.
        'rangemax'          :   [1, "string"],                                                      # takes: value or object to override RangeMax, to modify split range for example.
        'auonr'             :   [1,"string"],                                                       # takes: logic to set AuOnR input
        'auoffr'            :   [1,"string"],                                                       # takes: logic to set AuOffR input
        'audespd'           :   [2,"string", "string"],                                             # takes: (1) logic condition to set specified AuDeSpd, and (2) new AuDeSpd parameter or logic, etc 
        'auinspd'           :   [2,"string", "string"],                                             # takes: (1) logic condition to set specified AuInSpd, and (2) new AuInSpd parameter or logic, etc
        'aualackmaster'     :   [1, "string"],                                                      # takes: the name of 'master' device
        'max'               :   [0],                      # takes: nothing; if anado is controlled by multiple PIDs it will take greater OV
        'pid'               :   [-1]                      # list of condition:pid pairs if anado is controlled by multiple PIDs
    }

    # Process features ----------------------------------------------------------------
    RequestedFeatures = ProcessFeatures.parseFeatures(Features)                     # parse features
    ProcessFeatures.checkFeatures(name, RequestedFeatures, SupportedFeatures, thePlugin, theRawInstances)  # check corectness of parameters numbers
    
    (ListOfConditions, ListOfPositions) = ProcessFeatures.getConditionsAndPositions(Positioning, DB_DB_GLOBAL, listOfSteps)    # get conditions and positions for positioning
      
    # Feature: AuOnR ----------------------------------------------------------------
    AuOnR = "" # default
    if ConditionON != "":
        AuOnR = ConditionON
    elif "auonr" in RequestedFeatures:
        AuOnR = Decorator.decorateExpression(RequestedFeatures['auonr'][0], DB_DB_GLOBAL, listOfSteps)

    # Feature: AuOffR ----------------------------------------------------------------
    AuOffR = "" # default
    if "auoffr" in RequestedFeatures:
        AuOffR = Decorator.decorateExpression(RequestedFeatures['auoffr'][0], DB_DB_GLOBAL, listOfSteps)

    # Handle AuOnR and AuOffR ---------------------------------------------------------
    if not AuOnR and not AuOffR:
        thePlugin.writeWarningInUABLog("OnOff $name$: No condition for AuOnR nor for AuOffR provided! Assumed: AuOnR := $master$.RunOSt, AuOffR := NOT $master$.RunOSt")
        AuOnR = "$master$.RunOSt"
        AuOffR = "NOT $master$.RunOSt"
    else:
        if not AuOffR:        # if off condition not provided, make it complementary to On condition
            AuOffR = '''NOT $name$.AuOnR'''
        else:
            AuOffR = '''$AuOffR$'''
    
        if not AuOnR:         # if on conditions not provided, make it complementary to off condition
            AuOnR = '''NOT ($AuOffR$)'''
        else:
            AuOnR = '''$AuOnR$'''
    
      
    # Step 1.0. Header of the file
    thePlugin.writeTIALogic('''
FUNCTION $name$_DL : VOID
TITLE = '$name$_DL'
//
// Dependent Logic of $name$
// 
//
// Master: 	$master$
// Name: 	$name$
(*
 Lparam1:	$Lparam1$	// Name of the DB of the DB_GLOBAL
 Lparam2:	$Lparam2$	// Condition for AuOnR
 Lparam3:	$Lparam3$   // List of condition:position pairs for positioning        
 Lparam4:	$Lparam4$   // Additional condition for positioning
 Lparam5:	$Lparam5$
 Lparam6:	$Lparam6$ 
 Lparam7:	$Lparam7$   
 Lparam8:	$Lparam8$   // List of optional features
 Lparam9:	$Lparam9$
 Lparam10:  $Lparam10$
*)
AUTHOR: 'UNICOS'
NAME: 'Logic_DL'
FAMILY: 'AnaDO'
''')
    
    # Step 1.1: Create all the variables that we're using on this Function
    thePlugin.writeTIALogic('''
VAR_TEMP
   IN       : REAL; //Scaling input
   OUT      : REAL; //Scaling Output, connected to AuPosR
   In_Min   : REAL;
   In_Max   : REAL;
   Out_Min  : REAL;
   Out_Max  : REAL;
   mode_RTM : INT;
END_VAR
BEGIN
''')
    
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    #=========================== Default Position ===================================================================
    if 'defaultposition' in RequestedFeatures:
        DefaultPosition = Decorator.decorateExpression(RequestedFeatures['defaultposition'][0], DB_DB_GLOBAL, listOfSteps)
    else:
        DefaultPosition = "0.0"
    
    # AuOnR/AuOffR of AnaDO
    thePlugin.writeTIALogic('''
(*ON request Management*)
$name$.AuOnR := $AuOnR$;
    
$name$.AuOffR:= $AuOffR$;
''')
    
    # Step 1.2: Scaling and obtaining the Output to drive the AnaDO device
    # Gets all the controller objects that are child of the AnaDO object with some properties
    AllControllers = theRawInstances.findMatchingInstances("Controller","'#FEDeviceOutputs:Controlled Objects#' != ''")
    
    # go through all controllers and pick only the ones which control this Analog
    theControllerObjects = [Controller.getAttributeData("DeviceIdentification:Name").replace(",", " ") for Controller in AllControllers if name in Controller.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ").split()]
    
    theNumberOfControllers  = len(theControllerObjects)
    theNumberOfControlledObjects = 0
    ScalingMethod = ""    
    
    # get scaling method of the first PID of this Analog
    if theNumberOfControllers > 0:
        FirstController = theRawInstances.findMatchingInstances("Controller","'#DeviceIdentification:Name#'='$theControllerObjects[0]$'")[0]
        theNumberOfControlledObjects = len(FirstController.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ").split())
        ScalingMethod = FirstController.getAttributeData("FEDeviceParameters:Controller Parameters:Scaling Method")
    
    if "splitrange" in RequestedFeatures:   # split range
        In_Min = Decorator.decorateExpression(RequestedFeatures['splitrange'][0], DB_DB_GLOBAL, listOfSteps)
        In_Max = Decorator.decorateExpression(RequestedFeatures['splitrange'][1], DB_DB_GLOBAL, listOfSteps)
    else:                                   # default range
        In_Min = "0.0"
        In_Max = "100.0"
    
    # this replaces min and max of range (default 0-100 or split range)
    # and changes the function from increasing to decresing
    if "inverted" in RequestedFeatures:
        In_Min, In_Max = In_Max, In_Min     # swap the range boundaries
    
    # Gets the RangeMax and RangeMin for the current Analog
    thisAnaDOInstance = theRawInstances.findMatchingInstances ("AnaDO","'#DeviceIdentification:Name#'='$name$'")[0]
    ProcessOutput = thisAnaDOInstance.getAttributeData("FEDeviceOutputs:Analog Process Output")
        
    inst_ProcessOutput_Vec = theRawInstances.findMatchingInstances("AnalogOutput, AnalogOutputReal","'#DeviceIdentification:Name#'='$ProcessOutput$'")

    if (ProcessOutput <> ""):
        RangeMin = thePlugin.formatNumberPLC(inst_ProcessOutput_Vec[0].getAttributeData("FEDeviceParameters:Range Min"))
        RangeMax = thePlugin.formatNumberPLC(inst_ProcessOutput_Vec[0].getAttributeData("FEDeviceParameters:Range Max"))
    else:
        RangeMin = "0.0"
        RangeMax = "100.0"
    
    # === RangeMin ==========================================================================================================================
    # allow user to overwrite RangeMin, i.e. for split range, if he wants
    if "rangemin" in RequestedFeatures:   # split range
        RangeMin = Decorator.decorateExpression(RequestedFeatures['rangemin'][0], DB_DB_GLOBAL, listOfSteps)

    # === RangeMax ==========================================================================================================================
    # allow user to overwrite RangeMin, i.e. for split range, if he wants
    if "rangemax" in RequestedFeatures:   # split range
        RangeMax = Decorator.decorateExpression(RequestedFeatures['rangemax'][0], DB_DB_GLOBAL, listOfSteps)


    # there are no controllers driving this AnaDO (means No Scaling), the position is calculated according to steps/positions
    if theNumberOfControllers == 0:
        # is there a need to have these comments?
        if master.lower() <> "no_master": thePlugin.writeTIALogic('''
// There are no controllers driving the AnaDO Object $name$ and this AnaDO has master: $master$
''')
        else: thePlugin.writeTIALogic('''
// There is no controller driving the AnaDO Object $name$ and this AnaDO has no master
''')
        # generate logic for conditioned positioning
        for i, (condition, position) in enumerate(zip(ListOfConditions, ListOfPositions)):
            if i>0: thePlugin.writeTIALogic('''ELS''')
            thePlugin.writeTIALogic('''IF ($condition$ $PositioningON$) THEN
    $name$.AuPosR := $position$;
''')
        if ListOfConditions:
            thePlugin.writeTIALogic('''ELSE
    $name$.AuPosR := $DefaultPosition$;
END_IF;''')
        else:
            thePlugin.writeTIALogic('''$name$.AuPosR := $DefaultPosition$;''')

    # If there is only 1 controller driving this AnaDO, analyze the theNumberOfControllers, the theNumberOfControlledObjects and the ScalingMethod
    elif theNumberOfControllers == 1:
        thePlugin.writeTIALogic('''
$theControllerObjects[0]$.AuActR:= TRUE;
        ''')
        if theNumberOfControlledObjects == 1:
            thePlugin.writeTIALogic('''
// The AnaDO object "$name$" is controlled by 1 regulator ($theControllerObjects[0]$) and the user selected "Input Scaling"
''')
            # Input Scaling
            if ScalingMethod == "Input Scaling":
                thePlugin.writeTIALogic('''
IN := $theControllerObjects[0]$.OutOV;
(*Scaling Parameters**************) 
//Values to change if SPlit Range 
In_Min	:= $In_Min$;
In_Max 	:= $In_Max$;
Out_Min := $RangeMin$;
Out_Max := $RangeMax$;
OUT := (IN-In_Min) * (Out_Max-Out_Min)/(In_Max-In_Min) + Out_Min;  //Scaling
IF OUT > Out_Max Then 
	OUT := Out_Max; //High Limitation
END_IF;	
IF OUT < Out_Min Then 
	OUT := Out_Min; //Low Limitation
END_IF;	
''')
            else:   # ScalingMethod != "Input Scaling"
                thePlugin.writeTIALogic('''
OUT := $theControllerObjects[0]$.OutOV;
''')
        else:   # theNumberOfControlledObjects > 1
            thePlugin.writeTIALogic('''
// The AnaDO object "$name$" is controlled by 1 regulator ($theControllerObjects[0]$) and there are several controlled objects (as the $name$)
IN := $theControllerObjects[0]$.OutOV;
(*Scaling Parameters**************) 
//Values to change if SPlit Range
In_Min	:= $In_Min$;
In_Max 	:= $In_Max$;
Out_Min := $RangeMin$;
Out_Max := $RangeMax$;
OUT := (IN-In_Min) * (Out_Max-Out_Min)/(In_Max-In_Min) + Out_Min;  //Scaling
IF OUT > Out_Max Then 
	OUT := Out_Max; //High Limitation
END_IF;	
IF OUT < Out_Min Then 
	OUT := Out_Min; //Low Limitation
END_IF;
''')

    # If there are several controllers driving this AnaDO, there is not Scaling and the user has to fix the conditions
    else: # theNumberOfControllers > 1
        thePlugin.writeTIALogic('''
// The AnaDO object "$name$" is controlled by '''+ str(theNumberOfControllers) +''' regulators:
''')
        for controllerName in theControllerObjects:
            thePlugin.writeTIALogic('''// $controllerName$
''')
        if "max" in RequestedFeatures:
        # this will be adapted to work with any number of controllers
            thePlugin.writeTIALogic('''// We take the biggest controller output
IF $theControllerObjects[0]$.OutOV > $theControllerObjects[1]$.OutOV THEN
    OUT := $theControllerObjects[0]$.OutOV;
	$theControllerObjects[0]$.AuActR:= TRUE; 
	$theControllerObjects[1]$.AuActR:= FALSE; 
ELSE
    OUT := $theControllerObjects[1]$.OutOV;
	$theControllerObjects[1]$.AuActR:= TRUE; 
	$theControllerObjects[0]$.AuActR:= FALSE; 
END_IF;

''')
        elif "pid" in RequestedFeatures:

            thePlugin.writeTIALogic('''// Initialize AuActR to FALSE for both regulation loops
$theControllerObjects[0]$.AuActR:= FALSE; 
$theControllerObjects[1]$.AuActR:= FALSE; 
''')
            (ListOfPIDConditions, ListOfPIDs) = ProcessFeatures.getConditionsAndPositions(",".join(RequestedFeatures['pid']), DB_DB_GLOBAL, listOfSteps)    # get conditions and positions for positioning
            # generate logic for conditioned pids
            for i, (condition, pidname) in enumerate(zip(ListOfPIDConditions, ListOfPIDs)):
                pidname = pidname.replace(".OutOV","") # gets decorated by ProcessFeatures.getConditionsAndPositions automatically
                if i>0: thePlugin.writeTIALogic('''ELS''')
                thePlugin.writeTIALogic('''IF ($condition$) THEN
    OUT := $pidname$.OutOV;
    $pidname$.AuActR := TRUE;
''')
            thePlugin.writeTIALogic('''
END_IF;
''')

        else:   # to be checked/completed
            thePlugin.writeTIALogic('''
// The user must select the condition for the AuActR
OUT := $theControllerObjects[0]$.OutOV;
$theControllerObjects[0]$.AuActR:= TRUE;
$theControllerObjects[1]$.AuActR:= FALSE;
        ''')

    # assigning output to the AnaDO
    if theNumberOfControllers>0:    # this section is related ONLY to controlled AnaDOs
        # assign correct position
        #if "ignorecontroller" not in RequestedFeatures or not ListOfConditions: # if there is no scenario where controller is ignored or no steps specified
        if not ListOfConditions: # if there is no scenario where controller is ignored or no steps specified
            thePlugin.writeTIALogic('''
$name$.AuPosR:= OUT;
''')
        else:   # if there ARE steps where position is required to be different than ordered by controller
            for i, (condition, position) in enumerate(zip(ListOfConditions, ListOfPositions)):
                if i>0:
                    thePlugin.writeTIALogic('''ELS''')
                thePlugin.writeTIALogic('''IF ($condition$ $PositioningON$) THEN
    $name$.AuPosR := $position$;
''')
            thePlugin.writeTIALogic('''
ELSE
    $name$.AuPosR := OUT;
END_IF;
''')
    
    #=========================== AuIhFoMo ===================================================================
    if 'auihfomo' in RequestedFeatures:
        AuIhFoMo = Decorator.decorateExpression(RequestedFeatures['auihfomo'][0], DB_DB_GLOBAL, listOfSteps)
        thePlugin.writeTIALogic('''
$name$.AuIhFoMo := $AuIhFoMo$;''')

    #=========================== AuIhMMo ===================================================================
    if 'auihmmo' in RequestedFeatures:
        AuIhMMo = Decorator.decorateExpression(RequestedFeatures['auihmmo'][0], DB_DB_GLOBAL, listOfSteps)
        thePlugin.writeTIALogic('''
$name$.AuIhMMo := $AuIhMMo$;''')

    #=========================== AuDeSpd & AuInSpd ===================================================================
    # Step 1.3: Ramp Parameters
    
    ManualDecreaseSpeed = thisAnaDOInstance.getAttributeData("FEDeviceParameters:Manual Decrease Speed (Unit/s)")
    ManualIncreaseSpeed = thisAnaDOInstance.getAttributeData("FEDeviceParameters:Manual Increase Speed (Unit/s)")

    if ManualDecreaseSpeed.strip() == "":
        ManualDecreaseSpeed = '10'
    if ManualIncreaseSpeed.strip() == "":
        ManualIncreaseSpeed = '10'

    if 'audespd' in RequestedFeatures:
        ConditionDeSpd = Decorator.decorateExpression(RequestedFeatures['audespd'][0], DB_DB_GLOBAL, listOfSteps)
        DeSpd = Decorator.decorateExpression(RequestedFeatures['audespd'][1], DB_DB_GLOBAL, listOfSteps)
        thePlugin.writeTIALogic('''
		
// Ramp Parameters to fill by the user. By default the "Manual Decrease Speed" and "Manual Increase Speed" are taken
IF ($ConditionDeSpd$) THEN
	$name$.AuDeSpd := $DeSpd$;
ELSE
	$name$.AuDeSpd := $ManualDecreaseSpeed$;
END_IF;''')
    else:
        thePlugin.writeTIALogic('''

// Ramp Parameters to fill by the user. By default the "Manual Decrease Speed" and "Manual Increase Speed" are taken
$name$.AuDeSpd := $ManualDecreaseSpeed$; // To complete''')

    if 'auinspd' in RequestedFeatures:
        ConditionInSpd = Decorator.decorateExpression(RequestedFeatures['auinspd'][0], DB_DB_GLOBAL, listOfSteps)
        InSpd = Decorator.decorateExpression(RequestedFeatures['auinspd'][1], DB_DB_GLOBAL, listOfSteps)
        thePlugin.writeTIALogic('''
IF ($ConditionInSpd$) THEN
	$name$.AuInSpd := $InSpd$;
ELSE
	$name$.AuInSpd := $ManualIncreaseSpeed$;
END_IF;''')
    else:
        thePlugin.writeTIALogic('''
$name$.AuInSpd := $ManualIncreaseSpeed$; // To complete
''')
    
    VariablesToCreate = ""
    
    #=========================== Global DB ==========================================================================
    if "dbglobal" in RequestedFeatures:   # argument with name of global DB, has to be a single string, no spaces
        if ' ' in RequestedFeatures['dbglobal'][0]: 
            thePlugin.writeErrorInUABLog("PID : Param8 feature (dbglobal="+RequestedFeatures['dbglobal'][0]+''') is invalid. 
The dbglobal variable must be a valid siemens variable name, i.e. a string with no spaces.''')
        DB_Global = RequestedFeatures['dbglobal'][0] + "."
    else:
        DB_Global = ""

    #=========================== Anticourt cycle ====================================================================
    if "anticourtcycle" in RequestedFeatures:
        VariablesToCreate = VariablesToCreate + '''
        $name$ACrst:\tBOOL;\t// ACC reset 
        F_$name$SI:\tBOOL;\t// SI falling edge
        F_$name$SIo:\tBOOL;\t// SI falling edge old value
        '''
        ACC_TON     = RequestedFeatures['anticourtcycle'][0]    # timer name
        ACC_Reset   = RequestedFeatures['anticourtcycle'][1]    # OnOff to reset ACC
        ACC_APRest  = RequestedFeatures['anticourtcycle'][2]    # APAR for ACC duration
        ACC_ASLeft  = RequestedFeatures['anticourtcycle'][3]    # AS for time left
        
        thePlugin.writeTIALogic('''
(* ---------------------------------------  $name$ Restart CountDown ----------------------------------   *)
    
(*Timer Time assignment*)
$ACC_TON$.TON (IN := NOT $name$.OutOnOV AND NOT $DB_Global$$name$ACrst,
        PT := DINT_TO_TIME ( REAL_TO_DINT (1000.0 * $ACC_APRest$.PosSt)));	

(*Timer Input edge assignment*)
(*When AnaDO off Timer starts*)
(*When Reset Command On Timer Resets*)

IF $ACC_Reset$.OnSt THEN
    $DB_Global$$name$ACrst := TRUE;
END_IF;
IF $name$.OutOnOV THEN
    $DB_Global$$name$ACrst := FALSE;
END_IF;

(*Count Down evaluation*)

IF NOT $ACC_TON$.IN OR $ACC_TON$.Q THEN
    $ACC_ASLeft$.AuPosR := 0.0;
ELSE
    $ACC_ASLeft$.AuPosR := $ACC_APRest$.PosSt - ((DINT_TO_REAL (TIME_TO_DINT ($ACC_TON$.ET))) / 1000.0);
END_IF;

// Falling edge of StartI alarm for anti-court cycle $name$
$DB_Global$F_$name$SI := F_EDGE (new := $name$.StartISt, old := $DB_Global$F_$name$SIo);

IF $DB_Global$F_$name$SI AND $name$.EnRstartSt AND NOT $name$.TStopISt THEN
    $name$.AuAlAck := TRUE;
END_IF;
''')

    #=========================== Auto Acknowledge StartInterlock ====================================================
    #sets AuAlAck on F_EDGE of StartISt, if no FS or TS 
    if "auackstartinterlock" in RequestedFeatures:
        if "anticourtcycle" not in RequestedFeatures:
            VariablesToCreate = VariablesToCreate + '''
        F_$name$SI:\tBOOL;\t// SI falling edge
        F_$name$SIo:\tBOOL;\t// SI falling edge old value
        '''
            thePlugin.writeTIALogic('''
// AuAlAck on falling edge of StartI alarm for $name$
$DB_Global$F_$name$SI := F_EDGE (new := $name$.StartISt, old := $DB_Global$F_$name$SIo);

IF $DB_Global$F_$name$SI AND $name$.EnRstartSt AND NOT $name$.TStopISt THEN
    $name$.AuAlAck := TRUE;
END_IF;
    ''')
    
    #=========================== WorkTime caluclation ===============================================================
    if "worktime" in RequestedFeatures:
        WT_RTM      = RequestedFeatures['worktime'][0]      # RTM timer measuring WorkTime
        WT_Reset    = RequestedFeatures['worktime'][1]      # OnOff to reset timer
        WT_Time     = RequestedFeatures['worktime'][2]      # AS keeping WorkTime
        WT_Offset   = RequestedFeatures['worktime'][3]      # Offset time for WorkTime
        
        thePlugin.writeTIALogic('''
(*Counter of time*)
//Working TIME OF the AnaDO                
IF $WT_Reset$.OnSt THEN
    mode_RTM:=2; //Reset
ELSIF $name$.OnSt THEN
    mode_RTM:=1; //COUNTING
ELSE
    mode_RTM:=0; //STOP
END_IF;

$WT_RTM$(mode:= mode_RTM 	//IN
                    ,NewValue:=0 	//IN
                    );

$WT_Time$.AuPosR := DINT_TO_REAL($WT_RTM$.CurrentValue)/3600.0 + $WT_Offset$.PosSt;
''')

    #=========================== Starts counter with offset ========================================================
    if 'startcountwoff' in RequestedFeatures:            
        SC_Reset    = RequestedFeatures['startcountwoff'][0]    # OnOff to reset counter
        SC_Int      = RequestedFeatures['startcountwoff'][1]    # AS to keep number of starts
        SC_Tot      = RequestedFeatures['startcountwoff'][2]    # AS to keep number of starts + offset
        SC_Ofs      = RequestedFeatures['startcountwoff'][3]    # APAR with offset
        VariablesToCreate = VariablesToCreate + '''
        $name$prvSt:\tBOOL;\t// previous state
        '''
        
        thePlugin.writeTIALogic('''
(*Counter of starts*)
IF $SC_Reset$.OnSt THEN			// Reset
	$SC_Int$.AuPosR := 0;
ELSIF $name$.OnSt AND $DB_Global$$name$prvSt = 0 THEN		                    // If the ANADO is on and previously was off
   $SC_Int$.AuPosR := $SC_Int$.PosSt + 1.0;	// count it
END_IF;

$SC_Tot$.AuPosR  := $SC_Int$.PosSt + $SC_Ofs$.PosSt;

$DB_Global$$name$prvSt := $name$.OnSt;
''')

    #=========================== Starts counter ====================================================================
    if 'startcount' in RequestedFeatures:            
        SC_Reset    = RequestedFeatures['startcount'][0]    # OnOff to reset counter
        SC_Stat     = RequestedFeatures['startcount'][1]    # AS to keep number of starts
        VariablesToCreate = VariablesToCreate + '''
        $name$prvSt:\tBOOL;\t// previous state
        '''
        
        thePlugin.writeTIALogic('''
(*Counter of starts*)
IF $SC_Reset$.OnSt THEN			// Reset
	$SC_Stat$.AuPosR := 0;
ELSIF $name$.OnSt AND $DB_Global$$name$prvSt = 0 THEN		                    // If the ANADO is on and previously was off
   $SC_Stat$.AuPosR := $SC_Stat$.PosSt + 1.0;	// count it
END_IF;

$DB_Global$$name$prvSt := $name$.OnSt;
''')
    
    #=========================== Switch with the redundant AnaDO ====================================================
    # used only when working in pairs
    if "auaumoronfault" in RequestedFeatures:
        SwitchCondition = " OR \n".join([device.strip() + ".E_FuStopI" for device in RequestedFeatures["auaumoronfault"]])
        thePlugin.writeTIALogic('''
// If RE of FullStop on other AnaDO, set AuAuMoR
IF $SwitchCondition$ THEN
    $name$.AuAuMoR := TRUE;
END_IF;
''')

    #=========================== AuAlAck/AuRStart ====================================================================
    if 'aualackmaster' in RequestedFeatures:
        Actuator    = RequestedFeatures['aualackmaster'][0]    # 'master' actuator
        thePlugin.writeTIALogic('''
//In case AlAck in 'master' actuator, we do AuAlAck/AuRStart on the device
IF $Actuator$.E_MAlAckR OR $Actuator$.AuAlAck THEN
    $name$.AuAlAck := TRUE;
    $name$.AuRStart := TRUE;
END_IF;
''')
    
    if VariablesToCreate != "":
        thePlugin.writeWarningInUABLog("You need to add following variables: " + VariablesToCreate)
    
    #================================================================================================================

    # Step 1.5: Fill the IOError and IOSimu from the Inputs linked by the user logic (if procceds)
    thePlugin.writeTIALogic('''

(*IoSimu and IoError*****************)
// The user must connect the IOError and IOSimu from the linked devices ("IN" variable) if proceeds
DB_ERROR_SIMU.$name$_DL_E := 0; // To complete

DB_ERROR_SIMU.$name$_DL_S :=  0; // To complete
''')
    

    # Not configured alarms
    #Gets all the Digital Alarms that are child of the 'master' object
    theDigitalAlarms, theDigitalAlarmsMultiple, allTheDigitalAlarms, DAListPosition = TIALogic_DefaultAlarms_Template.getDigitalAlarms(theRawInstances, name)
    #Gets all the Analog Alarms that are child of the 'master' object
    theAnalogAlarms, theAnalogAlarmsMultiple, allTheAnalogAlarms, AAListPosition = TIALogic_DefaultAlarms_Template.getAnalogAlarms(theRawInstances, name)

    #-------------------------------------------------------------------------------------------------------------
    # The "writeNotConfiguredDAParameters" function writes default values for DA parameters: 
    #
    # If Input (in spec) is empty, writes IOError, IOSimu and I
    # Else if Input is not empty, IOError, IOSimu and I will be written by main template function
    # If Delay (in spec) is "logic", writes PAlDt
    # Else if Delay is not "logic", PAlDt will be written by main template function
    #
    # NOTE: the user can choose to remove this function call from the user template, in which case he will be 
    # responsible for writing the code for the appropriate pins himself
    #
    #-------------------------------------------------------------------------------------------------------------
    TIALogic_DefaultAlarms_Template.writeNotConfiguredDAParameters(thePlugin, theDigitalAlarms, theDigitalAlarmsMultiple, DAListPosition)

    #-------------------------------------------------------------------------------------------------------------
    # The "writeNotConfiguredAAParameters" function writes default values for AA parameters: 
    #
    # If HH Alarm (in spec) is a number, writes AuEHH. In this case HH will be written by main template function
    # If HH Alarm is "logic", writes AuEHH and HH
    # If HH Alarm is a reference to an object, writes AuEHH. In this case HH will be written by main template function
    # NOTE: the same applies for H Warning, L Warning, and LL Alarm
    # If Input (in spec) is empty, writes IOError, IOSimu and I
    # If Delay (in spec) is "logic", writes PAlDt
    #
    # NOTE: the user can choose to remove this function call from the user template, in which case he will be 
    # responsible for writing the code for the appropriate pins himself
    #
    #-------------------------------------------------------------------------------------------------------------
    TIALogic_DefaultAlarms_Template.writeNotConfiguredAAParameters(thePlugin, theAnalogAlarms, theAnalogAlarmsMultiple, AAListPosition)

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
