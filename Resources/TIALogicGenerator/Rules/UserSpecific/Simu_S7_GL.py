# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
reload(TIALogic_DefaultAlarms_Template)


def GLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)
# Step 1. Create the FUNCTION called DeviceName_GL.

    theDI= theRawInstances.findMatchingInstances("DigitalInput", "'#DeviceDocumentation:Description#'!='SPARE'")
    theAI= theRawInstances.findMatchingInstances("AnalogInput", "'#DeviceDocumentation:Description#'!='SPARE'")
    thePID= theRawInstances.findMatchingInstances("Controller", "'#DeviceDocumentation:Description#'!='SPARE'")
    
    
    Delay = "2" #Delay in second to simulate the digital feedbacks
    n = 60      #DB number to begin to generate

    
    #Creation of global DB
    thePlugin.writeTIALogic('''
DATA_BLOCK "DB$n$"
//
// Global DB for simulation
//
STRUCT
''')

    for instDI in theDI:
        DIName = instDI.getAttributeData ("DeviceIdentification:Name")
        Fon_Ana= theRawInstances.findMatchingInstances("Analog,AnaDO,AnalogDigital", "'#FEDeviceEnvironmentInputs:Feedback On#'='$DIName$'")
        Foff_Ana= theRawInstances.findMatchingInstances("Analog,AnalogDigital", "'#FEDeviceEnvironmentInputs:Feedback Off#'='$DIName$'")
        Fon_OnOff= theRawInstances.findMatchingInstances("OnOff", "'#FEDeviceEnvironmentInputs:Feedback On#'='$DIName$'")
        Foff_OnOff= theRawInstances.findMatchingInstances("OnOff", "'#FEDeviceEnvironmentInputs:Feedback Off#'='$DIName$'")
 
        for inst in Fon_Ana: 
            Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")

            thePlugin.writeTIALogic('''
$Actuator_Name$_TON: DINT;''')

        for inst in Foff_Ana: 
            Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")

            thePlugin.writeTIALogic('''
$Actuator_Name$_TOFF: DINT;''')

        for inst in Fon_OnOff: 
            Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")

            thePlugin.writeTIALogic('''
$Actuator_Name$_TON: DINT;''')

        for inst in Foff_OnOff: 
            Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")

            thePlugin.writeTIALogic('''
$Actuator_Name$_TOFF: DINT;''')

    thePlugin.writeTIALogic('''
END_STRUCT
BEGIN
END_DATA_BLOCK
''')

    #Creation of instance DB for PID
    i=n
    for inst in thePID: 
        PID_Name = inst.getAttributeData ("DeviceIdentification:Name")
        i = i+1
        
        thePlugin.writeTIALogic('''
DATA_BLOCK "DB$str(i)$" CPC_FILTER
//
// CPC_FILTER block for simulation of PID: $PID_Name$ 
//
BEGIN

END_DATA_BLOCK
''')
        
        
    thePlugin.writeTIALogic('''

FUNCTION $name$_GL : VOID
TITLE = '$name$_GL'
//
// Global Logic of $name$
//
// Lparam1:    $Lparam1$
// Lparam2:    $Lparam2$
// Lparam3:    $Lparam3$
// Lparam4:    $Lparam4$
// Lparam5:    $Lparam5$
// Lparam6:    $Lparam6$
// Lparam7:    $Lparam7$
// Lparam8:    $Lparam8$
// Lparam9:    $Lparam9$
// Lparam10:    $Lparam10$
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_GL'
FAMILY: 'GL'
BEGIN
''')

        
    # Write inside DI if forced value specified in Param2    
    thePlugin.writeTIALogic('''
 (*Forcing of  DI*)'''    )
 
    for instDI in theDI:
        DIName = instDI.getAttributeData ("DeviceIdentification:Name")
        Param2 = instDI.getAttributeData ("LogicDeviceDefinitions:CustomLogicParameters:Parameter2")

        if Param2 <> "":
            thePlugin.writeTIALogic('''
$DIName$.HFPos := $Param2$;'''    )

    # Feedbacks for DI    
    thePlugin.writeTIALogic('''
    
 (*Feedbacks of Digital actuators*)'''    )
 
    for instDI in theDI:
        DIName = instDI.getAttributeData ("DeviceIdentification:Name")
        Fon_AnaDO= theRawInstances.findMatchingInstances("AnaDO", "'#FEDeviceEnvironmentInputs:Feedback On#'='$DIName$'")
        Fon_Ana= theRawInstances.findMatchingInstances("Analog", "'#FEDeviceEnvironmentInputs:Feedback On#'='$DIName$'")
        Foff_Ana= theRawInstances.findMatchingInstances("Analog", "'#FEDeviceEnvironmentInputs:Feedback Off#'='$DIName$'")
        Fon_AnaDig= theRawInstances.findMatchingInstances("AnalogDigital", "'#FEDeviceEnvironmentInputs:Feedback On#'='$DIName$'")
        Foff_AnaDig= theRawInstances.findMatchingInstances("AnalogDigital", "'#FEDeviceEnvironmentInputs:Feedback Off#'='$DIName$'")
        Fon_OnOff= theRawInstances.findMatchingInstances("OnOff", "'#FEDeviceEnvironmentInputs:Feedback On#'='$DIName$'")
        Foff_OnOff= theRawInstances.findMatchingInstances("OnOff", "'#FEDeviceEnvironmentInputs:Feedback Off#'='$DIName$'")
 
        for inst in Fon_AnaDO: 
            Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")
            
            thePlugin.writeTIALogic('''
IF NOT $Actuator_Name$.OutOnOV THEN
    DB$n$.$Actuator_Name$_TON := DB_WinCCOA.UNICOS_LiveCounter;
END_IF;

$DIName$.HFPos := DB_WinCCOA.UNICOS_LiveCounter -DB$n$.$Actuator_Name$_TON > $Delay$ ;'''    )

        for inst in Fon_Ana: 
            Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")
            
            thePlugin.writeTIALogic('''
IF $Actuator_Name$.PosRSt < 90.0 THEN
    DB$n$.$Actuator_Name$_TON := DB_WinCCOA.UNICOS_LiveCounter;
END_IF;

$DIName$.HFPos := DB_WinCCOA.UNICOS_LiveCounter -DB$n$.$Actuator_Name$_TON > $Delay$ ;'''    )

        for instDI in Foff_Ana: 
            Actuator_Name = instDI.getAttributeData ("DeviceIdentification:Name")
            
            thePlugin.writeTIALogic('''
IF $Actuator_Name$.PosRSt > 10.0 THEN
    DB$n$.$Actuator_Name$_TOFF := DB_WinCCOA.UNICOS_LiveCounter;
END_IF;

$DIName$.HFPos := DB_WinCCOA.UNICOS_LiveCounter -DB$n$.$Actuator_Name$_TOFF > $Delay$ ;'''    )

        for inst in Fon_AnaDig: 
            Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")

            thePlugin.writeTIALogic('''
IF NOT $Actuator_Name$.DOutOnOV THEN
    DB$n$.$Actuator_Name$_TON := DB_WinCCOA.UNICOS_LiveCounter;
END_IF;

IF DB_WinCCOA.UNICOS_LiveCounter -DB$n$.$Actuator_Name$_TON > $Delay$ THEN
    $DIName$.HFPos := TRUE;
END_IF;

IF $Actuator_Name$.DOutOffOV THEN
    $DIName$.HFPos := FALSE;
END_IF;
''')

        for inst in Foff_AnaDig: 
            Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")

            thePlugin.writeTIALogic('''
IF NOT $Actuator_Name$.DOutOffOV THEN
    DB$n$.$Actuator_Name$_TOFF := DB_WinCCOA.UNICOS_LiveCounter;
END_IF;

IF DB_WinCCOA.UNICOS_LiveCounter -DB$n$.$Actuator_Name$_TOFF > $Delay$ THEN
    $DIName$.HFPos := TRUE;
END_IF;

IF $Actuator_Name$.DOutOnOV THEN
    $DIName$.HFPos := FALSE;
END_IF;
''')

        for inst in Fon_OnOff: 
            Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")
            POutOff = inst.getAttributeData ("FEDeviceOutputs:Process Output Off").strip()
            
            if POutOff == "":
                thePlugin.writeTIALogic('''
IF NOT $Actuator_Name$.OutOnOV THEN
    DB$n$.$Actuator_Name$_TON := DB_WinCCOA.UNICOS_LiveCounter;
END_IF;

$DIName$.HFPos := DB_WinCCOA.UNICOS_LiveCounter -DB$n$.$Actuator_Name$_TON > $Delay$ ;'''    )
            else:
                thePlugin.writeTIALogic('''
IF NOT $Actuator_Name$.OutOnOV THEN
    DB$n$.$Actuator_Name$_TON := DB_WinCCOA.UNICOS_LiveCounter;
END_IF;

IF DB_WinCCOA.UNICOS_LiveCounter -DB$n$.$Actuator_Name$_TON > $Delay$ THEN
    $DIName$.HFPos := TRUE;
END_IF;

IF $Actuator_Name$.OutOffOV THEN
    $DIName$.HFPos := FALSE;
END_IF;
''')

        for inst in Foff_OnOff: 
            Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")
            POutOff = inst.getAttributeData ("FEDeviceOutputs:Process Output Off").strip()
            
            if POutOff == "":
                thePlugin.writeTIALogic('''
IF $Actuator_Name$.OutOnOV THEN
    DB$n$.$Actuator_Name$_TOFF := DB_WinCCOA.UNICOS_LiveCounter;
END_IF;

$DIName$.HFPos := DB_WinCCOA.UNICOS_LiveCounter -DB$n$.$Actuator_Name$_TOFF > $Delay$ ;'''    )
            else:
                thePlugin.writeTIALogic('''
IF NOT $Actuator_Name$.OutOffOV THEN
    DB$n$.$Actuator_Name$_TOFF := DB_WinCCOA.UNICOS_LiveCounter;
END_IF;

IF DB_WinCCOA.UNICOS_LiveCounter -DB$n$.$Actuator_Name$_TOFF > $Delay$ THEN
    $DIName$.HFPos := TRUE;
END_IF;

IF $Actuator_Name$.OutOnOV THEN
    $DIName$.HFPos := FALSE;
END_IF;
''')

    # Feedbacks for AI    
    thePlugin.writeTIALogic(''' 
    
    (*Feedbacks of Analog actuators *)''')
 
    for instAI in theAI:
        AIName = instAI.getAttributeData ("DeviceIdentification:Name")
        PMinRan = instAI.getAttributeData ("FEDeviceParameters:Range Min")
        PMaxRan = instAI.getAttributeData ("FEDeviceParameters:Range Max")
        PMinRaw = instAI.getAttributeData ("FEDeviceParameters:Raw Min")
        PMaxRaw = instAI.getAttributeData ("FEDeviceParameters:Raw Max")
        FAnalog_Actuator= theRawInstances.findMatchingInstances("Analog,AnaDO", "'#FEDeviceEnvironmentInputs:Feedback Analog#'='$AIName$'")
        FAnaDig_Actuator= theRawInstances.findMatchingInstances("AnalogDigital", "'#FEDeviceEnvironmentInputs:Feedback Analog#'='$AIName$'")        
        PID= theRawInstances.findMatchingInstances("Controller", "'#FEDeviceEnvironmentInputs:Measured Value#'='$AIName$'")
        
        for inst in FAnalog_Actuator: 
            Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")
            thePlugin.writeTIALogic(''' 
$AIName$.HFPos := REAL_TO_INT( INT_TO_REAL($AIName$.PMaxRaw-$AIName$.PMinRaw) 
    *  $Actuator_Name$.OutOV/($AIName$.PMaxRan-$AIName$.PMinRan))+$AIName$.PMinRaw;
'''    )
        for inst in FAnaDig_Actuator: 
            Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")
            thePlugin.writeTIALogic(''' 
$AIName$.HFPos := REAL_TO_INT( INT_TO_REAL($AIName$.PMaxRaw-$AIName$.PMinRaw) 
    *  $Actuator_Name$.PosRSt/($AIName$.PMaxRan-$AIName$.PMinRan))+$AIName$.PMinRaw;
'''    )

    #PID for AI: assume stable 1st order system
    
    thePlugin.writeTIALogic(''' 
    
    (*PID LOOPS *)''')
    
    i = n
    for inst in thePID: 
        i = i+1
        PID_Name = inst.getAttributeData ("DeviceIdentification:Name")
        RA = inst.getAttributeData ("FEDeviceParameters:Controller Parameters:RA")
        PID_Kc = inst.getAttributeData ("FEDeviceVariables:Default PID Parameters:Kc")
        PID_Ti = inst.getAttributeData ("FEDeviceVariables:Default PID Parameters:Ti")
        PID_SP = inst.getAttributeData ("FEDeviceVariables:Default PID Parameters:Setpoint")
        MV = inst.getAttributeData ("FEDeviceEnvironmentInputs:Measured Value") 
        
        if PID_SP == "":
            PID_SP = 1.0
            
        if PID_Ti == "":
            PID_Ti = 100.0

        if MV <> "":
            #MVType=thePlugin.s7db_id(MV, "AnalogInput, AnalogInputReal, AnalogStatus")
            #MVType = theRawInstances.findMatchingInstances("AnalogInput, AnalogInputReal, AnalogStatus", "'#DeviceIdentification:Name#'='$MV$'")
            MVobject = theRawInstances.findInstanceByName(MV) 
            MVType = MVobject.getDeviceTypeName()
            
            DesiredActuator = 70
            
            if RA == "FALSE":
                if thePlugin.isString(PID_SP):
                    Offset =  PID_SP + ".PosSt * 0.5"
                else:
                    Offset = str(float(PID_SP)*0.5)
                sign = "+"
            else:
                if thePlugin.isString(PID_SP):
                    Offset =  PID_SP + ".PosSt * 1.5"
                else:
                    Offset = str(float(PID_SP)*1.5)
                sign = "-"
            
           
            TimeConstant = "50"

                
            if thePlugin.isString(PID_SP):
                Gain = sign + PID_SP + ".PosSt /" + str(DesiredActuator)
            else:
                Gain = sign + str(float(PID_SP)/DesiredActuator)
                    

            
            thePlugin.writeTIALogic('''
//Calculate the MV $MV$ for $PID_Name$ based on requirement to get $DesiredActuator$ actuator position '''    )

            #if MVType == "":
            
            thePlugin.writeTIALogic(''' // DEBUG MVtype is $MVType$ ''')
            
            if MVType == "AnalogInput":
                thePlugin.writeTIALogic('''
DB$str(i)$.INV := $Gain$ * $PID_Name$.OutOV;
DB$str(i)$.TM_LAG := T#$TimeConstant$s;
DB$str(i)$.CYCLE  := T#1s;
DB$str(i)$();'''    )
 
                thePlugin.writeTIALogic('''
$MV$.HFPos := REAL_TO_INT( INT_TO_REAL($MV$.PMaxRaw-$MV$.PMinRaw) 
        *($Offset$ + DB$str(i)$.OUTV)/($MV$.PMaxRan-$MV$.PMinRan));    '''    )                
            #elif MVType == "DB_AIR_All.AIR_SET.":
            elif MVType == "AnalogInputReal":
                thePlugin.writeTIALogic('''
DB$str(i)$.INV := $Gain$ * $PID_Name$.OutOV;
DB$str(i)$.TM_LAG := T#$TimeConstant$s;
DB$str(i)$.CYCLE  := T#1s;
DB$str(i)$();
//Nominally don't write to AIR because this could be the result of a calculation, and therefore will overwrite the user calculation
(*$MVType$$MV$.HFPos := REAL_TO_INT( $MVType$$MV$.PMaxRaw-$MVType$$MV$.PMinRaw
        *($Offset$ + DB$str(i)$.OUTV)/($MVType$$MV$.PMaxRan-$MVType$$MV$.PMinRan));  *)  '''    )
            else:
                thePlugin.writeTIALogic('''
DB$str(i)$.INV := $PID_Name$.OutOV;
DB$str(i)$.TM_LAG := T#$TimeConstant$s;
DB$str(i)$.CYCLE  := T#1s;
DB$str(i)$();
$MV$.AuPosR := $Offset$ + DB$str(i)$.OUTV;'''    )
