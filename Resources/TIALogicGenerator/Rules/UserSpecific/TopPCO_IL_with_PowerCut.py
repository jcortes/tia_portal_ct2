# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
reload(TIALogic_DefaultAlarms_Template)


def ILLogic(thePlugin, theRawInstances, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)
    
    FB_DB_GLOBAL = Lparam1   # FB name of DB_GLOBAL
    DB_DB_GLOBAL = Lparam2   # DB name of DB_GLOBAL

    if FB_DB_GLOBAL <> "" and DB_DB_GLOBAL <> "":
        thePlugin.writeTIALogic('''
//Compilation of instance DB of DB_GLOBAL
DATA_BLOCK $DB_DB_GLOBAL$ $FB_DB_GLOBAL$
BEGIN
END_DATA_BLOCK
''')

# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeTIALogic('''
FUNCTION $name$_IL : VOID
TITLE = '$name$_IL'
//
// Interlock Logic of $name$
//
//
// Master: 	$name$
// Name: 	Interlock
(*
 Lparam1:	$Lparam1$
 Lparam2:	$Lparam2$
 Lparam3:	$Lparam3$
 Lparam4:	$Lparam4$
 Lparam5:	$Lparam5$
 Lparam6:	$Lparam6$
 Lparam7:	$Lparam7$
 Lparam8:	$Lparam8$
 Lparam9:	$Lparam9$
 Lparam10:  $Lparam10$
*)
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_IL'
FAMILY: 'IL'
VAR_TEMP
    old_status : DWORD;
END_VAR
BEGIN''')

    PowerDI = Lparam4
    Motors = Lparam5
    PowerCutDt = Lparam6
    RestartDt = Lparam7

    # condition to detect a real power cut: All motors are Off and no power
    DIPower = [x.strip() + ".PosSt" for x in PowerDI.split(',')]
    DIPower_Cond = " AND \n".join(DIPower)

    MotorsList = ["NOT " + x.strip() + ".OnSt" for x in Motors.split(',')]
    if Motors <> "":
        Motors_RM_Off_Cond = " AND \n".join(MotorsList)
    else:
        Motors_RM_Off_Cond = "TRUE"

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')
    # POWER CUT MANAGEMENT IL PART (2nd part in GL)
    thePlugin.writeTIALogic('''
(*Gestion reprise apres coupure de courant*)
DB_GLOBAL.PrTension:= $DIPower_Cond$;

//Set LostPowerMem when lose power, to start window to check for retour marche
IF NOT DB_GLOBAL.PrTension AND DB_GLOBAL.old_PrTension  THEN
	DB_GLOBAL.LostPowerMem := TRUE;
	DB_GLOBAL.T_powerCutOn := CPC_GLOBAL_VARS.UNICOS_LiveCounter;
END_IF;

DB_GLOBAL.old_PrTension:=DB_GLOBAL.PrTension;

IF DB_GLOBAL.PrTension AND (CPC_GLOBAL_VARS.UNICOS_LiveCounter - DB_GLOBAL.T_powerCutOn > $PowerCutDt$.PosSt) THEN
    DB_GLOBAL.LostPowerMem := FALSE;       
END_IF;

//Setup the interlock if power is gone for a certain time and rotating machines are "OFF"
IF (DB_GLOBAL.LostPowerMem AND 
$Motors_RM_Off_Cond$) 
OR CPC_GLOBAL_VARS.First_Cycle THEN
    DB_GLOBAL.RealPowerCut:= TRUE;
END_IF;

//Store time when NOT real power cut, OR power is off. Will save value when RealPowerCut and PrTension are TRUE, to calculate delay.
IF (NOT DB_GLOBAL.RealPowerCut OR NOT DB_GLOBAL.PrTension) OR CPC_GLOBAL_VARS.First_Cycle OR (DB_GLOBAL.T_PowerCutOff > CPC_GLOBAL_VARS.UNICOS_LiveCounter) THEN
    DB_GLOBAL.T_PowerCutOff := CPC_GLOBAL_VARS.UNICOS_LiveCounter;
END_IF;

//Reset the interlock IF power is back FOR a certain TIME
IF (CPC_GLOBAL_VARS.UNICOS_LiveCounter - DB_GLOBAL.T_PowerCutOff) > $RestartDt$.PosSt THEN
    DB_GLOBAL.RealPowerCut:= FALSE;
END_IF;
''')

    # Not configured alarms
    # Gets all the Digital Alarms that are child of the 'master' object
    theDigitalAlarms, theDigitalAlarmsMultiple, allTheDigitalAlarms, DAListPosition = TIALogic_DefaultAlarms_Template.getDigitalAlarms(theRawInstances, name)
    # Gets all the Analog Alarms that are child of the 'master' object
    theAnalogAlarms, theAnalogAlarmsMultiple, allTheAnalogAlarms, AAListPosition = TIALogic_DefaultAlarms_Template.getAnalogAlarms(theRawInstances, name)

    #-------------------------------------------------------------------------------------------------------------
    # The "writeNotConfiguredDAParameters" function writes default values for DA parameters:
    #
    # If Input (in spec) is empty, writes IOError, IOSimu and I
    # Else if Input is not empty, IOError, IOSimu and I will be written by main template function
    # If Delay (in spec) is "logic", writes PAlDt
    # Else if Delay is not "logic", PAlDt will be written by main template function
    #
    # NOTE: the user can choose to remove this function call from the user template, in which case he will be
    # responsible for writing the code for the appropriate pins himself
    #
    #-------------------------------------------------------------------------------------------------------------
    TIALogic_DefaultAlarms_Template.writeNotConfiguredDAParameters(thePlugin, theDigitalAlarms, theDigitalAlarmsMultiple, DAListPosition)

    #-------------------------------------------------------------------------------------------------------------
    # The "writeNotConfiguredDAParameters" function writes default values for DA parameters:
    #
    # If Input (in spec) is empty, writes IOError, IOSimu and I
    # Else if Input is not empty, IOError, IOSimu and I will be written by main template function
    # If Delay (in spec) is "logic", writes PAlDt
    # Else if Delay is not "logic", PAlDt will be written by main template function
    #
    # NOTE: the user can choose to remove this function call from the user template, in which case he will be
    # responsible for writing the code for the appropriate pins himself
    #
    #-------------------------------------------------------------------------------------------------------------
    TIALogic_DefaultAlarms_Template.writeNotConfiguredAAParameters(thePlugin, theAnalogAlarms, theAnalogAlarmsMultiple, AAListPosition)

    # Step 1.3: Fill the IOError and IOSimu from the Inputs linked by the user logic (if procceds)
    thePlugin.writeTIALogic('''
(*IoSimu and IoError*****************)
DB_ERROR_SIMU.$name$_IL_E := 0; // To complete

DB_ERROR_SIMU.$name$_IL_S := 0; // To complete
''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
