# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
reload(TIALogic_DefaultAlarms_Template)

import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)

import ucpc_library.shared_grafcet_parsing
reload(ucpc_library.shared_grafcet_parsing)

import ProcessFeatures
reload(ProcessFeatures)

def CLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1,Lparam2,Lparam3,Lparam4,Lparam5,Lparam6,Lparam7,Lparam8,Lparam9,Lparam10=TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)


    # Init external classes -----------------------------------------------------------
    GrafcetParser   = ucpc_library.shared_grafcet_parsing.SharedGrafcetParsing()        
    Decorator       = ucpc_library.shared_decorator.ExpressionDecorator()

    # Load parameters -----------------------------------------------------------------
    DB_DB_GLOBAL      = Lparam2                                                           # Name of the DB
    listOfSteps     = Lparam10.split(',')                     # list of steps of DB_DB_GLOBAL (empty if DB_DB_GLOBAL is "")
    
    
    Features    = Lparam8   # List of optional features
    
    # List of supported features with number of parameters ----------------------------
    SupportedFeatures = {
        'fon':[1,"string"],              # takes: logic to set FOn input
        'foff':[1,"string"],             # takes: logic to set FOff input
        'auonr':[1,"string"],              # does nothing in this file - see PCO_DL
        'auoffr':[1,"string"],             # does nothing in this file - see PCO_DL
        'aucstopr':[1,"string"],           # does nothing in this file - see PCO_DL
        'auopmor':[1,"string"],            # takes: logic to set AuOpMoR input, if no_master
        'auihfomo':[1,"string"],           # does nothing in this file - see PCO_DL
        'auihmmo':[1,"string"],            # does nothing in this file - see PCO_DL
        'cstopfin':[1,"string"]           # takes: logic to set CStopFin input
    }
    
    # Process features ----------------------------------------------------------------
    RequestedFeatures = ProcessFeatures.parseFeatures(Features)                     # parse features
    ProcessFeatures.checkFeatures(name, RequestedFeatures, SupportedFeatures, thePlugin, theRawInstances)  # check corectness of parameters numbers
    
    # Feature: FOn ----------------------------------------------------------------
    FOn = "" # default
    if "fon" in RequestedFeatures:
        FOn = Decorator.decorateExpression(RequestedFeatures['fon'][0], DB_DB_GLOBAL, listOfSteps)


    # Feature: FOff ----------------------------------------------------------------
    FOff = "" # default
    if "foff" in RequestedFeatures:
        FOff = Decorator.decorateExpression(RequestedFeatures['foff'][0], DB_DB_GLOBAL, listOfSteps)

    # Handle FOn and FOff ---------------------------------------------------------
    if not FOn and not FOff:
        thePlugin.writeWarningInUABLog("OnOff $name$: No condition for FOn nor for FOff provided! Assumed: FOn := $name$.RunOSt, FOff := NOT $name$.FOn")
        FOn = "$name$.RunOSt"
        FOff = "NOT $name$.FOn"
    else:
        if not FOff:        # if off condition not provided, make it complementary to On condition
            FOff = '''NOT $name$.FOn'''
        else:
            FOff = '''$FOff$'''
    
        if not FOn:         # if on conditions not provided, make it complementary to off condition
            FOn = '''NOT ($FOff$)'''
        else:
            FOn = '''$FOn$'''
    

# Step 1: Create the FUNCTION called PCOName_SectionName. 

    thePlugin.writeTIALogic('''
FUNCTION $name$_CL : VOID
TITLE = '$name$_CL'
//
// Configuration Logic of  $name$
(*
 Lparam1:   $Lparam1$
 Lparam2:   $Lparam2$   // Name of the DB of the DB_GLOBAL 
 Lparam3:   $Lparam3$
 Lparam4:   $Lparam4$
 Lparam5:   $Lparam5$
 Lparam6:   $Lparam6$
 Lparam7:   $Lparam7$
 Lparam8:   $Lparam8$   // List of optional features
 Lparam9:   $Lparam9$
 Lparam10:  $Lparam10$
*)
AUTHOR: 'ICE/PLC'
NAME: 'Logic_CL'
FAMILY: 'Common'
BEGIN
  		 
''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')	

    # Step 1.1: No master
    if master == name:
        thePlugin.writeTIALogic('''(* No master defined (master of masters) *)
$name$.IhAuMRW := 1;

''')

    # Step 1.2: Feedback ON
    thePlugin.writeTIALogic('''(*Feedback ON**********************)
$name$.FOn := $FOn$;

''')
    # Step 1.3: Feedback OFF
    thePlugin.writeTIALogic('''(*Feedback OFF***********************)
$name$.FOff := $FOff$;

''')
    # Feature: CStopFin ----------------------------------------------------------------
    CStopFin = "0" # default
    if "cstopfin" in RequestedFeatures:
        CStopFin = Decorator.decorateExpression(RequestedFeatures['cstopfin'][0], DB_DB_GLOBAL, listOfSteps)
    # Step 1.4: Control Stop Finished
    thePlugin.writeTIALogic('''(*Control Stop Finished************)
$name$.CStopFin := $CStopFin$;

''')
    # Feature: AuOpMoR ----------------------------------------------------------------
    AuOpMoR = "1.0" # default
    if "auopmor" in RequestedFeatures:
        AuOpMoR = Decorator.decorateExpression(RequestedFeatures['auopmor'][0], DB_DB_GLOBAL, listOfSteps)
    # Step 1.5: Option Mode
    if master == name or master.lower() == "no_master":
        thePlugin.writeTIALogic('''(*Option Mode*************************)
$name$.AuOpMoR := $AuOpMoR$; // To complete

''')
    # Step 1.6: Warnings of CL Section
    thePlugin.writeTIALogic('''(*WArnings of CL Section**************)
DB_ERROR_SIMU.$name$_CL_E := 0 ; // To complete
DB_ERROR_SIMU.$name$_CL_S := 0 ; // To complete


''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
