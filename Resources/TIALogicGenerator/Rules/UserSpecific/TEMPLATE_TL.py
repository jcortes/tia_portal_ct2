# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
reload(TIALogic_DefaultAlarms_Template)


def TLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    # Template for COOL_EAST_AREA

    DB_DB_GLOBAL = Lparam2
    DB_Global = Lparam3

# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeTIALogic('''
FUNCTION $name$_TL : VOID
TITLE = '$name$_TL'
//
// Transition Logic of $name$
//
// Lparam1:	$Lparam1$	// FB name of DB_GLOBAL
// Lparam2:	$Lparam2$	// DB name of DB_GLOBAL
// Lparam3:	$Lparam3$	// Global DB Name
// Lparam4:	$Lparam4$	
// Lparam5:	$Lparam5$	
// Lparam6:	$Lparam6$	
// Lparam7:	$Lparam7$	
// Lparam8:	$Lparam8$	
// Lparam9:	$Lparam9$	
// Lparam10:$Lparam10$	
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_TL'
FAMILY: 'TL'

VAR_TEMP
    pco_transitions1 : WORD;
    pco_transitions1_bit AT pco_transitions1: ARRAY [0..15] OF BOOL;  
    
    //pco_transitions2 : WORD;
    //pco_transitions2_bit AT pco_transitions2: ARRAY [0..15] OF BOOL;  
END_VAR

BEGIN
''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------

$DB_DB_GLOBAL$.GOTO_FAULT             :=  NOT $name$.EnRStartSt;
$DB_DB_GLOBAL$.FAULT_TO_STOP          :=  $name$.EnRStartSt;
$DB_DB_GLOBAL$.STOP_TO_STARTING       :=  $name$.RunOSt;
$DB_DB_GLOBAL$.GOTO_STOP              :=  NOT $name$.RunOSt;


pco_transitions1:= 16#0;
pco_transitions1_bit[8]  := $DB_DB_GLOBAL$.GOTO_FAULT;            (* 0*)
pco_transitions1_bit[9]  := $DB_DB_GLOBAL$.FAULT_TO_STOP;         (* 1*)
pco_transitions1_bit[10] := $DB_DB_GLOBAL$.STOP_TO_STARTING;      (* 2*)
pco_transitions1_bit[11] := $DB_DB_GLOBAL$.GOTO_STOPPING;         (* 3*)
pco_transitions1_bit[12] := $DB_DB_GLOBAL$.TRANSITIONx;           (* 4*)
pco_transitions1_bit[13] := false;                              (* 5*)
pco_transitions1_bit[14] := false;                              (* 6*)
pco_transitions1_bit[15] := false;                              (* 7*)
pco_transitions1_bit[0]  := false;                              (* 8*)
pco_transitions1_bit[1]  := false;                              (* 9*)
pco_transitions1_bit[2]  := false;                              (*10*)
pco_transitions1_bit[3]  := false;                              (*11*)
pco_transitions1_bit[4]  := false;                              (*12*)
pco_transitions1_bit[5]  := false;                              (*13*)
pco_transitions1_bit[6]  := false;                              (*14*)
pco_transitions1_bit[7]  := false;                              (*15*)

pco_transitions2:= 16#0;
pco_transitions2_bit[8]  := false;                              (* 0*)
pco_transitions2_bit[9]  := false;                              (* 1*)
pco_transitions2_bit[10] := false;                              (* 2*)
pco_transitions2_bit[11] := false;                              (* 3*)
pco_transitions2_bit[12] := false;                              (* 4*)
pco_transitions2_bit[13] := false;                              (* 5*)
pco_transitions2_bit[14] := false;                              (* 6*)
pco_transitions2_bit[15] := false;                              (* 7*)


$name$_Tr1.AuPosR := pco_transitions1;
$name$_Tr2.AuPosR := pco_transitions2;
$name$_STATUS.AuPosR := INT_TO_WORD($DB_DB_GLOBAL$.S_NO);

DB_ERROR_SIMU.$name$_TL_E := 0 ; // To complete
DB_ERROR_SIMU.$name$_TL_S := 0 ; // To complete

''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
