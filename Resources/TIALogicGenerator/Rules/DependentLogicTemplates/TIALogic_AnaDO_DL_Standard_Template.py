# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)


def AnaDOLogic(thePlugin, theUnicosProject, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    thePlugin.writeTIALogic('''
FUNCTION $name$_DL : VOID
TITLE = '$name$_DL'
//
// Dependent Logic of $name$
//
//
// Master: 	$master$
// Name: 	$name$
// Lparam1:	$Lparam1$
// Lparam2:	$Lparam2$
// Lparam3:	$Lparam3$
// Lparam4:	$Lparam4$
// Lparam5:	$Lparam5$
// Lparam6:	$Lparam6$
// Lparam7:	$Lparam7$
// Lparam8:	$Lparam8$
// Lparam9:	$Lparam9$
// Lparam10:	$Lparam10$
//
AUTHOR: 'UNICOS'
NAME: 'Logic_DL'
FAMILY: 'AnaDO'
''')

    # Step 1.1: Create all the variable that we're using on this Function
    thePlugin.writeTIALogic('''
VAR_TEMP
   old_status: DWORD;
   IN 	: REAL; //Scaling input
   OUT 	: REAL; //Scaling Output, connected to AuPosR
   In_Min	: REAL;
   In_Max 	: REAL;
   Out_Min : REAL;
   Out_Max : REAL;
END_VAR
BEGIN
''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    # Step 1.2: Scaling and obtaining the Output to drive the AnaDO device
    # Gets all the controller objects that are child of the AnaDO object with some properties
    AllControllers = theUnicosProject.findMatchingInstances("Controller", "'#FEDeviceOutputs:Controlled Objects#' != ''")
    theControllerObjects = ArrayList()
    theNumberOfControllers = 0
    theNumberOfControlledObjects = 0

    for PID in AllControllers:
        PIDName = PID.getAttributeData("DeviceIdentification:Name").replace(",", " ")
        ControlledObjectNames = PID.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ")
        ControlledObjectList = ControlledObjectNames.split()
        for ControlledObjectName in ControlledObjectList:
            if (ControlledObjectName == name):
                theNumberOfControllers = theNumberOfControllers + 1
                theControllerObjects.add(PIDName)
                ScalingMethod = PID.getAttributeData("FEDeviceParameters:Controller Parameters:Scaling Method")
                for i in ControlledObjectList:
                    theNumberOfControlledObjects = theNumberOfControlledObjects + 1
    theNumberOfControllers = len(theControllerObjects)

    # Gets the RangeMax and RangeMin for the current AnaDO
    instVector = theUnicosProject.findMatchingInstances("AnaDO", "'#DeviceIdentification:Name#'='$name$'")
    ProcessOutput = ""
    for inst in instVector:
        ProcessOutput = inst.getAttributeData("FEDeviceOutputs:Analog Process Output")
    inst_ProcessOutput_Vec = theUnicosProject.findMatchingInstances("AnalogOutput, AnalogOutputReal", "'#DeviceIdentification:Name#'='$ProcessOutput$'")
    RangeMax = "100.0"
    RangeMin = "0.0"
    if (ProcessOutput <> ""):
        for inst_ProcessOutput in inst_ProcessOutput_Vec:
            RangeMin = thePlugin.formatNumberPLC(inst_ProcessOutput.getAttributeData("FEDeviceParameters:Range Min"))
            RangeMax = thePlugin.formatNumberPLC(inst_ProcessOutput.getAttributeData("FEDeviceParameters:Range Max"))

    # AuOnR/AuOffR of AnaDO
    if master.lower() <> "no_master":
        thePlugin.writeTIALogic('''
(*Position Management*)
$name$.AuOnR:= 0; // $master$.RunOSt AND ... To complete
$name$.AuOffR:= 0; // NOT $master$.RunOSt OR ... To complete
	''')
    else:
        thePlugin.writeTIALogic('''
(*Position Management*)
$name$.AuOnR := FALSE;// To complete
$name$.AuOffR := FALSE;// To complete
	''')

    # If there is not controller driving this AnaDO (means No Scaling), the AuPosR = RangeMax if $master$.RunOSt = TRUE, if not AuPosR = Range Min
    if theNumberOfControllers == 0:
        if master.lower() <> "no_master":
            thePlugin.writeTIALogic('''
// There is not controller driving the AnaDO Object $name$
// Default value. To complete.
IF $master$.RunOSt = TRUE AND 0 Then
   $name$.AuPosR := $RangeMax$; 
ELSE
   $name$.AuPosR := $RangeMin$; 
END_IF;

''')
        else:
            thePlugin.writeTIALogic('''
// There is not controller driving the AnaDO Object $name$ and this AnaDO has not master
$name$.AuPosR := 0.0; // To complete
''')

    # If there is only 1 controller driving this AnaDO, analyze the theNumberOfControllers, the theNumberOfControlledObjects and the ScalingMethod
    elif theNumberOfControllers == 1:
        if theNumberOfControlledObjects == 1:
            if ScalingMethod == "Input Scaling":
                thePlugin.writeTIALogic('''
// The AnaDO object "$name$" is controlled by 1 regulator ($theControllerObjects[0]$) and the user selected "Input Scaling"
IN := $theControllerObjects[0]$.OutOV;
(*Scaling Parameters**************) 
//Values to change if SPlit Range 
In_Min	:= 0.0;
In_Max 	:= 100.0;
Out_Min := $RangeMin$;
Out_Max := $RangeMax$;
OUT := (IN-In_Min) * (Out_Max-Out_Min)/(In_Max-In_Min) + Out_Min;  //Scaling
IF OUT > Out_Max Then 
	OUT := Out_Max; //High Limitation
END_IF;	
IF OUT < Out_Min Then 
	OUT := Out_Min; //Low Limitation
END_IF;	
// The OUT value is linked to the AuPosR
$name$.AuPosR:= OUT;
''')
            else:
                thePlugin.writeTIALogic('''
// The AnaDO object "$name$" is controlled by 1 regulator ($theControllerObjects[0]$) and the user selected "$ScalingMethod$"
$name$.AuPosR:= $theControllerObjects[0]$.OutOV;
''')
        else:
            thePlugin.writeTIALogic('''
// The AnaDO object "$name$" is controlled by 1 regulator ($theControllerObjects[0]$) and there are several controlled objects (as the $name$)
IN := $theControllerObjects[0]$.OutOV;
(*Scaling Parameters**************) 
//Values to change if SPlit Range 
In_Min	:= 0.0; // To complete
In_Max 	:= 100.0; // To complete
Out_Min := $RangeMin$;
Out_Max := $RangeMax$;
OUT := (IN-In_Min) * (Out_Max-Out_Min)/(In_Max-In_Min) + Out_Min;  //Scaling
IF OUT > Out_Max Then 
	OUT := Out_Max; //High Limitation
END_IF;	
IF OUT < Out_Min Then 
	OUT := Out_Min; //Low Limitation
END_IF;	
// The OUT value is linked to the AuPosR
$name$.AuPosR:= OUT;
''')
        thePlugin.writeTIALogic('''
// The AuActR of the regulator is activated
$theControllerObjects[0]$.AuActR:= TRUE;
''')
    # If there are several controllers driving this AnaDO, there is not Scaling and the user has to fix the conditions
    else:
        thePlugin.writeTIALogic('''
// The AnaDO object "$name$" is controlled by ''' + str(theNumberOfControllers) + ''' regulators:
''')
        for controllerName in theControllerObjects:
            thePlugin.writeTIALogic('''// $controllerName$
''')
        thePlugin.writeTIALogic('''// The user must select the condition for the AuPosR
$name$.AuPosR:= 0.0; // To complete
// The user must select the condition for the AuActR
''')
        for controllerName in theControllerObjects:
            thePlugin.writeTIALogic('''$controllerName$.AuActR:= TRUE; // To complete
''')

    # Step 1.3: Ramp Parameters
    for inst in instVector:
        ManualDecreaseSpeed = inst.getAttributeData("FEDeviceParameters:Manual Decrease Speed (Unit/s)")
        ManualIncreaseSpeed = inst.getAttributeData("FEDeviceParameters:Manual Increase Speed (Unit/s)")

    if ManualDecreaseSpeed.strip() == "":
        ManualDecreaseSpeed = '10'
    if ManualIncreaseSpeed.strip() == "":
        ManualIncreaseSpeed = '10'

    thePlugin.writeTIALogic('''
// Ramp Parameters to fill by the user. By default the "Manual Decrease Speed" and "Manual Increase Speed" are taken
$name$.AuDeSpd := $ManualDecreaseSpeed$; // To complete
$name$.AuInSpd := $ManualIncreaseSpeed$; // To complete
''')

    # Step 1.5: Fill the IOError and IOSimu from the Inputs linked by the user logic (if procceds)
    thePlugin.writeTIALogic('''

(*IoSimu and IoError*****************)
// The user must connect the IOError and IOSimu from the linked devices ("IN" variable) if proceeds
DB_ERROR_SIMU.$name$_DL_E := 0; // To complete

DB_ERROR_SIMU.$name$_DL_S :=  0; // To complete
''')

    # Not configured alarms
    # Gets all the Digital Alarms that are child of the 'master' object
    theDigitalAlarms, theDigitalAlarmsMultiple, allTheDigitalAlarms, DAListPosition = TIALogic_DefaultAlarms_Template.getDigitalAlarms(theUnicosProject, name)
    # Gets all the Analog Alarms that are child of the 'master' object
    theAnalogAlarms, theAnalogAlarmsMultiple, allTheAnalogAlarms, AAListPosition = TIALogic_DefaultAlarms_Template.getAnalogAlarms(theUnicosProject, name)

    #-------------------------------------------------------------------------------------------------------------
    # The "writeNotConfiguredDAParameters" function writes default values for DA parameters:
    #
    # If Input (in spec) is empty, writes IOError, IOSimu and I
    # Else if Input is not empty, IOError, IOSimu and I will be written by main template function
    # If Delay (in spec) is "logic", writes PAlDt
    # Else if Delay is not "logic", PAlDt will be written by main template function
    #
    # NOTE: the user can choose to remove this function call from the user template, in which case he will be
    # responsible for writing the code for the appropriate pins himself
    #
    #-------------------------------------------------------------------------------------------------------------
    TIALogic_DefaultAlarms_Template.writeNotConfiguredDAParameters(thePlugin, theDigitalAlarms, theDigitalAlarmsMultiple, DAListPosition)

    #-------------------------------------------------------------------------------------------------------------
    # The "writeNotConfiguredAAParameters" function writes default values for AA parameters:
    #
    # If HH Alarm (in spec) is a number, writes AuEHH. In this case HH will be written by main template function
    # If HH Alarm is "logic", writes AuEHH and HH
    # If HH Alarm is a reference to an object, writes AuEHH. In this case HH will be written by main template function
    # NOTE: the same applies for H Warning, L Warning, and LL Alarm
    # If Input (in spec) is empty, writes IOError, IOSimu and I
    # If Delay (in spec) is "logic", writes PAlDt
    #
    # NOTE: the user can choose to remove this function call from the user template, in which case he will be
    # responsible for writing the code for the appropriate pins himself
    #
    #-------------------------------------------------------------------------------------------------------------
    TIALogic_DefaultAlarms_Template.writeNotConfiguredAAParameters(thePlugin, theAnalogAlarms, theAnalogAlarmsMultiple, AAListPosition)

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
