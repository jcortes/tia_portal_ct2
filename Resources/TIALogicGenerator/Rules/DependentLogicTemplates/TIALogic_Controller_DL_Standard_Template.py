# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)


def ControllerLogic(thePlugin, theUnicosProject, master, name, LparamVector, MasterControllerName, IncreaseSpeed, DecreaseSpeed, MeasuredValue):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    Def_SP = thePlugin.formatNumberPLC(theUnicosProject.DlookupString("FEDeviceVariables:Default PID Parameters:Setpoint", "Controller", "'#DeviceIdentification:Name#' = '$name$'"))

    thePlugin.writeTIALogic('''
FUNCTION $name$_DL : VOID
TITLE = '$name$_DL'
//
// Dependent Logic of $name$
//
//
// Master: 	$master$
// Name: 	$name$
// Lparam1:	$Lparam1$
// Lparam2:	$Lparam2$
// Lparam3:	$Lparam3$
// Lparam4:	$Lparam4$
// Lparam5:	$Lparam5$
// Lparam6:	$Lparam6$
// Lparam7:	$Lparam7$
// Lparam8:	$Lparam8$
// Lparam9:	$Lparam9$
// Lparam10:	$Lparam10$
//
AUTHOR: 'UNICOS'
NAME: 'Logic_DL'
FAMILY: 'PID'
''')

    # Step 1.1. Create all the variable that we're using on this Function
    thePlugin.writeTIALogic('''	
VAR_TEMP
   par_$name$ : BOOL;
	
	
   IN 	: REAL; //Scaling input
   OUT 	: REAL; //Scaling Output, connected to AuPosR
   In_Min	: REAL;
   In_Max 	: REAL;
   Out_Min : REAL;
   Out_Max : REAL;	
	
END_VAR
BEGIN
''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')
    # Step 1.2: Setting the "Auto Parameter Restore" (AuPRest). Set the RE_RunOSt or the FE_RunOSt of his master into the AuPRest of the current control Object
    if master.lower() <> "no_master":
        thePlugin.writeTIALogic('''
(*Parameters**********)	
par_$name$:= $master$.RE_RunOSt OR $master$.FE_RunOSt;
(*$name$.AuPRest := par_$name$;*)
''')

    # Step 1.3: Setpoint management:
    # Step 1.3.1: Set the AuESP (The control logic enables of the auto set-Point) to 1 if the RE_RunOSt or FE_RunOSt or RE_AuDeSt of his master are active
    # Step 1.3.2: Set the AuRegR (The control logic requests the regulation working state) to 1 if the RunOSt of his master is active
    # Step 1.3.3: Set the AuPosR (Control logic Position value request) to 1 if the RunOSt of his master is active
    # Step 1.3.4: Setpoint speed change. Set the InSpd (The control logic set Increase Speed of Setpoint (SP unit/s)) and the DeSpd (The control logic set Decrease Speed of Setpoint (SP unit/s))

    # cascade with master

    # Make cascade if master controller exists
    if (MasterControllerName <> ""):
        SetPoint = "$name$.AuSPR := $MasterControllerName$.OutOV;"
        AuESP = "$name$.AuESP := $MasterControllerName$.RegSt;"
    elif not thePlugin.isString(Def_SP):
        SetPoint = "$name$.AuSPR := $name$.DefSP;  (*To Complete*)"
        if "$master$" <> "NO_Master":
            AuESP = "$name$.AuESP := FALSE; (*To Complete*)"
        else:
            AuESP = "$name$.AuESP := FALSE;  (*To Complete*)"
    else:
        AuESP = ""
        SetPoint = ""

    if master.lower() <> "no_master" and MasterControllerName <> "":

        thePlugin.writeTIALogic('''
// Setpoint management 
// AuESP and AuSPR
$AuESP$
$SetPoint$

// AuRegR and AuOutPR
IF $MasterControllerName$.RegSt THEN
   $name$.AuRegR := 1; //SET
ELSE 
  $name$.AuRegR := 0;  //RESET
END_If;  
$name$.AuOutPR := NOT $name$.AuRegR;

// AuPosR To complete
IF $master$.RunOSt THEN
   $name$.AuPosR := 0 ;	
ELSE
   $name$.AuPosR := 0 ;	
END_IF;
	
// Setpoint speed change
$name$.AuSPSpd.InSpd := $IncreaseSpeed$; 
$name$.AuSPSpd.DeSpd := $DecreaseSpeed$; 	
''')

    # No cascade with master
    elif master.lower() <> "no_master":
        thePlugin.writeTIALogic('''
// Setpoint management 
// AuESP and AuSPR
$AuESP$
$SetPoint$

// AuRegR and AuOutPR To complete
IF $master$.RunOSt AND 0 THEN 
   $name$.AuRegR := 1; //SET
ELSE 
  $name$.AuRegR := 0;  //RESET
END_If;  
$name$.AuOutPR := NOT $name$.AuRegR;

// AuPosR To complete
IF $master$.RunOSt THEN
   $name$.AuPosR := 0 ;	
ELSE 
   $name$.AuPosR := 0 ;	
END_IF;
	
// Setpoint speed change
$name$.AuSPSpd.InSpd := $IncreaseSpeed$; 
$name$.AuSPSpd.DeSpd := $DecreaseSpeed$; 

// Example Automatic setPoint setting (by the logic)
// $name$.AuSPR:=2.34;  
// $name$.AuESP:=par_$name$;
    
''')

    # No master
    else:
        thePlugin.writeTIALogic('''

// Setpoint management 
// AuESP and AuSPR
$AuESP$
$SetPoint$

// AuRegR and AuOutPR
$name$.AuRegR := 0; //RESET
$name$.AuOutPR := NOT $name$.AuRegR;

// AuPosR To complete
$name$.AuPosR := 0 ;	

// Setpoint speed change
$name$.AuSPSpd.InSpd := $IncreaseSpeed$; 
$name$.AuSPSpd.DeSpd := $DecreaseSpeed$; 

''')

    # Step 1.4: Driving the controller. HMV (Hardware Measured value. Feedback Measured Value of the controlled process.)
    thePlugin.writeTIALogic('''
(*Driving***********)
''')
    if MeasuredValue <> "":
        thePlugin.writeTIALogic('''$name$.HMV := ''' + MeasuredValue + '''.PosSt;
''')
    else:
        thePlugin.writeTIALogic('''$name$.HMV := 0.0 ; // to complete
''')

    thePlugin.writeTIALogic('''
DB_ERROR_SIMU.$name$_DL_E := 0 ; // To complete
DB_ERROR_SIMU.$name$_DL_S := 0 ; // To complete
''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
