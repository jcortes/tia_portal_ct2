# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from java.util import Vector
from time import strftime
import TIALogic_ProcessControlObject_DL_Standard_Template
import TIALogic_DefaultAlarms_Template
import sys


class ProcessControlObject_DL_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython for PCO.")
        reload(TIALogic_ProcessControlObject_DL_Standard_Template)
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        # reload(TIALogic_DefaultAlarms_Template)
        # reload(sys)

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for PCO.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for PCO.")
        dateAndTime = strftime("%Y-%m-%d %H:%M:%S")  # '2007-03-03 22:14:39'

    def process(self, *params):
        theCurrentPco = params[0]
        theCurrentDevice = params[1]
        name = theCurrentDevice.getDeviceName()
        callUserTemplate = str(params[2])
        userTemplateName = str(params[3])
        name = theCurrentDevice.getDeviceName()
        self.thePlugin.writeInUABLog("processInstances in Jython for PCO for the $name$.")

        # General Steps for Dependent Logic templates:
        # 1. Create the FUNCTION called DeviceName_DL. Inside this function we must include the next steps:
        #	1.1. Position Request calculation. The user should complet this part according the logic process. By default we put that to "0".
        #   1.2. IoSimu and IoError: both for DA and AA. We pass the IOError and the IOSimu from the Input associated to the DB_ERROR_SIMU created

        # 1. Create the FUNCTION called DeviceName_DL.
        master = theCurrentPco.getDeviceName()
        ApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
        instVector = self.theUnicosProject.findMatchingInstances("ProcessControlObject", "'#DeviceIdentification:Name#'='$name$'")
        if (instVector.size() == 0):
            self.thePlugin.writeErrorInUABLog("The instance $name$ does not exist in the specification file.")
            self.thePlugin.writeInfoInUABLog("Reload the specification file by clicking on the Reload Specs. button in the wizard.")
            return
        for inst in instVector:
            description = inst.getAttributeData("DeviceDocumentation:Description")
            LparamVector = TIALogic_DefaultAlarms_Template.getLparameters(inst)

        self.thePlugin.writeTIALogic('''(*Device Logic of $name$ ($description$) ********** Application: $ApplicationName$ *****)
	''')

        # call the standard template or the custom one
        if callUserTemplate == "true":
            eval(userTemplateName).ProcessControlObjectLogic(self.thePlugin, self.theUnicosProject, master, name, LparamVector)
        else:
            TIALogic_ProcessControlObject_DL_Standard_Template.ProcessControlObjectLogic(self.thePlugin, self.theUnicosProject, master, name, LparamVector)

        self.thePlugin.writeTIALogic('''

END_FUNCTION''')

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for PCO.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for PCO.")
