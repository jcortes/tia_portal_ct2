# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from java.util import ArrayList
from java.util import Vector
from time import strftime
import TIALogic_MassFlowController_DL_Standard_Template
import TIALogic_DefaultAlarms_Template
import sys


class MassFlowController_DL_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython for MassFlowController.")
        # reload(TIALogic_DefaultAlarms_Template)
        reload(TIALogic_MassFlowController_DL_Standard_Template)
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        # reload(sys)

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for MassFlowController.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for MassFlowController.")
        dateAndTime = strftime("%Y-%m-%d %H:%M:%S")  # '2007-03-03 22:14:39'

    def process(self, *params):
        self.thePlugin.writeInUABLog("the object MassFlowController has not been develop for the Siemens Platform. The logic file will be empty.")

        # call the standard template or the custom one
        if callUserTemplate == "true":
            eval(userTemplateName).MassFlowControllerLogic(self.thePlugin)
        else:
            TIALogic_MassFlowController_DL_Standard_Template.MassFlowControllerLogic(self.thePlugin)

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for MassFlowController.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for MassFlowController.")
