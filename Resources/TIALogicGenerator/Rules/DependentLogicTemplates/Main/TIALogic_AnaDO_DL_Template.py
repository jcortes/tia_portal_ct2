# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from java.util import ArrayList
from java.util import Vector
from time import strftime
import TIALogic_AnaDO_DL_Standard_Template
import TIALogic_DefaultAlarms_Template
import sys


class AnaDO_DL_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython for AnaDO.")
        reload(TIALogic_AnaDO_DL_Standard_Template)
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        # reload(TIALogic_DefaultAlarms_Template)
        # reload(sys)

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for AnaDO.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for AnaDO.")
        dateAndTime = strftime("%Y-%m-%d %H:%M:%S")  # '2007-03-03 22:14:39'

    def process(self, *params):
        theCurrentPco = params[0]
        theCurrentDevice = params[1]
        callUserTemplate = str(params[2])
        userTemplateName = str(params[3])
        name = theCurrentDevice.getDeviceName()
        self.thePlugin.writeInUABLog("processInstances in Jython for AnaDO for the $name$.")

        # General Steps for Dependent Logic templates:
        # 1. Create the FUNCTION called DeviceName_DL. Inside this function we must include the next steps:
        #	1.1. Create all the variable that we're using on this Function
        #	1.2. Scaling and obtaining the Output to drive the AnaDO device
        #	1.3. Ramp Parameters
        #	1.4. Interlock Conditions (both for analog and digital alarms) and IOError/IOSimu for the Alarms (both for analog and digital alarms)
        #	1.5. Fill the IOError and IOSimu from the Inputs linked by the user logic (if procceds)
        #   1.6. IoSimu and IoError: Adding of the IoError from the Logic related to the AnaDO object
        #   1.7. Instantiation of the AuAlAck for the DigitalAlarm objects related to the AnaDO object
        #   1.8. Interlock: Both for DA and AA (SI, FS, TS, AL)
        #   1.9. Blocked Alarm warning: both for DA and AA
        #   1.10. No master

        # 1. Create the FUNCTION called DeviceName_DL.
        # Step 1.1: Create all the variable that we're using on this Function
        master = theCurrentPco.getDeviceName()
        ApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
        instVector = self.theUnicosProject.findMatchingInstances("AnaDO", "'#DeviceIdentification:Name#'='$name$'")
        if (instVector.size() == 0):
            self.thePlugin.writeErrorInUABLog("The instance $name$ does not exist in the specification file.")
            self.thePlugin.writeInfoInUABLog("Reload the specification file by clicking on the Reload Specs. button in the wizard.")
            return
        for inst in instVector:
            LparamVector = TIALogic_DefaultAlarms_Template.getLparameters(inst)
            description = inst.getAttributeData("DeviceDocumentation:Description")

        self.thePlugin.writeTIALogic('''(*Device Logic of $name$ ($description$) ********** Application: $ApplicationName$ *****)
''')

        # Gets all the Digital Alarms that are children of the current object
        theDigitalAlarms, theDigitalAlarmsMultiple, allTheDigitalAlarms, DAListPosition = TIALogic_DefaultAlarms_Template.getDigitalAlarms(self.theUnicosProject, name)

        # Gets all the Analog Alarms that are children of the current object
        theAnalogAlarms, theAnalogAlarmsMultiple, allTheAnalogAlarms, AAListPosition = TIALogic_DefaultAlarms_Template.getAnalogAlarms(self.theUnicosProject, name)

        # Gets all the "FS Alarms" that are children of the current object.
        theDAFsAlarms, theDAFsAlarmsMultiple, allTheDAFsAlarms, theAAFsAlarms, theAAFsAlarmsMultiple, allTheAAFsAlarms = TIALogic_DefaultAlarms_Template.getFsAlarms(self.theUnicosProject, name)

        # Gets all the "TS Alarms" that are children of the current object.
        theDATsAlarms, theDATsAlarmsMultiple, allTheDATsAlarms, theAATsAlarms, theAATsAlarmsMultiple, allTheAATsAlarms = TIALogic_DefaultAlarms_Template.getTsAlarms(self.theUnicosProject, name)

        # Gets all the "SI Alarms" that are children of the current object.
        theDASiAlarms, theDASiAlarmsMultiple, allTheDASiAlarms, theAASiAlarms, theAASiAlarmsMultiple, allTheAASiAlarms = TIALogic_DefaultAlarms_Template.getSiAlarms(self.theUnicosProject, name)

        # Gets all the "AL Alarms" that are children of the current object.
        theDAAlAlarms, theDAAlAlarmsMultiple, allTheDAAlAlarms, theAAAlAlarms, theAAAlAlarmsMultiple, allTheAAAlAlarms = TIALogic_DefaultAlarms_Template.getAlAlarms(self.theUnicosProject, name)

        # call the standard template or the custom one
        if callUserTemplate == "true":
            eval(userTemplateName).AnaDOLogic(self.thePlugin, self.theUnicosProject, master, name, LparamVector)
        else:
            TIALogic_AnaDO_DL_Standard_Template.AnaDOLogic(self.thePlugin, self.theUnicosProject, master, name, LparamVector)

        # Configured parameters of alarms
        TIALogic_DefaultAlarms_Template.writeConfiguredDAParameters(self.thePlugin, theDigitalAlarms, theDigitalAlarmsMultiple, DAListPosition)
        TIALogic_DefaultAlarms_Template.writeConfiguredAAParameters(self.thePlugin, self.theUnicosProject, theAnalogAlarms, theAnalogAlarmsMultiple, AAListPosition)

        # Step 1.6. IoSimu and IoError: Adding of the IoError from the Logic related to the current object
        TIALogic_DefaultAlarms_Template.logicErrorSimuAssignment(self.thePlugin, self.theUnicosProject, allTheDigitalAlarms, allTheAnalogAlarms, name)

        # Step 1.7: Instantiation of the AuAlAck for the Alarm objects related to the current object
        TIALogic_DefaultAlarms_Template.AuAlAckAlarmsMethod(self.thePlugin, self.theUnicosProject, name, master, theDigitalAlarms, theDigitalAlarmsMultiple, theAnalogAlarms, theAnalogAlarmsMultiple, DAListPosition, AAListPosition)

        # Step 1.8: Interlock: Both for DA and AA (SI, FS, TS, AL)
        TIALogic_DefaultAlarms_Template.interlockAlarmsMethod(self.thePlugin, self.theUnicosProject, allTheDASiAlarms, allTheAASiAlarms, allTheDAFsAlarms, allTheAAFsAlarms, allTheDATsAlarms, allTheAATsAlarms, allTheDAAlAlarms, allTheAAAlAlarms, name)

        # Step 1.9: Blocked Alarm warning: both for DA and AA
        TIALogic_DefaultAlarms_Template.blockAlarmsMethod(self.thePlugin, self.theUnicosProject, allTheDigitalAlarms, allTheAnalogAlarms, name)

        # Step 1.10: No master
        if master.lower() == "no_master":
            self.thePlugin.writeTIALogic('''(* No master defined (master of masters) *)
$name$.IhAuMRW := 1;
''')

        self.thePlugin.writeTIALogic('''

END_FUNCTION''')

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for AnaDO.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for AnaDO.")
