# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from java.util import Vector
from time import strftime
import TIALogic_Controller_DL_Standard_Template
import TIALogic_DefaultAlarms_Template
import sys


class Controller_DL_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython for CONTROLLER.")
        reload(TIALogic_Controller_DL_Standard_Template)
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        # reload(TIALogic_DefaultAlarms_Template)
        # reload(sys)

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for CONTROLLER.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for CONTROLLER.")
        dateAndTime = strftime("%Y-%m-%d %H:%M:%S")  # '2007-03-03 22:14:39'

    def process(self, *params):
        theCurrentPco = params[0]
        theCurrentDevice = params[1]
        callUserTemplate = str(params[2])
        userTemplateName = str(params[3])
        name = theCurrentDevice.getDeviceName()
        self.thePlugin.writeInUABLog("processInstances in Jython for Controller for the $name$.")

        # General Steps for Dependent Logic templates:
        # 1. Create the FUNCTION called DeviceName_DL. Inside this function we must include the next steps:
        #	1.1. Create all the variable that we're using on this Function
        #	1.2. Setting the "Auto Parameter Restore" (AuPRest). Set the RE_RunOSt or the FE_RunOSt of his master into the AuPRest of the current control Object
        #	1.3. Setpoint management
        #	1.4. Driving the controller. HMV (Hardware Measured value. Feedback Measured Value of the controlled process.)
        #	1.5. Tracking the controller. HOutO (Hardware Output Order.Feedback position of controlled object.)
        #   1.6. IoSimu and IoError
        # 	1.7. Run the CPC_FB_PID Function Block from the Baseline
        # 	1.8. Copy the status of the current Controller to global status DB
        # 	1.9. No master

        # Step 1: Create the FUNCTION called DeviceName_DL.
        master = theCurrentPco.getDeviceName()

        recordNumber = 0
        currentRecordNumber = 0

        theControllerInstances = self.theUnicosProject.getDeviceType("Controller").getAllDeviceTypeInstances()
        for PIDInst in theControllerInstances:
            currentName = PIDInst.getAttributeData("DeviceIdentification:Name")
            recordNumber = recordNumber + 1
            if currentName == name:
                currentRecordNumber = recordNumber

        ApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
        instVector = self.theUnicosProject.findMatchingInstances("Controller", "'#DeviceIdentification:Name#'='$name$'")
        if (instVector.size() == 0):
            self.thePlugin.writeErrorInUABLog("The instance $name$ does not exist in the specification file.")
            self.thePlugin.writeInfoInUABLog("Reload the specification file by clicking on the Reload Specs. button in the wizard.")
            return
        for inst in instVector:
            Name = name = inst.getAttributeData("DeviceIdentification:Name")
            LparamVector = TIALogic_DefaultAlarms_Template.getLparameters(inst)
            IncreaseSpeed = inst.getAttributeData("FEDeviceAutoRequests:Default Setpoint Speed:Increase Speed")
            DecreaseSpeed = inst.getAttributeData("FEDeviceAutoRequests:Default Setpoint Speed:Decrease Speed")
            MeasuredValue = inst.getAttributeData("FEDeviceEnvironmentInputs:Measured Value")
            ScalingMethod = inst.getAttributeData("FEDeviceParameters:Controller Parameters:Scaling Method")
            PIDCycle = inst.getAttributeData("FEDeviceParameters:Controller Parameters:PID Cycle (s)")
            PIDCycle = str(float(PIDCycle) * 1000)
            description = inst.getAttributeData("DeviceDocumentation:Description")
            Def_Kc = self.thePlugin.formatNumberPLC(self.theUnicosProject.DlookupString("FEDeviceVariables:Default PID Parameters:Kc", "Controller", "'#DeviceIdentification:Name#' = '$name$'"))
            Def_Ti = self.thePlugin.formatNumberPLC(self.theUnicosProject.DlookupString("FEDeviceVariables:Default PID Parameters:Ti", "Controller", "'#DeviceIdentification:Name#' = '$name$'"))
            Def_Td = self.thePlugin.formatNumberPLC(self.theUnicosProject.DlookupString("FEDeviceVariables:Default PID Parameters:Td", "Controller", "'#DeviceIdentification:Name#' = '$name$'"))
            Def_Tds = self.thePlugin.formatNumberPLC(self.theUnicosProject.DlookupString("FEDeviceVariables:Default PID Parameters:Tds", "Controller", "'#DeviceIdentification:Name#' = '$name$'"))
            Def_SPH = self.thePlugin.formatNumberPLC(self.theUnicosProject.DlookupString("FEDeviceVariables:Default PID Parameters:SP High Limit", "Controller", "'#DeviceIdentification:Name#' = '$name$'"))
            Def_SPL = self.thePlugin.formatNumberPLC(self.theUnicosProject.DlookupString("FEDeviceVariables:Default PID Parameters:SP Low Limit", "Controller", "'#DeviceIdentification:Name#' = '$name$'"))
            Def_OutH = self.thePlugin.formatNumberPLC(self.theUnicosProject.DlookupString("FEDeviceVariables:Default PID Parameters:Out High Limit", "Controller", "'#DeviceIdentification:Name#' = '$name$'"))
            Def_OutL = self.thePlugin.formatNumberPLC(self.theUnicosProject.DlookupString("FEDeviceVariables:Default PID Parameters:Out Low Limit", "Controller", "'#DeviceIdentification:Name#' = '$name$'"))
            Def_SP = self.thePlugin.formatNumberPLC(self.theUnicosProject.DlookupString("FEDeviceVariables:Default PID Parameters:Setpoint", "Controller", "'#DeviceIdentification:Name#' = '$name$'"))

            # Count number of controlled Objects
            ControlledObjectNames = inst.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ")
            ControlledObjectList = ControlledObjectNames.split()
            ControlledObjectNumber = len(ControlledObjectList)

            # check if 1st controlled objet is a PID
            ControlledObject1IsPID = 0
            if ControlledObjectNumber > 0:
                instPID = self.theUnicosProject.findMatchingInstances("Controller", "'#DeviceIdentification:Name#'='$ControlledObjectList[0]$'")
                for inst in instPID:
                    ControlledObject1IsPID = 1

            # check if the current controller in controlled by another controller(cascade)
            MasterControllerName = ""
            AllControllers = self.theUnicosProject.findMatchingInstances("Controller", "'#FEDeviceOutputs:Controlled Objects#' != ''")
            for PID in AllControllers:
                PIDName = PID.getAttributeData("DeviceIdentification:Name").strip()
                ControlledObjectNames2 = PID.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ")
                ControlledObjectList2 = ControlledObjectNames2.split()
                for ControlledObjectName2 in ControlledObjectList2:
                    if (ControlledObjectName2 == Name):
                        MasterControllerName = PIDName

        self.thePlugin.writeTIALogic('''(*Device Logic of $name$ ($description$) ********** Application: $ApplicationName$ *****)
''')

        # call the standard template or the custom one
        if callUserTemplate == "true":
            eval(userTemplateName).ControllerLogic(self.thePlugin, self.theUnicosProject, master, name, LparamVector, MasterControllerName, IncreaseSpeed, DecreaseSpeed, MeasuredValue)
        else:
            TIALogic_Controller_DL_Standard_Template.ControllerLogic(self.thePlugin, self.theUnicosProject, master, name, LparamVector, MasterControllerName, IncreaseSpeed, DecreaseSpeed, MeasuredValue)

        # Step 1.5: Tracking the controller. HOutO (Hardware Output Order.Feedback position of controlled object.)
        self.thePlugin.writeTIALogic('''
(*Tracking***********)
''')

        # Discover which object (ANALOG or ANADIG for example) has been controlled by this PID. With this info set the Input variable
        # in order to run the inverse scaling funtion

        self.thePlugin.writeTIALogic('''
// Scaling Function
// Values to change if SPlit Range 
''')

        # 3.4 Compute HoutO coming from controlled Objects
        if ControlledObject1IsPID:
            ControlledOut = "$ControlledObjectList[0]$.ActSP"
            ControlledMaxRan = "$ControlledObjectList[0]$.PControl.PMaxRan"
            ControlledMinRan = "$ControlledObjectList[0]$.PControl.PMinRan"
        elif ControlledObjectNumber > 0:
            ControlledOut = "$ControlledObjectList[0]$.PosRSt"
            ControlledMaxRan = "$ControlledObjectList[0]$.Panalog.PMaxRan"
            ControlledMinRan = "$ControlledObjectList[0]$.Panalog.PMinRan"
        else:
            ControlledOut = "0.0"
            ControlledMaxRan = "100.0"
            ControlledMinRan = "0.0"

        if ScalingMethod == "Input Scaling" and ControlledObjectNumber == 1:
            self.thePlugin.writeTIALogic('''
IN := $ControlledOut$; 
In_Min := $ControlledMinRan$;
In_Max := $ControlledMaxRan$;
Out_Min	:= 0.0;
Out_Max := 100.0;

// Scaling for analog OUT := (IN-In_Min) * (Out_Max-Out_Min)/(In_Max-In_Min) + Out_Min; 
// Inverse Scaling function for Control Objects:
OUT := (IN - Out_Min) * (Out_Max-Out_Min)/(In_Max-In_Min) + Out_Min;  //scaling for PID

$name$.HOutO := OUT;

''')
        elif ((ScalingMethod == "Input/Output Scaling" or ScalingMethod == "No Scaling") and ControlledObjectNumber == 1):
            self.thePlugin.writeTIALogic('''
$name$.HOutO := $ControlledOut$;
''')
        else:
            self.thePlugin.writeTIALogic('''
// No tracking
$name$.HOutO := 0.0;
''')

        # setup PID parameter assigned if APAR or AS specified in spec
        if Def_SP <> "" and self.thePlugin.isString(Def_SP):
            self.thePlugin.writeTIALogic('''
$name$.AuESP := TRUE;
$name$.AuSPR := $Def_SP$.PosSt; (*SP setup by spec*)''')
        if Def_Kc <> "" and self.thePlugin.isString(Def_Kc):
            self.thePlugin.writeTIALogic('''
$name$.AuPPID.EKc := TRUE;
$name$.AuPPID.Kc := $Def_Kc$.PosSt; (*Kc setup by spec*)''')
        if Def_Ti <> "" and self.thePlugin.isString(Def_Ti):
            self.thePlugin.writeTIALogic('''
$name$.AuPPID.ETi := TRUE;
$name$.AuPPID.Ti := $Def_Ti$.PosSt; (*Ti setup by spec*)''')
        if Def_Td <> "" and self.thePlugin.isString(Def_Td):
            self.thePlugin.writeTIALogic('''
$name$.AuPPID.ETd := TRUE;
$name$.AuPPID.Td := $Def_Td$.PosSt; (*Td setup by spec*)''')
        if Def_Tds <> "" and self.thePlugin.isString(Def_Tds):
            self.thePlugin.writeTIALogic('''
$name$.AuPPID.ETds := TRUE;
$name$.AuPPID.Tds := $Def_Tds$.PosSt; (*Tds setup by spec*)''')
        if Def_SPH <> "" and self.thePlugin.isString(Def_SPH):
            self.thePlugin.writeTIALogic('''
$name$.AuPPID.ESPH := TRUE;
$name$.AuPPID.SPH := $Def_SPH$.PosSt; (*SPH setup by spec*)''')
        if Def_SPL <> "" and self.thePlugin.isString(Def_SPL):
            self.thePlugin.writeTIALogic('''
$name$.AuPPID.ESPL := TRUE;
$name$.AuPPID.SPL := $Def_SPL$.PosSt; (*SPL setup by spec*)''')
        if Def_OutH <> "" and self.thePlugin.isString(Def_OutH):
            self.thePlugin.writeTIALogic('''
$name$.AuPPID.EOutH := TRUE;
$name$.AuPPID.OutH := $Def_OutH$.PosSt; (*OutH setup by spec*)''')
        if Def_OutL <> "" and self.thePlugin.isString(Def_OutL):
            self.thePlugin.writeTIALogic('''
$name$.AuPPID.EOutL := TRUE;
$name$.AuPPID.OutL := $Def_OutL$.PosSt; (*OutL setup by spec*)''')

        # Set the AuTrR (The control logic requests to apply the tracking working state.)
        # The control logic requests to apply the tracking working state.
        self.thePlugin.writeTIALogic('''
$name$.AuTrR := ''')
        if ControlledObjectNumber == 1:
            self.thePlugin.writeTIALogic('''
NOT $ControlledObjectList[0]$.AuMoSt OR 
''')
            if not ControlledObject1IsPID:
                self.thePlugin.writeTIALogic('''
NOT $ControlledObjectList[0]$.RdyStartSt OR 
''')
        self.thePlugin.writeTIALogic('''0;
''')

        # Step 1.6: IoSimu and IoError:
        # IO Error. Set the Controller IOError with the controlled objects IOErrors

        self.thePlugin.writeTIALogic('''
// IO Error
$name$.IoError := ''')
        if MeasuredValue <> "" and self.theUnicosProject.findInstanceByName("AnalogInput,AnalogInputReal", MeasuredValue):
            self.thePlugin.writeTIALogic(MeasuredValue + '''.IoErrorW OR
''')
        for ControlledObject in ControlledObjectList:
            self.thePlugin.writeTIALogic(ControlledObject + '''.IoErrorW OR
''')
        self.thePlugin.writeTIALogic('''
0;''')

        # IO Simu. Set the Controller IOSimu with the controlled objects IOSimus
        ioSimuList = ["0"]
        if MeasuredValue <> "" and self.theUnicosProject.findInstanceByName("AnalogInput,AnalogInputReal", MeasuredValue):
            ioSimuList.append(MeasuredValue + ".IoSimuW OR " + MeasuredValue + ".FoMoSt")

        for controlledObject in ControlledObjectList:
            ioSimuList.append(controlledObject + ".IoSimuW")
        ioSimuList.append("0")
        self.thePlugin.writeTIALogic('''
// IO Simu
$name$.IoSimu := ''' + " OR\n".join(ioSimuList) + ";")

        # Step 1.7: Run the CPC_FB_PID Function Block from the Baseline
        group = self.thePlugin.getGroup(name)
        if group is None:
            return

        self.thePlugin.writeTIALogic('''
(*Execution ********)
// Controller: group=''' + str(group) + '''; Sampling Time= $PIDCycle$ms;
$name$.pid_activation:= DB_SCHED.LOOP_DAT[''' + str(group) + '''].ENABLE;
$name$.PControl.PIDCycle:=DB_SCHED.LOOP_DAT[''' + str(group) + '''].cycle;

// Passing the ManualRequest from the DB_PID_ManRequest to DB $name$
''')

        self.thePlugin.writeTIALogic('''
"CPC_FC_PID"(DB_PID := "$name$");

''')

        # Step 1.8: Copy the status of the current Controller to global status DB
        self.thePlugin.writeTIALogic('''
//Reset AuAuMoR
$Name$.AuAuMoR := FALSE;	

''')

        self.thePlugin.writeTIALogic('''
END_FUNCTION

''')

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for CONTROLLER.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for CONTROLLER.")
