# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)


def ProcessControlObjectLogic(thePlugin, theUnicosProject, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    thePlugin.writeTIALogic('''
FUNCTION $name$_DL : VOID
TITLE = '$name$_DL'
//
// Dependent Logic of $name$
// Master: $master$
// Lparam1:	$Lparam1$
// Lparam2:	$Lparam2$
// Lparam3:	$Lparam3$
// Lparam4:	$Lparam4$
// Lparam5:	$Lparam5$
// Lparam6:	$Lparam6$
// Lparam7:	$Lparam7$
// Lparam8:	$Lparam8$
// Lparam9:	$Lparam9$
// Lparam10:	$Lparam10$
//
AUTHOR: 'UNICOS'
NAME: 'Logic_DL'
FAMILY: 'PCO'
BEGIN''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')
    # Step 1.1: Position Request calculation. The user should complet this part according the logic process. By default we put that to "0".
    thePlugin.writeTIALogic('''
(*Position initialization*)
$name$.AuOnR:= 0; // $master$.RunOSt AND NOT $master$.CStopOSt AND ...To complete
$name$.AuOffR:= 0; // NOT $master$.RunOSt OR $master$.CStopOSt OR ... To complete
$name$.AuCStopR:= 0; // $master$.CStopOSt OR ... To complete
''')

    if master == name or master.lower() == "no_master":
        test = "do nothing"
    else:
        thePlugin.writeTIALogic('''
$name$.AuOpMoR := 1.0; //  To complete
''')

    # Step 1.2: IoSimu and IoError: both for DA and AA. We pass the IOError and the IOSimu from the Input associated to the DB_ERROR_SIMU created
    # IoError
    thePlugin.writeTIALogic('''
	
(*IoSimu and IoError*****************)
DB_ERROR_SIMU.$name$_DL_E := 0; // To complete
DB_ERROR_SIMU.$name$_DL_S := 0; // To complete
''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
