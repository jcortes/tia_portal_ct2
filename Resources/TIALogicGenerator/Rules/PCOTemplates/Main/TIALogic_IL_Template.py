# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from java.util import ArrayList
from java.util import Vector
from time import strftime
import TIALogic_IL_Standard_Template
import TIALogic_DefaultAlarms_Template
import sys


class IL_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython for IL.")
        reload(TIALogic_IL_Standard_Template)
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        # reload(TIALogic_DefaultAlarms_Template)
        # reload(sys)

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for IL.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for IL.")
        dateAndTime = strftime("%Y-%m-%d %H:%M:%S")  # '2007-03-03 22:14:39'

    def process(self, *params):
        theCurrentPco = params[0]
        theCurrentSection = params[1]
        callUserTemplate = str(params[2])
        userTemplateName = str(params[3])
        name = theCurrentPco.getDeviceName()
        master = master = theCurrentPco.getDeviceName()
        self.thePlugin.writeInUABLog("processInstances in Jython for IL for PCO $name$.")

        # General Steps for Dependent Logic templates:
        # 1. Create the FUNCTION called DeviceName_DL. Inside this function we must include the next steps:
        #	1.1. Create all the variable that we're using on this Function
        #	1.2. Interlock Conditions (both for analog and digital alarms) and IOError/IOSimu for the Alarms (both for analog and digital alarms)
        #	1.3. Fill the IOError and IOSimu from the Inputs linked by the user logic (if procceds)
        #	1.4. Instantiation of the AuAlAck for the DigitalAlarm objects related to the PCO object
        #   1.5. Interlock: Both for DA and AA (SI, FS, TS)

        # Step 1. Create the FUNCTION called DeviceName_IL.
        ApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
        instVector = self.theUnicosProject.findMatchingInstances("ProcessControlObject", "'#DeviceIdentification:Name#'='$name$'")
        instVector = self.theUnicosProject.findMatchingInstances("ProcessControlObject", "'#DeviceIdentification:Name#'='$name$'")
        if (instVector.size() == 0):
            self.thePlugin.writeErrorInUABLog("The instance $name$ does not exist in the specification file.")
            self.thePlugin.writeInfoInUABLog("Reload the specification file by clicking on the Reload Specs. button in the wizard.")
            return
        description = self.theUnicosProject.createSectionText(instVector, 1, 1, '''#DeviceDocumentation:Description#''').strip()
        PCOInstances = self.theUnicosProject.getDeviceType("ProcessControlObject").getAllDeviceTypeInstances()
        for PCOInstance in PCOInstances:
            PCOInstanceName = PCOInstance.getAttributeData("DeviceIdentification:Name")
            if PCOInstanceName == name:
                LparamVector = TIALogic_DefaultAlarms_Template.getLparameters(PCOInstance)
        self.thePlugin.writeTIALogic('''(**** Interlock Logic:  $name$ ($description$)********* Application: $ApplicationName$ *******************)
''')

        # Step 1.1: Create all the variable that we're using on this Function
        # Gets all the "Interlocks Alarms" that are children of the current PCO object. That means SI, FS and TS (all except "No" Alarms)
        # Gets all the Digital Alarms that are children of the current PCO object
        theDigitalAlarms, theDigitalAlarmsMultiple, allTheDigitalAlarms, DAListPosition = TIALogic_DefaultAlarms_Template.getDigitalAlarms(self.theUnicosProject, name)

        # Gets all the Analog Alarms that are children of the current PCO object
        theAnalogAlarms, theAnalogAlarmsMultiple, allTheAnalogAlarms, AAListPosition = TIALogic_DefaultAlarms_Template.getAnalogAlarms(self.theUnicosProject, name)

        # Gets all the "FS Alarms" that are child of the current PCO object.
        theDAFsAlarms, theDAFsAlarmsMultiple, allTheDAFsAlarms, theAAFsAlarms, theAAFsAlarmsMultiple, allTheAAFsAlarms = TIALogic_DefaultAlarms_Template.getFsAlarms(self.theUnicosProject, name)

        # Gets all the "SI Alarms" that are child of the current PCO object.
        theDASiAlarms, theDASiAlarmsMultiple, allTheDASiAlarms, theAASiAlarms, theAASiAlarmsMultiple, allTheAASiAlarms = TIALogic_DefaultAlarms_Template.getSiAlarms(self.theUnicosProject, name)

        # Gets all the "TS Alarms" that are child of the current PCO object.
        theDATsAlarms, theDATsAlarmsMultiple, allTheDATsAlarms, theAATsAlarms, theAATsAlarmsMultiple, allTheAATsAlarms = TIALogic_DefaultAlarms_Template.getTsAlarms(self.theUnicosProject, name)

        # Gets all the "AL Alarms" that are children of the OnOff object.
        theDAAlAlarms, theDAAlAlarmsMultiple, allTheDAAlAlarms, theAAAlAlarms, theAAAlAlarmsMultiple, allTheAAAlAlarms = TIALogic_DefaultAlarms_Template.getAlAlarms(self.theUnicosProject, name)

        # Step 1.2: Interlock Conditions (both for analog and digital alarms) and IOError/IOSimu for the Alarms (both for analog and digital alarms)

        # call the standard template or the custom one
        if callUserTemplate == "true":
            numArguments = eval(userTemplateName).ILLogic.func_code.co_argcount
            if numArguments == 4:
                # For backwards compatibility with RP 1.5.0 and previous (see UCPC-1550 : missing master in list of arguments)
                eval(userTemplateName).ILLogic(self.thePlugin, self.theUnicosProject, name, LparamVector)
            else:
                # Include master
                eval(userTemplateName).ILLogic(self.thePlugin, self.theUnicosProject, master, name, LparamVector)
        else:
            TIALogic_IL_Standard_Template.ILLogic(self.thePlugin, self.theUnicosProject, master, name, LparamVector)

        # Configured parameters of alarms
        TIALogic_DefaultAlarms_Template.writeConfiguredDAParameters(self.thePlugin, theDigitalAlarms, theDigitalAlarmsMultiple, DAListPosition)
        TIALogic_DefaultAlarms_Template.writeConfiguredAAParameters(self.thePlugin, self.theUnicosProject, theAnalogAlarms, theAnalogAlarmsMultiple, AAListPosition)

        # Step 1.4: Instantiation of the AuAlAck for the DigitalAlarm objects related to the PCO object
        TIALogic_DefaultAlarms_Template.AuAlAckAlarmsMethod(self.thePlugin, self.theUnicosProject, name, master, theDigitalAlarms, theDigitalAlarmsMultiple, theAnalogAlarms, theAnalogAlarmsMultiple, DAListPosition, AAListPosition)

        # Step 1.5: Interlock: Both for DA and AA (SI, FS, TS, AL)
        TIALogic_DefaultAlarms_Template.interlockAlarmsMethod(self.thePlugin, self.theUnicosProject, allTheDASiAlarms, allTheAASiAlarms, allTheDAFsAlarms, allTheAAFsAlarms, allTheDATsAlarms, allTheAATsAlarms, allTheDAAlAlarms, allTheAAAlAlarms, name)

        self.thePlugin.writeTIALogic('''	
END_FUNCTION''')

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for IL.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for IL.")
