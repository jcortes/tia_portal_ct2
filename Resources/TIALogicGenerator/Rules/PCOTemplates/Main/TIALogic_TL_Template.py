# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from time import strftime
import TIALogic_TL_Standard_Template
import TIALogic_DefaultAlarms_Template
import sys


class TL_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython for tl.")
        reload(TIALogic_TL_Standard_Template)
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        # reload(TIALogic_DefaultAlarms_Template)
        # reload(sys)

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for tl.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for tl.")
        dateAndTime = strftime("%Y-%m-%d %H:%M:%S")  # '2007-03-03 22:14:39'

    def process(self, *params):
        theCurrentPco = params[0]
        theCurrentSection = params[1]
        callUserTemplate = str(params[2])
        userTemplateName = str(params[3])
        self.thePlugin.writeInUABLog("processInstances in Jython for cdol.")
        name = theCurrentPco.getDeviceName()
        master = Master = theCurrentPco.getMasterDevice().getDeviceName()
        ApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
        instVector = self.theUnicosProject.findMatchingInstances("ProcessControlObject", "'#DeviceIdentification:Name#'='$name$'")
        if (instVector.size() == 0):
            self.thePlugin.writeErrorInUABLog("The instance $name$ does not exist in the specification file.")
            self.thePlugin.writeInfoInUABLog("Reload the specification file by clicking on the Reload Specs. button in the wizard.")
            return
        description = self.theUnicosProject.createSectionText(instVector, 1, 1, '''#DeviceDocumentation:Description#''').strip()

        for inst in instVector:
            LparamVector = TIALogic_DefaultAlarms_Template.getLparameters(inst)
        self.thePlugin.writeTIALogic('''(*Global Transition Logic: $name$ ($description$)*** Application: $ApplicationName$ *****)
''')

        # call the standard template or the custom one
        if callUserTemplate == "true":
            eval(userTemplateName).TLLogic(self.thePlugin, self.theUnicosProject, master, name, LparamVector)
        else:
            TIALogic_TL_Standard_Template.TLLogic(self.thePlugin, self.theUnicosProject, master, name, LparamVector)

        self.thePlugin.writeTIALogic('''
END_FUNCTION
''')

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for tl.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for tl.")
