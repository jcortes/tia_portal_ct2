# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
# from research.ch.cern.unicos.types.ai import Instance                 	#REQUIRED
from time import strftime
import TIALogic_INST_Standard_Template
import TIALogic_DefaultAlarms_Template
import sys


class INST_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython for SPCO.")
        reload(TIALogic_INST_Standard_Template)
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        # reload(TIALogic_DefaultAlarms_Template)
        # reload(sys)

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for SPCO.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for SPCO.")
        dateAndTime = strftime("%Y-%m-%d %H:%M:%S")  # '2007-03-03 22:14:39'

    def process(self, *params):
        theCurrentPco = params[0]
        theCurrentSection = params[1]
        callUserTemplate = str(params[2])
        userTemplateName = str(params[3])
        name = Name = theCurrentPco.getDeviceName()
        self.thePlugin.writeInUABLog("processInstances in Jython for SPCO for PCO $name$.")

        # General Steps for PCO section templates:
        # 1. Create the FUNCTION called DeviceName_DL. Inside this function we must include the next steps:
        #	1.1: Write the StsReg in the old_status, run the CPC_FB_PCO and write the status in the DB_bin_status_PCO and DB_ana_status_PCO

        recordNumber = 0
        currentRecordNumber = 0

        thePCOInstances = self.theUnicosProject.getDeviceType("ProcessControlObject").getAllDeviceTypeInstances()
        for PCOInst in thePCOInstances:
            currentName = PCOInst.getAttributeData("DeviceIdentification:Name")
            recordNumber = recordNumber + 1
            if currentName == name:
                currentRecordNumber = recordNumber
        master = Master = theCurrentPco.getMasterDevice().getDeviceName()
        ApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
        instVector = self.theUnicosProject.findMatchingInstances("ProcessControlObject", "'#DeviceIdentification:Name#'='$name$'")
        if (instVector.size() == 0):
            self.thePlugin.writeErrorInUABLog("The instance $name$ does not exist in the specification file.")
            self.thePlugin.writeInfoInUABLog("Reload the specification file by clicking on the Reload Specs. button in the wizard.")
            return
        description = self.theUnicosProject.createSectionText(instVector, 1, 1, '''#DeviceDocumentation:Description#''').strip()
        for inst in instVector:
            LparamVector = TIALogic_DefaultAlarms_Template.getLparameters(inst)
        # Step 1: Create the FUNCTION called PCOName_SectionName.
        self.thePlugin.writeTIALogic('''(**** INST instantiation:   $name$ ($description$) ********** Application: $ApplicationName$ **********)
''')

        # call the standard template or the custom one
        if callUserTemplate == "true":
            eval(userTemplateName).INSTLogic(self.thePlugin, self.theUnicosProject, master, name, LparamVector, currentRecordNumber)
        else:
            TIALogic_INST_Standard_Template.INSTLogic(self.thePlugin, self.theUnicosProject, master, name, LparamVector, currentRecordNumber)

        self.thePlugin.writeTIALogic('''
END_FUNCTION
''')

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for SPCO.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for SPCO.")
