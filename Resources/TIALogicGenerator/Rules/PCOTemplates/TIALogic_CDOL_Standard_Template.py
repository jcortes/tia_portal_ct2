# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)


def CDOLLogic(thePlugin, theUnicosProject, master, name, LparamVector, theCurrentPco):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    # Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeTIALogic('''
FUNCTION $name$_CDOL : VOID
TITLE = '$name$_CDOL'
//
// Common Dependant Object  Logic of $name$
//
// Lparam1:	$Lparam1$
// Lparam2:	$Lparam2$
// Lparam3:	$Lparam3$
// Lparam4:	$Lparam4$
// Lparam5:	$Lparam5$
// Lparam6:	$Lparam6$
// Lparam7:	$Lparam7$
// Lparam8:	$Lparam8$
// Lparam9:	$Lparam9$
// Lparam10:	$Lparam10$
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_CD'
FAMILY: 'CDOL'
BEGIN
''')

    # Gets all the PCO objects that are child of the 'master' object
    thePcoObjects = theUnicosProject.findMatchingInstances("ProcessControlObject", "$name$", None)
    # Gets all the Anadig objects that are child of the 'master' object
    theAnadigObjects = theUnicosProject.findMatchingInstances("AnalogDigital", "$name$", None)
    # Gets all the Analog objects that are child of the 'master' object
    theAnalogObjects = theUnicosProject.findMatchingInstances("Analog", "$name$", None)
    # Gets all the AnaDO objects that are child of the 'master' object
    theAnaDOObjects = theUnicosProject.findMatchingInstances("AnaDO", "$name$", None)
    # Gets all the OnOff objects that are child of the 'master' object
    theOnOffObjects = theUnicosProject.findMatchingInstances("OnOff", "$name$", None)
    # Gets all the controllers objects that are child of the 'master' object
    theControllersObjects = theUnicosProject.findMatchingInstances("Controller", "$name$", None)

    theDependentObjects = Vector(theAnalogObjects)
    theDependentObjects.addAll(theAnadigObjects)
    theDependentObjects.addAll(theAnaDOObjects)
    theDependentObjects.addAll(theOnOffObjects)

    theDependentDevices = theCurrentPco.getDependentDevices()  # all children

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
     (* Auto Auto Mode Request.*)
''')
    # Get the dependent_logic object for the current PCO
    # Step 1.1: Set the Auto Mode Request. For each Dependent Device of the current PCO with RE_RunOSt or FE_RunOSt or RE_CStopOSt or RE_AuDepOSt.

    if thePcoObjects.size() > 0 or theDependentObjects.size() > 0 or theControllersObjects.size() > 0:
        thePlugin.writeTIALogic('''
		IF $name$.RE_RunOSt OR $name$.FE_RunOSt OR $name$.RE_CStopOSt OR $name$.RE_AuDepOSt THEN
		''')
        for dependentDevice in theDependentDevices:
            dependentDeviceName = dependentDevice.getDeviceName()
            thePlugin.writeTIALogic('''
			$dependentDeviceName$.AuAuMoR := TRUE;						
			''')
        thePlugin.writeTIALogic('''END_IF;
		''')

    # Step 1.2: Set the Auto Dependent Request for PCO only. For each Dependent Device of the current PCO with RE_RunOSt or FE_RunOSt or RE_CStopOSt or RE_AuDepOSt.
    if thePcoObjects.size() > 0:
        thePlugin.writeTIALogic('''
	   (* Auto Dependent Request for PCO only.*)
	   IF $name$.RE_RunOSt 	OR 	$name$.FE_RunOSt OR	$name$.RE_CStopOSt OR $name$.RE_AuDepOSt THEN
''')

        for DependentObject in thePcoObjects:
            DependentObjectName = DependentObject.getAttributeData("DeviceIdentification:Name").strip()
            thePlugin.writeTIALogic('''
				$DependentObjectName$.AuAuDepR := TRUE;
				''')
        thePlugin.writeTIALogic('''END_IF;
			''')

    # Step 1.3: Set the Auto Alarm Acknowledge. For ANADIG, ANALOG and ONOFF Objects only. Pass the RE_AlUnAck from the current PCO to the AuAlAck of their dependent devices
    if theDependentObjects.size() > 0:
        thePlugin.writeTIALogic('''
	    (* Auto Alarm Acknowledge.*)
		IF $name$.E_MAlAckR OR $name$.AuAlAck THEN
''')

        for DependentObject in theDependentObjects:
            DependentObjectName = DependentObject.getAttributeData("DeviceIdentification:Name").strip()
            DependentObjectRestart = DependentObject.getAttributeData("FEDeviceParameters:ParReg:Manual Restart after Full Stop").strip()

            if DependentObjectRestart == "FALSE":
                thePlugin.writeTIALogic('''
	$DependentObjectName$.AuAlAck := NOT $DependentObjectName$.FuStopISt;''')
            else:
                thePlugin.writeTIALogic('''
	$DependentObjectName$.AuAlAck := TRUE;''')

        thePlugin.writeTIALogic('''
		END_IF;
				''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
