# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template


def BLLogic(thePlugin, theUnicosProject, master, name, LparamVector, theCurrentPco, allTheAnalogAlarms, allTheDigitalAlarms):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

# Step 1. Create the FUNCTION called DeviceName_BL.
    thePlugin.writeTIALogic('''
FUNCTION $name$_BL : VOID
TITLE = '$name$_BL'
//
// Warning of $name$
//
// Lparam1:	$Lparam1$
// Lparam2:	$Lparam2$
// Lparam3:	$Lparam3$
// Lparam4:	$Lparam4$
// Lparam5:	$Lparam5$
// Lparam6:	$Lparam6$
// Lparam7:	$Lparam7$
// Lparam8:	$Lparam8$
// Lparam9:	$Lparam9$
// Lparam10:	$Lparam10$
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_W'
FAMILY: 'WARN'

BEGIN


''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    thePlugin.writeTIALogic('''
(*Error********************************)
$name$.IOError := 0
''')
    thePcoSections = theCurrentPco.getSections()
    for theCurrentSection in thePcoSections:
        if theCurrentSection.getGenerateSection():
            theCurrentSectionName = theCurrentSection.getFullSectionName()
            theCurrentSectionNameLength = len(theCurrentSectionName)
            sectionType = theCurrentSectionName[theCurrentSectionNameLength - 4:]
            if not (sectionType == "CDOL" or sectionType == "INST"):
                thePlugin.writeTIALogic('''
	OR DB_ERROR_SIMU.''' + theCurrentSectionName + '''_E ''')
    theDependentDevices = theCurrentPco.getDependentDevices()
    for theDependentDevice in theDependentDevices:
        theDependentDeviceName = theDependentDevice.getDeviceName()
        thePlugin.writeTIALogic('''
	OR $theDependentDeviceName$.IoErrorW OR DB_ERROR_SIMU.$theDependentDeviceName$_DL_E''')
    for DigitalAlarm in allTheDigitalAlarms:
        DAName = DigitalAlarm.getAttributeData("DeviceIdentification:Name")
        thePlugin.writeTIALogic('''
	OR ''' + DAName + '''.IoErrorW''')
    for AnalogAlarm in allTheAnalogAlarms:
        AAName = AnalogAlarm.getAttributeData("DeviceIdentification:Name")
        thePlugin.writeTIALogic('''
	OR ''' + AAName + '''.IoErrorW''')

    thePlugin.writeTIALogic('''
	;''')

    thePlugin.writeTIALogic('''
(*SIMU*********************************)
$name$.IOSimu :=  0
''')
    for theCurrentSection in thePcoSections:
        if theCurrentSection.getGenerateSection():
            theCurrentSectionName = theCurrentSection.getFullSectionName()
            theCurrentSectionNameLength = len(theCurrentSectionName)
            sectionType = theCurrentSectionName[theCurrentSectionNameLength - 4:]
            if not (sectionType == "CDOL" or sectionType == "INST"):
                thePlugin.writeTIALogic('''
	OR DB_ERROR_SIMU.''' + theCurrentSectionName + '''_S''')
    for theDependentDevice in theDependentDevices:
        theDependentDeviceName = theDependentDevice.getDeviceName()
        thePlugin.writeTIALogic('''
	OR $theDependentDeviceName$.IoSimuW OR $theDependentDeviceName$.FoMoSt OR DB_ERROR_SIMU.$theDependentDeviceName$_DL_S''')
    for DigitalAlarm in allTheDigitalAlarms:
        DAName = DigitalAlarm.getAttributeData("DeviceIdentification:Name")
        thePlugin.writeTIALogic('''
	OR ''' + DAName + '''.IoSimuW''')
    for AnalogAlarm in allTheAnalogAlarms:
        AAName = AnalogAlarm.getAttributeData("DeviceIdentification:Name")
        thePlugin.writeTIALogic('''
	OR ''' + AAName + '''.IoSimuW''')
    thePlugin.writeTIALogic('''
	;''')

    # Step 1.2: Blocked Alarm warning
    thePlugin.writeTIALogic('''
(*Blocked Alarm warning ********************)
$name$.AlB :=
''')
    theDAAlarms = theUnicosProject.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Type#'!=''")
    theAAAlarms = theUnicosProject.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Type#'!=''")

    generatedText = theUnicosProject.createSectionText(theDAAlarms, 1, 1, '''	            #DeviceIdentification:Name#.MAlBRSt OR
''')
    thePlugin.writeTIALogic(generatedText)

    generatedText = theUnicosProject.createSectionText(theAAAlarms, 1, 1, '''                #DeviceIdentification:Name#.MAlBRSt OR
''')
    thePlugin.writeTIALogic(generatedText)


    for theDependentDevice in theDependentDevices:
        theDependentDeviceName = theDependentDevice.getDeviceName()
        theDependentDeviceType = theDependentDevice.getDeviceType()
        if (theDependentDeviceType == "Controller" or theDependentDeviceType == "ProcessControlObject"):
            # do nothing
            Test = "NOTHING TO DO"
        else:
            thePlugin.writeTIALogic('''
				$theDependentDeviceName$.AlBW OR''')
    thePlugin.writeTIALogic('''
      0;
''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
