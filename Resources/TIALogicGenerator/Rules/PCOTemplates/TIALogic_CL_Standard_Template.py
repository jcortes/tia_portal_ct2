# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)


def CLLogic(thePlugin, theUnicosProject, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

# Step 1: Create the FUNCTION called PCOName_SectionName.

    thePlugin.writeTIALogic('''
FUNCTION $name$_CL : VOID
TITLE = '$name$_CL'
//
// Configuration Logic of  $name$
//
// Lparam1:	$Lparam1$
// Lparam2:	$Lparam2$
// Lparam3:	$Lparam3$
// Lparam4:	$Lparam4$
// Lparam5:	$Lparam5$
// Lparam6:	$Lparam6$
// Lparam7:	$Lparam7$
// Lparam8:	$Lparam8$
// Lparam9:	$Lparam9$
// Lparam10:	$Lparam10$
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_CL'
FAMILY: 'Common'
BEGIN
  		 
''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    # Step 1.1: No master
    if master == name:
        thePlugin.writeTIALogic('''(* No master defined (master of masters) *)
$name$.IhAuMRW := 1;

''')

    # Step 1.2: Feedback ON
    thePlugin.writeTIALogic('''(*Feedback ON**********************)
$name$.FOn := $name$.RunOSt; // To complete

''')
    # Step 1.3: Feedback OFF
    thePlugin.writeTIALogic('''(*Feedback OFF***********************)
$name$.FOff := NOT $name$.FOn; // To complete

''')
    # Step 1.4: Control Stop Finished
    thePlugin.writeTIALogic('''(*Control Stop Finished************)
$name$.CStopFin := 0; // To complete

''')
    # Step 1.5: Option Mode
    if master == name or master.lower() == "no_master":
        thePlugin.writeTIALogic('''(*Option Mode*************************)
$name$.AuOpMoR := 1.0; // To complete

''')
    # Step 1.6: Warnings of CL Section
    thePlugin.writeTIALogic('''(*WArnings of CL Section**************)
DB_ERROR_SIMU.$name$_CL_E := 0 ; // To complete
DB_ERROR_SIMU.$name$_CL_S := 0 ; // To complete


''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
