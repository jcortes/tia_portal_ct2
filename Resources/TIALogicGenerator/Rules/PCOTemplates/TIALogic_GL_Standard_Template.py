# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)


def GLLogic(thePlugin, theUnicosProject, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

# Step 1. Create the FUNCTION called DeviceName_GL.
    thePlugin.writeTIALogic('''
FUNCTION $name$_GL : VOID
TITLE = '$name$_GL'
//
// Global Logic of $name$
//
// Lparam1:	$Lparam1$
// Lparam2:	$Lparam2$
// Lparam3:	$Lparam3$
// Lparam4:	$Lparam4$
// Lparam5:	$Lparam5$
// Lparam6:	$Lparam6$
// Lparam7:	$Lparam7$
// Lparam8:	$Lparam8$
// Lparam9:	$Lparam9$
// Lparam10:	$Lparam10$
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_GL'
FAMILY: 'GL'
''')

    # Third static variables
    thePlugin.writeTIALogic('''
VAR_TEMP
    old_status : DWORD;
END_VAR
BEGIN
''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    # Step 1.4: IOError IOSimu
    thePlugin.writeTIALogic('''

// Errors and Simulated data
DB_ERROR_SIMU.$name$_GL_E := 0; // To complete
DB_ERROR_SIMU.$name$_GL_S := 0; // To complete
''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
