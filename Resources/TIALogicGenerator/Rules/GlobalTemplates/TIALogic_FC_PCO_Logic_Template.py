# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from time import strftime


class FC_PCO_Logic_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.thePlugin.writeInUABLog("initialize in Jython for FC_PCO_Logic.")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for FC_PCO_Logic.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for FC_PCO_Logic.")
        dateAndTime = strftime("%Y-%m-%d %H:%M:%S")  # '2007-03-03 22:14:39'

    def process(self, *params):
        deviceVector = params[0]
        theXMLConfig = params[1]
        genGlobalFilesForAllSections = params[2].booleanValue()  # Comes from "Global files scope" dropdown on Wizard. true = All sections. false = Selected sections.
        self.thePlugin.writeInUABLog("processInstances in Jython for Compilation_Logic.")

        DA_FI_vector = self.thePlugin.getInstancesFI("DigitalAlarm")
        DA_FI_master_vector = []
        fast_interlock_present = not DA_FI_vector.isEmpty() # Fast interlock presence detected by any fast interlock digital alarm
        if fast_interlock_present:
            for instance_DA in DA_FI_vector:
                current_DA_FI_master_vector = instance_DA.getAttributeData("LogicDeviceDefinitions:Master").replace(",", " ").split()
                for mastername in current_DA_FI_master_vector:
                    DA_FI_master_vector.append(mastername)
            self.thePlugin.writeTIALogic('''FUNCTION FC_FI_LOGIC : VOID
TITLE = 'FC_FI_LOGIC'
//
// Call FI DL sections
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic'
FAMILY: 'UNICOS'
BEGIN
// Calling the DL of the FI devices with no master''')

            self.get_logic_sections(deviceVector, genGlobalFilesForAllSections, True, DA_FI_master_vector)
            self.thePlugin.writeTIALogic('''END_FUNCTION\n\n''')
        self.thePlugin.writeTIALogic('''FUNCTION FC_PCO_LOGIC : VOID
TITLE = 'FC_PCO_LOGIC'
//
// Call all PCO sections
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic'
FAMILY: 'UNICOS'
''')
        controllerObject = self.theUnicosProject.getDeviceType("Controller")
        if fast_interlock_present or (controllerObject != None and len(controllerObject.getAllDeviceTypeInstances()) > 0):
            self.thePlugin.writeTIALogic('''VAR_TEMP
  NbOfDelayedInterrupts : INT;
  NbOfQueuedInterrupts  : INT;
END_VAR
''')
        self.thePlugin.writeTIALogic('BEGIN')
        self.get_logic_sections(deviceVector, genGlobalFilesForAllSections, False, DA_FI_master_vector)
        if fast_interlock_present:
            self.thePlugin.writeTIALogic('''// Calling the sections inside fast interlock logic
// Delay interrupt OBs until EN_AIRT is called
NbOfDelayedInterrupts := DIS_AIRT();
FC_FI_Logic();
// Reenable interrupt OBs
NbOfQueuedInterrupts := EN_AIRT();''')
        self.thePlugin.writeTIALogic('''END_FUNCTION''')

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for FC_PCO_Logic.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for FC_PCO_Logic.")

    def get_logic_sections(self, deviceVector, genGlobalFilesForAllSections, fast_interlock_block, DA_FI_master_vector):
        for theCurrentPco in deviceVector:
            thePcoSections = theCurrentPco.getSections()
            theCurrentPcoName = theCurrentPco.getDeviceName()
            if not fast_interlock_block:
                self.thePlugin.writeTIALogic('''// Calling the PCO Sections for ''' + theCurrentPcoName)
            for theCurrentSection in thePcoSections:
                if theCurrentSection.getGenerateSection() or genGlobalFilesForAllSections:
                    theCurrentSectionName = theCurrentSection.getFullSectionName()
                    is_FI_PCO_IL = theCurrentSectionName.endswith("IL") and theCurrentPcoName in DA_FI_master_vector
                    if fast_interlock_block:
                        if is_FI_PCO_IL:
                            self.thePlugin.writeTIALogic('''// Calling the PCO IL Section for ''' + theCurrentPcoName)
                            self.thePlugin.writeTIALogic(theCurrentSectionName + '''();''')
                    else:
                        if is_FI_PCO_IL:
                            self.thePlugin.writeTIALogic('''// Delay interrupt OBs until EN_AIRT is called
NbOfDelayedInterrupts := DIS_AIRT();''')
                        self.thePlugin.writeTIALogic(theCurrentSectionName + '''();''')
                        if is_FI_PCO_IL:
                            self.thePlugin.writeTIALogic('''// Reenable interrupt OBs
NbOfQueuedInterrupts := EN_AIRT();''')
            self.get_device_logic(theCurrentPco, genGlobalFilesForAllSections, fast_interlock_block, DA_FI_master_vector)

    def get_device_logic(self, theCurrentPco, genGlobalFilesForAllSections, FIdevice, DA_FI_master_vector):
        theDependentDevices = theCurrentPco.getDependentDevices()
        theCurrentPcoName = theCurrentPco.getDeviceName()
        self.thePlugin.writeTIALogic('''// Calling the DL of the Dependent Devices for ''' + theCurrentPcoName)
        ONOFF_FI_instances = self.thePlugin.getInstancesFI("OnOff")
        for theCurrentDependentDevice in theDependentDevices:
            currentInstance = self.theUnicosProject.findInstanceByName(theCurrentDependentDevice.getDeviceName())
            FI_Dependent_Device_present = currentInstance in ONOFF_FI_instances
            if FI_Dependent_Device_present == FIdevice:
                if (not FIdevice and theCurrentDependentDevice.getDeviceName() in DA_FI_master_vector and currentInstance.getDeviceTypeName() != "ProcessControlObject")\
                or (theCurrentDependentDevice.getDeviceType() == "Controller"):
                    self.thePlugin.writeTIALogic('''// Delay interrupt OBs until EN_AIRT is called
NbOfDelayedInterrupts := DIS_AIRT();''')
                theDependentSections = theCurrentDependentDevice.getDependentSections()
                for section in theDependentSections:
                    if section.getGenerateSection() or genGlobalFilesForAllSections:
                        theCurrentSectionName = section.getFullSectionName()
                        self.thePlugin.writeTIALogic(theCurrentSectionName + '''();''')
                if (not FIdevice and theCurrentDependentDevice.getDeviceName() in DA_FI_master_vector and currentInstance.getDeviceTypeName() != "ProcessControlObject")\
                or(theCurrentDependentDevice.getDeviceType() == "Controller"):
                    self.thePlugin.writeTIALogic('''// Reenable interrupt OBs
NbOfQueuedInterrupts := EN_AIRT();''')