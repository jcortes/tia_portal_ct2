# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2015 all rights reserved
# $LastChangedRevision$
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from research.ch.cern.unicos.core import CoreManager  # REQUIRED
from time import strftime
from java.lang import System


# This is an example of a generic template to be extended by the device type templates
class Generic_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0
    theDeviceType = "generic"

    def initialize(self):
        # Get a reference to the plug-in
        self.thePlugin = APlugin.getPluginInterface()
        # Get a reference to the specs
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.thePlugin.writeInUABLog("initialize in Jython for $self.theDeviceType$.")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for $self.theDeviceType$.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for $self.theDeviceType$.")
        self.dateAndTime = strftime("%Y-%m-%d %H:%M:%S")  # '2007-03-03 22:14:39'
        self.theApplicationName = self.thePlugin.getApplicationName()
        self.thePluginId = self.thePlugin.getId()
        self.thePluginVersion = self.thePlugin.getVersionId()
        self.theUserName = System.getProperty("user.name")

    def process(self, *params):
        # This method is called on the current type by the plug-in.
        # To override in the device type templates to perform the specific logic.
        pass

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for $self.theDeviceType$.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for $self.theDeviceType$.")
