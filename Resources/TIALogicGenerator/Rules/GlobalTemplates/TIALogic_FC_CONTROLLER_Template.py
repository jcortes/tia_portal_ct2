# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from time import strftime


class FC_CONTROLLER_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython for FC_CONTROLLER.")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for FC_CONTROLLER.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for FC_CONTROLLER.")
        dateAndTime = strftime("%Y-%m-%d %H:%M:%S")  # '2007-03-03 22:14:39'

    def process(self, *params):
        deviceVector = params[0]
        theXMLConfig = params[1]
        genGlobalFilesForAllSections = params[2].booleanValue()  # Comes from "Global files scope" dropdown on Wizard. true = All sections. false = Selected sections.
        self.thePlugin.writeInUABLog("processInstances in Jython for Compilation_Logic.")
        # This method is called on every Instance of the current type by the Code Generation plug -in.
        self.thePlugin.writeTIALogic('''FUNCTION FC_CONTROLLER : VOID
TITLE = 'FC_CONTROLLER'
//
// Call all Controllers
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic'
FAMILY: 'UNICOS'
VAR_INPUT
   group: INT;
END_VAR
BEGIN''')
        maxGroup = 0
        for theCurrentPco in deviceVector:
            thePcoSections = theCurrentPco.getSections()
            theDependentDevices = theCurrentPco.getDependentDevices()
            theCurrentPcoName = theCurrentPco.getDeviceName()
            i = 0
            for theCurrentDependentDevice in theDependentDevices:
                theCurrentDeviceName = theCurrentDependentDevice.getDeviceName()
                theCurrentDeviceType = theCurrentDependentDevice.getDeviceType()
                if theCurrentDeviceType == "Controller":
                    theDependentSections = theCurrentDependentDevice.getDependentSections()
                    for section in theDependentSections:
                        if section.getGenerateSection() or genGlobalFilesForAllSections:
                            i = i + 1
                            group = self.thePlugin.getGroup(theCurrentDeviceName)
                            if group > maxGroup:
                                maxGroup = group
                            self.thePlugin.writeTIALogic('''IF group=''' + str(group) + ''' OR group=0 THEN ''' + theCurrentDeviceName + '''_DL(); END_IF;''')

        self.thePlugin.writeTIALogic('''END_FUNCTION''')
        # Create the scheduler FC here
        self.thePlugin.writeTIALogic('''FUNCTION CPC_SCHED : VOID
TITLE = 'CPC_SCHED'
//
// Schedules all Controllers (replaces LP_SCHED)
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic'
FAMILY: 'UNICOS'
VAR_INPUT
   PID_EXEC_CYCLE: Time;
END_VAR
VAR_TEMP
   I: Int;
   CYCLE_CALC: Int;
END_VAR
BEGIN''')       
        self.thePlugin.writeTIALogic('''FOR #I := 1 TO ''' + str(maxGroup) + ''' DO 
// Ensure that the loop cycle is at least as big as the Cyclic Interrupt OB cycle!
IF (TIME_TO_INT("DB_SCHED".LOOP_DAT[#I].MAN_CYC) < TIME_TO_INT(#PID_EXEC_CYCLE)) THEN
// "DB_SCHED".LOOP_DAT[#I].MAN_CYC := #PID_EXEC_CYCLE; 
// set the cycle to the Cyclic Interrupt OB cycle time
"DB_SCHED".LOOP_DAT[#I].CYCLE := #PID_EXEC_CYCLE;
ELSIF TIME_TO_INT("DB_SCHED".LOOP_DAT[#I].MAN_CYC) MOD TIME_TO_INT(#PID_EXEC_CYCLE) <> 0 THEN
// Ensure that the loop cycle is an integer multiple of the Cyclic Interrupt OB cycle!
// Round down for now
#CYCLE_CALC := TIME_TO_INT("DB_SCHED".LOOP_DAT[#I].MAN_CYC) / TIME_TO_INT(#PID_EXEC_CYCLE); // DIV
#CYCLE_CALC := #CYCLE_CALC * TIME_TO_INT(#PID_EXEC_CYCLE);
"DB_SCHED".LOOP_DAT[#I].CYCLE := INT_TO_TIME(#CYCLE_CALC);
ELSE
// Everything is ok. Set CYCLE = MAN_CYC
"DB_SCHED".LOOP_DAT[#I].CYCLE :="DB_SCHED".LOOP_DAT[#I].MAN_CYC;
END_IF;
// Reset all enabled flags to false after last call
"DB_SCHED".LOOP_DAT[#I].ENABLE := false;
// increment loop counter for each
"DB_SCHED".LOOP_DAT[#I].ILP_COU := "DB_SCHED".LOOP_DAT[#I].ILP_COU + 1;
IF ("DB_SCHED".LOOP_DAT[#I].ILP_COU * TIME_TO_INT("CPC_GLOBAL_VARS".PID_EXEC_CYCLE)) = TIME_TO_INT("DB_SCHED".LOOP_DAT[#I].CYCLE) THEN
// enable this loop for running and reset counter
"DB_SCHED".LOOP_DAT[#I].ENABLE := true;
"DB_SCHED".LOOP_DAT[#I].ILP_COU := 0;
END_IF;
END_FOR;''')
        self.thePlugin.writeTIALogic('''END_FUNCTION''')

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for FC_CONTROLLER.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for FC_CONTROLLER.")
