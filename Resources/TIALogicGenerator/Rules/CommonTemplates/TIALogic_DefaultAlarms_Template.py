# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
##
# This file contains all the common functions of the logic templates.
##

from java.util import Vector
from java.util import ArrayList
from java.lang import System
from research.ch.cern.unicos.utilities import SemanticVerifier

import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)


def getLparametersSplit(LparamVector):
    """
    These variable are set by default to map directly to the corresponding Specification parameters. E.g. Lparam1 = custom logic parameter1
    This (hopefully) should avoid any confusion with the 0-based index of LparamVector array.

    The arguments are:
    @param: LparamVector: the vector containing the 10 logic parameters

    This function returns:
    @return: The 10 parameters in independant variables

    """
    Lparam1 = LparamVector[0]
    Lparam2 = LparamVector[1]
    Lparam3 = LparamVector[2]
    Lparam4 = LparamVector[3]
    Lparam5 = LparamVector[4]
    Lparam6 = LparamVector[5]
    Lparam7 = LparamVector[6]
    Lparam8 = LparamVector[7]
    Lparam9 = LparamVector[8]
    Lparam10 = LparamVector[9]

    return Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10


def getLparameters(inst):
    """
    The arguments are:
    @param inst: object which is one instance i.e one line in the specification file,

    This function returns:
    @return: LparamVector: all the Lparameters in a vector
    """

    Lparam1 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter1")
    Lparam2 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter2")
    Lparam3 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter3")
    Lparam4 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter4")
    Lparam5 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter5")
    Lparam6 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter6")
    Lparam7 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter7")
    Lparam8 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter8")
    Lparam9 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter9")
    Lparam10 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter10")
    LparamVector = [Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10]
    return LparamVector


def getDigitalAlarms(theUnicosProject, name):
    """
    The arguments are:
    @param theUnicosProject: object which represents the specification file,
    @param name: the name of the master

    This function returns:
    @return: theDigitalAlarms: all the simple digital alarm instances of the master currently treated (name), 
    @return: theDigitalAlarmsMultiple: all the multiple digital alarms instances of the master currently treated (name), 
    @return: allTheDigitalAlarms: all the digital alarms instances of the master currently treated (name),
    @return: DAListPosition: all the master position for each multiple alarm. The ith element of DAListPosition corresponds to the position of the master currently treated (name) in the master list of the ith element of theDigitalAlarmsMultiple
    """

    DAListPosition = ArrayList()
    theDigitalAlarms = theUnicosProject.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Type#'!='', '#FEDeviceAlarm:Type#'!='Multiple'")
    theDigitalAlarmsMultiple = theUnicosProject.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'!=''", DAListPosition)
    allTheDigitalAlarms = Vector(theDigitalAlarms)
    allTheDigitalAlarms.addAll(theDigitalAlarmsMultiple)
    return theDigitalAlarms, theDigitalAlarmsMultiple, allTheDigitalAlarms, DAListPosition


def getAnalogAlarms(theUnicosProject, name):
    """
    The arguments are:
    @param theUnicosProject: object which represents the specification file,
    @param name: the name of the master

    This function returns: 
    @return theAnalogAlarms: all the simple analog alarm instances of the master currently treated (name), 
    @return theAnalogAlarmsMultiple: all the multiple analog alarms instances of the master currently treated (name), 
    @return allTheDigitalAlarms: all the analog alarms instances of the master currently treated (name), 
    @return AAListPosition: all the master position for each multiple alarm. The ith element of AAListPosition corresponds to the position of the master currently treated (name) in the master list of the ith element of theAnalogAlarmsMultiple
    """

    AAListPosition = ArrayList()
    theAnalogAlarms = theUnicosProject.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Type#'!='', '#FEDeviceAlarm:Type#'!='Multiple'")
    theAnalogAlarmsMultiple = theUnicosProject.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'!=''", AAListPosition)
    allTheAnalogAlarms = Vector(theAnalogAlarms)
    allTheAnalogAlarms.addAll(theAnalogAlarmsMultiple)
    return theAnalogAlarms, theAnalogAlarmsMultiple, allTheAnalogAlarms, AAListPosition


def getFsAlarms(theUnicosProject, name):
    """
    The arguments are:
    @param theUnicosProject: object which represents the specification file,
    @param name: the name of the master

    This function returns:
    @return: theDAFsAlarms: all the simple digital alarm instances of FS type of the master currently treated (name), 
    @return: theDAFsAlarmsMultiple: all the multiple digital alarms instances of FS type of the master currently treated (name), 
    @return: allTheDigitalAlarms: all the digital alarms instances of FS type of the master currently treated (name),
    @return: theAAFsAlarms: all the simple analog alarms instances of FS type of the master currently treated (name), 
    @return: theAAFsAlarmsMultiple: all the multiple analog alarms instances of FS type of the master currently treated (name), 
    @return: allTheAAFsAlarms: all the analog alarms instances of FS type of the master currently treated (name)
    """

    theDAFsAlarms = theUnicosProject.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Type#'='FS'")
    theDAFsAlarmsMultiple = theUnicosProject.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'='FS'")
    allTheDAFsAlarms = Vector(theDAFsAlarms)
    allTheDAFsAlarms.addAll(theDAFsAlarmsMultiple)

    theAAFsAlarms = theUnicosProject.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Type#'='FS'")
    theAAFsAlarmsMultiple = theUnicosProject.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'='FS'")
    allTheAAFsAlarms = Vector(theAAFsAlarms)
    allTheAAFsAlarms.addAll(theAAFsAlarmsMultiple)
    return theDAFsAlarms, theDAFsAlarmsMultiple, allTheDAFsAlarms, theAAFsAlarms, theAAFsAlarmsMultiple, allTheAAFsAlarms


def getTsAlarms(theUnicosProject, name):
    """
    The arguments are:
    @param theUnicosProject: object which represents the specification file,
    @param name: the name of the master

    This function returns:
    @return: theDATsAlarms: all the simple digital alarm instances of TS type of the master currently treated (name), 
    @return: theDATsAlarmsMultiple: all the multiple digital alarms instances of TS type of the master currently treated (name), 
    @return: allTheDATsAlarms: all the digital alarms instances of TS type of the master currently treated (name),
    @return: theAATsAlarms: all the simple analog alarms instances of TS type of the master currently treated (name), 
    @return: theAATsAlarmsMultiple: all the multiple analog alarms instances of TS type of the master currently treated (name), 
    allTheAATsAlarms: all the analog alarms instances of TS type of the master currently treated (name)
    """

    theDATsAlarms = theUnicosProject.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Type#'='TS'")
    theDATsAlarmsMultiple = theUnicosProject.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'='TS'")
    allTheDATsAlarms = Vector(theDATsAlarms)
    allTheDATsAlarms.addAll(theDATsAlarmsMultiple)

    theAATsAlarms = theUnicosProject.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Type#'='TS'")
    theAATsAlarmsMultiple = theUnicosProject.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'='TS'")
    allTheAATsAlarms = Vector(theAATsAlarms)
    allTheAATsAlarms.addAll(theAATsAlarmsMultiple)
    return theDATsAlarms, theDATsAlarmsMultiple, allTheDATsAlarms, theAATsAlarms, theAATsAlarmsMultiple, allTheAATsAlarms


def getSiAlarms(theUnicosProject, name):
    """
    The arguments are:
    @param theUnicosProject: object which represents the specification file,
    @param name: string containing the name of the master

    This function returns:
    @return: theDASiAlarms: all the simple digital alarm instances of SI type of the master currently treated (name), 
    @return: theDASiAlarmsMultiple: all the multiple digital alarms instances of SI type of the master currently treated (name), 
    @return: allTheDASiAlarms: all the digital alarms instances of SI type of the master currently treated (name),
    @return: theAASiAlarms: all the simple analog alarms instances of SI type of the master currently treated (name), 
    @return: theAASiAlarmsMultiple: all the multiple analog alarms instances of SI type of the master currently treated (name), 
    allTheAASiAlarms: all the analog alarms instances of SI type of the master currently treated (name)
    """

    theDASiAlarms = theUnicosProject.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Type#'='SI'")
    theDASiAlarmsMultiple = theUnicosProject.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'='SI'")
    allTheDASiAlarms = Vector(theDASiAlarms)
    allTheDASiAlarms.addAll(theDASiAlarmsMultiple)

    theAASiAlarms = theUnicosProject.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Type#'='SI'")
    theAASiAlarmsMultiple = theUnicosProject.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'='SI'")
    allTheAASiAlarms = Vector(theAASiAlarms)
    allTheAASiAlarms.addAll(theAASiAlarmsMultiple)
    return theDASiAlarms, theDASiAlarmsMultiple, allTheDASiAlarms, theAASiAlarms, theAASiAlarmsMultiple, allTheAASiAlarms


def getAlAlarms(theUnicosProject, name):
    """
    The arguments are:
    @param theUnicosProject: object which represents the specification file,
    @param name: string containing the name of the master

    This function returns:
    @return: theDAAlAlarms: all the simple digital alarm instances of AL type of the master currently treated (name), 
    @return: theDAAlAlarmsMultiple: all the multiple digital alarms instances of AL type of the master currently treated (name), 
    @return: allTheDAAlAlarms: all the digital alarms instances of AL type of the master currently treated (name),
    @return: theAAAlAlarms: all the simple analog alarms instances of AL type of the master currently treated (name), 
    @return: theAAAlAlarmsMultiple: all the multiple analog alarms instances of AL type of the master currently treated (name), 
    allTheAAAlAlarms: all the analog alarms instances of AL type of the master currently treated (name)
    """

    theDAAlAlarms = theUnicosProject.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Type#'='AL'")
    theDAAlAlarmsMultiple = theUnicosProject.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'='AL'")
    allTheDAAlAlarms = Vector(theDAAlAlarms)
    allTheDAAlAlarms.addAll(theDAAlAlarmsMultiple)

    theAAAlAlarms = theUnicosProject.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Type#'='AL'")
    theAAAlAlarmsMultiple = theUnicosProject.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'='AL'")
    allTheAAAlAlarms = Vector(theAAAlAlarms)
    allTheAAAlAlarms.addAll(theAAAlAlarmsMultiple)
    return theDAAlAlarms, theDAAlAlarmsMultiple, allTheDAAlAlarms, theAAAlAlarms, theAAAlAlarmsMultiple, allTheAAAlAlarms


def getIOErrorSimuCondition(expression, ED, theUnicosProject, objectsToRemove=[]):
    """
    This function generates string for IOError and IOSimu condition from given expression

    The arguments are:
    @param expression: expression to analyse
    @param ED: Expression decorator instance
    @param theUnicosProject: the UNICOS project (the spec)
    @param objectsToRemove: list of object names not to be included in IOError/IOSimu (as alarm object itself or master object)

    This function returns:
    @return IOError condition string
    @return IOSimu condition string
    """
    # get list of UNICOS objects in the condition
    listOfConditionObjects = ED.getListOfUNICOSObjects(expression, ["DIGITALINPUT", "DIGITALOUTPUT", "ANALOGINPUT", "ANALOGOUTPUT", "ANALOGINPUTREAL", "ANALOGOUTPUTREAL"])  # IO Only

    # get list of IO objects depending on the actuators
    listOfConditionObjects.extend(ED.getActuatorsIOs(expression))

    # remove unwanted objects from condition
    for obj in objectsToRemove:
        if obj in listOfConditionObjects:
            listOfConditionObjects.remove(obj)

    # empty output strings
    IOError = []
    IOSimu = []

    CRLF = System.getProperty("line.separator")

    for obj in listOfConditionObjects:
        IOSimu_str = ""
        object_type = theUnicosProject.findInstanceByName(obj).getDeviceType().getDeviceTypeName().upper()
        # 'DPAR', 'APAR', 'WPAR', 'WS', 'AS' do not have IOErrorW, IOSimuW, FoMoSt
        if object_type not in ["DIGITALPARAMETER", "ANALOGPARAMETER", "WORDPARAMETER", "WORDSTATUS", "ANALOGSTATUS"]:
            IOError.append(obj + ".IOErrorW")
            IOSimu_str = obj + ".IOSimuW"
            # Local, AA, DA do not have FoMoSt
            if object_type not in ["LOCAL", "DIGITALALARM", "ANALOGALARM"]:
                IOSimu_str = IOSimu_str + " OR " + obj + ".FoMoSt"
            IOSimu.append(IOSimu_str)

    if len(IOError) < 1:
        IOError.append("FALSE")
    if len(IOSimu) < 1:
        IOSimu.append("FALSE")

    return (" OR" + CRLF).join(IOError), (" OR" + CRLF).join(IOSimu)


def writeSingleConfiguredDAParameters(inst, ED, thePlugin):
    """
    This function will write PLC code concerning one instance of the digital alarm. It will only write the code concerning the parameters of the digital alarm which are configured in the specification file.

    The arguments are:
    @param inst: Instance of the Digital Alarm
    @param ED: Expression decorator instance
    @param thePlugin: object representing the java plug-in 

    This function returns:
    code for given alarm
    """
    DAName = inst.getAttributeData("DeviceIdentification:Name")
    DAI = inst.getAttributeData("FEDeviceEnvironmentInputs:Input")
    Delay = inst.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
    Description = inst.getAttributeData("DeviceDocumentation:Description")
    Master = inst.getAttributeData("LogicDeviceDefinitions:Master")
    # EnableCondition = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter6")  # this should be replaced with a new column in the spec

    bufferWriteLogic = ""
    CRLF = System.getProperty("line.separator")

    # Input condition
    if DAI != "" and thePlugin.isString(DAI) and DAI.strip().lower() <> "logic":
        DA_IOError, DA_IOSimu = getIOErrorSimuCondition(DAI, ED, thePlugin.getUnicosProject(), [DAName] + Master.split(','))  # return DB_SIMPLE if it exists to avoid 'Too Many Symbols' error for large applications

        bufferWriteLogic += '''    $DAName$.I := ''' + ED.decorateExpression(DAI) + ''';''' + CRLF  # return DB_SIMPLE if it exists to avoid 'Too Many Symbols' error for large applications
        bufferWriteLogic += '''    $DAName$.IOError := ''' + DA_IOError + ''';''' + CRLF
        bufferWriteLogic += '''    $DAName$.IOSimu := ''' +  DA_IOSimu + ''';''' + CRLF

    # Delay Alarm conditions
    if Delay.strip() != "" and thePlugin.isString(Delay) and Delay.strip().lower() <> "logic":
        bufferWriteLogic += '''    $DAName$.PAlDt := REAL_TO_INT(''' + Delay + '''.PosSt);''' + CRLF

    # Enable Alarm conditions
    # if EnableCondition is empty:      CONFIGRUED TRUE
    # if EnableCondition is "logic":    NOT CONFIGRUED TRUE
    # if EnableCondition is sth else:   CONFIGRUED with CONDITION
    # if EnableCondition.strip() == "":
    #    bufferWriteLogic += '''\tDB_DA_All.DA_SET.$DAName$.AuEAl := TRUE;''' + CRLF
    # elif EnableCondition.strip().lower() <> "logic":
    #    bufferWriteLogic += '''\tDB_DA_All.DA_SET.$DAName$.AuEAl := ''' + ED.decorateExpression(EnableCondition, "", [], True) + ''';''' + CRLF

    if len(bufferWriteLogic) > 0:
        bufferWriteLogic =  CRLF + '''    (* $Description$ *)''' + CRLF + bufferWriteLogic

    return bufferWriteLogic


def writeSingleConfiguredAAParameters(inst, ED, thePlugin):
    """
    This function will write PLC code concerning one instance of the analog alarms. It will only write the code concerning the parameters of the analog alarm which are configured in the specification file.

    The arguments are:
    @param inst: Instance of the Analog Alarm
    @param ED: Expression decorator instance
    @param thePlugin: object representing the java plug-in 

    This function returns:
    code for given alarm
    """
    AAName = inst.getAttributeData("DeviceIdentification:Name")
    AAI = inst.getAttributeData("FEDeviceEnvironmentInputs:Input")
    Delay = inst.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
    Master = inst.getAttributeData("LogicDeviceDefinitions:Master")
    HHAA = inst.getAttributeData("FEDeviceManualRequests:HH Alarm")
    HWAA = inst.getAttributeData("FEDeviceManualRequests:H Warning")
    LWAA = inst.getAttributeData("FEDeviceManualRequests:L Warning")
    LLAA = inst.getAttributeData("FEDeviceManualRequests:LL Alarm")
    Description = inst.getAttributeData("DeviceDocumentation:Description")
    EnableCondition = inst.getAttributeData("FEDeviceAlarm:Enable Condition")

    bufferWriteLogic = ""
    CRLF = System.getProperty("line.separator")

    # Input condition
    if AAI != "" and thePlugin.isString(AAI) and AAI.strip().lower() <> "logic":
        AA_IOError, AA_IOSimu = getIOErrorSimuCondition(AAI, ED, thePlugin.getUnicosProject(), [AAName] + Master.split(','))  # return DB_SIMPLE if it exists to avoid 'Too Many Symbols' error for large applications

        bufferWriteLogic += '''    $AAName$.I := ''' + ED.decorateExpression(AAI) + ''';''' + CRLF  # return DB_SIMPLE if it exists to avoid 'Too Many Symbols' error for large applications
        bufferWriteLogic += '''    $AAName$.IOError := ''' + AA_IOError + ''';''' + CRLF
        bufferWriteLogic += '''    $AAName$.IOSimu := ''' +  AA_IOSimu + ''';''' + CRLF

    # Delay Alarm conditions
    if Delay.strip() != "" and thePlugin.isString(Delay) and Delay.strip().lower() <> "logic":
        bufferWriteLogic += '''    $AAName$.PAlDt := REAL_TO_INT(''' + Delay + '''.PosSt);''' + CRLF

    # NOTE: regarding Enable Alarm conditions

    # if Threshold is empty:                                CONFIGURED FALSE
    # if Threshold not empty and EnableCondition empty      NOT CONFIGURED TRUE
    # if Threshold not empty and EnableCondition 'logic'    NOT CONFIGURED TRUE
    # if Threshold not empty and EnableCondition oth        CONFIGURED with CONDITION

    # HH conditions
    if HHAA != "" and thePlugin.isString(HHAA) and HHAA.lower() <> "logic":
        bufferWriteLogic += '''    $AAName$.HH := ''' + ED.decorateExpression(HHAA)  + ''';''' + CRLF
    if HHAA.strip() == "":
        bufferWriteLogic += '''    $AAName$.AuEHH := FALSE;''' + CRLF
    elif EnableCondition.strip() != "" and thePlugin.isString(EnableCondition) and EnableCondition.strip().lower() <> "logic":
        bufferWriteLogic += '''    $AAName$.AuEHH := ''' + ED.decorateExpression(EnableCondition) + ''';''' + CRLF

    # HW conditions
    if HWAA != "" and thePlugin.isString(HWAA) and HWAA.lower() <> "logic":
        bufferWriteLogic += '''    $AAName$.H := ''' + ED.decorateExpression(HWAA) + ''';''' + CRLF
    if HWAA.strip() == "":
        bufferWriteLogic += '''    $AAName$.AuEH := FALSE;''' + CRLF
    elif EnableCondition.strip() != "" and thePlugin.isString(EnableCondition) and EnableCondition.strip().lower() <> "logic":
        bufferWriteLogic += '''    $AAName$.AuEH := ''' + ED.decorateExpression(EnableCondition) + ''';''' + CRLF

    # LW conditions
    if LWAA != "" and thePlugin.isString(LWAA) and LWAA.lower() <> "logic":
        bufferWriteLogic += '''    $AAName$.L := ''' + ED.decorateExpression(LWAA) + ''';''' + CRLF
    if LWAA.strip() == "":
        bufferWriteLogic += '''    $AAName$.AuEL := FALSE;''' + CRLF
    elif EnableCondition.strip() != "" and thePlugin.isString(EnableCondition) and EnableCondition.strip().lower() <> "logic":
        bufferWriteLogic += '''    $AAName$.AuEL := ''' + ED.decorateExpression(EnableCondition) + ''';''' + CRLF

    # LL conditions
    if LLAA != "" and thePlugin.isString(LLAA) and LLAA.lower() <> "logic":
        bufferWriteLogic += '''    $AAName$.LL := ''' + ED.decorateExpression(LLAA) + ''';''' + CRLF
    if LLAA.strip() == "":
        bufferWriteLogic += '''    $AAName$.AuELL := FALSE;''' + CRLF
    elif EnableCondition.strip() != "" and thePlugin.isString(EnableCondition) and EnableCondition.strip().lower() <> "logic":
        bufferWriteLogic += '''    $AAName$.AuELL := ''' + ED.decorateExpression(EnableCondition) + ''';''' + CRLF

    if len(bufferWriteLogic) > 0:
        bufferWriteLogic =  CRLF + '''    (* $Description$ *)''' + CRLF + bufferWriteLogic

    return bufferWriteLogic


def writeConfiguredDAParameters(thePlugin, theDigitalAlarms, theDigitalAlarmsMultiple, DAListPosition):
    """
    This function will write PLC code concerning the digital alarms. It will only write the code concerning the parameters of the digital alarms which are configured in the specification file.

    The arguments are:
    @param thePlugin: object representing the java plug-in 
    @param theDigitalAlarms: vector containing the simple digital alarms which have to be treated
    @param theDigitalAlarmsMultiple: vector containing the multiple digital alarms which have to be treated
    @param DAListPosition: list containing the position of the master for each multiple alarm. The ith element of DAListPosition should corresponds to the ith element of theDigitalAlarmsMultiple

    This function returns:
    nothing
    """

    ED = ucpc_library.shared_decorator.ExpressionDecorator()
    CRLF = System.getProperty("line.separator")

    #thePlugin.writeDebugInUABLog("defaultAlarmsTemplate: executing writeConfiguredAlarmParameters function")

    # Step 1.3: Interlock Conditions (both for analog and digital alarms) and IOError/IOSimu for the Alarms (both for analog and digital alarms)
    thePlugin.writeTIALogic('''
(*Configured alarm parameters: Interlock Conditions to fill in according to the logic spec *)
(*Digital Interlock Conditions to fill in according to the logic spec*)
    // Simple Type DA conditions''' + CRLF)
    for inst in theDigitalAlarms:
        thePlugin.writeTIALogic(writeSingleConfiguredDAParameters(inst, ED, thePlugin))

    thePlugin.writeTIALogic('''    // Multiple Type DA conditions''' + CRLF)
    i = 0
    for inst in theDigitalAlarmsMultiple:
        positionDAMaster = DAListPosition[i]
        i = i + 1
        if positionDAMaster == 0:
            thePlugin.writeTIALogic(writeSingleConfiguredDAParameters(inst, ED, thePlugin))
        else:
            DAName = inst.getAttributeData("DeviceIdentification:Name")
            Description = inst.getAttributeData("DeviceDocumentation:Description")
            masterString = inst.getAttributeData("LogicDeviceDefinitions:Master").replace(",", " ")
            masterList = masterString.split()
            firstMasterName = masterList[0]
            thePlugin.writeTIALogic( CRLF + '''    (* $Description$ *)''' + CRLF)
            thePlugin.writeTIALogic('''    // For the Digital Alarm "$DAName$" the conditions have been created in the section: $firstMasterName$_DL''' + CRLF)


def writeConfiguredAAParameters(thePlugin, theUnicosProject, theAnalogAlarms, theAnalogAlarmsMultiple, AAListPosition):
    """
    This function will write PLC code concerning the analog alarms. It will only write the code concerning the parameters of the analog alarms which are configured in the specification file.

    The arguments are:
    @param thePlugin: object representing the java plug-in 
    @param theAnalogAlarms: vector containing the simple analog alarms which have to be treated
    @param theAnalogAlarmsMultiple: vector containing the multiple analog alarms which have to be treated
    @param AAListPosition: list containing the position of the master for each multiple alarm. The ith element of AAListPosition should corresponds to the ith element of theAnalogAlarmsMultiple

    This function returns:
    nothing
    """

    ED = ucpc_library.shared_decorator.ExpressionDecorator()
    CRLF = System.getProperty("line.separator")

    thePlugin.writeTIALogic(CRLF + '''(*Analog Interlock Conditions to fill in according to the logic spec*)''' + CRLF)
    thePlugin.writeTIALogic('''    // Simple Type AA conditions''' + CRLF)
    for inst in theAnalogAlarms:
        thePlugin.writeTIALogic(writeSingleConfiguredAAParameters(inst, ED, thePlugin))

    thePlugin.writeTIALogic('''    // Multiple Type AA conditions''' + CRLF)
    i = 0
    for inst in theAnalogAlarmsMultiple:
        positionAAMaster = AAListPosition[i]
        i = i + 1
        if positionAAMaster == 0:
            thePlugin.writeTIALogic(writeSingleConfiguredAAParameters(inst, ED, thePlugin))

        else:
            AAName = inst.getAttributeData("DeviceIdentification:Name")
            Description = inst.getAttributeData("DeviceDocumentation:Description")
            masterString = inst.getAttributeData("LogicDeviceDefinitions:Master").replace(",", " ")
            masterList = masterString.split()
            firstMasterName = masterList[0]
            thePlugin.writeTIALogic( CRLF + '''    (* $Description$ *)''' + CRLF)
            thePlugin.writeTIALogic('''    // For the Analog Alarm "$AAName$" the conditions have been created in the section: $firstMasterName$_DL''' + CRLF)


def logicErrorSimuAssignment(thePlugin, theUnicosProject, allTheDigitalAlarms, allTheAnalogAlarms, name):
    """
    This function will write PLC code concerning the digital alarms and the analogAlarms. More particularly it will write the code concerning the IOError and IOSimu. A link is created between the alarms and their master in order to propagate the IOError and the IOSimu signal coming from the alarms to their master.
    IOError is typically true when there is a problem in the INPUT-OUTPUT card.
    IOSimu is typically true when we pass on input or output in forced mode.

    The arguments are:
    @param thePlugin: object representing the java plug-in 
    @param theUnicosProject: object which represents the specification file,
    @param allTheDigitalAlarms: vector containing the digital alarms which have to be treated
    @param allTheAnalogAlarms: vector containing the analog alarms which have to be treated
    @param name: string containing the name of the master

    This function returns:
    nothing
    """

    #thePlugin.writeDebugInUABLog("defaultAlamrsTemplate: executing logicErrorSimuAssignment function")

    CRLF = System.getProperty("line.separator")

    # Step 1.5. OnOff IoSimu and IoError: Adding of the IoError from the Logic related to the OnOff object
    thePlugin.writeTIALogic('''

(*Adding of the IoError from the Logic related to $name$*********)
$name$.IoError := DB_ERROR_SIMU.$name$_DL_E OR''' + CRLF)
    # First with the theDigitalAlarms
    for inst in allTheDigitalAlarms:
        DAName = inst.getAttributeData("DeviceIdentification:Name")
        thePlugin.writeTIALogic('''       ''' + DAName + '''.IOErrorW OR''' + CRLF)

    # Second with the theAnalogAlarms
    for inst in allTheAnalogAlarms:
        AAName = inst.getAttributeData("DeviceIdentification:Name")
        thePlugin.writeTIALogic('''       ''' + AAName + '''.IOErrorW OR''' + CRLF)

    thePlugin.writeTIALogic('''       0;

(*Adding of the IoSimu from the Logic related to $name$*********)
$name$.IoSimu := DB_ERROR_SIMU.$name$_DL_S OR''' + CRLF)

    # First with the theDigitalAlarms
    for inst in allTheDigitalAlarms:
        DAName = inst.getAttributeData("DeviceIdentification:Name")
        thePlugin.writeTIALogic('''       ''' + DAName + '''.IoSimuW OR''' + CRLF)

    # Second with the theAnalogAlarms
    for inst in allTheAnalogAlarms:
        AAName = inst.getAttributeData("DeviceIdentification:Name")
        thePlugin.writeTIALogic('''       ''' + AAName + '''.IoSimuW OR''' + CRLF)

    thePlugin.writeTIALogic('''       0;''')


def AuAlAckAlarmsMethod(thePlugin, theUnicosProject, name, master, theDigitalAlarms, theDigitalAlarmsMultiple, theAnalogAlarms, theAnalogAlarmsMultiple, DAListPosition, AAListPosition):
    """
    This function will write PLC code concerning the digital alarms and the analogAlarms. More particularly it will write the code concerning the auto acknowledgment. A link is created between the alarms and their master in order to acknowledge the alarm if the master is acknowledged.

    The arguments are:
    @param thePlugin: object representing the java plug-in 
    @param theUnicosProject: object which represents the specification file,
    @param name: string containing the name of the master
    @param theDigitalAlarms: vector containing the simple digital alarms which have to be treated
    @param theDigitalAlarmsMultiple: vector containing the multiple digital alarms which have to be treated
    @param theAnalogAlarms: vector containing the simple analog alarms which have to be treated
    @param theAnalogAlarmsMultiple: vector containing the multiple analog alarms which have to be treated
    @param DAListPosition: list containing the position of the master for each multiple alarm. The ith element of DAListPosition should corresponds to the ith element of theDigitalAlarmsMultiple
    @param AAListPosition: list containing the position of the master for each multiple alarm. The ith element of AAListPosition should corresponds to the ith element of theAnalogAlarmsMultiple

    This function returns:
    nothing
    """

    #thePlugin.writeDebugInUABLog("defaultAlamrsTemplate: executing AuAlAckAlarmsMethod function")
    #thePlugin.writeInUABLog('''name = $name$. master = $master$''')
    CRLF = System.getProperty("line.separator")
    # Step 1.6: Instantiation of the AuAlAck for the Alarms objects related to the ANALOG object

    thePlugin.writeTIALogic('''

(*instantiation of the AuAlAck for the DigitalAlarm objects related to $name$*********)
    // Simple Type DA instantiation''')
    for inst in theDigitalAlarms:
        DAName = inst.getAttributeData("DeviceIdentification:Name")
        thePlugin.writeTIALogic('''
    $DAName$.AuAlAck:=$name$.E_MAlAckR OR $name$.AuAlAck;''' + CRLF)

    thePlugin.writeTIALogic('''    // Multiple Type DA instantiation''')
    i = 0
    for inst in theDigitalAlarmsMultiple:
        DAName = inst.getAttributeData("DeviceIdentification:Name")
        positionDAMaster = DAListPosition[i]
        i = i + 1
        if positionDAMaster == 0:
            alarmMaster = inst.getAttributeData("LogicDeviceDefinitions:Master").replace(",", " ")
            AuAlAckAssignment = '0'
            alarmMasterList = alarmMaster.split()
            for masterElement in alarmMasterList:
                AuAlAckAssignment = AuAlAckAssignment + " OR " + CRLF + masterElement + ".E_MAlAckR OR" + CRLF + masterElement + ".AuAlAck"
            thePlugin.writeTIALogic('''
    $DAName$.AuAlAck:=''' + AuAlAckAssignment + ''';''')

    thePlugin.writeTIALogic('''
(*instantiation of the AuAlAck for the AnalogAlarm objects related to $name$*********)
    // Simple Type AA instantiation''')
    for inst in theAnalogAlarms:
        AAName = inst.getAttributeData("DeviceIdentification:Name")
        thePlugin.writeTIALogic('''
    $AAName$.AuAlAck:=$name$.E_MAlAckR OR $name$.AuAlAck;''' + CRLF)

    thePlugin.writeTIALogic('''
    // Multiple Type AA instantiation''')
    i = 0
    for inst in theAnalogAlarmsMultiple:
        AAName = inst.getAttributeData("DeviceIdentification:Name")
        positionAAMaster = AAListPosition[i]
        i = i + 1
        if positionAAMaster == 0:
            alarmMaster = inst.getAttributeData("LogicDeviceDefinitions:Master").replace(",", " ")
            AuAlAckAssignment = '0'
            alarmMasterList = alarmMaster.split()
            for masterElement in alarmMasterList:
                AuAlAckAssignment = AuAlAckAssignment + " OR " + CRLF + masterElement + ".E_MAlAckR OR " + CRLF + masterElement + ".AuAlAck"
            thePlugin.writeTIALogic('''
    $AAName$.AuAlAck:=''' + AuAlAckAssignment + ''';''' + CRLF)


def interlockAlarmsMethod(thePlugin, theUnicosProject, allTheDASiAlarms, allTheAASiAlarms, allTheDAFsAlarms, allTheAAFsAlarms, allTheDATsAlarms, allTheAATsAlarms, allTheDAAlAlarms, allTheAAAlAlarms, name):
    """
    This function will write PLC code concerning the digital alarms and the analogAlarms. More particularly it will write the code concerning the interlock.

    The arguments are:
    @param thePlugin: object representing the java plug-in 
    @param theUnicosProject: object which represents the specification file,
    @param allTheDASiAlarms: vector containing the digital alarms of SI type which have to be treated
    @param allTheAASiAlarms: vector containing the analog alarms of SI type which have to be treated
    @param allTheDAFsAlarms: vector containing the digital alarms of FS type which have to be treated
    @param allTheAAFsAlarms: vector containing the analog alarms of FS type which have to be treated
    @param allTheDATsAlarms: vector containing the digital alarms of TS type which have to be treated
    @param allTheAATsAlarms: vector containing the analog alarms of TS type which have to be treated
    @param allTheDAAlAlarms: vector containing the digital alarms of AL type which have to be treated
    @param allTheAAAlAlarms: vector containing the analog alarms of AL type which have to be treated
    @param name: string containing the name of the master

    This function returns:
    nothing
    """

    #thePlugin.writeDebugInUABLog("defaultAlamrsTemplate: executing interlockAlarmsMethod function")

    # Step 1.7: Interlock: Both for DA and AA. We fix the 2 kind of interlocks for Field Objects: SI and ST
    thePlugin.writeTIALogic('''

(*Interlock*************************)''')

    # Start Interlock (SI)
    thePlugin.writeTIALogic('''
$name$.StartI := 
    // Start Interlock for DA''')
    generatedTextDA = theUnicosProject.createSectionText(allTheDASiAlarms, 1, 1, '''
    #DeviceIdentification:Name#.ISt OR''')
    thePlugin.writeTIALogic(generatedTextDA)

    thePlugin.writeTIALogic('''
    // Start Interlock for AA''')
    generatedTextAA = theUnicosProject.createSectionText(allTheAASiAlarms, 1, 1, '''
    #DeviceIdentification:Name#.ISt OR''')
    thePlugin.writeTIALogic(generatedTextAA)
    thePlugin.writeTIALogic('''
    0;''')

    # Full Stop Interlock (FS)
    thePlugin.writeTIALogic('''
$name$.FuStopI := 
    // Full Stop Interlock for DA''')
    generatedTextDA = theUnicosProject.createSectionText(allTheDAFsAlarms, 1, 1, '''
    #DeviceIdentification:Name#.ISt OR''')
    thePlugin.writeTIALogic(generatedTextDA)

    thePlugin.writeTIALogic('''
    // Full Stop Interlock for AA''')
    generatedTextAA = theUnicosProject.createSectionText(allTheAAFsAlarms, 1, 1, '''
    #DeviceIdentification:Name#.ISt OR''')
    thePlugin.writeTIALogic(generatedTextAA)
    thePlugin.writeTIALogic('''
    0;''')

    # Temporary Stop Interlock (TS)
    thePlugin.writeTIALogic('''
$name$.TStopI := 
    // Temporary Stop Interlock for DA''')
    generatedTextDA = theUnicosProject.createSectionText(allTheDATsAlarms, 1, 1, '''
    #DeviceIdentification:Name#.ISt OR''')
    thePlugin.writeTIALogic(generatedTextDA)

    thePlugin.writeTIALogic('''
    // Temporary Stop Interlock for AA''')
    generatedTextAA = theUnicosProject.createSectionText(allTheAATsAlarms, 1, 1, '''
    #DeviceIdentification:Name#.ISt OR''')
    thePlugin.writeTIALogic(generatedTextAA)
    thePlugin.writeTIALogic('''
    0;''')

    # Al (AL)
    thePlugin.writeTIALogic('''
$name$.Al := 
    // Alarm for DA''')
    generatedTextDA = theUnicosProject.createSectionText(allTheDAAlAlarms, 1, 1, '''
    #DeviceIdentification:Name#.ISt OR''')
    thePlugin.writeTIALogic(generatedTextDA)

    thePlugin.writeTIALogic('''
    // Alarm for AA''')
    generatedTextAA = theUnicosProject.createSectionText(allTheAAAlAlarms, 1, 1, '''
    #DeviceIdentification:Name#.ISt OR''')
    thePlugin.writeTIALogic(generatedTextAA)
    thePlugin.writeTIALogic('''
    0;''')


def blockAlarmsMethod(thePlugin, theUnicosProject, allTheDigitalAlarms, allTheAnalogAlarms, name):
    """
    This function will write PLC code concerning the digital alarms and the analogAlarms. More particularly it will write the code concerning the blocked alarm. When an alarm is blocked the corresponding master have to know it that is why we create a link.

    The arguments are:
    @param thePlugin: object representing the java plug-in 
    @param theUnicosProject: object which represents the specification file,
    @param allTheDigitalAlarms: vector containing the digital alarms which have to be treated
    @param allTheAnalogAlarms: vector containing the analog alarms which have to be treated
    @param name: string containing the name of the master

    This function returns:
    nothing
    """

    # Step 1.8: Blocked Alarm warning: both for DA and AA
    thePlugin.writeTIALogic('''

(*Blocked Alarm warning ********************)
$name$.AlB := ''')

    generatedText = theUnicosProject.createSectionText(allTheDigitalAlarms, 1, 1, '''
    #DeviceIdentification:Name#.MAlBRSt OR''')
    thePlugin.writeTIALogic(generatedText)

    generatedText = theUnicosProject.createSectionText(allTheAnalogAlarms, 1, 1, '''
    #DeviceIdentification:Name#.MAlBRSt OR''')
    thePlugin.writeTIALogic(generatedText)
    thePlugin.writeTIALogic('''
    0;''')


def writeSingleNotConfiguredDAParameters(inst, thePlugin):
    """
    This function will write PLC code concerning one instance of the analog alarms. It will only write the code concerning the parameters of the analog alarm which are not configured in the specification file.

    The arguments are:
    @param inst: Instance of the Analog Alarm
    @param thePlugin: object representing the java plug-in 

    This function returns:
    code for given alarm
    """
    DAName = inst.getAttributeData("DeviceIdentification:Name")
    DAI = inst.getAttributeData("FEDeviceEnvironmentInputs:Input")
    Delay = inst.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
    Description = inst.getAttributeData("DeviceDocumentation:Description")
    #EnableCondition = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter6")

    bufferWriteLogic = ""
    CRLF = System.getProperty("line.separator")

    # Input conditions
    if DAI == "" or DAI.strip().lower() == "logic":
        bufferWriteLogic += '''    $DAName$.I := 0; // To complete 
    $DAName$.IOError := 0; // To complete 
    $DAName$.IOSimu := 0; // To complete''' + CRLF

    # Delay Alarm conditions
    if Delay.strip().lower() == "logic":
        bufferWriteLogic += ''' $DAName$.PAlDt := 0; // To complete''' + CRLF

    # Enable Alarm conditions
    # if EnableCondition is empty:      CONFIGRUED TRUE
    # if EnableCondition is "logic":    NOT CONFIGRUED TRUE
    # if EnableCondition is sth else:   CONFIGRUED with CONDITION
    # if EnableCondition.strip().lower() == "logic":
    #    bufferWriteLogic += '''\tDB_DA_All.DA_SET.$DAName$.AuEAl := TRUE;''' + CRLF

    # Description
    if len(bufferWriteLogic) > 0:
        bufferWriteLogic =  CRLF + '''    (* $Description$ *)''' + CRLF + bufferWriteLogic

    return bufferWriteLogic


def writeSingleNotConfiguredAAParameters(inst, thePlugin):
    """
    This function will write PLC code concerning one instance of the analog alarms. It will only write the code concerning the parameters of the analog alarm which are not configured in the specification file.

    The arguments are:
    @param inst: Instance of the Analog Alarm
    @param thePlugin: object representing the java plug-in 

    This function returns:
    code for given alarm
    """
    AAName = inst.getAttributeData("DeviceIdentification:Name")
    AAI = inst.getAttributeData("FEDeviceEnvironmentInputs:Input")
    Delay = inst.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
    HHAA = inst.getAttributeData("FEDeviceManualRequests:HH Alarm")
    HWAA = inst.getAttributeData("FEDeviceManualRequests:H Warning")
    LWAA = inst.getAttributeData("FEDeviceManualRequests:L Warning")
    LLAA = inst.getAttributeData("FEDeviceManualRequests:LL Alarm")
    Description = inst.getAttributeData("DeviceDocumentation:Description")
    EnableCondition = inst.getAttributeData("FEDeviceAlarm:Enable Condition")

    bufferWriteLogic = ""
    CRLF = System.getProperty("line.separator")

    # Input conditions
    if AAI == "" or AAI.strip().lower() == "logic":
        bufferWriteLogic += '''    $AAName$.I := 0.0; // To complete 
    $AAName$.IOError := 0; // To complete 
    $AAName$.IOSimu := 0; // To complete ''' + CRLF

    # Delay Alarm conditions
    if Delay.strip().lower() == "logic":
        bufferWriteLogic += '''    $AAName$.PAlDt := 0; // To complete''' + CRLF

    # Alarm thresholds
    # NOTE: regarding Enable Alarm conditions

    # if Threshold is empty:                                CONFIGURED FALSE
    # if Threshold not empty and EnableCondition empty      NOT CONFIGURED TRUE
    # if Threshold not empty and EnableCondition 'logic'    NOT CONFIGURED TRUE
    # if Threshold not empty and EnableCondition oth        CONFIGURED with CONDITION

    # HH conditions
    if HHAA.strip().lower() == "logic":
        bufferWriteLogic += '''    $AAName$.HH := 0.0; // To complete''' + CRLF
    if HHAA.strip() != "":
        if EnableCondition.strip() == "":
            bufferWriteLogic += '''    $AAName$.AuEHH := TRUE;''' + CRLF
        elif EnableCondition.strip().lower() == "logic":
            bufferWriteLogic += '''    $AAName$.AuEHH := TRUE; // To complete''' + CRLF

    # HW conditions
    if HWAA.strip().lower() == "logic":
        bufferWriteLogic += '''    $AAName$.H := 0.0; // To complete''' + CRLF
    if HWAA.strip() != "":
        if EnableCondition.strip() == "":
            bufferWriteLogic += '''    $AAName$.AuEH := TRUE;''' + CRLF
        elif EnableCondition.strip().lower() == "logic":
            bufferWriteLogic += '''    $AAName$.AuEH := TRUE; // To complete''' + CRLF

    # LW conditions
    if LWAA.strip().lower() == "logic":
        bufferWriteLogic += '''    $AAName$.L := 0.0; // To complete''' + CRLF
    if LWAA.strip() != "":
        if EnableCondition.strip() == "":
            bufferWriteLogic += '''    $AAName$.AuEL := TRUE;''' + CRLF
        elif EnableCondition.strip().lower() == "logic":
            bufferWriteLogic += '''    $AAName$.AuEL := TRUE; // To complete''' + CRLF

    # LL conditions
    if LLAA.strip().lower() == "logic":
        bufferWriteLogic += '''    $AAName$.LL := 0.0; // To complete''' + CRLF
    if LLAA.strip() != "":
        if EnableCondition.strip() == "":
            bufferWriteLogic += '''    $AAName$.AuELL := TRUE;''' + CRLF
        elif EnableCondition.strip().lower() == "logic":
            bufferWriteLogic += '''    $AAName$.AuELL := TRUE; // To complete''' + CRLF

    if len(bufferWriteLogic) > 0:
        bufferWriteLogic =  CRLF + '''    (* $Description$ *)''' + CRLF + bufferWriteLogic

    return bufferWriteLogic


def writeNotConfiguredDAParameters(thePlugin, theDigitalAlarms, theDigitalAlarmsMultiple, DAListPosition):
    """
    This function will write PLC code concerning the digital alarms. It will only write the code concerning the parameters of the digital alarms which are not configured in the specification file.

    The arguments are:
    @param thePlugin: object representing the java plug-in 
    @param theDigitalAlarms: vector containing the simple digital alarms which have to be treated
    @param theDigitalAlarmsMultiple: vector containing the multiple digital alarms which have to be treated
    @param DAListPosition: list containing the position of the master for each multiple alarm. The ith element of DAListPosition should corresponds to the ith element of theDigitalAlarmsMultiple

    This function returns:
    nothing
    """

    CRLF = System.getProperty("line.separator")

    # Step 1.3: Interlock Conditions (both for analog and digital alarms) and IOError/IOSimu for the Alarms (both for analog and digital alarms)
    thePlugin.writeTIALogic('''
(*Not configured alarm parameters: Interlock Conditions to fill in according to the logic spec *)
(*Digital Interlock Conditions to fill in according to the logic spec*)
    // Simple Type DA conditions''' + CRLF)

    for inst in theDigitalAlarms:
        thePlugin.writeTIALogic(writeSingleNotConfiguredDAParameters(inst, thePlugin))

    thePlugin.writeTIALogic('''    // Multiple Type DA conditions''' + CRLF)

    i = 0
    for inst in theDigitalAlarmsMultiple:
        positionDAMaster = DAListPosition[i]
        i = i + 1
        if positionDAMaster == 0:
            thePlugin.writeTIALogic(writeSingleNotConfiguredDAParameters(inst, thePlugin))


def writeNotConfiguredAAParameters(thePlugin, theAnalogAlarms, theAnalogAlarmsMultiple, AAListPosition):
    """
    This function will write PLC code concerning the analog alarms. It will only write the code concerning the parameters of the analog alarms which are not configured in the specification file.

    The arguments are:
    @param thePlugin: object representing the java plug-in 
    @param theAnalogAlarms: vector containing the simple analog alarms which have to be treated
    @param theAnalogAlarmsMultiple: vector containing the multiple analog alarms which have to be treated
    @param AAListPosition: list containing the position of the master for each multiple alarm. The ith element of AAListPosition should corresponds to the ith element of theAnalogAlarmsMultiple

    This function returns:
    nothing
    """

    CRLF = System.getProperty("line.separator")

    thePlugin.writeTIALogic('''
(*Analog Interlock Conditions to fill in according to the logic spec*)''' + CRLF)
    thePlugin.writeTIALogic('''    // Simple Type AA conditions''' + CRLF)

    for inst in theAnalogAlarms:
        thePlugin.writeTIALogic(writeSingleNotConfiguredAAParameters(inst, thePlugin))

    thePlugin.writeTIALogic('''    // Multiple Type AA conditions''' + CRLF)

    i = 0
    for inst in theAnalogAlarmsMultiple:
        positionAAMaster = AAListPosition[i]
        i = i + 1
        if positionAAMaster == 0:
            thePlugin.writeTIALogic(writeSingleNotConfiguredAAParameters(inst, thePlugin))
