# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Application general check rules
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.utilities import XMLConfigMapper
from research.ch.cern.unicos.plugins.interfaces import APlugin

class ApplicationGeneral_Template(IUnicosTemplate):
    semantic_verifier = 0
    plugin = 0

    def initialize(self):
        self.semantic_verifier = SemanticVerifier.getUtilityInterface()
        self.plugin = APlugin.getPluginInterface()
        self.plugin.writeInUABLog("Application General check rules: initialize")

    def check(self):
        self.plugin.writeInUABLog("Application General check rules: check")

    def begin(self):
        self.plugin.writeInUABLog("Application General check rules: begin")

    def process(self, *params):
        xml_config = params[0]
        self.plugin.writeInUABLog("Application General check rules: processApplicationData")
        unicos_project = self.plugin.getUnicosProject()

        # Specs version
        specs_version = unicos_project.getProjectDocumentation().getSpecsVersion()
        if specs_version is None:
            self.plugin.writeErrorInUABLog("The spec version is missing.")
            self.plugin.writeInUABLog("Please, add the spec version in the ProjectDocumentation worksheet.")
        else:
            try:
                float(specs_version.strip())
            except Exception:
                self.plugin.writeErrorInUABLog("The format of the spec version number (" + specs_version + ") is not correct. It must be a number, either integer or real (X.Y). Please correct it in the ProjectDocumentation worksheet.")

        # PLC declarations
        plc_declarations = xml_config.getPLCDeclarations()

        # Checking all the permitted values
        self.semantic_verifier.checkPermittedValues(unicos_project)

        # check local ID connection number
        plc_name = plc_declarations.get(0).getName()
        plc_type = xml_config.getPLCParameter(plc_name + ":PLCType")

        if plc_type.lower() == "s7-1500":
            local_id = xml_config.getPLCParameter(plc_name + ":SiemensSpecificParameters:PLCS7Connection:LocalId")
            if len(local_id) < 3:
                self.plugin.writeErrorInUABLog("Wrong Local ID value for S7-1500 PLC: " + local_id + ". It must be greater than or equal to 100.")

        # Fast interlock rules
        if self.plugin.getPlcManufacturer().lower() == "siemens":
            from research.ch.cern.unicos.cpc.utilities.siemens import S7Functions
            S7Functions.initialize() #Necessary for processing the fast interlock objects

            FI_type = None
            DA_instances = S7Functions.get_instances_FI("DigitalAlarm") #Fast interlock just indicated in DA objects
            for DA_instance in DA_instances:
                current_FI_type = DA_instance.getAttributeData("LogicDeviceDefinitions:Fast Interlock Type")
                if not FI_type:
                    FI_type = current_FI_type
                elif FI_type != current_FI_type:
                    self.plugin.writeErrorInUABLog("Only one type of Fast Interlock can be selected. Choose either hardware interrupt or cyclic interrupt for all fast interlock objects.")
                    break

    def end(self):
        self.plugin.writeInUABLog("Application General check rules: end")

    def shutdown(self):
        self.plugin.writeInUABLog("Application General check rules: shutdown")
