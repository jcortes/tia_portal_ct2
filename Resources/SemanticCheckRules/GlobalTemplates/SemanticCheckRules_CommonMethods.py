# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
'''
This module holds commonly used functions for the semantic check.
'''


def writeSemanticWarning(thePlugin, deviceTypeName, deviceName, message):
    ''' Function unifies the way we write semantic warning messages.'''
    thePlugin.writeWarningInUABLog(str(deviceTypeName) + " instance: " + deviceName + ". " + message)


def writeSemanticError(thePlugin, deviceTypeName, deviceName, message):
    ''' Function unifies the way we write semantic error messages.'''
    thePlugin.writeErrorInUABLog(str(deviceTypeName) + " instance: " + deviceName + ". " + message)


def checkAlarmAutoAckValue(thePlugin, deviceTypeName, deviceName, autoAlarmAck, hhLimit, llLimit):
    """ There's an error if AutoAck property is defined but no threshould alarm is set.

    Args:
        autoAlarmAck: SCADADeviceAlarms:Alarm Config:Auto Acknowledge from the spec (blank, true, false)
        hhLimit: SCADADeviceAlarms:Analog Thresholds:HH Alarm from the spec
        llLimit: SCADADeviceAlarms:Analog Thresholds:LL Alarm from the spec
    """
    if (autoAlarmAck != "") and (hhLimit == "" and llLimit == ""):
        writeSemanticError(thePlugin, deviceTypeName, deviceName,
                           "AutoAck must be empty if neither HH Alarm nor LL Alarm threshould is defined.")


def checkAlarmMaskedValue(thePlugin, deviceTypeName, deviceName, alarmMasked, hhLimit, hLimit, lLimit, llLimit):
    """ There's an error if Alarm Masked property is defined but no threshould alarm or warning is set.

    Args:
        autoAlarmAck: SCADADeviceAlarms:Alarm Config:Auto Acknowledge from the spec (blank, true, false)
        hhLimit: SCADADeviceAlarms:Analog Thresholds:HH Alarm from the spec
        hLimit: SCADADeviceAlarms:Analog Thresholds:H Warning from the spec
        lLimit: SCADADeviceAlarms:Analog Thresholds:L Warning from the spec
        llLimit: SCADADeviceAlarms:Analog Thresholds:LL Alarm from the spec
    """
    if (alarmMasked != "") and (hhLimit == "" and hLimit == "" and lLimit == "" and llLimit == ""):
        writeSemanticError(thePlugin, deviceTypeName, deviceName,
                           "Alarm Masked must be empty if none of alarm threshould is defined.")

def checkAlarmSMSValue(thePlugin, deviceTypeName, deviceName, alarmSmsCategory, hhLimit, hLimit, lLimit, llLimit):
    """ There's an error if Alarm Masked property is defined but no threshould alarm or warning is set.

    Args:
        autoAlarmAck: SCADADeviceAlarms:Alarm Config:Auto Acknowledge from the spec (blank, true, false)
        hhLimit: SCADADeviceAlarms:Analog Thresholds:HH Alarm from the spec
        hLimit: SCADADeviceAlarms:Analog Thresholds:H Warning from the spec
        lLimit: SCADADeviceAlarms:Analog Thresholds:L Warning from the spec
        llLimit: SCADADeviceAlarms:Analog Thresholds:LL Alarm from the spec
    """
    if (alarmSmsCategory != "") and (hhLimit == "" and hLimit == "" and lLimit == "" and llLimit == ""):
        writeSemanticWarning(thePlugin, deviceTypeName, deviceName,
                           "Alarm SMS Category is specified, but no Alarm Values are defined.")

def checkAlarmMessage(thePlugin, deviceTypeName, deviceName, alarmMessage, hhLimit, hLimit, lLimit, llLimit):
    """ There's an error if Alarm Masked property is defined but no threshould alarm or warning is set.

    Args:
        autoAlarmAck: SCADADeviceAlarms:Alarm Config:Auto Acknowledge from the spec (blank, true, false)
        hhLimit: SCADADeviceAlarms:Analog Thresholds:HH Alarm from the spec
        hLimit: SCADADeviceAlarms:Analog Thresholds:H Warning from the spec
        lLimit: SCADADeviceAlarms:Analog Thresholds:L Warning from the spec
        llLimit: SCADADeviceAlarms:Analog Thresholds:LL Alarm from the spec
    """
    if (alarmMessage != "") and (hhLimit == "" and hLimit == "" and lLimit == "" and llLimit == ""):
        writeSemanticWarning(thePlugin, deviceTypeName, deviceName,
                           "Alarm Message is specified, but no Alarm Values are defined.")

def checkIfSpecifiedObjectExists(thePlugin, instance, attributeName, theSemanticVerifier, theUnicosProject, theCurrentDeviceTypeName, Name):
    """ Check if the object specified in field "attributeName" of theCurrentDeviceTypeName object Name exists in theUnicosProject (i.e. the spec)
    e.g. checkIfSpecifiedObjectExists(instance,"FEDeviceVariables:Default PID Parameters:Setpoint",theUnicosProject,theCurrentDeviceTypeName,Name)
    """
    attr = thePlugin.formatNumberPLC(instance.getAttributeData(attributeName))
    if attr <> "" and thePlugin.isString(attr):
        attrExist = theSemanticVerifier.doesObjectExist(attr, theUnicosProject)
        if attrExist is not True:
            thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: " + Name + ". The " + attributeName + " you specified (" + attr + ") doesn't exist.")


def getMaxNameLength(thePlugin, deviceTypeName, manufacturer):
    maxLength = 23  # if not siemens
    if (manufacturer.lower() == "siemens"):

        if(deviceTypeName.lower() in ["processcontrolobject", "controller", "analog", "anlogdigital", "anado", "onoff", "massflowcontroller", "steppingmotor"]):
            maxLength = 19
        elif (deviceTypeName.lower() == "local"):
            maxLength = 21
        else:
            maxLength = 24
        XMLConfig = thePlugin.getXMLConfig()
        thePLCName = XMLConfig.getPLCDeclarations().get(0).getName()
        PLCType = XMLConfig.getPLCParameter(thePLCName + ":PLCType")
        if PLCType.lower() == "s7-1500":
            maxLength += 101

    return maxLength


def checkNameLength(name, nameLengthLimit, thePlugin, deviceTypeName):
    """ Check if given name length is less than specified limit 
    """
    if len(name) > nameLengthLimit:
        writeSemanticError(thePlugin, deviceTypeName, name, "Max number of letters exceeded in the device type Name: current length = " + str(len(name)) + ". Max length allowed = " + str(nameLengthLimit))
