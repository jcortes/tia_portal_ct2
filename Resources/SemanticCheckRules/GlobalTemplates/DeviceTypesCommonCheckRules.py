# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# DeviceTypes Common check rules
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin
from research.ch.cern.unicos.utilities import DeviceTypeFactory
import re
import os

class AllDeviceTypes_Template(IUnicosTemplate):
    semantic_verifier = 0
    plugin = 0
    current_device_type = 0

    def initialize(self):
        self.semantic_verifier = SemanticVerifier.getUtilityInterface()
        self.plugin = APlugin.getPluginInterface()
        self.plugin.writeInUABLog("Device Types Common check rules: initialize")

    def check(self):
        self.plugin.writeInUABLog("Device Types Common check rules: check")

    def begin(self):
        self.plugin.writeInUABLog("Device Types Common check rules: begin")

    def checkAttributeWithRegex(self, instance, attribute, regex, message):
        attribute_value = instance.getAttributeData(attribute)
        if attribute_value:
            if re.match(ur".*[" + regex + ur"].*", attribute_value):
                device_type_name = str(instance.getDeviceTypeName())
                device_name = instance.getAttributeData("DeviceIdentification:Name")
                self.plugin.writeErrorInUABLog(device_type_name + " instance: " + device_name + ". Illegal character in " + attribute.split(':')[-1] + ": " + attribute_value + ". " + message + ".")

    def check_names_separately(self, instance, attributes, unicos_project):
        for attribute in attributes:
            self.semantic_verifier.isAttributeValueUnique(instance, attribute, unicos_project, True)

    def process(self, *params):
        current_device_type_name = params[0]
        current_device_type_definition = params[1]
        unicos_project = self.plugin.getUnicosProject()
        self.current_device_type = unicos_project.getDeviceType(current_device_type_name)

        # Common check: instances against type definition
        self.semantic_verifier.areInstancesValidToTypeDefinition(self.current_device_type, current_device_type_definition)

        # Master object existance
        self.semantic_verifier.doesMasterObjectExist(self.current_device_type, "LogicDeviceDefinitions:Master", current_device_type_definition, unicos_project)

        # Determine if CustomLogicParameters exist
        device_type_definition = DeviceTypeFactory.getInstance().getDeviceType(current_device_type_name)
        custom_logic_sections = []
        device_type_families = device_type_definition.getAttributeFamily()
        for device_type_family in device_type_families:
            family_name = device_type_family.getAttributeFamilyName()
            if family_name == "LogicDeviceDefinitions":
                attributes = device_type_family.getAttribute()
                for attribute in attributes:
                    if attribute.getAttributeName() == "CustomLogicSections":
                        subattributes = attribute.getAttribute()
                        for subattribute in subattributes:
                            is_spec_attribute = subattribute.getIsSpecificationAttribute()
                            if is_spec_attribute:
                                custom_logic_sections.append(is_spec_attribute.getNameRepresentation())

        for instance in self.current_device_type.getAllDeviceTypeInstances():
            # Common Name semantic rules
            name = instance.getAttributeData("DeviceIdentification:Name")
            expert_name = instance.getAttributeData("DeviceIdentification:Expert Name")
            name_columns = ["DeviceIdentification:Name", "DeviceIdentification:Expert Name"]

            if name.upper() == expert_name.upper():
                self.check_names_separately(instance, name_columns, unicos_project)
            elif not self.semantic_verifier.isInstanceAliasUnique(instance, unicos_project):
                self.plugin.writeErrorInUABLog(str(current_device_type_name) + " instance: " + name + ". Name or Expert Name is not unique within Specification file.")

            # Forbidden characters from PVSS and PLC
            if re.match(ur'.*[\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>|\], /\\\-\.].*', name):  # \u0024 is unicode for dollar sign
                self.plugin.writeErrorInUABLog(str(current_device_type_name) + " instance: " + name + ". Illegal character in Name: " + name + ". None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>|],/\\-. or space or dollar sign.")
            
            # Checking if there is double underscore for all platforms, per IEC-61131-3 section 2.1.2 Identifiers, see UCPC-1389
            if re.match(ur'.*__.*', name):
                self.plugin.writeErrorInUABLog(str(current_device_type_name) + " instance: " + name + ". Double underscore (e.g. '__') is not allowed in the object Name.")

            if re.match(ur'[0-9].*', name):
                if self.plugin.getPlcManufacturer().lower() == "schneider":
                    self.plugin.writeWarningInUABLog(str(current_device_type_name) + " instance: " + name + ". Object Name starts with a number.")
                else:
                    self.plugin.writeErrorInUABLog(str(current_device_type_name) + " instance: " + name + ". Object Name cannot start with a number.")

            # Check forbidden characters in PVSS

            self.checkAttributeWithRegex(instance, "DeviceIdentification:Expert Name",                                     ur'\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>\]|, /\\',       "None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>|],/\\ or space or dollar sign")
            self.checkAttributeWithRegex(instance, "SCADADeviceFunctionals:Access Control Domain",                         ur'\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>\]',             "None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>] or dollar sign")
            self.checkAttributeWithRegex(instance, "SCADADeviceGraphics:Synoptic",                                         ur'\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>\]| \-',         "None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>|]- or space or dollar sign")
            self.checkAttributeWithRegex(instance, "SCADADeviceGraphics:Diagnostic",                                       ur'\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>\]| \-',         "None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>|]- or space or dollar sign")
            self.checkAttributeWithRegex(instance, "SCADADeviceFunctionals:SCADADeviceClassificationTags:Domain",          ur'\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>\] /\\',         "None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>]/\\ or space or dollar sign")
            self.checkAttributeWithRegex(instance, "SCADADeviceFunctionals:SCADADeviceClassificationTags:Nature",          ur'\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>\]| /\\',        "None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>|]/\\ or space or dollar sign")
            self.checkAttributeWithRegex(instance, "SCADADeviceFunctionals:SCADADeviceClassificationTags:Device Links",    ur'\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>\]|/\\',         "None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>|]/\\ or dollar sign")
            self.checkAttributeWithRegex(instance, "SCADADeviceDataArchiving:Boolean Archive",                             ur'\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>\]|\.\-, /\\',   "None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>|].-,/\\ or space or dollar sign")
            self.checkAttributeWithRegex(instance, "SCADADeviceDataArchiving:Analog Archive",                              ur'\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>\]|\.\-, /\\',   "None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>|].-,/\\ or space or dollar sign")
            self.checkAttributeWithRegex(instance, "SCADADeviceDataArchiving:Event Archive",                               ur'\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>\]|\.\-, /\\',   "None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>|].-,/\\ or space or dollar sign")
            
            device_link_list = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Device Links").replace(" ", "").split(",")
            for device_link in device_link_list:
                if not self.semantic_verifier.doesObjectExist(device_link, unicos_project):
                    if not unicos_project.findMatchingInstances("*", "'#DeviceIdentification:Expert Name#'='" + device_link + "'"):
                        self.plugin.writeWarningInUABLog(str(current_device_type_name) + " instance: " + name + ". Linked device does not exists: " + device_link)

            if device_type_definition.doesSpecificationAttributeExist("SCADADeviceAlarms:Alarm Config:SMS Category"):
                self.checkAttributeWithRegex(instance, "SCADADeviceAlarms:Alarm Config:SMS Category", ur'^0-9a-zA-Z_,:', "Allowed characters: 0..9, A..Z, _, a..z:")

            if device_type_definition.doesSpecificationAttributeExist("LogicDeviceDefinitions:Master") and device_type_definition.doesSpecificationAttributeExist("LogicDeviceDefinitions:External Master"):
                master_device = instance.getAttributeData("LogicDeviceDefinitions:Master").strip()
                external_master = instance.getAttributeData("LogicDeviceDefinitions:External Master").strip()
                if master_device and external_master:
                    self.plugin.writeErrorInUABLog(str(current_device_type_name) + " instance: " + name + ". Both Master (" + master_device + ") and External Master (" + external_master + ") are specified. Only one should be filled. External Master is used if the object is controlled by a PCO in a different PLC.")

            FIRST_LOGIC_PARAM_INDEX = 1
            LAST_LOGIC_PARAM_INDEX = 10
            if device_type_definition.doesSpecificationAttributeExist("LogicDeviceDefinitions:CustomLogicParameters:Parameter1"):
                for param in range(FIRST_LOGIC_PARAM_INDEX, LAST_LOGIC_PARAM_INDEX + 1):
                    Lparam = instance.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter" + str(param))
                    if Lparam:
                        if re.match(ur'.*["\'\u0024].*', Lparam):  # \u0024 is unicode for dollar sign
                            self.plugin.writeErrorInUABLog(str(current_device_type_name) + " instance: " + name + ". Illegal character in CustomLogicParameters:Parameter" + str(param) + ": " + Lparam + ". None of the following characters are allowed: \" ' or dollar sign. ")

            
            for logic_section in custom_logic_sections:
                logic_section_file_path = instance.getAttributeData("LogicDeviceDefinitions:CustomLogicSections:" + logic_section).strip()
                if logic_section_file_path:
                    logic_section_filename = os.path.basename(logic_section_file_path)
                    first_character = logic_section_filename[0]
                    if first_character.isdigit():
                        self.plugin.writeErrorInUABLog(str(current_device_type_name) + " instance: " + name + ". " + logic_section + " (" + logic_section_filename + ") cannot start with a digit [0-9]")

            # Checking for the character ";" for the WinCCOA importation line
            description = instance.getAttributeData("DeviceDocumentation:Description")
            if ';' in description:
                self.plugin.writeErrorInUABLog(str(current_device_type_name) + " instance: " + name + ". The character ';' is not allowed (reserved for WinCCOA importation file), column Description")

            remarks = instance.getAttributeData("DeviceDocumentation:Remarks")
            if ';' in remarks:
                self.plugin.writeErrorInUABLog(str(current_device_type_name) + " instance: " + name + ". The character ';' is not allowed (reserved for WinCCOA importation file), column Remarks")

    def end(self):
        self.plugin.writeInUABLog("Device Types Common check rules: end")

    def shutdown(self):
        self.plugin.writeInUABLog("Device Types Common check rules: shutdown")
