# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from Semantic_Generic_Template import Semantic_Generic_Template

class DigitalOutput_Template(Semantic_Generic_Template):

    def process(self, *params):
        current_device_type_name = params[0]
        current_device_type_definition = params[1]
        current_device_type = self.unicos_project.getDeviceType(current_device_type_name)
        self.plugin.writeInUABLog("process in Jython in %s." % self.__class__.__name__)

        name_length_limit = self.getMaxNameLength(str(current_device_type_name), self.the_manufacturer)

        # Fast interlocks
        if self.the_manufacturer.lower() == "siemens":
            from research.ch.cern.unicos.cpc.utilities.siemens import S7Functions
            DO_FI_list = S7Functions.get_instances_FI("DigitalOutput")
            OnOff_FI_list = S7Functions.get_instances_FI("OnOff")

        for instance in current_device_type.getAllDeviceTypeInstances():
            name = instance.getAttributeData("DeviceIdentification:Name")

            # Check the length of the name
            self.checkNameLength(instance, name_length_limit)

            # Check the alarm configuration
            self.checkDependentAttibutes(instance, ["SCADADeviceAlarms:Alarm Config:Auto Acknowledge", "SCADADeviceAlarms:Alarm Config:Masked", "SCADADeviceAlarms:Alarm Config:SMS Category", "SCADADeviceAlarms:Message"], ["SCADADeviceAlarms:Binary State:Alarm On State"], severity="warning")

            # Check the FE Encoding Type
            if (self.the_manufacturer.lower() == "siemens"):
                fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type").strip()
                if fe_type not in ["", "0", "1", "101", "102", "103"]:
                    self.writeSemanticError(instance, "The FE Encoding Type defined " + fe_type + " is not allowed.")
                elif (fe_type == "1"):
                    self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam1", "q[0-9]+\.[0-7]", "Qxx.y, where xx is the Byte and y is the Bit.")

                elif (fe_type == "101"):
                    if self.siemens_plc_declarations:
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam1", "(db)?[0-9]+", "DBxx, where xx is a number.")
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam2", "(dbx)?[0-9]+", "DBXxx, where xx is a number.")
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam3", "[0-9]+", "xx where xx is a number.")
                    else: # TIA Portal
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam1", "[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+(\[[0-9]+\])*)+", "DB_name.variable, where variable can be a variable name or a complex structure.")

                elif fe_type in ["102", "103"]:
                    if self.siemens_plc_declarations:
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam1", "(db)?[0-9]+", "DBxx, where xx is a number.")
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam2", "(dbx)?[0-9]+", "DBXxx, where xx is a number.")
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam3", "[0-9]+", "xx where xx is a number.")
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam4", "(db)?[0-9]+", "DBxx, where xx is a number.")
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam5", "(dbx)?[0-9]+", "DBXxx, where xx is a number.")
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam6", "[0-9]+", "xx where xx is a number.")
                    else: # TIA Portal
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam1", "[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+(\[[0-9]+\])*)+", "DB_name.variable, where variable can be a variable name or a complex structure.")
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam2", "[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+(\[[0-9]+\])*)+", "DB_name.variable, where variable can be a variable name or a complex structure.")

            # Check the Fast Interlock configuration
            if self.the_manufacturer.lower() == "siemens":
                if instance in DO_FI_list:
                    self.checkInterfaceParameter(instance, "1", "FEDeviceIOConfig:FEChannel:InterfaceParam1", "q[0-9]+\.[0-7]", "Qxx.y, where xx is the Byte and y is the Bit.")
                    OnOff_instances = self.unicos_project.getDeviceType("OnOff").getAllDeviceTypeInstances()
                    for OnOff_instance in OnOff_instances:
                        if OnOff_instance not in OnOff_FI_list: # Non-fast interlock OnOff
                            if OnOff_instance.getAttributeData("FEDeviceOutputs:Process Output") == name or OnOff_instance.getAttributeData("FEDeviceOutputs:Process Output Off") == name:
                                self.writeSemanticError(self.plugin, current_device_type_name, name, "An OnOff that is not fast interlock cannot be connected to a fast interlock digital output")
                                break
