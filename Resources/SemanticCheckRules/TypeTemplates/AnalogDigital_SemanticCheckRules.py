# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from Semantic_Generic_Template import Semantic_Generic_Template

class AnalogDigital_Template(Semantic_Generic_Template):

    def process(self, *params):
        current_device_type_name = params[0]
        current_device_type_definition = params[1]
        current_device_type = self.unicos_project.getDeviceType(current_device_type_name)
        self.plugin.writeInUABLog("process in Jython in %s." % self.__class__.__name__)

        name_length_limit = self.getMaxNameLength(str(current_device_type_name), self.the_manufacturer)

        for instance in current_device_type.getAllDeviceTypeInstances():

            # Check the length of the name
            self.checkNameLength(instance, name_length_limit)

            # Check the FEDevice parameters
            self.checkObjectsInAttributes(instance, ["FEDeviceParameters:Warning Time Delay (s)", 
                                                     "FEDeviceParameters:Warning Deadband Value (Unit)"])

            # Check the FEDevice inputs
            self.checkObjectsInAttributes(instance, ["FEDeviceEnvironmentInputs:Feedback On",
                                                     "FEDeviceEnvironmentInputs:Feedback Off"], valid_types=["DigitalInput"])
                                                     
            self.checkObjectsInAttributes(instance, ["FEDeviceEnvironmentInputs:Feedback Analog",
                                                     "FEDeviceEnvironmentInputs:Hardware Analog Output"], valid_types=["AnalogInput", "AnalogInputReal", "AnalogStatus"])
                                                     
            self.checkObjectsInAttributes(instance, ["FEDeviceEnvironmentInputs:Local Drive"], negatable=True, valid_types=["DigitalInput"])

            # Check the FEDevice outputs
            self.checkObjectsInAttributes(instance, ["FEDeviceOutputs:Output On",
                                                     "FEDeviceOutputs:Output Off"], valid_types=["DigitalOutput"])

            # Check the ranges
            self.checkRanges(instance)

            self.checkDeadband(instance, "SCADADeviceDataArchiving:Deadband Value")
            self.checkDeadband(instance, "SCADADriverDataSmoothing:Deadband Value")
            
            self.checkArchiveConfig(instance)
            
            self.checkFormat(instance)
