# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from Semantic_Generic_Template import Semantic_Generic_Template

class MassFlowController_Template(Semantic_Generic_Template):

    def process(self, *params):
        current_device_type_name = params[0]
        current_device_type_definition = params[1]
        current_device_type = self.unicos_project.getDeviceType(current_device_type_name)
        self.plugin.writeInUABLog("process in Jython in %s." % self.__class__.__name__)

        name_length_limit = self.getMaxNameLength(str(current_device_type_name), self.the_manufacturer)

        for instance in current_device_type.getAllDeviceTypeInstances():

            # Check the length of the name
            self.checkNameLength(instance, name_length_limit)

            # Check the FEDevice outputs
            self.checkObjectsInAttributes(instance, ["FEDeviceOutputs:Valve Order"], valid_types=["OnOff"])

            self.checkDeadband(instance, "SCADADeviceDataArchiving:Flow:Deadband Value", "SCADADeviceGraphics:Flow:Format")
            self.checkDeadband(instance, "SCADADeviceDataArchiving:Totalizer:Deadband Value", "SCADADeviceGraphics:Totalizer:Format")
            self.checkDeadband(instance, "SCADADriverDataSmoothing:Flow:Deadband Value", "SCADADeviceGraphics:Flow:Format")
            self.checkDeadband(instance, "SCADADriverDataSmoothing:Totalizer:Deadband Value", "SCADADeviceGraphics:Totalizer:Format")

            self.checkArchiveConfig(instance, archive_mode_attr="SCADADeviceDataArchiving:Flow:Archive Mode", time_filter_attr="SCADADeviceDataArchiving:Flow:Time Filter (s)", deadband_type_attr="SCADADeviceDataArchiving:Flow:Deadband Type", deadband_value_attr="SCADADeviceDataArchiving:Flow:Deadband Value")
            self.checkArchiveConfig(instance, archive_mode_attr="SCADADeviceDataArchiving:Totalizer:Archive Mode", time_filter_attr="SCADADeviceDataArchiving:Totalizer:Time Filter (s)", deadband_type_attr="SCADADeviceDataArchiving:Totalizer:Deadband Type", deadband_value_attr="SCADADeviceDataArchiving:Totalizer:Deadband Value")
            
            self.checkFormat(instance, "SCADADeviceGraphics:Flow:Format")
            self.checkFormat(instance, "SCADADeviceGraphics:Totalizer:Format")