# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from Semantic_Generic_Template import Semantic_Generic_Template

class WordStatus_Template(Semantic_Generic_Template):

    def process(self, *params):
        current_device_type_name = params[0]
        current_device_type_definition = params[1]
        current_device_type = self.unicos_project.getDeviceType(current_device_type_name)
        self.plugin.writeInUABLog("process in Jython in %s." % self.__class__.__name__)

        name_length_limit = self.getMaxNameLength(str(current_device_type_name), self.the_manufacturer)

        for instance in current_device_type.getAllDeviceTypeInstances():

            # Check the length of the name
            self.checkNameLength(instance, name_length_limit)

            # check alarm masked and alarm ack
            self.checkDependentAttibutes(instance, ["SCADADeviceAlarms:Alarm Config:Auto Acknowledge"], ["SCADADeviceAlarms:Alarm Values:High", "SCADADeviceAlarms:Alarm Values:Safety"])
            self.checkDependentAttibutes(instance, ["SCADADeviceAlarms:Alarm Config:Masked"], ["SCADADeviceAlarms:Alarm Values:Low", "SCADADeviceAlarms:Alarm Values:Medium", "SCADADeviceAlarms:Alarm Values:High", "SCADADeviceAlarms:Alarm Values:Safety"])
            self.checkDependentAttibutes(instance, ["SCADADeviceAlarms:Alarm Config:SMS Category", "SCADADeviceAlarms:Message"], ["SCADADeviceAlarms:Alarm Values:Low", "SCADADeviceAlarms:Alarm Values:Medium", "SCADADeviceAlarms:Alarm Values:High", "SCADADeviceAlarms:Alarm Values:Safety"], severity="warning")
            
            self.checkArchiveConfig(instance)

            # Check the FE Encoding Type
            if (self.the_manufacturer.lower() == "siemens"):
                fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type").strip()
                if fe_type not in ["", "0", "1", "100", "101"]:
                    self.writeSemanticError(instance, "The FE Encoding Type defined " + fe_type + " is not allowed.")
                elif (fe_type == "1"):
                    self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam1", "p?i[bw][0-9]+", "PIWxxx or IWxxx or PIBxxx or IBxxx, where xxx is a number.")

                elif (fe_type == "101"):
                    if self.siemens_plc_declarations:
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam1", "(db)?[0-9]+", "DBxx, where xx is a number.")
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam2", "(db[wb])?[0-9]+", "DBWxx or DBBxx, where xx is a number.")
                    else: # TIA Portal
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam1", "[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+(\[[0-9]+\])*)+", "DB_name.variable, where variable can be a variable name or a complex structure.")

                elif (fe_type == "100"):
                    self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam1", "m[wb][0-9]+", "MWxxx or MBxxx, where xxx is a number.")

            # Check the pattern
            pattern = instance.getAttributeData("SCADADeviceGraphics:Pattern")
            if (pattern == "-"):
                self.writeSemanticError(instance, "Use of '-' as a pattern is deprecated. Please change it to empty cell ('').")

            # Check the ranges
            self.checkRanges(instance)
            
            self.checkFormat(instance)
