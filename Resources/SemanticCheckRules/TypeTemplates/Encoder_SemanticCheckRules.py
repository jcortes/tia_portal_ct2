# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from Semantic_Generic_Template import Semantic_Generic_Template

class Encoder_Template(Semantic_Generic_Template):

    def process(self, *params):
        current_device_type_name = params[0]
        current_device_type_definition = params[1]
        current_device_type = self.unicos_project.getDeviceType(current_device_type_name)
        self.plugin.writeInUABLog("process in Jython in %s." % self.__class__.__name__)

        name_length_limit = self.getMaxNameLength(str(current_device_type_name), self.the_manufacturer)

        for instance in current_device_type.getAllDeviceTypeInstances():

            # Check the length of the name
            self.checkNameLength(instance, name_length_limit)

            # Check the ranges
            self.checkRanges(instance)

            # Check the alarm configuration
            self.check5RangeAlertThresholds(instance)

            self.checkDeadband(instance, "SCADADeviceDataArchiving:Deadband Value")
            self.checkDeadband(instance, "SCADADriverDataSmoothing:Deadband Value")

            self.checkDependentAttibutes(instance, ["SCADADeviceAlarms:Alarm Config:Auto Acknowledge"], ["SCADADeviceAlarms:Analog Thresholds:HH Alarm", "SCADADeviceAlarms:Analog Thresholds:LL Alarm"], severity="warning")
            self.checkDependentAttibutes(instance, ["SCADADeviceAlarms:Alarm Config:Masked", "SCADADeviceAlarms:Alarm Config:SMS Category", "SCADADeviceAlarms:Message"], ["SCADADeviceAlarms:Analog Thresholds:HH Alarm", "SCADADeviceAlarms:Analog Thresholds:H Warning", "SCADADeviceAlarms:Analog Thresholds:L Warning", "SCADADeviceAlarms:Analog Thresholds:LL Alarm"], severity="warning")

            self.checkArchiveConfig(instance)
            
            self.checkFormat(instance)
            
            # Check the FE Encoding Type
            if (self.the_manufacturer.lower() == "siemens"):
                fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type").strip()
                if fe_type not in ["", "0", "101", "102"]:
                    self.writeSemanticError(instance, "The FE Encoding Type defined " + fe_type + " is not allowed.")
                else:
                    if (fe_type == "101") or (fe_type == "102"):
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam1", "p?ib[0-9]+", "a BYTE (PIBxxx or IBxxx where xxx is a number).")
