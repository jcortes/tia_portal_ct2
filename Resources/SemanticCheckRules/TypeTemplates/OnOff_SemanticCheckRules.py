# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from Semantic_Generic_Template import Semantic_Generic_Template

class OnOff_Template(Semantic_Generic_Template):

    def process(self, *params):
        current_device_type_name = params[0]
        current_device_type_definition = params[1]
        current_device_type = self.unicos_project.getDeviceType(current_device_type_name)
        self.plugin.writeInUABLog("process in Jython in %s." % self.__class__.__name__)

        name_length_limit = self.getMaxNameLength(str(current_device_type_name), self.the_manufacturer)

        # Fast interlocks
        if self.the_manufacturer.lower() == "siemens":
            from research.ch.cern.unicos.cpc.utilities.siemens import S7Functions
            OnOff_FI_list = S7Functions.get_instances_FI("OnOff")
            FI_output_instance = S7Functions.get_instances_FI("DigitalOutput")
            FI_alarm_instance = S7Functions.get_instances_FI("DigitalAlarm")

        for instance in current_device_type.getAllDeviceTypeInstances():

            # Check the length of the name
            self.checkNameLength(instance, name_length_limit)

            # Check the FEDevice parameters
            self.checkObjectsInAttributes(instance, ["FEDeviceParameters:Warning Time Delay (s)"])

            # Check the FEDevice inputs
            self.checkObjectsInAttributes(instance, ["FEDeviceEnvironmentInputs:Feedback On",
                                                     "FEDeviceEnvironmentInputs:Feedback Off"], valid_types=["DigitalInput"])
                                                     
            self.checkObjectsInAttributes(instance, ["FEDeviceEnvironmentInputs:Local Drive",
                                                     "FEDeviceEnvironmentInputs:Local On",
                                                     "FEDeviceEnvironmentInputs:Local Off"], negatable=True, valid_types=["DigitalInput"])

            # Check the OnOff specific configuration
            fail_safe_open = instance.getAttributeData("FEDeviceParameters:ParReg:Fail-Safe").strip()
            process_output_off = instance.getAttributeData("FEDeviceOutputs:Process Output Off").strip()
            pulse_duration = instance.getAttributeData("FEDeviceParameters:Pulse Duration (s)").strip()

            # Fail Safe Warning
            if fail_safe_open.lower() == "2 do on" or (fail_safe_open.lower() == "on/open" and pulse_duration and not process_output_off) or (fail_safe_open.lower() == "2 do off" and not pulse_duration and process_output_off):
                self.writeSemanticWarning(instance, "The Fail Safe Position '" + fail_safe_open + "' is ambiguous because Start Interlock cannot be applied.")

            # Fail Safe Error
            if not process_output_off and (fail_safe_open.lower() in ["2 do off", "2 do on"]):
                self.writeSemanticError(instance, "The Fail Safe Position '" + fail_safe_open + "' cannot be selected when there is no Process Output Off.")

            # Check the FEDevice outputs
            self.checkObjectsInAttributes(instance, ["FEDeviceOutputs:Process Output", 
                                                     "FEDeviceOutputs:Process Output Off"], valid_types=["DigitalOutput"])

            # Check the labels
            label_on = instance.getAttributeData("SCADADeviceGraphics:Label On").strip()
            label_off = instance.getAttributeData("SCADADeviceGraphics:Label Off").strip()

            if len(label_on) > 13:
                self.writeSemanticWarning(instance, "The LabelOn field's length (" + label_on + ") is > 13 characters. Text may wrap around on the faceplate.")
            if len(label_off) > 13:
                self.writeSemanticWarning(instance, "The LabelOff field's length (" + label_off + ") is > 13 characters. Text may wrap around on the faceplate.")

            # Fast Interlocks
            if self.the_manufacturer.lower() == "siemens":
                if instance in OnOff_FI_list:
                    name = instance.getAttributeData("DeviceIdentification:Name")
                    output_instances = [self.unicos_project.findInstanceByName(instance.getAttributeData("FEDeviceOutputs:Process Output")),
                                        self.unicos_project.findInstanceByName(instance.getAttributeData("FEDeviceOutputs:Process Output Off"))]
                    for output_instance in output_instances:
                        if output_instance and output_instance not in FI_output_instance:
                            self.writeSemanticError(instance, "An OnOff object that is fast interlock must have Fast Interlock DOs connected to the output.")
                    DigitalAlarmInstances = self.unicos_project.getDeviceType("DigitalAlarm").getAllDeviceTypeInstances()
                    for alarm_instance in DigitalAlarmInstances:
                        if alarm_instance not in FI_alarm_instance:
                            alarm_master = alarm_instance.getAttributeData("LogicDeviceDefinitions:Master").replace(","," ").split()
                            if name in alarm_master:
                                self.writeSemanticError(instance, "An OnOff object that is fast interlock cannot have DAs not marked as fast interlock connected to it")
                                break

                    if instance.getAttributeData("LogicDeviceDefinitions:Master").strip():
                        self.writeSemanticError(instance, "A fast interlock OnOff object can't have a master")
