# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin
from research.ch.cern.unicos.utilities import DeviceTypeFactory

import re

class Semantic_Generic_Template(IUnicosTemplate):
    semantic_verifier = 0
    plugin = 0
    unicos_project = 0
    the_manufacturer = ""
    siemens_plc_declarations = 0

    def initialize(self):
        self.semantic_verifier = SemanticVerifier.getUtilityInterface()
        self.plugin = APlugin.getPluginInterface()
        self.plugin.writeInUABLog("initialize in Jython in %s." % self.__class__.__name__)
        self.the_manufacturer = self.plugin.getPlcManufacturer()
        self.siemens_plc_declarations = self.plugin.getXMLConfig().getSiemensPLCDeclarations()
        self.unicos_project = self.plugin.getUnicosProject()

    def check(self):
        self.plugin.writeInUABLog("check in Jython in %s." % self.__class__.__name__)

    def begin(self):
        self.plugin.writeInUABLog("begin in Jython in %s." % self.__class__.__name__)

    def end(self):
        self.plugin.writeInUABLog("end in Jython in %s." % self.__class__.__name__)

    def shutdown(self):
        self.plugin.writeInUABLog("shutdown in Jython in %s." % self.__class__.__name__)

    def getMaxNameLength(self, device_type_name, manufacturer):
        ''' Get maximal allowed length of the object name for given device type name supported for given manufacturer'''
        max_length = 23  # if not siemens
        if (manufacturer.lower() == "siemens"):
            if(device_type_name.strip().lower() in ["processcontrolobject", "controller", "analog", "anlogdigital", "anado", "onoff", "massflowcontroller", "steppingmotor"]):
                max_length = 19
            elif (device_type_name.strip().lower() == "local"):
                max_length = 21
            else:
                max_length = 24
            xml_config = self.plugin.getXMLConfig()
            plc_name = xml_config.getPLCDeclarations().get(0).getName()
            plc_type = xml_config.getPLCParameter(plc_name + ":PLCType")
            if plc_type.lower() == "s7-1500":
                max_length += 101

        return max_length

    def checkNameLength(self, instance, name_length_limit):
        ''' Check if given name length is less than specified limit. '''
        name = instance.getAttributeData("DeviceIdentification:Name")
        if len(name) > name_length_limit:
            self.writeSemanticError(instance, "Max number of letters exceeded in the device type Name: current length = " + str(len(name)) + ". Max length allowed = " + str(name_length_limit) + ".")

    def writeSemanticWarning(self, instance, message):
        ''' Function unifies the way we write semantic warning messages.'''
        device_type_name = str(instance.getDeviceTypeName())
        device_name = instance.getAttributeData("DeviceIdentification:Name")
        self.plugin.writeWarningInUABLog(device_type_name + " instance: " + device_name + ". " + message)

    def writeSemanticError(self, instance, message):
        ''' Function unifies the way we write semantic error messages.'''
        device_type_name = str(instance.getDeviceTypeName())
        device_name = instance.getAttributeData("DeviceIdentification:Name")
        self.plugin.writeErrorInUABLog(device_type_name + " instance: " + device_name + ". " + message)

    def checkInterfaceParameter(self, instance, fe_type, attribute, regex, format):
        ''' Check the interface parameter using provided regular expression'''
        interface_param = instance.getAttributeData(attribute).strip()
        interface_param_name = attribute.split(":")[-1]
        if not interface_param:
            self.writeSemanticError(instance, "The " + interface_param_name + " must be defined if the FE Encoding Type is " + fe_type)
        elif not re.match(ur"^" + regex + ur"\u0024", interface_param.lower()):
            self.writeSemanticError(instance, "The " + interface_param_name + " (" + interface_param + ") is not well defined. The correct format is " + format)

    def getParameterValue(self, instance, attr):
        ''' Return value of given attr of an instance; if the attr value is a CPC parameter object, return its default value instead'''
        attr_value = instance.getAttributeData(attr).strip()
        # if it's a CPC parameter object - return its default value
        if self.plugin.isString(attr_value) and self.semantic_verifier.doesObjectExist(attr_value, self.unicos_project):
            attr_instance = self.unicos_project.findInstanceByName(attr_value)
            if attr_instance.getDeviceTypeName() in ["DigitalParameter", "WordParameter", "AnalogParameter"]:
                return attr_instance.getAttributeData("FEDeviceParameters:Default Value")
        return attr_value

    def checkDefaultValue(self, instance, defaulf_value_attr="FEDeviceParameters:Default Value", min_range_attr="FEDeviceParameters:Range Min", max_range_attr="FEDeviceParameters:Range Max"):
        ''' Check if the default value is inside the range (range_min, range_max) '''
        default_value = instance.getAttributeData(defaulf_value_attr)
        range_min = self.plugin.formatNumberPLC(self.getParameterValue(instance, min_range_attr))
        range_max = self.plugin.formatNumberPLC(self.getParameterValue(instance, max_range_attr))
        if float(default_value) > float(range_max) or float(default_value) < float(range_min):
            self.writeSemanticError(instance, "Default value " + default_value + " is outside the device range: (" + range_min + ", " + range_max + ").")

    def checkRanges(self, instance, min_range_attr="FEDeviceParameters:Range Min", max_range_attr="FEDeviceParameters:Range Max"):
        ''' Check if ranges are specified in correct order range_min < range_max '''

        range_min = self.plugin.formatNumberPLC(self.getParameterValue(instance, min_range_attr))
        range_max = self.plugin.formatNumberPLC(self.getParameterValue(instance, max_range_attr))
        if range_min.replace(".", "").isdigit() and range_max.replace(".", "").isdigit() and\
                float(range_min) > float(range_max):
            self.writeSemanticError(instance, min_range_attr.split(':')[-1] + " (" + range_min + ") is greater than " + max_range_attr.split(':')[-1] + " (" + range_max + ").")

    def check5RangeAlertThresholds(self, instance, threshold_values = None, range_values = None):
        ''' Check the thresholds of an alarms in terms of fitting in a range and their descending order '''
        if threshold_values == None:
            threshold_values = [instance.getAttributeData("SCADADeviceAlarms:Analog Thresholds:HH Alarm"),
                                instance.getAttributeData("SCADADeviceAlarms:Analog Thresholds:H Warning"),
                                instance.getAttributeData("SCADADeviceAlarms:Analog Thresholds:L Warning"),
                                instance.getAttributeData("SCADADeviceAlarms:Analog Thresholds:LL Alarm")]

        if range_values == None:
            range_values = [instance.getAttributeData("FEDeviceParameters:Range Min"),
                            instance.getAttributeData("FEDeviceParameters:Range Max")]

        # take only the limits that are specified as numerical values
        thresholds = [float(threshold) for threshold in threshold_values if not self.plugin.isString(threshold)]

        # check if limits are in the specified range
        if len(range_values) == 2 and len(thresholds) > 0:
            range_min = self.plugin.formatNumberPLC(range_values[0].replace(" ", ""))
            range_max = self.plugin.formatNumberPLC(range_values[1].replace(" ", ""))
            for threshold in thresholds:
                if (range_min and float(threshold) < float(range_min)) or (range_max and float(threshold) > float(range_max)):
                    self.writeSemanticError(instance, "The alarm threshold " + str(threshold) + " is out of range [" + str(range_min) + ", " + str(range_max) + "] defined in the FEDeviceParameters.")

        # check if values are unique and in correct order
        if len(thresholds) > 1:
            if thresholds != sorted(list(set(thresholds)), reverse=True):   # set removes duplicates, sort descending so it should match HH>H>L>LL
                self.writeSemanticError(instance, "The alarm thresholds must be in descending order: HH>H>L>LL, but are: " + ">".join([str(threshold) for threshold in thresholds]) + ".")

    def checkDependentAttibutes(self, instance, checked_attributes, required_attributes, severity="error"):
        ''' Check if at least one of required_attributes is specified when any of checked_attributes is defined. '''

        non_empty_required = [instance.getAttributeData(attr) for attr in required_attributes if instance.getAttributeData(attr).strip()]

        # none of the required attributes is specified
        if len(required_attributes) > 0 and not non_empty_required:
            for attribute in checked_attributes:
                if instance.getAttributeData(attribute):
                    message = attribute.split(':')[-1] + " attribute cannot be defined if "
                    if len(required_attributes) == 1:
                        message +=  required_attributes[0].split(':')[-1] + " attribute is not defined."
                    else:
                        message += "none of the following attributes are defined: " + ", ".join([a.split(':')[-1] for a in required_attributes]) + "."

                    if(severity=="error"):
                        self.writeSemanticError(instance, message)
                    else:
                        self.writeSemanticWarning(instance, message)

    def remove_negation(self, input_str):
        ''' Remove leading negation from the string. '''
        if input_str.strip().lower().startswith("not "):
            return input_str[4:]
        else:
            return input_str

    def checkObjectsInAttributes(self, instance, attributes, negatable=False, valid_types=[], invalid_types=[]):
        ''' Check if objects specified in attrbiutes axist in the project and are of valid/invalid types '''
        for attribute in attributes:
            objects = [object for object in instance.getAttributeData(attribute).split(',')]    # get list of objects from attribute

            # remove leading "not " from the string
            if negatable:
                objects = [self.remove_negation(object) for object in objects]

            for object in objects:
                if object.strip() and self.plugin.isString(object):
                    message = "The object " + object + " defined in " + attribute + " attribute"
                    if not self.semantic_verifier.doesObjectExist(object.strip(), self.unicos_project):
                        self.writeSemanticError(instance,  message + " does not exist.")
                    elif len(valid_types) > 0:
                        object_device_type_name = self.unicos_project.findInstanceByName(object.strip()).getDeviceTypeName()
                        if object_device_type_name.lower() not in [type.lower() for type in valid_types]:
                            self.writeSemanticError(instance, message + " must be " + "/".join(valid_types) + ", but is: " + str(object_device_type_name) + ".")
                    elif len(invalid_types) > 0:
                        object_device_type_name = self.unicos_project.findInstanceByName(object.strip()).getDeviceTypeName()
                        if object_device_type_name.lower()  in [type.lower() for type in invalid_types]:
                            self.writeSemanticError(instance, message + " cannot be " + str(object_device_type_name) + ".")

    def getPermittedValues(self, device_type_definition, requested_family_name, requested_attribute_name):
        ''' Retrieve from device type definition list of permitted values of the requested attribute in requested family. '''
        device_type_families = device_type_definition.getAttributeFamily()
        for device_type_family in device_type_families:
            family_name = device_type_family.getAttributeFamilyName()
            if family_name == requested_family_name:
                attributes = device_type_family.getAttribute()
                for attribute in attributes:
                    attribute_name = attribute.getAttributeName()
                    if attribute_name == requested_attribute_name:
                        is_spec_attribute = attribute.getIsSpecificationAttribute()
                        if not is_spec_attribute:
                            self.writeSemanticError(instance, "There is no isSpecificationAttribute in the attribute " + attributeName + ".")
                            return []
                        return is_spec_attribute.getPermittedValue()
                    elif not attribute_name:
                        self.writeSemanticError(instance, "There is no an attribute in the attributeFamily " + family_name + ".")
                        return []
                return []

    def checkDeadband(self, instance, deadband_attribute, format_attribute="SCADADeviceGraphics:Format"):
        format = instance.getAttributeData(format_attribute).upper()
        deadband = instance.getAttributeData(deadband_attribute)
        if 'EXP' in format:  # skip exponential format
            return
        if "." in deadband: # only if deadband has decimal part
            deadband_precision = len(deadband.split('.')[1].rstrip('0'))
            format_precision = 0    # assuming no precision
            if 'D' in format:       # fixed display format, in SCADA it gets precision of 20
                format_precision = 20
            elif format.replace("#", "") == '.': # ##.## format
                format_precision = len(format.split('.')[1])
                
            if deadband_precision > format_precision:
                self.writeSemanticWarning(instance, "Deadband " + deadband_attribute + " has value " + str(deadband) + " that does not fit in defined format: " + format + ". Deadband will be configured, but might be not visible in configuration panels in SCADA.")
                
    def checkFormat(self, instance, format_attribute="SCADADeviceGraphics:Format"):
        format = instance.getAttributeData(format_attribute).upper()
        pattern_dec = re.compile(ur"^(#*(\.#+)?)\u0024")
        pattern_exp = re.compile(ur"^[0-9]*EXP\u0024")
        pattern_fix = re.compile(ur"^[0-9]*[dD]\u0024")
        
        if not pattern_dec.match(format) and not pattern_exp.match(format) and not pattern_fix.match(format):
            self.writeSemanticWarning(instance, "Wrong format: '" + format + "'. It should be decimal format: #.##, exponential format: xEXP or fixed format: xD.")
                
    def checkArchiveConfig(self, instance, archive_mode_attr="SCADADeviceDataArchiving:Archive Mode", time_filter_attr="SCADADeviceDataArchiving:Time Filter (s)", deadband_type_attr="SCADADeviceDataArchiving:Deadband Type", deadband_value_attr="SCADADeviceDataArchiving:Deadband Value"):
        archive_mode    = instance.getAttributeData(archive_mode_attr)
        time_filter     = instance.getAttributeData(time_filter_attr).lower() if time_filter_attr else None
        deadband_type   = instance.getAttributeData(deadband_type_attr).lower() if deadband_type_attr else None
        deadband_value  = instance.getAttributeData(deadband_value_attr).lower() if deadband_value_attr else None
            
        if time_filter and "time" not in archive_mode.lower():
            self.writeSemanticWarning(instance, "Incoherent archiving configuration. Archive mode set to '" + archive_mode + "' and time filter specified (" + time_filter + ").")
        if deadband_type and "deadband" not in archive_mode.lower():
            self.writeSemanticWarning(instance, "Incoherent archiving configuration. Archive mode set to '" + archive_mode + "' and deadband type specified (" + deadband_type + ").")
        if deadband_value and "deadband" not in archive_mode.lower():
            self.writeSemanticWarning(instance, "Incoherent archiving configuration. Archive mode set to '" + archive_mode + "' and deadband value specified (" + deadband_value + ").")
        