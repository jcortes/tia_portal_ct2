# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from Semantic_Generic_Template import Semantic_Generic_Template

import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)

class DigitalAlarm_Template(Semantic_Generic_Template):

    def process(self, *params):
        current_device_type_name = params[0]
        current_device_type_definition = params[1]
        current_device_type = self.unicos_project.getDeviceType(current_device_type_name)
        self.plugin.writeInUABLog("process in Jython in %s." % self.__class__.__name__)

        decorator = ucpc_library.shared_decorator.ExpressionDecorator()

        # Retrieve permitted alarm type values from the DA device type definition
        device_instance = DeviceTypeFactory.getInstance()
        device_type_definition = device_instance.getDeviceType(current_device_type_name)
        permitted_alarm_types = self.getPermittedValues(device_type_definition, "FEDeviceAlarm", "Type")

        name_length_limit = self.getMaxNameLength(str(current_device_type_name), self.the_manufacturer)

        for instance in current_device_type.getAllDeviceTypeInstances():
            name = instance.getAttributeData("DeviceIdentification:Name")

            # Check the length of the name
            self.checkNameLength(instance, name_length_limit)

            # Check the alarm delay
            delay = instance.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
            if delay.strip() and self.plugin.isString(delay) and delay.strip().lower() != "logic":
                if not self.semantic_verifier.doesObjectExist(delay, self.unicos_project):
                    self.writeSemanticError(instance, "The Delay Alarm " + delay + ", defined as an AnalogParameter or an AnalogStatus, doesn't exist")
            elif not self.plugin.isString(delay):
                if (self.the_manufacturer.lower() == "siemens") and (round(float(delay)) != float(delay)):
                    self.writeSemanticWarning(instance, "The Delay Alarm time " + delay + " sec, is not an integer. It will be rounded to " + str(int(round(float(delay)))) + " sec due to Siemens limitation of non integer delay times")

            # Check the FEDevice inputs
            input = instance.getAttributeData("FEDeviceEnvironmentInputs:Input")
            if input and self.plugin.isString(input) and (input.lower() != "logic"):
                decorator.plcExpressionSemanticCheck(self.semantic_verifier, self.unicos_project, input, current_device_type_name, name, 'Input', True)

            # Check the masters
            self.checkObjectsInAttributes(instance, ["LogicDeviceDefinitions:Master"], invalid_types=["DigitalInput", "AnalogInput", "AnalogInputReal", "Encoder", "DigitalOutput", "AnalogOutput", "AnalogOutputReal", "DigitalParameter", "AnalogParameter", "WordParameter", "WordStatus", "AnalogStatus", "Local", "Controller", "AnalogAlarm", "DigitalAlarm"])

            # Check the alarm configuration
            alarm_type = instance.getAttributeData("FEDeviceAlarm:Type").replace(",", " ")
            alarm_master_list = instance.getAttributeData("LogicDeviceDefinitions:Master").replace(",", " ").split()
            alarm_multiple_types_list = instance.getAttributeData("FEDeviceAlarm:Multiple Types").replace(",", " ").split()

            if (alarm_type == "Multiple"): # Multiple Alarms

                # Check if the numbers of Masters and Types are the same
                if alarm_master_list and (len(alarm_multiple_types_list) != len(alarm_master_list)):
                    self.writeSemanticError(instance, "The number of Masters and Types is not the same.")

                # Check if all Alarm Types defined in the spec are defined in the deviceTypeDefinition
                for type in alarm_multiple_types_list:
                    if type not in permitted_alarm_types:
                        self.writeSemanticError(instance, "The Alarm type " + str(type) + " is not defined in the deviceType")

            else: # Single Alarm

                # chek number of masters
                if len(alarm_master_list) > 1:
                    self.writeSemanticError(instance, "Alarm has " + str(len(alarm_master_list)) + " masters defined, but its type is not set to multiple.")

                # Check if a master is defined when the alarm is a FS, TS or SI
                if not alarm_master_list:
                    if alarm_type in ["FS", "TS", "SI"]:
                        self.writeSemanticError(instance, "When the Alarm is a FS, TS or SI the master definition is mandatory.")
                    else:
                        self.writeSemanticWarning(instance, "Alarm has no master. Its logic will not be generated.")
                else:
                    if not alarm_type:
                        self.writeSemanticError(instance, "When the Alarm master is defined the alarm type is mandatory.")

                if alarm_type and (alarm_type not in permitted_alarm_types):
                    self.writeSemanticError(instance, "The Alarm type " + alarm_type + " is not defined in the deviceType.")

                if alarm_multiple_types_list:
                    self.writeSemanticError(instance, "This Alarm has been defined as a simple alarm (" + alarm_type + "). Then it's not allowed to add some information in the column: Multiple Types.")

            # Check the Fast Interlock configuration
            if instance.getAttributeData("LogicDeviceDefinitions:Fast Interlock Type"):
                if self.the_manufacturer.lower() == "siemens":
                    if not instance.getAttributeData("FEDeviceEnvironmentInputs:Input"):
                        self.writeSemanticError(instance, "A fast interlock DA object must have at least one Fast Interlock DI connected to the input.")
                    FI_master = False
                    normal_master_count = 0
                    for master in alarm_master_list:
                        master_instance = self.unicos_project.findInstanceByName(master)
                        if master_instance.getDeviceTypeName() != 'OnOff':
                            normal_master_count += 1
                        else:
                            FI_master = True
                    if normal_master_count > 1:
                        self.writeSemanticError(instance, "A DA object marked as fast interlock cannot have more than one PCO master.")
                    if not FI_master:
                        self.writeSemanticError(instance, "A DA object marked as fast interlock must have at least one Fast Interlock OnOff master")
                else:
                    self.writeSemanticError(instance, "Fast interlocks are not supported for the current platform. Leave blank")
