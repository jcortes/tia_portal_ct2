# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from Semantic_Generic_Template import Semantic_Generic_Template

class Controller_Template(Semantic_Generic_Template):

    def process(self, *params):
        current_device_type_name = params[0]
        current_device_type_definition = params[1]
        current_device_type = self.unicos_project.getDeviceType(current_device_type_name)
        self.plugin.writeInUABLog("process in Jython in %s." % self.__class__.__name__)

        name_length_limit = self.getMaxNameLength(str(current_device_type_name), self.the_manufacturer)

        for instance in current_device_type.getAllDeviceTypeInstances():

            # Check the length of the name
            self.checkNameLength(instance, name_length_limit)

            # Check the FEDevice inputs
            self.checkObjectsInAttributes(instance, ["FEDeviceEnvironmentInputs:Measured Value"], valid_types=["AnalogInput", "AnalogInputReal", "AnalogStatus"]) 

            # Check the FEDevice outputs
            self.checkObjectsInAttributes(instance, ["FEDeviceOutputs:Controlled Objects"], valid_types=["Analog", "AnalogDigital", "AnaDO", "MassFlowController", "Controller"])

            # Check the Output Range
            self.checkDependentAttibutes(instance, ["FEDeviceParameters:Controller Parameters:Output Range Min"], ["FEDeviceParameters:Controller Parameters:Output Range Max"])
            self.checkDependentAttibutes(instance, ["FEDeviceParameters:Controller Parameters:Output Range Max"], ["FEDeviceParameters:Controller Parameters:Output Range Min"])

            # Check the ranges
            self.checkRanges(instance, "FEDeviceParameters:Controller Parameters:Output Range Min", "FEDeviceParameters:Controller Parameters:Output Range Max")
            self.checkRanges(instance, "FEDeviceVariables:Default PID Parameters:SP Low Limit", "FEDeviceVariables:Default PID Parameters:SP High Limit")

            # Check setpoint for cascade controllers
            master_controllers_count = 0
            name = instance.getAttributeData("DeviceIdentification:Name")
            controller_instances = self.unicos_project.findMatchingInstances("Controller", "'#FEDeviceOutputs:Controlled Objects#' != ''")
            for PID in controller_instances:
                ControlledObjectList = PID.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ").split()
                if name in ControlledObjectList:
                    master_controllers_count += 1

            # Check if controller has more than one master controller
            if master_controllers_count > 1:
                self.writeSemanticError(instance, "Multiple master controllers assigned. Only one should be used.")

            # Check if controller is in a cascade and has a setpoint specified as a variable
            setpoint = instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Setpoint")
            if master_controllers_count >= 1 and setpoint.strip() and self.plugin.isString(setpoint):
                self.writeSemanticError(instance, "This controller is in a cascade and cannot have a default setpoint variable " + setpoint + " assigned.")

            # Check default PID parameters specified in spec
            self.checkObjectsInAttributes(instance, ["FEDeviceVariables:Default PID Parameters:Setpoint",
                                                     "FEDeviceVariables:Default PID Parameters:Kc",
                                                     "FEDeviceVariables:Default PID Parameters:Ti",
                                                     "FEDeviceVariables:Default PID Parameters:Td",
                                                     "FEDeviceVariables:Default PID Parameters:Tds",
                                                     "FEDeviceVariables:Default PID Parameters:Out High Limit",
                                                     "FEDeviceVariables:Default PID Parameters:Out Low Limit"])
                                                     
            self.checkArchiveConfig(instance, archive_mode_attr="SCADADeviceDataArchiving:MV and SP Archiving:Archive Mode", time_filter_attr="SCADADeviceDataArchiving:MV and SP Archiving:Time Filter (s)", deadband_type_attr="SCADADeviceDataArchiving:MV and SP Archiving:Deadband Type", deadband_value_attr="SCADADeviceDataArchiving:MV and SP Archiving:Deadband Value")
            self.checkArchiveConfig(instance, archive_mode_attr="SCADADeviceDataArchiving:Output Archiving:Archive Mode", time_filter_attr="SCADADeviceDataArchiving:Output Archiving:Time Filter (s)", deadband_type_attr="SCADADeviceDataArchiving:Output Archiving:Deadband Type", deadband_value_attr="SCADADeviceDataArchiving:Output Archiving:Deadband Value")
