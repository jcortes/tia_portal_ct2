# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
##
# This file contains all the common functions for the WinCCOA
##

from java.util import Vector
from java.util import ArrayList

# ==========================================================================


def getExpertName(instance):
    """
    This function returns the expert name (if available) from a given instance.
    If the expert name is not defined, then will return the object name.

    The arguments are:
    @param instance: object instance

    This function returns:
    @return: name: the expert name (or name) of the object for use in WinCCOA db file
    """

    if instance.getAttributeData("DeviceIdentification:Expert Name") != "":
        return instance.getAttributeData("DeviceIdentification:Expert Name")
    else:
        return instance.getAttributeData("DeviceIdentification:Name")

# ==========================================================================


def appendDeviceLinkListFromSpec(DeviceLinkListSpec, deviceLinkList, thePlugin, strAllDeviceTypes):
    """
    This function returns the deviceLinkList with the device links from the spec appended.
    Separated by SEPARATOR
    For each device link in the spec, will look up expert name if it exists.

    The arguments are:
    @param DeviceLinkListSpec: comma-separated list of device links from the spec
    @param deviceLinkList: comma-separated list of device links for the object
    @param thePlugin: for access to methods
    @param strAllDeviceTypes: string of all device types

    This function returns:
    @return: deviceLinkList: comma-separated list of device links
    """
    L_DeviceLinkListSpec = DeviceLinkListSpec.split(",")
    if DeviceLinkListSpec.strip() != "":
        deviceLinkList = deviceLinkList + ",SEPARATOR"
        for devlink in L_DeviceLinkListSpec:
            deviceLinkList = deviceLinkList + "," + thePlugin.getLinkedExpertName(devlink, strAllDeviceTypes)
    return deviceLinkList

# ==========================================================================


def updateDeviceLinksAndChildren(input, deviceLinkList, children, thePlugin, decorator):
    """
    This function returns the deviceLinkList and the children updated with all UNICOS
    objects included in 'input'

    The arguments are:
    @param input: simplified PLC code from the spec
    @param deviceLinkList: comma-separated list of device links for the object
    @param children: list of the object children
    @param thePlugin: for access to methods
    @param decorator instance

    This function returns:
    @return: deviceLinkList: comma-separated list of updated device links
    @return: children: updated list of the object children
    """

    listOfInputObjects = decorator.getListOfUNICOSObjects(input)
    # extend with list of IO objects depending on the actuators
    listOfInputObjects.extend(decorator.getActuatorsIOs(input))
    for object in listOfInputObjects:
        winccoaName = thePlugin.getLinkedExpertName(object)
        deviceLinkList = thePlugin.appendLinkedDevice(deviceLinkList, winccoaName)
        children.append(winccoaName)
    return deviceLinkList, children

# ==========================================================================


def removeDuplicatesAndGivenObjects(deviceLinkList, excludeNames):
    """
    This function removes duplicates from deviceLinkList and names that are in excludeNames

    The arguments are:
    @param deviceLinkList: comma-separated list of device links for the object
    @param excludeNames: list of the object to be removed

    This function returns:
    @return: deviceLinkList: comma-separated list of updated device links

    """

    excludeNames = [name.lower() for name in excludeNames]

    if deviceLinkList.strip():
        DeviceLinkListVector = deviceLinkList.split(",")
        uniqueListDeviceLinkList = [""]  # add empty element to start generated DeviceLinkList with a colon
        for DeviceLink in DeviceLinkListVector:
            if DeviceLink not in uniqueListDeviceLinkList:
                if DeviceLink.lower() not in excludeNames and DeviceLink.strip() != "":
                    uniqueListDeviceLinkList.append(DeviceLink)
        return ",".join(uniqueListDeviceLinkList)
    else:
        return deviceLinkList

# ==========================================================================


def findParentsFromDevice(deviceTypes, columnName, operator, name, parents, theUnicosProject, decorator):
    """
    This function finds and returns the Parents of a given object (name) if they are found in the
    column 'columnName' of the specified deviceTypes

    Args:
        deviceTypes: deviceTypes comma separated string where to look for parents
        columnName: column name in "deviceTypes" sheets in spec in which to look for "name"
        operator: operator for use in findMatchingInstances call '#$columnName$#' $operator$ '$name$'
                    see findMatchingInstances documentation in the Templates Development API available
                    in the logic or expert generator in the UCPC wizard
        name: name to look for in "deviceTypes" "columnName"
        parents: list of the object parents, to be updated
        theUnicosProject: for access to all objects
        decorator instance

    Returns:
        parents: updated list of the object parents
    """
    # Dollar sign variables cause java exception, replace with string concatenation
    # newParents = theUnicosProject.findMatchingInstances(deviceTypes, "'#$columnName$#' $operator$ '$name$'")
    newParents = theUnicosProject.findMatchingInstances(deviceTypes, "'#" + columnName + "#' " + operator + " '" + name + "'")
    for parent in newParents:
        listOfInputObjects = decorator.getListOfUNICOSObjects(parent.getAttributeData(columnName))
        if name in listOfInputObjects:
            parents.append(getExpertName(parent))
    return parents

# ==========================================================================


def getPvssAlarmAckDigital(specAlarmAck, specAlarmOnState):
    """
    This function returns the AlarmAck property given spec specAlarmAck input and specAlarmOnState
    for a digital device (DI, DO)

    Args:
        specAlarmAck: SCADADeviceAlarms:Alarm Config:Auto Acknowledge from the spec (blank, true, false)
        specAlarmOnState: SCADADeviceAlarms:Binary State:Alarm On State from the spec (blank, true, false)

    Returns:
        pvssAlarmAck: Alarm Ack property for pvss importation line
    """
    specAlarmAck = specAlarmAck.strip().lower()
    specAlarmOnState = specAlarmOnState.strip()
    if (specAlarmAck == "true") or \
       (specAlarmOnState == ""):  # condition when there's no pvss alarm -> ack property can't be stored
        return "false"
    else:
        return "true"

# ==========================================================================


def getPvssAlarmAckAnalog(specAutoAlarmAck, specHHLimit, specLLLimit):
    """
    This function returns the AlarmAck property given spec specAutoAlarmAck input and specHHLimit, specLLLimit
    for an analog device (AI, AIR, AO, AOR)

    Args:
        specAutoAlarmAck: SCADADeviceAlarms:Alarm Config:Auto Acknowledge from the spec (blank, true, false)
        specHHLimit: SCADADeviceAlarms:Analog Thresholds:HH Alarm from the spec
        specLLLimit: SCADADeviceAlarms:Analog Thresholds:LL Alarm from the spec

    Returns:
        pvssAlarmAck: Alarm Ack property for pvss importation line (in principle it is opposite to specAutoAlarmAck)
    """
    specAutoAlarmAck = specAutoAlarmAck.strip().lower()
    if (specAutoAlarmAck == "true") or \
       (specHHLimit == "" and specLLLimit == ""):  # condition when there's no pvss alarm -> ack property can't be stored
        return "false"
    else:
        return "true"

# ==========================================================================


def getPvssAlarmActiveAnalog(specAlarmMasked, specHHLimit, specHLimit, specLLimit, specLLLimit):
    """
    This function returns the AlarmActive property given spec specAlarmMasked input and specHHLimit, specLLLimit
    for an analog device (AI, AIR, AO, AOR)

    Args:
        specAlarmMasked: SCADADeviceAlarms:Alarm Config:Masked from the spec (blank, true, false)
        specHHLimit: SCADADeviceAlarms:Analog Thresholds:HH Alarm from the spec
        specHLimit: SCADADeviceAlarms:Analog Thresholds:H Warning from the spec
        specLLimit: SCADADeviceAlarms:Analog Thresholds:L Warning from the spec
        specLLLimit: SCADADeviceAlarms:Analog Thresholds:LL Alarm from the spec

    Returns:
        pvssAlarmActive: Alarm Active property for pvss importation line (in principle it is opposite to specAlarmMasked)
    """
    specAlarmMasked = specAlarmMasked.strip().lower()
    # AlarmMasked --> NOT Alarm Active
    if specAlarmMasked == "true" or \
       (specHHLimit == "" and specHLimit == "" and specLLimit == "" and specLLLimit == ""):  # condition when there's no pvss alarm -> cannot be active
        return "false"
    else:
        return "true"

# ==========================================================================


def getAttributeDataIfExist(instance, specAttribute, defaultValue=""):
    """
    This function returns the specAttribute from instance if it exists
    if it doesn't exist returns ""

    Args:
        instance: spec instance
        specAttribute: spec attribute
        defaultValue: value to be returned in case if attribute does not exist

    Returns:
        value from spec (if attr ezists), default value (if provided), or "" (otherwise)
    """
    if instance.getDeviceType().doesSpecificationAttributeExist(specAttribute):
        return instance.getAttributeData(specAttribute)
    else:
        return defaultValue
