# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Encoder Objects.

import Generic_Template
reload(Generic_Template)


class Encoder_Template(Generic_Template.Generic_Type_Template):
    default_nature = "ENCODER"

    def device_type(self):
        return "AnalogInput"

    def getFirstDeviceNumber(self):
        return 99500

    def deviceFormat(self, instance=None):
        return ["deviceType", "deviceNumber", 'Alias[,DeviceLinkList]', 'Description - ElectricalDiagram', 'Diagnostics', 'WWWLink', 'Synoptic', 'Domain', 'Nature', 'WidgetType',
                'Unit', 'Format', 'RangeMax', 'RangeMin', 'HHLimit', 'HLimit', 'LLimit', 'LLLimit', 'AlarmActive', 'DriverDeadbandValue', 'DriverDeadbandType',
                'ArchiveMode', 'TimeFilter', 'SMSCat', 'AlarmMessage', 'AlarmAck', 'addr_StsReg01', 'addr_EvStsReg01', "addr_PosSt", "addr_HFSt",
                "addr_ManReg01", "addr_MPosR", 'BooleanArchive', 'AnalogArchive', 'EventArchive', 'MaskEvent', 'Parameters', 'Master', 'Parents', 'children', 'Type', 'SecondAlias']

    def processInstance(self, instance, params, values):
        category_filter = {"NO_SMS_ON_CHANGE": ["PosSt"]}
        values['Unit'] = self.getUnit(instance)
        values['Format'] = self.getFormat(instance)
        values['RangeMin'], values['RangeMax'] = self.getRanges(instance)
        values['HHLimit'], values['HLimit'], values['LLimit'], values['LLLimit'] = self.get5RangeLimits(instance)
        values['AlarmActive'] = self.get5RangesAlarmActive(instance)
        values['DriverDeadbandValue'] = self.getDriverDeadbandValue(instance)
        values['DriverDeadbandType'] = self.getDriverDeadbandType(instance)

        values['ArchiveMode'], values['TimeFilter'] = self.getArchiveConfig(instance)

        values['SMSCat'] = self.getFilteredSMSCategory(instance, category_filter.keys())
        values['AlarmMessage'] = self.getAlarmMessage(instance, remove_commas=False)
        values['AlarmAck'] = self.getAlarmAck(instance)
        values['MaskEvent'] = self.getMaskEvent(instance)
        self.getFilteredSMSCategoryParameters(instance, values['Parameters'], category_filter)

        values['Parents'] += self.getRIndex(instance, "SteppingMotor", "FEDeviceEnvironmentInputs:Feedback Analog")
        values['Type'] = self.getIOFEType(instance)

        values['Alias[,DeviceLinkList]'] += self.getDeviceLinksFromSpec(instance)
