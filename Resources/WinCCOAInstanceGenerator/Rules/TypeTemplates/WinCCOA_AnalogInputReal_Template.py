# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for AnalogInputReal Objects.

import Generic_Template
reload(Generic_Template)


class AnalogInputReal_Template(Generic_Template.Generic_Type_Template):
    default_nature = "AIR"

    def scada_device_type(self):
        return "AnalogInput"

    def getFirstDeviceNumber(self):
        return 50000

    def deviceFormat(self, instance=None):
        return ["deviceType", "deviceNumber", 'Alias[,DeviceLinkList]', 'Description - ElectricalDiagram', 'Diagnostics', 'WWWLink', 'Synoptic', 'Domain', 'Nature', 'WidgetType',
                'Unit', 'Format', 'RangeMax', 'RangeMin', 'HHLimit', 'HLimit', 'LLimit', 'LLLimit', 'AlarmActive', 'DriverDeadbandValue', 'DriverDeadbandType',
                'ArchiveMode', 'TimeFilter', 'SMSCat', 'AlarmMessage', 'AlarmAck', 'addr_StsReg01', 'addr_EvStsReg01', "addr_PosSt", "addr_HFSt",
                "addr_ManReg01", "addr_MPosR", 'BooleanArchive', 'AnalogArchive', 'EventArchive', 'MaskEvent', 'Parameters', 'Master', 'Parents', 'children', 'Type', 'SecondAlias']

    def processInstance(self, instance, params, values):
        category_filter = {"NO_SMS_ON_CHANGE": ["PosSt"]}
        values['Unit'] = self.getUnit(instance)
        values['Format'] = self.getFormat(instance)
        values['RangeMin'], values['RangeMax'] = self.getRanges(instance)
        values['HHLimit'], values['HLimit'], values['LLimit'], values['LLLimit'] = self.get5RangeLimits(instance)
        values['AlarmActive'] = self.get5RangesAlarmActive(instance)
        values['DriverDeadbandValue'] = self.getDriverDeadbandValue(instance)
        values['DriverDeadbandType'] = self.getDriverDeadbandType(instance)

        values['ArchiveMode'], values['TimeFilter'] = self.getArchiveConfig(instance)

        values['SMSCat'] = self.getFilteredSMSCategory(instance, category_filter.keys())
        values['AlarmMessage'] = self.getAlarmMessage(instance, remove_commas=False)
        values['AlarmAck'] = self.getAlarmAck(instance)
        values['MaskEvent'] = self.getMaskEvent(instance)

        filter_time = self.plugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Filtering Time (s)").replace(" ", ""))
        values['Parameters'] += self.getParameterValue("FIRST_ORDER_FILTER", filter_time, filter_time)
        self.getFilteredSMSCategoryParameters(instance, values['Parameters'], category_filter)

        values['Parents'] += self.getRIndex(instance, "Analog,AnaDO,AnalogDigital", ["FEDeviceEnvironmentInputs:Feedback Analog",
                                                                                     "FEDeviceEnvironmentInputs:Hardware Analog Output",
                                                                                     "FEDeviceParameters:Warning Deadband Value (Unit)"])
        values['Parents'] += self.getRIndex(instance, "Analog,AnaDO,AnalogDigital,OnOff", "FEDeviceParameters:Warning Time Delay (s)")
        values['Parents'] += self.getRIndex(instance, "Controller", ["FEDeviceEnvironmentInputs:Measured Value",
                                                                     "FEDeviceVariables:Default PID Parameters:Kc",
                                                                     "FEDeviceVariables:Default PID Parameters:Ti",
                                                                     "FEDeviceVariables:Default PID Parameters:Td",
                                                                     "FEDeviceVariables:Default PID Parameters:Tds",
                                                                     "FEDeviceVariables:Default PID Parameters:SP High Limit",
                                                                     "FEDeviceVariables:Default PID Parameters:SP Low Limit",
                                                                     "FEDeviceVariables:Default PID Parameters:Out High Limit",
                                                                     "FEDeviceVariables:Default PID Parameters:Out Low Limit",
                                                                     "FEDeviceVariables:Default PID Parameters:Setpoint"])
        values['Parents'] += self.getRIndex(instance, "DigitalAlarm", "FEDeviceEnvironmentInputs:Input")
        values['Parents'] += self.getRIndex(instance, "AnalogAlarm", ["FEDeviceEnvironmentInputs:Input",
                                                                      "FEDeviceAlarm:Enable Condition",
                                                                      "FEDeviceManualRequests:HH Alarm",
                                                                      "FEDeviceManualRequests:H Warning",
                                                                      "FEDeviceManualRequests:L Warning",
                                                                      "FEDeviceManualRequests:LL Alarm"])

        values['Type'] = self.getIOFEType(instance)

        values['Alias[,DeviceLinkList]'] += self.getDeviceLinksFromSpec(instance)
