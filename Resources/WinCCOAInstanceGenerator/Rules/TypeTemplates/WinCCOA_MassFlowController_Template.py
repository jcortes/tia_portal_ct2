# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved

# Jython source file for MassFlowController Objects.

import Generic_Template
reload(Generic_Template)


class MassFlowController_Template(Generic_Template.Generic_Type_Template):
    default_nature = "MassFlowController"

    def getImportationLine(self):
        return Generic_Template.Generic_Type_Template.getImportationLine(self).replace('Master', 'master').replace('Parents', 'stringparents').replace('children', 'stringchildren').replace(';Type', ';type').replace('SecondAlias','secondAlias')

    def getCalibrationCurves(self, instance):
        CC0Name = instance.getAttributeData("SCADADeviceGraphics:CurveName:CC0")
        CC1Name = instance.getAttributeData("SCADADeviceGraphics:CurveName:CC1")
        CC2Name = instance.getAttributeData("SCADADeviceGraphics:CurveName:CC2")
        CC3Name = instance.getAttributeData("SCADADeviceGraphics:CurveName:CC3")
        CC4Name = instance.getAttributeData("SCADADeviceGraphics:CurveName:CC4")

        CalibCurves = "0=" + CC0Name
        if CC1Name != "":
            CalibCurves = CalibCurves + ",1=" + CC1Name
        if CC2Name != "":
            CalibCurves = CalibCurves + ",2=" + CC2Name
        if CC3Name != "":
            CalibCurves = CalibCurves + ",3=" + CC3Name
        if CC4Name != "":
            CalibCurves = CalibCurves + ",4=" + CC4Name
        return CalibCurves

    def setDescription(self, instance, values):
        values['Description'] = instance.getAttributeData("DeviceDocumentation:Description")

    def deviceFormat(self, instance=None):
        return ["deviceType", "deviceNumber", 'Alias[,DeviceLinkList]', 'Description', 'Diagnostics', 'WWWLink', 'Synoptic', 'Domain', 'Nature', 'WidgetType', 'FlowUnit',
                'FlowFormat', 'DriverFlowDbValue', 'DriverFlowDbType', 'ArchiveFlowMode', 'ArchiveFlowTime', 'TotUnit', 'TotFormat', 'DriverTotDbValue',
                'DriverTotalizerDbType', 'ArchiveTotalizerMode', 'ArchiveTotTime', 'ArchiveModeMode', 'ArchiveModeTime', 'NormalPosition', 'addr_StsReg01',
                "addr_StsReg02", 'addr_EvStsReg01', "addr_EvStsReg02", "addr_PosSt", "addr_PosRSt", "addr_MaxPosSt", "addr_MPosRSt", "addr_AuPosRSt",
                "addr_AuVoTOfSt", "addr_AuCCRSt", "addr_AuDMoRSt", "addr_AuVoTMRSt", "addr_MVoTMRSt", "addr_MDMoRSt", "addr_MCCRSt", "addr_VoTSt",
                "addr_ActOutOV", "addr_ActDMo", "addr_ActCC", "addr_ActVoTMo", "addr_ManReg01", "addr_MPosR", "addr_MDMoR", "addr_MCCR", "addr_MVoTMoR",
                'CalibCurves', 'CalibUnit', 'CalibGasName', 'BooleanArchive', 'AnalogArchive', 'EventArchive', 'MaskEvent', 'Parameters', 'Master',
                'Parents', 'children', 'Type', 'SecondAlias']

    def processInstance(self, instance, params, values):
        values['FlowUnit'] = self.getUnit(instance, "SCADADeviceGraphics:Flow:Unit")
        values['FlowFormat'] = self.getFormat(instance, "SCADADeviceGraphics:Flow:Format")
        values['DriverFlowDbValue'] = self.getDriverDeadbandValue(instance, driver_deadband_value_attribute="SCADADriverDataSmoothing:Flow:Deadband Value", driver_deadband_type_attribute="SCADADriverDataSmoothing:Flow:Deadband Type", format_number_plc=False)
        values['DriverFlowDbType'] = self.getDriverDeadbandType(instance, driver_deadband_type_attribute="SCADADriverDataSmoothing:Flow:Deadband Type")
        values['ArchiveFlowMode'], values['ArchiveFlowTime'] = self.getArchiveConfig(instance, archive_mode_family="SCADADeviceDataArchiving:Flow", format_number_plc=False)
        if not values['ArchiveFlowTime']:
            values['ArchiveFlowTime'] = "0.0"
        values['TotUnit'] = self.getUnit(instance, "SCADADeviceGraphics:Totalizer:Unit")
        values['TotFormat'] = self.getFormat(instance, "SCADADeviceGraphics:Totalizer:Format")
        values['DriverTotDbValue'] = self.getDriverDeadbandValue(instance, driver_deadband_value_attribute="SCADADriverDataSmoothing:Totalizer:Deadband Value", driver_deadband_type_attribute="SCADADriverDataSmoothing:Totalizer:Deadband Type", format_number_plc=False)
        values['DriverTotalizerDbType'] = self.getDriverDeadbandType(instance, driver_deadband_type_attribute="SCADADriverDataSmoothing:Totalizer:Deadband Type")
        values['ArchiveTotalizerMode'], values['ArchiveTotTime'] = self.getArchiveConfig(instance, archive_mode_family="SCADADeviceDataArchiving:Totalizer", format_number_plc=False)
        if not values['ArchiveTotTime']:
            values['ArchiveTotTime'] = "0.0"
        values['ArchiveModeMode'], values['ArchiveModeTime'] = self.getArchiveConfig(instance, archive_mode_family="SCADADeviceDataArchiving:Modes", format_number_plc=False)
        if not values['ArchiveModeTime']:
            values['ArchiveModeTime'] = "0"
        values['NormalPosition'] = "0"

        values['CalibCurves'] = self.getCalibrationCurves(instance)
        values['CalibUnit'] = self.getUnit(instance, "SCADADeviceGraphics:Calibration:Unit")
        values['CalibGasName'] = instance.getAttributeData("SCADADeviceGraphics:Calibration:Gas Name")
        values['MaskEvent'] = self.getMaskEvent(instance)

        values['Parameters'] += self.getParameterValue("PFSPosOn", instance.getAttributeData("FEDeviceParameters:ParReg:Fail-Safe").lower() == "off/close", "FALSE", "TRUE")
        values['Parameters'] += self.getParameterValue("PFeedbackSim", instance.getAttributeData("FEDeviceParameters:ParReg:Feedback Off").lower() == "false", "FALSE", "TRUE")
        values['Parameters'] += self.getParameterValue("PIhMTot", instance.getAttributeData("FEDeviceParameters:ParReg:Inhibit Totalizer cmd").lower() == "false", "FALSE", "TRUE")
        values['Parameters'] += self.getParameterValue("PNoiseFilter", instance.getAttributeData("FEDeviceParameters:ParReg:Noise Filter").lower() == "false", "FALSE", "TRUE")
        values['Parameters'] += self.getParameterValue("PEnRstart", instance.getAttributeData("FEDeviceParameters:ParReg:Manual Restart after Full Stop").lower() == "false", "FALSE", "TRUE")
        values['Parameters'] += self.getParameterValue("PRstartFS", instance.getAttributeData("FEDeviceParameters:ParReg:Manual Restart after Full Stop").lower() == "true even if full stop still active", "TRUE", "FALSE")
        values['Parameters'] += self.getParameterValue("FIRST_ORDER_FILTER", instance.getAttributeData("FEDeviceParameters:Filtering Time (s)"), self.plugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Filtering Time (s)").replace(" ", "")))

        values['Master'] = self.getMaster(instance)

        values['Parents'] += self.getParentControllers(instance)

        io_objects = self.getObjectsFromAttributes(instance, ["FEDeviceOutputs:Valve Order"])

        values['children'] += io_objects
        values['children'] += self.getChildAlarms(instance)

        values['Alias[,DeviceLinkList]'] += self.getObjectsFromAttributes(instance, ["LogicDeviceDefinitions:Master", "LogicDeviceDefinitions:External Master"])
        values['Alias[,DeviceLinkList]'] += io_objects
        values['Alias[,DeviceLinkList]'] += self.getParentControllers(instance)
        values['Alias[,DeviceLinkList]'] += self.getDeviceLinksFromSpec(instance)

        values['Type'] = ""
