# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for AnalogAlarm Objects.
import Generic_Template
reload(Generic_Template)


class AnalogAlarm_Template(Generic_Template.Generic_Type_Template):
    default_nature = "AA"

    def deviceFormat(self, instance=None):
        return ["deviceType", "deviceNumber", 'Alias[,DeviceLinkList]', 'Description', 'Diagnostics', 'WWWLink', 'Synoptic', 'Domain', 'Nature', 'WidgetType', 'Unit',
                'Format', 'ArchiveModeI', 'ArchiveModeT', 'TimeFilterT', 'SMSCat', 'AlarmMessage', 'AlarmAck', "Level", 'NormalPosition', 'addr_StsReg01',
                "addr_StsReg02", 'addr_EvStsReg01', "addr_EvStsReg02", "addr_PosSt", "addr_HHSt", "addr_HSt", "addr_LSt", "addr_LLSt", "addr_ManReg01",
                "addr_HH", "addr_H", "addr_L", "addr_LL", 'BooleanArchive', 'AnalogArchive', 'EventArchive', 'MaskEvent', 'Parameters', 'Master', 'Parents',
                'children', 'Type', 'SecondAlias']

    def getInputInstanceWithUnit(self, instance):
        Input = instance.getAttributeData("FEDeviceEnvironmentInputs:Input").strip()
        listOfInputObjects = self.decorator.getListOfUNICOSObjects(Input)
        output_inst = None
        for inst in listOfInputObjects:
            inst_I = self.spec.findInstanceByName(inst)
            if inst_I is not None and inst_I.getDeviceType().doesSpecificationAttributeExist("SCADADeviceGraphics:Unit"):
                output_inst = inst_I
        return output_inst

    def processInstance(self, instance, params, values):
        category_filter = {"NO_SMS_ON_WARNING": ["HWSt", "LWSt"],
                           "NO_SMS_ON_ALERT": ["HHAlSt", "LLAlSt"]}
        values['Unit'] = self.getUnit(self.getInputInstanceWithUnit(instance))
        values['Format'] = self.getFormat(self.getInputInstanceWithUnit(instance), default_value="###.###")
        values['ArchiveModeI'] = self.getDigitalArchiveConfig(instance, family="SCADADeviceDataArchiving:Interlock Archiving")
        values['ArchiveModeT'], values['TimeFilterT'] = "O", "10.0"
        if self.getInputInstanceWithUnit(instance):
            values['ArchiveModeT'], values['TimeFilterT'] = self.getArchiveConfig(self.getInputInstanceWithUnit(instance), default_time_filter="")
        values['SMSCat'] = self.getFilteredSMSCategory(instance, category_filter.keys())
        values['AlarmMessage'] = self.getAlarmMessage(instance, remove_commas=False, use_description=True)
        values['AlarmAck'] = self.getAlarmAckBoolean(instance)
        values["Level"] = self.getAlarmLevel(instance)
        values['NormalPosition'] = self.getAlarmNormalPosition(instance)
        values['MaskEvent'] = self.getMaskEvent(instance)

        values['Parameters'] += self.getParameterValue("PAlDt", True, self.getDelayParameter(instance))
        values['Parameters'] += self.getParameterValue("INPUT_SOURCE", True, str(self.plugin.getLinkedExpertName(self.decorator.extractSingleUnicosObjectFromPLCExpression(instance.getAttributeData("FEDeviceEnvironmentInputs:Input"), True))))

        values['Parameters'] += self.getParameterValue("HH_SOURCE", not instance.getAttributeData("FEDeviceManualRequests:HH Alarm").strip(), "disabled", str(self.plugin.getLinkedExpertName(self.decorator.extractSingleUnicosObjectFromPLCExpression(instance.getAttributeData("FEDeviceManualRequests:HH Alarm").strip(), False))))
        values['Parameters'] += self.getParameterValue("H_SOURCE", not instance.getAttributeData("FEDeviceManualRequests:H Warning").strip(), "disabled", str(self.plugin.getLinkedExpertName(self.decorator.extractSingleUnicosObjectFromPLCExpression(instance.getAttributeData("FEDeviceManualRequests:H Warning").strip(), False))))
        values['Parameters'] += self.getParameterValue("L_SOURCE", not instance.getAttributeData("FEDeviceManualRequests:L Warning").strip(), "disabled", str(self.plugin.getLinkedExpertName(self.decorator.extractSingleUnicosObjectFromPLCExpression(instance.getAttributeData("FEDeviceManualRequests:L Warning").strip(), False))))
        values['Parameters'] += self.getParameterValue("LL_SOURCE", not instance.getAttributeData("FEDeviceManualRequests:LL Alarm").strip(), "disabled", str(self.plugin.getLinkedExpertName(self.decorator.extractSingleUnicosObjectFromPLCExpression(instance.getAttributeData("FEDeviceManualRequests:LL Alarm").strip(), False))))
        self.getFilteredSMSCategoryParameters(instance, values['Parameters'], category_filter)

        master_objects = self.getObjectsFromAttributes(instance, ["LogicDeviceDefinitions:Master"], extend_with_actuators_ios=True)
        values['Alias[,DeviceLinkList]'] += master_objects
        values['Parents'] += master_objects

        objects = self.getObjectsFromAttributes(instance, ["FEDeviceEnvironmentInputs:Input",
                                                           "FEDeviceManualRequests:HH Alarm",
                                                           "FEDeviceManualRequests:H Warning",
                                                           "FEDeviceManualRequests:L Warning",
                                                           "FEDeviceManualRequests:LL Alarm",
                                                           "FEDeviceParameters:Alarm Delay (s)",
                                                           "FEDeviceAlarm:Enable Condition"],
                                                extend_with_actuators_ios=True)
        values['children'] += objects

        values['Alias[,DeviceLinkList]'] += objects
        values['Alias[,DeviceLinkList]'] += self.getDeviceLinksFromSpec(instance)

        values['Type'] = self.getAlarmType(instance)
