# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for AnalogParameter Objects.

import Generic_Template
reload(Generic_Template)


class DigitalParameter_Template(Generic_Template.Generic_Type_Template):
    default_nature = "DPar"

    def deviceFormat(self, instance=None):
        return ["deviceType", "deviceNumber", 'Alias[,DeviceLinkList]', 'Description', 'Diagnostics', 'WWWLink', 'Synoptic', 'Domain', 'Nature', 'WidgetType',
                'ArchiveMode', 'addr_StsReg01', 'addr_EvStsReg01', "addr_ManReg01", 'DefaultValue', 'BooleanArchive', 'AnalogArchive', 'EventArchive', 'MaskEvent',
                'Parameters', 'Master', 'Parents', 'children', 'Type', 'SecondAlias']

    def getArchiveConfig(self, instance):
        archive_mode = instance.getAttributeData("SCADADeviceDataArchiving:Archive Mode").lower()
        # Archiving Mode
        if (archive_mode == "old/new comparison"):
            return "Y"
        else:
            return "N"

    def processInstance(self, instance, params, values):
        values['ArchiveMode'] = self.getArchiveConfig(instance)
        values['DefaultValue'] = instance.getAttributeData("FEDeviceParameters:Default Value")
        values['MaskEvent'] = self.getMaskEvent(instance)
        LabelOn = instance.getAttributeData("SCADADeviceGraphics:Label On").strip()
        LabelOff = instance.getAttributeData("SCADADeviceGraphics:Label Off").strip()
        values['Parameters'] += self.getParameterValue("LabelOn", LabelOn, LabelOn)
        values['Parameters'] += self.getParameterValue("LabelOff", LabelOff, LabelOff)

        values['Parents'] += self.getRIndex(instance, "DigitalAlarm", "FEDeviceEnvironmentInputs:Input")
        values['Parents'] += self.getRIndex(instance, "AnalogAlarm", "FEDeviceAlarm:Enable Condition")

        values['Alias[,DeviceLinkList]'] += self.getDeviceLinksFromSpec(instance)
