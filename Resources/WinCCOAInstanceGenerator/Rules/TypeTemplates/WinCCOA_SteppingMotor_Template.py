# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Analog Objects.

import Generic_Template
reload(Generic_Template)


class SteppingMotor_Template(Generic_Template.Generic_Type_Template):
    default_nature = "SteppingMotor"

    def deviceFormat(self, instance=None):
        return ["deviceType", "deviceNumber", 'Alias[,DeviceLinkList]', 'Description', 'Diagnostics', 'WWWLink', 'Synoptic', 'Domain', 'Nature', 'WidgetType', 'Unit',
                'Format', 'RangeMax', 'RangeMin', "spdRangeMax", "spdRangeMin", 'DriverDeadbandValue', 'DriverDeadbandType', 'ArchiveMode', 'TimeFilter', 'NormalPosition',
                'addr_StsReg01', "addr_StsReg02", 'addr_EvStsReg01', "addr_EvStsReg02", "addr_PosSt", "addr_SpdSt", "addr_PosRSt", "addr_SpdRSt", "addr_AuPosRSt", "addr_AuSpdRSt", "addr_MPosRSt", "addr_MSpdRSt",
                "addr_ManReg01", "addr_MPosR", "addr_MSpdR", 'BooleanArchive', 'AnalogArchive', 'EventArchive', 'MaskEvent', 'Parameters', 'Master',
                'Parents', 'children', 'Type', 'SecondAlias']

    def processInstance(self, instance, params, values):
        values['Unit'] = self.getUnit(instance)
        values['Format'] = self.getFormat(instance)
        values['RangeMin'], values['RangeMax'] = self.getRanges(instance, range_min_attr="FEDeviceParameters:Minimum Range", range_max_attr="FEDeviceParameters:Maximum Range", format_number_plc=False)
        values['spdRangeMin'], values['spdRangeMax'] = self.getRanges(instance, range_max_attr="FEDeviceParameters:Max Speed", default_min="1", default_max="10", format_number_plc=False)

        values['DriverDeadbandValue'] = self.getDriverDeadbandValue(instance)
        values['DriverDeadbandType'] = self.getDriverDeadbandType(instance)
        values['ArchiveMode'], values['TimeFilter'] = self.getArchiveConfig(instance, self.getProcessOutput(instance))
        values['NormalPosition'] = "0"
        values['MaskEvent'] = self.getMaskEvent(instance)

        values['Parameters'] += self.getParameterValue("PEnRstart", instance.getAttributeData("FEDeviceParameters:ParReg:Manual Restart after Full Stop").lower() == "false", "FALSE", "TRUE")
        values['Parameters'] += self.getParameterValue("PRstartFS", instance.getAttributeData("FEDeviceParameters:ParReg:Manual Restart after Full Stop").lower() == "true even if full stop still active", "TRUE", "FALSE")

        # values['Parameters'] += self.getParameterValue("PHFInt", instance.getAttributeData("FEDeviceParameters:ParReg:Switches Configuration").lower() == "2 switches. new config.", "TRUE", "FALSE")
        values['Parameters'] += self.getParameterValue("PHFCW", instance.getAttributeData("FEDeviceParameters:ParReg:Switches Configuration").lower() in ["3 switches", "2 switches. old config."], "TRUE", "FALSE")
        values['Parameters'] += self.getParameterValue("PHFCCW", instance.getAttributeData("FEDeviceParameters:ParReg:Switches Configuration").lower() in ["3 switches", "2 switches. old config."], "TRUE", "FALSE")
        values['Parameters'] += self.getParameterValue("PHFRefS", instance.getAttributeData("FEDeviceParameters:ParReg:Switches Configuration").lower() == "3 switches", "TRUE", "FALSE")

        values['Parameters'] += self.getParameterValue("PHFEnc", instance.getAttributeData("FEDeviceParameters:ParReg:Feedback").lower() in ["encoder", "potentiometer (support)"], "TRUE", "FALSE")
        values['Parameters'] += self.getParameterValue("PHFPot", instance.getAttributeData("FEDeviceParameters:ParReg:Feedback").lower() == "potentiometer", "TRUE", "FALSE")
        values['Parameters'] += self.getParameterValue("PHFAnFbSup", True, "FALSE")

        values['Master'] = self.getMaster(instance)

        io_objects = self.getObjectsFromAttributes(instance, ["FEDeviceEnvironmentInputs:ClockWise Limit",
                                                              "FEDeviceEnvironmentInputs:CounterClockWise Limit",
                                                              "FEDeviceEnvironmentInputs:Feedback Analog",
                                                              "FEDeviceOutputs:Driver Enable",
                                                              "FEDeviceOutputs:ClockWise to Reference Switch",
                                                              "FEDeviceOutputs:Simulated Reference Switch"],
                                                   extend_with_actuators_ios=True)
        values['children'] += io_objects
        values['children'] += self.getChildAlarms(instance)

        values['Alias[,DeviceLinkList]'] += self.getObjectsFromAttributes(instance, ["LogicDeviceDefinitions:Master", "LogicDeviceDefinitions:External Master"])
        values['Alias[,DeviceLinkList]'] += io_objects
        values['Alias[,DeviceLinkList]'] += self.getDeviceLinksFromSpec(instance)

        values['Type'] = ""
