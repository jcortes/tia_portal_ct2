# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for AnaDO Objects.

import Generic_Template
reload(Generic_Template)


class AnaDO_Template(Generic_Template.Generic_Type_Template):
    default_nature = "AnaDO"

    def getImportationLine(self):
        return Generic_Template.Generic_Type_Template.getImportationLine(self).replace('Master', 'master').replace('Parents', 'stringparents').replace('children', 'stringchildren').replace(';Type', ';type').replace('SecondAlias','secondAlias')

    def deviceFormat(self, instance=None):
        return ["deviceType", "deviceNumber", 'Alias[,DeviceLinkList]', 'Description', 'Diagnostics', 'WWWLink', 'Synoptic', 'Domain', 'Nature', 'WidgetType', 'Unit',
                'Format', 'RangeMax', 'RangeMin', 'PLiOn', 'PLiOff', 'DriverDeadbandValue', 'DriverDeadbandType', 'ArchiveMode', 'TimeFilter', 'NormalPosition',
                'addr_StsReg01', "addr_StsReg02", 'addr_EvStsReg01', "addr_EvStsReg02", "addr_PosSt", "addr_PosRSt", "addr_AuPosRSt", "addr_MPosRSt",
                "addr_ManReg01", "addr_MPosR", "addr_PLiOn", "addr_PLiOff", 'BooleanArchive', 'AnalogArchive', 'EventArchive', 'MaskEvent', 'Parameters', 'Master',
                'Parents', 'children', 'Type', 'SecondAlias']

    def processInstance(self, instance, params, values):
        values['Unit'] = self.getUnit(instance)
        values['Format'] = self.getFormat(instance)
        values['RangeMin'], values['RangeMax'] = self.getRanges(instance, dependent_object=self.getProcessOutput(instance))
        values['PLiOn'], values['PLiOff'] = self.getPLimits(instance, dependent_object=self.getProcessOutput(instance))
        values['DriverDeadbandValue'] = self.getDriverDeadbandValue(instance)
        values['DriverDeadbandType'] = self.getDriverDeadbandType(instance)
        values['ArchiveMode'], values['TimeFilter'] = self.getArchiveConfig(instance, self.getProcessOutput(instance))
        values['NormalPosition'] = "0"
        values['MaskEvent'] = self.getMaskEvent(instance)

        WarningDelay = self.plugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Warning Time Delay (s)"))
        WarningDeadband = self.plugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Warning Deadband Value (Unit)"))

        values['Parameters'] += self.getParameterValue("PFSPosOn", instance.getAttributeData("FEDeviceParameters:ParReg:Fail-Safe").lower() == "on/open", "TRUE", "FALSE")
        values['Parameters'] += self.getParameterValue("PEnRstart", instance.getAttributeData("FEDeviceParameters:ParReg:Manual Restart after Full Stop").lower() == "false", "FALSE", "TRUE")
        values['Parameters'] += self.getParameterValue("PRstartFS", instance.getAttributeData("FEDeviceParameters:ParReg:Manual Restart after Full Stop").lower() == "true even if full stop still active", "TRUE", "FALSE")
        values['Parameters'] += self.getParameterValue("PHLDrive", instance.getAttributeData("FEDeviceEnvironmentInputs:Local Drive").strip(), "TRUE", "FALSE")
        values['Parameters'] += self.getParameterValue("WARNING_DELAY_TIME", WarningDelay and self.plugin.isString(WarningDelay), self.plugin.getLinkedExpertName(WarningDelay))
        values['Parameters'] += self.getParameterValue("WARNING_DEADBAND", WarningDeadband and self.plugin.isString(WarningDeadband), self.plugin.getLinkedExpertName(WarningDeadband))

        values['Master'] = self.getMaster(instance)

        values['Parents'] += self.getParentControllers(instance)

        io_objects = self.getObjectsFromAttributes(instance, ["FEDeviceEnvironmentInputs:Feedback On",
                                                              "FEDeviceEnvironmentInputs:Feedback Analog",
                                                              "FEDeviceEnvironmentInputs:Local Drive",
                                                              "FEDeviceEnvironmentInputs:Hardware Analog Output",
                                                              "FEDeviceEnvironmentInputs:Local On",
                                                              "FEDeviceEnvironmentInputs:Local Off",
                                                              "FEDeviceOutputs:Analog Process Output",
                                                              "FEDeviceOutputs:Digital Process Output"])

        param_objects = self.getObjectsFromAttributes(instance, ["FEDeviceParameters:Warning Time Delay (s)",

                                                                 "FEDeviceParameters:Warning Deadband Value (Unit)"],
                                                      extend_with_actuators_ios=True)

        values['children'] += io_objects
        values['children'] += param_objects
        values['children'] += self.getChildAlarms(instance)

        values['Alias[,DeviceLinkList]'] += self.getObjectsFromAttributes(instance, ["LogicDeviceDefinitions:Master", "LogicDeviceDefinitions:External Master"])
        values['Alias[,DeviceLinkList]'] += io_objects
        values['Alias[,DeviceLinkList]'] += param_objects
        values['Alias[,DeviceLinkList]'] += self.getParentControllers(instance)
        values['Alias[,DeviceLinkList]'] += self.getDeviceLinksFromSpec(instance)
