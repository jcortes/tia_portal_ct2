# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for AnalogParameter Objects.

import Generic_Template
reload(Generic_Template)


class Local_Template(Generic_Template.Generic_Type_Template):
    default_nature = "Local"

    def deviceFormat(self, instance=None):
        return ["deviceType", "deviceNumber", 'Alias[,DeviceLinkList]', 'Description', 'Diagnostics', 'WWWLink', 'Synoptic', 'Domain', 'Nature', 'WidgetType', 'ArchiveMode',
                'SMSCat', 'AlarmMessage', 'AlarmAck', 'NormalPosition', 'addr_StsReg01', 'addr_EvStsReg01', "addr_ManReg01", 'BooleanArchive', 'AnalogArchive',
                'EventArchive', 'MaskEvent', 'Parameters', 'Master', 'Parents', 'children', 'Type', 'SecondAlias']

    def getArchiveConfig(self, instance):
        archive_mode = instance.getAttributeData("SCADADeviceDataArchiving:Archive Mode").lower()
        # Archiving Mode
        if (archive_mode == "old/new comparison"):
            return "Y"
        else:
            return "N"

    def processInstance(self, instance, params, values):
        values['ArchiveMode'] = self.getArchiveConfig(instance)
        values['SMSCat'] = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:SMS Category")
        values['AlarmMessage'] = instance.getAttributeData("SCADADeviceAlarms:Message")
        values['AlarmAck'] = 'false' if instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Auto Acknowledge").lower().strip() == 'true' else 'true'
        values['NormalPosition'] = '3' if instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Masked").lower() == "true" else '0'
        values['MaskEvent'] = self.getMaskEvent(instance)

        values['Parameters'] += self.getParameterValue("PHFOn", instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback On").strip(), "TRUE", "FALSE")
        values['Parameters'] += self.getParameterValue("PHFOff", instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Off").strip(), "TRUE", "FALSE")

        io_objects = self.getObjectsFromAttributes(instance, ["FEDeviceEnvironmentInputs:Feedback On",
                                                              "FEDeviceEnvironmentInputs:Feedback Off"])
        values['children'] += io_objects

        values['Alias[,DeviceLinkList]'] += io_objects
        values['Alias[,DeviceLinkList]'] += self.getDeviceLinksFromSpec(instance)
