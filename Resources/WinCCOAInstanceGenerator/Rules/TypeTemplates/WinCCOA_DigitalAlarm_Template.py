# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for DigitalAlarm Objects.


import Generic_Template
reload(Generic_Template)


class DigitalAlarm_Template(Generic_Template.Generic_Type_Template):
    default_nature = "DA"

    def deviceFormat(self, instance=None):
        return ["deviceType", "deviceNumber", 'Alias[,DeviceLinkList]', 'Description', 'Diagnostics', 'WWWLink', 'Synoptic', 'Domain', 'Nature', 'WidgetType', 'ArchiveMode',
                'SMSCat', 'AlarmMessage', 'AlarmAck', "Level", 'NormalPosition', 'addr_StsReg01', 'addr_EvStsReg01', "addr_ManReg01", 'BooleanArchive',
                'AnalogArchive', 'EventArchive', 'MaskEvent', 'Parameters', 'Master', 'Parents', 'children', 'Type', 'SecondAlias']

    def processInstance(self, instance, params, values):
        values['ArchiveMode'] = self.getDigitalArchiveConfig(instance)
        values['SMSCat'] = self.getSMSCategory(instance)
        values['AlarmMessage'] = self.getAlarmMessage(instance, remove_commas=False, use_description=True)
        values['AlarmAck'] = self.getAlarmAckBoolean(instance)
        values["Level"] = self.getAlarmLevel(instance)
        values['NormalPosition'] = self.getAlarmNormalPosition(instance)
        values['MaskEvent'] = self.getMaskEvent(instance)

        values['Parameters'] += self.getParameterValue("PAlDt", True, self.getDelayParameter(instance))
        values['Parameters'] += self.getParameterValue("INPUT_SOURCE", True, str(self.plugin.getLinkedExpertName(self.decorator.extractSingleUnicosObjectFromPLCExpression(instance.getAttributeData("FEDeviceEnvironmentInputs:Input"), True))))

        master_objects = self.getObjectsFromAttributes(instance, ["LogicDeviceDefinitions:Master"], extend_with_actuators_ios=True)

        values['Parents'] += master_objects

        objects = self.getObjectsFromAttributes(instance, ["FEDeviceEnvironmentInputs:Input",
                                                           "FEDeviceParameters:Alarm Delay (s)"],
                                                extend_with_actuators_ios=True)
        values['children'] += objects

        values['Alias[,DeviceLinkList]'] += master_objects
        values['Alias[,DeviceLinkList]'] += objects
        values['Alias[,DeviceLinkList]'] += self.getDeviceLinksFromSpec(instance)

        values['Type'] = self.getAlarmType(instance)
