# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for WordStatus Objects.

import Generic_Template
reload(Generic_Template)


class WordStatus_Template(Generic_Template.Generic_Type_Template):
    default_nature = "WS"

    def isW2A(self, instance):
        # function returns True if WordStatus instance is a Word2AnalogStatus, False otherwise
        return instance.getAttributeData("SCADADeviceGraphics:Widget Type") == "Word2AnalogStatus"

    def deviceFormat(self, instance):
        if self.isW2A(instance):
            return self.W2AdeviceFormat()
        else:
            return self.WSdeviceFormat()

    def WSdeviceFormat(self):
        return ["deviceType", "deviceNumber", 'Alias[,DeviceLinkList]', 'Description', 'Diagnostics', 'WWWLink', 'Synoptic', 'Domain', 'Nature', 'WidgetType', 'Unit',
                'Format', 'DriverDeadbandValue', 'DriverDeadbandType', 'RangeMax', 'RangeMin', 'ArchiveMode', 'TimeFilter', "addr_PosSt", 'Pattern', 'BooleanArchive',
                'AnalogArchive', 'EventArchive', 'Parameters', 'Master', 'Parents', 'children', 'Type', 'SecondAlias']

    def W2AdeviceFormat(self):
        return ["deviceType", "deviceNumber", 'Alias[,DeviceLinkList]', 'Description', 'Diagnostics', 'WWWLink', 'Synoptic', 'Domain', 'Nature', 'WidgetType', 'Unit',
                'Format', 'DriverDeadbandValue', 'DriverDeadbandType', 'RangeMax', 'RangeMin', 'FRangeMax', 'FRangeMin', 'ArchiveMode', 'TimeFilter', "addr_PosSt",
                'BooleanArchive', 'AnalogArchive', 'EventArchive', 'Parameters', 'Master', 'Parents', 'children', 'Type', 'SecondAlias']

    def writeDeviceHeader(self):
        # Write some content in the content buffer
        self.plugin.writeComment("#")
        self.plugin.writeComment("# Object: WordStatus and Word2AnalogStatus ")
        self.plugin.writeComment("#")
        self.plugin.writeComment("#Config Line : CPC_WordStatus;%s" % ";".join(self.WSdeviceFormat()).replace("deviceType;", '').replace('Master', 'master').replace('Parents', 'parents').replace(';Type', ';type').replace('SecondAlias','secondAlias') + ';')
        self.plugin.writeComment("#Config Line : CPC_Word2AnalogStatus;%s" % ";".join(self.W2AdeviceFormat()).replace("deviceType;", '').replace('Master', 'master').replace('Parents', 'parents').replace(';Type', ';type').replace('SecondAlias','secondAlias') + ';')
        self.plugin.writeComment("#")

    def getUnit(self, instance):
        pattern = self.getPattern(instance)
        if not self.isW2A(instance) and pattern:
            return '-'
        else:
            return instance.getAttributeData("SCADADeviceGraphics:Unit")

    def getFormat(self, instance):
        format = instance.getAttributeData("SCADADeviceGraphics:Format").replace(" ", "")
        if self.isW2A(instance):
            if not format:
                self.writeWarning(instance, "Format is missing, set as Format: (#####.#).")
                return "#####.#"
            elif "." not in format:
                self.writeWarning(instance, "Format should be decimal, set Format: (#####.#).")
                return "#####.#"
        else:
            pattern = self.getPattern(instance)
            if pattern:          # bit decoding
                return "######"  # format is useless but WinCCOA require format, '-' is not accepted.
            else:
                if not format:
                    self.writeWarning(instance, "Format is missing, set Format: (######).")
                    return "######"
                elif '.' in format:
                    widget_type = instance.getAttributeData("SCADADeviceGraphics:Widget Type")
                    self.writeWarning(instance, "Format (" + widget_type + ") cannot be decimal, set Format: (######).")
                    return "######"
        return format

    def labelFilter(self, pair):
        return pair[0] in ["no_status", "multiple_status"]

    def getCustomLabels(self, instance):
        labels = self.getWordLabels(instance)
        customLabels = filter(lambda pair: self.labelFilter(pair), labels)  # extract custom labels
        return customLabels

    def getNonCustomLabels(self, instance):
        labels = self.getWordLabels(instance)
        nonCustomLabels = filter(lambda pair: not self.labelFilter(pair), labels)  # remove custom labels from pattern
        return nonCustomLabels

    def getPattern(self, instance):
        pattern = instance.getAttributeData("SCADADeviceGraphics:Pattern")
        if not self.isW2A(instance):
            patternList = self.getNonCustomLabels(instance)
            pattern = ",".join(map(lambda pair: "=".join(pair), patternList))
        return pattern

    def processInstance(self, instance, params, values):
        widget_type = instance.getAttributeData("SCADADeviceGraphics:Widget Type")

        if widget_type == "Word2AnalogStatus":
            values['deviceType'] = "CPC_Word2AnalogStatus"

        values['Unit'] = self.getUnit(instance)
        values['Format'] = self.getFormat(instance)
        values['DriverDeadbandValue'] = self.getDriverDeadbandValue(instance)
        values['DriverDeadbandType'] = self.getDriverDeadbandType(instance)
        values['RangeMin'], values['RangeMax'] = self.getRanges(instance, default_min=0, default_max=100, format_number_plc=False)
        values['FRangeMin'], values['FRangeMax'] = self.getRanges(instance, range_max_attr="SCADADeviceParameters:F Range Max", range_min_attr="SCADADeviceParameters:F Range Min", format_number_plc=False)
        values['ArchiveMode'], values['TimeFilter'] = self.getArchiveConfig(instance, default_time_filter="0")
        values['Pattern'] = self.getPattern(instance)

        customLabels = dict(self.getCustomLabels(instance))
        if "no_status" in customLabels:
            values['Parameters'] += self.getParameterValue("PNoStatus", True, customLabels["no_status"])
        if "multiple_status" in customLabels:
            values['Parameters'] += self.getParameterValue("PMultipleStatus", True, customLabels["multiple_status"])

        alarm_values_low = instance.getAttributeData("SCADADeviceAlarms:Alarm Values:Low").replace(" ", "").replace(",", "|")
        alarm_values_medium = instance.getAttributeData("SCADADeviceAlarms:Alarm Values:Medium").replace(" ", "").replace(",", "|")
        alarm_values_high = instance.getAttributeData("SCADADeviceAlarms:Alarm Values:High").replace(" ", "").replace(",", "|")
        alarm_values_safety = instance.getAttributeData("SCADADeviceAlarms:Alarm Values:Safety").replace(" ", "").replace(",", "|")
        alarm_sms = self.getSMSCategory(instance).replace(",", "|")
        alarm_message = self.getAlarmMessage(instance)
        alarm_ack = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Auto Acknowledge").replace(" ", "").lower() == "true"
        alarm_active = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Masked").replace(" ", "").lower() == "true"

        values['Parameters'] += self.getParameterValue("ALARM_VALUES_LOW", alarm_values_low, alarm_values_low)
        values['Parameters'] += self.getParameterValue("ALARM_VALUES_MEDIUM", alarm_values_medium, alarm_values_medium)
        values['Parameters'] += self.getParameterValue("ALARM_VALUES_HIGH", alarm_values_high, alarm_values_high)
        values['Parameters'] += self.getParameterValue("ALARM_VALUES_SAFETY", alarm_values_safety, alarm_values_safety)
        values['Parameters'] += self.getParameterValue("ALARM_SMS", alarm_sms, alarm_sms)
        values['Parameters'] += self.getParameterValue("ALARM_ACK", alarm_ack, "false")
        values['Parameters'] += self.getParameterValue("ALARM_ACTIVE", alarm_active, "false")
        values['Parameters'] += self.getParameterValue("ALARM_MESSAGE", alarm_message, alarm_message)

        values['Parents'] += self.getRIndex(instance, "DigitalAlarm", "FEDeviceEnvironmentInputs:Input")
        values['Parents'] += self.getRIndex(instance, "AnalogAlarm", "FEDeviceAlarm:Enable Condition")

        values['Alias[,DeviceLinkList]'] += self.getDeviceLinksFromSpec(instance)
