# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for AnalogParameter Objects.

import Generic_Template
reload(Generic_Template)


class WordParameter_Template(Generic_Template.Generic_Type_Template):
    default_nature = "WPar"
    
    def getImportationLine(self):
        return Generic_Template.Generic_Type_Template.getImportationLine(self).replace('Master', 'master').replace('Parents', 'parents').replace(';Type', ';type').replace('SecondAlias','secondAlias')

    def deviceFormat(self, instance=None):
        return ["deviceType", "deviceNumber", 'Alias[,DeviceLinkList]', 'Description', 'Diagnostics', 'WWWLink', 'Synoptic', 'Domain', 'Nature', 'WidgetType', 'Unit',
                'Format', 'DriverDeadbandValue', 'DriverDeadbandType', 'RangeMax', 'RangeMin', 'ArchiveMode', 'TimeFilter', "addr_MPosRSt", "addr_PosSt",
                "addr_MPosR", 'addr_StsReg01', 'addr_EvStsReg01', "addr_ManReg01", 'DefaultValue', 'BooleanArchive', 'AnalogArchive', 'EventArchive', 'MaskEvent',
                'Pattern', 'Master', 'Parents', 'children', 'Type', 'SecondAlias']

    def getPattern(self, instance):
        return ",".join(map(lambda pair: "=".join(pair), self.getWordLabels(instance)))

    def processInstance(self, instance, params, values):
        values['Unit'] = self.getUnit(instance)
        values['Format'] = self.getFormat(instance)
        values['DriverDeadbandValue'] = self.getDriverDeadbandValue(instance)
        values['DriverDeadbandType'] = self.getDriverDeadbandType(instance)
        values['RangeMin'], values['RangeMax'] = self.getRanges(instance, default_min=0, default_max=100, format_number_plc=False)
        values['ArchiveMode'], values['TimeFilter'] = self.getArchiveConfig(instance)

        values['DefaultValue'] = instance.getAttributeData("FEDeviceParameters:Default Value")
        values['MaskEvent'] = self.getMaskEvent(instance)
        values['Pattern'] = self.getPattern(instance)

        values['Parents'] += self.getRIndex(instance, "DigitalAlarm", "FEDeviceEnvironmentInputs:Input")
        values['Parents'] += self.getRIndex(instance, "AnalogAlarm", "FEDeviceAlarm:Enable Condition")

        values['Alias[,DeviceLinkList]'] += self.getDeviceLinksFromSpec(instance)
