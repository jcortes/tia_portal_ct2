# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for AnalogStatus Objects.

import Generic_Template
reload(Generic_Template)


class AnalogStatus_Template(Generic_Template.Generic_Type_Template):
    default_nature = "AS"

    def deviceFormat(self, instance=None):
        return ["deviceType", "deviceNumber", 'Alias[,DeviceLinkList]', 'Description', 'Diagnostics', 'WWWLink', 'Synoptic', 'Domain', 'Nature', 'WidgetType', 'Unit',
                'Format', 'DriverDeadbandValue', 'DriverDeadbandType', 'RangeMax', 'RangeMin', 'ArchiveMode', 'TimeFilter', "addr_PosSt", 'BooleanArchive',
                'AnalogArchive', 'EventArchive', 'Parameters', 'Master', 'Parents', 'children', 'Type', 'SecondAlias']

    def processInstance(self, instance, params, values):
        values['Unit'] = self.getUnit(instance)
        values['Format'] = self.getFormat(instance)
        values['DriverDeadbandValue'] = self.getDriverDeadbandValue(instance)
        values['DriverDeadbandType'] = self.getDriverDeadbandType(instance)
        values['RangeMin'], values['RangeMax'] = self.getRanges(instance)
        values['ArchiveMode'], values['TimeFilter'] = self.getArchiveConfig(instance)

        filter_time = self.plugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Filtering Time (s)").replace(" ", ""))
        hhLimit, hLimit, lLimit, llLimit = self.get5RangeLimits(instance)
        alarm_ack = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Auto Acknowledge").replace(" ", "").lower() == "true"
        alarm_message = self.getAlarmMessage(instance)
        alarm_active = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Masked").replace(" ", "").lower() == "true"
        alarm_sms = self.getSMSCategory(instance).replace(",", "|")
        values['Parameters'] += self.getParameterValue("FIRST_ORDER_FILTER", filter_time, filter_time)
        values['Parameters'] += self.getParameterValue("ALARM_VALUES", hhLimit or hLimit or lLimit or llLimit, hhLimit + "|" + hLimit + "|" + lLimit + "|" + llLimit + "|")
        values['Parameters'] += self.getParameterValue("ALARM_SMS", alarm_sms, alarm_sms)
        values['Parameters'] += self.getParameterValue("ALARM_ACK", alarm_ack, "false")
        values['Parameters'] += self.getParameterValue("ALARM_ACTIVE", alarm_active, "false")
        values['Parameters'] += self.getParameterValue("ALARM_MESSAGE", alarm_message, alarm_message)

        values['Parents'] += self.getRIndex(instance, "Analog,AnaDO,AnalogDigital", "FEDeviceParameters:Warning Deadband Value (Unit)")
        values['Parents'] += self.getRIndex(instance, "Analog,AnaDO,AnalogDigital,OnOff", "FEDeviceParameters:Warning Time Delay (s)")
        values['Parents'] += self.getRIndex(instance, "Controller", ["FEDeviceVariables:Default PID Parameters:Kc",
                                                                     "FEDeviceVariables:Default PID Parameters:Ti",
                                                                     "FEDeviceVariables:Default PID Parameters:Td",
                                                                     "FEDeviceVariables:Default PID Parameters:Tds",
                                                                     "FEDeviceVariables:Default PID Parameters:SP High Limit",
                                                                     "FEDeviceVariables:Default PID Parameters:SP Low Limit",
                                                                     "FEDeviceVariables:Default PID Parameters:Out High Limit",
                                                                     "FEDeviceVariables:Default PID Parameters:Out Low Limit",
                                                                     "FEDeviceVariables:Default PID Parameters:Setpoint"])
        values['Parents'] += self.getRIndex(instance, "DigitalAlarm", "FEDeviceEnvironmentInputs:Input")
        values['Parents'] += self.getRIndex(instance, "AnalogAlarm", ["FEDeviceEnvironmentInputs:Input",
                                                                      "FEDeviceAlarm:Enable Condition",
                                                                      "FEDeviceManualRequests:HH Alarm",
                                                                      "FEDeviceManualRequests:H Warning",
                                                                      "FEDeviceManualRequests:L Warning",
                                                                      "FEDeviceManualRequests:LL Alarm"])
        values['Parents'] += self.getRIndex(instance, "AnalogAlarm,DigitalAlarm", "FEDeviceParameters:Alarm Delay (s)")

        values['Alias[,DeviceLinkList]'] += self.getDeviceLinksFromSpec(instance)
