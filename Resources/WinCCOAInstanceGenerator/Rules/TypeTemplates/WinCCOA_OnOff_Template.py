# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for OnOff Objects.

import Generic_Template
reload(Generic_Template)


class OnOff_Template(Generic_Template.Generic_Type_Template):
    default_nature = "OnOff"

    def deviceFormat(self, instance=None):
        return ["deviceType", "deviceNumber", 'Alias[,DeviceLinkList]', 'Description', 'Diagnostics', 'WWWLink', 'Synoptic', 'Domain', 'Nature', 'WidgetType',
                'NormalPosition', 'addr_StsReg01', "addr_StsReg02", 'addr_EvStsReg01', "addr_EvStsReg02", "addr_ManReg01", 'BooleanArchive', 'AnalogArchive',
                'EventArchive', 'MaskEvent', 'LabelOn', 'LabelOff', 'Parameters', 'Master', 'Parents', 'children', 'Type', 'SecondAlias']

    def processInstance(self, instance, params, values):
        values['NormalPosition'] = '0'
        values['MaskEvent'] = self.getMaskEvent(instance)
        values['LabelOn'] = instance.getAttributeData("SCADADeviceGraphics:Label On").strip()
        values['LabelOff'] = instance.getAttributeData("SCADADeviceGraphics:Label Off").strip()

        values['Parameters'] += self.getParameterValue("PFSPosOn", instance.getAttributeData("FEDeviceParameters:ParReg:Fail-Safe").lower() == "on/open", "TRUE")
        values['Parameters'] += self.getParameterValue("PFSPosOn", instance.getAttributeData("FEDeviceParameters:ParReg:Fail-Safe").lower() == "off/close", "FALSE")
        values['Parameters'] += self.getParameterValue("PFSPosOn", instance.getAttributeData("FEDeviceParameters:ParReg:Fail-Safe").lower() == "2 do off", "2DOOFF")
        values['Parameters'] += self.getParameterValue("PFSPosOn", instance.getAttributeData("FEDeviceParameters:ParReg:Fail-Safe").lower() == "2 do on", "2DOON")
        values['Parameters'] += self.getParameterValue("PHFOnSt", instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback On").strip(), "TRUE", "FALSE")
        values['Parameters'] += self.getParameterValue("PHFOffSt", instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Off").strip(), "TRUE", "FALSE")
        values['Parameters'] += self.getParameterValue("PPulseSt", instance.getAttributeData("FEDeviceParameters:Pulse Duration (s)").strip() in ['', '0'], "FALSE", "TRUE")
        values['Parameters'] += self.getParameterValue("PEnRstart", instance.getAttributeData("FEDeviceParameters:ParReg:Manual Restart after Full Stop").strip().lower() == "false", "FALSE", "TRUE")
        values['Parameters'] += self.getParameterValue("PRstartFS", instance.getAttributeData("FEDeviceParameters:ParReg:Manual Restart after Full Stop").strip().lower() == "true even if full stop still active", "TRUE", "FALSE")
        values['Parameters'] += self.getParameterValue("POutOff", instance.getAttributeData("FEDeviceOutputs:Process Output Off").strip(), "TRUE", "FALSE")
        values['Parameters'] += self.getParameterValue("PHLDrive", instance.getAttributeData("FEDeviceEnvironmentInputs:Local Drive").strip(), "TRUE", "FALSE")
        WarningDelay = self.plugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Warning Time Delay (s)"))
        values['Parameters'] += self.getParameterValue("WARNING_DELAY_TIME", WarningDelay and self.plugin.isString(WarningDelay), self.plugin.getLinkedExpertName(WarningDelay))

        values['Master'] = self.getMaster(instance)

        values['Alias[,DeviceLinkList]'] += self.getObjectsFromAttributes(instance, ["LogicDeviceDefinitions:Master",
                                                                                  "LogicDeviceDefinitions:External Master"])

        io_objects = self.getObjectsFromAttributes(instance, ["FEDeviceEnvironmentInputs:Feedback On",
                                                              "FEDeviceEnvironmentInputs:Feedback Off",
                                                              "FEDeviceEnvironmentInputs:Local Drive",
                                                              "FEDeviceEnvironmentInputs:Local On",
                                                              "FEDeviceEnvironmentInputs:Local Off",
                                                              "FEDeviceOutputs:Process Output",
                                                              "FEDeviceOutputs:Process Output Off"])

        param_objects = self.getObjectsFromAttributes(instance, ["FEDeviceParameters:Warning Time Delay (s)"], extend_with_actuators_ios=True)

        values['children'] += io_objects
        values['children'] += param_objects
        values['children'] += self.getChildAlarms(instance)

        values['Alias[,DeviceLinkList]'] += io_objects
        values['Alias[,DeviceLinkList]'] += self.getDeviceLinksFromSpec(instance)
        values['Alias[,DeviceLinkList]'] += param_objects
