# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for AnalogParameter Objects.
# todo: use generic functions
import Generic_Template
reload(Generic_Template)


class Controller_Template(Generic_Template.Generic_Type_Template):
    default_nature = "PID"

    def getImportationLine(self):
        return Generic_Template.Generic_Type_Template.getImportationLine(self).replace('Parents', 'parents').replace('children', 'stringchildren').replace(';Type', ';type').replace('SecondAlias','secondAlias')

    def deviceFormat(self, instance=None):
        return ["deviceType", "deviceNumber", 'Alias[,DeviceLinkList]', 'Description', 'Diagnostics', 'WWWLink', 'Synoptic', 'Domain', 'Nature', 'WidgetType', 'Alias',
                'HMV_Unit', 'HMV_Format', "HMV_RangeMax", "HMV_RangeMin", "Out_Unit", "Out_Format", "Out_RangeMax", "Out_RangeMin", "ScaMethod", "DriverDeadbandValueOut",
                "DriverDeadbandTypeOut", "DriverDeadbandValueHMV", "DriverDeadbandTypeHMV", "ArchiveModeOut", "TimeFilterOut", "ArchiveModeHMV", "TimeFilterHMV",
                'addr_StsReg01', "addr_StsReg02", 'addr_EvStsReg01', "addr_EvStsReg02", "addr_ActSP", "addr_ActSPL", "addr_ActSPH", "addr_OutOV",
                "addr_ActOutL", "addr_ActOutH", "addr_MV", "addr_MSPSt", "addr_AuSPSt", "addr_MPosRSt", "addr_AuPosRSt", "addr_ActKc", "addr_ActTi",
                "addr_ActTd", "addr_ActTds", "addr_ManReg01", "addr_ManReg02", "addr_MPosR", "addr_MSP", "addr_MSPL", "addr_MSPH", "addr_MOutL",
                "addr_MOutH", "addr_MKc", "addr_MTi", "addr_MTd", "addr_MTds", 'BooleanArchive', 'AnalogArchive', 'EventArchive', 'MaskEvent', "Def_Kc",
                "Def_Ti", "Def_Td", "Def_Tds", "Def_SP", "Def_SPH", "Def_SPL", "Def_OutH", "Def_OutL", 'Parameters', 'Master', 'Parents', 'children', 'Type',
                'SecondAlias']

    def getControlledObjects(self, instance):
        controlled_object_list = instance.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ").split()
        output_list = []
        for name in controlled_object_list:
            output_instance = self.spec.findInstanceByName(name)
            if output_instance:
                output_list.append(output_instance)
        return output_list

    def getFirstControlledObject(self, instance):
        controlled_objects = self.getControlledObjects(instance)
        if len(controlled_objects) > 0:
            self.writeWarning(instance, "first controlled object: " + controlled_objects[0].getAttributeData("DeviceIdentification:Name"))
            return controlled_objects[0]
        else:
            return None

    def getScalingMethod(self, instance):
        scaling_method = instance.getAttributeData("FEDeviceParameters:Controller Parameters:Scaling Method").lower()
        if scaling_method == "input scaling":
            return "1"
        elif scaling_method == "input/output scaling":
            return "2"
        elif(scaling_method == "no scaling"):
            return "3"

    def getUnit(self, instance, default_unit="%"):
        mv_instance = self.getMeasuredValueInstance(instance)
        if mv_instance:
            if self.isInstanceOfType(mv_instance, ["massflowcontroller"]):
                return mv_instance.getAttributeData("SCADADeviceGraphics:Flow:Unit")
            else:
                return mv_instance.getAttributeData("SCADADeviceGraphics:Unit")
        else:
            return default_unit

    def getFormat(self, instance, default_format="###.#"):
        mv_instance = self.getMeasuredValueInstance(instance)
        if mv_instance:
            if self.isInstanceOfType(mv_instance, ["massflowcontroller"]):
                return mv_instance.getAttributeData("SCADADeviceGraphics:Flow:Format")
            else:
                return mv_instance.getAttributeData("SCADADeviceGraphics:Format")
        else:
            return default_format

    def getOutUnit(self, instance):
        controlled_object = self.getFirstControlledObject(instance)
        if self.getScalingMethod(instance) == "1":
            return "%"
        elif controlled_object:
            if self.isInstanceOfType(controlled_object, ["controller"]):
                return self.getUnit(controlled_object, default_unit="%")
            elif self.isInstanceOfType(controlled_object, ["analog", "analogdigital", "anado"]):
                return controlled_object.getAttributeData("SCADADeviceGraphics:Unit")
            elif self.isInstanceOfType(controlled_object, ["massflowcontroller"]):
                return controlled_object.getAttributeData("SCADADeviceGraphics:Flow:Unit")
        else:
            return "%"

    def getOutFormat(self, instance):
        controlled_object = self.getFirstControlledObject(instance)
        if self.getScalingMethod(instance) == "1":
            return "###.#"
        elif controlled_object:
            if self.isInstanceOfType(controlled_object, ["controller"]):
                return self.getFormat(controlled_object, default_format="###.#")
            elif self.isInstanceOfType(controlled_object, ["analog", "analogdigital", "anado"]):
                return controlled_object.getAttributeData("SCADADeviceGraphics:Format")
            elif self.isInstanceOfType(controlled_object, ["massflowcontroller"]):
                return controlled_object.getAttributeData("SCADADeviceGraphics:Flow:Format")
        else:
            return "###.#"

    def getDefaultValue(self, instance, attribute, default_value, range_min=None, range_max=None):
        value = self.plugin.formatNumberPLC(instance.getAttributeData(attribute))
        self.writeWarning(instance, "getDefaultValue, value: " + value + " attr: " + attribute + ", def: " + str(default_value) + " ranges: " + str(range_min) + ", " + str(range_max))
        if value == "" or self.plugin.isString(value):
            return default_value
        else:
            if range_min and float(value) < float(range_min):
                return range_min
            elif range_max and float(value) > float(range_max):
                return range_max
            return value

    def processInstance(self, instance, params, values):
        values['Alias'] = self.getWinCCOAAlias(instance)
        values['HMV_Unit'] = self.getUnit(instance)
        values['HMV_Format'] = self.getFormat(instance)
        values['HMV_RangeMin'], values['HMV_RangeMax'] = self.getRanges(instance, default_max="100.0", default_min="0.0", dependent_object=self.getMeasuredValueInstance(instance))
        values['Out_Unit'] = self.getOutUnit(instance)
        values['Out_Format'] = self.getOutFormat(instance)
        # In percentage scaling, PID output is 0/100 %
        if (self.getScalingMethod(instance) == "1"):
            values['Out_RangeMin'] = "0.0"
            values['Out_RangeMax'] = "100.0"
        else:
            values['Out_RangeMin'], values['Out_RangeMax'] = self.getRanges(instance, range_max_attr="FEDeviceParameters:Controller Parameters:Output Range Max", range_min_attr="FEDeviceParameters:Controller Parameters:Output Range Min", dependent_object=self.getFirstControlledObject(instance))

        values['ScaMethod'] = self.getScalingMethod(instance)
        values['DriverDeadbandValueOut'] = self.getDriverDeadbandValue(instance, "SCADADriverDataSmoothing:Output Smoothing:Deadband Value", "SCADADriverDataSmoothing:Output Smoothing:Deadband Type")
        values['DriverDeadbandTypeOut'] = self.getDriverDeadbandType(instance, "SCADADriverDataSmoothing:Output Smoothing:Deadband Type")
        values['DriverDeadbandValueHMV'] = self.getDriverDeadbandValue(instance, "SCADADriverDataSmoothing:MV and SP Smoothing:Deadband Value", "SCADADriverDataSmoothing:MV and SP Smoothing:Deadband Type")
        values['DriverDeadbandTypeHMV'] = self.getDriverDeadbandType(instance, "SCADADriverDataSmoothing:MV and SP Smoothing:Deadband Type")
        values['ArchiveModeOut'], values['TimeFilterOut'] = self.getArchiveConfig(instance, dependent_object=self.getFirstControlledObject(instance), archive_mode_family="SCADADeviceDataArchiving:Output Archiving", default_time_filter="0")
        values['ArchiveModeHMV'], values['TimeFilterHMV'] = self.getArchiveConfig(instance, dependent_object=self.getMeasuredValueInstance(instance), archive_mode_family="SCADADeviceDataArchiving:MV and SP Archiving", default_time_filter="0")
        values['MaskEvent'] = self.getMaskEvent(instance)

        values['Def_Kc'] = self.getDefaultValue(instance, "FEDeviceVariables:Default PID Parameters:Kc", "1.0")
        values['Def_Ti'] = self.getDefaultValue(instance, "FEDeviceVariables:Default PID Parameters:Ti", "100.0")
        values['Def_Td'] = self.getDefaultValue(instance, "FEDeviceVariables:Default PID Parameters:Td", "0.0")
        values['Def_Tds'] = self.getDefaultValue(instance, "FEDeviceVariables:Default PID Parameters:Tds", "0.0")
        values['Def_SP'] = self.getDefaultValue(instance, "FEDeviceVariables:Default PID Parameters:Setpoint", "0.0")
        values['Def_SPH'] = self.getDefaultValue(instance, "FEDeviceVariables:Default PID Parameters:SP High Limit", (values['HMV_RangeMax'] if values['HMV_RangeMax'] else "100.0"), range_min=values['HMV_RangeMin'], range_max=values['HMV_RangeMax'])
        values['Def_SPL'] = self.getDefaultValue(instance, "FEDeviceVariables:Default PID Parameters:SP Low Limit", (values['HMV_RangeMin'] if values['HMV_RangeMin'] else "0.0"), range_min=values['HMV_RangeMin'], range_max=values['HMV_RangeMax'])
        values['Def_OutH'] = self.getDefaultValue(instance, "FEDeviceVariables:Default PID Parameters:Out High Limit", values['Out_RangeMax'], range_min=values['Out_RangeMin'], range_max=values['Out_RangeMax'])
        values['Def_OutL'] = self.getDefaultValue(instance, "FEDeviceVariables:Default PID Parameters:Out Low Limit", values['Out_RangeMin'], range_min=values['Out_RangeMin'], range_max=values['Out_RangeMax'])

        Def_Kc = self.plugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Kc"))
        Def_Ti = self.plugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Ti"))
        Def_Td = self.plugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Td"))
        Def_Tds = self.plugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Tds"))

        Def_SPH = self.plugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:SP High Limit"))
        Def_SPL = self.plugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:SP Low Limit"))
        Def_OutH = self.plugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Out High Limit"))
        Def_OutL = self.plugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Out Low Limit"))
        Def_SP = self.plugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Setpoint"))

        values['Parameters'] += self.getParameterValue("SAMPLING_TIME", instance.getAttributeData("FEDeviceParameters:Controller Parameters:PID Cycle (s)"), instance.getAttributeData("FEDeviceParameters:Controller Parameters:PID Cycle (s)"), "")
        values['Parameters'] += self.getParameterValue("RA", instance.getAttributeData("FEDeviceParameters:Controller Parameters:RA"), instance.getAttributeData("FEDeviceParameters:Controller Parameters:RA"), "")
        values['Parameters'] += self.getParameterValue("MV", instance.getAttributeData("FEDeviceEnvironmentInputs:Measured Value"), self.plugin.getLinkedExpertName(instance.getAttributeData("FEDeviceEnvironmentInputs:Measured Value")))
        values['Parameters'] += self.getParameterValue("OUT", self.getControlledObjects(instance), '|'.join(instance.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ").split()))
        values['Parameters'] += self.getParameterValue("SP_SOURCE", Def_SP and self.plugin.isString(Def_SP), self.plugin.getLinkedExpertName(Def_SP))
        values['Parameters'] += self.getParameterValue("KC_SOURCE", Def_Kc and self.plugin.isString(Def_Kc), self.plugin.getLinkedExpertName(Def_Kc))
        values['Parameters'] += self.getParameterValue("TI_SOURCE", Def_Ti and self.plugin.isString(Def_Ti), self.plugin.getLinkedExpertName(Def_Ti))
        values['Parameters'] += self.getParameterValue("TD_SOURCE", Def_Td and self.plugin.isString(Def_Td), self.plugin.getLinkedExpertName(Def_Td))
        values['Parameters'] += self.getParameterValue("TDS_SOURCE", Def_Tds and self.plugin.isString(Def_Tds), self.plugin.getLinkedExpertName(Def_Tds))
        values['Parameters'] += self.getParameterValue("SPH_SOURCE", Def_SPH and self.plugin.isString(Def_SPH), self.plugin.getLinkedExpertName(Def_SPH))
        values['Parameters'] += self.getParameterValue("SPL_SOURCE", Def_SPL and self.plugin.isString(Def_SPL), self.plugin.getLinkedExpertName(Def_SPL))
        values['Parameters'] += self.getParameterValue("OUTH_SOURCE", Def_OutH and self.plugin.isString(Def_OutH), self.plugin.getLinkedExpertName(Def_OutH))
        values['Parameters'] += self.getParameterValue("OUTL_SOURCE", Def_OutL and self.plugin.isString(Def_OutL), self.plugin.getLinkedExpertName(Def_OutL))
        values['Master'] = self.getMaster(instance)

        dep_objects = self.getObjectsFromAttributes(instance, ["FEDeviceEnvironmentInputs:Measured Value",
                                                               "FEDeviceOutputs:Controlled Objects",
                                                               "FEDeviceVariables:Default PID Parameters:Setpoint",
                                                               "FEDeviceVariables:Default PID Parameters:Kc",
                                                               "FEDeviceVariables:Default PID Parameters:Ti",
                                                               "FEDeviceVariables:Default PID Parameters:Td",
                                                               "FEDeviceVariables:Default PID Parameters:Tds",
                                                               "FEDeviceVariables:Default PID Parameters:SP High Limit",
                                                               "FEDeviceVariables:Default PID Parameters:SP Low Limit",
                                                               "FEDeviceVariables:Default PID Parameters:Out High Limit",
                                                               "FEDeviceVariables:Default PID Parameters:Out Low Limit"],
                                                    extend_with_actuators_ios=True)

        values['children'] += dep_objects

        values['Alias[,DeviceLinkList]'] += self.getObjectsFromAttributes(instance, ["LogicDeviceDefinitions:Master", "LogicDeviceDefinitions:External Master"])
        values['Alias[,DeviceLinkList]'] += dep_objects
        values['Alias[,DeviceLinkList]'] += self.getDeviceLinksFromSpec(instance)
