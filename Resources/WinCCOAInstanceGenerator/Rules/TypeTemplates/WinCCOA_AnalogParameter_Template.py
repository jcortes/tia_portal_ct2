# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for AnalogParameter Objects.

import Generic_Template
reload(Generic_Template)


class AnalogParameter_Template(Generic_Template.Generic_Type_Template):
    default_nature = "APar"

    def deviceFormat(self, instance=None):
        return ["deviceType", "deviceNumber", 'Alias[,DeviceLinkList]', 'Description', 'Diagnostics', 'WWWLink', 'Synoptic', 'Domain', 'Nature', 'WidgetType', 'Unit',
                'Format', 'DriverDeadbandValue', 'DriverDeadbandType', 'RangeMax', 'RangeMin', 'ArchiveMode', 'TimeFilter', "addr_MPosRSt", "addr_PosSt",
                "addr_MPosR", 'addr_StsReg01', 'addr_EvStsReg01', "addr_ManReg01", 'DefaultValue', 'BooleanArchive', 'AnalogArchive', 'EventArchive', 'MaskEvent',
                'Parameters', 'Master', 'Parents', 'children', 'Type', 'SecondAlias']

    def processInstance(self, instance, params, values):
        values['Unit'] = self.getUnit(instance)
        values['Format'] = self.getFormat(instance)
        values['DriverDeadbandValue'] = self.getDriverDeadbandValue(instance)
        values['DriverDeadbandType'] = self.getDriverDeadbandType(instance)
        values['RangeMin'], values['RangeMax'] = self.getRanges(instance)
        values['ArchiveMode'], values['TimeFilter'] = self.getArchiveConfig(instance)

        values['DefaultValue'] = instance.getAttributeData("FEDeviceParameters:Default Value")
        values['MaskEvent'] = self.getMaskEvent(instance)

        values['Parents'] += self.getRIndex(instance, "Analog,AnaDO,AnalogDigital", "FEDeviceParameters:Warning Deadband Value (Unit)")
        values['Parents'] += self.getRIndex(instance, "Analog,AnaDO,AnalogDigital,OnOff", "FEDeviceParameters:Warning Time Delay (s)")
        values['Parents'] += self.getRIndex(instance, "Controller", ["FEDeviceVariables:Default PID Parameters:Kc",
                                                                     "FEDeviceVariables:Default PID Parameters:Ti",
                                                                     "FEDeviceVariables:Default PID Parameters:Td",
                                                                     "FEDeviceVariables:Default PID Parameters:Tds",
                                                                     "FEDeviceVariables:Default PID Parameters:SP High Limit",
                                                                     "FEDeviceVariables:Default PID Parameters:SP Low Limit",
                                                                     "FEDeviceVariables:Default PID Parameters:Out High Limit",
                                                                     "FEDeviceVariables:Default PID Parameters:Out Low Limit",
                                                                     "FEDeviceVariables:Default PID Parameters:Setpoint"])
        values['Parents'] += self.getRIndex(instance, "DigitalAlarm", "FEDeviceEnvironmentInputs:Input")
        values['Parents'] += self.getRIndex(instance, "AnalogAlarm", ["FEDeviceEnvironmentInputs:Input",
                                                                      "FEDeviceAlarm:Enable Condition",
                                                                      "FEDeviceManualRequests:HH Alarm",
                                                                      "FEDeviceManualRequests:H Warning",
                                                                      "FEDeviceManualRequests:L Warning",
                                                                      "FEDeviceManualRequests:LL Alarm"])
        values['Parents'] += self.getRIndex(instance, "AnalogAlarm,DigitalAlarm", "FEDeviceParameters:Alarm Delay (s)")

        values['Alias[,DeviceLinkList]'] += self.getDeviceLinksFromSpec(instance)
