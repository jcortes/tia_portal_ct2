# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for DigitalInput Objects.

import Generic_Template
reload(Generic_Template)


class DigitalInput_Template(Generic_Template.Generic_Type_Template):
    default_nature = "DI"

    def deviceFormat(self, instance=None):
        return ["deviceType", "deviceNumber", 'Alias[,DeviceLinkList]', 'Description - ElectricalDiagram', 'Diagnostics', 'WWWLink', 'Synoptic', 'Domain', 'Nature', 'WidgetType',
                'ArchiveMode', 'SMSCat', 'AlarmMessage', 'AlarmAck', 'NormalPosition', 'addr_StsReg01', 'addr_EvStsReg01', "addr_ManReg01", 'BooleanArchive',
                'AnalogArchive', 'EventArchive', 'MaskEvent', 'Parameters', 'Master', 'Parents', 'children', 'Type', 'SecondAlias']

    def processInstance(self, instance, params, values):
        values['ArchiveMode'] = self.getDigitalArchiveConfig(instance)
        values['SMSCat'] = self.getSMSCategory(instance)
        values['AlarmMessage'] = self.getAlarmMessage(instance, remove_commas=False)
        values['AlarmAck'] = self.getAlarmAckDigital(instance)
        values['NormalPosition'] = self.getDigitalIONormalPosition(instance)
        values['MaskEvent'] = self.getMaskEvent(instance)

        values['Parents'] += self.getRIndex(instance, "OnOff", ["FEDeviceEnvironmentInputs:Feedback On",
                                                                "FEDeviceEnvironmentInputs:Feedback Off",
                                                                "FEDeviceEnvironmentInputs:Local Drive",
                                                                "FEDeviceEnvironmentInputs:Local On",
                                                                "FEDeviceEnvironmentInputs:Local Off"])
        values['Parents'] += self.getRIndex(instance, "Analog", ["FEDeviceEnvironmentInputs:Feedback On",
                                                                 "FEDeviceEnvironmentInputs:Feedback Off",
                                                                 "FEDeviceEnvironmentInputs:Local Drive"])
        values['Parents'] += self.getRIndex(instance, "AnaDO", ["FEDeviceEnvironmentInputs:Feedback On",
                                                                "FEDeviceEnvironmentInputs:Local Drive",
                                                                "FEDeviceEnvironmentInputs:Local On",
                                                                "FEDeviceEnvironmentInputs:Local Off"])
        values['Parents'] += self.getRIndex(instance, "AnalogDigital", ["FEDeviceEnvironmentInputs:Feedback On",
                                                                        "FEDeviceEnvironmentInputs:Feedback Off",
                                                                        "FEDeviceEnvironmentInputs:Local Drive"])
        values['Parents'] += self.getRIndex(instance, "Local", ["FEDeviceEnvironmentInputs:Feedback On",
                                                                "FEDeviceEnvironmentInputs:Feedback Off"])
        values['Parents'] += self.getRIndex(instance, "DigitalAlarm", "FEDeviceEnvironmentInputs:Input")
        values['Parents'] += self.getRIndex(instance, "AnalogAlarm", "FEDeviceAlarm:Enable Condition")

        values['Type'] = self.getIOFEType(instance)

        values['Alias[,DeviceLinkList]'] += self.getDeviceLinksFromSpec(instance)
