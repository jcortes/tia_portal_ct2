# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for PCO Objects.

import Generic_Template
reload(Generic_Template)


class ProcessControlObject_Template(Generic_Template.Generic_Type_Template):
    default_nature = "PCO"

    def deviceFormat(self, instance=None):
        return ["deviceType", "deviceNumber", 'Alias[,DeviceLinkList]', 'Description', 'Diagnostics', 'WWWLink', 'Synoptic', 'Domain', 'Nature', 'WidgetType', 'reserved_empty',
                'NormalPosition', 'addr_StsReg01', "addr_StsReg02", 'addr_EvStsReg01', "addr_EvStsReg02", "addr_AuOpMoSt", "addr_OpMoSt", "addr_ManReg01",
                "addr_MOpMoR", "ModeName1", "ModeName2", "ModeName3", "ModeName4", "ModeName5", "ModeName6", "ModeName7", "ModeName8", "ModeAllowance1", "ModeAllowance2",
                "ModeAllowance3", "ModeAllowance4", "ModeAllowance5", "ModeAllowance6", "ModeAllowance7", "ModeAllowance8", 'BooleanArchive', 'AnalogArchive',
                'EventArchive', 'MaskEvent', 'Parameters', 'Master', 'Parents', 'children', 'Type', 'SecondAlias']

    def processInstance(self, instance, params, values):
        values['reserved_empty'] = ""
        values['NormalPosition'] = "0"

        values['ModeName1'] = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 1 Label")
        values['ModeName2'] = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 2 Label")
        values['ModeName3'] = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 3 Label")
        values['ModeName4'] = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 4 Label")
        values['ModeName5'] = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 5 Label")
        values['ModeName6'] = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 6 Label")
        values['ModeName7'] = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 7 Label")
        values['ModeName8'] = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 8 Label")
        values['ModeAllowance1'] = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 1 Allowance")
        values['ModeAllowance2'] = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 2 Allowance")
        values['ModeAllowance3'] = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 3 Allowance")
        values['ModeAllowance4'] = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 4 Allowance")
        values['ModeAllowance5'] = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 5 Allowance")
        values['ModeAllowance6'] = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 6 Allowance")
        values['ModeAllowance7'] = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 7 Allowance")
        values['ModeAllowance8'] = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 8 Allowance")
        values['MaskEvent'] = self.getMaskEvent(instance)

        values['Parameters'] += self.getParameterValue("PEnRstart", instance.getAttributeData("FEDeviceParameters:ParReg:Manual Restart after Full Stop").lower() == "false", "FALSE", "TRUE")
        values['Parameters'] += self.getParameterValue("PRstartFS", instance.getAttributeData("FEDeviceParameters:ParReg:Manual Restart after Full Stop").lower() == "true even if full stop still active", "TRUE", "FALSE")
        values['Parameters'] += self.getParameterValue("PCO_NAME", instance.getAttributeData("SCADADeviceGraphics:Display Name"), instance.getAttributeData("SCADADeviceGraphics:Display Name"))

        values['Master'] = self.getMaster(instance)

        values['children'] = self.getChildObjects(instance, "OnOff, Analog, AnaDO, AnalogDigital, Controller, MassFlowController, ProcessControlObject, AnalogAlarm, DigitalAlarm")

        values['Alias[,DeviceLinkList]'] += self.getDeviceLinksFromSpec(instance)
        values['Alias[,DeviceLinkList]'] += self.getObjectsFromAttributes(instance, [  # "LogicDeviceDefinitions:Master",
                                                                 "LogicDeviceDefinitions:External Master"])

        values['Type'] = ""
