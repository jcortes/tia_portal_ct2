# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
##
# This file contains shared functions related to UCPC Spec
##

from java.util import Vector
from java.util import ArrayList
from java.lang import System

# --------------------------------------------------------------------------------------------------


def convert2text(thePlugin, theUnicosProject):
    """
    This function converts the spec into text format as follows. For each object type:
       SHEET:XXX where XXX is the device type
       1 header row containing all column names, separated by |
       N data rows containing value for all columns, separated by |

    @param: thePlugin: the plugin - allows access to plugin functions
    @param: theUnicosProject: the spec

    This function returns:
    @return: outputText: the spec in text format

    """
    CRLF = System.getProperty("line.separator")
    allDevices = theUnicosProject.getAllDeviceTypes()
    outputText = ""

    for deviceType in allDevices:
        thePlugin.writeDebugInUABLog("Processing Device Type: " + deviceType.getDeviceTypeName())

        instances = deviceType.getAllDeviceTypeInstances()
        attributes = deviceType.getSpecificationAttributes()
        attribute_list = []
        for attribute in attributes:
            attribute_list.append(attribute.getSpecsPath())

        outputText = outputText + "SHEET:" + deviceType.getDeviceTypeName() + CRLF
        headerRow = " | ".join(attribute_list) + CRLF
        outputText = outputText + headerRow

        for instance in instances:
            data_list = []
            for attribute in attribute_list:
                data_list.append(instance.getAttributeData(attribute).strip())
            dataRow = " | ".join(data_list) + CRLF
            outputText = outputText + dataRow

    return outputText
# --------------------------------------------------------------------------------------------------
