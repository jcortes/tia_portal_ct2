# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.plugins.interfaces import APlugin
import os
from collections import OrderedDict
from research.ch.cern.unicos.utilities import DeviceTypeFactory


class SpecHelper(object):

    def __init__(self, plugin, logger):
        self.plugin = plugin
        self.logger = logger

    def get_attribute_value(self, instance, spec_field, default_value=""):
        """Function returns the value of the spec field of the instance.
        It returns default_value if cell is empty or value is a device reference.
        """
        value = instance.getAttributeData(spec_field)
        # TODO : remove "or self.plugin.isString(value)" from the if
        if value == "" or self.plugin.isString(value):
            if default_value != "":
                self.logger.write_warning(instance, "Undefined value for %s. Use default value %s" % (spec_field, default_value))
            return default_value
        if spec_field == "FEDeviceIOConfig:FE Encoding Type":
            return value
        else:
            return self.plugin.formatNumberPLC(value)

    @staticmethod
    def get_bit_from_attribute(instance, spec_field, off_value="", on_value=None):
        """ Function returns the binary value (0 or 1) as a comparison function between value in the {{spec_field}} of {{instance}}
        It's recommended to use off_value only for empty values ('' or '0.0')
        """
        value = instance.getAttributeData(spec_field).strip().lower()
        if on_value is not None:
            if isinstance(on_value, basestring):
                on_value = [on_value]
            on_value = [v.lower() for v in on_value]
            return '1' if value in on_value else '0'
        else:
            if isinstance(off_value, basestring):
                off_value = [off_value]
            off_value = [v.lower() for v in off_value]
            return '0' if value in off_value else '1'

    @staticmethod
    def attributes_in_family(device_type_attribute_families, target_family_name):
        for family in device_type_attribute_families:
            family_name = family.getAttributeFamilyName()
            if family_name == target_family_name:
                attribute_list = family.getAttribute()
                for attribute in attribute_list:
                    if attribute.getIsCommunicated():
                        yield attribute

    def is_large_application(self):
        is_large_application = self.plugin.getXMLConfig().getPLCParameter("GeneralConfiguration:LargeApplication", False)
        if is_large_application is None:
            return False
        else:
            return is_large_application.lower() == 'true'


class Logger(object):

    def __init__(self, plugin):
        self.plugin = plugin

    def write_warning(self, instance, message):
        name = instance.getAttributeData("DeviceIdentification:Name")
        type = instance.getDeviceTypeName()
        self.plugin.writeWarningInUABLog(type + " instance: " + name + ". " + message)


class TIAInst_Generic_Template(IUnicosTemplate):
    # TODO renamte thePlugin => plugin
    thePlugin = 0
    # TODO renamte theUnicosProject => spec
    theUnicosProject = 0
    # TODO renamte spec => spec_helper
    spec = 0
    logger = 0
    device_name = 0
    device_type_definition = 0
    CRLF = os.linesep

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython in %s." % self.__class__.__name__)
        self.logger = Logger(self.thePlugin)
        self.spec = SpecHelper(self.thePlugin, self.logger)
        self.device_name = self.__class__.__name__.replace("_Template", "")
        devite_type_definitions = DeviceTypeFactory.getInstance()
        try:
            self.device_type_definition = devite_type_definitions.getDeviceType(self.device_name)
        except:
            pass

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython in %s." % self.__class__.__name__)

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython in %s." % self.__class__.__name__)

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython in %s." % self.__class__.__name__)

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython in %s." % self.__class__.__name__)

    def writeDeviceInstances(self, output):
        xpathRepName = "/attributeFamily[attributeFamilyName='TargetDeviceInformation']/attribute[attributeName='Target' and defaultValue='Siemens']/attribute[attributeName='RepresentationName']/defaultValue"
        representationName = self.device_type_definition.getContext().getValue(xpathRepName)
        self.thePlugin.writeInstanceInfo(representationName + ".SCL", output)

    def writeFCDeviceInstances(self, output):
        xpathRepName = "/attributeFamily[attributeFamilyName='TargetDeviceInformation']/attribute[attributeName='Target' and defaultValue='Siemens']/attribute[attributeName='RepresentationName']/defaultValue"
        representationName = self.device_type_definition.getContext().getValue(xpathRepName)
        self.thePlugin.writeInstanceInfo("FC_" + representationName + ".SCL", output)

    def fill_communication_interface(self, device_type_attribute_families, params):
        for attribute in self.spec.attributes_in_family(device_type_attribute_families, "FEDeviceManualRequests"):
            attr_name = attribute.getAttributeName()
            attr_primitive_type = attribute.getPrimitiveType()
            attr_primitive_type_plc = self.thePlugin.getPLCequivalencePrimitiveType(attr_primitive_type)
            self.append_value_if_key_present(params, 'TYPE_ManRequest', '''      ''' + attr_name + ''' : ''' + attr_primitive_type_plc + ''';''')

        for attribute in self.spec.attributes_in_family(device_type_attribute_families, "FEDeviceOutputs"):
            attr_name = attribute.getAttributeName()
            is_event = attribute.getIsEventAttribute()
            attr_primitive_type = attribute.getPrimitiveType()
            attr_primitive_type_plc = self.thePlugin.getPLCequivalencePrimitiveType(attr_primitive_type)
            if attribute.getIsCommunicated():
                if is_event:
                    self.append_value_if_key_present(params, 'TYPE_bin_Status', '''      ''' + attr_name + ''' : ''' + str(attr_primitive_type_plc) + ''';''')
                    self.append_value_if_key_present(params, 'TYPE_event', "      ev" + attr_name + " : DWord;")
                elif is_event is None or is_event == 0:
                    self.append_value_if_key_present(params, 'TYPE_ana_Status', '''      ''' + attr_name + ''' : ''' + str(attr_primitive_type_plc) + ''';''')

    @staticmethod
    def append_value_if_key_present(params, key, value):
        if key in params:
            params[key].append(value)

    def process_template(self, scl_id, params):
        # in java it copies the files so __file__ does not work
        script_path = self.thePlugin.getPluginConfigPath("Templates:TemplatesFolder")
        folder = "TypeTemplates"
        if "Communication" in self.__class__.__name__ or "Compilation" in self.__class__.__name__  or "TSPP" in self.__class__.__name__:
            folder = "GlobalTemplates"
        script_path = os.path.join(script_path, folder, "TIAInst_" + self.__class__.__name__ + ".py")
        scl_path = os.path.dirname(script_path) + "\\" + scl_id
        output = open(scl_path, 'rb').read()
        for k in params.keys():
            value = params[k]
            if isinstance(value, list):
                value = self.CRLF.join(value)
            value = str(value)
            output = output.replace(chr(36) + k + chr(36), value)
        output = self.CRLF.join([line for line in output.splitlines()])
        return output

    @staticmethod
    def get_block_number(block_number):
        return "" if block_number == 0 else str(block_number + 1)

    @classmethod
    def get_io_error_and_simu(cls, unicos_project, name, linked_objects, exclude_itself=False):
        feedback_list = []
        for feedback in linked_objects:
            if feedback:
                # include linked objects to instance' IOError/ IOSimu but not for AnalogStatus/WordStatus
                # handle preceeding NOT
                if feedback.lower().startswith("not "):
                    feedback = feedback[4:]
                type = unicos_project.findInstanceByName(feedback).getDeviceType().getDeviceTypeName()
                if type.lower() not in ["wordstatus", "analogstatus"]:
                    feedback_list.append(feedback)

        if len(feedback_list) > 0:
            io_error = ['''%s.IOErrorW''' % feedback for feedback in feedback_list]
            if not exclude_itself:
                io_error.append('''%s.IOError''' % name)  # include device itself into result
            io_error = '''"%s".IOError := ''' % name + cls.CRLF + '''    OR '''.join(io_error) + ''';'''
            io_error += cls.CRLF + "   "
        else:
            io_error = '''// Nothing to add to the IOError\r\n    '''
        if len(feedback_list) > 0:
            io_simu = ['''%s.IOSimuW OR %s.FoMoSt''' % (feedback, feedback) for feedback in feedback_list]
            if not exclude_itself:
                io_simu.append('''%s.IOSimu''' % name)  # include device itself into result
            io_simu = '''"%s".IOSimu := ''' % name + '''\r\n	OR '''.join(io_simu) + ''';'''
            io_simu += "\r\n	"
        else:
            io_simu = '''// Nothing to add to the IOSimu\r\n	'''

        return io_error, io_simu

    @staticmethod
    def get_types_to_process(plugin, unicos_project, xml_config, generate_global_files):
        """
        Function returns the dictionary of {device_type: amount_of_instances}
        Types that have no instances or excluded from generation would not appear in the map
        :param unicos_project:
        :param xml_config:
        :param generate_global_files: Boolean(java) class instance passed from the generator plugin
        :return: {device_type: amount_of_instances} dictionary
        """
        generate_global_files = generate_global_files.booleanValue()
        types_to_process = OrderedDict()
        plugin_id = plugin.getId()

        for device_type in unicos_project.getAllDeviceTypes():
            device_type_name = device_type.getDeviceTypeName()
            process_enabled = xml_config.getTechnicalParametersMap(plugin_id + ":UNICOSTypesToProcess").get(device_type_name)
            instance_amount = unicos_project.getDeviceType(device_type_name).getAllDeviceTypeInstances().size()
            if instance_amount > 0:
                if process_enabled == "true" or generate_global_files:
                    types_to_process[device_type] = instance_amount
        return types_to_process

    def get_recipe_buffer_size(self, xml_config):
        buffer_size = xml_config.getPLCParameter("RecipeParameters:BufferSize").strip()
        buffer_size_calculated = int(buffer_size)
        if buffer_size_calculated % 120 != 0:
            buffer_size_calculated = (((buffer_size_calculated - 1) / 120) + 1) * 120
            if buffer_size_calculated > 1000:
                buffer_size_calculated = 1000

        if buffer_size_calculated > 1000:
            self.thePlugin.writeErrorInUABLog("The maximun buffer size allowed is 1000. BufferSize = 1000 has been taken")
            buffer_size_calculated = 1000

        if buffer_size_calculated < 1:
            self.thePlugin.writeErrorInUABLog("The buffer size must be a positive integer. BufferSize = 120 has been taken")
            buffer_size_calculated = 120
        return buffer_size_calculated

    @staticmethod
    def get_instance_pos(unicos_project, instance, air_limit_size):
        """ Function return the position in the slice for given instance.
        If instance has 'Feedback Analog' column empty it returns 1
        If instance has 'Feedback Analog' linked not to AIR it returns 1
        If instance has 'Feedback Analog' linked to AIR_FB1 it returns 1
        If instance has 'Feedback Analog' linked to AIR_FB2 it returns 2
        """
        feedback_analog = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Analog")
        if feedback_analog == "":
            return 1
        else:
            linked_objects = unicos_project.findMatchingInstances("AnalogInputReal", "'#DeviceIdentification:Name#'='%s'" % feedback_analog)
            if linked_objects.size() == 0:
                return 1
            else:
                air_number = linked_objects.get(0).getInstanceNumber()
                air_block_number = (air_number - 1) / air_limit_size
                return air_block_number + 1
