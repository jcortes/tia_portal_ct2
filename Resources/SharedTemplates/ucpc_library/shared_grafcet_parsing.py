# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
import re
import textwrap
from research.ch.cern.unicos.plugins.interfaces import APlugin

import ucpc_library.shared_decorator
# reload(ucpc_library.shared_decorator)
import ucpc_library.shared_usage_finder
# reload(ucpc_library.shared_usage_finder)


class SharedGrafcetParsing ():
    '''
    This class provides functionality of parsing Siemens/Schneider grafcets
    '''
    def __init__(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.Decorator = ucpc_library.shared_decorator.ExpressionDecorator()
        self.UsageFinder = ucpc_library.shared_usage_finder.Finder()
        self.root = 0

    def find(self, f, seq):
        '''Return first item in sequence where f(item) == True.'''
        for item in seq:
            if f(item):
                return item
        return None

    def exportGrafcetToXml(self, inputDotFile, PCOname, TLfile, grafcetDB, grafcetSteps, grafcetTransitions, PVSSSystemName="dist_101:", headerText="", PVSSAddTransitionLabels=True, panelW=1270, panelH=830, marginX=25, marginTop=50, marginBottom=50, GLfile=""):
        ''' This function generates pvss stepper in .xml format
        name should be probably changed to generic: exportGrafcetToXml

        @param inputDotFile         :  input file in .dot format with positions
        @param pcoName              :  PCO name
        @param TLfile               :  transition logic file
        @param grafcetDB            :  name of the grafcetDB
        @param grafcetSteps         :  list of steps
        @param grafcetTransitions   :  list of transitions
        @param PVSSSystemName       :  PVSSSystemName
        @param headerText           :  text for a header of the panel
        @param PVSSAddTransitionLabels    :   flag to mark if transition labels should be included
        @param panelW               :  width of the panel
        @param panelH               :  height of the panel
        @param marginX              :  left/right margin
        @param marginY              :  top/bottom margin
        '''

        # load simple dot and parse
        lines = [line.split() for line in inputDotFile.readlines()]

        # extract information about graph, nodes and edges
        graph = [line for line in lines if line[0] == "graph"][0]
        edges = [line for line in lines if line[0] == "edge"]
        nodes = [line for line in lines if line[0] == "node"]

        graphWidth = graph[2]
        graphHeight = graph[3]

        if float(graphWidth) == 0 or float(graphHeight) == 0:
            self.thePlugin.writeWarningInUABLog("Graph for %s has 0 width or height. Skipping..." % PCOname)
            return ""

        # extract data about edges, nodes and jumps
        edges = [[edge[4],    # startX
                  edge[5],    # startY
                  edge[2 + 2 * int(edge[3])],   # endX
                  edge[3 + 2 * int(edge[3])],   # endY
                  edge[4 + 2 * int(edge[3])],   # transition
                  edge[2].startswith("to")      # info if the edge end is a jump
                  ] for edge in edges]

        steps = [[node[1],  # name
                  node[2],  # x
                  node[3],  # y
                  node[6]   # label
                  ] for node in nodes if "<b>" not in node[6]]

        jumps = [[node[1],  # name
                  node[2],  # x
                  node[3],  # y
                  node[6]   # label
                  ] for node in nodes if "<b>" in node[6]]

        canvasW = panelW - 2 * marginX    # size of the area with stepper
        canvasH = panelH - (marginTop + marginBottom)

        scaleX = float(canvasW) / float(graphWidth)       # scaling factors
        scaleY = float(canvasH) / float(graphHeight)

        outputXml = ""
        # write header of the Xml
        outputXml += self.addPVSSXmlHeader(panelW, panelH)

        # common counter for objects in the panel
        uniqueIdCounter = 0
        outputXml += self.addPVSSHeaderBar(uniqueIdCounter, headerText)
        uniqueIdCounter += 1
        outputXml += self.addPVSSPCO(uniqueIdCounter, 0, 20, PVSSSystemName + self.getExpertNameFromName(PCOname))
        uniqueIdCounter += 1

        # draw edges
        for (e_startX, e_startY, e_endX, e_endY, e_transition, e_jump) in edges:
            startX = scaleX * float(e_startX)
            startY = panelH - scaleY * float(e_startY)

            endX = scaleX * float(e_endX)
            endY = panelH - scaleY * float(e_endY)

            # make vertical line longer!
            if e_jump:
                endY = endY + 50

            midY = startY + 25  # the distance to bend connection vertically

            startX = self.align(marginX + startX)
            startY = self.align(-marginBottom + startY)
            endX = self.align(marginX + endX)
            endY = self.align(-marginBottom + endY)
            midY = self.align(-marginBottom + midY)

            # position in the middle of lowest segment of the connector
            midmidY = (midY + endY) / 2

            if startX != endX:
                outputXml += self.addPVSSLine(uniqueIdCounter, startX, startY, startX, midY)
                uniqueIdCounter += 1
                outputXml += self.addPVSSLine(uniqueIdCounter, startX, midY, endX, midY)
                uniqueIdCounter += 1
                outputXml += self.addPVSSLine(uniqueIdCounter, endX, midY, endX, endY)
                uniqueIdCounter += 1
            else:   # if there is no bending of connector - draw straight line
                outputXml += self.addPVSSLine(uniqueIdCounter, startX, startY, endX, endY)
                uniqueIdCounter += 1

            # find the condition for transition variable
            transitionVar = e_transition
            # in the case of (, ), space and " in the Transition, replace, and look for ...
            for character in ["(", ")", " ", "\""]:
                transitionVar = transitionVar.replace(character, "")

            # find Word Status name and bit numbers connected to the transition
            wordStatus = ""
            bitNo = ""
            if self.UsageFinder.isSiemens():

                pattern = re.compile(r"^(.+?)_bit\[(.+?)\]\s*:=\s*.+?\." + transitionVar + "\s*;", re.IGNORECASE | re.MULTILINE)
                result = re.search(pattern, TLfile)
                varName = ""
                bitNo = 0
                if not result:
                    raise Exception("Search for expression varName_bit[bitNo] := " + transitionVar + "; in _TL.scl file failed.")
                else:
                    varName = result.group(1)
                    bitNo = (int(result.group(2)) + 8) % 16  # siemens calculation bits 0-7 are 8-15 and 8-15 are 0-7,

                pattern = re.compile(r"^DB_WS_ALL\.WS_SET\.(.+?)\.AuPosR\s*:=\s*" + varName + ";", re.IGNORECASE | re.MULTILINE)
                result = re.search(pattern, TLfile)
                wordStatus = ""
                if not result:
                    raise Exception("Search for expression DB_WS_ALL.WS_SET.(name of status).AuPosR := (" + varName + "); in _TL.scl file failed.")
                else:
                    wordStatus = result.group(1)

            elif self.UsageFinder.isSchneider():
                pattern = re.compile(r"^([^\n]+?)\s*:=\s*BIT_TO_WORD\s*\(\s*(.*?" + transitionVar + ".*?)\);\s*", re.IGNORECASE | re.MULTILINE | re.DOTALL)
                result = re.search(pattern, TLfile)
                if not result:
                    raise Exception("Search for word status for the transition " + transitionVar + " in TL section failed.")
                else:
                    wordStatus = result.group(1).rsplit('_', 1)[0]  # remove _AuPosR from the end
                    bits = result.group(2)

                    pattern = re.compile(r"BIT([0-9]+)\s*:=\s*" + transitionVar + "\s*", re.IGNORECASE | re.MULTILINE | re.DOTALL)
                    result = re.search(pattern, bits)
                    if not result:
                        raise Exception("Search for bit number of the transition " + transitionVar + " in TL section failed.")
                    else:
                        bitNo = result.group(1)
            if self.UsageFinder.isTia():
                self.thePlugin.writeWarningInUABLog("exportGrafcetToXml function: no bit number search implemented for TIA portal")

            outputXml += self.addPVSSTransition(uniqueIdCounter, endX - 20, midmidY - 24, bitNo, PVSSSystemName + self.getExpertNameFromName(wordStatus))
            uniqueIdCounter += 1

            tansitionLabel = ""
            if PVSSAddTransitionLabels:
                condition = self.find(lambda transition: transition[2].lower().strip() == transitionVar.lower().strip(), grafcetTransitions)
                if condition:
                    condition = "\n".join(textwrap.wrap(condition[3], width=30))
                else:
                    condition = "NotFound"
                    self.thePlugin.writeWarningInUABLog("No condition found for transition: " + transitionVar + " in stepper " + grafcetDB)
                tansitionLabel = condition
            else:
                tansitionLabel = transitionVar

            outputXml += self.addPVSSText(uniqueIdCounter, endX + 12, midmidY - 12, tansitionLabel)
            uniqueIdCounter += 1
            if e_jump:
                outputXml += self.addPVSSArrow(uniqueIdCounter, endY - 5, endX - 16)
                uniqueIdCounter += 1

        sIdentifier = ""
        if self.UsageFinder.isSiemens():

            pattern = re.compile(r"^DB_WS_ALL\.WS_SET\.(.+?)\.AuPosR\s*:=\s*INT_TO_WORD\s*\(%s\.S_NO\);" % grafcetDB, re.IGNORECASE | re.MULTILINE)
            result = re.search(pattern, TLfile)
            sIdentifier = ""
            if not result:
                raise Exception("Search for expression DB_WS_ALL.WS_SET.Status.PosSt := INT_TO_WORD(%s.S_NO) in _TL.scl file failed." % grafcetDB)
            else:
                sIdentifier = result.group(1)

        elif self.UsageFinder.isSchneider():
            for step in grafcetSteps:
                stepName = step[0]
                pattern = re.compile(stepName + r"\.x\s* THEN\n\s*(.+?)\s*:=\s*(.+?);", re.IGNORECASE | re.MULTILINE)
                result = re.search(pattern, GLfile)
                if not result:
                    raise Exception("Could not find the step number and Word status in the " + PCOname + " GL section for " + stepName)
                else:
                    sIdentifier = result.group(1).rsplit('_', 1)[0]  # remove _AuPosR from the end
                    stepNumber = result.group(2)
                    # update the step number
                    grafcetSteps = [(k, v) if (k != stepName) else (k, stepNumber) for (k, v) in grafcetSteps]
        if self.UsageFinder.isTia():
                self.thePlugin.writeWarningInUABLog("exportGrafcetToXml function: no step number and Word status search implemented for TIA portal")
        # add label with STATUS
        outputXml += self.addPVSSStatus(uniqueIdCounter, 20, 100, PVSSSystemName + self.getExpertNameFromName(sIdentifier))
        uniqueIdCounter += 1
        # put nodes
        for step in steps:
            posX = self.align(marginX + scaleX * float(step[1]))
            posY = self.align(-marginBottom + panelH - scaleY * float(step[2]))
            label = step[0]

            stepData = self.find(lambda step: step[0].lower() == label.lower(), grafcetSteps)
            iValue = stepData[1]

            outputXml += self.addPVSSStep(uniqueIdCounter, posX - 43, posY - 29, iValue, PVSSSystemName + self.getExpertNameFromName(sIdentifier), "FALSE")
            uniqueIdCounter += 1
            
        # put jumps
        for jump in jumps:
            posX = self.align(marginX + scaleX * float(jump[1]))
            posY = self.align(-marginBottom + panelH - scaleY * float(jump[2]))
            label = jump[0][2:jump[0].rfind("_")]

            outputXml += self.addPVSSText(uniqueIdCounter, posX, posY + 50, label)
            uniqueIdCounter += 1

        # write footer
        outputXml += self.addPVSSFooter()
        return outputXml

    def exportGrafcetToGraphviz(self, grafcetSteps, grafcetTransitions, graphName="Grafcet"):
        ''' This function exports grafcet to Graphviz format
        The argument are:
        @param grafcetSteps         :   list of tuples (stepName, stepNumber)
        @param grafcetTransitions   :   list of fives (fromStep, toStep, label, condition, jump, priority)
        @param graphName            :   name of the graph
        '''

        outputFile = '''digraph %s {
    rankdir=TD;
    rank=source;
    #splines=ortho;
    #splines=polyline;
    ordering=out
    node [shape=Mrecord, style=bold, filling="white"];
    edge [fontsize=8, fontcolor="dimgray"]; 
	//edge [headport=n, tailport=s];
    
    ''' % graphName
        # split transitions into two categories
        jumps = [transition for transition in grafcetTransitions if transition[4]]
        notJumpsTmp = [transition for transition in grafcetTransitions if not transition[4]]

        notJumps = []
        for (fromPosition, toPosition, conditionLabel, condition, jump, priority) in notJumpsTmp:
            if self.find(lambda t: t[1] == toPosition, notJumps):
                jumps.append((fromPosition, toPosition, conditionLabel, condition, jump, priority))
            else:
                notJumps.append((fromPosition, toPosition, conditionLabel, condition, jump, priority))

        rank = {}   # pairs node:rank

        # transitions for processing
        tree = [(fromPosition, toPosition) for (fromPosition, toPosition, conditionLabel, conditionLogic, jump, priority) in grafcetTransitions if not jump]

        # calculate rank of each node (length of the longest path starting in the node)
        while tree:
            # find nodes that do not have outgoing connections
            leaves = [toPosition for (fromPosition, toPosition) in tree if not self.find(lambda transition: transition[0] == toPosition, tree)]

            # calculate rank of each leaf
            for leaf in leaves:
                # add each leaf to rank with max rank of it's child + 1
                leafChilds = [toPosition for (fromPosition, toPosition, conditionLabel, condition, jump, priority) in notJumps if fromPosition == leaf]

                if leafChilds:
                    rank[leaf] = rank[max(leafChilds, key=lambda node:rank[node])] + 1
                else:
                    rank[leaf] = 0

            # cut the tree: remove all the connections leading to leaves
            tree = [(fromPosition, toPosition) for (fromPosition, toPosition) in tree if toPosition not in leaves]

        # collect nodes in paths into groups so they can be displayed as columns
        groups = {}         # dictionary
        groupCounter = 0

        tree = notJumps
        while tree:
            groupCounter += 1

            # first 'nextStep' is the initial step - step that has outgoing transitions, but not incoming
            nextStep = self.find(lambda t1: not self.find(lambda t2: t2[1] == t1[0], tree), tree)[0]
            groups[nextStep] = groupCounter

            while nextStep:
                # find child nodes of the step
                childNodes = [toPosition for (fromPosition, toPosition, conditionLabel, condition, jump, priority) in tree if fromPosition == nextStep]
                if not childNodes:  # if nothing found, finish the path
                    nextStep = None
                else:
                    nextStep = max(childNodes, key=lambda node: rank[node])  # next step is the child with highest rank
                    groups[nextStep] = groupCounter

            tree = [transition for transition in tree if transition[0] not in groups]

        nodes = [stepName for (stepName, stepNumber) in grafcetSteps if stepName not in groups]
        for node in nodes:
            groupCounter += 1
            groups[node] = groupCounter

        # print steps
        for (stepName, stepNumber) in grafcetSteps:
            outputFile += stepName
            if stepName in groups:
                outputFile += ''' [group=col%s];''' % groups[stepName]
            outputFile += '''\n'''

        # add invisible nodes to reloacte leaves to different levels (makes grafcet a bit more compact)
        #outputFile += '''\n\n// Invisible connections'''

        #leaves = [node for node in rank if rank[node] == 0]
        # group the leaves by parents
        #parentsOfleaves = {}
        # for leaf in leaves:
        #    parent = self.find(lambda (fromStep, toStep, c): toStep == leaf , notJumps)[0]
        #    if parent not in parentsOfleaves:
        #        parentsOfleaves[parent] = list()
        #    parentsOfleaves[parent].append(leaf)

        # for parent in parentsOfleaves:
        #    if len(parentsOfleaves[parent]) > 1:
        #        firstLeaf = parentsOfleaves[parent].pop(0)
        #        for leaf in parentsOfleaves[parent]:
        #            outputFile += '''
        #//$firstLeaf$ -> $leaf$ [style=invis];'''
        # -----------------------------------------------------------------------------------------

        # print all the connections that are not jumps
        #outputFile += '''\n\n// Not Jumps'''
        # sort by "fromStep" name and priritize transitions belonging to the columns
        #notJumps = sorted(notJumps, key = lambda x: (x[0], not groups[x[0]]==groups[x[1]]))

        counter = 0

        for (stepName, stepNumber) in grafcetSteps:
            # get list of transitions going out of this node
            outGoing = [transition for transition in jumps if transition[0] == stepName]
            outGoing.extend([transition for transition in notJumps if transition[0] == stepName])
            outGoing.sort(key=lambda x: x[5], reverse=True)
            for (fromPosition, toPosition, conditionLabel, condition, jump, priority) in outGoing:
                if jump:   # jump
                    outputFile += '''
    to''' + str(toPosition) + '''_''' + str(counter) + ''' [label=<<b>''' + str(toPosition) + '''</b>>, shape="plaintext", style="bold", tailport = "s"];
    ''' + str(fromPosition) + ''' -> to''' + str(toPosition) + '''_''' + str(counter) + ''' [label="''' + str(conditionLabel.strip()) + '''"];
    '''
                    counter += 1
                else:  # not jump
                    outputFile += '''
    ''' + str(fromPosition) + ''' -> ''' + str(toPosition) + ''' [label="''' + str(conditionLabel.strip()) + '''" , minlen=3];'''

        outputFile += '''}'''
        return outputFile

    def parseGrafcet(self, unitName, TLSource="", grafcetSource=""):
        ''' this function parses grafcet:
            in Schneider based on unitName
            in Siemens based on TL file and Grafcet file

        The arguments are:
        @param unitName:        name of the grafcet/unit
        @param TLSource:        source file with corresponding transition logic (as string)
        @param grafcetSource:   source file with grafcet (as string)

        This function returns grafcet as:
        pair:
            list of tuples: (stepName, stepNumber)
            list of six:    (fromStep, toStep, conditionLabel, conditionLogic, isJump, priority)

        would be good to refactor and prepare classes for STEP and TRANSITION
        '''

        if self.UsageFinder.isSiemens():
            grafcetSteps, grafcetTransitions = self.parseS7Grafcet(unitName, TLSource, grafcetSource)
        elif self.UsageFinder.isSchneider():
            grafcetSteps, grafcetTransitions = self.parseSchneiderGrafcet(unitName)
        else:
            self.thePlugin.writeWarningInUABLog("parseGrafcet not developed for this platform yet. Please contact icecontrols.support@cern.ch for more info.")
            return ([], [])

        # sort grafcetTransitions according to the "right" order in grafcetSteps
        # additionally, in scope of the same fromStep sort the transitions alphabetically basing on name of toStep
        grafcetTransitionsSorted = []
        for step in grafcetSteps:
            tmp = [(a.lower(), b.lower(), c, d, e, f) for (a, b, c, d, e, f) in grafcetTransitions if a.lower() == step[0].lower()]  # [0] because need to take only step from tuple: (step, number)
            if tmp:
                tmp.sort(key=lambda x: x[1])         # this sorts each sub-list alphabetically taking names of second argument - toStep
            grafcetTransitionsSorted.extend(tmp)

        return (grafcetSteps, grafcetTransitionsSorted)

    def parseS7Grafcet(self, unitName, TLsourceFile, grafcetSource):
        ''' this function reads S7 grafcet file and retrievs list of steps and transitions

        The arguments are:
        @param unitName     : not used, to be consistent with schneider
        @param TLsourceFile : transition logic file as string
        @param grafcetSource: grafcet file as string

        This function returns:
        pair:
            list of tuples: (stepName, stepNumber)
            list of six:    (fromStep, toStep, conditionLabel, conditionLogic, isJump, priority)
        '''

        pattern = re.compile(r"^(?:(?:INITIAL_)?STEP\s+)(.+?)\s+.+?_NUM\s+(.+?)\*", re.IGNORECASE | re.MULTILINE)          # this pattern retrieves steps from grafcet file
        steps = re.findall(pattern, grafcetSource.lower())

        # pattern     = re.compile(r"(?:^\s*FROM\s+)(.+?)(?:\s+TO\s+)(.+?)\s*(.*?)\s*CONDITION\s*:=\s*(.+?)\n", re.IGNORECASE|re.MULTILINE)    # this pattern matches pairs of steps: transitions from grafcet file
        pattern = re.compile(r"(?:^\s*FROM\s+)(.+?)(?:\s+TO\s+)(.+?)\s+(.*?)\s*CONDITION\s*:=\s*(.+?)\n", re.IGNORECASE | re.MULTILINE)    # this pattern matches pairs of steps: transitions from grafcet file + jumps
        transitions = re.findall(pattern, grafcetSource)

        outputTransitions = []
        # in Siemens priority of the transitions depends on the location in file
        priority = 0
        for transition in transitions:
            jump = False
            if "jump" in transition[2].lower():
                jump = True
            outputTransitions.append((transition[0], transition[1], transition[3], self.Decorator.unDecorateExpression(self.getS7TransitionCondition(TLsourceFile, transition[3])), jump, priority))
            priority += 1

        return (steps, outputTransitions)

    def parseSchneiderGrafcet(self, unitName):
        ''' this function reads schneider logic file and retrievs list of steps and pairs of transitions

            before running this function, user has to init Schneider project root using initTree_schneider function

        The arguments are:
        @param unitName: name of the stepper unit

        This function returns:
        @return pair:
            list of tuples:  (stepName, stepNumber)
            list of six:    (fromStep, toStep, conditionLabel, conditionLogic, isJump, priority)
        '''
        # if sources have not been loaded
        if self.root == 0:
            return [], []
        
        # Find requested grafcet TL and GL in logic files
        tl_logic = self.get_program_content(unitName + "_TL", programs=self.root.findall('program'))
        gl_logic = self.get_program_content(unitName + "_GL", programs=self.root.findall('program'))
        
        programsList = self.root.findall('SFCProgram')   # find all the program units containing steppers
        for program in programsList:
            if program.find('identProgram').attrib.get("name") == unitName + "_SL":  # take only the one with the requested grafcet
                # get all the xml info
                rawSteps = program.findall('chartSource/networkSFC/step')                # steps in form of xml nodes
                rawTransitions = program.findall('chartSource/networkSFC/transition')    # transitions
                rawJumps = program.findall('chartSource/networkSFC/jumpSFC')             # jumps
                rawAltBranches = program.findall('chartSource/networkSFC/altBranch')     # alternative branches
                rawAltJoints = program.findall('chartSource/networkSFC/altJoint')        # alternative joints
                rawLinksSFC = program.findall('chartSource/networkSFC/linkSFC')          # links

                # links: fromX, fromY, toX, toY
                linksSFC = [(int(link.find('./directedLinkSource/objPosition').attrib.get("posX")), int(link.find('./directedLinkSource/objPosition').attrib.get("posY")), int(link.find('./directedLinkDestination/objPosition').attrib.get("posX")), int(link.find('./directedLinkDestination/objPosition').attrib.get("posY"))) for link in rawLinksSFC]

                # list of steps: X, Y, NAME
                steps = [(int(step.find('./objPosition').attrib.get("posX")), int(step.find('./objPosition').attrib.get("posY")), step.attrib.get("stepName")) for step in rawSteps]

                # list of jumps: X, Y, NAME
                jumps = [(int(jump.find('./objPosition').attrib.get("posX")), int(jump.find('./objPosition').attrib.get("posY")), jump.attrib.get("stepName")) for jump in rawJumps]

                # alternative branches: X, Y, WIDTH, FROM_STEP
                altBranches = []
                for branch in rawAltBranches:
                    posX = int(branch.find('./objPosition').attrib.get("posX"))
                    posY = int(branch.find('./objPosition').attrib.get("posY"))
                    width = int(branch.attrib.get("width"))

                    # find the node right above alternative branch (there can be only one)
                    sourceStep = self.find(lambda step: posX <= step[0] < posX + width and step[1] == posY - 1, steps)
                    if not sourceStep: # if no step right above alt branch, it could be a SFC link
                        sourceLink = self.find(lambda link: posX <= link[2] < posX + width and link[3] == posY, linksSFC)
                        if sourceLink:
                            sourceStep = self.find(lambda step: step[0] == sourceLink[0] and step[1] == sourceLink[1], steps)

                    if sourceStep:
                        altBranches.append((posX, posY, width, sourceStep[2]))
                    else:
                        self.thePlugin.writeWarningInUABLog("Grafcet " + str(unitName) + " parsing: No source step")

                # alternative joints: X, Y, WIDTH, TO_STEP
                altJoints = []
                for joint in rawAltJoints:
                    posX = int(joint.find('./objPosition').attrib.get("posX"))
                    posY = int(joint.find('./objPosition').attrib.get("posY"))
                    width = int(joint.attrib.get("width"))

                    # find the node right below alternative joint (there can be only one)
                    destinationStep = self.find(lambda step: posX <= step[0] < posX + width and step[1] == posY + 1, steps)

                    if destinationStep:
                        altJoints.append((posX, posY, width, destinationStep[2]))
                    else:
                        self.thePlugin.writeWarningInUABLog("Grafcet " + str(unitName) + " parsing: No destination step")

                # links: fromX, fromY, toX, toY
                linksSFC = [(int(link.find('./directedLinkSource/objPosition').attrib.get("posX")), int(link.find('./directedLinkSource/objPosition').attrib.get("posY")), int(link.find('./directedLinkDestination/objPosition').attrib.get("posX")), int(link.find('./directedLinkDestination/objPosition').attrib.get("posY"))) for link in rawLinksSFC]

                # transitions
                transitions = []
                for transition in rawTransitions:
                    variable = transition.find('./transitionCondition/variableName').text
                    posX = int(transition.find('./objPosition').attrib.get("posX"))
                    posY = int(transition.find('./objPosition').attrib.get("posY"))
                    fromStep = ""
                    toStep = ""

                    # check if there is any alternative branch above this transition
                    altBranch = self.find(lambda branch: posX >= branch[0] and posX < branch[0] + branch[2] and posY == branch[1], altBranches)
                    if altBranch:
                        fromStep = altBranch[3]
                    else:
                        # if there is none, there must be the step instead
                        sourceStep = self.find(lambda step: posX == step[0] and posY == step[1] + 1, steps)
                        if sourceStep:
                            fromStep = sourceStep[2]

                    # coordinates of the end of transition
                    destX = posX
                    destY = posY

                    # if there is link with this transition as start, remember the target of transition
                    link = self.find(lambda l: l[0] == posX and l[1] == posY, linksSFC)
                    if link:
                        destX = link[2]
                        destY = link[3]

                    # retrive toStep
                    jump = False
                    destinationStep = self.find(lambda step: step[0] == destX and (step[1] == destY + 1 or step[1] == destY), steps + jumps)
                    if destinationStep:
                        toStep = destinationStep[2]
                    else:
                        destinationJoint = self.find(lambda altJoint: destY <= altJoint[1] and altJoint[0] <= destX < altJoint[0] + altJoint[2], altJoints)
                        if destinationJoint:
                            toStep = destinationJoint[3]

                    if destinationStep in jumps and destinationStep not in steps:
                        jump = True

                    transitions.append((variable, posX, posY, fromStep, toStep, jump))    # add here info about jumps

                    # debug
                    if fromStep == "" or toStep == "":
                        self.thePlugin.writeWarningInUABLog("Grafcet " + str(unitName) + " parsing: Missing step in transition! transition= " + ",".join([variable, str(posX), str(posY), fromStep, toStep, str(jump)]))
                
                steps_output = self.get_schneider_steps_output(
                    steps=[step[2].lower() for step in steps],
                    tl_content=tl_logic,
                    gl_content=gl_logic
                )
                # get the condition from logic additionally
                transitions_output = []
                priority = 0
                for transition in transitions:
                    transition_condition = self.getSchneiderTransitionCondition(tl_logic, transition[0])
                    transitions_output.append((transition[3], transition[4], transition[0], self.Decorator.unDecorateExpression(transition_condition), transition[5], priority))
                    priority += 1

                return steps_output, transitions_output
        # this is executed if no grafcet was found
        self.thePlugin.writeWarningInUABLog("There is no stepper " + str(unitName) + " in the project.")
        return [], []

    @staticmethod
    def get_program_content(name, programs):
        """ Finds program in given list of programs and returns its content as a string
        @param name: string
            Name of the program
        @param programs:
            List of programs
        @return str
            Program content or empty string in case program name is not found
        """
        for program in programs:
            if program.find('identProgram').attrib.get("name") == name:
                return program.find('STSource').text
        
        # If here, program was not found, so return empty string
        return ''
    
    def getS7TransitionCondition(self, tlFile, transition):
        ''' this function analyses the transition condition from grafcet file and repalces it with condition existing in logic
            transition might be whole expression, split and analyse independently each part

        The arguments are:
        @param tlFile       : transition logic file as string
        @param transition   : transition condition from grafcet file

        This function returns:
        @return condition extracted from project logic
        '''

        for character in ["(", ")", "+", "-", "*", "/", "<", ">"]:
            transition = transition.replace(character, " " + character + " ")
        transition = transition.split()  # turn string into list
        condition = ""  # this will be the output
        for part in transition:  # go through the list
            if part.lower() not in ["(", ")", "+", "-", "*", "/", "<", ">", "and", "or", "not"]:            # if the part is not a condition
                pattern = re.compile(r"\.%s\s*:=\s*(.+?);" % part, re.IGNORECASE | re.MULTILINE | re.DOTALL)     # find condition in the code
                m = re.findall(pattern, tlFile)
                if m:
                    condition = condition + " " + " ".join(re.findall(pattern, tlFile))
                else:  # if doesn't match, append original part
                    condition = condition + " " + part
            else:
                condition = condition + " " + part
        condition = condition.replace("<", "&lt;").replace(">", "&gt;")             # write safely into xml
        return condition
        
    def get_schneider_steps_output(self, steps, tl_content, gl_content):
        """ Returns list of steps and its corresponding number
        @param steps: list
            List of steps
        @param tl_content: str
            Content from TL logic file
        @param gl_content: str
            Content from GL logic file
        @return list of tuples
            List of steps and its corresponding numbers: [(step_name, step_number), ...]
        """
        # Look for numbers in the TL file
        numbers = self.get_schneider_step_numbers(set(steps), tl_content)
        # In case it was not found, look in the GL file
        if not numbers:
            numbers = self.get_schneider_step_numbers(set(steps), gl_content)
        
        return [(step, numbers.get(step, '')) for step in steps]
    
    @staticmethod
    def get_schneider_step_numbers(steps, file_content):
        """ Looks for steps numbers in Schneider logic file
        @param steps: set
            Set of steps
        @param file_content: string
            File content, as a string
        @return: dict
            Number of each given step.
            Numbers are formatted as strings
            In the following situations the step number is not included in the output:
                - Number is not found in the file content
                - Number is found more than once
                - Regex pattern finds something that is not a number
        """
        # Initialize output as empty dictionary
        numbers = {}
        for step in steps:
            # Get regex pattern
            pattern_str = r"IF\s*%s.X\s*THEN\s*\n.*_AuPosR\s+:=\s+(.*);" % step
            pattern = re.compile(pattern_str, re.MULTILINE | re.IGNORECASE)
            # Look for it in file content
            res = re.findall(pattern, file_content.lower())
            # Add to output dict in case only one result is found and it is a digit
            if len(res) == 1 and res[0].isdigit():
                numbers[step] = res[0]

        return numbers

    def getSchneiderTransitionCondition(self, sourceFile, label):
        ''' this function analyses the transition condition from grafcet logic and repalces it with condition existing in logic

        The arguments are:
        @param sourceFile   : transition logic as string
        @param label        : transition condition from grafcet file

        This function returns:
        @return condition extracted from project logic
        '''
        pattern = re.compile(r"\s*%s\s*:=\s*(.+?);" % label, re.IGNORECASE | re.MULTILINE | re.DOTALL)
        condition = re.findall(pattern, sourceFile.replace("&lt;", "<").replace("&gt;", ">"))
        if condition:
            return " ".join(condition[0].replace("<", "&lt;").replace(">", "&gt;").split())
        else:
            return "NOT FOUND"

    def getFBofDBStepper(self, DBStepper, symbolFile):
        if self.UsageFinder.isSiemens():
            return self.getS7FBofDBStepper(DBStepper, symbolFile)
        else:
            self.thePlugin.writeWarningInUABLog("getFBofDBStepper function will return an empty string for the current platform")
            return ""

    def getS7FBofDBStepper(self, DBStepper, symbolFile):
        ''' this function retrieves FB number of the DBStepper from symbol file (SIEMENS ONLY)

        The arguments are:
        @param DBStepper   : name of the DB stepper
        @param symbolFile  : symbol file as string

        This function returns:
        @return FB number
        '''
        if DBStepper == "":
            return ""
        pattern = re.compile(r"^\"%s\s*\",\s*\"DB\s*.+?\s*\",\s*\"FB\s*(.+?)\s*\"" % DBStepper, re.IGNORECASE | re.MULTILINE)    # this looks for FB number of the graph that DB_Stepper is an instance of
        if re.search(pattern, symbolFile):
            return re.search(pattern, symbolFile).group(1)
        else:
            return ""

    def getGrafcetNameFromFB(self, grafcetFB, symbolFile):
        if self.UsageFinder.isSiemens():
            return self.getS7GrafcetNameFromFB(grafcetFB, symbolFile)
        else:
            self.thePlugin.writeWarningInUABLog("getGrafcetNameFromFB function will return an empty string for the current platform")
            return ""

    def getS7GrafcetNameFromFB(self, grafcetFB, symbolFile):
        ''' this function retrieves name of the grafcet from symbol file based on FB number (SIEMENS ONLY)

        The arguments are:
        @param grafcetFB   : FB number of the stepper
        @param symbolFile  : symbol file as string

        This function returns:
        @return name of the grafcet
        '''
        if grafcetFB == "":
            return ""
        pattern = re.compile(r"^\"\s*([^\"]+?)\s*\",\"FB\s*%s\s*\"" % grafcetFB, re.IGNORECASE | re.MULTILINE)                        # find the grafcet name based on its FB number
        if re.search(pattern, symbolFile):
            return re.search(pattern, symbolFile).group(1)
        else:
            return ""

    def getGrafcetSourceFromName(self, grafcetFB, grafcetName, ProjectSources):
        if self.UsageFinder.isSiemens():
            return self.getS7GrafcetSourceFromName(grafcetFB, grafcetName, ProjectSources)
        else:
            self.thePlugin.writeWarningInUABLog("getGrafcetSourceFromName function will return an empty string for the current platform")
            return ""

    def getS7GrafcetSourceFromName(self, grafcetFB, grafcetName, ProjectSources):
        ''' this function finds the source file of the grafcet either by its name or FB number (SIEMENS ONLY)
        The arguments are:
        @param grafcetFB        : FB number of the stepper
        @param grafcetName      : name of the grafcet
        @param ProjectSources   : set of the project sources

        This function returns:
        @return name of the grafcet
        '''
        if ((grafcetFB == "") or (grafcetName == "")):
            return ""
        pattern = re.compile(r"^FUNCTION_BLOCK\s*(FB" + str(grafcetFB) + "|\"" + str(grafcetName) + "\")", re.IGNORECASE | re.MULTILINE)
        return self.find(lambda fname: re.search(pattern, ProjectSources[fname]), ProjectSources)

    def getStepsOfDBStepper(self, DB_Stepper, ProjectSources=[]):
        if self.UsageFinder.isSiemens():
            return self.getS7StepsOfDBStepper(DB_Stepper, ProjectSources)
        else:
            self.thePlugin.writeWarningInUABLog("getStepsOfDBStepper function will return an empty list for the current platform")
            return []

    def getS7StepsOfDBStepper(self, DB_Stepper, ProjectSources):
        ''' this function finds the steps list for given DBstepper name
        The arguments are:
        @param DB_Stepper       : FB number of the stepper
        @param ProjectSources   : set of the project sources

        This function returns:
        @return name of the grafcet
        '''
        if DB_Stepper == "":
            return []

        # load sources
        if not ProjectSources:
            ProjectSources = self.UsageFinder.loadSources()
            #pathToSources = str(self.UsageFinder.locateDir("*exported_src", AbsolutePathBuilder.getAbsolutePath("../"))) + "\\"
            # if not os.path.exists(pathToSources):       # use generated output
            #    ProjectSources = self.UsageFinder.loadSources()
            # else:                                       # use exported sources
            #    ProjectSources = self.UsageFinder.loadSources(pathToSources)

        # get the FB and name of the Grafcet
        symbolFile = self.find(lambda filename: filename.endswith(".sdf"), ProjectSources)  # filename of .sdf file
        grafcetFB = self.getFBofDBStepper(DB_Stepper, ProjectSources[symbolFile])
        if grafcetFB == "":
            self.thePlugin.writeWarningInUABLog("No grafcetFB found in symbolFile for %s." % DB_Stepper)
            return []
        grafcetName = self.getGrafcetNameFromFB(grafcetFB, ProjectSources[symbolFile])
        if grafcetName == "":
            self.thePlugin.writeWarningInUABLog("No grafcetName found in symbolFile for FB %s." % grafcetFB)
            return []

        self.thePlugin.writeInUABLog("DBStepper: %s, instance of grafcet %s (FB%s)" % (DB_Stepper, grafcetName, grafcetFB))

        # find the grafcet source and parse it
        GrafcetSourceFile = self.getGrafcetSourceFromName(grafcetFB, grafcetName, ProjectSources)
        if GrafcetSourceFile is None:
            self.thePlugin.writeWarningInUABLog("No source file found for grafcet %s." % grafcetName)
            return []
        else:
            (grafcetSteps, grafcetTransitions) = self.parseGrafcet(DB_Stepper, "", ProjectSources[GrafcetSourceFile])
            return [stepName for (stepName, stepNumber) in grafcetSteps]  # get the step names

    @staticmethod
    def addPVSSXmlHeader(width=1270, height=830):
        return '''<?xml version="1.0" encoding="utf-8"?>
    <panel>
        <properties>
            <prop name="Size">%s %s</prop>
            <prop name="BackColor">unDisplayValue_Background</prop>
            <prop name="RefPoint">0 0</prop>
            <prop name="InitAndTermRef">True</prop>
            <prop name="SendClick">False</prop>
        </properties>
        <shapes>
        ''' % (width, height)

    @staticmethod
    def addPVSSFooter():
        return '''</shapes>
    </panel>'''

    @staticmethod
    def addPVSSTransition(uniqueId, posX, posY, iBit, sIdentifier, geometry="1 0 0 1 0 0"):
        return u'''<reference parentSerial="-1" Name="PANEL_REF%(uniqueId)s" referenceId="%(uniqueId)s">
        <properties>
            <prop name="FileName">objects/CPC_OBJECTS/cpcWidget_WordStatusTransition.pnl</prop>
            <prop name="Location">%(posX)s %(posY)s</prop>
            <prop name="TabOrder">10</prop>
            <prop name="dollarParameters">
                <prop name="dollarParameter">
                  <prop name="Dollar">\x24iBit</prop>
                  <prop name="Value">%(iBit)s</prop>
                </prop>
                <prop name="dollarParameter">
                  <prop name="Dollar">\x24sActive_Color</prop>
                  <prop name="Value">{0,255,0}</prop>
                </prop>
                <prop name="dollarParameter">
                    <prop name="Dollar">\x24sIdentifier</prop>
                    <prop name="Value">%(sIdentifier)s</prop>
                </prop>
                <prop name="dollarParameter">
                    <prop name="Dollar">\x24sPassive_Color</prop>
                    <prop name="Value">{255,0,0}</prop>
                </prop>
            </prop>
            <prop name="layoutAlignment">AlignNone</prop>
        </properties>
    </reference>''' % {'uniqueId': uniqueId, 'geometry': geometry, 'posX': posX, 'posY': posY, 'iBit': iBit, 'sIdentifier': sIdentifier}

    def addPVSSArrow(self, uniqueId, posX, posY):
        return u'''<reference referenceId="%(uniqueId)s" Name="PANEL_REF%(uniqueId)s" parentSerial="-1">
       <properties>
        <prop name="FileName">objects\CPC_ARROWS\Air.pnl</prop>
        <prop name="Location">%(posX)s %(posY)s</prop>
        <prop name="Geometry">0 1 1 0 0 0</prop>
        <prop name="TabOrder">27</prop>
       </properties>
      </reference>''' % {'uniqueId': uniqueId, 'posX': posX, 'posY': posY}

    def addPVSSLine(self, uniqueId, startX, startY, endX, endY):
        return '''<shape shapeType="LINE" layerId="0" Name="LINE%(uniqueId)s">
        <properties>
            <prop name="serialId">%(uniqueId)s</prop>
            <prop name="RefPoint">0 0</prop>
            <prop name="Enable">True</prop>
            <prop name="Visible">True</prop>
            <prop name="ForeColor">_3DFace</prop>
            <prop name="BackColor">_3DFace</prop>
            <prop name="TabOrder">2</prop>
            <prop name="DashBackColor">_Transparent</prop>
            <prop name="LineType">[solid,oneColor,JoinMiter,CapButt,1]</prop>
            <prop name="Start">%(startX)s %(startY)s</prop>
            <prop name="End">%(endX)s %(endY)s</prop>
        </properties>
    </shape>''' % {'uniqueId': uniqueId, 'startX': startX, 'startY': startY, 'endX': endX, 'endY': endY}

    def addPVSSText(self, uniqueId, posX, posY, label, font="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"):
        return '''<shape shapeType="PRIMITIVE_TEXT" layerId="0" Name="PRIMITIVE_TEXT%(uniqueId)s">
                <properties>
                    <prop name="serialId">%(uniqueId)s</prop>
                    <prop name="RefPoint">0 0</prop>
                    <prop name="Enable">True</prop>
                    <prop name="Visible">True</prop>
                    <prop name="ForeColor">unSynoptic_staticText</prop>
                    <prop name="BackColor">_Window</prop>
                    <prop name="TabOrder">3</prop>
                    <prop name="DashBackColor">_Transparent</prop>
                    <prop name="LineType">[solid,oneColor,JoinMiter,CapButt,1]</prop>
                    <prop name="FillType">[outline]</prop>
                    <prop name="Geometry">1 0 0 1 0 0</prop>
                    <prop name="Location">%(posX)s %(posY)s</prop>
                    <prop name="Font">
                        <prop name="en_US.iso88591">%(font)s</prop>
                    </prop>
                    <prop name="Text">
                        <prop name="en_US.iso88591">%(label)s</prop>
                    </prop>
                    <prop name="Distance">2</prop>
                    <prop name="BorderOffset">2</prop>
                    <prop name="Bordered">False</prop>
                    <prop name="Fit">True</prop>
                    <prop name="TextFormat">[0s,,,ALIGNMENT_BEGINNING]</prop>
                </properties>
            </shape>''' % {'uniqueId': uniqueId, 'posX': posX, 'posY': posY, 'label': label, 'font': font}

    def addPVSSStep(self, uniqueId, posX, posY, iValue, sIdentifier, xInit_Step):
        return u'''<reference parentSerial="-1" Name="PANEL_REF%(uniqueId)s" referenceId="%(uniqueId)s">
                <properties>
                    <prop name="FileName">objects/CPC_OBJECTS/cpcWidget_WordStatusStepWide.pnl</prop>
                    <prop name="Location">%(posX)s %(posY)s</prop>
                    <prop name="Geometry">1 0 0 1 0 0</prop>
                    <prop name="TabOrder">1</prop>
                    <prop name="dollarParameters">
                        <prop name="dollarParameter">
                            <prop name="Dollar">\x24LabelMode</prop>
                            <prop name="Value">1</prop>
                        </prop>
                        <prop name="dollarParameter">
                            <prop name="Dollar">\x24iValue</prop>
                            <prop name="Value">%(iValue)s</prop>
                        </prop>
                        <prop name="dollarParameter">
                            <prop name="Dollar">\x24sActive_Color</prop>
                            <prop name="Value">{0,255,0}</prop>
                        </prop>
                        <prop name="dollarParameter">
                            <prop name="Dollar">\x24sIdentifier</prop>
                            <prop name="Value">%(sIdentifier)s</prop>
                        </prop>
                        <prop name="dollarParameter">
                            <prop name="Dollar">\x24sPassive_Color</prop>
                            <prop name="Value">{255,255,255}</prop>
                        </prop>
                        <prop name="dollarParameter">
                            <prop name="Dollar">\x24xInitStep</prop>
                            <prop name="Value">%(xInit_Step)s</prop>
                        </prop>
                    </prop>
                    <prop name="layoutAlignment">AlignNone</prop>
               </properties>
          </reference>
            ''' % {'uniqueId': uniqueId, 'posX': posX, 'posY': posY, 'iValue': iValue, 'sIdentifier': sIdentifier, 'xInit_Step': xInit_Step}

    def align(self, input, v=10):
        return v * int(input / float(v))

    def addPVSSHeaderBar(self, uniqueId, text, posX=30, posY=53):
        return u'''<reference referenceId="%(uniqueId)s" Name="PANEL_REF%(uniqueId)s" parentSerial="-1">
   <properties>
    <prop name="FileName">objects\\UN_OBJECTS\\unSynopticHeader.pnl</prop>
    <prop name="Location">%(posX)s %(posY)s</prop>
    <prop name="Geometry">1 0 0 1 -18 -30</prop>
    <prop name="TabOrder">2</prop>
    <prop name="dollarParameters">
     <prop name="dollarParameter">
      <prop name="Dollar">\x24headerTitle</prop>
      <prop name="Value">\x24headerTitle</prop>
     </prop>
     <prop name="dollarParameter">
      <prop name="Dollar">\x24sLayerState</prop>
      <prop name="Value">1;1;1;1</prop>
     </prop>
    </prop>
   </properties>
   <shape shapeType="RefShape" RefShapeSerial="6" layerId="0" GroupPath="">
    <properties>
     <prop type="LANG_TEXT_ARRAY" name="primitiveText">
      <prop name="en_US.iso88591">%(text)s</prop>
     </prop>
     <prop type="TRANSFORM" name="transform">1 0 0 1 2 0</prop>
    </properties>
   </shape>
  </reference>''' % {'uniqueId': uniqueId, 'posX': posX, 'posY': posY, 'text': text}

    def addPVSSPCO(self, uniqueId, posX, posY, sIdentifier):
        return u'''<reference referenceId="%(uniqueId)s" Name="PANEL_REF%(uniqueId)s" parentSerial="-1">
   <properties>
    <prop name="FileName">objects\\CPC_OBJECTS\\cpcWidget_PCO.pnl</prop>
    <prop name="Location">%(posX)s %(posY)s</prop>
    <prop name="Geometry">1 0 0 1 0 0</prop>
    <prop name="TabOrder">199</prop>
    <prop name="dollarParameters">
     <prop name="dollarParameter">
      <prop name="Dollar">\x24sIdentifier</prop>
      <prop name="Value">%(sIdentifier)s</prop>
     </prop>
    </prop>
   </properties>
  </reference>''' % {'uniqueId': uniqueId, 'posX': posX, 'posY': posY, 'sIdentifier': sIdentifier}

    def addPVSSStatus(self, uniqueId, posX, posY, sIdentifier):
        return u'''<reference referenceId="%(uniqueId)s" Name="PANEL_REF%(uniqueId)s" parentSerial="-1">
   <properties>
    <prop name="FileName">objects\\CPC_OBJECTS\\cpcWidget_WordStatus.pnl</prop>
    <prop name="Location">%(posX)s %(posY)s</prop>
    <prop name="Geometry">1 0 0 1 0 0</prop>
    <prop name="TabOrder">199</prop>
    <prop name="dollarParameters">
     <prop name="dollarParameter">
      <prop name="Dollar">\x24sIdentifier</prop>
      <prop name="Value">%(sIdentifier)s</prop>
     </prop>
    </prop>
   </properties>
  </reference>''' % {'uniqueId': uniqueId, 'posX': posX, 'posY': posY, 'sIdentifier': sIdentifier}

    def getExpertNameFromName(self, name):
        """
        This function returns the expert name (if available) from a given name.
        If the expert name is not defined, then will return the object name.

        The arguments are:
        @param name: object name

        This function returns:
        @return: name: the expert name (or name) of the object for use in WinCCOA db file
        """

        instance = self.thePlugin.getUnicosProject().findInstanceByName(name)
        if instance is not None:
            if instance.getAttributeData("DeviceIdentification:Expert Name") != "":
                return instance.getAttributeData("DeviceIdentification:Expert Name")
            else:
                return instance.getAttributeData("DeviceIdentification:Name")
        else:
            return ""
