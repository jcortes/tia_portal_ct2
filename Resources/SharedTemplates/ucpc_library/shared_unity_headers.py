# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2015 all rights reserved
# $LastChangedRevision$


class SharedUnityHeaders:
    # return strings for Unity header/footer when splitting output file into various parts

    def STheader(self):
        return """<STExchangeFile>
    <fileHeader company="Schneider Automation" product="Unity Pro XL V7.0 - 120823C" dateTime="date_and_time#2014-6-17-16:48:11" content="Structured source file" DTDVersion="41"></fileHeader>
"""

    def STfooter(self):
        return """</STExchangeFile>"""

    def FBDheader(self):
        return """<FBDExchangeFile>
    <fileHeader company="Schneider Automation" product="Unity Pro XL V7.0 - 120823C" dateTime="date_and_time#2014-7-9-15:28:4" content="Derived Function Block source file" DTDVersion="41"></fileHeader>
"""

    def FBDfooter(self):
        return """</FBDExchangeFile>"""

    def SFCheader(self):
        return """<SFCExchangeFile>
    <fileHeader company="Schneider Automation" product="Unity Pro XL V7.0 - 120823C" dateTime="date_and_time#2014-7-9-16:49:53" content="SFC source file" DTDVersion="41"></fileHeader>
"""

    def SFCfooter(self):
        return """</SFCExchangeFile>"""

    def IOheader(self):
        return """<IOExchangeFile>
    <fileHeader company="Schneider Automation" product="Unity Pro XL V7.0 - 120823C" dateTime="date_and_time#2015-6-26-15:40:26" content="Configuration source file" DTDVersion="41"></fileHeader>
"""

    def IOfooter(self):
        return """</IOExchangeFile>"""

    def FBheader(self):
        return """<FBExchangeFile>
    <fileHeader company="Schneider Automation" product="Unity Pro XL V7.0 - 120823C" dateTime="date_and_time#2015-6-26-14:32:36" content="Function Block source file" DTDVersion="41"></fileHeader>
"""

    def FBfooter(self):
        return """</FBExchangeFile>"""

    def DDTheader(self):
        return """<DDTExchangeFile>
    <fileHeader company="Schneider Automation" product="Unity Pro XL V7.0 - 120823C" dateTime="date_and_time#2015-6-26-14:32:11" content="Derived Data Type source file" DTDVersion="41"></fileHeader>
"""

    def DDTfooter(self):
        return """</DDTExchangeFile>"""

    def COMheader(self):
        return """<COMExchangeFile>
    <fileHeader company="Schneider Automation" product="Unity Pro XL V7.0 - 120823C" dateTime="date_and_time#2015-6-26-14:33:31" content="Communication source file" DTDVersion="41"></fileHeader>
"""

    def COMfooter(self):
        return """</COMExchangeFile>"""
