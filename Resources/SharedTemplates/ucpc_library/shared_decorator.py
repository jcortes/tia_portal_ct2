# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$

from java.lang import System
import re
from research.ch.cern.unicos.plugins.interfaces import APlugin
import textwrap
import ucpc_library.shared_usage_finder
# reload(ucpc_library.shared_usage_finder)
import ucpc_library.shared_generic_functions
# reload(ucpc_library.shared_generic_functions)

"""
class is responsible for :
    - decorating siemens logic decoration with DB_ prefixes and default outputPins
    - formatting output so there are no unnecessary spaces
    - splits generated line if it exceeds predefined line length
    
    class member functions are:
    
    splitExpression         : convert string expression to list of independend parts; it adds NEWLINE_IN_EXPRESSION string in each occurence of \n (newline)
    formatExpression        : removes unnecessary spaces, replaces NEWLINE_IN_EXPRESSION with platform-specific newline character and splits lines which are exceeding maxLineLength (=200 by default)
    
    unDecorateExpression    : removes DB_ prefixes and default output pins from expression
    unDecorateString        : removes DB_ prefixes and default output pins from single string
    
    decorateExpression      : adds necessary DB_ prefixes and default output pins to the expression
    decorateString          : adds necessary DB_ prefixes and default output pins to single string
"""


class ExpressionDecorator:
    """ This function initializes object of the S7ExpressionDecorator class

    The arguments are:
    @param thePlugin: object representing the java plug-in 
    @param maxLineLength: maximum length of the generated line of code

    """
    def __init__(self):
        self.finder = ucpc_library.shared_usage_finder.Finder()
        self.thePlugin = APlugin.getPluginInterface()
        self.CRLF = System.getProperty("line.separator")
        self.maxLineLength = 500

        self.S7Functions = None

        # default output pins for devices
        # this set should be reviewed against Schneider types names
        self.defaultOutputPin = {}
        self.defaultOutputPin.update(dict.fromkeys(["DI", "DIGITALINPUT", "AI", "ANALOGINPUT", "AIR", "ANALOGINPUTREAL", "ENC", "ENCODER", "DO", "DIGITALOUTPUT", "AO", "ANALOGOUTPUT", "AOR", "ANALOGOUTPUTREAL", "DPAR", "DIGITALPARAMETER", "WPAR", "WORDPARAMETER", "APAR", "ANALOGPARAMETER", "WS", "WORDSTATUS", "AS", "ANALOGSTATUS"], ".PosSt"))
        self.defaultOutputPin.update(dict.fromkeys([""], ""))
        self.defaultOutputPin.update(dict.fromkeys(["LOCAL", "ONOFF", "ANADIG", "ANALOGDIGITAL", "ANADO"], ".OnSt"))
        self.defaultOutputPin.update(dict.fromkeys(["ANALOG", "MFC", "MASSFLOWCONTROLLER", "STPMOT", "STEPPINGMOTOR"], ".PosSt"))
        self.defaultOutputPin.update(dict.fromkeys(["PID", "CONTROLLER"], ".OutOV"))
        self.defaultOutputPin.update(dict.fromkeys(["PCO", "PROCESSCONTROLOBJECT"], ".RunOSt"))
        self.defaultOutputPin.update(dict.fromkeys(["DA", "DIGITALALARM", "AA", "ANALOGALARM"], ".ISt"))

    def splitExpression(self, expression):
        """ This function splits a string expression into list of consecutive independent parts
        @param expression:  string with expression to split
        @return list of strings: split expression 
        """
        # these characters must be treaded as separate, but they can stick to other parts of expression: (name or a=b
        for character in ["(", ")", "+", "-", "*", "/", "<", "&lt", ">", "&gt", "=", ",", ":", ";"]:
            expression = expression.replace(character, " " + character + " ")
        # workaround to prevent newlines in the processed expression
        expression = expression.replace("\n", " NEWLINE_IN_EXPRESSION ")
        return expression.split()

    def getListOfUNICOSObjects(self, expression, typesToFilter=[]):
        if self.finder.isSiemens():
            return self.getListOfUNICOSS7Objects(expression, typesToFilter)
        if self.finder.isTia():
            return self.getListOfUNICOSTIAObjects(expression, typesToFilter)
        if self.finder.isSchneider():
            return self.getListOfUNICOSSchObjects(expression, typesToFilter)

    def getActuatorsIOs(self, expression):
        """ This function returns list of IO object names that are linked to the actuators in the input expression
        @param expression:  string with expression to process
        @return list of strings: IO object names corresponding to IO pins of the actuators in the input expression
        """
        IOPinMapping = {}

        IOPinMapping["outonov"] = {"onoff": "FEDeviceOutputs:Process Output",
                                   "anado": "FEDeviceOutputs:Digital Process Output"}

        IOPinMapping["outoffov"] = {"onoff": "FEDeviceOutputs:Process Output Off"}

        IOPinMapping["onst"] = {"onoff": "FEDeviceEnvironmentInputs:Feedback On",
                                "analog": "FEDeviceEnvironmentInputs:Feedback On",
                                "anadig": "FEDeviceEnvironmentInputs:Feedback On",
                                "anado": "FEDeviceEnvironmentInputs:Feedback On"}

        IOPinMapping["offst"] = {"onoff": "FEDeviceEnvironmentInputs:Feedback Off",
                                 "analog": "FEDeviceEnvironmentInputs:Feedback Off",
                                 "anadig": "FEDeviceEnvironmentInputs:Feedback Off"}

        IOPinMapping["outov"] = {"analog": "FEDeviceOutputs:Process Output",
                                 "anado": "FEDeviceOutputs:Analog Process Output"}

        IOPinMapping["posst"] = {"analog": "FEDeviceEnvironmentInputs:Feedback Analog",
                                 "anadig": "FEDeviceEnvironmentInputs:Feedback Analog",
                                 "anado": "FEDeviceEnvironmentInputs:Feedback Analog"}

        IOPinMapping["posrst"] = {"analog": "FEDeviceOutputs:Process Output"}

        IOPinMapping["doutonov"] = {"anadig": "FEDeviceOutputs:Output On"}

        IOPinMapping["doutoffov"] = {"anadig": "FEDeviceOutputs:Output Off"}

        IOPinMapping["outonovst"] = {"anado": "FEDeviceOutputs:Digital Process Output"}

        # split input expression (on fullstops too)
        listOfStrings = [words for segments in self.splitExpression(expression) for words in segments.split('.')]

        outputIOSet = []

        UNICOSProject = self.thePlugin.getUnicosProject()

        # go through the strings with indexing
        for index, word in enumerate(listOfStrings):
            # if the word IS a pin
            if word.lower() in IOPinMapping:
                if index == 0:
                    self.thePlugin.writeWarningInUABLog(word.lower() + " must follow a device.")
                else:
                    # find preceeding object
                    actuatorInstance = UNICOSProject.findInstanceByName(listOfStrings[index - 1])
                    if not actuatorInstance:
                        self.thePlugin.writeWarningInUABLog("Device " + listOfStrings[index - 1] + " does not exist in UNICOS project.")
                    else:
                        actuatorType = actuatorInstance.getDeviceType()
                        actuatorTypeName = actuatorType.getDeviceTypeName()
                        if actuatorTypeName.lower() in IOPinMapping[word.lower()]:
                            pinColumnName = IOPinMapping[word.lower()][actuatorTypeName.lower()]
                            IOname = actuatorInstance.getAttributeData(pinColumnName)
                            if IOname:
                                outputIOSet.append(IOname)

        return outputIOSet

    def getListOfUNICOSS7Objects(self, expression, typesToFilter=[]):
        """ This function returns list of the UNICOS objects that appear in the given expression
        @param expression:  string with expression to be checked
        @param typesToFilter:  list of strings with types to be filtered; if empty, all types are taken; e.g. ["DI", "DO", "AI", "AO"]
        @return list of unicos objects' names
        """
        # preapre set that's easier to process
        typesToFilter = [type.lower() + "_set" for type in typesToFilter]

        # split input expression (on fullstops too)
        listOfStrings = [words for segments in self.splitExpression(expression) for words in segments.split('.')]

        # for each object on the input list - check if it has UNICOS prefix and (if that prefix is on the filter list or filter is empty)
        try:
            if not self.S7Functions:
                from research.ch.cern.unicos.cpc.utilities.siemens import S7Functions
                self.S7Functions = S7Functions
            listOfObjects = [s for s in listOfStrings if self.S7Functions.s7db_id(s).lower() and (not typesToFilter or self.S7Functions.s7db_id(s).lower().split('.')[1] in typesToFilter)]
            return self.uniq(listOfObjects)
        except:
            self.thePlugin.writeWarningInUABLog("Cannot load S7Functions from research.ch.cern.unicos.cpc.utilities.siemens. Function getListOfUNICOSS7Objects is not working correctly.")
            return listOfStrings

    def getListOfUNICOSTIAObjects(self, expression, typesToFilter=[]):
        # preapre set that's easier to process
        typesToFilter = [type.lower() for type in typesToFilter]
        listOfStrings = [words for segments in self.splitExpression(expression) for words in segments.split('.')]
        UNICOSProject = self.thePlugin.getUnicosProject()
        listOfObjects = [s for s in listOfStrings if UNICOSProject.findInstanceByName(s) and (not typesToFilter or UNICOSProject.findInstanceByName(s).getDeviceType().getDeviceTypeName().lower() in typesToFilter)]
        return self.uniq(listOfObjects)

    def getListOfUNICOSSchObjects(self, expression, typesToFilter=[]):
        # preapre set that's easier to process
        typesToFilter = [type.lower() for type in typesToFilter]

        # split input expression (on fullstops too)
        listOfStrings = [words for segments in self.splitExpression(expression) for words in segments.split('.')]
        UNICOSProject = self.thePlugin.getUnicosProject()
        # for each object on the input list - check if it has UNICOS prefix and (if that prefix is on the filter list or filter is empty)
        listOfObjects = [s for s in listOfStrings if UNICOSProject.findInstanceByName(s) and (not typesToFilter or UNICOSProject.findInstanceByName(s).getDeviceType().getDeviceTypeName().lower() in typesToFilter)]
        return self.uniq(listOfObjects)

    def formatExpression(self, expression):
        """ This function formats expression: removes spaces next to parentheses, removes multiple spaces, splits output line into segments of specified length
        @param expression:  string with expression to format
        @return string:     formated expression
        """
        leadingWhitespace = re.match("(\s*)", expression).group(0)
        return leadingWhitespace + (self.CRLF + leadingWhitespace).join(textwrap.wrap(" ".join(expression.replace("( ", "(").replace(" )", ")").replace("NEWLINE_IN_EXPRESSION", self.CRLF).replace("< >", "<>").replace("> =", ">=").replace("< =", "<=").replace("&lt; &gt;", "&lt;&gt;").replace("&gt; =", "&gt;=").replace("&lt; =", "&lt;=").replace(": =", ":=").replace("= >", "=>").replace("= &gt;", "=&gt;").replace("/ /", "//").replace(" ;", ";").replace("; ", ";").split()), width=self.maxLineLength - len(leadingWhitespace)))

    def unDecorateExpression(self, expression):
        """ This function UNdecorates all parts of expression from DB_ prefixes and default outputPins if present
        @param expression:  string with expression to UNdecorate
        @return string:     UNdecorated expression
        """
        if self.finder.isSiemens():
            return self.formatExpression(" ".join([self.unDecorateS7String(part) for part in self.splitExpression(expression)]))
        if self.finder.isTia():
            return self.formatExpression(" ".join([self.unDecorateTIAString(part) for part in self.splitExpression(expression)]))
        if self.finder.isSchneider():
            return self.formatExpression(" ".join([self.unDecorateSchString(part) for part in self.splitExpression(expression)]))

    def decorateExpression(self, expression, grafcetName="", listOfSteps=[], isDBSimpleRequested=False):
        """ This function decorates all parts of expression with DB_ prefixes if needed and outputPins if not given
        @param expression:  string with expression to decorate
        @param grafcetName:   grafcetName 
        @param listOfSteps:   listOfSteps 
        @param isDBSimpleRequested (Siemens only): if True, then if AIR or DI, use "DB_Type_All_S.Type_SET." prefix
        @return string:     decorated expression
        """
        if self.finder.isSiemens():
            return self.CRLF.join([self.formatExpression(re.match("(\s*)", line).group(0) + " ".join([self.decorateS7String(part, grafcetName, listOfSteps, isDBSimpleRequested) for part in self.splitExpression(line)])) for line in expression.split("\n")])
        if self.finder.isTia():
            return self.CRLF.join([self.formatExpression(re.match("(\s*)", line).group(0) + " ".join([self.decorateTIAString(part, grafcetName, listOfSteps) for part in self.splitExpression(line)])) for line in expression.split("\n")])
        if self.finder.isSchneider():
            return self.CRLF.join([self.formatExpression(re.match("(\s*)", line).group(0) + " ".join([self.decorateSchString(part, grafcetName, listOfSteps) for part in self.splitExpression(line)])) for line in expression.split("\n")])

    def decorateString(self, input, grafcetName="", listOfSteps=[], isDBSimpleRequested=False):
        """ This function decorates input string with prefix if needed and default output pin if not provided
        @param input:   input string - part of expression to decorate

                    can be in form: 'deviceName' - function will find prefix if [device exists and prefix is required] and add default outputPin after the name:
                        result: deviceName.defaultOutputPin                                     # if prefix not needed (Local, OnOff, Analog, AnaDig, AnaDO, MFC, PID, PCO)
                        result: DB_deviceType_ALL.deviceType_SET.deviceName.defaultOutputPin    # if prefix needed (DI, DO, AI, AO, AIR, AOR, APAR, DPAR, WPAR, AS, WS, AA, DA)
                        if there is no default outputPin for that device type, warning will be produced

                    or in form: 'deviceName.outputPin' - then function will look for prefix for the device only
                        result: deviceName.outputPin                                     # if prefix not needed (Local, OnOff, Analog, AnaDig, AnaDO, MFC, PID, PCO)
                        result: DB_deviceType_ALL.deviceType_SET.deviceName.outputPin    # if prefix needed (DI, DO, AI, AO, AIR, AOR, APAR, DPAR, WPAR, AS, WS, AA, DA)

                    for other strings, like: AND, (, DB_AI_ALL.AI_SET.Name.Output 
                        it will simply take substring from the beginning of string to the first '.' occurence (or the whole string if no . inside) and try to retrieve device type
                        if device type is not existing (=empty, "") then no prefix will be provided (there are no devices called AND/NOT/DB_AI_ALL, etc)
                        for type "" default outputPin is empty
                        in result, input string won't be changed
                    # potential problem: name of PLC variable CAN be the same as name of device which type require DB_ prefix
                    # example:
                    # Timer called  FSVE_Timer_028
                    # AS called     FSVE_Timer_028
                    # used in expression as: FSVE_Timer_028.Q will be expanded to: DB_AS_ALL.AS_SET.FSVE_Timer_028.Q which is not correct
                    # need to find smart solution for this problem
        @param grafcetName:   grafcetName 
        @param listOfSteps:   listOfSteps 
        @param isDBSimpleRequested (Siemens only): if True, then if AIR or DI, use "DB_Type_All_S.Type_SET." prefix
        @return string: completed string
        """
        if self.finder.isSiemens():
            return self.decorateS7String(input, grafcetName, listOfSteps, isDBSimpleRequested)
        if self.finder.isTia():
            return self.decorateTIAString(input, grafcetName, listOfSteps)
        if self.finder.isSchneider():
            return self.decorateSchString(input, grafcetName, listOfSteps)

    def decorateSchString(self, input, grafcetName="", listOfSteps=[]):
        """ This function decorates input Schneider string with prefix and default output pin if provided
        @param input:   input string 
        @return string: completed string
        """

        # check if the input is a step of the grafcet (case-insensitive)
        if input.upper() in (step.upper() for step in listOfSteps):
            return input + ".X"

        # determine correct outputPin
        outputPin = ""

        if "." not in input:               # output pin not provided - find default one
            # get device name
            name = input.split(".")[0].strip()
            # find instance
            instance = self.thePlugin.getUnicosProject().findInstanceByName(name)
            if instance:
                deviceType = instance.getDeviceType().getDeviceTypeName()
                if deviceType:
                    outputPin = self.defaultOutputPin[deviceType.upper()]  # get the default outputPin for type of device
                    # security check, if user did not provide outputPin and default one does not exist
                    if deviceType != "" and outputPin == "":
                        self.thePlugin.writeErrorInUABLog("No default pin for: " + deviceType + ", " + name)
        else:
            self.validatePin(input)

        return input + outputPin

    def decorateTIAString(self, input, grafcetName="", listOfSteps=[]):
        """ This function decorates input TIA string with prefix and default output pin if provided
        @param input:   input string
        @return string: completed string
        """

        # check if the input is a step of the grafcet (case-insensitive)
        grafcet_var = input.split(".")[0]
        if grafcet_var.upper() in (step.upper() for step in listOfSteps):
            if input == grafcet_var:
                return grafcetName + "." + input + ".X"
            else:
                return grafcetName + "." + input

        # determine correct outputPin
        outputPin = ""

        if "." not in input:               # output pin not provided - find default one
            # get device name
            name = input.split(".")[0].strip()
            # find instance
            instance = self.thePlugin.getUnicosProject().findInstanceByName(name)
            if instance:
                deviceType = instance.getDeviceType().getDeviceTypeName()
                if deviceType:
                    outputPin = self.defaultOutputPin[deviceType.upper()]  # get the default outputPin for type of device
                    # security check, if user did not provide outputPin and default one does not exist
                    if deviceType != "" and outputPin == "":
                        self.thePlugin.writeErrorInUABLog("No default pin for: " + deviceType + ", " + name)
        else:
            self.validatePin(input)

        return input + outputPin

    def decorateS7String(self, input, grafcetName="", listOfSteps=[], isDBSimpleRequested=False):
        """ This function decorates input s7 string with prefix and default output pin if provided
        @param input:   input string 
        @param grafcetName:   grafcetName 
        @param listOfSteps:   listOfSteps 
        @param isDBSimpleRequested: if True, then if AIR or DI, use "DB_Type_All_S.Type_SET." prefix
        @return string: completed string
        """

        # check if the input is a step of the grafcet (case-insensitive)
        grafcet_var = input.split(".")[0]
        if grafcet_var.upper() in (step.upper() for step in listOfSteps):
            if input == grafcet_var:
                return grafcetName + "." + input + ".X"
            else:
                return grafcetName + "." + input

        # retrieve prefix for the given input (substring of input from the beginning to the first . occurence)
        try:
            if not self.S7Functions:
                from research.ch.cern.unicos.cpc.utilities.siemens import S7Functions
                self.S7Functions = S7Functions
            prefix = self.S7Functions.s7db_id(str(input.split(".")[0]), isDBSimpleRequested)
        except:
            self.thePlugin.writeWarningInUABLog("Cannot load S7Functions from research.ch.cern.unicos.cpc.utilities.siemens. Function decorateS7String is not working correctly.")
            prefix = ""

        # determine correct outputPin
        if "." in input:    # output pin provided
            outputPin = ""
            self.validatePin(input)
        else:               # output pin not provided - find default one
            if prefix != "":    # recognise type of the device
                type = str(prefix.split("_")[1]).upper()   # find the type based on prefix: DB_type_ALL.type_SET.
            else:
                type = ""       # no prefix <=> no type (=> no changes)

            outputPin = self.defaultOutputPin[type.upper()]  # get the default outputPin for type of device

            # security check, if user did not provide outputPin and default one does not exist
            if prefix != "" and outputPin == "":
                self.thePlugin.writeErrorInUABLog("No default pin for: " + prefix)

        # no need to add DB prefix for some types
        # prefixes for following devices have to be included in code, otherwise, remove it:
        prefixRequired = [type + "_SET" for type in ["DI", "DO", "AI", "AO", "AIR", "AOR", "WS", "AS", "DPAR", "WPAR", "APAR", "AA", "DA", "ENC"]]
        if len(prefix.split(".")) > 1 and prefix.split(".")[1].upper() not in prefixRequired:
            prefix = ""

        return prefix.upper() + input + outputPin

    def unDecorateString(self, input):
        """ This function undecorates input string from prefix and default output pin if provided
        @param input:   input string 
        @return string: undecorated string
        """
        if self.finder.isSiemens():
            return self.unDecorateS7String(input)
        if self.finder.isTia():
            return self.unDecorateTIAString(input)
        if self.finder.isSchneider():
            return self.unDecorateSchString(input)

    def unDecorateSchString(self, input):
        """ This function undecorates input Schneider string from prefix and default output pin if provided
        @param input:   input string 
        @return string: undecorated string
        """
        output = input

        if "." in output:
            name = input.split(".")[0].strip()
            # find instance
            instance = self.thePlugin.getUnicosProject().findInstanceByName(name)
            if instance:
                deviceTypeName = instance.getDeviceType().getDeviceTypeName()
                if deviceTypeName:
                    defaultOutputPin = self.defaultOutputPin[deviceTypeName.upper()]
                    outputPin = "." + input.split(".")[1].strip()
                    if defaultOutputPin.lower() == outputPin.lower():
                        return name

        return output

    def unDecorateS7String(self, input):
        """ This function undecorates input Siemens/TiaPortal string from prefix and default output pin if provided
        @param input:   input string 
        @return string: undecorated string
        """
        output = input
        # this pattern matches beginning of input string of form: DB_anything_ALL.anything_SET. or "DB_anything_ALL".anything_SET.
        pattern = re.compile(r"^\"?DB_.+_ALL[_2S]*\"?\..+_SET\.", re.IGNORECASE)
        prefix = re.match(pattern, input)   # find the occurence of given pattern

        if prefix:                          # if there was a match
            prefix = prefix.group(0)        # retrieve prefix

            output = output[len(prefix):]   # remove prefix from output

            if output.count(".") == 1:                              # if there is a dot in the string it means: name.outputPin
                type = str(prefix.split("_")[1]).upper()            # retrieve type from prefix
                if "." + output.split('.')[1].lower() == self.defaultOutputPin[type.upper()].lower():   # check if the outputPin is the default one
                    output = output.split(".")[0]                   # if is, remove it

        return output

    def unDecorateTIAString(self, input):
        """ This function undecorates input TIA string from default output pin if provided
        @param input:   input string 
        @return string: undecorated string
        """
        output = input

        if "." in output:
            name = input.split(".")[0].strip()
            # find instance
            instance = self.thePlugin.getUnicosProject().findInstanceByName(name)
            if instance:
                deviceTypeName = instance.getDeviceType().getDeviceTypeName()
                if deviceTypeName:
                    defaultOutputPin = self.defaultOutputPin[deviceTypeName.upper()]
                    outputPin = "." + input.split(".")[1].strip()
                    if defaultOutputPin.lower() == outputPin.lower():
                        return name

        return output

    def plcExpressionSemanticCheck(self, theSemanticVerifier, theUnicosProject, input, theCurrentDeviceTypeName, name, columnName, removeFirstKeyword=False):
        """ Function to do a semantic check on a given input expression
            if a single object (format #1), will give a SEVERE error if is not a UNICOS object (for backwards compatibility)
            for a complicated expression (format #2), will simply give WARNINGs for each object that is not a UNICOS object
            @param input - simplified PLC code from the spec. Two different formats are supported : 
                1. UNICOS_ALIAS or NOT UNICOS_ALIAS
                2. simplified PLC logic, e.g. DBXX.RUN.X AND DIOBJECT
        """

        singleObject = self.extractSingleUnicosObjectFromPLCExpression(input, removeFirstKeyword)
        if singleObject != "":  # format 1
            if not theSemanticVerifier.doesObjectExist(singleObject, theUnicosProject):
                self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance " + name + ": object ($singleObject$) in the '$columnName$' column doesn't exist")

        else:  # format 2
            splitInput = self.splitExpression(input)
            if not splitInput:
                return
            ignoreStrings = ["(", ")", "+", "-", "*", "/", "<", ">", "=", "and", "not", "or", "xor", "abs", "min", "max", ":", "=", ",", ":=",
                             "in1", "in2", "in3", "sqrt", "sqr", "exp", "expd", "ln", "log", "acos", "asin", "atan", "sin", "cos", "tan",
                             "mod", "div"]
            for part in splitInput:
                undecoratedPart = self.unDecorateString(part).strip()
                if "." in undecoratedPart:  # remove what's after dot
                    undecoratedPart = undecoratedPart.split(".")[0]
                if not ucpc_library.shared_generic_functions.SharedGenericFunctions().is_number(part) and undecoratedPart.lower() not in ignoreStrings and not theSemanticVerifier.doesObjectExist(undecoratedPart, theUnicosProject):
                    self.thePlugin.writeWarningInUABLog("" + str(theCurrentDeviceTypeName) + " instance " + name + ": variable '$part$' in the '$columnName$' column doesn't exist")

    def extractSingleUnicosObjectFromPLCExpression(self, input, removeFirstKeyword=False):
        """ Function to return a single UNICOS object from a given line of simplified PLC code
            if not a single object, returns empty string.
            @see plcExpressionSemanticCheck
        """
        splitInput = self.splitExpression(input)
        if not splitInput:
            return ""

        # remove initial "not" if it exists, and removeFirstKeyword flag is set
        if removeFirstKeyword and splitInput[0].lower() == "not":
            splitInput.pop(0)

        # if single string, not a number, and just alphanumeric (re: \w) then return single UNICOS object (because it will have passed the semantic check)
        if len(splitInput) == 1 and not ucpc_library.shared_generic_functions.SharedGenericFunctions().is_number(splitInput[0]) and (re.match('^[\w]+\x24', splitInput[0]) is not None):
            return splitInput[0]

        # if not a single object, return nothing
        else:
            return ""

    # Copied from crgutils.py here
    # https://git.cern.ch/web/cryo-controls-lhc-tunnel.git/tree/HEAD:/appskel/app_skel_crg/Resources/SharedTemplates
    # can use this function to replace S7Functions in RP 1.5 applications since S7Functions is not available
    # (thanks TE-CRG !)
    def get_db(self, object_name, status_only=False):
        """ return proper DB for an object
            (by default return "simple" DBs with status,
            status_only arg should be set to false if need to reference object's input)
            also accepts object type given as object name
        """
        if self.get_plc_manufacturer() != 'Siemens':
            # DBs exist only for Siemens
            return ''

        dev_type = self.get_device_type(object_name)
        if dev_type == None:
            dev_type = object_name

        # LimitSize = int(self.plugin.getTargetDeviceInformationParam("LimitSize", dev_type))    # ah, again...

        if dev_type == 'AnalogInput':
            db = "DB_AI_All.AI_SET"

        elif dev_type == 'AnalogInputReal':
            if status_only and \
               self.is_s_generated(dev_type):
                db = "DB_AIR_All_S"
            else:
                #db = self.plugin.s7db_id(object_name, dev_type)[:-9]
                db = "DB_AIR_All" + \
                    ("2" if self.get_object_index(object_name, dev_type) > 1000 else '')
            db += ".AIR_SET"

        elif dev_type == 'DigitalInput':
            if status_only and \
               self.is_s_generated(dev_type):
                db = "DB_DI_All_S"
            else:
                #db = self.plugin.s7db_id(object_name, dev_type)[:-8]
                db = "DB_DI_All" + \
                    ("2" if self.get_object_index(object_name, dev_type) > 1300 else '')
            db += ".DI_SET"

        elif dev_type == 'DigitalOutput':
            #db = self.plugin.s7db_id(object_name, dev_type)[:-1]
            db = "DB_DO_All" + \
                ( "2" if self.get_object_index(object_name, dev_type) > 2000 else '' ) +\
                ".DO_SET"

        elif dev_type == 'AnalogOutput':
            #db = self.plugin.s7db_id(object_name, dev_type)[:-1]
            db = "DB_AO_All" + \
                ( "2" if self.get_object_index(object_name, dev_type) > 1000 else '' ) +\
                ".AO_SET"

        elif dev_type == 'AnalogOutputReal':
            #db = self.plugin.s7db_id(object_name, dev_type)[:-1]
            db = "DB_AOR_All" + \
                ( "2" if self.get_object_index(object_name, dev_type) > 1000 else '' ) +\
                ".AOR_SET"

        elif dev_type == 'DigitalAlarm':
            db = 'DB_DA_All.DA_SET'

        elif dev_type == 'AnalogAlarm':
            db = 'DB_AA_All.AA_SET'

        elif dev_type == 'AnalogParameter':
            db = 'DB_APAR_All.APAR_SET'

        elif dev_type == 'AnalogStatus':
            db = 'DB_AS_All.AS_SET'

        elif dev_type == 'WordStatus':
            db = 'DB_WS_All.WS_SET'

        elif dev_type in ['OnOff', 'Analog', 'AnaDO', 'AnalogDigital', 'Local', 'ProcessControlObject']:
            return ""   # each has its own DB

        else:
            err = " Getting DB for type: " + dev_type + " is not implemented in get_db()!!!! "
            self.logger.log_error(err)
            return err

        return db

    # copied from http://stackoverflow.com/questions/480214/how-do-you-remove-duplicates-from-a-list-in-python-whilst-preserving-order
    def uniq(self, seq):
        """ return unique list from seq while preserving order
        """
        seen = set()
        seen_add = seen.add
        return [x for x in seq if not (x in seen or seen_add(x))]

    def validatePin(self, input):
        if '.' in input:
            pin = input.split('.')[-1]
            name = input.split('.')[-2]
            input_instance = self.thePlugin.getUnicosProject().findInstanceByName(name)
            if input_instance and pin.lower() not in [p.lower() for p in self.finder.getInstancePins(input_instance)]:
                self.thePlugin.writeErrorInUABLog("Pin " + pin + " does not exist for " + input_instance.getDeviceTypeName() + " device " + name + ".")
