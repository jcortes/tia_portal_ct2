# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2015 all rights reserved
# $LastChangedRevision$
import os
import re
import sys
from research.ch.cern.unicos.utilities import AbsolutePathBuilder
import xml.etree.ElementTree as ET
from xml.parsers.expat import *
from research.ch.cern.unicos.plugins.interfaces import APlugin
import fnmatch
import codecs
from research.ch.cern.unicos.utilities import DeviceTypeFactory


class Finder:
    def __init__(self):
        self.tree = 0
        self.root = 0
        self.plcBlocks = 0
        self.thePlugin = APlugin.getPluginInterface()
        self.theUnicosProject = self.thePlugin.getUnicosProject()

    """Return first item in sequence where f(item) == True."""

    def find(self, f, seq):
        for item in seq:
            if f(item):
                return item
        return None

    def isSiemens(self):
        return self.thePlugin.getXMLConfig().getPLCDeclarations().get(0).getClass().getSimpleName() == "SiemensPLC"

    def isSchneider(self):
        return self.thePlugin.getXMLConfig().getPLCDeclarations().get(0).getClass().getSimpleName() == "SchneiderPLC"

    def isTia(self):
        return self.thePlugin.getXMLConfig().getPLCDeclarations().get(0).getClass().getSimpleName() == "TiaPLC"

    # Copied from http://code.activestate.com/recipes/499305-locating-files-throughout-a-directory-tree/
    # and modified to find directory
    def locateDir(self, pattern, root=os.curdir):
        '''Locate all directories matching supplied pattern in and below
        supplied root directory.'''
        for path, dirs, files in os.walk(os.path.abspath(root)):
            for dirname in fnmatch.filter(dirs, pattern):
                return os.path.join(path, dirname)

    def locateFile(self, patternDir, patternFile, root=os.curdir):
        '''Locate all files matching patternFile in 
        directory matching supplied patternDir in and below
        supplied root directory.'''
        directory = self.locateDir(patternDir, root)
        if directory == None:
            return ""
        for path, dirs, files in os.walk(directory):
            #self.thePlugin.writeWarningInUABLog("path=" + str(path) + "; dirs=" + str(dirs) + "; files=" + str(files))
            for fname in fnmatch.filter(files, patternFile):
                return path + "/" + fname

    """ this function loads the projects sources from specified location
        
    The arguments are:
    @param pathToSource:  path to the directory with sources (Siemens) or to the project file (Schneider)

    This function returns directory:
    { filenameInLowercase:contentOfTheFile } in case of Siemens
    { unitName:contentOfTheFile } in case of Schneider
    """

    def loadSources(self, pathToSource=""):
        if self.isSiemens():
            return self.__loadSources_siemens(pathToSource)
        elif self.isTia():
            self.thePlugin.writeWarningInUABLog("Finder.loadSources not developed for TiaPLC yet. Please contact icecontrols.support@cern.ch for more info.")
            return {}
        elif self.isSchneider():
            return self.__loadSources_schneider(pathToSource)

    """ this function loads the Siemens projects sources from specified location to memory
        
    The arguments are:
    @param pathToSource:    path to the directory with sources
                            if this path is not specified, generated files are taken

    This function returns directory:
    { filenameInLowercase:contentOfTheFile } in case of Siemens
    """

    def __loadSources_siemens(self, pathToSource=""):
        try:
            # empty dictionary for sources
            sources = {}
            # if user did not provide path to sources, default OutputFolder will be processed
            if pathToSource == "":
                pathToSource = AbsolutePathBuilder.getTechnicalPathParameter("S7LogicGenerator:OutputParameters:OutputFolder")

                # add symbol file
                symbolFile = self.find(lambda fname: fname.lower().endswith(".sdf"), os.listdir(os.path.join(os.path.dirname(os.path.dirname(pathToSource)), "S7InstanceGenerator")))
                sources[os.path.basename(symbolFile).lower()] = self.readfile(os.path.join(os.path.dirname(os.path.dirname(pathToSource)), "S7InstanceGenerator", symbolFile))

                # add grafcets from default "grafcets" directory
                pathToGrafcets = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(pathToSource))), "grafcets") + "\\"
                if os.path.exists(pathToGrafcets):
                    for grafcetFileName in os.listdir(pathToGrafcets):
                        sources[os.path.basename(grafcetFileName).lower()] = self.readfile(pathToGrafcets + grafcetFileName)

            # rest of sources to process
            output = [os.path.join(pathToSource, filename) for filename in os.listdir(pathToSource) if os.path.isfile(os.path.join(pathToSource, filename))]

            for file in output:
                # do not read 'instance' files (in the case you pass in an pathToSource which contains all exported source)
                if os.path.basename(file).lower() not in \
                    ['1_compilation_baseline.inp',
                     '2_compilation_instance.inp',
                     '4_compilation_ob.scl',
                     'aa.scl',
                     'ai.scl',
                     'air.scl',
                     'anado.scl',
                     'analog.scl',
                     'ao.scl',
                     'aor.scl',
                     'apar.scl',
                     'as.scl',
                     'communication.scl',
                     'cpc_tspp_unicos.scl',
                     'da.scl',
                     'di.scl',
                     'do.scl',
                     'dpar.scl',
                     'iocommissioning.xml',
                     'local.scl',
                     'onoff.scl',
                     'pco.scl',
                     'pid.scl',
                     'recipes.scl',
                     'wpar.scl',
                     'ws.scl']:
                    sources[os.path.basename(file).lower()] = self.readfile(file)

            self.thePlugin.writeInUABLog("Finder.loadSources_siemens: Sources loaded")
            return sources
        except:
            self.thePlugin.writeWarningInUABLog("Unexpected error:" + str(sys.exc_info()))
            self.thePlugin.writeWarningInUABLog("Finder.loadSources_siemens: Unable to load the sources")
            return {}

    """ this function loads the Schneider project sources from specified location to memory
        
    The arguments are:
    @param pathToSource:    path to the directory with sources
                            if this path is not specified, generated files are taken

    This function returns directory:
    { filenameInLowercase:contentOfTheFile } in case of Siemens
    """

    def __loadSources_schneider(self, pathToSource=""):
        try:
            self.initTree_schneider(pathToSource)
            sources = {}
            for program in self.root.findall("program"):
                blockName = program.findall("identProgram")[0].attrib['name']
                #self.thePlugin.writeDebugInUABLog("Finder.loadSources_schneider: blockName = " + blockName)
                # do not read 'instance' files (in the case you pass in an pathToSource which contains all exported source)
                if blockName not in \
                    ['Time_Manager',
                     'DS_PLC_Command',
                     'Extract_Date_Time',
                     'CPC_Recipe'] and not blockName.endswith(tuple(
                         ['_DS',
                          '_DI',
                          '_AI',
                          '_DPAR',
                          '_WPAR',
                          '_APAR',
                          '_AIR',
                          '_DA',
                          '_AA',
                          '_Analog',
                          '_AnaDO',
                          '_LO',
                          '_OnOff',
                          '_MassFlowController',
                          '_INST',
                          '_WS',
                          '_AS',
                          '_DO',
                          '_AO',
                          '_AOR'])):
                    #sources[program] = program.findall("STSource")[0].text
                    sources[blockName] = program.findall("STSource")[0].text
            self.thePlugin.writeInUABLog("Finder.loadSources_schneider: Sources loaded")
            return sources
        except:
            self.thePlugin.writeWarningInUABLog("Unexpected error:" + str(sys.exc_info()))
            self.thePlugin.writeWarningInUABLog("Finder.loadSources_schneider: Unable to load the sources")
            return {}

    """ this function looks for occurences of the specified instance in code
        
    The arguments are:
    @param instance:        instance to be found
    @param pathToSource:    path to the directory with sources
                            if this path is not specified, generated files are taken

    This function returns list of device names in which files there is reference to instance
    """

    def findUsedIn(self, instance, pathToSource=""):
        if self.isSiemens():
            return self.findUsedIn_siemens(instance, pathToSource)
        elif self.isTia():
            self.thePlugin.writeWarningInUABLog("Finder.findUsedIn not developed for TiaPLC yet. Please contact icecontrols.support@cern.ch for more info.")
            return []
        elif self.isSchneider():
            return self.findUsedIn_schneider(instance, pathToSource)

    """ this function looks for occurences of the specified instance in code
        
    The arguments are:
    @param instancesNames:      list of instances' names

    This function returns list of dictionaries:
    {instanceName:[list of devices that use instance in code]}
    """
    # findUsedIn for multiple instances
    # names are passed only

    def multiFindUsedIn(self, instancesNames, sources):
        if self.isSiemens():
            return self.multiFindUsedIn_siemens(instancesNames, sources)
        elif self.isTia():
            self.thePlugin.writeWarningInUABLog("Finder.multiFindUsedIn not developed for TiaPLC yet. Please contact icecontrols.support@cern.ch for more info.")
            return []
        elif self.isSchneider():
            return self.multiFindUsedIn_schneider(instancesNames, sources)

    """ this function looks for occurences of the specified instance in Siemens code
        code is loaded from specified path (or by default from output directory)
        
    The arguments are:
    @param instance:        instance to be found
    @param pathToSource:    path to the directory with sources
                            if this path is not specified, generated files are taken

    This function returns list of device names in which files there is reference to instance
    """

    def findUsedIn_siemens(self, instance, pathToSource=""):
        try:
            if pathToSource == "":
                pathToSource = AbsolutePathBuilder.getTechnicalPathParameter("S7LogicGenerator:OutputParameters:OutputFolder")
            output = [pathToSource + filename for filename in os.listdir(pathToSource)]
            name = instance.getAttributeData("DeviceIdentification:Name")
            # return [self.getPCONameFromFilename_siemens(file) for file in output if self.grep(file, name)]
            filenames = []
            for file in output:
                # if self.grep(file, name):
                content = self.readfile(file)
                # find whole word using r'\b' word boundary
                match = re.search(r'\b' + name + r'\b', content, re.IGNORECASE)
                if match:
                    #self.thePlugin.writeDebugInUABLog(match.group() + ". Found in " + str(file))
                    filenames.append(file)

            finalset = set()
            for file in filenames:
                if self.getPCONameFromFilename_siemens(file) not in finalset:
                    finalset.add(self.getPCONameFromFilename_siemens(file))

            return list(finalset)
        except Exception, ioe:
            #self.thePlugin.writeWarningInUABLog("Finder.findUsedIn_siemens directory " + pathToSource + " : " + ioe.strerror)
            pass  # don't want to write warning each time look for a parameter in the log
        return list()

    """ this function looks for occurences of the specified instances in Siemens code
        
    The arguments are:
    @param instancesNames:  list of instances names to be found
    @param sources:         dictionary of sources {filenameInLowerCase:ContentOfFile}

    This function returns dictionary in format:
    {instanceName:[list of units that use instance in logic]}
    """

    def multiFindUsedIn_siemens(self, instancesNames, sources):
        try:
            filenames = {}  # dictionary instanceName:[list of files where instance used]
            for name in instancesNames:
                filenames[name] = []    # empty init

            for file in sources:   # go through all the sources (names are in lowercase)
                if file.endswith(".scl"):   # skip symbolic file

                    for name in instancesNames:  # go through all the sources
                        if name.lower() in sources[file].lower():  # pre-check occurence as re.search is slow
                            # find whole word using r'\b' word boundary
                            if re.search(r'\b' + name + r'\b', sources[file], re.IGNORECASE):
                                filenames[name].append(file)    # remember that instance is in this source

            finalSet = {}   # dictionary instanceName[list of places where used]
            for name in instancesNames:
                finalSet[name] = set()
                for file in filenames[name]:
                    if self.getPCONameFromFilename_siemens(file) not in finalSet[name]:
                        finalSet[name].add(self.getPCONameFromFilename_siemens(file))

            return finalSet
        except Exception, ioe:
            #self.thePlugin.writeWarningInUABLog("Finder.findUsedIn_siemens directory " + pathToSource + " : " + ioe.strerror)
            pass  # don't want to write warning each time look for a parameter in the log
        return dict()

    def grep(self, file, pattern):
        ff = open(file)
        for line in open(file):
            if pattern.lower() in line.lower():
                ff.close()
                return True
        ff.close()
        return False

    def getPCONameFromFilename_siemens(self, filename):
        return re.sub(r"_[^_]{2,4}.SCL", "", os.path.basename(filename.upper()))

    """ this function looks for occurences of the specified instance in Schneider code
        code is loaded from specified path (or by default from output directory)
        
    The arguments are:
    @param instance:        instance to be found
    @param pathToSource:    path to the directory with sources
                            if this path is not specified, generated files are taken

    This function returns list of device names in which files there is reference to instance
    """

    def findUsedIn_schneider(self, instance, pathToSource=""):
        try:
            self.initTree_schneider(pathToSource)
            name = instance.getAttributeData("DeviceIdentification:Name")
            output = []
            for program in self.root.findall("program"):
                src = program.findall("STSource")[0].text
                if src.lower().find(name.lower()) >= 0:  # case in-sensitive
                    pins = self.getInstancePins(self.theUnicosProject.findInstanceByName(name))
                    pins = ["_" + pin for pin in pins]
                    pins = [""] + pins
                    for suffix in pins:
                        if re.search(r'\b' + name + suffix + r'\b', src, re.IGNORECASE):
                            blockName = program.findall("identProgram")[0].attrib['name']
                            output.append(blockName)

            myset = set()
            for blockName in output:
                if self.getPCONameFromBlockName_schneider(blockName) not in myset:
                    myset.add(self.getPCONameFromBlockName_schneider(blockName))

            return list(myset)
        except ExpatError:
            self.thePlugin.writeWarningInUABLog("Finder.findUsedIn_schneider xml file " + self.getFile_schneider(pathToSource) + " is badly formatted.")
        except Exception, ioe:
            #self.thePlugin.writeWarningInUABLog("Finder.findUsedIn_schneider xml file " + self.getFile_schneider(pathToSource) + " : " + ioe.strerror)
            pass  # don't want to write warning each time look for a parameter in the log
        return list()

    """ this function looks for occurences of the specified instances in Schneider code
        
    The arguments are:
    @param instancesNames:  list of instances names to be found
    @param sources:         dictionary of sources {unitName:ContentOfFile}

    This function returns dictionary in format:
    {instanceName:[list of units that use instance in logic]}
    """

    def multiFindUsedIn_schneider(self, instancesNames, sources):
        try:

            blocks = {}     # dictionary instanceName:[list of blocks where used]
            for name in instancesNames:
                blocks[name] = []   # init empty

            # for program in sources:    # go through all the sources
            #     src = sources[program]
            #     for name in instancesNames: # go through all instances
            #         if src.lower().find(name.lower()) >= 0: #case in-sensitive  # if there is occurence in block...
            #             if re.search(r'\b' + name + r'\b', src, re.IGNORECASE):
            #                 blockName = program.findall("identProgram")[0].attrib['name']
            #                 blocks[name].append(blockName)      # ...remember block name

            for blockName in sources:    # go through all the sources
                src = sources[blockName]
                for name in instancesNames:  # go through all instances
                    if src.lower().find(name.lower()) >= 0:  # case in-sensitive  # if there is occurence in block...
                        pins = self.getInstancePins(self.theUnicosProject.findInstanceByName(name))
                        pins = ["_" + pin for pin in pins]
                        pins = [""] + pins
                        for suffix in pins:
                            if re.search(r'\b' + name + suffix + r'\b', src, re.IGNORECASE):
                                blocks[name].append(blockName)      # ...remember block name

            finalSet = {}   # dictionary instanceName:[places where used]
            for name in instancesNames:
                finalSet[name] = set()
                for blockName in blocks[name]:
                    if self.getPCONameFromBlockName_schneider(blockName) not in finalSet[name]:
                        finalSet[name].add(self.getPCONameFromBlockName_schneider(blockName))

            return finalSet
        except ExpatError:
            self.thePlugin.writeWarningInUABLog("Finder.multiFindUsedIn_schneider xml file " + self.getFile_schneider() + " is badly formatted.")
        except Exception, ioe:
            #self.thePlugin.writeWarningInUABLog("Finder.findUsedIn_schneider xml file " + self.getFile_schneider() + " : " + ioe.strerror)
            pass  # don't want to write warning each time look for a parameter in the log
        return dict()

    def getPCONameFromBlockName_schneider(self, blockName):
        return re.sub(r'_[^_]{2,4}\Z', "", blockName)

    def initTree_schneider(self, pathToSource=""):
        if self.tree == 0:
            self.tree = ET.parse(self.getFile_schneider(pathToSource))
            self.root = self.tree.getroot()
            self.plcBlocks = map(lambda e: e.attrib['name'], self.tree.getroot().findall("program/identProgram"))
            return self.root

    def getFile_schneider(self, pathToSource=""):
        if pathToSource == "":
            return AbsolutePathBuilder.getTechnicalPathParameter("UnityLogicGenerator:OutputParameters:OutputFolder") + \
                self.thePlugin.getXMLConfig().getTechnicalParameter("UnityLogicGenerator:OutputParameters:OutputFile")
        else:
            return pathToSource

# --------------------------------------------------------
# Functions to find logic assigned to given input pin
# i.e. for code "instanceName.inputPin := XXXXX ;"
# findGivenInput(instance,inputPin) will return XXXXX
# --------------------------------------------------------
    def findGivenInput(self, instance, inputPin, pathToSource=""):
        if self.isSiemens():
            return self.findGivenInput_siemens(instance, inputPin, pathToSource)
        elif self.isTia():
            self.thePlugin.writeWarningInUABLog("Finder.findGivenInput not developed for TiaPLC yet. Please contact icecontrols.support@cern.ch for more info.")
            return ""
        elif self.isSchneider():
            return self.findGivenInput_schneider(instance, inputPin, pathToSource)

    # finds given input for multiple instances
    # instances names are passed
    def multiFindGivenInput(self, instancesNames, inputPin, sources):
        if self.isSiemens():
            return self.multiFindGivenInput_siemens(instancesNames, inputPin, sources)
        elif self.isTia():
            self.thePlugin.writeWarningInUABLog("Finder.multiFindGivenInput not developed for TiaPLC yet. Please contact icecontrols.support@cern.ch for more info.")
            return ""
        elif self.isSchneider():
            return self.multiFindGivenInput_schneider(instancesNames, inputPin, sources)

    def findGivenInput_siemens(self, instance, inputPin, pathToSource=""):
        try:
            if pathToSource == "":
                pathToSource = AbsolutePathBuilder.getTechnicalPathParameter("S7LogicGenerator:OutputParameters:OutputFolder")
            output = [pathToSource + filename for filename in os.listdir(pathToSource)]
            name = instance.getAttributeData("DeviceIdentification:Name")
            for file in output:
                content = self.readfile(file)
                logicString = self.find_assignment(name + "\." + inputPin, content)
                if logicString != "":
                    #self.thePlugin.writeDebugInUABLog(name + "." + inputPin + " := " + logicString)
                    return logicString
            return ""
        except Exception, ioe:
            #self.thePlugin.writeWarningInUABLog("Finder.findGivenInput_siemens directory " + pathToSource + " : " + ioe.strerror)
            pass  # don't want to write warning each time look for a parameter in the log
        return ""

    def multiFindGivenInput_siemens(self, instancesNames, inputPin, sources):
        try:
            outputSet = {}

            for name in instancesNames:  # init empty
                outputSet[name] = ""

            for file in sources:   # go through all the sources
                content = sources[file]
                for name in instancesNames:  # go through all the instances
                    if outputSet[name] == "":       # if instance already has input found, skip it
                        if name.lower() in content.lower():  # pre-check, faster than find_assignment
                            outputSet[name] = self.find_assignment(name + "\." + inputPin, content)

            return outputSet
        except Exception, ioe:
            #self.thePlugin.writeWarningInUABLog("Finder.findGivenInput_siemens directory " + pathToSource + " : " + ioe.strerror)
            pass  # don't want to write warning each time look for a parameter in the log
        return dict()   # empty dict in case of fail!

    def readfile(self, file):
        f = codecs.open(file, "r", "cp1250")
        content = f.read()
        f.close()
        return content

    def find_assignment(self, variable, content):
        match = re.search(variable + "\s*:=([^;]*);", content, re.MULTILINE)
        if match:
            return "\n".join(match.groups())
        else:
            return ""

    def findGivenInput_schneider(self, instance, inputPin, pathToSource=""):
        try:
            self.initTree_schneider(pathToSource)
            name = instance.getAttributeData("DeviceIdentification:Name")
            for program in self.root.findall("program"):
                src = program.findall("STSource")[0].text
                logicString = self.find_assignment(name + "_" + inputPin, src)
                if logicString != "":
                    #self.thePlugin.writeDebugInUABLog(name + "." + inputPin + " := " + logicString)
                    return logicString
            return ""
        except Exception, ioe:
            #self.thePlugin.writeWarningInUABLog("Finder.findGivenInput_siemens directory " + pathToSource + " : " + ioe.strerror)
            pass  # don't want to write warning each time look for a parameter in the log
        return ""

    # finds given input for multiple instances
    def multiFindGivenInput_schneider(self, instancesNames, inputPin, sources):
        try:
            outputSet = {}

            for name in instancesNames:  # init empty
                outputSet[name] = ""

            for program in sources:    # go through all the sources
                src = sources[program]
                for name in instancesNames:  # go through all the instances
                    if outputSet[name] == "":   # if input for isntance already found - skip it
                        if name.lower() in src.lower():  # pre-check, faster than find_assignment
                            outputSet[name] = self.find_assignment(("$name$_$inputPin$"), src)

            return outputSet
        except Exception, ioe:
            #self.thePlugin.writeWarningInUABLog("Finder.findGivenInput_siemens directory " + pathToSource + " : " + ioe.strerror)
            pass  # don't want to write warning each time look for a parameter in the log
        return dict()

    # check if the given alarmType is included in the copy of masterInstance
    def findMasterCopy(self, masterCopyInstance, inputAlarmType):
        # Param4 of alarm copy indicates what types of the alarms should be included in the copy
        Param4 = masterCopyInstance.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter4").lower()
        if Param4 == "fs":
            types = ["fs"]
        elif Param4 == "ts":
            types = ["ts"]
        elif Param4 == "al":
            types = ["al"]
        elif Param4 == "fstsal":
            types = ["fs", "ts", "al"]
        elif Param4 == "all":
            types = ["fs", "ts", "al", "si"]
        else:
            types = ["fs", "ts"]

        # if the type is supported, return the instance of the master copy
        if inputAlarmType in types:
            return masterCopyInstance
        else:
            return None

    # function finds CCC alarm for given inputAlarmName
    # in case of no direct connection in spec (copy of the alarm/copy for the master) sources are analysed
    # for this sources must be set to the sources or the output will be taken if its empty
    # parameter alarmInputs is passed in case of recursive call to increase efficiency
    def findCCCtarget(self, inputAlarmName, sources=None, alarmInputs=None):
        #self.thePlugin.writeWarningInUABLog("Checking alarm " + inputAlarmName)

        inputAlarmInstance = self.theUnicosProject.findInstanceByName(inputAlarmName)
        if (inputAlarmInstance.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter1").lower() == "ccc"):
            # alarm is itself a CCC alarm... so it goes directly to CCC
            return inputAlarmName

        # get the DA instances
        DAInstances = self.theUnicosProject.getDeviceType("DigitalAlarm").getAllDeviceTypeInstances()

        # find all the Copies
        copyAlarmsList = [copy for copy in DAInstances if copy.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter1").lower() == "copy"]   # get all the alarms' copies

        # 1. check if alarm has its own copy
        directCopy = self.find(lambda alarm: alarm.getAttributeData("FEDeviceEnvironmentInputs:Input").lower() == inputAlarmName.lower(), copyAlarmsList)
        if directCopy:
            #self.thePlugin.writeWarningInUABLog("Direct copy ")
            return directCopy.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter2")

        # 2. check if master has a copy including this alarm
        alarmTypes = []     # list of types of the alarm
        alarmMasters = []   # list of masters of the alarm

        if (inputAlarmInstance.getAttributeData("FEDeviceAlarm:Type").lower() == "multiple"):        # in case of multiple alarm
            alarmTypes.extend(inputAlarmInstance.getAttributeData("FEDeviceAlarm:Multiple Types").lower().replace(" ", "").split(','))
        else:
            alarmTypes.append(inputAlarmInstance.getAttributeData("FEDeviceAlarm:Type").lower())     # single-type alarm

        alarmMasters.extend(inputAlarmInstance.getAttributeData("LogicDeviceDefinitions:Master").lower().replace(" ", "").split(','))

        # go through all the pairs type:master and check if for given type, master copy exists
        for (type, master) in zip(alarmTypes, alarmMasters):
            masterCopies = [alarm for alarm in copyAlarmsList if alarm.getAttributeData("FEDeviceEnvironmentInputs:Input").lower() == master.lower()]
            #masterCopy = self.find(lambda alarm: alarm.getAttributeData("FEDeviceEnvironmentInputs:Input").lower() == master.lower(), copyAlarmsList)
            for masterCopy in masterCopies:
                # if masterCopy:
                masterAlarmCopy = self.findMasterCopy(masterCopy, type)
                if masterAlarmCopy:
                    # self.thePlugin.writeWarningInUABLog("masterAlarmCopy")
                    return masterAlarmCopy.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter2")

        # 3. not direct link - there is no direct copy

        # get AA
        AAInstances = self.theUnicosProject.getDeviceType("AnalogAlarm").getAllDeviceTypeInstances()

        # all alarms that are not copies/ccc/spares
        allInstances = [alarm.getAttributeData("DeviceIdentification:Name") for alarm in DAInstances if ((alarm.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter1").lower() not in ["copy", "ccc"]) and (alarm.getAttributeData("DeviceDocumentation:Description").lower() != "spare"))]
        allInstances.extend([alarm.getAttributeData("DeviceIdentification:Name") for alarm in AAInstances if (alarm.getAttributeData("DeviceDocumentation:Description").lower() != "spare")])

        # create dictionary alarm:input based on the sources
        # if it was not passed as an argument, load
        if sources and not alarmInputs:
            alarmInputs = self.multiFindGivenInput(allInstances, "I", sources)

        # recursively look for CCC target of "parent" alarms
        for alarm in alarmInputs:
            #self.thePlugin.writeWarningInUABLog(alarm + " = " + alarmInputs[alarm])
            for (type, master) in zip(alarmTypes, alarmMasters):
                if re.search(r'\b' + inputAlarmName + r'\b', alarmInputs[alarm], re.IGNORECASE) \
                        or (type in ["ts"] and master + ".tstopist" in alarmInputs[alarm].lower()) \
                        or (type in ["fs"] and "not " + master + ".enrstartst" in alarmInputs[alarm].lower()) \
                        or (type in ["fs"] and master + ".fustopist" in alarmInputs[alarm].lower()) \
                        or (type in ["si"] and master + ".startist" in alarmInputs[alarm].lower()) \
                        or (type in ["al"] and master + ".alst" in alarmInputs[alarm].lower()):
                    #self.thePlugin.writeDebugInUABLog("inputAlarmName=" + str(inputAlarmName) + "; alarm = " + str(alarm) + ": calling self.findCCCtarget")
                    parentCCCTarget = self.findCCCtarget(alarm, None, alarmInputs)
                    if parentCCCTarget:
                        # self.thePlugin.writeWarningInUABLog("parentCCCTarget")
                        return parentCCCTarget

        # self.thePlugin.writeWarningInUABLog("empty")
        return ""
        
    def getInstancePins(self, instance):
        ''' Retrieve all posible interface pins for given object. '''
        list_of_pins = []
        for device_type_family in DeviceTypeFactory.getInstance().getDeviceType(instance.getDeviceTypeName()).getAttributeFamily():
            if device_type_family.getAttributeFamilyName() in ["FEDeviceParameters", "FEDeviceInterlocks", "FEDeviceAutoRequests", "FEDeviceManualRequests", "FEDeviceEnvironmentInputs", "FEDeviceOutputs", "FEConfigurationLogicRequest", "FEDeviceAlarm", "FEDeviceVariables"]:
                list_of_pins.extend([attribute.getAttributeName() for attribute in device_type_family.getAttribute()])

        return list_of_pins
