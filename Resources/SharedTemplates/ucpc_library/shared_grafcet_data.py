# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
import openpyxl # Version 1.2.3 used by FlexExtractor. Same one here
import io
import re
import os
from research.ch.cern.unicos.utilities import AbsolutePathBuilder

class GridElement:
    """Class used to represent any element that can be included in a grafcet in the grid"""
    def __init__(self):
        self.posX = 0
        self.posY = 0

class GridStep(GridElement):
    """Class used to represent a Step in the grid"""
    def __init__(self, name):
        GridElement.__init__(self)
        self.name = name
        self.WS_step = 0
        self.WS_pattern = ''
        self.treated = False
        self.located = False

class GridTrans(GridElement):
    """Class used to represent a Transition in the grid"""
    def __init__(self, name):
        GridElement.__init__(self)
        self.name = name
        self.or_step = None
        self.dest_step = None
        self.conn_type = ''
        self.condition = ''
        self.altbranch = None
        self.altbranch_idx = 0

class GridAltBranch(GridElement):
    """Class used to represent an Alternative Branch in the grid"""
    def __init__(self):
        GridElement.__init__(self)
        self.length = 0
        self.group = 0

class GridConn(GridElement):
    """Class used to represent a Connection (vertical line to join other elements) in the grid"""
    def __init__(self):
        GridElement.__init__(self)
        self.length = 0
        self.dest = ''

class Grid:
    """Class used to represent a grid containing all the elements of a grafcet"""
    def __init__(self):
        # Initialize instance attributes
        self.steps = []
        self.trans = []
        self.altbranches = []
        self.conns = []

    def add_step(self, name, WS_step):
        """Function used to insert a Step in the grid

        Sets the value of the Word Status in case it is used
        """
        step = GridStep(name)
        if WS_step:
            step.WS_step = WS_step
        else:
            step.WS_step = len(self.steps) + 1
        self.steps.append(step)

    def add_step_pattern(self, name, WS_step_pattern):
        """Function used to add a value to the Word Status of a Step

        Will be used for the comment generation related to the Word Status of the steps
        """
        for step in self.steps:
            if step.name == name.split('-')[-1]:
                if WS_step_pattern:
                    step.WS_pattern = WS_step_pattern
                else:
                    step.WS_pattern = name.split('-')[-1]

    def add_trans(self, name, cond, or_step, dest_step, WS_trans):
        """Function used to insert a Transition in the grid

        Contains information of the steps that it's connected to and the transition condition logic
        """
        trans = GridTrans(name)
        for step in self.steps:
            if step.name == or_step:
                trans.or_step = step
            if step.name == dest_step:
                trans.dest_step = step
        if cond:
            trans.condition = cond + ";"
        if WS_trans or WS_trans == 0:
            trans.WS_trans = WS_trans
        self.trans.append(trans)

    def move_elements(self):
        """Function used to move the elements already present in the grafcet grid for a proper configuration"""
        for step in self.steps:
            if step.posY > self.posY:
                step.posY += 2
            elif step.posY == self.posY and step.posX < self.posX and step.posX == max([current_step.posX for current_step in self.steps if current_step.posY == self.posY and current_step.posX < self.posX]):
                conn = GridConn()
                conn.posY = step.posY
                conn.posX = step.posX
                conn.length = 1
                conn.dest = "altBranch" if [altbranch for altbranch in self.altbranches if altbranch.posX == step.posX and altbranch.posY == step.posY+1] else "transition" 
                self.conns.append(conn)
        for trans in self.trans:
            if trans.posY > self.posY:
                trans.posY += 2
        for conn in self.conns:
            if conn.posX < self.posX and conn.posY + conn.length > self.posY:
                conn.length += 2
        for altbranch in self.altbranches:
            if altbranch.posY > self.posY:
                altbranch.posY += 2

    def locate_trans(self, all_transitions):
        """Function used to add a series of transitions to the grid

        It returns the next step to treat or None if there are no more steps pending
        """
        # Generate alt branches
        if len(all_transitions) > 1:
            altbranch = GridAltBranch()
            altbranch.posX = self.posX
            altbranch.posY = self.posY + 1
            altbranch.length = len(all_transitions)
            altbranch.group = max([current_altbranch.group for current_altbranch in self.altbranches]) + 1 if len(self.altbranches) else 1
            self.altbranches.append(altbranch)

        # Locate steps and transitions
        for trans_idx, trans in enumerate(all_transitions, 1):
            trans.posX = self.posX
            trans.posY = self.posY + 1
            if not trans.dest_step.located:
                trans.dest_step.posX = self.posX
                trans.dest_step.posY = self.posY+2
                trans.dest_step.located = True
                trans.conn_type = 'n'
            else:
                trans.conn_type = 'j'
            if len(all_transitions) > 1:
                trans.altbranch = altbranch
                trans.altbranch_idx = trans_idx
            self.posX += 1

        # Prepare next iteration and generate connections
        next_steps = [current_step for current_step in self.steps if not current_step.treated and current_step.located]
        if next_steps:
            self.posY = min([current_step.posY for current_step in next_steps])
            self.posX = min([current_step.posX for current_step in next_steps if current_step.posY == self.posY])
            self.move_elements()
            next_step = [current_step for current_step in next_steps if current_step.posY == self.posY and current_step.posX == self.posX][0]
            next_step.treated = True
            return next_step.name
        else:
            return None

    def create_graph(self, init_step):
        """Function used to place in the grid and configure all the elements of the grafcet"""
        self.posX = 0
        self.posY = 0

        # Treat initial step
        for step in self.steps:
            if step.name == init_step:
                step.posX = self.posX
                step.posY = self.posY
                step.conn_type = 'n'
                step.treated = True
                step.located = True

        next_step = init_step
        # Iterate over transitions of the step given as 'next_step'
        while next_step:
            all_transitions = []
            for trans in self.trans:
                if trans.or_step.name == next_step:
                    all_transitions.append(trans)
            next_step = self.locate_trans(all_transitions)

class GraphInstance:
    """Class used to represent an individual grafcet instance"""
    def __init__(self):
        self.DB_name = ''
        self.WS_step = ''
        self.WS_trans = ''
        self.SL_file_name = ''
        self.TL_file_name = ''

class Graph:
    """Class used to represent a grafcet schema (with 1 or multiple instances)"""
    def __init__(self):
        self.out_file_name = ''
        self.FB_name = ''
        self.init_step = ''
        self.graph_instances = {}
        self.grid_instance = None

class DescriptionColumn:
    """Class used to contain the information of each column of the description spreadsheet"""
    def __init__(self, title, individual_data, mandatory_platforms):
        self.title = title
        self.individual_data = individual_data
        self.mandatory_platforms = mandatory_platforms
        self.value = ''

class Variable:
    """Class used to contain the information of a variable obtained from the spec"""
    def __init__(self, name, value, type):
        self.name = name
        self.value = value
        self.type = type

class MissingSheetException(Exception):
    """Class used to raise an exception when a spreadsheet is not in the workbook"""
    pass

class SharedGrafcetData:
    """Class used for checking, storing and retrieving formatted the information contained in a grafcet input file"""
    thePlugin = None
    siemens_PLC = None
    unity_PLC = None
    TIA_PLC = None
    wrong_data = False

    def __init__(self, thePlugin, file_name):
        """The constructor of the class

        It checks the data and extracts it if no problems are found
        """
        self.thePlugin = thePlugin
        self.file_name = file_name
        xml_config = self.thePlugin.getXMLConfig()
        self.siemens_PLC = xml_config.getSiemensPLCDeclarations()
        self.unity_PLC = xml_config.getSchneiderPLCDeclarations()
        self.TIA_PLC = xml_config.getTiaPLCDeclarations()
        self.thePlugin.writeInUABLog("Processing file \"%s\"" % self.file_name)
        self.check_data()
        if not self.wrong_data:
            self.get_data()

    def write_log_error(self, message):
        """Function used to write an error in the log.

        It sets the data of the input file as invalid
        """
        self.thePlugin.writeErrorInUABLog(message)
        self.wrong_data = True

    def write_spreadsheet_log_error(self, message, tabname):
        """Function used to write an error in the log related to a specific spreadsheet"""
        self.write_log_error("Spreadsheet \"%s\": " % tabname + message)

    def write_spreadsheet_log_warning(self, message, tabname):
        """Function used to write a warning in the log related to a specific spreadsheet"""
        self.thePlugin.writeWarningInUABLog("Spreadsheet \"%s\": " % tabname + message)

    def get_workbook(self):
        """Function used to retrieve the workbook object using openpyxl

        Version 1.2.3 is used in UAB. Other versions have been tested outside the framework but are not supported

        It returns the workbook object
        """
        if openpyxl.__version__ == "1.2.3":
            with open(self.file_name, "rb") as f:
                in_mem_file = io.BytesIO(f.read())
            return openpyxl.reader.excel.load_workbook(in_mem_file)
        elif openpyxl.__version__ in ["2.4.9", "2.6.2"]:
            # Versions not included in UAB. Included here only tested versions for debug tests and potential openpyxl updates
            return openpyxl.reader.excel.load_workbook(self.file_name, data_only=True)
        else:
            self.write_log_error("Version of openpyxl not supported: %s" % openpyxl.__version__)
            return None

    def get_spreadsheet_data(self, wb, tabname):
        """Function used to retrieve the data from a spreadsheet of the workbook object obtained from openpyxl

        Version 1.2.3 is used in UAB. Other versions have been tested outside the framework but are not supported

        It returns the spreadhseet content if the spreadsheet exists
        """
        if tabname in wb.get_sheet_names():
            if openpyxl.__version__ == "1.2.3":
                sheet = wb.get_sheet_by_name(tabname)
                return [[cell.value for cell in row] for row in sheet.range(sheet.calculate_dimension())]
            elif openpyxl.__version__ in ["2.4.9", "2.6.2"]:
                # Versions not included in UAB. Included here only tested versions for debug tests and potential openpyxl updates
                sheet = wb[tabname]
                return [list(row) for row in sheet.values]
            else:
                self.write_log_error("Version of openpyxl not supported: %s" % openpyxl.__version__)
                return None
        else:
            return None

    def get_mandatory_spreadsheet_data(self, wb, tabname):
        """Function used to retrieve the data from a spreadsheet that must exist in the workbook object obtained from openpyxl

        It raises a "MissingSheetException" error if the spreadsheet does not exist and it returns the spreadsheet data otherwise
        """
        data = self.get_spreadsheet_data(wb, tabname)
        if not data:
            raise MissingSheetException()
        else:
            return data

    def fill_row_data(self, fields, row, header):
        """Function used to fill the information of a row in the description spreadsheet

        It returns the mandatory individual and collective fields and the name of the spreadsheet of the grafcet
        """
        individual_fields = []
        collective_fields = []
        tabname = ''
        for field in fields:
            if field.title in header:
                # Get values of existent columns in the description spreadsheet
                field_idx = header.index(field.title)
                field.value = row[field_idx]
                if field.title == "Tab Name":
                    tabname = field.value
            if self.thePlugin.getPlcManufacturer().lower() in field.mandatory_platforms:
                # Store mandatory fields
                if field.individual_data:
                    individual_fields.append(field)
                else:
                    collective_fields.append(field)
        return individual_fields, collective_fields, tabname

    def check_mandatory_fields(self, individual_fields, collective_fields, first_row):
        """Function used to check the mandatory fields in description spreadsheet"""
        for individual_field in individual_fields:
            if not individual_field.value:
                self.write_log_error("\"Description\" spreadsheet: %s field is mandatory" % individual_field.title)
        filled_collective_fields = [collective_field for collective_field in collective_fields if collective_field.value]
        if filled_collective_fields != collective_fields and (first_row or len(filled_collective_fields) > 0):
            for collective_field in collective_fields:
                if collective_field not in filled_collective_fields:
                    self.write_log_error("\"Description\" spreadsheet: %s field is mandatory" % collective_field.title)

    @staticmethod
    def is_valid_tabname(tabname):
        """Function used to check if a tabname is valid as to be considered

        It returns True if the tabname is empty (None) or it starts with a "#" (comment) and False otherwise
        """
        return not tabname or not tabname.startswith("#")

    def check_description_spreadsheet(self, content):
        """Function used to check the description spreadsheet of the input file"""
        header = content[0]
        fields = [DescriptionColumn("File Name", False, ["siemens"]),
                              DescriptionColumn("FB Name", False, ["siemens"]),
                              DescriptionColumn("Tab Name", False, ["siemens","schneider"]),
                              DescriptionColumn("Init Step", False, ["siemens","schneider"]),
                              DescriptionColumn("Version", False, ["siemens","schneider"]),
                              DescriptionColumn("PCO Name", True, ["siemens","schneider"]),
                              DescriptionColumn("DB Name", True, ["siemens"]),
                              DescriptionColumn("WS Step", True, []),
                              DescriptionColumn("WS Trans", True, []),
                              DescriptionColumn("SL Logic File", True, []),
                              DescriptionColumn("TL Logic File", True, [])]

        tabname = None
        for row in content[1:]:
            individual_fields, collective_fields, new_tabname = self.fill_row_data(fields, row, header)
            if new_tabname or self.is_valid_tabname(tabname):
                tabname = new_tabname
            if self.is_valid_tabname(tabname):
                self.check_mandatory_fields(individual_fields, collective_fields, row == content[1])


    @staticmethod
    def get_separator_data(data, separator):
        """Function used to separate some data based on the separator provided

        It returns an array containing the first element before the separator and, if existant, the rest of the data
        """
        data_array = data.split(separator)
        if len(data_array) <= 2:
            return data_array
        else:
            return [data_array[0], separator.join(data_array[1:])]

    @staticmethod
    def get_step_data(step):
        """Function used to separate the Word Status of a step (if existent) from the step itself

        It returns an array containing the WS of the step (if explicitly indicated) and the step name
        """
        return SharedGrafcetData.get_separator_data(step, "-")

    @staticmethod
    def get_step_name(step):
        """Function used to get the step name from a step

        It returns the step name
        """
        return SharedGrafcetData.get_step_data(step)[-1]

    @staticmethod
    def get_step_WS(step):
        """Function used to get the word status for a step

        It returns the word status number
        """
        return SharedGrafcetData.get_step_data(step)[0]

    @staticmethod
    def has_WS(step):
        """Function used to check if the step defined has a word status

        It returns True if there is a WS defined and False otherwise
        """
        return "-" in step

    @staticmethod
    def get_transition_data(trans):
        """Function used to separate the Condition variable from the logic of a transition

        It returns an array containing the condition variable and the logic itself (or the part that is available
        """
        return SharedGrafcetData.get_separator_data(trans, ":=")

    @staticmethod
    def get_transition_condition(trans):
        """Function used to get the transition condition variable

        It returns the variable name
        """
        return SharedGrafcetData.get_transition_data(trans)[0]

    @staticmethod
    def get_transition_logic(trans):
        """Function used to get the transition condition logic

        It returns the logic for the transition condition
        """
        return SharedGrafcetData.get_transition_data(trans)[-1]

    @staticmethod
    def has_condition(trans):
        """Function used to check if the transition defined has an explicit condition variable name

        It returns True if an explicit condition is defined and False otherwise
        """
        return ":=" in trans

    @staticmethod
    def check_dest_steps(dest_steps, reachable_steps, step):
        """Function used to add the steps that are directly reachable from step to the reachable_steps list if they are not already there

        It returns True if a step is added and False otherwise
        """
        for dest_step in dest_steps[step]:
            if dest_step not in reachable_steps:
                reachable_steps.append(dest_step)
                return True
        return False

    @staticmethod
    def find_unreachable_step(dest_steps, reachable_steps):
        """Function used to find a step that is not reachable from the initial step

        It returns the step found
        """
        for step in dest_steps.keys():
            if step not in reachable_steps:
                return step

    def check_partial_steps(self, dest_steps, reachable_steps, tabname):
        """Function used to add a new step from a previous step that can be reached from the current step

        It returns True if it adds a new step and False otherwise
        """
        # Find first accessible step from last step
        check_from_step = reachable_steps[-1]
        no_prev_step = False
        while not no_prev_step:
            no_prev_step = True
            for origin_step in reachable_steps[reachable_steps.index(check_from_step):]:
                for reachable_step in reachable_steps:
                    if reachable_steps.index(reachable_step) < reachable_steps.index(check_from_step):
                        # Check if step is reachable from current origin_step
                        if reachable_step in dest_steps[origin_step]:
                            check_from_step = reachable_step
                            no_prev_step = False
                    else:
                        # Iterate from first accessible step
                        if self.check_dest_steps(dest_steps, reachable_steps, reachable_step):
                            return True
        # Check grafcet state
        if len(reachable_steps) < len(dest_steps):
            # Not all steps are available
            self.write_spreadsheet_log_error("Grafcet is not properly configured. Cannot reach step %s from step %s" % (SharedGrafcetData.get_step_name(self.find_unreachable_step(dest_steps, reachable_steps)), SharedGrafcetData.get_step_name(reachable_steps[-1])), tabname)
        elif not check_from_step == reachable_steps[0]:
            # Cannot return to initial step
            self.write_spreadsheet_log_error("Grafcet is not properly configured. Cannot reach step %s from step %s" % (SharedGrafcetData.get_step_name(reachable_steps[0]), SharedGrafcetData.get_step_name(reachable_steps[-1])), tabname)
        return False

    @staticmethod
    def len_without_dollars(expression, prefix = None):
        """Function used to get the len of an expression with replaced variable parameters after removing the (optional) prefix

        It returns the length of the expression without the dollar parameters and (oprtional) prefix
        """
        if prefix:
            expression = expression.replace(prefix + "_", "", 1)
        return len(re.sub("\\\x24.*?\\\x24", "", expression))

    def check_transition_length(self, tabname, content, index, index_2, row_header, column_header, max_length, prefix, has_explicit_condition):
        """Function used to check if the length of a transition condition variable is over the limit of the platform"""
        if has_explicit_condition:
            transition_condition = self.get_transition_condition(content[index][index_2])
            step_message = ": %s" % transition_condition
            condition_len = self.len_without_dollars(transition_condition)
            dollar_in_condition = "\x24" in transition_condition
        else:
            origin_step_name = SharedGrafcetData.get_step_name(row_header[index_2])
            dest_step_name = SharedGrafcetData.get_step_name(column_header[index])
            step_message = " based on steps: %s and %s" % (column_header[index], row_header[index_2])
            if prefix:
                condition_len = self.len_without_dollars(prefix) + self.len_without_dollars(origin_step_name, prefix) + self.len_without_dollars(dest_step_name, prefix) + 2
            else:
                condition_len = self.len_without_dollars(origin_step_name) + self.len_without_dollars(dest_step_name) + 1
            dollar_in_condition = "\x24" in origin_step_name or "\x24" in dest_step_name
        if condition_len > max_length:
            self.write_spreadsheet_log_error("Transition name too long%s" % step_message, tabname)
        elif dollar_in_condition:
            self.write_spreadsheet_log_warning("Transition name contains dollar parameters that could exceed the allowed length (%s)%s" % (max_length, step_message), tabname)

    def check_grafcet_transitions(self, tabname, content, index, index_2, row_header, column_header, transition_name_pattern, max_length, prefix):
        """Function used to check transition conditions format validity"""
        if not re.match(ur"^" + transition_name_pattern + ur"\u0024", content[index][index_2]):
            self.write_spreadsheet_log_error("Wrong transition format: %s" % content[index][index_2], tabname)
        self.check_transition_length(tabname, content, index, index_2, row_header, column_header, max_length, prefix, SharedGrafcetData.has_condition(content[index][index_2]))

    @staticmethod
    def find_prefix(prefix, step):
        """Function used to find a common prefix between the already existent prefix and step (with a "_" symbol after it)

        It returns the new common prefix
        """
        new_prefix = []
        for prefix_part in step.split("_"):
            if prefix.startswith(prefix_part):
                new_prefix.append(prefix_part)
                prefix = "_".join(prefix.split("_")[1:])
            else:
                return "_".join(new_prefix)
        return "_".join(new_prefix)

    def get_max_length(self):
        """Function used to get the maximum length of a variable depending on the platform

        It returns the maximum length allowed
        """
        if self.siemens_PLC or self.TIA_PLC:
            return 24
        elif self.unity_PLC:
            return 32
        else:
            # Set to the maximum of the previous, not relevant
            return 32

    def get_reachable_steps(self, tabname, dest_steps, init_step):
        """Function used to get the directly reachable steps of every step

        It returns the list of reachable steps
        """
        reachable_steps = []
        if init_step in dest_steps.keys():
            reachable_steps.append(init_step)
        else:
            reachable_steps.append(dest_steps.keys()[0])
        for step in dest_steps.items():
            if not step[1]:
                self.write_log_error("Spreadsheet \"%s\": Step %s does not transition to any step" % (tabname, SharedGrafcetData.get_step_name(step[0])))
        return reachable_steps

    def check_grafcet_spreadsheet(self, wb, tabname, init_step):
        """Function used to check the validity of a grafcet spreadsheet"""
        self.thePlugin.writeInUABLog("Checking sheet %s" % tabname)

        # Set max length of variables based on the platform
        max_length = self.get_max_length()

        # Pattern for steps and transitions
        step_name_pattern = "([1-9][0-9]*-)?[a-zA-Z0-9_\\\x24]+"
        transition_name_pattern = "([a-zA-Z0-9_\\\x24]+:=)?[a-zA-Z0-9_.<>=\*/\-\+\\\x24\(\)\[\]\n ]*"

        # Graph spreadsheets check
        content = self.get_mandatory_spreadsheet_data(wb, tabname)
        row_header = content[0]
        column_header = [row[0] for row in content]
        # Check row and column headers
        if row_header != column_header:
            row_header = column_header
            self.write_log_error("Row and column header in \"%s\" spreadsheet do not match" % tabname)
        # Check initial step in headers
        if init_step not in [SharedGrafcetData.get_step_name(header_step) for header_step in row_header[1:]]:
            self.write_log_error("Initial step not present in \"%s\" spreadsheet steps" % tabname)
        # Check steps and transitions format
        transition_amount = 0
        dest_steps = {}
        prefix = "_".join(SharedGrafcetData.get_step_name(row_header[1]).split("_")[:-1])
        for step in row_header[1:]:
            dest_steps[step] = []
            if not SharedGrafcetData.get_step_name(step).startswith(prefix):
                prefix = self.find_prefix(prefix, SharedGrafcetData.get_step_name(step))
        for row_index in range(1, len(content)):
            step = row_header[row_index]
            step_name = SharedGrafcetData.get_step_name(step)
            if content[row_index][row_index]:
                self.write_spreadsheet_log_error("A step cannot transition to itself", tabname)
            if not re.match(ur"^" + step_name_pattern + ur"\u0024", step):
                self.write_spreadsheet_log_error("Wrong step format: %s" % step, tabname)
            if len(re.sub("\\\x24.*?\\\x24", "", step_name)) > max_length:
                self.write_spreadsheet_log_error("Step name too long: %s" % step_name, tabname)
            elif "\x24" in step_name:
                self.write_spreadsheet_log_warning("Step name contains dollar parameters that could exceed the allowed length (%s): %s" % (max_length, step_name), tabname)
            for column_index in range(row_index + 1, len(content)):
                if content[row_index][column_index]:
                    transition_amount += 1
                    dest_steps[column_header[row_index]].append(row_header[column_index])
                    self.check_grafcet_transitions(tabname, content, row_index, column_index, row_header, column_header, transition_name_pattern, max_length, prefix)
                if content[column_index][row_index]:
                    transition_amount += 1
                    dest_steps[column_header[column_index]].append(step)
                    self.check_grafcet_transitions(tabname, content, column_index, row_index, row_header, column_header, transition_name_pattern, max_length, prefix)
        if self.thePlugin.getPlcManufacturer().lower() == "siemens" and transition_amount > 250:
            self.write_spreadsheet_log_error("Too many transitions in grafcet: %s" % transition_amount, tabname)
        self.thePlugin.writeDebugInUABLog("Spreadsheet \"%s\": All steps and their destination: %s" % (tabname, str(dest_steps)))

        reachable_steps = self.get_reachable_steps(tabname, dest_steps, init_step)

        step_added = True
        while step_added:
            step_added = self.check_dest_steps(dest_steps, reachable_steps, reachable_steps[-1])
            if not step_added:
                step_added = self.check_partial_steps(dest_steps, reachable_steps, tabname)
        self.thePlugin.writeDebugInUABLog("Spreadsheet \"%s\": Reachable steps: %s" % (tabname, str(reachable_steps)))

    def check_variable_column_spreadsheet(self, wb, tabname, minimum_column_length):
        """Function used to check that a spreadsheet has a minimum of 2 rows and the columns passed as parameter

        It returns the content of the spreadsheet
        """
        content = self.get_spreadsheet_data(wb, tabname)
        if content and (len(content) < 2 or len(content[0]) < minimum_column_length):
            self.write_spreadsheet_log_error("There need to be at least two rows and %s columns in the spreadsheet" % minimum_column_length, tabname)
        return content

    def check_variables_spreadsheet(self, wb, tabname):
        """Function used to check the "Variables" spreadhseet related to one of the grafcets in the input file"""
        minimum_column_length = 2
        content = self.check_variable_column_spreadsheet(wb, tabname, minimum_column_length)
        if content:
            variable_names = []
            for row in content[1:]:
                if not row[0] or not row[1]:
                    self.write_spreadsheet_log_error("The name or value is empty in row %s" % (content.index(row) + 1), tabname)
                else:
                    if row[0] not in variable_names:
                        variable_names.append(row[0])
                    else:
                        self.write_spreadsheet_log_error("The variable %s is repeated" % row[0], tabname)
                    if not ("\x24" in row[0]) and (len(row) < 3 or not row[2]):
                        self.write_spreadsheet_log_warning("The variable %s does not have a type in row %s. Bool will be used by default" % (row[0], content.index(row) + 1), tabname)

    def check_pattern_spreadsheet(self, wb, tabname):
        """Function used to check the "Pattern" spreadsheet related to one of the grafcets in the input file"""
        minimum_column_length = 1
        content = self.check_variable_column_spreadsheet(wb, tabname, minimum_column_length)
        if content:
            for row in content[1:]:
                if not row[0]:
                    self.write_spreadsheet_log_error("The name is empty in row %s" % (content.index(row) + 1), tabname)
                elif len(row) < 2 or not row[1]:
                    self.write_spreadsheet_log_warning("The step %s does not have a value for the WS pattern generation in row %s. The step name will be used instead" % (row[0], content.index(row) + 1), tabname)

    def check_data(self):
        """Function used to check the data contained in the input file

        It raises an IOError if the file does not exist, a BadZipfile error if it does not have the correct format and a MissingSheetException if a mandatory spreadsheet is missing
        """
        import zipfile
        try:
            wb = self.get_workbook()
            tabname = "Description"
            description_content = self.get_mandatory_spreadsheet_data(wb, tabname)

            self.check_description_spreadsheet(description_content)

            description_header = description_content[0]
            for description_row in description_content[1:]:
                tabname = description_row[description_header.index("Tab Name")] if "Tab Name" in description_header else ''
                init_step = description_row[description_header.index("Init Step")] if "Init Step" in description_header else ''
                if tabname and not tabname.startswith("#"):
                    self.check_grafcet_spreadsheet(wb, tabname, init_step)
                    self.check_variables_spreadsheet(wb, tabname + "_Variables")
                    self.check_pattern_spreadsheet(wb, tabname + "_Pattern")

        except IOError:
            self.write_log_error("File \"%s\" does not exist" % self.file_name)
        except zipfile.BadZipfile:
            self.write_log_error("File \"%s\" does not have the correct format" % self.file_name)
        except MissingSheetException:
            self.write_log_error("\"%s\" spreadsheet not found in file" % tabname)

    def get_raw_data(self, wb):
        """Function used to obtain the raw data from the input file

        It returns the description sheet data and all the grafcet, grafcet variables and grafcet pattern spreadsheets data
        """
        tabname = "Description"
        description_content = self.get_spreadsheet_data(wb, tabname)

        self.graphs = {}
        graph_content = {}
        graph_vars_content = {}
        graph_pattern_content = {}
        description_header = description_content[0]
        description_tabname_idx = description_header.index("Tab Name")
        for description_row in description_content[1:]:
            tabname = description_row[description_tabname_idx]
            if tabname and not tabname.startswith("#"):
                self.graphs[tabname] = Graph()
                graph_content[tabname] = self.get_spreadsheet_data(wb, tabname)
                graph_vars_content[tabname] = self.get_spreadsheet_data(wb, tabname + "_Variables")
                graph_pattern_content[tabname] = self.get_spreadsheet_data(wb, tabname + "_Pattern")
        return description_content, graph_content, graph_vars_content, graph_pattern_content

    @staticmethod
    def extract_description_tab(content, graph_name, graph):
        """Function used to obtain the data from the "Description" spreadsheet of the input file"""
        header = content[0]
        tabname_idx = header.index("Tab Name")
        table = [row for row in content[1:]]

        row_idx = [row_idx for row_idx in range(len(table)) if table[row_idx][tabname_idx] == graph_name][0]
        graph.out_file_name = table[row_idx][header.index("File Name")]
        graph.FB_name = table[row_idx][header.index("FB Name")]
        graph.init_step = table[row_idx][header.index("Init Step")]
        graph.graph_instances = {}
        graph_instances = graph.graph_instances
        while row_idx < len(table) and (table[row_idx][tabname_idx] == graph_name or not table[row_idx][tabname_idx]):
            PCO_name = table[row_idx][header.index("PCO Name")]
            graph_instances[PCO_name] = GraphInstance()
            graph_instances[PCO_name].DB_name = table[row_idx][header.index("DB Name")]
            graph_instances[PCO_name].WS_step = table[row_idx][header.index("WS Step")]
            graph_instances[PCO_name].WS_trans = table[row_idx][header.index("WS Trans")]
            graph_instances[PCO_name].SL_file_name = table[row_idx][header.index("SL Logic File")]
            graph_instances[PCO_name].TL_file_name = table[row_idx][header.index("TL Logic File")]
            row_idx += 1

    @classmethod
    def extract_transition_tab(cls, content, grid_instance):
        """Function used to obtain the data from one of the grafcet spreadsheets of the input file"""
        steps_content = [row[0] for row in content[1:]]
        dest_steps = content[0][1:]
        table = [row[1:] for row in content[1:]]
        conditions = []
        prefix = "_".join(SharedGrafcetData.get_step_name(steps_content[0]).split("_")[:-1])
        for step in steps_content:
            if SharedGrafcetData.has_WS(step):
                step_WS, step = SharedGrafcetData.get_step_data(step)
            else:
                step_WS = None
            if not step.startswith(prefix):
                prefix = cls.find_prefix(prefix, step)
            grid_instance.add_step(step, step_WS)
        for step_idx, step in enumerate(steps_content):
            step = SharedGrafcetData.get_step_name(step)
            for dest_step_idx, dest_step in enumerate(dest_steps):
                dest = table[step_idx][dest_step_idx]
                if dest:
                    trans_dest_step = SharedGrafcetData.get_step_name(dest_step)
                    if SharedGrafcetData.has_condition(dest):
                        trans_var, trans_cond = SharedGrafcetData.get_transition_data(dest)
                    else:
                        if prefix:
                            trans_var = prefix + "".join([step.replace(prefix,"",1),trans_dest_step.replace(prefix,"",1)])
                        else:
                            trans_var = "_".join([step, trans_dest_step])
                        trans_cond = dest
                    if trans_var not in conditions:
                        trans_WS = len(conditions)
                        conditions.append(trans_var)
                    else:
                        trans_WS = None
                    grid_instance.add_trans(trans_var, trans_cond, step, trans_dest_step, trans_WS)

    @staticmethod
    def extract_variables_tab(content, graph):
        """Function used to obtain the data from one of the grafcet variables spreadsheets of the input file"""
        graph.template_vars = []
        graph.code_vars = []
        if content:
            table = [row for row in content[1:]]
            for row in table:
                name = row[0]
                value = str(row[1])
                type = 'Bool'
                if not len(row) < 3 and row[2]:
                    type = str(row[2]).capitalize()
                if '\x24' in name:
                    graph.template_vars.append(name[1:-1] + " = \"" + value + "\"")
                else:
                    graph.code_vars.append(Variable(name, value, type))

    @staticmethod
    def extract_pattern_tab(content, grid_instance):
        """Function used to obtain the data from one of the grafcet pattern spreadsheets of the input file"""
        if content:
            pattern_table = [row for row in content[1:]]
            for pattern_row in pattern_table:
                WS_step_name = pattern_row[0]
                if len(pattern_row) > 1:
                    WS_step_pattern = pattern_row[1]
                else:
                    WS_step_pattern = ''
                grid_instance.add_step_pattern(WS_step_name, WS_step_pattern)

    def get_data(self):
        """Function used to extract the data from the input file"""
        wb = self.get_workbook()
        description_content, graph_content, graph_vars_content, graph_pattern_content = self.get_raw_data(wb)
        self.all_data = {}
        for graph_name, transition_content in graph_content.items():
            graph = self.graphs[graph_name]
            graph.grid_instance = Grid()
            grid_instance = graph.grid_instance
            self.extract_description_tab(description_content, graph_name, graph)
            self.extract_transition_tab(transition_content, grid_instance)
            self.extract_variables_tab(graph_vars_content[graph_name], graph)
            self.extract_pattern_tab(graph_pattern_content[graph_name], grid_instance)

    def get_grid(self, tab_name):
        """Function used to get the grid associated to a spreadsheet"""
        graph_instance = self.graphs[tab_name]
        grid_instance = graph_instance.grid_instance
        grid_instance.create_graph(graph_instance.init_step)

    def get_graph_tab_name(self, PCO_name):
        """Function used to get the Graph object associated to a PCO name

        It returns the Graph object if found
        """
        for graph_name, graph in self.graphs.items():
            for instance_PCO_name in graph.graph_instances:
                if instance_PCO_name == PCO_name:
                    return graph_name
        return None

    def get_PCO_list(self):
        """Function used to get a list of the PCOs that have been treated from the input file

        It return the list of PCO names
        """
        PCO_list = []
        for graph in self.graphs.values():
            PCO_list.extend(graph.graph_instances.keys())
        return PCO_list

    def get_logic_file(self, PCO_name, logic_file_name):
        """Function used to get the logic file name that should be used as the input for the generation

        It returns the file to be used if the proper logic is selected (an empty string if no file is specified) and None otherwise
        """
        graph_name = self.get_graph_tab_name(PCO_name)
        if graph_name:
            graph = self.graphs[graph_name]
            if logic_file_name == "SL":
                return graph.graph_instances[PCO_name].SL_file_name
            elif logic_file_name == "TL":
                return graph.graph_instances[PCO_name].TL_file_name
            else:
                self.thePlugin.writeErrorInUABLog("Logic file not existent for this generation")
                return None

    def replace_values(self, text, vars, DB_name):
        """Function used to decorate the grafcet variables present in a text

        It returns the same text with the values decorated (DB of the grafcet followed by a dot and the variable)"""
        var_pattern = "((?<=[\(<>=\*/\-\+\[\n\s])|^)%s(?=[\)<>=\*/\-\+\]\n\s]?)"
        for var in vars:
            text = re.sub(var_pattern % var, DB_name + "." + var, text)
        return text

    def get_graph_file(self, PCO_name):
        """Function used to get the grafcet file for the PCO PCO_name

        It returns the file name and its content (grafcet instance and variables separated in the case of Schneider)
        """
        self.thePlugin.writeInUABLog("Getting graph file for PCO \"%s\"" % PCO_name)
        graph_name = self.get_graph_tab_name(PCO_name)
        if graph_name:
            graph = self.graphs[graph_name]
            if 'posX' not in dir(graph.grid_instance):
                self.get_grid(graph_name)
            grid = graph.grid_instance

            if self.siemens_PLC:
                params = {'FB_name': graph.FB_name,
                          'vars_input': '',
                          'vars_static': '',
                          'grafcet_body': ''}

                step_txt = ''
                for step in grid.steps:
                    if step.name != graph.init_step:
                        step_txt += '''\n\nSTEP %(step_name)s (*\x24_NUM %(step_WS)s*):
(*\x24_COM Step comment*)

END_STEP ''' % {'step_name': step.name, 'step_WS': step.WS_step}
                    else:
                        init_step_num = step.WS_step
                step_txt = '''INITIAL_STEP %(init_step)s (*\x24_NUM %(step_num)s*):
(*\x24_COM Step comment*)

END_STEP''' % {'init_step': graph.init_step, 'step_num': init_step_num} + step_txt

                transition_txt = '\n'
                for trans_idx, trans in reversed(list(enumerate(grid.trans, 1))):
                    jump_txt = ' (*\x24_JUMP*)' if trans.conn_type =='j' else ''
        
                    transition_txt += '''
TRANSITION Trans%(trans_idx)s (*\x24_NUM %(trans_idx)s*)
  FROM %(or_step)s
  TO %(dest_step)s%(jump_txt)s
CONDITION := %(trans_name)s 
END_TRANSITION 
''' % {'trans_idx': trans_idx, 'or_step': trans.or_step.name, 'dest_step': trans.dest_step.name, 'jump_txt': jump_txt, 'trans_name': trans.name}
                    if 'WS_trans' in dir(trans):
                        params['vars_input'] += '''    %(trans_name)s   : Bool := FALSE;\n''' % {'trans_name': trans.name}

                code_vars = self.get_code_variables_declaration(PCO_name)
                if code_vars:
                    params['vars_static'] = '\n'.join(['VAR', code_vars, 'END_VAR'])

                params['grafcet_body'] = step_txt + transition_txt

                grafcet_output = open(os.path.join(AbsolutePathBuilder.getApplicationPathParameter("GeneralData:SharedTemplatesFolder"), "ucpc_library", "siemens_grafcet_template.GR7")).read()
                for param in params.keys():
                    grafcet_output = grafcet_output.replace(chr(36) + param + chr(36), params[param])

                return "%s.GR7" % graph.out_file_name, grafcet_output

            elif self.unity_PLC:
                step_txt = '''                <step stepType="initialStep" stepName="%(init_step)s">
                    <objPosition posX="0" posY="0"></objPosition>
                    <literals max="" min="" delay=""></literals>
                </step>\n''' % {'init_step': graph.init_step}
                jump_txt = ''
                transition_txt = ''
                alt_branch_txt = ''
                link_txt = ''
                variables = ''

                for step in grid.steps:
                    if step.name != graph.init_step:
                        step_txt += '''                <step stepType="step" stepName="%(step_name)s">
                    <objPosition posX="%(posX)s" posY="%(posY)s"></objPosition>
                    <literals max="" min="" delay=""></literals>
                </step>\n''' % {'step_name': step.name, 'posX': step.posX, 'posY': step.posY}
                for trans in grid.trans:
                    transition_txt += '''                <transition>
                    <objPosition posX="%(posX)s" posY="%(posY)s"></objPosition>
                    <transitionCondition invertLogic="false">
                        <variableName>%(trans_name)s</variableName>
                    </transitionCondition>
                </transition>\n''' % {'trans_name': trans.name, 'posX': trans.posX, 'posY': trans.posY}
                    if trans.conn_type == 'j':
                        jump_txt += '''                <jumpSFC stepName="%(dest_step)s">
                    <objPosition posX="%(posX)s" posY="%(posY)s"></objPosition>
                </jumpSFC>\n''' % {'dest_step': trans.dest_step.name, 'posX': trans.posX, 'posY': trans.posY+1}
                    if 'WS_trans' in dir(trans):
                        variables += '''<variables name="%(trans_name)s" typeName="BOOL"></variables>\n''' % {'trans_name': trans.name}
                for alt_branch in grid.altbranches:
                    alt_branch_txt += '''                <altBranch width="%(length)s" relativePos="0">
                    <objPosition posX="%(posX)s" posY="%(posY)s"></objPosition>
                </altBranch>\n''' % {'length': alt_branch.length, 'posX': alt_branch.posX, 'posY': alt_branch.posY}
                for conn in grid.conns:
                    link_txt += '''                <linkSFC>
                    <directedLinkSource objectType="step">
                        <objPosition posX="%(posX)s" posY="%(posY)s"></objPosition>
                    </directedLinkSource>
                    <directedLinkDestination objectType="%(dest)s">
                        <objPosition posX="%(posX)s" posY="%(dest_posY)s"></objPosition>
                    </directedLinkDestination>
                </linkSFC>\n''' % {'posX': conn.posX, 'posY': conn.posY, 'dest': conn.dest, 'dest_posY': conn.posY + conn.length}

                # Write text file
                return "%s.xsf" % graph.out_file_name, [step_txt + jump_txt + transition_txt + alt_branch_txt + link_txt, variables]

            elif self.TIA_PLC:
                params = {'FB_name': graph.FB_name,
                          'vars_input': '',
                          'vars_static': '',
                          'grafcet_body': ''}

                step_txt = ''
                transition_txt = '    <Transitions>\n'
                alt_branch_txt = '    <Branches>\n'
                connections_txt = '    <Connections>\n'

                for trans_idx, trans in enumerate(grid.trans, 1):
                    if 'WS_trans' in dir(trans):
                        params['vars_input'] += '    <Member Name="%(trans_name)s" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList></Member>\n'\
                                                % {'trans_name': trans.name}
                    params['vars_static'] += '    <Member Name="Trans%(trans_idx)s" Datatype="G7_TransitionPlus_V2" Version="1.0" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">false</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Transition structure</MultiLanguageText></Comment><Sections><Section Name="None"><Member Name="TV" Datatype="Bool" /><Member Name="TT" Datatype="Bool" /><Member Name="TS" Datatype="Bool" /><Member Name="TNO" Datatype="Int"><StartValue Informative="true">%(trans_idx)s</StartValue></Member></Section></Sections></Member>\n'\
                                             % {'trans_idx': trans_idx}
                    transition_txt += '''      <Transition IsMissing="false" Name="Trans%(trans_idx)s" Number="%(trans_idx)s" ProgrammingLanguage="LAD">
                            <FlgNet>
                              <Parts>
                                <Access Scope="LocalVariable" UId="21">
                                  <Symbol>
                                    <Component Name="%(trans_name)s" />
                                  </Symbol>
                                </Access>
                                <Part Name="Contact" UId="22" />
                                <Part Name="TrCoil" UId="23" />
                              </Parts>
                              <Wires>
                                <Wire UId="24">
                                  <Powerrail />
                                  <NameCon UId="22" Name="in" />
                                </Wire>
                                <Wire UId="25">
                                  <IdentCon UId="21" />
                                  <NameCon UId="22" Name="operand" />
                                </Wire>
                                <Wire UId="26">
                                  <NameCon UId="22" Name="out" />
                                  <NameCon UId="23" Name="in" />
                                </Wire>
                              </Wires>
                            </FlgNet>
                          </Transition>\n''' % {'trans_idx': trans_idx, 'trans_name': trans.name}
                    if not trans.altbranch:  # Only one transition
                        connections_txt += '''      <Connection>
                            <NodeFrom>
                              <StepRef Number="%(or_step_WS)s" />
                            </NodeFrom>
                            <NodeTo>
                              <TransitionRef Number="%(trans_idx)s" />
                            </NodeTo>
                            <LinkType>Direct</LinkType>
                          </Connection>\n''' % {'or_step_WS': trans.or_step.WS_step, 'trans_idx': trans_idx}
                    else:
                        if trans.altbranch_idx == 1:  # New alternative branch
                            connections_txt += '''      <Connection>
                            <NodeFrom>
                              <StepRef Number="%(or_step_WS)s" />
                            </NodeFrom>
                            <NodeTo>
                              <BranchRef Number="%(alt_branch_group)s" In="0" />
                            </NodeTo>
                            <LinkType>Direct</LinkType>
                          </Connection>\n''' % {'or_step_WS': trans.or_step.WS_step,
                                                'alt_branch_group': trans.altbranch.group}
                        connections_txt += '''      <Connection>
                            <NodeFrom>
                              <BranchRef Number="%(alt_branch_group)s" Out="%(alt_branch_idx)s" />
                            </NodeFrom>
                            <NodeTo>
                              <TransitionRef Number="%(trans_idx)s" />
                            </NodeTo>
                            <LinkType>Direct</LinkType>
                          </Connection>\n''' % {'alt_branch_group': trans.altbranch.group,
                                                'alt_branch_idx': trans.altbranch_idx - 1,
                                                'trans_idx': trans_idx}
                    jump_txt = 'Direct' if trans.conn_type == 'n' else 'Jump'
                    connections_txt += '''      <Connection>
                            <NodeFrom>
                              <TransitionRef Number="%(trans_idx)s" />
                            </NodeFrom>
                            <NodeTo>
                              <StepRef Number="%(dest_step_WS)s" />
                            </NodeTo>
                            <LinkType>%(jump_txt)s</LinkType>
                          </Connection>\n''' % {'trans_idx': trans_idx, 'dest_step_WS': trans.dest_step.WS_step, 'jump_txt': jump_txt}

                for step in grid.steps:
                    params['vars_static'] += '    <Member Name="%(step_name)s" Datatype="G7_StepPlus_V2" Version="1.0" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">false</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Step structure</MultiLanguageText></Comment><Sections><Section Name="None"><Member Name="S1" Datatype="Bool" /><Member Name="L1" Datatype="Bool" /><Member Name="V1" Datatype="Bool" /><Member Name="R1" Datatype="Bool" /><Member Name="A1" Datatype="Bool" /><Member Name="S0" Datatype="Bool" /><Member Name="L0" Datatype="Bool" /><Member Name="V0" Datatype="Bool" /><Member Name="X" Datatype="Bool" /><Member Name="LA" Datatype="Bool" /><Member Name="VA" Datatype="Bool" /><Member Name="RA" Datatype="Bool" /><Member Name="AA" Datatype="Bool" /><Member Name="SS" Datatype="Bool" /><Member Name="LS" Datatype="Bool" /><Member Name="VS" Datatype="Bool" /><Member Name="SNO" Datatype="Int"><StartValue Informative="true">%(step_WS)s</StartValue></Member><Member Name="T" Datatype="Time" /><Member Name="U" Datatype="Time" /><Member Name="T_MAX" Datatype="Time"><StartValue Informative="true">T#10S</StartValue></Member><Member Name="T_WARN" Datatype="Time"><StartValue Informative="true">T#7S</StartValue></Member><Member Name="SM" Datatype="Bool" /><Member Name="H_IL_ERR" Datatype="Byte" /><Member Name="H_SV_FLT" Datatype="Byte" /></Section></Sections></Member>\n' % {'step_name': step.name, 'step_WS': step.WS_step}
                    current_step_txt = '''      <Step Number="%(step_WS)s" Init="%(init_step)s" Name="%(step_name)s" MaximumStepTime="t#10s" WarningTime="t#7s">
        <Actions>
          <Action />
        </Actions>
        <Supervisions>
          <Supervision ProgrammingLanguage="LAD">
            <FlgNet>
              <Parts>
                <Part Name="SvCoil" UId="21" />
              </Parts>
              <Wires>
                <Wire UId="22">
                  <Powerrail />
                  <NameCon UId="21" Name="in" />
                </Wire>
              </Wires>
            </FlgNet>
          </Supervision>
        </Supervisions>
        <Interlocks>
          <Interlock ProgrammingLanguage="LAD">
            <FlgNet>
              <Parts>
                <Part Name="IlCoil" UId="21" />
              </Parts>
              <Wires>
                <Wire UId="22">
                  <Powerrail />
                  <NameCon UId="21" Name="in" />
                </Wire>
              </Wires>
            </FlgNet>
          </Interlock>
        </Interlocks>
      </Step>\n''' % {'step_WS': step.WS_step,
                      'init_step': 'true' if step.name == graph.init_step else 'false',
                      'step_name': step.name}
                    if step.name != graph.init_step:
                        step_txt += current_step_txt
                    else:
                        step_txt = '    <Steps>\n' + current_step_txt + step_txt

                for alt_branch in grid.altbranches:
                    alt_branch_txt += '      <Branch Number="%(alt_branch_group)s" Type="AltBegin" Cardinality="%(length)s" />\n' % {'alt_branch_group': alt_branch.group, 'length': alt_branch.length}

                params['vars_static'] += self.get_code_variables_declaration(PCO_name)

                step_txt += '    </Steps>\n'
                transition_txt += '    </Transitions>\n'
                alt_branch_txt += '    </Branches>\n'
                connections_txt += '    </Connections>\n'

                params['grafcet_body'] = step_txt + transition_txt + alt_branch_txt + connections_txt

                grafcet_output = open(os.path.join(AbsolutePathBuilder.getApplicationPathParameter("GeneralData:SharedTemplatesFolder"), "ucpc_library", "tia_grafcet_template.xml")).read()
                for param in params.keys():
                    grafcet_output = grafcet_output.replace(chr(36) + param + chr(36), params[param])

                # Write text file
                return "%s.xml" % graph.out_file_name, grafcet_output

            else:
                self.thePlugin.writeErrorInUABLog("Platform not supported")

    def write_graph_file(self, PCO_name, folder_name=""):
        """Function used to write the grafcet file related to PCO PCO_name

        The optional parameter folder_name allows to write the file to a specific folder
        """
        file_name, content = self.get_graph_file(PCO_name)
        if self.siemens_PLC or self.TIA_PLC:
            self.thePlugin.writeFile(folder_name + file_name, content)
        elif self.unity_PLC:
            self.thePlugin.writeProgram(content[0])
            self.thePlugin.writeVariable(content[1])
        else:
            self.thePlugin.writeErrorInUABLog("Platform not supported")

    def get_DB_instance(self, PCO_name):
        """Function used to get the DB instance of the grafcet associated to PCO_name

        TIA Portal specific

        It returns the instance data block
        """
        self.thePlugin.writeInUABLog("Getting instance DB file")
        graph_name = self.get_graph_tab_name(PCO_name)
        if graph_name:
            graph = self.graphs[graph_name]
            if self.TIA_PLC:
                return '''DATA_BLOCK "%s"
{ S7_Optimized_Access := 'TRUE' }
"%s"
BEGIN
END_DATA_BLOCK\n\n''' % (graph.graph_instances[PCO_name].DB_name, graph.FB_name)
            else:
                self.thePlugin.writeErrorInUABLog("Platform not supported")

    def write_DB_instances(self, folder_name=""):
        """Function used to write the DB instances of all the grafcets

        TIA Portal specific

        The optional parameter folder_name allows to write the file to a specific folder
        """
        if self.TIA_PLC:
            instances_DB = ""
            for PCO_name in self.get_PCO_list():
                instances_DB += self.get_DB_instance(PCO_name)
            self.thePlugin.writeFile(folder_name + "instances_DB.scl", instances_DB.replace("\n", "\r\n"))

    def write_logic(self, logic_section):
        """Function used to write a grafcet logic depending on the platform"""
        if self.siemens_PLC:
            self.thePlugin.writeSiemensLogic(logic_section)
        elif self.TIA_PLC:
            self.thePlugin.writeTIALogic(logic_section)
        elif self.unity_PLC:
            self.thePlugin.writeProgram(logic_section)
        else:
            self.thePlugin.writeErrorInUABLog("Platform not supported")
        
    def get_graph_call(self, PCO_name):
        """Function used to get the grafcet call for PCO_name

        Siemens platforms specific

        It returns the grafcet instance call
        """
        self.thePlugin.writeInUABLog("Getting graph call for PCO \"%s\"" % PCO_name)
        graph_name = self.get_graph_tab_name(PCO_name)
        if graph_name:
            graph = self.graphs[graph_name]
            if self.siemens_PLC:
                return graph.FB_name + "." + graph.graph_instances[PCO_name].DB_name + "();\n"
            elif self.TIA_PLC:
                return "\"" + graph.graph_instances[PCO_name].DB_name + "\"();\n"
            else:
                self.thePlugin.writeErrorInUABLog("Platform not supported")
                return ""

    def write_graph_call(self, PCO_name):
        """Function used to write the grafcet call to a logic file

        Siemens platforms specific
        """
        # Explicitly disable unity generation
        if self.unity_PLC:
            self.thePlugin.writeErrorInUABLog("Platform not supported")
        else:
            self.write_logic("\n    " + self.get_graph_call(PCO_name))

    def get_transition_conditions(self, PCO_name):
        """Function used to get the transition condition's logic

        It returns the logic for the conditions
        """
        self.thePlugin.writeInUABLog("Getting transition conditions for PCO \"%s\"" % PCO_name)
        graph_name = self.get_graph_tab_name(PCO_name)
        if graph_name:
            graph = self.graphs[graph_name]
            grid = graph.grid_instance
            conditions_txt = "(* TRANSITIONS COMPUTATION *)\n\n"
            var_names = [var.name for var in graph.code_vars]
            for trans in grid.trans:
                if 'WS_trans' in dir(trans):
                    transition_numbers = "(* %s TO %s *)" % (trans.or_step.WS_step, trans.dest_step.WS_step)
                    if self.siemens_PLC or self.TIA_PLC:
                        if not trans.condition:
                            trans.condition = "0; // To complete"
                        conditions_txt += '''%(transition_numbers)s %(DB_name)s.%(trans_name)s := %(condition)s\n'''\
                                          % {'transition_numbers': transition_numbers,
                                             'DB_name': graph.graph_instances[PCO_name].DB_name,
                                             'trans_name': trans.name,
                                             'condition': self.replace_values(
                                                 trans.condition, var_names, graph.graph_instances[PCO_name].DB_name)}
                    elif self.unity_PLC:
                        if not trans.condition:
                            trans.condition = "0; (* To complete *)"
                        conditions_txt += '''%(transition_numbers)s %(trans_name)s := %(condition)s\n'''\
                                          % {'transition_numbers': transition_numbers,
                                             'trans_name': trans.name,
                                             'condition': trans.condition}
                    else:
                        self.thePlugin.writeErrorInUABLog("Platform not supported")
            return conditions_txt

    def write_transition_conditions(self, PCO_name):
        """Function used to write the conditions logic to a logic file"""
        self.write_logic('\n\n' + self.get_transition_conditions(PCO_name))

    def get_word_status_steps(self, PCO_name):
        """Function used to get the logic for the word status related to the steps

        It returns the word status assignment
        """
        self.thePlugin.writeInUABLog("Getting steps word status for PCO \"%s\"" % PCO_name)
        graph_name = self.get_graph_tab_name(PCO_name)
        if graph_name:
            graph = self.graphs[graph_name]
            grid = graph.grid_instance
            WS_step = graph.graph_instances[PCO_name].WS_step
            if WS_step:
                word_status_step_txt = '(* WORD STATUS FOR STEPS *)\n'
                if self.siemens_PLC:
                    word_status_step_txt += 'DB_WS_ALL.WS_SET.%s.AuPosR := INT_TO_WORD(%s.S_NO);\n' % (WS_step, graph.graph_instances[PCO_name].DB_name)
                elif self.TIA_PLC:
                    word_status_step_txt += '%s.AuPosR := INT_TO_WORD(%s.S_NO);\n' % (WS_step, graph.graph_instances[PCO_name].DB_name)
                elif self.unity_PLC:
                    word_status_step_txt += '''IF %s.X THEN
    %s_AuPosR := %s;
''' % (grid.steps[0].name, WS_step, grid.steps[0].WS_step)
                    for step in graph.grid_instance.steps[1:]:
                        word_status_step_txt += '''ELSIF %s.X THEN
    %s_AuPosR := %s;
''' % (step.name, WS_step, step.WS_step)
                    word_status_step_txt += 'END_IF;\n'
                else:
                    self.thePlugin.writeErrorInUABLog("Platform not supported")
                return word_status_step_txt
            else:
                return None

    def write_word_status_steps(self, PCO_name):
        """Function used to write the word status assignment to a logic file"""
        self.write_logic('\n\n' + self.get_word_status_steps(PCO_name))

    @staticmethod
    def get_siemens_word_status_transitions(conditions, WS_trans, DB_name):
        """Function used to get the logic for the word status related to the transitions

        Siemens specific

        It returns the word status assignments
        """
        trans_cond_txt = ""
        word_status_trans_assignement = ""
        word_status_trans_txt = '(* WORD STATUS FOR TRANSITIONS *)\n'
        for trans in conditions:
            if not trans.WS_trans % 16:
                trans_cond_txt += '''    pco_transitions%i : WORD;
    pco_transitions%i_bit AT pco_transitions%i: ARRAY [0..15] OF BOOL;
''' % (trans.WS_trans / 16, trans.WS_trans / 16, trans.WS_trans / 16)
                word_status_trans_assignement += 'DB_WS_ALL.WS_SET.%s.AuPosR := pco_transitions%i;\n' % (WS_trans.replace('#', str(trans.WS_trans / 16)), trans.WS_trans / 16)
            word_status_trans_txt += 'pco_transitions%i_bit%-5s:= %-80s (*%2i*)\n' % (trans.WS_trans / 16, '[%i]' % ((trans.WS_trans + 8) % 16), '%s.%s;' % (DB_name, trans.name), trans.WS_trans % 16)
        if len(conditions) % 16:
            for remaining_WS_idx in range(len(conditions) % 16, 16):
                word_status_trans_txt += 'pco_transitions%i_bit%-5s:= %-80s (*%2i*)\n' % (len(conditions) / 16, '[%i]' % ((remaining_WS_idx + 8) % 16), 'false;', remaining_WS_idx % 16)
        word_status_trans_txt += '\n\n' + word_status_trans_assignement + '\n'
        return word_status_trans_txt, trans_cond_txt

    @staticmethod
    def get_tia_word_status_transitions(conditions, WS_trans, DB_name):
        """Function used to get the logic for the word status related to the transitions

        TIA specific

        It returns the word status assignments
        """
        trans_cond_txt = ""
        word_status_trans_assignement = ""
        word_status_trans_txt = '(* WORD STATUS FOR TRANSITIONS *)\n'
        for trans in conditions:
            if not trans.WS_trans % 16:
                trans_cond_txt += '''    pco_transitions%i : WORD;
''' % (trans.WS_trans / 16)
                word_status_trans_assignement += '%s.AuPosR := pco_transitions%i;\n' % (WS_trans.replace('#', str(trans.WS_trans / 16)), trans.WS_trans / 16)
            word_status_trans_txt += 'pco_transitions%i.%%X%-3s:= %s.%s;\n' % (trans.WS_trans / 16, '%i' % (trans.WS_trans % 16), DB_name, trans.name)
        if len(conditions) % 16:
            for remaining_WS_idx in range(len(conditions) % 16, 16):
                word_status_trans_txt += 'pco_transitions%i.%%X%-3s:= false;\n' % (len(conditions) / 16, '%i' % (remaining_WS_idx % 16))
        word_status_trans_txt += '\n\n' + word_status_trans_assignement + '\n'
        return word_status_trans_txt, trans_cond_txt

    @staticmethod
    def get_schneider_word_status_transitions(conditions, WS_trans):
        """Function used to get the logic for the word status related to the transitions

        Schneider specific

        It returns the word status assignments
        """
        word_status_trans_txt = '(* WORD STATUS FOR TRANSITIONS *)\n'
        for trans in conditions:
            if not trans.WS_trans % 16:
                word_status_trans_txt += '''
%s_AuPosR := BIT_TO_WORD (
''' % (WS_trans.replace('#', str(trans.WS_trans / 16)))
            word_status_trans_txt += '            BIT%s := %s' % (str(trans.WS_trans % 16), trans.name)
            if not (trans.WS_trans + 1) % 16:
                word_status_trans_txt += '\n);\n'
            elif trans != conditions[-1]:
                word_status_trans_txt += ',\n'
        if len(conditions) % 16:
            word_status_trans_txt += '\n);\n'
        return word_status_trans_txt

    def get_word_status_transitions(self, PCO_name):
        """Function used to get the logic for the word status related to the transitions

        It returns the word status assignments
        """
        self.thePlugin.writeInUABLog("Getting transitions word status for PCO \"%s\"" % PCO_name)
        graph_name = self.get_graph_tab_name(PCO_name)
        if graph_name:
            graph = self.graphs[graph_name]
            grid = graph.grid_instance
            WS_trans = graph.graph_instances[PCO_name].WS_trans
            if WS_trans:
                conditions = [trans for trans in grid.trans if 'WS_trans' in dir(trans)]
                if self.siemens_PLC:
                    return self.get_siemens_word_status_transitions(conditions, WS_trans, graph.graph_instances[PCO_name].DB_name)
                elif self.TIA_PLC:
                    return self.get_tia_word_status_transitions(conditions, WS_trans, graph.graph_instances[PCO_name].DB_name)
                elif self.unity_PLC:
                    return self.get_schneider_word_status_transitions(conditions, WS_trans)
                else:
                    self.thePlugin.writeErrorInUABLog("Platform not supported")
            else:
                return None

    def write_word_status_transitions_variables(self, PCO_name):
        """Function used to write the variables used for the word status related to the transitions to a logic file

        Schneider specific
        """
        # Explicitly disable unity generation
        if self.unity_PLC:
            self.thePlugin.writeErrorInUABLog("Platform not supported")
        else:
            self.write_logic(self.get_word_status_transitions(PCO_name)[1])

    def write_word_status_transitions(self, PCO_name):
        """Function used to write the logic for the word status related to the transitions to a logic file"""
        if self.unity_PLC:
            self.write_logic('\n\n' + self.get_word_status_transitions(PCO_name))
        else:
            self.write_logic('\n\n' + self.get_word_status_transitions(PCO_name)[0])

    def get_template_variables(self, PCO_name):
        """Function used to get the variables defined in the input file for the template

        It returns the variables generated
        """
        self.thePlugin.writeInUABLog("Getting template variables for PCO \"%s\"" % PCO_name)
        graph_name = self.get_graph_tab_name(PCO_name)
        if graph_name:
            graph = self.graphs[graph_name]
            return "    # Automatically generated variables\n    " + "\n    ".join(graph.template_vars)

    def write_template_variables(self, PCO_name):
        """Function used to write the logic for the template variables to a logic file"""
        self.write_logic('\n\n' + self.get_template_variables(PCO_name))

    def get_code_variables(self, PCO_name):
        """Function used to get the variables defined in the input file for the PLC code

        It returns the variables generated
        """
        self.thePlugin.writeInUABLog("Getting code variables for PCO \"%s\"" % PCO_name)
        graph_name = self.get_graph_tab_name(PCO_name)
        if graph_name:
            graph = self.graphs[graph_name]
            if self.siemens_PLC or self.TIA_PLC:
                var_names = [var.name for var in graph.code_vars]
                code_vars = [graph.graph_instances[PCO_name].DB_name + "." + var.name + " := " +
                             self.replace_values(var.value, var_names, graph.graph_instances[PCO_name].DB_name) + ";"
                             for var in graph.code_vars]
            elif self.unity_PLC:
                code_vars = [var.name + " := " + var.value + ";" for var in graph.code_vars]
            else:
                self.thePlugin.writeErrorInUABLog("Platform not supported")
            return "(* VARIABLES COMPUTATION *)\n\n" + "\n".join(code_vars)

    def write_code_variables(self, PCO_name):
        """Function used to write the logic for the code variables to a logic file"""
        self.write_logic('\n\n' + self.get_code_variables(PCO_name))

    def get_code_variables_declaration(self, PCO_name):
        """Function used to get the declaration of the variables defined in the input file to the PLC code

        It returns the variables declaration (with type specified or boolean by default)
        """
        self.thePlugin.writeInUABLog("Getting code variables declaration for PCO \"%s\"" % PCO_name)
        graph_name = self.get_graph_tab_name(PCO_name)
        if graph_name:
            graph = self.graphs[graph_name]
            if self.siemens_PLC:
                return "\n".join(["    %s: %s := FALSE;" % (var.name, var.type) for var in graph.code_vars])
            elif self.TIA_PLC:
                return "\n".join(["""    <Member Name="%s" Datatype="%s" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList></Member>""" % (var.name, var.type) for var in graph.code_vars])
            elif self.unity_PLC:
                return "\n".join(["""<variables name="%s" typeName="%s"></variables>""" % (var.name, var.type) for var in graph.code_vars])
            else:
                self.thePlugin.writeErrorInUABLog('Platform not supported')

    def write_code_variables_declaration(self, PCO_name):
        """Function used to write the declaration of the code variables to a logic file"""
        self.write_logic('\n\n' + self.get_code_variables_declaration(PCO_name))

    def get_WS_pattern(self, PCO_name):
        """Function used to get the pattern for the word status related to the steps

        It returns a comment with the pattern generated
        """
        self.thePlugin.writeInUABLog("Getting word status pattern for PCO \"%s\"" % PCO_name)
        graph_name = self.get_graph_tab_name(PCO_name)
        if graph_name:
            graph = self.graphs[graph_name]
            grid = graph.grid_instance
            return "(*" + ",".join([str(step.WS_step) + "=" + step.WS_pattern for step in grid.steps if step.WS_pattern]) + "*)"

    def write_WS_pattern(self, PCO_name):
        """Function used to write the comment with the pattern of the word status related to the steps to a logic file"""
        self.write_logic('\n\n' + self.get_WS_pattern(PCO_name))
