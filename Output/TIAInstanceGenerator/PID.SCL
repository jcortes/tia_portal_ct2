(******* All PID devices instance *************************)  
// Specs version used for this generation: $spec_version$

//PID DB Creation file: UNICOS application

// DB_PID_ManRequest
(*DB for THE MAPPING of UNICOS objects INPUTS*)
DATA_BLOCK "DB_PID_ManRequest"
TITLE = DB_PID_ManRequest
{ S7_Optimized_Access := 'FALSE' }
AUTHOR : 'ICE/PLC'
FAMILY : UNICOS
NAME : Comm
//
// Contains all Manual Request signals from WinCCOA
   STRUCT
      PID_Requests : Array[1..17] of "PID_ManRequest";
   END_STRUCT;


BEGIN



END_DATA_BLOCK

// DB_Event_PID Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK "DB_Event_PID"
TITLE = 'DB_Event'
{ S7_Optimized_Access := 'FALSE' }
AUTHOR : 'ICE/PLC'
FAMILY : UNICOS
NAME : Comm
//
// Contains all evstsreg signals of UNICOS objects type PID
   STRUCT 
      Nb_PID : Int;
      PID_evstsreg : Array[1..17] of "PID_event";
   END_STRUCT;


BEGIN
   Nb_PID := 17;

END_DATA_BLOCK

(*binaries Status of the PID OBJECTS************************************************)
DATA_BLOCK "DB_bin_status_PID"
TITLE = 'DB_bin_status_PID'
{ S7_Optimized_Access := 'FALSE' }
AUTHOR : UNICOS
FAMILY : status
NAME : Status
//
// Global binary status DB of PID
   STRUCT 
      StsReg01 : Array[1..17] of "PID_bin_Status";
   END_STRUCT;


BEGIN

END_DATA_BLOCK


(*old binaries Status of the PID OBJECTS************************************************)
DATA_BLOCK "DB_bin_status_PID_old"
TITLE = 'DB_bin_status_PID_old'
{ S7_Optimized_Access := 'FALSE' }
AUTHOR : UNICOS
FAMILY : status
NAME : Status
//
// Old Global binary status DB of PID
   STRUCT 
      StsReg01 : Array[1..17] of "PID_bin_Status";
   END_STRUCT;


BEGIN

END_DATA_BLOCK

(*analogic Status of the PID OBJECTS************************************************)
DATA_BLOCK "DB_ana_status_PID"
TITLE = 'DB_ana_status_PID'
{ S7_Optimized_Access := 'FALSE' }
AUTHOR : UNICOS
FAMILY : status
NAME : Status
//
// Global analogic status DB of PID
   STRUCT 
      status : Array[1..17] of "PID_ana_Status";
   END_STRUCT;


BEGIN

END_DATA_BLOCK


(*old analogic Status of the PID OBJECTS************************************************)
DATA_BLOCK "DB_ana_status_PID_old"
TITLE = 'DB_ana_status_PID_old'
{ S7_Optimized_Access := 'FALSE' }
AUTHOR : UNICOS
FAMILY : status
NAME : Status
//
// Old Global analogic status DB of PID
   STRUCT 
      status : Array[1..17] of "PID_ana_Status";
   END_STRUCT;


BEGIN

END_DATA_BLOCK


// INIT of PARAMETERS DONE in the DB creation
// DB scheduler to manage the PID sampling times

DATA_BLOCK "DB_SCHED"
{ S7_Optimized_Access := 'TRUE' }
   STRUCT 
      GLP_NBR : Int;   // greatest loop number
      ALP_NBR : Int;   // actual loop number
      LOOP_DAT : Array  [1 .. 6] of STRUCT 
         MAN_CYC : Time;   // loop data: manual sample time
         MAN_DIS : Bool;   // loop data: manual loop disable
         MAN_CRST : Bool;  // loop data: manual complete restart
         ENABLE : Bool;   // loop data: enable loop
         COM_RST : Bool;   // loop data: complete restart
         ILP_COU : Int;   // loop data: internal loop counter
         CYCLE : Time;   // loop data: sample time
      END_STRUCT;
   END_STRUCT;


BEGIN
    GLP_NBR := 6;
    ALP_NBR := 0;
	#LOOP_DAT[1].MAN_CYC := T#0.1s;   // Group: 1; Components: 3 PIDs
	#LOOP_DAT[2].MAN_CYC := T#0.1s;   // Group: 2; Components: 3 PIDs
	#LOOP_DAT[3].MAN_CYC := T#0.1s;   // Group: 3; Components: 3 PIDs
	#LOOP_DAT[4].MAN_CYC := T#1.0s;   // Group: 4; Components: 3 PIDs
	#LOOP_DAT[5].MAN_CYC := T#1.0s;   // Group: 5; Components: 3 PIDs
	#LOOP_DAT[6].MAN_CYC := T#1.0s;   // Group: 6; Components: 3 PIDs

END_DATA_BLOCK

DATA_BLOCK "FDEU_893_FC5101" "CPC_FB_PID"
BEGIN
   index := 1;
   AuSPSpd.InSpd := 1.0;
   AuSPSpd.DeSpd := 1.0;
   PControl.PMaxRan := 100.0;
   PControl.PMinRan := 0.0;
   PControl.POutMaxRan := 100.0;
   PControl.POutMinRan := 0.0;
   PControl.MVFiltTime := T#1.0s;
   PControl.PIDCycle := T#1s;
   PControl.ScaMethod := 3;
   PControl.RA := FALSE;
   DefKc := 2.0;
   DefTi := 2.0;
   DefTd := 0.0;
   DefTds := 0.0;
   DefSP := 0.0;
   DefSPH := 50.0;
   DefSPL := 0.0;
   DefOutH := 100.0;
   DefOutL := 30.0;
END_DATA_BLOCK

DATA_BLOCK "FDEU_893_FC1101" "CPC_FB_PID"
BEGIN
   index := 2;
   AuSPSpd.InSpd := 1.0;
   AuSPSpd.DeSpd := 1.0;
   PControl.PMaxRan := 100.0;
   PControl.PMinRan := 0.0;
   PControl.POutMaxRan := 100.0;
   PControl.POutMinRan := 0.0;
   PControl.MVFiltTime := T#1.0s;
   PControl.PIDCycle := T#1s;
   PControl.ScaMethod := 3;
   PControl.RA := FALSE;
   DefKc := 2.0;
   DefTi := 2.0;
   DefTd := 0.0;
   DefTds := 0.0;
   DefSP := 0.0;
   DefSPH := 100.0;
   DefSPL := 0.0;
   DefOutH := 100.0;
   DefOutL := 0.0;
END_DATA_BLOCK

DATA_BLOCK "FDEU_893_FC5201" "CPC_FB_PID"
BEGIN
   index := 3;
   AuSPSpd.InSpd := 1.0;
   AuSPSpd.DeSpd := 1.0;
   PControl.PMaxRan := 100.0;
   PControl.PMinRan := 0.0;
   PControl.POutMaxRan := 100.0;
   PControl.POutMinRan := 0.0;
   PControl.MVFiltTime := T#1.0s;
   PControl.PIDCycle := T#1s;
   PControl.ScaMethod := 3;
   PControl.RA := FALSE;
   DefKc := 0.2;
   DefTi := 0.25;
   DefTd := 0.0;
   DefTds := 0.0;
   DefSP := 0.0;
   DefSPH := 100.0;
   DefSPL := 0.0;
   DefOutH := 100.0;
   DefOutL := 0.0;
END_DATA_BLOCK

DATA_BLOCK "FDEU_893_PC1501" "CPC_FB_PID"
BEGIN
   index := 4;
   AuSPSpd.InSpd := 1.0;
   AuSPSpd.DeSpd := 1.0;
   PControl.PMaxRan := 16.0;
   PControl.PMinRan := 0.0;
   PControl.POutMaxRan := 100.0;
   PControl.POutMinRan := 0.0;
   PControl.MVFiltTime := T#1.0s;
   PControl.PIDCycle := T#1s;
   PControl.ScaMethod := 3;
   PControl.RA := FALSE;
   DefKc := 3.125;
   DefTi := 2.0;
   DefTd := 0.0;
   DefTds := 0.0;
   DefSP := 0.0;
   DefSPH := 5.0;
   DefSPL := 1.0;
   DefOutH := 100.0;
   DefOutL := 0.0;
END_DATA_BLOCK

DATA_BLOCK "FDEU_893_PC2401" "CPC_FB_PID"
BEGIN
   index := 5;
   AuSPSpd.InSpd := 5.0;
   AuSPSpd.DeSpd := 5.0;
   PControl.PMaxRan := 16.0;
   PControl.PMinRan := 0.0;
   PControl.POutMaxRan := 100.0;
   PControl.POutMinRan := 0.0;
   PControl.MVFiltTime := T#1.0s;
   PControl.PIDCycle := T#1s;
   PControl.ScaMethod := 3;
   PControl.RA := FALSE;
   DefKc := 6.25;
   DefTi := 2.0;
   DefTd := 0.0;
   DefTds := 0.0;
   DefSP := 0.0;
   DefSPH := 6.2;
   DefSPL := 1.4;
   DefOutH := 100.0;
   DefOutL := 0.0;
END_DATA_BLOCK

DATA_BLOCK "FDEU_893_FC2403" "CPC_FB_PID"
BEGIN
   index := 6;
   AuSPSpd.InSpd := 1.0;
   AuSPSpd.DeSpd := 1.0;
   PControl.PMaxRan := 100.0;
   PControl.PMinRan := 0.0;
   PControl.POutMaxRan := 100.0;
   PControl.POutMinRan := 0.0;
   PControl.MVFiltTime := T#1.0s;
   PControl.PIDCycle := T#1s;
   PControl.ScaMethod := 3;
   PControl.RA := FALSE;
   DefKc := 1.0;
   DefTi := 2.0;
   DefTd := 0.0;
   DefTds := 0.0;
   DefSP := 0.0;
   DefSPH := 30.0;
   DefSPL := 1.0;
   DefOutH := 100.0;
   DefOutL := 0.0;
END_DATA_BLOCK

DATA_BLOCK "FDEU_893_FC2701" "CPC_FB_PID"
BEGIN
   index := 7;
   AuSPSpd.InSpd := 5.0;
   AuSPSpd.DeSpd := 5.0;
   PControl.PMaxRan := 100.0;
   PControl.PMinRan := 0.0;
   PControl.POutMaxRan := 100.0;
   PControl.POutMinRan := 0.0;
   PControl.MVFiltTime := T#1.0s;
   PControl.PIDCycle := T#1s;
   PControl.ScaMethod := 3;
   PControl.RA := FALSE;
   DefKc := 1.0;
   DefTi := 2.0;
   DefTd := 0.0;
   DefTds := 0.0;
   DefSP := 0.0;
   DefSPH := 20.0;
   DefSPL := 1.0;
   DefOutH := 100.0;
   DefOutL := 0.0;
END_DATA_BLOCK

DATA_BLOCK "FDEU_893_FC1501" "CPC_FB_PID"
BEGIN
   index := 8;
   AuSPSpd.InSpd := 1.0;
   AuSPSpd.DeSpd := 1.0;
   PControl.PMaxRan := 100.0;
   PControl.PMinRan := 0.0;
   PControl.POutMaxRan := 100.0;
   PControl.POutMinRan := 0.0;
   PControl.MVFiltTime := T#1.0s;
   PControl.PIDCycle := T#1s;
   PControl.ScaMethod := 3;
   PControl.RA := FALSE;
   DefKc := 1.0;
   DefTi := 2.0;
   DefTd := 0.0;
   DefTds := 0.0;
   DefSP := 0.0;
   DefSPH := 100.0;
   DefSPL := 0.0;
   DefOutH := 100.0;
   DefOutL := 0.0;
END_DATA_BLOCK

DATA_BLOCK "FDEU_893_DC7301" "CPC_FB_PID"
BEGIN
   index := 9;
   AuSPSpd.InSpd := 0.1;
   AuSPSpd.DeSpd := 0.1;
   PControl.PMaxRan := 14.0;
   PControl.PMinRan := 0.0;
   PControl.POutMaxRan := 100.0;
   PControl.POutMinRan := 0.0;
   PControl.MVFiltTime := T#1.0s;
   PControl.PIDCycle := T#1s;
   PControl.ScaMethod := 3;
   PControl.RA := TRUE;
   DefKc := 1.428;
   DefTi := 2.0;
   DefTd := 0.0;
   DefTds := 0.0;
   DefSP := 0.0;
   DefSPH := 14.0;
   DefSPL := 7.0;
   DefOutH := 3.0;
   DefOutL := 0.0;
END_DATA_BLOCK

DATA_BLOCK "FDEU_893_PID10" "CPC_FB_PID"
BEGIN
   index := 10;
   AuSPSpd.InSpd := 1.0;
   AuSPSpd.DeSpd := 1.0;
   PControl.PMaxRan := 100.0;
   PControl.PMinRan := 0.0;
   PControl.POutMaxRan := 100.0;
   PControl.POutMinRan := 0.0;
   PControl.MVFiltTime := T#0.1s;
   PControl.PIDCycle := T#0.1s;
   PControl.ScaMethod := 3;
   PControl.RA := TRUE;
   DefKc := 7.0;
   DefTi := 90.0;
   DefTd := 0.0;
   DefTds := 0.0;
   DefSP := 10.0;
   DefSPH := 100.0;
   DefSPL := 0.0;
   DefOutH := 100.0;
   DefOutL := 0.0;
END_DATA_BLOCK

DATA_BLOCK "FDEU_893_PID11" "CPC_FB_PID"
BEGIN
   index := 11;
   AuSPSpd.InSpd := 1.0;
   AuSPSpd.DeSpd := 1.0;
   PControl.PMaxRan := 100.0;
   PControl.PMinRan := 0.0;
   PControl.POutMaxRan := 100.0;
   PControl.POutMinRan := 0.0;
   PControl.MVFiltTime := T#0.1s;
   PControl.PIDCycle := T#0.1s;
   PControl.ScaMethod := 3;
   PControl.RA := TRUE;
   DefKc := 7.0;
   DefTi := 90.0;
   DefTd := 0.0;
   DefTds := 0.0;
   DefSP := 10.0;
   DefSPH := 100.0;
   DefSPL := 0.0;
   DefOutH := 100.0;
   DefOutL := 0.0;
END_DATA_BLOCK

DATA_BLOCK "FDEU_893_PID12" "CPC_FB_PID"
BEGIN
   index := 12;
   AuSPSpd.InSpd := 1.0;
   AuSPSpd.DeSpd := 1.0;
   PControl.PMaxRan := 100.0;
   PControl.PMinRan := 0.0;
   PControl.POutMaxRan := 100.0;
   PControl.POutMinRan := 0.0;
   PControl.MVFiltTime := T#0.1s;
   PControl.PIDCycle := T#0.1s;
   PControl.ScaMethod := 3;
   PControl.RA := TRUE;
   DefKc := 7.0;
   DefTi := 90.0;
   DefTd := 0.0;
   DefTds := 0.0;
   DefSP := 10.0;
   DefSPH := 100.0;
   DefSPL := 0.0;
   DefOutH := 100.0;
   DefOutL := 0.0;
END_DATA_BLOCK

DATA_BLOCK "FDEU_893_PID13" "CPC_FB_PID"
BEGIN
   index := 13;
   AuSPSpd.InSpd := 1.0;
   AuSPSpd.DeSpd := 1.0;
   PControl.PMaxRan := 100.0;
   PControl.PMinRan := 0.0;
   PControl.POutMaxRan := 100.0;
   PControl.POutMinRan := 0.0;
   PControl.MVFiltTime := T#0.1s;
   PControl.PIDCycle := T#0.1s;
   PControl.ScaMethod := 3;
   PControl.RA := TRUE;
   DefKc := 7.0;
   DefTi := 90.0;
   DefTd := 0.0;
   DefTds := 0.0;
   DefSP := 10.0;
   DefSPH := 100.0;
   DefSPL := 0.0;
   DefOutH := 100.0;
   DefOutL := 0.0;
END_DATA_BLOCK

DATA_BLOCK "FDEU_893_PID14" "CPC_FB_PID"
BEGIN
   index := 14;
   AuSPSpd.InSpd := 1.0;
   AuSPSpd.DeSpd := 1.0;
   PControl.PMaxRan := 100.0;
   PControl.PMinRan := 0.0;
   PControl.POutMaxRan := 100.0;
   PControl.POutMinRan := 0.0;
   PControl.MVFiltTime := T#0.1s;
   PControl.PIDCycle := T#0.1s;
   PControl.ScaMethod := 3;
   PControl.RA := TRUE;
   DefKc := 7.0;
   DefTi := 90.0;
   DefTd := 0.0;
   DefTds := 0.0;
   DefSP := 10.0;
   DefSPH := 100.0;
   DefSPL := 0.0;
   DefOutH := 100.0;
   DefOutL := 0.0;
END_DATA_BLOCK

DATA_BLOCK "FDEU_893_PID15" "CPC_FB_PID"
BEGIN
   index := 15;
   AuSPSpd.InSpd := 1.0;
   AuSPSpd.DeSpd := 1.0;
   PControl.PMaxRan := 100.0;
   PControl.PMinRan := 0.0;
   PControl.POutMaxRan := 100.0;
   PControl.POutMinRan := 0.0;
   PControl.MVFiltTime := T#0.1s;
   PControl.PIDCycle := T#0.1s;
   PControl.ScaMethod := 3;
   PControl.RA := TRUE;
   DefKc := 7.0;
   DefTi := 90.0;
   DefTd := 0.0;
   DefTds := 0.0;
   DefSP := 10.0;
   DefSPH := 100.0;
   DefSPL := 0.0;
   DefOutH := 100.0;
   DefOutL := 0.0;
END_DATA_BLOCK

DATA_BLOCK "FDEU_893_PID16" "CPC_FB_PID"
BEGIN
   index := 16;
   AuSPSpd.InSpd := 1.0;
   AuSPSpd.DeSpd := 1.0;
   PControl.PMaxRan := 100.0;
   PControl.PMinRan := 0.0;
   PControl.POutMaxRan := 100.0;
   PControl.POutMinRan := 0.0;
   PControl.MVFiltTime := T#0.1s;
   PControl.PIDCycle := T#0.1s;
   PControl.ScaMethod := 3;
   PControl.RA := TRUE;
   DefKc := 7.0;
   DefTi := 90.0;
   DefTd := 0.0;
   DefTds := 0.0;
   DefSP := 10.0;
   DefSPH := 100.0;
   DefSPL := 0.0;
   DefOutH := 100.0;
   DefOutL := 0.0;
END_DATA_BLOCK

DATA_BLOCK "FDEU_893_PID17" "CPC_FB_PID"
BEGIN
   index := 17;
   AuSPSpd.InSpd := 1.0;
   AuSPSpd.DeSpd := 1.0;
   PControl.PMaxRan := 100.0;
   PControl.PMinRan := 0.0;
   PControl.POutMaxRan := 100.0;
   PControl.POutMinRan := 0.0;
   PControl.MVFiltTime := T#0.1s;
   PControl.PIDCycle := T#0.1s;
   PControl.ScaMethod := 3;
   PControl.RA := TRUE;
   DefKc := 7.0;
   DefTi := 90.0;
   DefTd := 0.0;
   DefTds := 0.0;
   DefSP := 10.0;
   DefSPH := 100.0;
   DefSPL := 0.0;
   DefOutH := 100.0;
   DefOutL := 0.0;
END_DATA_BLOCK

