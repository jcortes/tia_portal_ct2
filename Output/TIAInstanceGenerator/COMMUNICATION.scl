DATA_BLOCK "DB_WinCCOA"
TITLE = 'DB_WinCCOA'
{ S7_Optimized_Access := 'FALSE' }
AUTHOR : UNICOS
FAMILY : COMMS
NAME : Comm
// 
// Contains TSPP parameters for WinCCOA
   STRUCT 
      address_IP : DInt;   // Address IP of WinCCOA
      address_Counter : Int;   // Watchdog Counter
      address_CommandInterface : Array[1..5] of Word;   // Command Interface FOR WinCCOA (Synchro - RequestAll)
      Data : Array[1..5] of Word;   // Information about S7 PLC (cycle/Error)
      TSPP_EventPeriod : Time;   // Period before sending Event to WinCCOA if stack not full
      TSPP_Reqlist : Array[1..4] of Int;   // Reqlist of TSPP_UNICOS_Manager
      TSPP_Error : Bool;   // Function error indicator of TSPP
      TSPP_Error_code : Word;   // Function error code of TSPP
      TSPP_Special_code : Word;   // Error code of internaly used functions
      UNICOS_LiveCounter : DInt;   // Live counter (1 second)
      Application_version : Real;   // Application version
      ResourcePackage_version : Struct   // Resource Package version
         Major : Int;
         Minor : Int;
         Small : Int;
      END_STRUCT;
      SCADA_IP : DInt;   // Diagnostics address IP of WinCCOA
      UNICOS_NewExtensions : Array[1..40] of Word;   // Future extensions of S7-UNICOS
   END_STRUCT;


BEGIN
   TSPP_EventPeriod := T#2S;
   Application_version := 1.0;
   ResourcePackage_version.Major := 1;
   ResourcePackage_version.Minor := 12;
   ResourcePackage_version.Small := 0;

END_DATA_BLOCK



DATA_BLOCK "DB_COMM"
TITLE = 'DB_COMM' 
{ S7_Optimized_Access := 'FALSE' }
AUTHOR : UNICOS
FAMILY : COMMS
NAME : Comm
//
// Contains all numbers of status DB
   STRUCT 
      nbDB : Int;
      DB_List : Array[1..27] of "CPC_DB_COMM";
   END_STRUCT;


BEGIN
   nbDB := 27;
   (* Recipes interface DB *)
   DB_List[1].status_DB := 106;
   DB_List[1].status_DB_old := 107;
   DB_List[1].size := 82;
   // nbDB:= 1
   (* Global Binary status DB of DI *)
   DB_List[2].Status_DB := 140;
   DB_List[2].Status_DB_old := 141;
   DB_List[2].size := 594;
   // nbDB:= 2
   (* Global Binary status DB of AI *)
   DB_List[3].Status_DB := 145;
   DB_List[3].Status_DB_old := 146;
   DB_List[3].size := 112;
   // nbDB:= 3
   (* Global Analog status DB of AI *)
   DB_List[4].Status_DB := 149;
   DB_List[4].Status_DB_old := 150;
   DB_List[4].size := 448;
   // nbDB:= 4
   (* Global Binary status DB of AIR *)
   DB_List[5].Status_DB := 152;
   DB_List[5].Status_DB_old := 153;
   DB_List[5].size := 138;
   // nbDB:= 5
   (* Global Analog status DB of AIR *)
   DB_List[6].Status_DB := 156;
   DB_List[6].Status_DB_old := 157;
   DB_List[6].size := 552;
   // nbDB:= 6
   (* Global Binary status DB of DO *)
   DB_List[7].Status_DB := 166;
   DB_List[7].Status_DB_old := 167;
   DB_List[7].size := 128;
   // nbDB:= 7
   (* Global Binary status DB of AO *)
   DB_List[8].Status_DB := 171;
   DB_List[8].Status_DB_old := 172;
   DB_List[8].size := 24;
   // nbDB:= 8
   (* Global Analog status DB of AO *)
   DB_List[9].Status_DB := 175;
   DB_List[9].Status_DB_old := 176;
   DB_List[9].size := 96;
   // nbDB:= 9
   (* Global Binary status DB of DPAR *)
   DB_List[10].Status_DB := 185;
   DB_List[10].Status_DB_old := 186;
   DB_List[10].size := 14;
   // nbDB:= 10
   (* Global Binary status DB of WPAR *)
   DB_List[11].Status_DB := 190;
   DB_List[11].Status_DB_old := 191;
   DB_List[11].size := 10;
   // nbDB:= 11
   (* Global Analog status DB of WPAR *)
   DB_List[12].Status_DB := 194;
   DB_List[12].Status_DB_old := 195;
   DB_List[12].size := 20;
   // nbDB:= 12
   (* Global Binary status DB of APAR *)
   DB_List[13].Status_DB := 197;
   DB_List[13].Status_DB_old := 198;
   DB_List[13].size := 1000;
   // nbDB:= 13
   (* Global Analog status DB of APAR *)
   DB_List[14].Status_DB := 201;
   DB_List[14].Status_DB_old := 202;
   DB_List[14].size := 4000;
   // nbDB:= 14
   (* Global Analog status DB of WS *)
   DB_List[15].Status_DB := 206;
   DB_List[15].Status_DB_old := 207;
   DB_List[15].size := 128;
   // nbDB:= 15
   (* Global Analog status DB of AS *)
   DB_List[16].Status_DB := 211;
   DB_List[16].Status_DB_old := 212;
   DB_List[16].size := 996;
   // nbDB:= 16
   (* Global Binary status DB of LOCAL *)
   DB_List[17].Status_DB := 214;
   DB_List[17].Status_DB_old := 215;
   DB_List[17].size := 60;
   // nbDB:= 17
   (* Global Binary status DB of ONOFF *)
   DB_List[18].Status_DB := 218;
   DB_List[18].Status_DB_old := 219;
   DB_List[18].size := 440;
   // nbDB:= 18
   (* Global Binary status DB of ANADO *)
   DB_List[19].Status_DB := 234;
   DB_List[19].Status_DB_old := 235;
   DB_List[19].size := 44;
   // nbDB:= 19
   (* Global Analog status DB of ANADO *)
   DB_List[20].Status_DB := 237;
   DB_List[20].Status_DB_old := 238;
   DB_List[20].size := 176;
   // nbDB:= 20
   (* Global Binary status DB of PID *)
   DB_List[21].Status_DB := 252;
   DB_List[21].Status_DB_old := 253;
   DB_List[21].size := 68;
   // nbDB:= 21
   (* Global Analog status DB of PID *)
   DB_List[22].Status_DB := 255;
   DB_List[22].Status_DB_old := 256;
   DB_List[22].size := 1020;
   // nbDB:= 22
   (* Global Binary status DB of PCO *)
   DB_List[23].Status_DB := 258;
   DB_List[23].Status_DB_old := 259;
   DB_List[23].size := 40;
   // nbDB:= 23
   (* Global Analog status DB of PCO *)
   DB_List[24].Status_DB := 261;
   DB_List[24].Status_DB_old := 262;
   DB_List[24].size := 80;
   // nbDB:= 24
   (* Global Binary status DB of DA *)
   DB_List[25].Status_DB := 264;
   DB_List[25].Status_DB_old := 265;
   DB_List[25].size := 1396;
   // nbDB:= 25
   (* Global Binary status DB of AA *)
   DB_List[26].Status_DB := 269;
   DB_List[26].Status_DB_old := 270;
   DB_List[26].size := 520;
   // nbDB:= 26
   (* Global Analog status DB of AA *)
   DB_List[27].Status_DB := 273;
   DB_List[27].Status_DB_old := 274;
   DB_List[27].size := 2600;
   // nbDB:= 27

END_DATA_BLOCK



DATA_BLOCK "DB_EventData"
TITLE = 'EventData'
{ S7_Optimized_Access := 'FALSE' }
AUTHOR : 'ICE/PLC'
FAMILY : UNICOS
NAME : Comm
//
// Contains pointers on evstsreg which have been changed
   STRUCT 
      Nb_Event : Int;
      List_Event : Array[1..1000] of Struct
         S7_ID : Byte;
         DataType : Byte;
         NbOfData : Int;
         DBNumber : Int;
         Address : DWord;
      END_STRUCT;
   END_STRUCT;


BEGIN

END_DATA_BLOCK



FUNCTION "FC_TSPP": Void
TITLE = 'FC_TSPP'
{ S7_Optimized_Access := 'TRUE' }
AUTHOR : UNICOS
FAMILY : COMMS
NAME : Comm
//
// Calls the TSPP manager function
   VAR_INPUT 
      NE : Bool;
      RA : Bool;
   END_VAR

BEGIN
	// Optimal TSPP parameters: 
	// MaxStatusTable = NbStatusTables solved in the TSPP file generated
	// MaxTableInOneSend = MaxTableInOneSend solved in the TSPP file generated
	// StatusWordSize = 100
	"TSPP_Unicos_DB"(
	                 Init := FALSE,
	                 SendID0 := W#16#100,
	                 SendEventPeriod := "DB_WinCCOA".TSPP_EventPeriod,
	                 NewEvent := #NE,
	                 EventTSIncluded := FALSE,
	                 MultipleEvent := TRUE,
	                 EventListDB := "DB_EventData",
	                 SendAllStatus := #RA,
	                 ListOfStatusTable := "DB_COMM",
	                 WatchDog := "DB_WinCCOA".address_Counter   // for 1500 DB_WinCCOA must be not optimized access
	);
END_FUNCTION



(*FUNCTION WHICH GENERATE EVENT FOR evstsreg01 and CALL TS_EVENT_MANAGER*)
FUNCTION "FC_Event" : Void
TITLE = 'FC_Event'
{ S7_Optimized_Access := 'TRUE' }
AUTHOR : 'ICE/PLC'
FAMILY : UNICOS
NAME : Comm
//
// FUNCTION WHICH GENERATE EVENT FOR evstsreg01 and CALL TS_EVENT_MANAGER
   VAR_TEMP 
      NewEvent : Bool;
      RequestAll : Bool;
      Synchro : Bool;
      i : Int;
      j : Int;
      k : Int;
      First_obj : Int;
      dummyVar : Int;
      ListEventSize : Int;
   END_VAR


BEGIN
	"DB_EventData".Nb_Event := 0;
	#First_obj := 0;
	#k := 2;
	#j := "TSPP_Unicos_DB".ID_NewEvent;
	#NewEvent := 0;
	#RequestAll := FALSE;
	#Synchro := FALSE;
	#ListEventSize := 1000;
	
    (* Test if evStsReg01 of DI have changed *)
    FOR #i := 1 TO "DB_Event_DI".Nb_DI DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_DI".DI_evstsreg[#i].evStsReg01) <> DWORD_TO_WORD(ROR(IN := "DB_Event_DI".DI_evstsreg[#i].evStsReg01, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_DI"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 4 + #k), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    (* Test if evStsReg01 of AI have changed *)
    FOR #i := 1 TO "DB_Event_AI".Nb_AI DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_AI".AI_evstsreg[#i].evStsReg01) <> DWORD_TO_WORD(ROR(IN := "DB_Event_AI".AI_evstsreg[#i].evStsReg01, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_AI"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 4 + #k), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    (* Test if evStsReg01 of AIR have changed *)
    FOR #i := 1 TO "DB_Event_AIR".Nb_AIR DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_AIR".AIR_evstsreg[#i].evStsReg01) <> DWORD_TO_WORD(ROR(IN := "DB_Event_AIR".AIR_evstsreg[#i].evStsReg01, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_AIR"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 4 + #k), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    (* Test if evStsReg01 of DO have changed *)
    FOR #i := 1 TO "DB_Event_DO".Nb_DO DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_DO".DO_evstsreg[#i].evStsReg01) <> DWORD_TO_WORD(ROR(IN := "DB_Event_DO".DO_evstsreg[#i].evStsReg01, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_DO"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 4 + #k), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    (* Test if evStsReg01 of AO have changed *)
    FOR #i := 1 TO "DB_Event_AO".Nb_AO DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_AO".AO_evstsreg[#i].evStsReg01) <> DWORD_TO_WORD(ROR(IN := "DB_Event_AO".AO_evstsreg[#i].evStsReg01, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_AO"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 4 + #k), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    (* Test if evStsReg01 of DPAR have changed *)
    FOR #i := 1 TO "DB_Event_DPAR".Nb_DPAR DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_DPAR".DPAR_evstsreg[#i].evStsReg01) <> DWORD_TO_WORD(ROR(IN := "DB_Event_DPAR".DPAR_evstsreg[#i].evStsReg01, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_DPAR"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 4 + #k), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    (* Test if evStsReg01 of WPAR have changed *)
    FOR #i := 1 TO "DB_Event_WPAR".Nb_WPAR DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_WPAR".WPAR_evstsreg[#i].evStsReg01) <> DWORD_TO_WORD(ROR(IN := "DB_Event_WPAR".WPAR_evstsreg[#i].evStsReg01, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_WPAR"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 4 + #k), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    (* Test if evStsReg01 of APAR have changed *)
    FOR #i := 1 TO "DB_Event_APAR".Nb_APAR DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_APAR".APAR_evstsreg[#i].evStsReg01) <> DWORD_TO_WORD(ROR(IN := "DB_Event_APAR".APAR_evstsreg[#i].evStsReg01, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_APAR"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 4 + #k), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    (* Test if evStsReg01 of LOCAL have changed *)
    FOR #i := 1 TO "DB_Event_LOCAL".Nb_LOCAL DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_LOCAL".LOCAL_evstsreg[#i].evStsReg01) <> DWORD_TO_WORD(ROR(IN := "DB_Event_LOCAL".LOCAL_evstsreg[#i].evStsReg01, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_LOCAL"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 4 + #k), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    (* Test if evStsReg01 of ONOFF have changed *)
    FOR #i := 1 TO "DB_Event_ONOFF".Nb_ONOFF DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_ONOFF".ONOFF_evstsreg[#i].evStsReg01) <> DWORD_TO_WORD(ROR(IN := "DB_Event_ONOFF".ONOFF_evstsreg[#i].evStsReg01, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_ONOFF"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 8 + #k), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    #First_obj := "DB_Event_ONOFF".Nb_ONOFF;
    (* Test if evStsReg02 of ONOFF have changed *)
    FOR #i := 1 TO "DB_Event_ONOFF".Nb_ONOFF DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_ONOFF".ONOFF_evstsreg[#i].evStsReg02) <> DWORD_TO_WORD(ROR(IN := "DB_Event_ONOFF".ONOFF_evstsreg[#i].evStsReg02, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_ONOFF"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 8 + #k + 4), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    (* Test if evStsReg01 of ANADO have changed *)
    FOR #i := 1 TO "DB_Event_ANADO".Nb_ANADO DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_ANADO".ANADO_evstsreg[#i].evStsReg01) <> DWORD_TO_WORD(ROR(IN := "DB_Event_ANADO".ANADO_evstsreg[#i].evStsReg01, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_ANADO"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 8 + #k), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    #First_obj := "DB_Event_ANADO".Nb_ANADO;
    (* Test if evStsReg02 of ANADO have changed *)
    FOR #i := 1 TO "DB_Event_ANADO".Nb_ANADO DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_ANADO".ANADO_evstsreg[#i].evStsReg02) <> DWORD_TO_WORD(ROR(IN := "DB_Event_ANADO".ANADO_evstsreg[#i].evStsReg02, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_ANADO"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 8 + #k + 4), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    (* Test if evStsReg01 of PID have changed *)
    FOR #i := 1 TO "DB_Event_PID".Nb_PID DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_PID".PID_evstsreg[#i].evStsReg01) <> DWORD_TO_WORD(ROR(IN := "DB_Event_PID".PID_evstsreg[#i].evStsReg01, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_PID"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 8 + #k), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    #First_obj := "DB_Event_PID".Nb_PID;
    (* Test if evStsReg02 of PID have changed *)
    FOR #i := 1 TO "DB_Event_PID".Nb_PID DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_PID".PID_evstsreg[#i].evStsReg02) <> DWORD_TO_WORD(ROR(IN := "DB_Event_PID".PID_evstsreg[#i].evStsReg02, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_PID"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 8 + #k + 4), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    (* Test if evStsReg01 of PCO have changed *)
    FOR #i := 1 TO "DB_Event_PCO".Nb_PCO DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_PCO".PCO_evstsreg[#i].evStsReg01) <> DWORD_TO_WORD(ROR(IN := "DB_Event_PCO".PCO_evstsreg[#i].evStsReg01, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_PCO"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 8 + #k), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    #First_obj := "DB_Event_PCO".Nb_PCO;
    (* Test if evStsReg02 of PCO have changed *)
    FOR #i := 1 TO "DB_Event_PCO".Nb_PCO DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_PCO".PCO_evstsreg[#i].evStsReg02) <> DWORD_TO_WORD(ROR(IN := "DB_Event_PCO".PCO_evstsreg[#i].evStsReg02, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_PCO"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 8 + #k + 4), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    (* Test if evStsReg01 of DA have changed *)
    FOR #i := 1 TO "DB_Event_DA".Nb_DA DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_DA".DA_evstsreg[#i].evStsReg01) <> DWORD_TO_WORD(ROR(IN := "DB_Event_DA".DA_evstsreg[#i].evStsReg01, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_DA"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 4 + #k), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    (* Test if evStsReg01 of AA have changed *)
    FOR #i := 1 TO "DB_Event_AA".Nb_AA DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_AA".AA_evstsreg[#i].evStsReg01) <> DWORD_TO_WORD(ROR(IN := "DB_Event_AA".AA_evstsreg[#i].evStsReg01, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_AA"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 8 + #k), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
    #First_obj := "DB_Event_AA".Nb_AA;
    (* Test if evStsReg02 of AA have changed *)
    FOR #i := 1 TO "DB_Event_AA".Nb_AA DO
      //if new_stsreg <> old_stsreg
      IF DWORD_TO_WORD("DB_Event_AA".AA_evstsreg[#i].evStsReg02) <> DWORD_TO_WORD(ROR(IN := "DB_Event_AA".AA_evstsreg[#i].evStsReg02, N := 16)) THEN
        #NewEvent := 1;
        #j := #j + 1;
        IF #j > #ListEventSize THEN
          #j := 1;
        END_IF;
        "DB_EventData".List_Event[#j].S7_ID := B#16#10;
        "DB_EventData".List_Event[#j].DataType := 6; // DWORD
        "DB_EventData".List_Event[#j].NbOfData := 1;
        "DB_EventData".List_Event[#j].DBNumber := UINT_TO_INT(DB_ANY_TO_UINT("DB_Event_AA"));
        "DB_EventData".List_Event[#j].Address := SHL(IN := INT_TO_DWORD((#i - 1) * 8 + #k + 4), N := 3) OR DW#16#84000000;
      END_IF;
    END_FOR;
    
	"DB_EventData".Nb_Event := #j;
	"TSPP_Unicos_DB".ID_NewEvent := #j;
	(*Action by WinCCOA via the Command Interface*)
	CASE WORD_TO_INT("DB_WinCCOA".address_CommandInterface[1]) OF
	  1:
	    #Synchro := TRUE;   // Synchronisation of timing from WinCCOA
	    "DB_WinCCOA".address_CommandInterface[1] := 0;
	  2:
	    #RequestAll := TRUE;// Request All : All object status are sent to WinCCOA
	    "DB_WinCCOA".address_CommandInterface[1] := 0;
	END_CASE;
	"FC_TSPP"(NE := #NewEvent,
	            RA := #RequestAll);
	
	#Synchro := FALSE;
	#RequestAll := FALSE;
	"DB_WinCCOA".Data[1] := DINT_TO_WORD(TIME_TO_DINT("CPC_GLOBAL_VARS".T_CYCLE)); //PLC cycle
	"DB_WinCCOA".Data[2] := INT_TO_WORD("TSPP_Unicos_DB".NbOfTables);
	"DB_WinCCOA".Data[3] := INT_TO_WORD("TSPP_Unicos_DB".NbOfStatusReq);
	"DB_WinCCOA".Data[4] := "TSPP_Unicos_DB".Error_code;
	"DB_WinCCOA".Data[5] := "TSPP_Unicos_DB".Special_code;
	
	(*From TS Event Manager*)
	"DB_WinCCOA".TSPP_Reqlist := "TSPP_Unicos_DB".ReqList;
	"DB_WinCCOA".TSPP_Error := "TSPP_Unicos_DB".Error;
	"DB_WinCCOA".TSPP_Error_code := "TSPP_Unicos_DB".Error_code;
	"DB_WinCCOA".TSPP_Special_code := "TSPP_Unicos_DB".Special_code;
	// Live Counter (Time in seconds from last startUP)
	"DB_WinCCOA".UNICOS_LiveCounter := "CPC_GLOBAL_VARS".UNICOS_LiveCounter;
END_FUNCTION

