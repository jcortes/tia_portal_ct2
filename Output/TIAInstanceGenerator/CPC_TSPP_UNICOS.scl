FUNCTION_BLOCK "TSPP_Unicos_Manager"
TITLE = 'TSPP_Unicos_Manager'
{ S7_Optimized_Access := 'FALSE' }
AUTHOR : UNICOS
FAMILY : COMMS
NAME : COMM
VERSION : 4.3
// ### FBxxx  The symbol must be declared first
   VAR_INPUT 
      Init : Bool;   // Initialisation of the function
      SendID0 : Word;   // ID0 of the S7 link with WinCCOA
      SendEventPeriod : Time;   // Period for sending event buffer
      NewEvent : Bool;   // New event indication
      EventTSIncluded : Bool;   // Event(s) with(out)time stamp indication
      MultipleEvent : Bool;   // EventData point to a list of events
      EventData : Any;   // Pointer to event
      IN_Event AT EventData : Struct   // NOT USED
         S7_ID : Byte;
         DataType : Byte;
         NbOfData : Int;
         DBNumber : Int;
         Address : DWord;
      END_STRUCT;
      EventListDB : DB_ANY;
      ListOfStatusTable : DB_ANY;   // DB containing the list of status tables
      SendAllStatus : Bool;
      WatchDog : Any;
      IN_WatchDog AT WatchDog : Struct
         S7_ID : Byte;
         DataType : Byte;
         NbOfData : Int;
         DBNumber : Word;
         Address : DWord;
      END_STRUCT;
   END_VAR

   VAR_OUTPUT 
      Error : Bool;   // Function error indicator
      Error_code : Word;   // Function error code
      Special_code : Word;   // Error code of internaly used functions
   END_VAR

   VAR 
      ID_NewEvent : Int;
      ID_EventSent : Int;
      CurrentEvent : Int := 0;
      EventBufferSize : Int := 6;
      //Bsend_Done : Bool;
      //Bsend_Error : Bool;
      TimerFlag : Bool;
      TimeOut : Bool;
      SendReq : Bool;
      BufferFullSendReq : Bool;
      ExtendedBufFull : Bool := FALSE;
      SendPending : Bool := FALSE;
      SendProblem : Bool := FALSE;
      InitDone : Bool := FALSE;
      Dummy : Bool := TRUE;   // NOT USED
      RemoveFromList : Bool := FALSE;
      WaitDelay : Bool;
      SendAllStatusReq : Bool := FALSE;
      LinkNotEstablish : Bool;
      WatchDogInList : Bool;
      StatusInList : Bool;
      IPReceived : Bool;
      SendStatusCommand : Bool;
      EventInList : Bool;
      TSPPTableFull : Bool := FALSE;
      WatchDogMissing : Bool;
      WatchDogTimeOut : Bool;
      WinCCOANotAlive : Bool;
      OldSendAllStatus : Bool;
      //Bsend_Status : Word;
      NbOfTables : Int;
      NbOfGroupOfTable : Int := 1;
      WorkingGroup : Int := 1;
      SizeOfLastGroup : Int;
      NbOfStatusReq : Int;
      NbOfRequest : Int := 0;
      BsendLen : Word;
      TSComm_Alive : Struct
         TSPP_ID1 : Char := 'T';
         TSPP_ID2 : Char := 'S';
         TSPP_ID3 : Char := 'P';
         Nb_of_TSWord : Byte := 1;
         TS_Data_Length : Int := 7;
         TimeStamp : Date_And_Time;
         DBNumber : Word;
         Address : Int;
         CommAliveCounter : Int;
      END_STRUCT;
      LocalActual : Array[1..#StatusWordSize] of Word;
      LocalOld : Array[1..#StatusWordSize] of Word;
      ReqList : Array[1..4] of Int;   // := #ReqListSize(0); High index = MaxReqNumber + 1
      StatusReqList : Array[1..88] of Int;   // := #StatusReqListSize(0); High index = MaxStatusReqNb + 1
      AlreadyInReqList : Array[1..#MaxStatusTable] of Bool;   // := #MaxStatusTable(FALSE);
      ActualTableList : Array[1..#MaxStatusTable] of Struct
         S7_ID : Byte;
         DataType : Byte;
         NbOfData : Int;
         DBNumber : Int;
         Address : DWord;
      END_STRUCT;
      OldTableList : Array[1..#MaxStatusTable] of Struct
         S7_ID : Byte;
         DataType : Byte;
         NbOfData : Int;
         DBNumber : Int;
         Address : DWord;
      END_STRUCT;
      TS_EventBuffer : Struct
         TSPP_ID1 : Char := 'T';
         TSPP_ID2 : Char := 'S';
         TSPP_ID3 : Char := 'P';
         Nb_of_TSWord : Byte := 2;   // = Word_in_one_TSEvent
         TS_Data_Length : Int;
         Data : Array[1..#ExtendedNbOfEvent] of Struct
            TimeStamp : Date_And_Time;
            DBNumber : Int;
            Address : Int;
            Data : Array[1..#Word_in_one_TSEvent] of Word;
         END_STRUCT;
      END_STRUCT;
      BSendBuffer : Struct   // Also used directly as StatusBuffer
         TSPP_ID1 : Char := 'T';
         TSPP_ID2 : Char := 'S';
         TSPP_ID3 : Char := 'P';
         Nb_of_TSWord : Byte;
         TS_Data_Length : Int;
         TableList : Array[1..#MaxTableInOneSend] of Struct
            TimeStamp : Date_And_Time;
            DBNumber : Int;
            Address : Int;
            Table : Array[1..#StatusWordSize] of Word;
         END_STRUCT;
         ExtendedData : Array[0..#EventExtension] of Byte;
      END_STRUCT;
      SendTimeOutTimer : TON;
      SendEventTimer : TON;
      WatchDogTimer : TON;
      NbOfRetry : Int := 0;
      BSEND_TON : TON;
      BSEND_DB : BSEND;
   END_VAR

   VAR_TEMP 
      Er_Code : Int;
      Result : Int;
      TempAdr : DWord;
      CurrentTime : Time;
      InEventByteDataSize : Int;
      NumberOfEvents : Int;
      CurrentInEvent : Int;
      EventListElemPtr : Any;
      EventListElement AT EventListElemPtr : Struct
         IDAndType : Word;
         DataAndDBNb : DWord;
         Address : DWord;
      END_STRUCT;
      EventPtr : Any;
      Event AT EventPtr : Struct
         S7_ID : Byte;
         DataType : Byte;
         NbOfData : Int;
         DBNumber : Int;
         Address : DWord;
      END_STRUCT;
      EventListAdr : Int;
      Index : Int;
      Index2 : Int;
      Index3 : Int;
      Index4 : Int;
      First : Int;
      Last : Int;
      Src : Any;
      ATSrc AT Src : Struct
         S7_ID : Byte;
         DataType : Byte;
         NbOfData : Int;
         DBNumber : Int;
         Address : DWord;
      END_STRUCT;
      Dst : Any;
      ATDst AT Dst : Struct
         S7_ID : Byte;
         DataType : Byte;
         NbOfData : Int;
         DBNumber : Int;
         Address : DWord;
      END_STRUCT;
      DWNumber : Int;
      TempCounter : Int;
      ActualTable : Any;
      CreateActual AT ActualTable : Struct
         S7_ID : Byte;
         DataType : Byte;
         NbOfData : Int;
         DBNumber : Int;
         Address : DWord;
      END_STRUCT;
      OldTable : Any;
      CreateOld AT OldTable : Struct
         S7_ID : Byte;
         DataType : Byte;
         NbOfData : Int;
         DBNumber : Int;
         Address : DWord;
      END_STRUCT;
      TSPPTableIndex : Int;
      NumberOfTableDB : Int;
      TablesInDB : Int;
      LastTableSize : Int;
      ChangeInTable : Bool;
      Bsend_Done : Bool;
      Bsend_Error : Bool;
      Bsend_Status : Word;

   END_VAR

   VAR CONSTANT 
      (* ############################################################################
      These parameters MUST be adjusted to fullfill the user requirements 
      You can change them in the jython template in \Resources\TIAInstanceGenerator\Rules\GlobalTemplates\TIAInst_CPC_TSPP_UNICOS_Template.py 
      Do not edit SCL file directly *)
      MaxNbOfTSEvent : Int := 100;   // Nb of event wich will trig the send to WinCCOA
      SpareEventNumber : Int := 50;   // spare places to be able to continue to ../.. record event when the send function is buzy
      Word_in_one_TSEvent : Int := 2;   // Number of data word in one Event
      MaxStatusTable : Int := 87;   // Max number of status tables (the length of one table is 200 bytes without the header)
      MaxTableInOneSend : Int := 87;   // (MAX value S7-400:250  S7-300:100); Max nb of status tables in one TSPP message; Must lower or equal to MaxStatusTable
      ListEventSize : Int := 1000;   // Size of the Event List
      (* ########################################################################### *)
      ExtendedNbOfEvent : Int := 150;   // Full size of event buffer
      TSEventHeaderSize : Int := 6;   // TimeStamp + DB number + Address
      TSEventWordSize : Int := 8;
      TSEventByteSize : Int := 16;
      EventByteDataSize : Int := 4;
      MaxStatusReqNb : Int := 87;
      StatusReqListSize : Int := 88;
      StatusWordSize : Int := 100;   // Size of status table
      StatusByteSize : Int := 200;
      MaxReqNumber : Int := 3;
      ReqListSize : Int := 4;
      MaxBufferSize_Event : Int := 2406;
      MaxBufferSize_Status : Int := 18450;
      MaxBufferSize : Int := 16444;   // VARIABLE NOT USED
      EventExtension : Int := 0;
      EventBufferFull : Word := W#16#F001;   // The number of event is exceeded
      WrongDBNumber : Word := W#16#F002;   // The DB number is 0
      WrongEventSize : Word := W#16#F003;   // In Event size <> Event size in buffer
      MainQueueFull : Word := W#16#F004;   // Number of req in the global queue exceeded
      NbTableExceeded : Word := W#16#F005;   // Number of tables exceeded
      NoAccessToStatus : Word := W#16#F007;   // Error during access to one or more status table
      EventAccessError : Word := W#16#F008;   // Error during access of the  event (in application part)
      TimeStampError : Word := W#16#F009;   // READ_CLK error; NOT USED
      SendTimeOutError : Word := W#16#F00A;   // No reaction from network after sendind TSPP frame
      Transmission_Error : Word := W#16#F00B;   // The BSEND error code is added to this value
      BSEND_Timeout : Time := T#100ms;   // Maximum time for the BSEND to send the information in one cycle
   END_VAR


BEGIN
   // Timer for sending events
   #SendEventTimer(
                   IN := NOT (#TimerFlag),
                   PT := #SendEventPeriod
   );
   #TimerFlag := #SendEventTimer.Q;
   #CurrentTime := #SendEventTimer.ET;
   
   // Timer for watch dog
   #WatchDogTimer(
                  IN := #WatchDogMissing,
                  PT := t#10s
   );
   #WatchDogTimeOut := #WatchDogTimer.Q;
   #CurrentTime := #WatchDogTimer.ET;
   
   #Error := FALSE;
   #Error_code := 0;
   #Special_code := 0;
   
   // Initialisation
   IF #Init OR NOT #InitDone THEN
      #ID_NewEvent := 0;   //changed TNR
      #ID_EventSent := 0;   //changed TNR
      #Error_code := 0;
      #Special_code := 0;
      #InitDone := TRUE;
      #Init := FALSE;

      #CurrentEvent := 0;
      #EventBufferSize := 6;   // Taille du header
      #BufferFullSendReq := FALSE;
      #ExtendedBufFull := FALSE;
      #NbOfTables := 0;
      #NbOfStatusReq := 0;
      #SendProblem := FALSE;
      #RemoveFromList := FALSE;
      #WaitDelay := FALSE;
      #SendAllStatusReq := FALSE;
      #SendStatusCommand := FALSE;
      #WatchDogInList := FALSE;
      #StatusInList := FALSE;
      #EventInList := FALSE;
      #TSPPTableFull := FALSE;
      #WorkingGroup := 1;
      #TS_EventBuffer.TS_Data_Length := 0;

      #NbOfRequest := 0;
      FOR #Index := 1 TO #MaxReqNumber + 1 BY 1 DO
         #ReqList[#Index] := 0;
      END_FOR;
      FOR #Index := 1 TO #MaxStatusReqNb + 1 BY 1 DO
         #StatusReqList[#Index] := 0;
      END_FOR;
      FOR #Index := 1 TO #MaxStatusTable BY 1 DO
         #AlreadyInReqList[#Index] := FALSE;
      END_FOR;
     
      #SendPending := FALSE;
      #SendReq := FALSE;
     
      // Create table list
      #NumberOfTableDB := WORD_TO_INT(PEEK_WORD(area := 16#84, dbNumber := #ListOfStatusTable, byteOffset := 0));
      #Index3 := 0;
      FOR #Index := 1 TO #NumberOfTableDB BY 1 DO  // scan of DBs containing tables
         #TablesInDB := WORD_TO_INT(PEEK_WORD(area := 16#84, dbNumber := #ListOfStatusTable, byteOffset := (6 + ((#Index - 1) * 6)))) / #StatusByteSize;
         #LastTableSize := WORD_TO_INT(PEEK_WORD(area := 16#84, dbNumber := #ListOfStatusTable, byteOffset := (6 + ((#Index - 1) * 6)))) MOD #StatusByteSize;
       
         IF #LastTableSize <> 0 THEN
            #TablesInDB := #TablesInDB + 1;
         ELSE
            #LastTableSize := #StatusByteSize;
         END_IF;
         FOR #Index2 := 1 TO #TablesInDB BY 1 DO  // scan of tables inside each DB
            #Index3 := #Index3 + 1;
            IF #Index3 > #MaxStatusTable THEN
               #Error := TRUE;
               #Error_code := #NbTableExceeded;
               #Special_code := 0;
               #InitDone := FALSE;
            ELSE
               #ActualTableList[#Index3].S7_ID := b#16#10;
               #ActualTableList[#Index3].DataType := 2;
               #OldTableList[#Index3].S7_ID := b#16#10;
               #OldTableList[#Index3].DataType := 2;
               IF #Index2 = #TablesInDB THEN // Last table in the DB (can be shorter)
                  #ActualTableList[#Index3].NbOfData := #LastTableSize;
                  #OldTableList[#Index3].NbOfData := #LastTableSize;
               ELSE
                  #ActualTableList[#Index3].NbOfData := #StatusByteSize;
                  #OldTableList[#Index3].NbOfData := #StatusByteSize;
               END_IF;
              
               #ActualTableList[#Index3].DBNumber := WORD_TO_INT(PEEK_WORD(area := 16#84, dbNumber := #ListOfStatusTable, byteOffset := (2 + ((#Index - 1) * 6))));
               #OldTableList[#Index3].DBNumber := WORD_TO_INT(PEEK_WORD(area := 16#84, dbNumber := #ListOfStatusTable, byteOffset := (4 + ((#Index - 1) * 6))));
               #ActualTableList[#Index3].Address := SHL(IN:=DINT_TO_DWORD((INT_TO_DINT(#Index2) - 1) * #StatusByteSize),N:=3) OR dw#16#84000000;
               #OldTableList[#Index3].Address := SHL(IN:=DINT_TO_DWORD((INT_TO_DINT(#Index2) - 1) * #StatusByteSize),N:=3) OR dw#16#84000000;
            END_IF;
         END_FOR;
      END_FOR;
     
      #SendAllStatusReq := TRUE;
      #NbOfTables := #Index3;   // Total number of tables
      #NbOfGroupOfTable := #NbOfTables / #MaxTableInOneSend;   // replaced DIV with /
      #SizeOfLastGroup := #NbOfTables MOD #MaxTableInOneSend;
      IF #SizeOfLastGroup = 0 THEN
         #SizeOfLastGroup := #MaxTableInOneSend;
      ELSE
         #NbOfGroupOfTable := #NbOfGroupOfTable + 1;
      END_IF;
     
      // End create table list

   ELSE
      // End of initialisation     
      // Send all table
      // Copy Tables to OldTables and Put to SendTable queue in order to ..
      // send all tables...
      // 1 - once at the beginning,   
      // 2 - on request,
      // 3 - after communication lost and re-establismnet
      // For cases 1 and 3: wait enable from WinCCOA by IPReceived, that is to guaranty
      // that WinCCOA is ready to receive the tables
      // For case 2: send on the rising edge of SendAllStatus 
      IF (#SendAllStatus AND NOT #OldSendAllStatus) OR (#SendAllStatusReq AND #IPReceived) THEN
         #SendStatusCommand := TRUE;
         #SendAllStatusReq := FALSE;
      END_IF;
      #OldSendAllStatus := #SendAllStatus;
      IF #SendStatusCommand THEN
         FOR #Index := 1 TO #NbOfTables BY 1 DO
            #CreateActual := #ActualTableList[#Index];
            #CreateOld := #OldTableList[#Index];
	      POKE_BLK(
	               area_src := 16#84,
	               dbNumber_src := #CreateActual.DBNumber,
	               byteOffset_src := SHR(IN:=DWORD_TO_DINT(#CreateActual.Address),N:=3),
	               area_dest := 16#84,
	               dbNumber_dest := #CreateOld.DBNumber,
	               byteOffset_dest := SHR(IN:=DWORD_TO_DINT(#CreateOld.Address),N:=3),
	               count := #CreateActual.NbOfData
	      );
	      
	      #Result := 0; // !!
            IF #Result <> 0 THEN
               #Error := TRUE;
               #Error_code := #NoAccessToStatus;
               #Special_code := INT_TO_WORD(#Result);
            ELSE
               IF NOT #AlreadyInReqList[#Index] THEN
                  #NbOfStatusReq := #NbOfStatusReq + 1;
                  #StatusReqList[#NbOfStatusReq] := #Index;
                  #AlreadyInReqList[#Index] := TRUE;
               END_IF;
            END_IF;
         END_FOR;
         #SendStatusCommand := FALSE;
      END_IF;
     
      // Events treatment
      IF #NewEvent THEN
         IF #MultipleEvent THEN
         //#EventListAdr := 0;
         //#NumberOfEvents := WORD_TO_INT(#EventListDB.DW(#EventListAdr));   // corrected parentheses
         #NumberOfEvents := #ID_NewEvent - #ID_EventSent;
         IF #NumberOfEvents < 0 THEN
            #NumberOfEvents := #NumberOfEvents + #ListEventSize;
         END_IF;
         #EventListAdr := 2 + #ID_EventSent * 10;
	      #EventListElement.IDAndType := PEEK_WORD(area := 16#84, dbNumber := #EventListDB, byteOffset := #EventListAdr);
	      #EventListElement.DataAndDBNb := PEEK_DWORD(area := 16#84, dbNumber := #EventListDB, byteOffset := #EventListAdr + 2);
	      #EventListElement.Address := PEEK_DWORD(area := 16#84, dbNumber := #EventListDB, byteOffset := #EventListAdr + 6);
         #EventPtr := #EventListElemPtr;
         ELSE
            #NumberOfEvents := 0;
            #EventPtr := #EventData;
         END_IF;
         #CurrentInEvent := 1;
         REPEAT
            CASE CHAR_TO_INT(BYTE_TO_CHAR(#Event.DataType)) OF
               2:
                  #InEventByteDataSize := #Event.NbOfData;
               4..5:
                  #InEventByteDataSize := #Event.NbOfData * 2;
               6..8:
                  #InEventByteDataSize := #Event.NbOfData * 4;
               ELSE:
                  ;
            END_CASE;
            IF #EventTSIncluded THEN
               #InEventByteDataSize := #InEventByteDataSize - 8;   //data size without time stamp   
            END_IF;
            IF #InEventByteDataSize <> #EventByteDataSize THEN
               #Error := TRUE;
               #Error_code := #WrongEventSize;
               #Special_code := 0;
            ELSIF #Event.DBNumber = 0 THEN
               #Error := TRUE;
               #Error_code := #WrongDBNumber;
               #Special_code := 0;
            ELSIF #CurrentEvent >= #ExtendedNbOfEvent THEN
               #Error := TRUE;
               #Error_code := #EventBufferFull;
               #Special_code := 0;
               #ExtendedBufFull := TRUE;
            ELSE   // Record new event

               #CurrentEvent := #CurrentEvent + 1;
               #TS_EventBuffer.Data[#CurrentEvent].DBNumber := #Event.DBNumber;
               #TempAdr := SHR(IN := #Event.Address, N := 3);
               IF #EventTSIncluded THEN
                  #TS_EventBuffer.Data[#CurrentEvent].Address := DWORD_TO_INT(#TempAdr) + 8;// Address of data  
                  #Event.DataType := 2;
                  #Event.NbOfData := 8;
	          #TS_EventBuffer.Data[#CurrentEvent].TimeStamp := LDT_TO_DT(LWORD_TO_LDT(PEEK_LWORD(area := 16#84, dbNumber := #IN_Event.DBNumber, byteOffset := DWORD_TO_DINT(#IN_Event.Address))));
	          #Result := 0; // !!
                  IF #Result <> 0 THEN
                     #Error := TRUE;
                     #Error_code := #EventAccessError;
                     #Special_code := INT_TO_WORD(#Result);
                  END_IF;
                  #Event.NbOfData := #InEventByteDataSize;   // Size of data only
                  #Event.Address := DINT_TO_DWORD(DWORD_TO_DINT(#Event.Address) + 64);   //8 bytes more
               ELSE
                  #TS_EventBuffer.Data[#CurrentEvent].Address := DWORD_TO_INT(#TempAdr);
                  #Er_Code := RD_SYS_T(#TS_EventBuffer.Data[#CurrentEvent].TimeStamp);   // removed CDT:=  // changed from READ_CLK
               END_IF;
	        FOR #Index4 := 1 TO #Word_in_one_TSEvent DO
	          #TS_EventBuffer.Data[#CurrentEvent].Data[#Index4] := PEEK_WORD(area := 16#84,
	                                                                         dbNumber := #Event.DBNumber,
	                                                                         byteOffset := DWORD_TO_DINT(#TempAdr) + 2 * (#Index4 - 1));
	        END_FOR;
	        #Result := 0; // !!
               IF #Result = 0 THEN
                  #TS_EventBuffer.TS_Data_Length := #TS_EventBuffer.TS_Data_Length + #TSEventWordSize;
                  #EventBufferSize := #EventBufferSize + #TSEventByteSize;
               ELSE
                  #CurrentEvent := #CurrentEvent - 1;
                  #Error := TRUE;
                  #Error_code := #EventAccessError;
                  #Special_code := INT_TO_WORD(#Result);
               END_IF;

               IF #CurrentEvent > #MaxNbOfTSEvent THEN
                  #BufferFullSendReq := TRUE;
               END_IF;
            END_IF;
            IF #MultipleEvent THEN
               #ID_EventSent := #ID_EventSent + 1;   // changed tnr
           
               IF #ID_EventSent > #ListEventSize - 1 THEN
                  #ID_EventSent := 0;
                  #EventListAdr := 2 + #ID_EventSent * 10;
               ELSE
                  #EventListAdr := #EventListAdr + 10;
               END_IF;
	      #EventListElement.IDAndType := PEEK_WORD(area := 16#84, dbNumber := #EventListDB, byteOffset := #EventListAdr);
	      #EventListElement.DataAndDBNb := PEEK_DWORD(area := 16#84, dbNumber := #EventListDB, byteOffset := #EventListAdr + 2);
	      #EventListElement.Address := PEEK_DWORD(area := 16#84, dbNumber := #EventListDB, byteOffset := #EventListAdr + 6);
               #EventPtr := #EventListElemPtr;
               #CurrentInEvent := #CurrentInEvent + 1;
            END_IF;
         UNTIL #CurrentInEvent > #NumberOfEvents OR #Error
         END_REPEAT;
         // DB_EventData.ID_NewEvent := DB_EventData.ID_NewEvent + #NumberOfEvents;//changed tnr
      END_IF;
      IF (#TimerFlag AND #CurrentEvent <> 0) OR #BufferFullSendReq THEN
         IF #NbOfRequest < #MaxReqNumber AND NOT #EventInList THEN
            #NbOfRequest := #NbOfRequest + 1;   // One more event in send queue
            #ReqList[#NbOfRequest] := 1;   // Code 1 = send Event Buffer
            #EventInList := TRUE;
         END_IF;
      END_IF;
      // END manage event
     
      // Status table treatment
      // Scan all tables, and in case of change:
      // copy Actual to Old 
      // put the table number in the send waiting list
      #WorkingGroup := #WorkingGroup + 1;
      IF #WorkingGroup > #NbOfGroupOfTable THEN
         #WorkingGroup := 1;
      END_IF;
      #First := 1 + ((#WorkingGroup - 1) * #MaxTableInOneSend);
      IF #WorkingGroup = #NbOfGroupOfTable THEN
         #Last := #First + #SizeOfLastGroup - 1;
      ELSE
         #Last := #First + #MaxTableInOneSend - 1;
      END_IF;
      FOR #Index := #First TO #Last BY 1 DO
         IF NOT #AlreadyInReqList[#Index] THEN
            #CreateActual := #ActualTableList[#Index];
            #CreateOld := #OldTableList[#Index];
	      FOR #Index4 := 1 TO #CreateActual.NbOfData / 2 DO
	        #LocalActual[#Index4] := PEEK_WORD(area := 16#84,
	                                           dbNumber := #CreateActual.DBNumber,
	                                           byteOffset := DWORD_TO_DINT(SHR(IN:=#CreateActual.Address,N:=3)) + 2 * (#Index4 - 1));
	      END_FOR;
	      #Result := 0; // !!
            IF #Result <> 0 THEN
               #Error := TRUE;
               #Error_code := #NoAccessToStatus;
               #Special_code := INT_TO_WORD(#Result);
               #InitDone := FALSE;
            END_IF;
	      FOR #Index4 := 1 TO #CreateOld.NbOfData / 2 DO
	        #LocalOld[#Index4] := PEEK_WORD(area := 16#84,
	                                        dbNumber := #CreateOld.DBNumber,
	                                        byteOffset := DWORD_TO_DINT(SHR(IN:=#CreateOld.Address,N:=3)) + 2 * (#Index4 - 1));
	      END_FOR;
	      #Result := 0; // !!
            IF #Result <> 0 THEN
               #Error := TRUE;
               #Error_code := #NoAccessToStatus;
               #Special_code := INT_TO_WORD(#Result);
               #InitDone := FALSE;
            END_IF;
            // Check for change in table
            #ChangeInTable := FALSE;
            FOR #Index2 := 1 TO #CreateActual.NbOfData / 2 BY 1 DO
               IF #LocalActual[#Index2] <> #LocalOld[#Index2] THEN
                  #ChangeInTable := TRUE;
               END_IF;
            END_FOR;
         
            IF #ChangeInTable THEN
               #NbOfStatusReq := #NbOfStatusReq + 1;
               #StatusReqList[#NbOfStatusReq] := #Index;   // Table number to send in waiting list
               #AlreadyInReqList[#Index] := TRUE;
            END_IF;
         END_IF;   // IF NOT AlreadyInReqList
      END_FOR;
      // Put TS_Table in the global waiting list
      IF #NbOfStatusReq > 0 AND NOT #StatusInList AND (#NbOfRequest < #MaxReqNumber) THEN
         #NbOfRequest := #NbOfRequest + 1;   // One more event in send queue
         #ReqList[#NbOfRequest] := 2;   // Code 2= send Status Table 
         #StatusInList := TRUE;
      END_IF;
      // END Status table treatment
      // Communication watch-dog treatment
	  IF DWORD_TO_DINT(PEEK_DWORD(area := 16#84, dbNumber := WORD_TO_INT(#IN_WatchDog.DBNumber), byteOffset := 0)) <> -1 THEN // new IP received
	    POKE(area := 16#84,
	               dbNumber := WORD_TO_INT(#IN_WatchDog.DBNumber), 
	               byteOffset := 0, 
	               value := DINT_TO_DWORD(-1));
         #IPReceived := TRUE;
         #WatchDogMissing := FALSE;
         IF #WinCCOANotAlive THEN   // Si WinCCOA etait inactif auparavant
            #SendAllStatusReq := TRUE;   // On renvoie toutes les tables
         END_IF;
         #WinCCOANotAlive := FALSE;
         #TempAdr := SHR(IN := #IN_WatchDog.Address, N := 3);
         #DWNumber := 4;   // Counter address in watch dog DB
         #TempCounter := WORD_TO_INT(PEEK_WORD(area := 16#84, dbNumber := WORD_TO_INT(#IN_WatchDog.DBNumber), byteOffset := #DWNumber));
         #TempCounter := #TempCounter + 1;
         IF #TempCounter < 0 THEN
            #TempCounter := 0;
         END_IF;
         POKE(area := 16#84,
	         dbNumber := WORD_TO_INT(#IN_WatchDog.DBNumber),
	         byteOffset := #DWNumber,
	         value := INT_TO_WORD(#TempCounter));
         #Er_Code := RD_SYS_T(#TSComm_Alive.TimeStamp);   // removed CDT :=; changed from READ_CLK
         #TSComm_Alive.DBNumber := #IN_WatchDog.DBNumber;
         #TSComm_Alive.Address := DWORD_TO_INT(#TempAdr);
         #TSComm_Alive.CommAliveCounter := #TempCounter;
         IF NOT #WatchDogInList THEN
            IF #NbOfRequest < #MaxReqNumber THEN
               #NbOfRequest := #NbOfRequest + 1;   // One more event in send queue
               #ReqList[#NbOfRequest] := 3;   // Code 3= send comm alive Buffer
               #WatchDogInList := TRUE;
            ELSE
               #Error := TRUE;
               #Error_code := #MainQueueFull;
               #Special_code := 0;
            END_IF;
         END_IF;
      ELSE
         #IPReceived := FALSE;
         #WatchDogMissing := TRUE;
      END_IF;
      IF #WatchDogTimeOut THEN
         #WinCCOANotAlive := TRUE;
      END_IF;
      // END comm alive treatment



      // Check if buffer free and if there is data to send   
      IF NOT #SendReq AND NOT #SendPending THEN
         CASE #ReqList[1] OF
            1:   // It's an event
               #RemoveFromList := TRUE;
               #EventInList := FALSE;

               #Src := #TS_EventBuffer;
	        #Result := BLKMOV(SRCBLK := #Src,
	                          DSTBLK => #BSendBuffer   // Struct
	        );
               #BsendLen := INT_TO_WORD(#EventBufferSize);
               #CurrentEvent := 0;
               #EventBufferSize := 6;   //Taille du header
               #TS_EventBuffer.TS_Data_Length := 0;
               #BufferFullSendReq := FALSE;
               #ExtendedBufFull := FALSE;

            2:   // It's a status table
               #RemoveFromList := TRUE;
               #StatusInList := FALSE;
               // Put the status tables from the status queue directly to the BSend buffer  
               #TSPPTableIndex := 1;
               #BSendBuffer.TS_Data_Length := 0;
               #TSPPTableFull := FALSE;
               #BSendBuffer.Nb_of_TSWord := DINT_TO_BYTE(#StatusWordSize);
               WHILE #NbOfStatusReq > 0 AND NOT #TSPPTableFull DO  // There is table to send
                  IF #StatusReqList[1] <> 0 THEN
                     #CreateActual := #ActualTableList[#StatusReqList[1]];
                     #CreateOld := #OldTableList[#StatusReqList[1]];
                     #TempAdr := SHR(IN := #CreateActual.Address, N := 3);
	            FOR #Index4 := 1 TO #CreateActual.NbOfData / 2 DO
	              #BSendBuffer.TableList[#TSPPTableIndex].Table[#Index4] := PEEK_WORD(area := 16#84,
	                                                                                  dbNumber := #CreateActual.DBNumber,
	                                                                                  byteOffset := DWORD_TO_DINT(#TempAdr) + 2 * (#Index4 - 1));
	            END_FOR;
	            POKE_BLK(
	                     area_src := 16#84,
	                     dbNumber_src := #CreateActual.DBNumber,
	                     byteOffset_src := DWORD_TO_DINT(#TempAdr),
	                     area_dest := 16#84,
	                     dbNumber_dest := #CreateOld.DBNumber,
	                     byteOffset_dest := DWORD_TO_DINT(SHR(IN:=#CreateOld.Address,N:=3)),
	                     count := #CreateActual.NbOfData
	            );
                     #Er_Code := RD_SYS_T(#BSendBuffer.TableList[#TSPPTableIndex].TimeStamp);   // removed CDT:= // in place of READ_CLK
                     #BSendBuffer.TableList[#TSPPTableIndex].DBNumber := #CreateActual.DBNumber;
                     #BSendBuffer.TableList[#TSPPTableIndex].Address := WORD_TO_INT(DWORD_TO_WORD(#TempAdr));
                     #BSendBuffer.TS_Data_Length := #BSendBuffer.TS_Data_Length + #StatusWordSize + 6;
                     // Remove table from table waiting list
                     #AlreadyInReqList[#StatusReqList[1]] := FALSE;   // The current table is nomore in list  
                     #TSPPTableIndex := #TSPPTableIndex + 1;
                     IF #TSPPTableIndex > #MaxTableInOneSend THEN
                        #TSPPTableFull := TRUE;
                     END_IF;
                  END_IF;
                  FOR #Index := 1 TO #NbOfTables BY 1 DO
                     #StatusReqList[#Index] := #StatusReqList[#Index + 1];
                  END_FOR;
                  #NbOfStatusReq := #NbOfStatusReq - 1;
               END_WHILE;
               #BsendLen := INT_TO_WORD((#BSendBuffer.TS_Data_Length * 2) + 6);
            3:   // It's a watch dog event
               #WatchDogInList := FALSE;
               #RemoveFromList := TRUE;
               #Result := BLKMOV(
                             SRCBLK := #TSComm_Alive,   // IN: Any
                             DSTBLK => #BSendBuffer   // OUT: Any
               );   // INT
               #BsendLen := 20;
            ELSE:
               ;
         END_CASE;
       
         IF #RemoveFromList THEN
            FOR #Index := 1 TO #NbOfRequest BY 1 DO
               #ReqList[#Index] := #ReqList[#Index + 1];
            END_FOR;
            #NbOfRequest := #NbOfRequest - 1;
            #RemoveFromList := FALSE;
            #SendReq := TRUE;  // Last thing done
         END_IF;
      END_IF;

      #BSEND_TON(IN:=FALSE,
                   PT:=#BSEND_Timeout);
      #Bsend_Done := FALSE;
      #Bsend_Error := FALSE;
        
      WHILE (#SendReq OR #SendPending) AND NOT #Bsend_Error AND NOT #BSEND_TON.Q DO
          #BSEND_TON(IN := TRUE,
                     PT := #BSEND_Timeout);
            
          // Check if last message sent
          #BSEND_DB(REQ := FALSE,   // IN: Bool
                     ID := #SendID0,   // IN: WORD
                     SD_1 := #BSendBuffer,   // INOUT: Any
                     R_ID := DW#16#1,
                     LEN := #BsendLen   // INOUT: WORD
                     );
            
          #Bsend_Done := #BSEND_DB.DONE;   // OUT: Bool
          #Bsend_Error := #BSEND_DB.ERROR;   // OUT: Bool
          #Bsend_Status := #BSEND_DB.STATUS;   // OUT: WORD
            
          IF #Bsend_Done THEN // Transmission of data finished
              #SendPending := FALSE;
              #WaitDelay := FALSE;
              IF #LinkNotEstablish THEN
                  #LinkNotEstablish := FALSE;
                  #SendAllStatusReq := TRUE;
              END_IF;
          ELSE
              IF #TimeOut THEN
                  #SendReq := TRUE;
                  #WaitDelay := TRUE;
                  #NbOfRetry := #NbOfRetry + 1;
              END_IF;
              
              IF #Bsend_Error AND #Bsend_Status = 1 THEN
                  #LinkNotEstablish := TRUE;
              END_IF;
          END_IF;
            
          // Check if there is data in buffer and try to send them
          IF #SendReq THEN
              // Timer for communication time out and retry
              #SendTimeOutTimer(IN := FALSE,
                                PT := t#10s);
              #TimeOut := #SendTimeOutTimer.Q;
              #CurrentTime := #SendTimeOutTimer.ET;
                
              #BSEND_DB(REQ := #SendReq,   // IN: Bool
                         ID := #SendID0,   // IN: WORD
                         SD_1 := #BSendBuffer,   // INOUT: Any
                         R_ID := DW#16#1,
                         LEN := #BsendLen   // INOUT: WORD
                         );
                
              #SendPending := TRUE;
              #SendReq := FALSE;
          END_IF;
      END_WHILE;
        
      // Timer for communication time out and retry
      #SendTimeOutTimer(IN := #SendPending,
                        PT := t#10s);
      #TimeOut := #SendTimeOutTimer.Q;
      #CurrentTime := #SendTimeOutTimer.ET;
        
      IF #ExtendedBufFull THEN // BufferFull information of higher priority ...
          #Error := TRUE;   // it must inform the user that he has ...
          #Error_code := #EventBufferFull;   // to retry to send the event
      ELSIF #Bsend_Error THEN
          #Error := TRUE;
          #Error_code := #Transmission_Error;
          #Special_code := #Bsend_Status;
      ELSIF #WaitDelay THEN
          #Error := TRUE;
          #Error_code := #SendTimeOutError;
          #Special_code := #Bsend_Status;
      END_IF;
   END_IF;   // Else Init

END_FUNCTION_BLOCK



DATA_BLOCK "TSPP_Unicos_DB" "TSPP_Unicos_Manager"
BEGIN

END_DATA_BLOCK
     
