(*Device Logic of FDEU_893_P2701 (RO Recirculation pump P2701) ********** Application: CT2_TIA *****)

FUNCTION FDEU_893_P2701_DL : VOID
TITLE = 'FDEU_893_P2701_DL'
//
// Dependent Logic of FDEU_893_P2701
// 
//
// Master: 	FDEU_893_CCD_RO
// Name: 	FDEU_893_P2701
(*
 Lparam1:	GRAPH_CCD_RO	// Name of the DB of the DB_GLOBAL
 Lparam2:	GRAPH_CCD_RO.S3_03_1.X OR GRAPH_CCD_RO.S3_03_2.X OR GRAPH_CCD_RO.S3_08_2.X	// Condition for AuOnR
 Lparam3:	GRAPH_CCD_RO.S3_03_1.X OR GRAPH_CCD_RO.S3_08_2.X:10   // List of condition:position pairs for positioning        
 Lparam4:	   // Additional condition for positioning
 Lparam5:	
 Lparam6:	 
 Lparam7:	   
 Lparam8:	worktime=FDEU_893_P2701_RTM,FDEU_893_P2701WTRAZ,FDEU_893_P2701_WT,FDEU_893_P2701Offs;anticourtcycle=FDEU_893_P2701_TONACC,FDEU_893_P2701CCRAZ,FDEU_893_CourtCycleDt,FDEU_893_P2701ACCRes;dbglobal=DB_GLOBAL   // List of optional features
 Lparam9:	
 Lparam10: 
SHUTDOWN,STOP,STANDBY,S3_01_1,S3_01_2,S3_01_3,S3_01_4,S3_02,S3_03_1,S3_03_2,S3_04_1,S3_04_2,S3_05_2,S3_05_3,S3_05_4,S3_05_5,S3_05_6,S3_05_7,S3_06_1,S3_06_2,S3_06_3,S3_06_4,S3_06_5,S3_06_6,S3_06_7,S3_07_1,S3_07_2,S3_07_3,S3_07_4,S3_07_5,S3_07_6,S3_07_7,S3_07_8,S3_07_9,S3_08_1,S3_08_2,RO_CIP
*)
AUTHOR: 'UNICOS'
NAME: 'Logic_DL'
FAMILY: 'AnaDO'

VAR_TEMP
   IN       : REAL; //Scaling input
   OUT      : REAL; //Scaling Output, connected to AuPosR
   In_Min   : REAL;
   In_Max   : REAL;
   Out_Min  : REAL;
   Out_Max  : REAL;
   mode_RTM : INT;
END_VAR
BEGIN

// ----------------------------------------------------- USER code <begin>------------------------------------------------------------

(*ON request Management*)
FDEU_893_P2701.AuOnR := GRAPH_CCD_RO.S3_03_1.X OR GRAPH_CCD_RO.S3_03_2.X OR GRAPH_CCD_RO.S3_08_2.X;
    
FDEU_893_P2701.AuOffR:= NOT FDEU_893_P2701.AuOnR;

FDEU_893_FC2701.AuActR:= TRUE;
        
// The AnaDO object "FDEU_893_P2701" is controlled by 1 regulator (FDEU_893_FC2701) and the user selected "Input Scaling"

OUT := FDEU_893_FC2701.OutOV;
IF (GRAPH_CCD_RO.S3_03_1.X OR GRAPH_CCD_RO.S3_08_2.X ) THEN
    FDEU_893_P2701.AuPosR := 10;

ELSE
    FDEU_893_P2701.AuPosR := OUT;
END_IF;


// Ramp Parameters to fill by the user. By default the "Manual Decrease Speed" and "Manual Increase Speed" are taken
FDEU_893_P2701.AuDeSpd := 10; // To complete
FDEU_893_P2701.AuInSpd := 10; // To complete

(* ---------------------------------------  FDEU_893_P2701 Restart CountDown ----------------------------------   *)
    
(*Timer Time assignment*)
FDEU_893_P2701_TONACC.TON (IN := NOT FDEU_893_P2701.OutOnOV AND NOT DB_GLOBAL.FDEU_893_P2701ACrst,
        PT := DINT_TO_TIME ( REAL_TO_DINT (1000.0 * FDEU_893_CourtCycleDt.PosSt)));	

(*Timer Input edge assignment*)
(*When AnaDO off Timer starts*)
(*When Reset Command On Timer Resets*)

IF FDEU_893_P2701CCRAZ.OnSt THEN
    DB_GLOBAL.FDEU_893_P2701ACrst := TRUE;
END_IF;
IF FDEU_893_P2701.OutOnOV THEN
    DB_GLOBAL.FDEU_893_P2701ACrst := FALSE;
END_IF;

(*Count Down evaluation*)

IF NOT FDEU_893_P2701_TONACC.IN OR FDEU_893_P2701_TONACC.Q THEN
    FDEU_893_P2701ACCRes.AuPosR := 0.0;
ELSE
    FDEU_893_P2701ACCRes.AuPosR := FDEU_893_CourtCycleDt.PosSt - ((DINT_TO_REAL (TIME_TO_DINT (FDEU_893_P2701_TONACC.ET))) / 1000.0);
END_IF;

// Falling edge of StartI alarm for anti-court cycle FDEU_893_P2701
DB_GLOBAL.F_FDEU_893_P2701SI := F_EDGE (new := FDEU_893_P2701.StartISt, old := DB_GLOBAL.F_FDEU_893_P2701SIo);

IF DB_GLOBAL.F_FDEU_893_P2701SI AND FDEU_893_P2701.EnRstartSt AND NOT FDEU_893_P2701.TStopISt THEN
    FDEU_893_P2701.AuAlAck := TRUE;
END_IF;

(*Counter of time*)
//Working TIME OF the AnaDO                
IF FDEU_893_P2701WTRAZ.OnSt THEN
    mode_RTM:=2; //Reset
ELSIF FDEU_893_P2701.OnSt THEN
    mode_RTM:=1; //COUNTING
ELSE
    mode_RTM:=0; //STOP
END_IF;

FDEU_893_P2701_RTM(mode:= mode_RTM 	//IN
                    ,NewValue:=0 	//IN
                    );

FDEU_893_P2701_WT.AuPosR := DINT_TO_REAL(FDEU_893_P2701_RTM.CurrentValue)/3600.0 + FDEU_893_P2701Offs.PosSt;


(*IoSimu and IoError*****************)
// The user must connect the IOError and IOSimu from the linked devices ("IN" variable) if proceeds
DB_ERROR_SIMU.FDEU_893_P2701_DL_E := 0; // To complete

DB_ERROR_SIMU.FDEU_893_P2701_DL_S :=  0; // To complete

(*Not configured alarm parameters: Interlock Conditions to fill in according to the logic spec *)
(*Digital Interlock Conditions to fill in according to the logic spec*)
    // Simple Type DA conditions
    // Multiple Type DA conditions

(*Analog Interlock Conditions to fill in according to the logic spec*)
    // Simple Type AA conditions
    // Multiple Type AA conditions

// ----------------------------------------------------- USER code <end>------------------------------------------------------------

(*Configured alarm parameters: Interlock Conditions to fill in according to the logic spec *)
(*Digital Interlock Conditions to fill in according to the logic spec*)
    // Simple Type DA conditions

    (* DEFAUT VARIATEUR POMPE P2701 *)
    FDEU_893_P2701_AL2.I := NOT FDEU_893_P2701DefVar.PosSt;
    FDEU_893_P2701_AL2.IOError := FDEU_893_P2701DefVar.IOErrorW;
    FDEU_893_P2701_AL2.IOSimu := FDEU_893_P2701DefVar.IOSimuW OR FDEU_893_P2701DefVar.FoMoSt;
    FDEU_893_P2701_AL2.PAlDt := REAL_TO_INT(FDEU_893_digAlmDelay.PosSt);

    (* ANTI-COURT CYCLE POMPE P2701 *)
    FDEU_893_P2701_AL3.I := FDEU_893_P2701RestCD.PosSt > 0.0 AND NOT FDEU_893_P2701.OutOnOV;
    FDEU_893_P2701_AL3.IOError := FDEU_893_P2701DO.IOErrorW;
    FDEU_893_P2701_AL3.IOSimu := FDEU_893_P2701DO.IOSimuW OR FDEU_893_P2701DO.FoMoSt;

    (* DISCORDANCE ARRET POMPE P2701 *)
    FDEU_893_P2701_AL5.I := FDEU_893_P2701On.PosSt AND NOT FDEU_893_P2701DO.PosSt;
    FDEU_893_P2701_AL5.IOError := FDEU_893_P2701On.IOErrorW OR
FDEU_893_P2701DO.IOErrorW;
    FDEU_893_P2701_AL5.IOSimu := FDEU_893_P2701On.IOSimuW OR FDEU_893_P2701On.FoMoSt OR
FDEU_893_P2701DO.IOSimuW OR FDEU_893_P2701DO.FoMoSt;
    FDEU_893_P2701_AL5.PAlDt := REAL_TO_INT(FDEU_893_P2701_AL5Dt.PosSt);

    (* DISCORDANCE MARCHE POMPE P2701 *)
    FDEU_893_P2701_AL6.I := NOT FDEU_893_P2701On.PosSt AND FDEU_893_P2701DO.PosSt;
    FDEU_893_P2701_AL6.IOError := FDEU_893_P2701On.IOErrorW OR
FDEU_893_P2701DO.IOErrorW;
    FDEU_893_P2701_AL6.IOSimu := FDEU_893_P2701On.IOSimuW OR FDEU_893_P2701On.FoMoSt OR
FDEU_893_P2701DO.IOSimuW OR FDEU_893_P2701DO.FoMoSt;
    FDEU_893_P2701_AL6.PAlDt := REAL_TO_INT(FDEU_893_P2701_AL6Dt.PosSt);
    // Multiple Type DA conditions

(*Analog Interlock Conditions to fill in according to the logic spec*)
    // Simple Type AA conditions
    // Multiple Type AA conditions


(*Adding of the IoError from the Logic related to FDEU_893_P2701*********)
FDEU_893_P2701.IoError := DB_ERROR_SIMU.FDEU_893_P2701_DL_E OR
       FDEU_893_P2701_AL2.IOErrorW OR
       FDEU_893_P2701_AL3.IOErrorW OR
       FDEU_893_P2701_AL5.IOErrorW OR
       FDEU_893_P2701_AL6.IOErrorW OR
       0;

(*Adding of the IoSimu from the Logic related to FDEU_893_P2701*********)
FDEU_893_P2701.IoSimu := DB_ERROR_SIMU.FDEU_893_P2701_DL_S OR
       FDEU_893_P2701_AL2.IoSimuW OR
       FDEU_893_P2701_AL3.IoSimuW OR
       FDEU_893_P2701_AL5.IoSimuW OR
       FDEU_893_P2701_AL6.IoSimuW OR
       0;

(*instantiation of the AuAlAck for the DigitalAlarm objects related to FDEU_893_P2701*********)
    // Simple Type DA instantiation
    FDEU_893_P2701_AL2.AuAlAck:=FDEU_893_P2701.E_MAlAckR OR FDEU_893_P2701.AuAlAck;

    FDEU_893_P2701_AL3.AuAlAck:=FDEU_893_P2701.E_MAlAckR OR FDEU_893_P2701.AuAlAck;

    FDEU_893_P2701_AL5.AuAlAck:=FDEU_893_P2701.E_MAlAckR OR FDEU_893_P2701.AuAlAck;

    FDEU_893_P2701_AL6.AuAlAck:=FDEU_893_P2701.E_MAlAckR OR FDEU_893_P2701.AuAlAck;
    // Multiple Type DA instantiation
(*instantiation of the AuAlAck for the AnalogAlarm objects related to FDEU_893_P2701*********)
    // Simple Type AA instantiation
    // Multiple Type AA instantiation

(*Interlock*************************)
FDEU_893_P2701.StartI := 
    // Start Interlock for DA
    FDEU_893_P2701_AL3.ISt OR

    // Start Interlock for AA
    0;
FDEU_893_P2701.FuStopI := 
    // Full Stop Interlock for DA
    FDEU_893_P2701_AL6.ISt OR

    // Full Stop Interlock for AA
    0;
FDEU_893_P2701.TStopI := 
    // Temporary Stop Interlock for DA
    // Temporary Stop Interlock for AA
    0;
FDEU_893_P2701.Al := 
    // Alarm for DA
    FDEU_893_P2701_AL2.ISt OR

    FDEU_893_P2701_AL5.ISt OR

    // Alarm for AA
    0;

(*Blocked Alarm warning ********************)
FDEU_893_P2701.AlB := 
    FDEU_893_P2701_AL2.MAlBRSt OR

    FDEU_893_P2701_AL3.MAlBRSt OR

    FDEU_893_P2701_AL5.MAlBRSt OR

    FDEU_893_P2701_AL6.MAlBRSt OR

    0;

END_FUNCTION