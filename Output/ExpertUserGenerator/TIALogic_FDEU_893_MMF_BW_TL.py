# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)


def TLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    Decorator = ucpc_library.shared_decorator.ExpressionDecorator()
    # Automatically generated variables
    

# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeTIALogic('''
FUNCTION $name$_TL : VOID
TITLE = '$name$_TL'
//
// Transition Logic of $name$
//
// Lparam1:    $Lparam1$
// Lparam2:    $Lparam2$
// Lparam3:    $Lparam3$
// Lparam4:    $Lparam4$
// Lparam5:    $Lparam5$
// Lparam6:    $Lparam6$
// Lparam7:    $Lparam7$
// Lparam8:    $Lparam8$
// Lparam9:    $Lparam9$
// Lparam10:    $Lparam10$
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_TL'
FAMILY: 'TL'
VAR_TEMP
    pco_transitions0 : WORD;
    pco_transitions1 : WORD;
    pco_transitions2 : WORD;
    pco_transitions3 : WORD;
    pco_transitions4 : WORD;


END_VAR
BEGIN

''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    thePlugin.writeTIALogic('''

DB_ERROR_SIMU.$name$_TL_E := 0 ; // To complete
DB_ERROR_SIMU.$name$_TL_S := 0 ; // To complete

''')

    thePlugin.writeTIALogic('''(* Automatic generated code from StepperSpecs.xlsx using the TIA_Expert_Stepper_Template <begin> *)
''')

    thePlugin.writeTIALogic('''(* VARIABLES COMPUTATION *)



''')

    thePlugin.writeTIALogic(Decorator.decorateExpression('''(* TRANSITIONS COMPUTATION *)

(* 1 TO 2 *) GRAPH_MMF_BW.INIT_B1_01_1A := GRAPH_MMF_UF.MMF_BW.X;
(* 2 TO 1 *) GRAPH_MMF_BW.B1_01_1A_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 2 TO 3 *) GRAPH_MMF_BW.B1_01_1A_B1_01_2A := FDEU_893_AQE1001.OnSt;
(* 3 TO 1 *) GRAPH_MMF_BW.B1_01_2A_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 3 TO 4 *) GRAPH_MMF_BW.B1_01_2A_B1_01_3A := FDEU_893_AQE1211.OnSt AND FDEU_893_AQE1212.OnSt AND FDEU_893_AQE1204.OnSt AND FDEU_893_AQE1206.OnSt;
(* 4 TO 1 *) GRAPH_MMF_BW.B1_01_3A_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 4 TO 5 *) GRAPH_MMF_BW.B1_01_3A_B1_02A := FDEU_893_P1101.OnSt AND FDEU_893_MFV5301.OnSt AND TON_MMF_BW_B1_01_3A.Q;
(* 5 TO 1 *) GRAPH_MMF_BW.B1_02A_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 5 TO 6 *) GRAPH_MMF_BW.B1_02A_B1_03_1A := TON_MMF_BW_B1_02A_AIR.Q;
(* 6 TO 1 *) GRAPH_MMF_BW.B1_03_1A_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 6 TO 7 *) GRAPH_MMF_BW.B1_03_1A_B1_03_2A := NOT FDEU_893_MFV5301.OnSt AND TON_MMF_BW_B1_03_1A.Q;
(* 7 TO 1 *) GRAPH_MMF_BW.B1_03_2A_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 7 TO 8 *) GRAPH_MMF_BW.B1_03_2A_B1_04_1A := FDEU_893_AQE1206.OffSt;
(* 8 TO 1 *) GRAPH_MMF_BW.B1_04_1A_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 8 TO 9 *) GRAPH_MMF_BW.B1_04_1A_B1_04_2A := FDEU_893_AQE1203.OnSt;
(* 9 TO 1 *) GRAPH_MMF_BW.B1_04_2A_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 9 TO 10 *) GRAPH_MMF_BW.B1_04_2A_B1_05A := FDEU_893_P5101.OnSt AND TON_MMF_BW_B1_04_2A.Q;
(* 10 TO 1 *) GRAPH_MMF_BW.B1_05A_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 10 TO 11 *) GRAPH_MMF_BW.B1_05A_B1_06_1A := TON_MMF_BW_B1_05A_BW.Q;
(* 11 TO 1 *) GRAPH_MMF_BW.B1_06_1A_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 11 TO 12 *) GRAPH_MMF_BW.B1_06_1A_B1_06_2A := NOT FDEU_893_P1101.OnSt AND NOT FDEU_893_P5101.OnSt AND TON_MMF_BW_B1_06_1A.Q;
(* 12 TO 1 *) GRAPH_MMF_BW.B1_06_2A_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 12 TO 13 *) GRAPH_MMF_BW.B1_06_2A_B1_07_1A := FDEU_893_AQE1211.OffSt AND FDEU_893_AQE1212.OffSt AND FDEU_893_AQE1203.OffSt AND FDEU_893_AQE1204.OffSt;
(* 13 TO 1 *) GRAPH_MMF_BW.B1_07_1A_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 13 TO 14 *) GRAPH_MMF_BW.B1_07_1A_B1_07_2A := FDEU_893_AQE1201.OnSt AND FDEU_893_AQE1205.OnSt;
(* 14 TO 1 *) GRAPH_MMF_BW.B1_07_2A_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 14 TO 15 *) GRAPH_MMF_BW.B1_07_2A_B1_08A := FDEU_893_P1101.OnSt AND TON_MMF_BW_B1_07_2A.Q;
(* 15 TO 1 *) GRAPH_MMF_BW.B1_08A_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 15 TO 16 *) GRAPH_MMF_BW.B1_08A_B1_09_1A := TON_MMF_BW_B1_08A_BWR.Q;
(* 16 TO 1 *) GRAPH_MMF_BW.B1_09_1A_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 16 TO 17 *) GRAPH_MMF_BW.B1_09_1A_B1_09_2A := NOT FDEU_893_P1101.OnSt AND TON_MMF_BW_B1_09_1A.Q;
(* 17 TO 1 *) GRAPH_MMF_BW.B1_09_2A_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 17 TO 18 *) GRAPH_MMF_BW.B1_09_2A_B1_01_2B := FDEU_893_AQE1201.OffSt AND FDEU_893_AQE1205.OffSt;
(* 18 TO 1 *) GRAPH_MMF_BW.B1_01_2B_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 18 TO 19 *) GRAPH_MMF_BW.B1_01_2B_B1_01_3B := FDEU_893_AQE1201.OnSt AND FDEU_893_AQE1202.OnSt AND FDEU_893_AQE1214.OnSt AND FDEU_893_AQE1216.OnSt;
(* 19 TO 1 *) GRAPH_MMF_BW.B1_01_3B_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 19 TO 20 *) GRAPH_MMF_BW.B1_01_3B_B1_02B := FDEU_893_P1101.OnSt AND FDEU_893_MFV5301.OnSt AND TON_MMF_BW_B1_01_3B.Q;
(* 20 TO 1 *) GRAPH_MMF_BW.B1_02B_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 20 TO 21 *) GRAPH_MMF_BW.B1_02B_B1_03_1B := TON_MMF_BW_B1_02B_AIR.Q;
(* 21 TO 1 *) GRAPH_MMF_BW.B1_03_1B_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 21 TO 22 *) GRAPH_MMF_BW.B1_03_1B_B1_03_2B := NOT FDEU_893_MFV5301.OnSt AND TON_MMF_BW_B1_03_1B.Q;
(* 22 TO 1 *) GRAPH_MMF_BW.B1_03_2B_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 22 TO 23 *) GRAPH_MMF_BW.B1_03_2B_B1_04_1B := FDEU_893_AQE1216.OffSt;
(* 23 TO 1 *) GRAPH_MMF_BW.B1_04_1B_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 23 TO 24 *) GRAPH_MMF_BW.B1_04_1B_B1_04_2B := FDEU_893_AQE1213.OnSt;
(* 24 TO 1 *) GRAPH_MMF_BW.B1_04_2B_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 24 TO 25 *) GRAPH_MMF_BW.B1_04_2B_B1_05B := FDEU_893_P5101.OnSt AND TON_MMF_BW_B1_04_2B.Q;
(* 25 TO 1 *) GRAPH_MMF_BW.B1_05B_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 25 TO 26 *) GRAPH_MMF_BW.B1_05B_B1_06_1B := TON_MMF_BW_B1_05B_BW.Q;
(* 26 TO 1 *) GRAPH_MMF_BW.B1_06_1B_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 26 TO 27 *) GRAPH_MMF_BW.B1_06_1B_B1_06_2B := NOT FDEU_893_P1101.OnSt AND NOT FDEU_893_P5101.OnSt AND TON_MMF_BW_B1_06_1B.Q;
(* 27 TO 1 *) GRAPH_MMF_BW.B1_06_2B_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 27 TO 28 *) GRAPH_MMF_BW.B1_06_2B_B1_07_1B := FDEU_893_AQE1201.OffSt AND FDEU_893_AQE1202.OffSt AND FDEU_893_AQE1213.OffSt AND FDEU_893_AQE1214.OffSt;
(* 28 TO 1 *) GRAPH_MMF_BW.B1_07_1B_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 28 TO 29 *) GRAPH_MMF_BW.B1_07_1B_B1_07_2B := FDEU_893_AQE1211.OnSt AND FDEU_893_AQE1215.OnSt;
(* 29 TO 1 *) GRAPH_MMF_BW.B1_07_2B_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 29 TO 30 *) GRAPH_MMF_BW.B1_07_2B_B1_08B := FDEU_893_P1101.OnSt AND TON_MMF_BW_B1_07_2B.Q;
(* 30 TO 1 *) GRAPH_MMF_BW.B1_08B_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 30 TO 31 *) GRAPH_MMF_BW.B1_08B_B1_09_1B := TON_MMF_BW_B1_08B_BWR.Q;
(* 31 TO 1 *) GRAPH_MMF_BW.B1_09_1B_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 31 TO 32 *) GRAPH_MMF_BW.B1_09_1B_B1_09_2B := NOT FDEU_893_P1101.OnSt AND TON_MMF_BW_B1_09_1B.Q;
(* 32 TO 1 *) GRAPH_MMF_BW.B1_09_2B_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 32 TO 33 *) GRAPH_MMF_BW.B1_09_2B_B1_09_3B := FDEU_893_AQE1211.OffSt AND FDEU_893_AQE1215.OffSt;
(* 33 TO 1 *) GRAPH_MMF_BW.B1_09_3B_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
(* 33 TO 34 *) GRAPH_MMF_BW.B1_09_3B_SYNC := FDEU_893_AQE1001.OffSt OR FDEU_893_AQE1001.AuOnRSt;
(* 34 TO 1 *) GRAPH_MMF_BW.SYNC_INIT := NOT GRAPH_MMF_UF.MMF_BW.X;
'''))

    thePlugin.writeTIALogic('''
(* WORD STATUS FOR STEPS *)
FDEU_893_MMF_BW_St.AuPosR := INT_TO_WORD(GRAPH_MMF_BW.S_NO);
''')

    thePlugin.writeTIALogic('''
(* WORD STATUS FOR TRANSITIONS *)
pco_transitions0.%X0  := GRAPH_MMF_BW.INIT_B1_01_1A;
pco_transitions0.%X1  := GRAPH_MMF_BW.B1_01_1A_INIT;
pco_transitions0.%X2  := GRAPH_MMF_BW.B1_01_1A_B1_01_2A;
pco_transitions0.%X3  := GRAPH_MMF_BW.B1_01_2A_INIT;
pco_transitions0.%X4  := GRAPH_MMF_BW.B1_01_2A_B1_01_3A;
pco_transitions0.%X5  := GRAPH_MMF_BW.B1_01_3A_INIT;
pco_transitions0.%X6  := GRAPH_MMF_BW.B1_01_3A_B1_02A;
pco_transitions0.%X7  := GRAPH_MMF_BW.B1_02A_INIT;
pco_transitions0.%X8  := GRAPH_MMF_BW.B1_02A_B1_03_1A;
pco_transitions0.%X9  := GRAPH_MMF_BW.B1_03_1A_INIT;
pco_transitions0.%X10 := GRAPH_MMF_BW.B1_03_1A_B1_03_2A;
pco_transitions0.%X11 := GRAPH_MMF_BW.B1_03_2A_INIT;
pco_transitions0.%X12 := GRAPH_MMF_BW.B1_03_2A_B1_04_1A;
pco_transitions0.%X13 := GRAPH_MMF_BW.B1_04_1A_INIT;
pco_transitions0.%X14 := GRAPH_MMF_BW.B1_04_1A_B1_04_2A;
pco_transitions0.%X15 := GRAPH_MMF_BW.B1_04_2A_INIT;
pco_transitions1.%X0  := GRAPH_MMF_BW.B1_04_2A_B1_05A;
pco_transitions1.%X1  := GRAPH_MMF_BW.B1_05A_INIT;
pco_transitions1.%X2  := GRAPH_MMF_BW.B1_05A_B1_06_1A;
pco_transitions1.%X3  := GRAPH_MMF_BW.B1_06_1A_INIT;
pco_transitions1.%X4  := GRAPH_MMF_BW.B1_06_1A_B1_06_2A;
pco_transitions1.%X5  := GRAPH_MMF_BW.B1_06_2A_INIT;
pco_transitions1.%X6  := GRAPH_MMF_BW.B1_06_2A_B1_07_1A;
pco_transitions1.%X7  := GRAPH_MMF_BW.B1_07_1A_INIT;
pco_transitions1.%X8  := GRAPH_MMF_BW.B1_07_1A_B1_07_2A;
pco_transitions1.%X9  := GRAPH_MMF_BW.B1_07_2A_INIT;
pco_transitions1.%X10 := GRAPH_MMF_BW.B1_07_2A_B1_08A;
pco_transitions1.%X11 := GRAPH_MMF_BW.B1_08A_INIT;
pco_transitions1.%X12 := GRAPH_MMF_BW.B1_08A_B1_09_1A;
pco_transitions1.%X13 := GRAPH_MMF_BW.B1_09_1A_INIT;
pco_transitions1.%X14 := GRAPH_MMF_BW.B1_09_1A_B1_09_2A;
pco_transitions1.%X15 := GRAPH_MMF_BW.B1_09_2A_INIT;
pco_transitions2.%X0  := GRAPH_MMF_BW.B1_09_2A_B1_01_2B;
pco_transitions2.%X1  := GRAPH_MMF_BW.B1_01_2B_INIT;
pco_transitions2.%X2  := GRAPH_MMF_BW.B1_01_2B_B1_01_3B;
pco_transitions2.%X3  := GRAPH_MMF_BW.B1_01_3B_INIT;
pco_transitions2.%X4  := GRAPH_MMF_BW.B1_01_3B_B1_02B;
pco_transitions2.%X5  := GRAPH_MMF_BW.B1_02B_INIT;
pco_transitions2.%X6  := GRAPH_MMF_BW.B1_02B_B1_03_1B;
pco_transitions2.%X7  := GRAPH_MMF_BW.B1_03_1B_INIT;
pco_transitions2.%X8  := GRAPH_MMF_BW.B1_03_1B_B1_03_2B;
pco_transitions2.%X9  := GRAPH_MMF_BW.B1_03_2B_INIT;
pco_transitions2.%X10 := GRAPH_MMF_BW.B1_03_2B_B1_04_1B;
pco_transitions2.%X11 := GRAPH_MMF_BW.B1_04_1B_INIT;
pco_transitions2.%X12 := GRAPH_MMF_BW.B1_04_1B_B1_04_2B;
pco_transitions2.%X13 := GRAPH_MMF_BW.B1_04_2B_INIT;
pco_transitions2.%X14 := GRAPH_MMF_BW.B1_04_2B_B1_05B;
pco_transitions2.%X15 := GRAPH_MMF_BW.B1_05B_INIT;
pco_transitions3.%X0  := GRAPH_MMF_BW.B1_05B_B1_06_1B;
pco_transitions3.%X1  := GRAPH_MMF_BW.B1_06_1B_INIT;
pco_transitions3.%X2  := GRAPH_MMF_BW.B1_06_1B_B1_06_2B;
pco_transitions3.%X3  := GRAPH_MMF_BW.B1_06_2B_INIT;
pco_transitions3.%X4  := GRAPH_MMF_BW.B1_06_2B_B1_07_1B;
pco_transitions3.%X5  := GRAPH_MMF_BW.B1_07_1B_INIT;
pco_transitions3.%X6  := GRAPH_MMF_BW.B1_07_1B_B1_07_2B;
pco_transitions3.%X7  := GRAPH_MMF_BW.B1_07_2B_INIT;
pco_transitions3.%X8  := GRAPH_MMF_BW.B1_07_2B_B1_08B;
pco_transitions3.%X9  := GRAPH_MMF_BW.B1_08B_INIT;
pco_transitions3.%X10 := GRAPH_MMF_BW.B1_08B_B1_09_1B;
pco_transitions3.%X11 := GRAPH_MMF_BW.B1_09_1B_INIT;
pco_transitions3.%X12 := GRAPH_MMF_BW.B1_09_1B_B1_09_2B;
pco_transitions3.%X13 := GRAPH_MMF_BW.B1_09_2B_INIT;
pco_transitions3.%X14 := GRAPH_MMF_BW.B1_09_2B_B1_09_3B;
pco_transitions3.%X15 := GRAPH_MMF_BW.B1_09_3B_INIT;
pco_transitions4.%X0  := GRAPH_MMF_BW.B1_09_3B_SYNC;
pco_transitions4.%X1  := GRAPH_MMF_BW.SYNC_INIT;
pco_transitions4.%X2  := false;
pco_transitions4.%X3  := false;
pco_transitions4.%X4  := false;
pco_transitions4.%X5  := false;
pco_transitions4.%X6  := false;
pco_transitions4.%X7  := false;
pco_transitions4.%X8  := false;
pco_transitions4.%X9  := false;
pco_transitions4.%X10 := false;
pco_transitions4.%X11 := false;
pco_transitions4.%X12 := false;
pco_transitions4.%X13 := false;
pco_transitions4.%X14 := false;
pco_transitions4.%X15 := false;


FDEU_893_MMF_BW_Tr0.AuPosR := pco_transitions0;
FDEU_893_MMF_BW_Tr1.AuPosR := pco_transitions1;
FDEU_893_MMF_BW_Tr2.AuPosR := pco_transitions2;
FDEU_893_MMF_BW_Tr3.AuPosR := pco_transitions3;
FDEU_893_MMF_BW_Tr4.AuPosR := pco_transitions4;

''')

    thePlugin.writeTIALogic('''
(*1=1- INIT,2=2- B1_01_1A (FCA1201) - Open / Wait Inlet Valve,3=3- B1_01_2A (FCA1201) - Open / Wait Air Backwash Valves & Service valve,4=4- B1_01_3A (FCA1201) - Start Air Blower & Feed pump,5=5- B1_02A (Air BW FCA1201) - Operation Air Backwash,6=6- B1_03_1A (FCA1201) - Stop Air Blower,7=7- B1_03_2A (FCA1201) - Close / Wait Air Backwash Valves,8=8- B1_04_1A (FCA1201) - Open / Wait Backwash Valves,9=9- B1_04_2A (FCA1201) - Start Backwash Pump,10=10- B1_05A (BW FCA1201) - Operation Backwash,11=11- B1_06_1A (FCA1201) - Stop Backwash Pump & Feed pump,12=12- B1_06_2A (FCA1201) - Close / Wait Backwash Valves & Service valve,13=13- B1_07_1A (FCA1201) - Open / Wait Rinse Valves,14=14- B1_07_2A (FCA1201) - Start Feed Pump,15=15- B1_08A (Rinse FCA1201) - Operation Rinse,16=16- B1_09_1A (FCA1201) - Stop Feed Pump,17=17- B1_09_2A (FCA1201) - Close / Wait Rinse Valves,18=18- B1_01_2B (FCA1211) - Open / Wait Air Backwash Valves & Service valve,19=19- B1_01_3B (FCA1211) - Start Air Blower & Feed pump,20=20- B1_02B (Air BW FCA1211) - Operation Air Backwash,21=21- B1_03_1B (FCA1211) - Stop Air Blower,22=22- B1_03_2B (FCA1211) - Close / Wait Air Backwash Valves,23=23- B1_04_1B (FCA1211) - Open / Wait Backwash Valves,24=24- B1_04_2B (FCA1211) - Start Backwash Pump,25=25- B1_05B (BW FCA1211) - Operation Backwash,26=26- B1_06_1B (FCA1211) - Stop Backwash Pump & Feed pump,27=27- B1_06_2B (FCA1211) - Close / Wait Backwash Valves & Service valve,28=28- B1_07_1B (FCA1211) - Open / Wait Rinse Valves,29=29- B1_07_2B (FCA1211) - Start Feed Pump,30=30- B1_08B (Rinse FCA1211) - Operation Rinse,31=31- B1_09_1B (FCA1211) - Stop Feed Pump,32=32- B1_09_2B (FCA1211) - Close / Wait Rinse Valves,33=33- B1_09_3B (FCA1211) - Close / Wait Inlet Valve,34=34- SYNC*)''')

    thePlugin.writeTIALogic('''
(* Automatic generated code from StepperSpecs.xlsx using the TIA_Expert_Stepper_Template <end> *)''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
