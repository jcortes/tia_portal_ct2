# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)


def TLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    Decorator = ucpc_library.shared_decorator.ExpressionDecorator()
    # Automatically generated variables
    

# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeTIALogic('''
FUNCTION $name$_TL : VOID
TITLE = '$name$_TL'
//
// Transition Logic of $name$
//
// Lparam1:    $Lparam1$
// Lparam2:    $Lparam2$
// Lparam3:    $Lparam3$
// Lparam4:    $Lparam4$
// Lparam5:    $Lparam5$
// Lparam6:    $Lparam6$
// Lparam7:    $Lparam7$
// Lparam8:    $Lparam8$
// Lparam9:    $Lparam9$
// Lparam10:    $Lparam10$
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_TL'
FAMILY: 'TL'
VAR_TEMP
    pco_transitions0 : WORD;
    pco_transitions1 : WORD;
    pco_transitions2 : WORD;


END_VAR
BEGIN

''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    thePlugin.writeTIALogic('''

DB_ERROR_SIMU.$name$_TL_E := 0 ; // To complete
DB_ERROR_SIMU.$name$_TL_S := 0 ; // To complete

''')

    thePlugin.writeTIALogic('''(* Automatic generated code from StepperSpecs.xlsx using the TIA_Expert_Stepper_Template <begin> *)
''')

    thePlugin.writeTIALogic('''(* VARIABLES COMPUTATION *)



''')

    thePlugin.writeTIALogic(Decorator.decorateExpression('''(* TRANSITIONS COMPUTATION *)

(* 1 TO 2 *) GRAPH_UF_ACBW.INIT_B4_01_1 := GRAPH_MMF_UF.UF_AC_BW.X;
(* 2 TO 1 *) GRAPH_UF_ACBW.B4_01_1_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 2 TO 3 *) GRAPH_UF_ACBW.B4_01_1_B4_01_2 := FDEU_893_AQE1305.OnSt AND FDEU_893_AQE5301.OnSt;
(* 3 TO 1 *) GRAPH_UF_ACBW.B4_01_2_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 3 TO 4 *) GRAPH_UF_ACBW.B4_01_2_B4_02 := FDEU_893_MFV5301.OnSt AND TON_UF_ACBW_B4_01_2.Q;
(* 4 TO 1 *) GRAPH_UF_ACBW.B4_02_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 4 TO 5 *) GRAPH_UF_ACBW.B4_02_B4_03_1 := TON_UF_ACBW_B4_02_AIR.Q;
(* 5 TO 1 *) GRAPH_UF_ACBW.B4_03_1_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 5 TO 6 *) GRAPH_UF_ACBW.B4_03_1_B4_03_2 := NOT FDEU_893_MFV5301.OnSt AND TON_UF_ACBW_B4_03_1.Q;
(* 6 TO 1 *) GRAPH_UF_ACBW.B4_03_2_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 6 TO 7 *) GRAPH_UF_ACBW.B4_03_2_B4_04 := FDEU_893_AQE5301.OffSt;
(* 7 TO 1 *) GRAPH_UF_ACBW.B4_04_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 7 TO 8 *) GRAPH_UF_ACBW.B4_04_B4_05 := FDEU_893_AQE1304.OnSt ;
(* 8 TO 1 *) GRAPH_UF_ACBW.B4_05_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 8 TO 9 *) GRAPH_UF_ACBW.B4_05_B4_06 := TON_UF_ACBW_B4_05_PUR.Q;
(* 9 TO 1 *) GRAPH_UF_ACBW.B4_06_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 9 TO 10 *) GRAPH_UF_ACBW.B4_06_B4_07_1 := FDEU_893_AQE1304.OffSt ;
(* 10 TO 1 *) GRAPH_UF_ACBW.B4_07_1_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 10 TO 11 *) GRAPH_UF_ACBW.B4_07_1_B4_07_2 := FDEU_893_AQE1303.OnSt ;
(* 11 TO 1 *) GRAPH_UF_ACBW.B4_07_2_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 11 TO 12 *) GRAPH_UF_ACBW.B4_07_2_B4_07_3 := FDEU_893_P5201.OnSt AND TON_UF_ACBW_B4_07_2.Q;
(* 12 TO 1 *) GRAPH_UF_ACBW.B4_07_3_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 12 TO 13 *) GRAPH_UF_ACBW.B4_07_3_B4_08 := (FDEU_893_DP7101.OnSt OR NOT FDEU_893_DP7101.EnRstartSt) AND TON_UF_ACBW_B4_07_3.Q;
(* 13 TO 1 *) GRAPH_UF_ACBW.B4_08_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 13 TO 14 *) GRAPH_UF_ACBW.B4_08_B4_09_1 := TON_UF_ACBW_B4_08_TOP.Q;
(* 14 TO 1 *) GRAPH_UF_ACBW.B4_09_1_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 14 TO 15 *) GRAPH_UF_ACBW.B4_09_1_B4_09_2 := (NOT FDEU_893_DP7101.OnSt OR NOT FDEU_893_DP7101.EnRstartSt) AND TON_UF_ACBW_B4_09_1.Q;
(* 15 TO 1 *) GRAPH_UF_ACBW.B4_09_2_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 15 TO 16 *) GRAPH_UF_ACBW.B4_09_2_B4_09_3 := NOT FDEU_893_P5201.OnSt AND TON_UF_ACBW_B4_09_2.Q;
(* 16 TO 1 *) GRAPH_UF_ACBW.B4_09_3_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 16 TO 17 *) GRAPH_UF_ACBW.B4_09_3_B4_10_1 := FDEU_893_AQE1305.OffSt;
(* 17 TO 1 *) GRAPH_UF_ACBW.B4_10_1_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 17 TO 18 *) GRAPH_UF_ACBW.B4_10_1_B4_10_2 := FDEU_893_AQE1304.OnSt ;
(* 18 TO 1 *) GRAPH_UF_ACBW.B4_10_2_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 18 TO 19 *) GRAPH_UF_ACBW.B4_10_2_B4_10_3 := FDEU_893_P5201.OnSt AND TON_UF_ACBW_B4_10_2.Q;
(* 19 TO 1 *) GRAPH_UF_ACBW.B4_10_3_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 19 TO 20 *) GRAPH_UF_ACBW.B4_10_3_B4_11 := (FDEU_893_DP7101.OnSt OR NOT FDEU_893_DP7101.EnRstartSt) AND TON_UF_ACBW_B4_10_3.Q;
(* 20 TO 1 *) GRAPH_UF_ACBW.B4_11_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 20 TO 21 *) GRAPH_UF_ACBW.B4_11_B4_12_1 := TON_UF_ACBW_B4_11_BOT.Q;
(* 21 TO 1 *) GRAPH_UF_ACBW.B4_12_1_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 21 TO 22 *) GRAPH_UF_ACBW.B4_12_1_B4_12_2 := (NOT FDEU_893_DP7101.OnSt OR NOT FDEU_893_DP7101.EnRstartSt) AND TON_UF_ACBW_B4_12_1.Q;
(* 22 TO 1 *) GRAPH_UF_ACBW.B4_12_2_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 22 TO 23 *) GRAPH_UF_ACBW.B4_12_2_B4_12_3 := NOT FDEU_893_P5201.OnSt AND TON_UF_ACBW_B4_12_2.Q;
(* 23 TO 1 *) GRAPH_UF_ACBW.B4_12_3_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 23 TO 24 *) GRAPH_UF_ACBW.B4_12_3_B4_13 := FDEU_893_AQE1303.OffSt AND FDEU_893_AQE1304.OffSt;
(* 24 TO 1 *) GRAPH_UF_ACBW.B4_13_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
(* 24 TO 25 *) GRAPH_UF_ACBW.B4_13_SYNC := TON_UF_ACBW_B4_13.Q;
(* 25 TO 1 *) GRAPH_UF_ACBW.SYNC_INIT := NOT GRAPH_MMF_UF.UF_AC_BW.X;
'''))

    thePlugin.writeTIALogic('''
(* WORD STATUS FOR STEPS *)
FDEU_893_UF_ACBW_St.AuPosR := INT_TO_WORD(GRAPH_UF_ACBW.S_NO);
''')

    thePlugin.writeTIALogic('''
(* WORD STATUS FOR TRANSITIONS *)
pco_transitions0.%X0  := GRAPH_UF_ACBW.INIT_B4_01_1;
pco_transitions0.%X1  := GRAPH_UF_ACBW.B4_01_1_INIT;
pco_transitions0.%X2  := GRAPH_UF_ACBW.B4_01_1_B4_01_2;
pco_transitions0.%X3  := GRAPH_UF_ACBW.B4_01_2_INIT;
pco_transitions0.%X4  := GRAPH_UF_ACBW.B4_01_2_B4_02;
pco_transitions0.%X5  := GRAPH_UF_ACBW.B4_02_INIT;
pco_transitions0.%X6  := GRAPH_UF_ACBW.B4_02_B4_03_1;
pco_transitions0.%X7  := GRAPH_UF_ACBW.B4_03_1_INIT;
pco_transitions0.%X8  := GRAPH_UF_ACBW.B4_03_1_B4_03_2;
pco_transitions0.%X9  := GRAPH_UF_ACBW.B4_03_2_INIT;
pco_transitions0.%X10 := GRAPH_UF_ACBW.B4_03_2_B4_04;
pco_transitions0.%X11 := GRAPH_UF_ACBW.B4_04_INIT;
pco_transitions0.%X12 := GRAPH_UF_ACBW.B4_04_B4_05;
pco_transitions0.%X13 := GRAPH_UF_ACBW.B4_05_INIT;
pco_transitions0.%X14 := GRAPH_UF_ACBW.B4_05_B4_06;
pco_transitions0.%X15 := GRAPH_UF_ACBW.B4_06_INIT;
pco_transitions1.%X0  := GRAPH_UF_ACBW.B4_06_B4_07_1;
pco_transitions1.%X1  := GRAPH_UF_ACBW.B4_07_1_INIT;
pco_transitions1.%X2  := GRAPH_UF_ACBW.B4_07_1_B4_07_2;
pco_transitions1.%X3  := GRAPH_UF_ACBW.B4_07_2_INIT;
pco_transitions1.%X4  := GRAPH_UF_ACBW.B4_07_2_B4_07_3;
pco_transitions1.%X5  := GRAPH_UF_ACBW.B4_07_3_INIT;
pco_transitions1.%X6  := GRAPH_UF_ACBW.B4_07_3_B4_08;
pco_transitions1.%X7  := GRAPH_UF_ACBW.B4_08_INIT;
pco_transitions1.%X8  := GRAPH_UF_ACBW.B4_08_B4_09_1;
pco_transitions1.%X9  := GRAPH_UF_ACBW.B4_09_1_INIT;
pco_transitions1.%X10 := GRAPH_UF_ACBW.B4_09_1_B4_09_2;
pco_transitions1.%X11 := GRAPH_UF_ACBW.B4_09_2_INIT;
pco_transitions1.%X12 := GRAPH_UF_ACBW.B4_09_2_B4_09_3;
pco_transitions1.%X13 := GRAPH_UF_ACBW.B4_09_3_INIT;
pco_transitions1.%X14 := GRAPH_UF_ACBW.B4_09_3_B4_10_1;
pco_transitions1.%X15 := GRAPH_UF_ACBW.B4_10_1_INIT;
pco_transitions2.%X0  := GRAPH_UF_ACBW.B4_10_1_B4_10_2;
pco_transitions2.%X1  := GRAPH_UF_ACBW.B4_10_2_INIT;
pco_transitions2.%X2  := GRAPH_UF_ACBW.B4_10_2_B4_10_3;
pco_transitions2.%X3  := GRAPH_UF_ACBW.B4_10_3_INIT;
pco_transitions2.%X4  := GRAPH_UF_ACBW.B4_10_3_B4_11;
pco_transitions2.%X5  := GRAPH_UF_ACBW.B4_11_INIT;
pco_transitions2.%X6  := GRAPH_UF_ACBW.B4_11_B4_12_1;
pco_transitions2.%X7  := GRAPH_UF_ACBW.B4_12_1_INIT;
pco_transitions2.%X8  := GRAPH_UF_ACBW.B4_12_1_B4_12_2;
pco_transitions2.%X9  := GRAPH_UF_ACBW.B4_12_2_INIT;
pco_transitions2.%X10 := GRAPH_UF_ACBW.B4_12_2_B4_12_3;
pco_transitions2.%X11 := GRAPH_UF_ACBW.B4_12_3_INIT;
pco_transitions2.%X12 := GRAPH_UF_ACBW.B4_12_3_B4_13;
pco_transitions2.%X13 := GRAPH_UF_ACBW.B4_13_INIT;
pco_transitions2.%X14 := GRAPH_UF_ACBW.B4_13_SYNC;
pco_transitions2.%X15 := GRAPH_UF_ACBW.SYNC_INIT;


FDEU_893_UF_ACBW_Tr0.AuPosR := pco_transitions0;
FDEU_893_UF_ACBW_Tr1.AuPosR := pco_transitions1;
FDEU_893_UF_ACBW_Tr2.AuPosR := pco_transitions2;

''')

    thePlugin.writeTIALogic('''
(*1=1- INIT,2=2- B4_01_1 - Open / Wait Air Scour Valves,3=3- B4_01_2 - Start Air Scour Fan,4=4- B4_02 (Air Scour UF AC BW) - Operation Air Scour,5=5- B4_03_1 - Stop Air Scour Fan,6=6- B4_03_2 - Close / Wait Air Scour Valves,7=7- B4_04 - Open / Wait Purge Valves,8=8- B4_05 (Purge UF AC BW) - Operation Purge,9=9- B4_06 - Close / Wait Purge Valves,10=10- B4_07_1 - Open / Wait Top Backwash Valves,11=11- B4_07_2 - Start Backwash Pump,12=12- B4_07_3 - Start NaOCL Dosing Pump,13=13- B4_08 (TOP UF AC BW) - Operation Top Backwash CEB,14=14- B4_09_1 - Stop NaOCL Dosing Pump,15=15- B4_09_2 - Stop Backwash Pump,16=16- B4_09_3 - Close / Wait Top Backwash Valves,17=17- B4_10_1 - Open / Wait Bottom Backwash Valves,18=18- B4_10_2 - Start Backwash Pump,19=19- B4_10_3 - Start HCL Dosing Pump,20=20- B4_11 (BOT UF AC BW) - Operation Bottom Backwash CEB,21=21- B4_12_1 - Stop HCL Dosing Pump,22=22- B4_12_2 - Stop Backwash Pump,23=23- B4_12_3 - Close / Wait Bottom Backwash Valves,24=24- B4_13 (Soak UF AC BW) - Operation Soak,25=25- SYNC*)''')

    thePlugin.writeTIALogic('''
(* Automatic generated code from StepperSpecs.xlsx using the TIA_Expert_Stepper_Template <end> *)''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
