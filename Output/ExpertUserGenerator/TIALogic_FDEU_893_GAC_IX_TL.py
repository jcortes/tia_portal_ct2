# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)


def TLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    Decorator = ucpc_library.shared_decorator.ExpressionDecorator()
    # Automatically generated variables
    

# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeTIALogic('''
FUNCTION $name$_TL : VOID
TITLE = '$name$_TL'
//
// Transition Logic of $name$
//
// Lparam1:    $Lparam1$
// Lparam2:    $Lparam2$
// Lparam3:    $Lparam3$
// Lparam4:    $Lparam4$
// Lparam5:    $Lparam5$
// Lparam6:    $Lparam6$
// Lparam7:    $Lparam7$
// Lparam8:    $Lparam8$
// Lparam9:    $Lparam9$
// Lparam10:    $Lparam10$
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_TL'
FAMILY: 'TL'
VAR_TEMP
    pco_transitions0 : WORD;
    pco_transitions1 : WORD;
    pco_transitions2 : WORD;
    pco_transitions3 : WORD;
    pco_transitions4 : WORD;
    pco_transitions5 : WORD;


END_VAR
BEGIN

''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    thePlugin.writeTIALogic('''

DB_ERROR_SIMU.$name$_TL_E := 0 ; // To complete
DB_ERROR_SIMU.$name$_TL_S := 0 ; // To complete

''')

    thePlugin.writeTIALogic('''(* Automatic generated code from StepperSpecs.xlsx using the TIA_Expert_Stepper_Template <begin> *)
''')

    thePlugin.writeTIALogic('''(* VARIABLES COMPUTATION *)



''')

    thePlugin.writeTIALogic(Decorator.decorateExpression('''(* TRANSITIONS COMPUTATION *)

(* 1 TO 2 *) GRAPH_GAC_IX.SHUTDOWN_STOP := NOT $name$.FuStopISt AND $name$.EnRStartSt;
(* 2 TO 1 *) GRAPH_GAC_IX.STOP_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 2 TO 3 *) GRAPH_GAC_IX.STOP_STANDBY := $name$.RunOSt;
(* 3 TO 1 *) GRAPH_GAC_IX.STANDBY_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 3 TO 2 *) GRAPH_GAC_IX.STANDBY_STOP := $name$.TStopISt;
(* 3 TO 4 *) GRAPH_GAC_IX.STANDBY_S2_01_1 := FDEU_893_GAC_IX AND NOT FDEU_893GAC_IX_StopCon AND (FDEU_893_GAC_IX_StartCon OR FDEU_893_GAC_IX_RUN) 
AND NOT FDEU_893_BYPASS_GAC_IX AND ((NOT FDEU_893_GAC_IX_BWCon AND NOT FDEU_893_GAC_IX_BW) OR NOT FDEU_893_GAC_IX_BWEn);
(* 3 TO 9 *) GRAPH_GAC_IX.STANDBY_B5_01_1 := FDEU_893_GAC_IX AND NOT FDEU_893_BYPASS_GAC_IX AND FDEU_893_GAC_IX_BWEn AND (FDEU_893_GAC_IX_BWCon OR FDEU_893_GAC_IX_BW);
(* 3 TO 31 *) GRAPH_GAC_IX.STANDBY_BYPASS := FDEU_893_GAC_IX.RunOSt AND FDEU_893_BYPASS_GAC_IX.PosSt AND (FDEU_893_GAC_IX_StartCon OR (FDEU_893_GAC_IX_RUN.OnSt AND NOT FDEU_893GAC_IX_StopCon));
(* 4 TO 1 *) GRAPH_GAC_IX.S2_01_1_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 4 TO 2 *) GRAPH_GAC_IX.S2_01_1_STOP := $name$.TStopISt;
(* 4 TO 5 *) GRAPH_GAC_IX.S2_01_1_S2_01_2 := FDEU_893_AQE1601.OnSt AND FDEU_893_AQE1602.OnSt AND FDEU_893_AQE1701.OnSt AND FDEU_893_AQE1702.OnSt ;
(* 5 TO 1 *) GRAPH_GAC_IX.S2_01_2_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 5 TO 2 *) GRAPH_GAC_IX.S2_01_2_STOP := $name$.TStopISt;
(* 5 TO 6 *) GRAPH_GAC_IX.S2_01_2_S2_02 := FDEU_893_P1501.OnSt AND TON_GAC_IX_S2_01_2.Q;
(* 6 TO 1 *) GRAPH_GAC_IX.S2_02_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 6 TO 2 *) GRAPH_GAC_IX.S2_02_STOP := $name$.TStopISt;
(* 6 TO 7 *) GRAPH_GAC_IX.S2_02_S2_03_1 := NOT FDEU_893_GAC_IX.RunOSt OR FDEU_893GAC_IX_StopCon OR (FDEU_893_GAC_IX_SBY.OnSt  AND NOT FDEU_893_GAC_IX_StartCon) OR ((FDEU_893_GAC_IX_BWCon OR FDEU_893_GAC_IX_BW.OnSt) AND FDEU_893_GAC_IX_BWEn);
(* 7 TO 1 *) GRAPH_GAC_IX.S2_03_1_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 7 TO 2 *) GRAPH_GAC_IX.S2_03_1_STOP := $name$.TStopISt;
(* 7 TO 8 *) GRAPH_GAC_IX.S2_03_1_S2_03_2 := NOT FDEU_893_P1501.OnSt AND TON_GAC_IX_S2_03_1.Q;
(* 8 TO 1 *) GRAPH_GAC_IX.S2_03_2_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 8 TO 2 *) GRAPH_GAC_IX.S2_03_2_STOP := $name$.TStopISt;
(* 8 TO 3 *) GRAPH_GAC_IX.S2_03_2_STANDBY := FDEU_893_AQE1601.OffSt AND FDEU_893_AQE1602.OffSt AND FDEU_893_AQE1701.OffSt AND FDEU_893_AQE1702.OffSt ;
(* 9 TO 1 *) GRAPH_GAC_IX.B5_01_1_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 9 TO 2 *) GRAPH_GAC_IX.B5_01_1_STOP := $name$.TStopISt;
(* 9 TO 10 *) GRAPH_GAC_IX.B5_01_1_B5_01_2 := FDEU_893_AQE1001.OnSt;
(* 10 TO 1 *) GRAPH_GAC_IX.B5_01_2_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 10 TO 2 *) GRAPH_GAC_IX.B5_01_2_STOP := $name$.TStopISt;
(* 10 TO 11 *) GRAPH_GAC_IX.B5_01_2_B5_01_3 := FDEU_893_AQE1603.OnSt AND FDEU_893_AQE1604.OnSt;
(* 11 TO 1 *) GRAPH_GAC_IX.B5_01_3_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 11 TO 2 *) GRAPH_GAC_IX.B5_01_3_STOP := $name$.TStopISt;
(* 11 TO 12 *) GRAPH_GAC_IX.B5_01_3_B5_02 := FDEU_893_P5101.OnSt AND TON_GAC_IX_B5_01_3.Q;
(* 12 TO 1 *) GRAPH_GAC_IX.B5_02_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 12 TO 2 *) GRAPH_GAC_IX.B5_02_STOP := $name$.TStopISt;
(* 12 TO 13 *) GRAPH_GAC_IX.B5_02_B5_03_1 := TON_GAC_IX_B5_02.Q;
(* 13 TO 1 *) GRAPH_GAC_IX.B5_03_1_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 13 TO 2 *) GRAPH_GAC_IX.B5_03_1_STOP := $name$.TStopISt;
(* 13 TO 14 *) GRAPH_GAC_IX.B5_03_1_B5_03_2 := NOT FDEU_893_P5101.OnSt AND TON_GAC_IX_B5_03_1.Q;
(* 14 TO 1 *) GRAPH_GAC_IX.B5_03_2_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 14 TO 2 *) GRAPH_GAC_IX.B5_03_2_STOP := $name$.TStopISt;
(* 14 TO 15 *) GRAPH_GAC_IX.B5_03_2_B5_04_1 := FDEU_893_AQE1603.OffSt AND FDEU_893_AQE1604.OffSt;
(* 15 TO 1 *) GRAPH_GAC_IX.B5_04_1_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 15 TO 2 *) GRAPH_GAC_IX.B5_04_1_STOP := $name$.TStopISt;
(* 15 TO 16 *) GRAPH_GAC_IX.B5_04_1_B5_04_2 := FDEU_893_AQE1601.OnSt AND FDEU_893_AQE1605.OnSt;
(* 16 TO 1 *) GRAPH_GAC_IX.B5_04_2_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 16 TO 2 *) GRAPH_GAC_IX.B5_04_2_STOP := $name$.TStopISt;
(* 16 TO 17 *) GRAPH_GAC_IX.B5_04_2_B5_05 := FDEU_893_P1501.OnSt AND TON_GAC_IX_B5_04_2.Q;
(* 17 TO 1 *) GRAPH_GAC_IX.B5_05_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 17 TO 2 *) GRAPH_GAC_IX.B5_05_STOP := $name$.TStopISt;
(* 17 TO 18 *) GRAPH_GAC_IX.B5_05_B5_06_1 := TON_GAC_IX_B5_05.Q;
(* 18 TO 1 *) GRAPH_GAC_IX.B5_06_1_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 18 TO 2 *) GRAPH_GAC_IX.B5_06_1_STOP := $name$.TStopISt;
(* 18 TO 19 *) GRAPH_GAC_IX.B5_06_1_B5_06_2 := NOT FDEU_893_P1501.OnSt AND TON_GAC_IX_B5_06_1.Q;
(* 19 TO 1 *) GRAPH_GAC_IX.B5_06_2_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 19 TO 2 *) GRAPH_GAC_IX.B5_06_2_STOP := $name$.TStopISt;
(* 19 TO 20 *) GRAPH_GAC_IX.B5_06_2_B6_01_2 := FDEU_893_AQE1601.OffSt AND FDEU_893_AQE1605.OffSt;
(* 20 TO 1 *) GRAPH_GAC_IX.B6_01_2_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 20 TO 2 *) GRAPH_GAC_IX.B6_01_2_STOP := $name$.TStopISt;
(* 20 TO 21 *) GRAPH_GAC_IX.B6_01_2_B6_01_3 := FDEU_893_AQE1703.OnSt AND FDEU_893_AQE1704.OnSt;
(* 21 TO 1 *) GRAPH_GAC_IX.B6_01_3_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 21 TO 2 *) GRAPH_GAC_IX.B6_01_3_STOP := $name$.TStopISt;
(* 21 TO 22 *) GRAPH_GAC_IX.B6_01_3_B6_02 := FDEU_893_P5101.OnSt AND TON_GAC_IX_B6_01_3.Q;
(* 22 TO 1 *) GRAPH_GAC_IX.B6_02_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 22 TO 2 *) GRAPH_GAC_IX.B6_02_STOP := $name$.TStopISt;
(* 22 TO 23 *) GRAPH_GAC_IX.B6_02_B6_03_1 := TON_GAC_IX_B6_02.Q;
(* 23 TO 1 *) GRAPH_GAC_IX.B6_03_1_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 23 TO 2 *) GRAPH_GAC_IX.B6_03_1_STOP := $name$.TStopISt;
(* 23 TO 24 *) GRAPH_GAC_IX.B6_03_1_B6_03_2 := NOT FDEU_893_P5101.OnSt AND TON_GAC_IX_B6_03_1.Q;
(* 24 TO 1 *) GRAPH_GAC_IX.B6_03_2_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 24 TO 2 *) GRAPH_GAC_IX.B6_03_2_STOP := $name$.TStopISt;
(* 24 TO 25 *) GRAPH_GAC_IX.B6_03_2_B6_04_1 := FDEU_893_AQE1703.OffSt AND FDEU_893_AQE1704.OffSt;
(* 25 TO 1 *) GRAPH_GAC_IX.B6_04_1_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 25 TO 2 *) GRAPH_GAC_IX.B6_04_1_STOP := $name$.TStopISt;
(* 25 TO 26 *) GRAPH_GAC_IX.B6_04_1_B6_04_2 := FDEU_893_AQE1701.OnSt AND FDEU_893_AQE1705.OnSt;
(* 26 TO 1 *) GRAPH_GAC_IX.B6_04_2_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 26 TO 2 *) GRAPH_GAC_IX.B6_04_2_STOP := $name$.TStopISt;
(* 26 TO 27 *) GRAPH_GAC_IX.B6_04_2_B6_05 := FDEU_893_P1501.OnSt AND TON_GAC_IX_B6_04_2.Q;
(* 27 TO 1 *) GRAPH_GAC_IX.B6_05_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 27 TO 2 *) GRAPH_GAC_IX.B6_05_STOP := $name$.TStopISt;
(* 27 TO 28 *) GRAPH_GAC_IX.B6_05_B6_06_1 := TON_GAC_IX_B6_05.Q;
(* 28 TO 1 *) GRAPH_GAC_IX.B6_06_1_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 28 TO 2 *) GRAPH_GAC_IX.B6_06_1_STOP := $name$.TStopISt;
(* 28 TO 29 *) GRAPH_GAC_IX.B6_06_1_B6_06_2 := NOT FDEU_893_P1501.OnSt AND TON_GAC_IX_B6_06_1.Q;
(* 29 TO 1 *) GRAPH_GAC_IX.B6_06_2_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 29 TO 2 *) GRAPH_GAC_IX.B6_06_2_STOP := $name$.TStopISt;
(* 29 TO 30 *) GRAPH_GAC_IX.B6_06_2_B6_06_3 := FDEU_893_AQE1701.OffSt AND FDEU_893_AQE1705.OffSt;
(* 30 TO 1 *) GRAPH_GAC_IX.B6_06_3_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 30 TO 2 *) GRAPH_GAC_IX.B6_06_3_STOP := $name$.TStopISt;
(* 30 TO 3 *) GRAPH_GAC_IX.B6_06_3_STANDBY := FDEU_893_AQE1001.OffSt OR FDEU_893_AQE1001.AuOnRSt;
(* 31 TO 3 *) GRAPH_GAC_IX.BYPASS_STANDBY := NOT $name$.RunOSt OR FDEU_893GAC_IX_StopCon OR (FDEU_893_GAC_IX_SBY.OnSt AND NOT FDEU_893_GAC_IX_StartCon);
'''))

    thePlugin.writeTIALogic('''
(* WORD STATUS FOR STEPS *)
FDEU_893_GAC_IX_St.AuPosR := INT_TO_WORD(GRAPH_GAC_IX.S_NO);
''')

    thePlugin.writeTIALogic('''
(* WORD STATUS FOR TRANSITIONS *)
pco_transitions0.%X0  := GRAPH_GAC_IX.SHUTDOWN_STOP;
pco_transitions0.%X1  := GRAPH_GAC_IX.STOP_SHUTDOWN;
pco_transitions0.%X2  := GRAPH_GAC_IX.STOP_STANDBY;
pco_transitions0.%X3  := GRAPH_GAC_IX.STANDBY_SHUTDOWN;
pco_transitions0.%X4  := GRAPH_GAC_IX.STANDBY_STOP;
pco_transitions0.%X5  := GRAPH_GAC_IX.STANDBY_S2_01_1;
pco_transitions0.%X6  := GRAPH_GAC_IX.STANDBY_B5_01_1;
pco_transitions0.%X7  := GRAPH_GAC_IX.STANDBY_BYPASS;
pco_transitions0.%X8  := GRAPH_GAC_IX.S2_01_1_SHUTDOWN;
pco_transitions0.%X9  := GRAPH_GAC_IX.S2_01_1_STOP;
pco_transitions0.%X10 := GRAPH_GAC_IX.S2_01_1_S2_01_2;
pco_transitions0.%X11 := GRAPH_GAC_IX.S2_01_2_SHUTDOWN;
pco_transitions0.%X12 := GRAPH_GAC_IX.S2_01_2_STOP;
pco_transitions0.%X13 := GRAPH_GAC_IX.S2_01_2_S2_02;
pco_transitions0.%X14 := GRAPH_GAC_IX.S2_02_SHUTDOWN;
pco_transitions0.%X15 := GRAPH_GAC_IX.S2_02_STOP;
pco_transitions1.%X0  := GRAPH_GAC_IX.S2_02_S2_03_1;
pco_transitions1.%X1  := GRAPH_GAC_IX.S2_03_1_SHUTDOWN;
pco_transitions1.%X2  := GRAPH_GAC_IX.S2_03_1_STOP;
pco_transitions1.%X3  := GRAPH_GAC_IX.S2_03_1_S2_03_2;
pco_transitions1.%X4  := GRAPH_GAC_IX.S2_03_2_SHUTDOWN;
pco_transitions1.%X5  := GRAPH_GAC_IX.S2_03_2_STOP;
pco_transitions1.%X6  := GRAPH_GAC_IX.S2_03_2_STANDBY;
pco_transitions1.%X7  := GRAPH_GAC_IX.B5_01_1_SHUTDOWN;
pco_transitions1.%X8  := GRAPH_GAC_IX.B5_01_1_STOP;
pco_transitions1.%X9  := GRAPH_GAC_IX.B5_01_1_B5_01_2;
pco_transitions1.%X10 := GRAPH_GAC_IX.B5_01_2_SHUTDOWN;
pco_transitions1.%X11 := GRAPH_GAC_IX.B5_01_2_STOP;
pco_transitions1.%X12 := GRAPH_GAC_IX.B5_01_2_B5_01_3;
pco_transitions1.%X13 := GRAPH_GAC_IX.B5_01_3_SHUTDOWN;
pco_transitions1.%X14 := GRAPH_GAC_IX.B5_01_3_STOP;
pco_transitions1.%X15 := GRAPH_GAC_IX.B5_01_3_B5_02;
pco_transitions2.%X0  := GRAPH_GAC_IX.B5_02_SHUTDOWN;
pco_transitions2.%X1  := GRAPH_GAC_IX.B5_02_STOP;
pco_transitions2.%X2  := GRAPH_GAC_IX.B5_02_B5_03_1;
pco_transitions2.%X3  := GRAPH_GAC_IX.B5_03_1_SHUTDOWN;
pco_transitions2.%X4  := GRAPH_GAC_IX.B5_03_1_STOP;
pco_transitions2.%X5  := GRAPH_GAC_IX.B5_03_1_B5_03_2;
pco_transitions2.%X6  := GRAPH_GAC_IX.B5_03_2_SHUTDOWN;
pco_transitions2.%X7  := GRAPH_GAC_IX.B5_03_2_STOP;
pco_transitions2.%X8  := GRAPH_GAC_IX.B5_03_2_B5_04_1;
pco_transitions2.%X9  := GRAPH_GAC_IX.B5_04_1_SHUTDOWN;
pco_transitions2.%X10 := GRAPH_GAC_IX.B5_04_1_STOP;
pco_transitions2.%X11 := GRAPH_GAC_IX.B5_04_1_B5_04_2;
pco_transitions2.%X12 := GRAPH_GAC_IX.B5_04_2_SHUTDOWN;
pco_transitions2.%X13 := GRAPH_GAC_IX.B5_04_2_STOP;
pco_transitions2.%X14 := GRAPH_GAC_IX.B5_04_2_B5_05;
pco_transitions2.%X15 := GRAPH_GAC_IX.B5_05_SHUTDOWN;
pco_transitions3.%X0  := GRAPH_GAC_IX.B5_05_STOP;
pco_transitions3.%X1  := GRAPH_GAC_IX.B5_05_B5_06_1;
pco_transitions3.%X2  := GRAPH_GAC_IX.B5_06_1_SHUTDOWN;
pco_transitions3.%X3  := GRAPH_GAC_IX.B5_06_1_STOP;
pco_transitions3.%X4  := GRAPH_GAC_IX.B5_06_1_B5_06_2;
pco_transitions3.%X5  := GRAPH_GAC_IX.B5_06_2_SHUTDOWN;
pco_transitions3.%X6  := GRAPH_GAC_IX.B5_06_2_STOP;
pco_transitions3.%X7  := GRAPH_GAC_IX.B5_06_2_B6_01_2;
pco_transitions3.%X8  := GRAPH_GAC_IX.B6_01_2_SHUTDOWN;
pco_transitions3.%X9  := GRAPH_GAC_IX.B6_01_2_STOP;
pco_transitions3.%X10 := GRAPH_GAC_IX.B6_01_2_B6_01_3;
pco_transitions3.%X11 := GRAPH_GAC_IX.B6_01_3_SHUTDOWN;
pco_transitions3.%X12 := GRAPH_GAC_IX.B6_01_3_STOP;
pco_transitions3.%X13 := GRAPH_GAC_IX.B6_01_3_B6_02;
pco_transitions3.%X14 := GRAPH_GAC_IX.B6_02_SHUTDOWN;
pco_transitions3.%X15 := GRAPH_GAC_IX.B6_02_STOP;
pco_transitions4.%X0  := GRAPH_GAC_IX.B6_02_B6_03_1;
pco_transitions4.%X1  := GRAPH_GAC_IX.B6_03_1_SHUTDOWN;
pco_transitions4.%X2  := GRAPH_GAC_IX.B6_03_1_STOP;
pco_transitions4.%X3  := GRAPH_GAC_IX.B6_03_1_B6_03_2;
pco_transitions4.%X4  := GRAPH_GAC_IX.B6_03_2_SHUTDOWN;
pco_transitions4.%X5  := GRAPH_GAC_IX.B6_03_2_STOP;
pco_transitions4.%X6  := GRAPH_GAC_IX.B6_03_2_B6_04_1;
pco_transitions4.%X7  := GRAPH_GAC_IX.B6_04_1_SHUTDOWN;
pco_transitions4.%X8  := GRAPH_GAC_IX.B6_04_1_STOP;
pco_transitions4.%X9  := GRAPH_GAC_IX.B6_04_1_B6_04_2;
pco_transitions4.%X10 := GRAPH_GAC_IX.B6_04_2_SHUTDOWN;
pco_transitions4.%X11 := GRAPH_GAC_IX.B6_04_2_STOP;
pco_transitions4.%X12 := GRAPH_GAC_IX.B6_04_2_B6_05;
pco_transitions4.%X13 := GRAPH_GAC_IX.B6_05_SHUTDOWN;
pco_transitions4.%X14 := GRAPH_GAC_IX.B6_05_STOP;
pco_transitions4.%X15 := GRAPH_GAC_IX.B6_05_B6_06_1;
pco_transitions5.%X0  := GRAPH_GAC_IX.B6_06_1_SHUTDOWN;
pco_transitions5.%X1  := GRAPH_GAC_IX.B6_06_1_STOP;
pco_transitions5.%X2  := GRAPH_GAC_IX.B6_06_1_B6_06_2;
pco_transitions5.%X3  := GRAPH_GAC_IX.B6_06_2_SHUTDOWN;
pco_transitions5.%X4  := GRAPH_GAC_IX.B6_06_2_STOP;
pco_transitions5.%X5  := GRAPH_GAC_IX.B6_06_2_B6_06_3;
pco_transitions5.%X6  := GRAPH_GAC_IX.B6_06_3_SHUTDOWN;
pco_transitions5.%X7  := GRAPH_GAC_IX.B6_06_3_STOP;
pco_transitions5.%X8  := GRAPH_GAC_IX.B6_06_3_STANDBY;
pco_transitions5.%X9  := GRAPH_GAC_IX.BYPASS_STANDBY;
pco_transitions5.%X10 := false;
pco_transitions5.%X11 := false;
pco_transitions5.%X12 := false;
pco_transitions5.%X13 := false;
pco_transitions5.%X14 := false;
pco_transitions5.%X15 := false;


FDEU_893_GAC_IX_Tr0.AuPosR := pco_transitions0;
FDEU_893_GAC_IX_Tr1.AuPosR := pco_transitions1;
FDEU_893_GAC_IX_Tr2.AuPosR := pco_transitions2;
FDEU_893_GAC_IX_Tr3.AuPosR := pco_transitions3;
FDEU_893_GAC_IX_Tr4.AuPosR := pco_transitions4;
FDEU_893_GAC_IX_Tr5.AuPosR := pco_transitions5;

''')

    thePlugin.writeTIALogic('''
(*1=1-SHUTDOWN,2=2-STOP,3=3-STANDBY,4=4-S2_01_1 - Open / Wait Service Valves,5=5-S2_01_2 - StartGAC Pump,6=6-S2_02 - OperationGAC and IX,7=7-S2_03_1 - StopGAC Pump,8=8-S2_03_2 - Close / Wait Service Valves,9=9-B5_01_1 - Open / Wait Common Inlet Valve,10=10-B5_01_2 - Open / Wait Backwash Valves,11=11-B5_01_3 - Start Backwash Pump,12=12-B5_02 - Operation Backwash,13=13-B5_03_1 - Stop Backwash Pump,14=14-B5_03_2 - Close / Wait Backwash Valves,15=15-B5_04_1 - Open / WaitRinse Valves,16=16-B5_04_2 - StartGAC Pump,17=17-B5_05 - Operation Rinse,18=18-B5_06_1 - StopGAC Pump,19=19-B5_06_2 - Close / Wait Rinse Valves,20=20-B6_01_2 - Open / Wait Backwash Valves,21=21-B6_01_3 - Start Backwash Pump,22=22-B6_02 - Operation Backwash,23=23-B6_03_1 - Stop Backwash Pump,24=24-B6_03_2 - Close / Wait Backwash Valves,25=25-B6_04_1 - Open / Wait Rinse Valves,26=26-B6_04_2 - Start GAC Pump,27=27-B6_05 - Operation Rinse,28=28-B6_06_1 - Stop GAC Pump,29=29-B6_06_2 - Close / Wait Rinse Valves,30=30-B6_06_3 - Close / Wait Common Inlet Valve,31=31-BYPASS*)''')

    thePlugin.writeTIALogic('''
(* Automatic generated code from StepperSpecs.xlsx using the TIA_Expert_Stepper_Template <end> *)''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
