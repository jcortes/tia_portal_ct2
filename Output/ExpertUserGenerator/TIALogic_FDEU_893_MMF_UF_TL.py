# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)


def TLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    Decorator = ucpc_library.shared_decorator.ExpressionDecorator()
    # Automatically generated variables
    

# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeTIALogic('''
FUNCTION $name$_TL : VOID
TITLE = '$name$_TL'
//
// Transition Logic of $name$
//
// Lparam1:    $Lparam1$
// Lparam2:    $Lparam2$
// Lparam3:    $Lparam3$
// Lparam4:    $Lparam4$
// Lparam5:    $Lparam5$
// Lparam6:    $Lparam6$
// Lparam7:    $Lparam7$
// Lparam8:    $Lparam8$
// Lparam9:    $Lparam9$
// Lparam10:    $Lparam10$
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_TL'
FAMILY: 'TL'
VAR_TEMP
    pco_transitions0 : WORD;
    pco_transitions1 : WORD;


END_VAR
BEGIN

''')
    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    thePlugin.writeTIALogic('''

DB_ERROR_SIMU.$name$_TL_E := 0 ; // To complete
DB_ERROR_SIMU.$name$_TL_S := 0 ; // To complete

''')

    thePlugin.writeTIALogic('''(* Automatic generated code from StepperSpecs.xlsx using the TIA_Expert_Stepper_Template <begin> *)
''')

    thePlugin.writeTIALogic('''(* VARIABLES COMPUTATION *)



''')

    thePlugin.writeTIALogic(Decorator.decorateExpression('''(* TRANSITIONS COMPUTATION *)

(* 1 TO 2 *) GRAPH_MMF_UF.SHUTDOWN_STOP := NOT $name$.FuStopISt AND $name$.EnRStartSt;
(* 2 TO 1 *) GRAPH_MMF_UF.STOP_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 2 TO 3 *) GRAPH_MMF_UF.STOP_STANDBY := $name$.RunOSt;
(* 3 TO 1 *) GRAPH_MMF_UF.STANDBY_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 3 TO 2 *) GRAPH_MMF_UF.STANDBY_STOP := $name$.TStopISt;
(* 3 TO 4 *) GRAPH_MMF_UF.STANDBY_FILTRATION := FDEU_893_MMF_UF  AND NOT FDEU_893_MMF_UF_StopCon AND (FDEU_893_MMF_UF_RUN OR FDEU_893_MMF_UF_StartCon)
AND NOT (((FDEU_893_MMF_BW_Con OR FDEU_893_MMF_BW_RUN) AND FDEU_893_MMF_BW_En) OR 
((FDEU_893_UF_BW_Con OR FDEU_893_UF_BW_RUN) AND FDEU_893_UF_BW_En) OR
((FDEU_893_UF_OXBW_Con OR FDEU_893_UF_OX_BW) AND FDEU_893_UF_OXBW_En) OR
((FDEU_893_UF_ACBW_Con OR FDEU_893_UF_AC_BW) AND FDEU_893_UF_ACBW_En));
(* 3 TO 5 *) GRAPH_MMF_UF.STANDBY_MMF_BW := (FDEU_893_MMF_UF AND (FDEU_893_MMF_BW_Con OR FDEU_893_MMF_BW_RUN) AND FDEU_893_MMF_BW_En);
(* 3 TO 6 *) GRAPH_MMF_UF.STANDBY_UF_BW := (FDEU_893_MMF_UF AND (FDEU_893_UF_BW_Con OR FDEU_893_UF_BW_RUN) AND FDEU_893_UF_BW_En)
AND NOT (FDEU_893_UF_OXBW_Con OR FDEU_893_UF_OX_BW) 
AND NOT (FDEU_893_UF_ACBW_Con OR FDEU_893_UF_AC_BW);
(* 3 TO 7 *) GRAPH_MMF_UF.STANDBY_UF_OX_BW := (FDEU_893_MMF_UF AND (FDEU_893_UF_OXBW_Con OR FDEU_893_UF_OX_BW) AND FDEU_893_UF_OXBW_En);
(* 3 TO 8 *) GRAPH_MMF_UF.STANDBY_UF_AC_BW := (FDEU_893_MMF_UF AND (FDEU_893_UF_ACBW_Con OR FDEU_893_UF_AC_BW) AND FDEU_893_UF_ACBW_En);
(* 4 TO 1 *) GRAPH_MMF_UF.FILTRATION_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 4 TO 2 *) GRAPH_MMF_UF.FILTRATION_STOP := $name$.TStopISt;
(* 4 TO 3 *) GRAPH_MMF_UF.FILTRATION_STANDBY := GRAPH_MMF_UF_FIL.SYNC.X;
(* 5 TO 1 *) GRAPH_MMF_UF.MMF_BW_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 5 TO 2 *) GRAPH_MMF_UF.MMF_BW_STOP := $name$.TStopISt;
(* 5 TO 3 *) GRAPH_MMF_UF.MMF_BW_STANDBY := GRAPH_MMF_BW.SYNC.X;
(* 6 TO 1 *) GRAPH_MMF_UF.UF_BW_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 6 TO 2 *) GRAPH_MMF_UF.UF_BW_STOP := $name$.TStopISt;
(* 6 TO 3 *) GRAPH_MMF_UF.UF_BW_STANDBY := GRAPH_UF_BW.SYNC.X;
(* 7 TO 1 *) GRAPH_MMF_UF.UF_OX_BW_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 7 TO 2 *) GRAPH_MMF_UF.UF_OX_BW_STOP := $name$.TStopISt;
(* 7 TO 6 *) GRAPH_MMF_UF.UF_OX_BW_UF_BW := GRAPH_UF_OXBW.SYNC.X;
(* 8 TO 1 *) GRAPH_MMF_UF.UF_AC_BW_SHUTDOWN := $name$.FuStopISt OR NOT $name$.EnRStartSt;
(* 8 TO 2 *) GRAPH_MMF_UF.UF_AC_BW_STOP := $name$.TStopISt;
(* 8 TO 6 *) GRAPH_MMF_UF.UF_AC_BW_UF_BW := GRAPH_UF_ACBW.SYNC.X;
'''))

    thePlugin.writeTIALogic('''
(* WORD STATUS FOR STEPS *)
FDEU_893_MMF_UF_St.AuPosR := INT_TO_WORD(GRAPH_MMF_UF.S_NO);
''')

    thePlugin.writeTIALogic('''
(* WORD STATUS FOR TRANSITIONS *)
pco_transitions0.%X0  := GRAPH_MMF_UF.SHUTDOWN_STOP;
pco_transitions0.%X1  := GRAPH_MMF_UF.STOP_SHUTDOWN;
pco_transitions0.%X2  := GRAPH_MMF_UF.STOP_STANDBY;
pco_transitions0.%X3  := GRAPH_MMF_UF.STANDBY_SHUTDOWN;
pco_transitions0.%X4  := GRAPH_MMF_UF.STANDBY_STOP;
pco_transitions0.%X5  := GRAPH_MMF_UF.STANDBY_FILTRATION;
pco_transitions0.%X6  := GRAPH_MMF_UF.STANDBY_MMF_BW;
pco_transitions0.%X7  := GRAPH_MMF_UF.STANDBY_UF_BW;
pco_transitions0.%X8  := GRAPH_MMF_UF.STANDBY_UF_OX_BW;
pco_transitions0.%X9  := GRAPH_MMF_UF.STANDBY_UF_AC_BW;
pco_transitions0.%X10 := GRAPH_MMF_UF.FILTRATION_SHUTDOWN;
pco_transitions0.%X11 := GRAPH_MMF_UF.FILTRATION_STOP;
pco_transitions0.%X12 := GRAPH_MMF_UF.FILTRATION_STANDBY;
pco_transitions0.%X13 := GRAPH_MMF_UF.MMF_BW_SHUTDOWN;
pco_transitions0.%X14 := GRAPH_MMF_UF.MMF_BW_STOP;
pco_transitions0.%X15 := GRAPH_MMF_UF.MMF_BW_STANDBY;
pco_transitions1.%X0  := GRAPH_MMF_UF.UF_BW_SHUTDOWN;
pco_transitions1.%X1  := GRAPH_MMF_UF.UF_BW_STOP;
pco_transitions1.%X2  := GRAPH_MMF_UF.UF_BW_STANDBY;
pco_transitions1.%X3  := GRAPH_MMF_UF.UF_OX_BW_SHUTDOWN;
pco_transitions1.%X4  := GRAPH_MMF_UF.UF_OX_BW_STOP;
pco_transitions1.%X5  := GRAPH_MMF_UF.UF_OX_BW_UF_BW;
pco_transitions1.%X6  := GRAPH_MMF_UF.UF_AC_BW_SHUTDOWN;
pco_transitions1.%X7  := GRAPH_MMF_UF.UF_AC_BW_STOP;
pco_transitions1.%X8  := GRAPH_MMF_UF.UF_AC_BW_UF_BW;
pco_transitions1.%X9  := false;
pco_transitions1.%X10 := false;
pco_transitions1.%X11 := false;
pco_transitions1.%X12 := false;
pco_transitions1.%X13 := false;
pco_transitions1.%X14 := false;
pco_transitions1.%X15 := false;


FDEU_893_MMF_UF_Tr0.AuPosR := pco_transitions0;
FDEU_893_MMF_UF_Tr1.AuPosR := pco_transitions1;

''')

    thePlugin.writeTIALogic('''
(*1=1-SHUTDOWN,2=2-STOP,3=3-STANDBY,4=4-FILTRATION,5=5-MMF_BW,6=6-UF_BW,7=7-UF_OX_BW,8=8-UF_AC_BW*)''')

    thePlugin.writeTIALogic('''
(* Automatic generated code from StepperSpecs.xlsx using the TIA_Expert_Stepper_Template <end> *)''')

    thePlugin.writeTIALogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
